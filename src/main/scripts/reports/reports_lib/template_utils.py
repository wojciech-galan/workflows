#!/usr/bin/python3

import json
from docxtpl import DocxTemplate, RichText, InlineImage
from docx.shared import Pt
import math
import re


def get_json_as_dict(json_path: str) -> json:
    with open(json_path) as json_file:
        return json.load(json_file)


def set_precision_and_split_genes(panel: dict, interval: int) -> dict:
    for gene in panel:
        gene['score'] = format(gene['score'], '.2f')

    example_cell = {"name": "Gene name", "score": "phenotype match", "type": "type"}
    panel.insert(0, example_cell)

    return [panel[i:i + interval] for i in range(0, len(panel), interval)]


def round_value(value: float) -> float:
    return round(value, 3 - int(math.floor(math.log10(abs(value)))) - 1)


def prepare_basic_images(sex: str, doc):
    return InlineImage(doc, f"/resources/images/human-{sex}.png", width=Pt(12), height=Pt(12))


def prepare_sample_info_field(sample_info: dict, field: str):
    size = '8pt'
    bold = False
    if field in ["name", "surname"]:
        size = '12pt'
        bold = True
    if field == "sex":
        sample_info[field] = "Male" if sample_info[field] == "M" else "Female" if sample_info[field] == "F" else ""
    return RichText(sample_info[field], color='01275B', size=size, font='Muli', bold=bold)


def prepare_sample_info_dict(sample_info: dict) -> dict:
    results = dict()
    for key in sample_info.keys():
        results[key] = prepare_sample_info_field(sample_info, key)
    results["fullname"] = RichText(sample_info['name'] + " " + sample_info['surname'],
                                   color='01275B', size='8pt', font='Muli')
    return results


def get_basic_template_json(sample_info: dict, doc) -> dict:
    basic_template_json = {
        'page_break': RichText('\f'),
        'images': {
            'M': prepare_basic_images("male", doc),
            'F': prepare_basic_images("female", doc)
        },
        'sample_info': prepare_sample_info_dict(sample_info)
    }
    return basic_template_json


def get_igv_images(igv_pngs_json: dict, doc) -> dict:
    images = dict()
    for igv_png in igv_pngs_json:
        for key in igv_png.keys():
            images[key] = InlineImage(doc, igv_png[key], width=Pt(550), height=Pt(400))
    return images


def variant_information(variant: dict) -> list:
    rs = variant["ISEQ_DBSNP_RS"][0] if variant["ISEQ_DBSNP_RS"] else None
    chrom_pos = variant["CHROM"] + ":" + str(variant['POS'])
    GT = variant[list(variant.keys())[-1]]["GT"]
    zygosity = "Heterozygote " + variant["REF"] + "/" + variant["ALT"] if 0 in GT else "Homozygote " + variant["REF"] \
                                                                                       + "/" + variant["ALT"] \
                                                                                       if 0 not in GT \
                                                                                       else "Unknown zygosity"
    feature_ID = variant["ISEQ_REPORT_ANN"][0]["Feature_ID"]
    HGVS_c = variant["ISEQ_REPORT_ANN"][0]["HGVS.c"]
    HGVS_p = variant["ISEQ_REPORT_ANN"][0]["HGVS.p"]
    depth = "Allel depth (ref/alt): " + str(variant[list(variant.keys())[-1]]["AD"][0]) + "/" + \
            str(variant[list(variant.keys())[-1]]["AD"][1])
    try:
        quality_value = round_value(variant["QUAL"])
        quality_level = "Low" if quality_value < 100 else "Dubious" if 100 <= quality_value < 200 \
            else "Acceptable" if 200 <= quality_value < 300 else "High"
        quality = quality_level + " quality call (QC: {})".format(str(quality_value))
    except:
        quality = "QC: N/A"
    variant_info = [i for i in [rs, chrom_pos, zygosity, feature_ID, HGVS_c, HGVS_p, depth, quality] if i]
    return variant_info


def prepare_data(classification: dict) -> dict:
    for item in classification:
        # VARIANT
        item['VARIANT'] = variant_information(item)

        # CLINVAR classification
        item["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"] = item["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0] \
            if (item["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"] and
                item["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0] != 'not_provided') else "undefined"
        others_classifications = ["affects", "association", "association_not_found", "drug_response", "not_provided",
                                  "other", "protective", "risk_factor"]
        if item["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"] in others_classifications:
            item["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"] = "others"

        # DISEASES
        diseases = []
        try:
            for disease in item["ISEQ_DISEASE_GHR"].split("^"):
                disease = re.sub('([a-zA-Z])', lambda x: x.groups()[0].upper(), disease, 1)
                diseases.append(disease.replace("_", " "))
            item['DISEASES'] = diseases
        except AttributeError:
            item['DISEASES'] = diseases

        # INHERITANCE
        inheritance = []
        try:
            for inh in item["ISEQ_HPO_INHERITANCE"][0].split(":")[0].split("^"):
                if inh != ".":
                    inheritance.append(inh.replace("_", " "))
            item['INHERITANCE'] = inheritance
        except TypeError:
            item['INHERITANCE'] = inheritance

        # VARIANT FREQUENCY
        item["WES_AF"] = item["ISEQ_GNOMAD_EXOMES_AF_nfe"][0] if item["ISEQ_GNOMAD_EXOMES_AF_nfe"] else None
        item["WES_AF"] = round_value(item["WES_AF"]) if item["WES_AF"] else None
        GENOMES_AF_nfe = item["ISEQ_GNOMAD_GENOMES_AF_nfe"][0] if item["ISEQ_GNOMAD_GENOMES_AF_nfe"] else None
        GENOMES_V3_AF_nfe = item["ISEQ_GNOMAD_GENOMES_V3_AF_nfe"][0] if item["ISEQ_GNOMAD_GENOMES_V3_AF_nfe"] else None
        WGS_AF = [i for i in [GENOMES_AF_nfe, GENOMES_V3_AF_nfe] if i]
        item["WGS_AF"] = max(WGS_AF) if WGS_AF else None
        item["WGS_AF"] = round_value(item["WGS_AF"]) if item["WGS_AF"] else None
        EXOMES_popmax_AF = item["ISEQ_GNOMAD_EXOMES_popmax_AF"][0] if item["ISEQ_GNOMAD_EXOMES_popmax_AF"] else None
        GENOMES_popmax_AF = item["ISEQ_GNOMAD_GENOMES_popmax_AF"][0] if item["ISEQ_GNOMAD_GENOMES_popmax_AF"] else None
        EXAC_popmax_AF = item["ISEQ_EXAC_POPMAX_AF"][0] if item["ISEQ_EXAC_POPMAX_AF"] else None
        GENOMES_V3_popmax_AF = item["ISEQ_GNOMAD_GENOMES_V3_popmax_AF"][0] if item[
            "ISEQ_GNOMAD_GENOMES_V3_popmax_AF"] else None
        valuesAF = [EXOMES_popmax_AF, GENOMES_popmax_AF, EXAC_popmax_AF, GENOMES_V3_popmax_AF]
        EXOMES_popmax = item["ISEQ_GNOMAD_EXOMES_popmax"][0] if item["ISEQ_GNOMAD_EXOMES_popmax"] else None
        GENOMES_popmax = item["ISEQ_GNOMAD_GENOMES_popmax"][0] if item["ISEQ_GNOMAD_GENOMES_popmax"] else None
        EXAC_popmax = item["ISEQ_EXAC_POPMAX"][0] if item["ISEQ_EXAC_POPMAX"] else None
        GENOMES_V3_popmax = item["ISEQ_GNOMAD_GENOMES_V3_popmax"][0] if item["ISEQ_GNOMAD_GENOMES_V3_popmax"] else None
        valuesPOP = [EXOMES_popmax, GENOMES_popmax, EXAC_popmax, GENOMES_V3_popmax]
        try:
            item["maxAF"] = max([i for i in valuesAF if i])
            item["maxAF"] = round_value(item["maxAF"]) if item["maxAF"] else None
            maxAF_index = valuesAF.index(max([i for i in valuesAF if i]))
            item["maxPOP"] = valuesPOP[maxAF_index].upper()
        except ValueError:
            item["maxAF"] = None
            item["maxPOP"] = None

        # PREDICTED IMPACT
        items_to_change = {
            "ISEQ_SIFT4G_MIN": "SIFT",
            "PhastCons100way": "PhastCons",
            "ISEQ_GERP": "GERP",
            "ISEQ_M_CAP": "MCAP"
        }
        for key, value in items_to_change.items():
            item[value] = item[key][0] if item[key] else None
            item[value] = round_value(item[value]) if item[value] else None

        for field in item:
            if re.findall('SCORE', field) and field != "ISEQ_GENE_PANEL_SCORE":
                try:
                    item[field] = round(item[field], 3 - int(math.floor(math.log10(abs(item[field])))) - 1)
                except ValueError:
                    continue
        # Gene
        item["GENE"] = item["ISEQ_REPORT_ANN"][0]["Gene_Name"]
    return classification


def prepare_methods(content: dict, genome_or_exome: str, analysis_group: str) -> dict:
    # repeatable variables
    variant_calling = 'variant_calling_and_spurious_variant_removing'
    variant_annotation = 'variant_annotation_and_annotation_based_filtering'

    methods = dict()
    methods['fastq_quality_check'] = content['methods']['fastq_quality_check']['text'][0]
    methods['alignment_and_bam_files_processing'] = content['methods']['alignment_and_bam_files_processing']['text'][0]
    methods['variant_calling_and_spurious_variant_removing_paragraph_1'] = \
        content['methods'][variant_calling]['text'][0].format(
        variant_calling_and_spurious_variant_removing_intervals=
        content['methods'][variant_calling][genome_or_exome]['intervals'])
    methods['variant_calling_and_spurious_variant_removing_paragraph_2'] = \
        content['methods'][variant_calling]['text'][1].format(
        variant_calling_and_spurious_variant_removing_filtering=
        content['methods'][variant_calling][genome_or_exome]['filtering'])
    methods['variant_calling_and_spurious_variant_removing_paragraph_3'] = \
        content['methods'][variant_calling]['text'][2].format(
        variant_calling_and_spurious_variant_removing_hard_filtering=
        content['methods'][variant_calling][genome_or_exome]['hard_filtering'])
    methods['variant_calling_and_spurious_variant_removing_paragraph_4'] = \
        content['methods'][variant_calling]['text'][3]
    if analysis_group == "carrier_screening":
        methods['variant_annotation_and_annotation_based_filtering_paragraph_1'] = \
            content['methods'][variant_annotation][analysis_group]['text'][0]
    else:
        methods['variant_annotation_and_annotation_based_filtering_paragraph_1'] = \
            content['methods'][variant_annotation][analysis_group]['text'][0].format(
            variant_annotation_and_annotation_based_filtering=
            content['methods'][variant_annotation][analysis_group][genome_or_exome]['filtering'])
    methods['variant_annotation_and_annotation_based_filtering_paragraph_2'] = \
        content['methods'][variant_annotation][analysis_group]['text'][1]
    return methods

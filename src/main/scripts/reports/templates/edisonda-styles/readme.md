## link
text to click on 

`<text:p><text:a xlink:type="simple" xlink:href="#LABEL" text:style-name="Internet_20_link"> Tutaj klikasz </text:a></text:p>`

text to be linked to

```<text:p> <text:bookmark-start text:name="LABEL"/> Tu Cię przenosi <text:bookmark-end text:name="LABEL"/> </text:p>```


## whole page background
Add to `<style:master-page style:name="Standard" style:page-layout-name="{{layout.name}}"/>` those lines:

```
<style:page-layout style:name="{{layout.name}}">
      <style:page-layout-properties
        (...)
        draw:fill="bitmap"
        draw:fill-image-name="gradient"
        style:repeat="stretch">
      <style:background-image
        xlink:href="Pictures/gradient.png"
        xlink:type="simple"
        xlink:actuate="onLoad"
        style:repeat="stretch"/>
      (...)
      </style:page-layout-properties>
      
      <style:header-style/>
      <style:footer-style/>
    </style:page-layout>
```
    
and include `master-style` in main.style-template
```
<office:master-styles>
  {% include 'iseq-styles/master.styles-template.jinja' %}
</office:master-styles>
```

also to main.style-template add the line  `draw:fill-image` as below
```
<office:styles>
  {% include 'iseq-styles/paragraph.style-template.jinja' %}
  <draw:fill-image draw:name="gradient" xlink:href="Pictures/gradient.png" xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad"/>
</office:styles>
```


## table background 

Create new table style - with  `<style:background-image...` in table style part
```
<style:style style:name="IseqTable" style:family="table">
  <style:table-properties table:border-model="collapsing">
  <style:background-image xlink:href="Pictures/gradient.png" xlink:type="simple" xlink:actuate="onLoad" style:repeat="stretch"/>
  </style:table-properties>
</style:style>
```


for Row/TableHeader/Cell style add  `<style:background-image/>` into row/cell properties
```
<style:style style:name="IseqTableRow" style:family="table-row">
  <style:table-row-properties fo:keep-together="always">
    <style:background-image/>
  </style:table-row-properties>
</style:style>
```
#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/.sh/.py/')
#SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/report-fq-qc

python3 $SCRIPT \
  --input-template "fq-qc-template.docx" \
  --input-sample-info-json "${RESOURCES_DIR}/data/json/282-sample-info.json" \
  --input-dict-json "dict-ang.json" \
  --input-qc-json "${RESOURCES_DIR}/data/json/report_1-quality-check.json" "${RESOURCES_DIR}/data/json/report_2-quality-check.json" \
  --input-qc-png "${RESOURCES_DIR}/data/other/report_1-qc.png" "${RESOURCES_DIR}/data/other/report_2-qc.png" \
  --input-name "ang-report-fq-qc" \
  --output-dir "/tmp/test/report-fq-qc"

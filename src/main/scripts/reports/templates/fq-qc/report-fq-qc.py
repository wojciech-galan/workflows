#!/usr/bin/python3
from docxtpl import DocxTemplate, RichText, InlineImage
from docx.shared import Pt
import json
import argparse
import re
import ntpath
import sys
import os

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils

__version__ = "0.1.0"


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates report fq qc from template and given json files')
    parser.add_argument('--input-template', '-t', metavar='input_template', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--input-sample-info-json', '-s', metavar='input_sample_info_json', type=str, required=True,
                        help='Json file containing sample info')
    parser.add_argument('--input-path-to-images', '-i', metavar='input_path_to_images', type=str, required=True,
                        help='Path to images to create report')
    parser.add_argument('--input-dict-json', '-d', metavar='input_dict_json', type=str, required=True,
                        help='Table headers and etc in selected language')
    parser.add_argument('--input-qc-jsons', '-j',  nargs="+", metavar='input_qc_jsons', type=str, required=True,
                        help='Json array containing QC information')
    parser.add_argument('--input-qc-pngs', '-p', nargs="+", metavar='input_qc_pngs', type=str, required=True,
                        help='QC charts array in .png format')
    parser.add_argument('--input-name', '-n', metavar='input_name', type=str, required=True,
                        help='Report name')
    parser.add_argument('--output-dir', '-l', metavar='output_dir', type=str, required=True,
                        help='Directory where goes the output')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()
    return args


def get_quality_scores_charts(qc_pngs):
    charts = dict()
    for qc_png in qc_pngs:
        filename = ntpath.basename(qc_png)
        chart_prefix = filename.split('-')[0]
        charts[chart_prefix] = InlineImage(doc, qc_png, width=Pt(500), height=Pt(400))
    return charts


def get_tables(qc_jsons, charts):
    tables = list()
    for qc_json in qc_jsons:
        filename = ntpath.basename(qc_json)
        cleared_filename = re.match(r"(.*q\.gz+)", filename)
        if cleared_filename:
            fq_qz_filename = str(cleared_filename.group())
        else:
            fq_qz_filename = filename
        with open(qc_json) as json_file:
            report = json.load(json_file)
            table_prefix = filename.split('-')[0]
            table = {f'filename': RichText(fq_qz_filename, color='000000', size='12pt', font='Muli'), 'report': report, 'quality_scores_chart': charts[table_prefix]}
            tables.append(table)
    return tables


def generate_final_json(template_json, tables, language_dict):
    template_json['language_dict'] = {
            'analysis_name': RichText(language_dict['analysis_name'], color='01275B', size='12pt', font='Muli',
                                      bold=True),
            'section_title': RichText(language_dict['section_title'], color='000000', size='12pt', font='Muli'),
            'table_measure': RichText(language_dict['table_measure'], color='000000', size='8pt', font='Muli',
                                      bold=True),
            'table_value': RichText(language_dict['table_value'], color='000000', size='8pt', font='Muli', bold=True),
            'table_warning': RichText(language_dict['table_warning'], color='000000', size='8pt', font='Muli',
                                      bold=True),
            'table_error': RichText(language_dict['table_error'], color='000000', size='8pt', font='Muli', bold=True),
            'table_comment': RichText(language_dict['table_comment'], color='000000', size='8pt', font='Muli',
                                      bold=True),
            'graph_description': RichText(language_dict['graph_description'], color='000000', size='8pt', font='Muli',
                                          italic=True)
        }
    template_json['tables'] = tables

    return template_json


if __name__ == "__main__":
    args = args_parser_init()
    doc = DocxTemplate(args.input_template)

    sample_info = template_utils.get_json_as_dict(args.input_sample_info_json)
    charts = get_quality_scores_charts(args.input_qc_pngs)
    tables = get_tables(args.input_qc_jsons, charts)
    language_dict = template_utils.get_json_as_dict(args.input_dict_json)

    basic_template_json = template_utils.get_basic_template_json(sample_info, doc, args.input_path_to_images)
    final_json = generate_final_json(basic_template_json, tables, language_dict)

    filename = ntpath.basename(args.input_template)

    doc.render(final_json)
    doc.save(f'{args.output_dir}/{args.input_name}.docx')

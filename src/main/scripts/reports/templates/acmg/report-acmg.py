#!/usr/bin/python3
from docxtpl import DocxTemplate, RichText, InlineImage
from docx.shared import Pt
import argparse
import sys
import os

REPORT_UTILS_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, REPORT_UTILS_DIR)

from reports_lib import template_utils


__version__ = "1.1.0"


def get_image(analysis_group: str, name: str, clinvar_or_acmg: str, width: int):
    name = name.replace("^", "_").lower()
    if name == "pathogenic/likely_pathogenic":
        name = "pathogenic"
    elif name == "benign/likely_benign":
        name = "likely_benign"
    path = f"/resources/images/{analysis_group}/{name}{clinvar_or_acmg}.png"
    return InlineImage(doc, path, width=Pt(width), height=Pt(14))


def prepare_var_for_report(vcf_dict: dict, variant: str):
    var_len = len(vcf_dict[variant]) if variant in vcf_dict else 0
    return RichText(var_len if var_len else "0", color='ffffff', size='36pt', font='Muli', bold=True)


def prepare_dict_for_report(jsons: list) -> dict:
    result = dict()
    for json_file in jsons:
        name = os.path.basename(json_file).split(".json")[0].replace("-", "_")
        try:
            result[name] = template_utils.prepare_data(template_utils.get_json_as_dict(json_file))
        except:
            result[name] = template_utils.get_json_as_dict(json_file)
    return result


def generate_final_json(template_json: dict, vcf_dict: dict, others_dict: dict, analysis_type:str, analysis_start: str,
                        analysis_group: str, content: dict, methods: dict) -> dict:

    for category, values in others_dict["images_width"].items():
        for name, width in values.items():
            template_json['images'].update({name: get_image(analysis_group, name, category, width)})

    # Number of variants
    if analysis_group == "germline":
        num_var = len(vcf_dict["pathogenic"]) + len(vcf_dict["likely_pathogenic"]) + len(vcf_dict["uncertain"]) + \
                  len(vcf_dict["likely_benign"]) + len(vcf_dict["benign"]) + len(vcf_dict["undefined"])
    else:
        num_var = len(vcf_dict["pathogenic"]) + len(vcf_dict["likely_pathogenic"]) + len(vcf_dict["others"])

    # Prepare genes panel
    genes_panel = template_utils.set_precision_and_split_genes(others_dict["panel"], 7) if "panel" in others_dict else []
    len_gene_panel = len(others_dict["panel"])-1 if "panel" in others_dict else 0
    panel_inputs = others_dict["panel_inputs"] if "panel_inputs" in others_dict else []
    genes_bed_target = others_dict["genes_bed_target"] if "genes_bed_target" in others_dict else []
    len_genes_bed_target = len(genes_bed_target)

    template_json.update({
        'igv_images': template_utils.get_igv_images(others_dict["picture_generate_report_sh"], doc),
        'igv_desc': content['igv_plot_description']['text'][0].format(
            igv_plot_description=content['igv_plot_description'][analysis_start]['panel_description']),
        'analysis_type': analysis_type,
        'analysis_start': analysis_start,
        'analysis_group': analysis_group,
        'scope_of_anylysis': "",
        'methods': methods,
        'content': content,
        'num_path': prepare_var_for_report(vcf_dict, "pathogenic"),
        'num_like': prepare_var_for_report(vcf_dict, "likely_pathogenic"),
        'num_unc': prepare_var_for_report(vcf_dict, "uncertain"),
        'num_var': RichText(num_var if num_var else "0", color='ffffff', size='36pt', font='Muli',
                            bold=True),
        'num_variants': num_var,
        'genes_panel': genes_panel,
        'panel_inputs': panel_inputs,
        'genes_bed_target': genes_bed_target,
        'num_genes': len_gene_panel,
        'num_genes_bed': len_genes_bed_target
    })
    template_json.update(vcf_dict)
    return template_json


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates clinical report from template and given json files')
    parser.add_argument('--template', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--vcf-jsons', type=str, required=True, nargs='+',
                        help='Json file containing variants')
    parser.add_argument('--other-jsons', type=str, required=True, nargs='+',
                        help='Other json files (etc. sample_info, content, genes_panel')
    parser.add_argument('--analysis-type', type=str, required=True,
                        help='Analysis type (genome, exome or target)')
    parser.add_argument('--analysis-start', type=str, required=True,
                        help='Analysis start (fastq or vcf)')
    parser.add_argument('--analysis-group', type=str, required=True,
                        help='Analysis group (etc. germline, carrier_screening)')
    parser.add_argument('--output-filename', type=str, required=True,
                        help='Output docx filename')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    doc = DocxTemplate(args.template)

    # Dict with jsons with pathogenicity from vcf
    vcf_dict = prepare_dict_for_report(args.vcf_jsons)
    
    # Dict with other jsons (etc. sample_info, content, genes_panel)
    others_dict = prepare_dict_for_report(args.other_jsons)
 
    # Prepare methods description
    methods = template_utils.prepare_methods(
        content=others_dict["content"],
        genome_or_exome=args.analysis_type,
        analysis_group=args.analysis_group
    )
    basic_template_json = template_utils.get_basic_template_json(
        sample_info=others_dict["sample_info"],
        doc=doc
    )

    final_json = generate_final_json(
        template_json=basic_template_json,
        vcf_dict=vcf_dict,
        others_dict=others_dict,
        analysis_type=args.analysis_type,
        analysis_start=args.analysis_start,
        analysis_group=args.analysis_group,
        content=others_dict["content"],
        methods=methods
    )

    doc.render(final_json)
    doc.save(f'{args.output_filename}')

{
  "name": {
    "germline": {
      "exome": "Whole exome sequencing analysis report",
      "genome": "Whole genome sequencing analysis report",
      "target": "Targeted sequencing analysis report"
    },
    "carrier_screening": {
      "exome": "Whole exome sequencing carrier screening analysis report",
      "genome": "Whole genome sequencing carrier screening analysis report"
    }
  },
  "limitations_of_liability": {
    "header": "The clinical diagnosis should never be based on the Patient’s genomic sequence analysis alone.",
    "body": "Factors such as errors due to sample contamination, rare events of technical errors, genetic events affecting the patient’s condition impossible to detect using currently available knowledge as well as other technical limitations should always be considered. This report should be one of many aspects used by the healthcare provider to help with the diagnosis and treatment plan, but it is not the diagnosis itself."
  },
  "data_protection": "According to Article 24(1) of the Law of 29 August 1997 on Personal Data Protection, the patient's Personal data will be used solely for the purpose of conducting the DNA analysis and will not be shared with any third party. Intelliseq sp. z o.o. declares that it applies all necessary measures to protect the patient's personal data, and in particular will refrain from sharing the data with any unauthorized party as well as prevent the loss of data or data corruption.",
  "limitations_of_the_method": "The employed method is based on the DNA isolated from blood samples and consequently might not reflect the changes in other parts of the body or detect mosaic mutations. Regions characterized with high homology to other parts of genome may not be represented accurately. Some parts of exome have limited coverage which impacts the variant detection accuracy. The ability to detect structural variants is limited. The method is largely based on utilizing the information from external databases, hence the ability to detect variants relevant to patient's condition is limited by the scope of information present in those databases.",
  "methods": {
    "fastq_quality_check": {
      "text": [
        "The quality of the raw reads was analyzed with the fastQC software."
      ]
    },
    "alignment_and_bam_files_processing": {
      "text": [
        "Reads were aligned to the human reference genome GRCh38 without alternative loci with the BWA-MEM software. Mapped reads were then examined for contamination. Reads with a  high fraction of mismatched bases and gap openings (above 0.1), as well as very short reads with soft-clipped bases at both ends, were removed. Duplicates were marked with GATK MarkDuplicates. The GATK BaseRecalibrator and ApplTyBQSR tools were used to adjust base qualities."
      ]
    },
    "variant_calling_and_spurious_variant_removing": {
      "text": [
        "Variants were called with GATK HaplotypeCaller (with the -ERC GVCF option). {variant_calling_and_spurious_variant_removing_intervals}",
        "Single sample genotyping was performed with GATK GenotypeGVCF (with default parameters). {variant_calling_and_spurious_variant_removing_filtering}",
        "If the recalibration model could not be successfully built for the analyzed data hard filtering was applied. SNPs were removed if any of the following conditions was true: QualByDepth (QD) < 2.0, FisherStrand (FS) > 60.0, RMSMappingQuality (MQ) < 40.0, variant quality (QUAL) < 30, MappingQualityRankSumTest (MQRankSum) < -12.5, ReadPosRankSum Test (ReadPosRankSum) < -8.0. Indels were removed if any of the following conditions were met: QD < 2.0, FS > 200.0, ReadPosRankSum < -20.0, StrandOddsRatio (SOR) > 10.0, QUAL < 30.0. For more information see: https://gatk.broadinstitute.org/hc/en-us/articles/360035890471-Hard-filtering-germline-short-variants. {variant_calling_and_spurious_variant_removing_hard_filtering}",
        "Mitochondrial variants were called and filtered separately with GATK Mutect2 and FilterMutectCalls programs run in the mitochondrial mode."
      ],
      "exome": {
        "intervals": "Intervals appropriate for a given kit (with 200bp padding) were used.",
        "filtering": "",
        "hard_filtering": "Resulting vcf was hard filtered.  SNPs were removed if any of the following conditions was true: QualByDepth (QD) < 2.0, FisherStrand (FS) > 60.0, RMSMappingQuality (MQ) < 40.0, variant quality (QUAL) < 30, MappingQualityRankSumTest (MQRankSum) < -12.5, ReadPosRankSum Test (ReadPosRankSum) < -8.0. Indels were removed if any of the following conditions were met: QD < 2.0, FS > 200.0, ReadPosRankSum < -20.0, StrandOddsRatio (SOR) > 10.0, QUAL < 30.0. For more information see: https://gatk.broadinstitute.org/hc/en-us/articles/360035890471-Hard-filtering-germline-short-variants."
      },
      "genome": {
        "intervals": "Broad Institute WGS calling intervals were used.",
        "filtering": "Resulting vcf was filtered with the VQSR procedure. Recalibration models for SNP were based on known sites from 1000Genomes, HapMap and dbSNP138 databases. INDEL models were based on Mills and 1000G gold standard, Axion Exome Plus and dbSNP138 datasets.",
        "hard_filtering": ""
      },
      "target": {
        "intervals": "User-defined intervals (with 1000bp padding) were used.",
        "filtering": "",
        "hard_filtering": "Resulting vcf was hard filtered. SNPs were removed if any of the following conditions was true: QualByDepth (QD) < 2.0, FisherStrand (FS) > 60.0, RMSMappingQuality (MQ) < 40.0, variant quality (QUAL) < 30, MappingQualityRankSumTest (MQRankSum) < -12.5, ReadPosRankSum Test (ReadPosRankSum) < -8.0. Indels were removed if any of the following conditions were met: QD < 2.0, FS > 200.0, ReadPosRankSum < -20.0, StrandOddsRatio (SOR) > 10.0, QUAL < 30.0. For more information see: https://gatk.broadinstitute.org/hc/en-us/articles/360035890471-Hard-filtering-germline-short-variants."
      }
    },
    "variant_annotation_and_annotation_based_filtering": {
      "germline": {
        "text": [
          "Information that can be used for variant pathogenicity evaluation was added to the vcf file. Databases used for annotation included: gnomAD v2.1 and v3 (frequencies, coverage, constraint), 1000Genomes (frequencies), MITOMAP (frequencies, contributed diseases), ClinVar (contributed diseases, pathogenicity), HPO (inheritance mode, contributed phenotypes and diseases), UCSC (repeats, PHAST conservation scores), SIFT4G (constraint), SnpEff (predicted impact on gene product), dbSNP (rsID), Ensembl (gene and transcript information), COSMIC (somatic mutations data). {variant_annotation_and_annotation_based_filtering}",
          "Annotated variants, for genes that are likely to contribute to the patient phenotype and/or for genes from the user-defined gene list/panels, were then classified according to the ACMG criteria (see Variants classification according to the ACMG  criteria for details) and prioritized. A maximum number of 50 variants are shown in the main report. These include all variants classified in the ClinVar database as pathogenic or likely pathogenic, along with variants that gained the highest pathogenicity score in the ACMG classification."
        ],
        "exome": {
          "filtering": "Common and low impact variants were then filtered out (with max frequency threshold of 0.05 and minimal SnpEff predicted impact on gene product set as MODERATE)."
        },
        "genome": {
          "filtering": "Common and low impact variants were then filtered out (with max frequency threshold of 0.05 and minimal SnpEff predicted impact on gene product set as MODERATE)."
        },
        "target": {
          "filtering": ""
        }
      },
      "carrier_screening": {
        "text": [
          "Information that can be used for variant pathogenicity evaluation was added to the vcf file. Databases used for annotation included: gnomAD v2.1 and v3 (frequencies, coverage, constraint), 1000Genomes (frequencies), MITOMAP (frequencies, contributed diseases), ClinVar (contributed diseases, pathogenicity), HPO (inheritance mode, contributed phenotypes and diseases), UCSC (repeats, PHAST conservation scores), SIFT4G (constraint), SnpEff (predicted impact on gene product), dbSNP (rsID), Ensembl (gene and transcript information), COSMIC (somatic mutations data). Variants of Pathogenic or Likely pathogenic clinical significance are selected (based on the ClinVar significance record). During the selection step, the clinical testing assertion criteria method is required.",
          "Annotated variants located within genes from all the Rare Diseases Genomics England panels, were then classified according to the ACMG criteria (see Variants classification according to the ACMG  criteria for details) and prioritized. A maximum number of 50 variants with the highest ACMG pathogenicity score are shown in the main report."
        ]
      }
    }
  },
  "igv_plot_description": {
    "text": [
      "{igv_plot_description} The coverage section presents a cumulative histogram. In alignment section sequencing reads are drawn as grey horizontal bars, with variant bases highlighted in color. Color intensity indicates the quality of the base call. The reference genome sequence is presented above alignment panels. Gene annotation track with marked exons (dark blue rectangles) and introns (dark blue line) is presented below the alignment panels."
    ],
    "fastq": {
      "panel_description": "The upper panel presents NGS reads as realigned by BWA. The bottom panel presents reads aligned by GATK HaplotypeCaller. Each panel is divided into coverage and alignment sections."
    },
    "vcf": {
      "panel_description": "The graph presents NGS reads provided by user in bam files."
    }
  }
}


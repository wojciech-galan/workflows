# Intelliseq reports

## Table of contents

- [General information](#general-information)
- [Content](#content)
- [Developing new template](#developing-new-template) 
- [Changing existing template](#changing-existing-template) 
- [Testing](#testing)
- [Version control](#version-control)
- [Docker image for report](#docker-image-for-report)
- [Template styles](#template-styles)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## General information

Documentation for new reports created in .docx templates using templating language jinja. 

## Content

Reports catalog contains following directories:

    - fonts stored in ```src/main/scripts/reports/font```
    - images stored in ```src/main/scripts/reports/images```
    - frequently used python functions in script located in ```src/main/scripts/reports/lib```
    - templates (described below) in ```src/main/scripts/reports/templates```

In templates catalog's name should be the report's name (for example fq-qc, acmg, bam-qc etc)
The content usually consists of:

    - template file with .docx extension and name's scheme: report-name-template.docx,
    for example fq-qc-template.docx, report-acmg-template.docx
    - dictionaries:
        -language file should contain ang/pl in name
        -file associated with analysis should contain exome/genome/target in name
    - python script converting jinja template to final .docx file 



## Developing new template

While designing content of new report you should provide a valid name (based on the convention above).
Create new catalog with the new report's name (for example acmg, fq-qc, panel).
To design a new .docx template you should firstly copy base .docx file from ```src/main/scripts/reports/templates/base/template.docx```.
It contains the same header, footer, and sample info section as in other reports.
The template file should always be saved as .docx.
Every jinja2 code injection should be performed in Muli font (installed in reports Dockerfile).

While developing a python script you need to import frequently used functions from template utils located in 
```src/main/scripts/reports/lib/template_utils.py```
When the python script works you need to develop wdl file based on the previous ones convention.
For example: ```src/main/wdl/tasks/report-fq-qc/report-fq-qc.wdl```
Remember about lauching special bash script formatting patient sample info at the beginning of you wdl script.
It can be found there: ```src/main/scripts/reports/templates/prepare-sample-info-json.sh```
Example of use in test: ```src/test/scripts/reports/templates/prepare-sample-info-json-test.sh```
Python script should be report-name.py (for example report-fq-qc.py, report-panel.py).

After developing all of above you need to close it in docker.
More information about this process is in [Docker image for report](#docker-image-for-report) section below.

## Changing existing template

If it's needed you can change the script and template.
During the changes you can mount report catalog to docker test to see performance of your actual .docx template and python script.
After performing update you must remember to push a newer docker.

## Testing

To test script creating report you should create report-name.sh file in following directory in templates test catalog ```/src/test/scripts/reports/templates```.
The tests are performed using docker image.
The test resources should be placed in this directory ```/src/test/resources/data/``` and mounted to the docker if needed.
Flag -v mounts resources to the docker allowing them to be used in it.

For example: 
```
#!/bin/bash

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"


mkdir -p /tmp/test/report-fq-qc
OUTPUT_FILE_PATH="/tmp/test/report-fq-qc/bash-test-fq-qc-report.docx"


docker run --rm -it -v /tmp/test/report-fq-qc:/outputs \
  -v $RESOURCES_DIR/data/json/282-sample-info.json:/resources/282-sample-info.json \
  -v $RESOURCES_DIR/data/json/report_1-quality-check.json:/resources/report_1-quality-check.json \
  -v $RESOURCES_DIR/data/json/report_2-quality-check.json:/resources/report_2-quality-check.json \
  -v $RESOURCES_DIR/data/other/report_1-qc.png:/resources/report_1-qc.png \
  -v $RESOURCES_DIR/data/other/report_2-qc.png:/resources/report_2-qc.png \
  -v $TEMPLATE_DIR/fq-qc:/intelliseqtools/reports/templates/testtools \
  intelliseqngs/report_fq-qc:1.0.2 \
  python3 /intelliseqtools/reports/templates/testtools/report-fq-qc.py \
        --input-template "/intelliseqtools/reports/templates/testtools/fq-qc-template.docx" \
        --input-sample-info-json "/resources/282-sample-info.json" \
        --input-path-to-images "/resources/images" \
        --input-dict-json "/intelliseqtools/reports/templates/testtools/dict-ang.json" \
        --input-qc-json "/resources/report_1-quality-check.json" "/resources/report_2-quality-check.json" \
        --input-qc-png "/resources/report_1-qc.png" "/resources/report_2-qc.png" \
        --input-name "bash-test-fq-qc-report" \
        --output-dir "/outputs"


soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH" \
        --outdir "/tmp/test/report-fq-qc"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH | grep -c 'Measure')

if [ -f "$OUTPUT_FILE_PATH" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "Measure phrase exists"; fi

   ```
If you wish to test script and template currently present in templates catalog just mount it to the docker
(it refers to example shown above, this line: ```-v $TEMPLATE_DIR/fq-qc:/intelliseqtools/reports/templates/testtools \```).
It is the directory located in docker.
If you wish to test script present in the docker rename testtools to script in the path above.
When you want to test .docx template present in the docker you can find it in resources
(rename path ```/intelliseqtools/reports/templates/testtools/fq-qc-template.docx``` to ```/resources/fq-qc-template.docx```)
This test is located here: ```src/test/scripts/reports/templates/fq-qc/report-fq-qc.sh```

## Version control

Versioning is based on wdl script and you should tag it after every change.

## Docker image for report

Dockerfiles are located in ```/src/main/docker/reports```.
Docker image name should look like ```report_name```.
The reports base docker contains basic catalogs created in it.
New docker images should be built using reports docker which can be found there ```src/main/docker/reports/reports/Dockerfile```.
For example: 
```
# name: report_fq-qc
# version: 1.0.2

FROM intelliseqngs/reports:4.0.3


ADD src/main/scripts/reports/templates/fq-qc/report-fq-qc.py /intelliseqtools/reports/templates/script/report-fq-qc.py

ADD src/main/scripts/reports/templates/fq-qc/dict-ang.json /resources/dict-ang.json
ADD src/main/scripts/reports/templates/fq-qc/dict-pl.json /resources/dict-pl.json
ADD src/main/scripts/reports/templates/fq-qc/fq-qc-template.docx /resources/fq-qc-template.docx
```

Example names: report_fq-qc, report_acmg, report_bam-qc.

## Template styles

The description above refers to the following reports:

    - acmg
    - bam-qc
    - fq-qc
    - panel
    - sex-check

If you are interested in generating old style reports you can see deprecated documentation in here
```https://gitlab.com/intelliseq/workflows/-/tree/report-acmg@2.0.5/src/main/scripts/reports```
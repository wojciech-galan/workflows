#!/usr/bin/python3
import os
import json
import jinja2
import argparse

__version__ = '1.0.0'

parser = argparse.ArgumentParser()
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument("-j", "--json", metavar="JSON", help='Comma delimited list of json files with names name1=file1,name2=file2')
parser.add_argument("-t", "--template", metavar='TEMPLATE', help='Template file')

args = parser.parse_args()
jsonFiles = args.json.split(',')
templateFile = args.template

templatesPath = [
    os.path.dirname(os.path.realpath(templateFile)),
    os.path.dirname(os.path.dirname(os.path.realpath(templateFile)))
]

jsonDict = {}
for jsonFile in jsonFiles:
    jsonName, jsonPath = jsonFile.split('=')
    with open(jsonPath, 'r') as jsonInput:
        jsonDict[jsonName] = json.load(jsonInput)

#print(templatesPath)

templateLoader = jinja2.FileSystemLoader(searchpath=templatesPath)
templateEnv = jinja2.Environment(loader=templateLoader)
templateFile = os.path.basename(templateFile)
template = templateEnv.get_template(templateFile)
outputText = template.render(jsonDict)
print(outputText)

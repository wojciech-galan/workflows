#!/bin/bash

GRAPHITE_BG=$'\e[48;2;46;52;64m'
GRAPHITE_FG=$'\e[38;2;46;52;64m'
DARKGREY_FG=$'\e[38;2;59;66;81m'
RED_FG=$'\e[38;2;189;98;107m'
GREEN_FG=$'\e[38;2;164;189;142m'
YELLOW_FG=$'\e[38;2;234;202;143m'
BLUE_FG=$'\e[38;2;130;162;192m'
PURPLE_FG=$'\e[38;2;179;143;172m'
CYAN_FG=$'\e[38;2;138;192;207m'
WHITE_FG=$'\e[38;2;229;233;240m'

RESET_COL=$'\x1B[0m'
COLOR_EOL=$'\x1B[K'

BOLD_ON=$'\x1b[1m'
BOLD_OFF=$'\x1b[22m'

log() {
  DATE=$(date '+%d/%m/%Y %H:%M:%S')
  printf "${DARKGREY_FG}[${BOLD_ON}$3$2${BOLD_OFF}${DARKGREY_FG}] ${BLUE_FG}$DATE "
  printf "${WHITE_FG}$1"
  printf "${RESET_COL}"
  printf "\n"
}

debug() { log $1 DEBUG ${WHITE_FG}; }
info() { log $1 INFO ${WHITE_FG}; }
warn() { log $1 WARN ${YELLOW_FG}; }
error() { log $1 ERROR ${RED_FG}; }

debug what
info what
warn what
error what

#!/usr/bin/python3
import pysam
import sys
import argparse
import gzip
import zlib

__version__ = '1.0.1'

parser = argparse.ArgumentParser(description='summarizes acmg scores into one recommendation')
parser.add_argument('--input', '-i', type=str, required=True, help='input vcf or vcf.gz file; input can also be piped through stdin')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
args = parser.parse_args()

input = pysam.VariantFile(args.input, "r")
input.header.info.add("ISEQ_VARIANT_DESCRIPTION", ".", "String", "ACMG summary description")

output = pysam.VariantFile('-', 'w', header=input.header)

def get_info_by_key(record, key):
    return record.info[key]


def get_acmg_score_by_key(record, key):
    try:
        return float(get_info_by_key(record, "ISEQ_ACMG_" + key + "_SCORE"))
    except:
        return 0.0


def get_acmg_description_by_key(record, key):
    try:
        score = float(get_info_by_key(record, "ISEQ_ACMG_" + key + "_SCORE"))

        description = str(get_info_by_key(record, "ISEQ_ACMG_" + key + "_DESCRIPTION"))
        description = description.rstrip('.') + "^(ACMG^" + key + "^score:^" + "{0:.2f}".format(score) + ").^"

        return description.replace("', '", ",")
    except:
        return "NO^DESCRIPTION^FOR^" + key + ".^"


for record in input.fetch():
    ba1 = get_acmg_description_by_key(record, "BA1")
    bp1 = get_acmg_description_by_key(record, "BP1")
    bp3 = get_acmg_description_by_key(record, "BP3")
    bp4 = get_acmg_description_by_key(record, "BP4")
    bp5 = get_acmg_description_by_key(record, "BP5")
    bp7 = get_acmg_description_by_key(record, "BP7")
    bs2 = get_acmg_description_by_key(record, "BS2")
    pm1 = get_acmg_description_by_key(record, "PM1")
    pm2 = get_acmg_description_by_key(record, "PM2")
    pm4 = get_acmg_description_by_key(record, "PM4")
    pm5 = get_acmg_description_by_key(record, "PM5")
    pp2 = get_acmg_description_by_key(record, "PP2")
    pp3 = get_acmg_description_by_key(record, "PP3")
    ps1 = get_acmg_description_by_key(record, "PS1")
    ps3 = get_acmg_description_by_key(record, "PS3")
    pvs1 = get_acmg_description_by_key(record, "PVS1")

    description = pvs1 + ps1 + ps3 + pm1 + pm2 + pm4 + pm5 + pp2 + pp3 + ba1 + bs2 + bp1 + bp3 + bp4 + bp5 + bp7
    record.info["ISEQ_VARIANT_DESCRIPTION"] = description.rstrip("^").replace(" ", "^")
    output.write(record)

#!/usr/bin/python3
import sys
import argparse

import wdldescribe
import wdlplot
import wdltest

VERSION="0.1"
AUTHOR="gitlab.com/marpiech"

if len(sys.argv) < 2 or sys.argv[1] not in ["describe", "plot", "test"]:
    print("USE:")
    print("wdltools.py TOOL_ID")
    print("  where TOOL_ID is:")
    print("    help - shows this message")
    print("    describe - generates markdown description of wdl based on metadata")
    exit(0)

if sys.argv[1] == "describe":
    parser = argparse.ArgumentParser(description='produces markdown from parsing meta header')
    parser.add_argument('--wdl', '-w', type=str, required=True, help='path to wdl')
    args = parser.parse_args(args = sys.argv[2:])
    wdldescribe.describe(args.wdl)
    exit(0)

if sys.argv[1] == "plot":
    parser = argparse.ArgumentParser(description='produces plot for wdl')
    parser.add_argument('--wdl', '-w', type=str, required=True, help='path to wdl')
    args = parser.parse_args(args = sys.argv[2:])
    wdlplot.plot(args.wdl)
    exit(0)

if sys.argv[1] == "test":
    parser = argparse.ArgumentParser(description='test wdl')
    parser.add_argument('--wdl', '-w', type=str, required=True, help='path to wdl')
    args = parser.parse_args(args = sys.argv[2:])
    wdltest.test(args.wdl)
    exit(0)

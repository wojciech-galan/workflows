#!/usr/bin/python3
import os
import re
import subprocess
import time

def test(wdlurl):
    print("Testing " + wdlurl)

    # try if wdl file exist
    try:
        absolutepath = os.path.abspath(wdlurl)
        if not os.path.exists(absolutepath):
            raise Exception()
    except:
        print("WDL file does not exits: " + wdlurl)
        exit(1)

    # try if test exist
    try:
        testpath = re.sub(r'main', 'test', os.path.dirname(absolutepath)) + "/test.sh"
        if not os.path.exists(testpath):
            raise Exception()
    except:
        print("Test file does not exits: " + testpath)
        exit(1)

    # try if test is executable
    try:
        if not (os.path.isfile(testpath) and os.access(testpath, os.X_OK)):
            raise Exception()
    except:
        print("Test file is not executable: " + testpath)
        exit(1)

    tic = time.perf_counter()
    try:
        result = subprocess.run([testpath, "--verbose"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=3600).stdout.decode('utf-8')
        toc = time.perf_counter()
        print("Test took " + "{0:.1f}".format(toc - tic) + "s to finish")
        for line in result.split('\n'):
            line = re.sub(r'\[[0-9;]*(m|K)', '', line)
            line = re.sub(r'\x1b', '', line)
            if "[ OK ]" in line:
                print(line)
            if "[ERROR]" in line:
                print(line)
    except:
        toc = time.perf_counter()
        print("Test failed in " + "{0:.1f}".format(toc - tic) + "s to finish")
    #print(result)
    #print(type(result))

        #print(line)
    #except:
    #    print("Failed to run test: " + testpath)

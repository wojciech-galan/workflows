#!/usr/bin/env bash
function version() {
  printf "bco-after-start.sh 1.2.3\n"
}

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -v|--version)
    version
    exit 0
    shift # past argument
    ;;
    -i|--task-name-with-index)
    export task_name_with_index="$2"
    shift # past argument
    shift # past value
    ;;
esac
done
>&2 echo -e "\n>>>>> stderr for bco after start script \n\n"
### docker size
docker_size=$(du -sh --exclude=/etc/ssl/private --exclude=/proc --exclude=/cromwell_root / | awk '{print $1}')
echo "$docker_size" > /docker_size
### start time
date +%s > /starttime
echo -e "\n>>>>> $task_name_with_index \n\n"
>&2 echo -e "\n>>>>> stderr for task $task_name_with_index \n\n"


import os
import json
import sys
import subprocess

def find_command_aptget():
    line_found = os.popen('zgrep "Commandline: apt-get install -y" /var/log/apt/history.log').read()
    command_list = line_found.split("\n")[:-1]
    commands = []
    for cnt, command in enumerate(command_list):
        command_list[cnt] = command[32:]
        for el in command_list[cnt].split():
            commands.append(el)
    return commands

def find_command_apt():
    line_found = os.popen('zgrep "Commandline: apt install -y" /var/log/apt/history.log').read()
    command_list = line_found.split("\n")[:-1]
    commands = []
    for cnt, command in enumerate(command_list):
        command_list[cnt] = command[28:]
        for el in command_list[cnt].split():
            commands.append(el)
    return commands

def conc_apts(aptget_var, apt_var):
    names = []
    if aptget_var and apt_var:
        names = aptget_var + apt_var
    if not apt_var:
        names = aptget_var
    if not aptget_var:
        names = apt_var
    return names

def get_versions(names):
    versions = []
    for name in names:
        apt_cache_str = 'apt-cache policy ' + str(name)
        apt_cache = os.popen(apt_cache_str).read()
        lines = apt_cache.split('\n')
        version = lines[4][5:-4]
        versions.append(version)

    return versions

def create_json(names, versions):
    apt_dict = {}
    for name, version in zip(names, versions):
        apt_dict[name] = version
    return apt_dict

def write_json(final_json, name):
    with open(name, 'w') as outfile:
        json.dump(final_json, outfile, indent=4)

def custom_tools_ver():
    try:
        line_found = os.popen('ls /opt/tools').read()
        tools_list = line_found.split('\n')[:-1]
        versions_dict = {}
        if tools_list:
            for tool in tools_list:
                if tool == "intelliseq":
                    continue
                name = tool.split('-')[0]
                version = tool.split('-')[1]
                versions_dict[name] = version
    except:
        versions_dict = {}
    return versions_dict


def tools_intelliseq_ver():
    try:
        line_found = os.popen('ls /opt/tools/intelliseq').read()
        tools_list = line_found.split('\n')[:-1]
        versions_intelliseq_dict = {}
        if tools_list:
            for tool in tools_list:
                extension = tool.split('.')[-1]
                if extension == "py":
                    path_script = '/opt/tools/intelliseq/' + tool
                    cmd = ['python3', path_script, '--version']
                    version_out_line = str(subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0])
                    version = version_out_line.split()[-1][:-3]
                # condition for other extensions
                # if extension == "sh":
                # if extension == "r"
                versions_intelliseq_dict[tool] = version
    except:
        versions_intelliseq_dict = {}
    return versions_intelliseq_dict


def resource_ver():
    try:
        resources = os.listdir("/resources")
        resource_dict = {}

        for resource in resources:
            version = os.listdir("/resources/" + str(resource))[0]
            resource_dict[resource] = version
    except:
        resource_dict = {}
    return resource_dict


def main():
    # aptget apt tools bco
    aptget_var = find_command_aptget()
    apt_var = find_command_apt()
    names = conc_apts(aptget_var, apt_var)
    versions = get_versions(names)
    apt_dict = create_json(names, versions)
    write_json(apt_dict, 'software_bco_apts.bco')

    # custom tools bco
    versions_dict = custom_tools_ver()
    write_json(versions_dict, 'software_bco_custom.bco')

    # intelliseq tools bco
    versions_intelliseq_dict = tools_intelliseq_ver()
    write_json(versions_intelliseq_dict, 'software_bco_intelliseq.bco')

    # resources bco

    resource_dict = resource_ver()
    write_json(resource_dict, 'datasource_bco_recources.bco')


if __name__ == '__main__': main()

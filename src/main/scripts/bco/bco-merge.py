#!/usr/bin/python3

import os, sys
import json
import argparse

__version__ = '1.0.1'
AUTHOR="gitlab.com/MateuszMarynowski"

parser = argparse.ArgumentParser(description='Merge bco')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-path', '-i', metavar='input_path', type=str, required=True, help='input path to bco files')
parser.add_argument('--module-name', '-mn', metavar='module_name', type=str, required=False, help='module name')
parser.add_argument('--module-version', '-mv', metavar='module_version', type=str, required=False, help='module version')
parser.add_argument('--pipeline-name', '-pn', metavar='pipeline_name', type=str, required=False, help='pipeline name')
parser.add_argument('--pipeline-version', '-pv', metavar='pipeline_version', type=str, required=False, help='pipeline version')
parser.add_argument('--output-file', '-o', metavar='output_file', type=str, required=True, help='output bco file')
args = parser.parse_args()

def main():

    main_path = os.getcwd()
    files = [f for f in os.listdir(args.input_path) if f.endswith('.json')]
    os.chdir(args.input_path)

    steps = []
    execution_domain = []
    parametric_domain = []
    keywords = []
    pipeline_steps = []

    if args.pipeline_name:
        for i in range(len(files)):
            with open(files[i], 'r') as json_file:
                bco = json.load(json_file)
            try:
                # provenance_domain
                steps += bco["provenance_domain"]["steps"]
                # execution_domain
                if type(bco["execution_domain"]) == dict:
                    execution_domain.append(bco["execution_domain"])
                else:
                    execution_domain += bco["execution_domain"]
                # parametric_domain
                parametric_domain += bco["parametric_domain"]
                # description_domain
                if "keywords" in bco["description_domain"]:
                    keywords += bco["description_domain"]["keywords"]
                pipeline_steps += bco["description_domain"]["pipeline_steps"]
            except TypeError:
                name = bco["provenance_domain"]["name"]
                print("Incorrect bco in {} file from module {}".format(files[i], name), file=sys.stderr)
            except KeyError:
                print("Empty {} file".format(files[i]), file=sys.stderr)
        provenance_domain = {"name": args.pipeline_name, "version": args.pipeline_version, "steps": steps}
        description_domain = {"keywords": list(set(keywords)), "pipeline_steps": pipeline_steps}
    else:
        for i in range(len(files)):
            with open(files[i], 'r') as json_file:
                bco = json.load(json_file)
            try:
                # provenance_domain
                bco["provenance_domain"]["steps"][0]["module_name"] = args.module_name
                steps += bco["provenance_domain"]["steps"]
                # execution_domain
                bco["execution_domain"]["module_name"] = args.module_name
                execution_domain.append(bco["execution_domain"])
                # parametric_domain
                for j in range(len(bco["parametric_domain"])):
                    bco["parametric_domain"][j]["module_name"] = args.module_name
                parametric_domain += bco["parametric_domain"]
                # description_domain
                bco["description_domain"]["pipeline_steps"][0]["task_info"]["module_name"] = args.module_name
                if "keywords" in bco["description_domain"]:
                    keywords += bco["description_domain"]["keywords"]
                pipeline_steps += bco["description_domain"]["pipeline_steps"]
            except TypeError:
                name = bco["provenance_domain"]["steps"][0]["name"]
                print("Incorrect bco in {} file from task {}".format(files[i], name), file=sys.stderr)
            except KeyError:
                print("Empty {} file".format(files[i]), file=sys.stderr)
        provenance_domain = {"name": args.module_name, "version": args.module_version, "steps": steps}
        description_domain = {"keywords": list(set(keywords)), "pipeline_steps": pipeline_steps}

    biocomputeobject = {}
    biocomputeobject["bco_spec_version"] = "https://w3id.org/biocompute/1.3.0/"
    biocomputeobject["bco_id"] = "https://intelliseq.com/flow/fdb3091e-5420-46b9-b1f6-e6e88e94bac1"
    biocomputeobject["provenance_domain"] = provenance_domain
    biocomputeobject["execution_domain"] = execution_domain
    biocomputeobject["parametric_domain"] = parametric_domain
    biocomputeobject["description_domain"] = description_domain
    os.chdir(main_path)

    with open(args.output_file, 'w') as json_file:
        json.dump(biocomputeobject, json_file, indent=3)

if __name__ == "__main__": main()

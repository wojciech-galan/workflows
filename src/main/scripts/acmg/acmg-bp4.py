#!/usr/bin/python3

import pysam
import argparse

__version__ = '1.0.2'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]
    except KeyError:
        return None


def get_sift_score(record):
    score = get_string_info_field_or_none(record, "ISEQ_SIFT4G_MIN")  # string/none
    # print(score)
    if score == None:
        description = " SIFT score is not provided for this genomic position."
        result = 0
    else:
        if float(score[0]) < 0.05:
            description = " Changes at this genomic position are predicted to be pathogenic by the SIFT algorithm (SIFT score: " + str(
                "{:.3f}".format(float(score[0]))) + ")."
            result = 0
        else:
            description = " Changes at this genomic position are predicted to be benign by the SIFT algorithm (SIFT score: " + str(
                "{:.3f}".format(float(score[0]))) + ")."
            result = 1
    return result, description


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP4_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BP4_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BP4 (Supporting evidence of benign impact): A positive score is given to variants with SIFT score higher than 0.05 (lack of conservation). Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SIFT4G - ISEQ_SIFT4G field'),
             ('Version', 'the same as in the ISEQ_SIFT4G field (SIFT4G)')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP4_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BP4_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BP4_SCORE"),
             ('Source', 'SIFT4G - ISEQ_SIFT4G field'),
             ('Version', 'the same as in the ISEQ_SIFT4G field (SIFT4G)')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        description = "This variant is localized at " + str(record.pos) + " position of the chromosome " + str(
            record.chrom.replace("chr", "")) + "."
        score, sift_description = get_sift_score(record)
        description += sift_description
        description = description.replace(" ", "^").replace(",", "*")

        record.info["ISEQ_ACMG_BP4_SCORE"] = score
        record.info["ISEQ_ACMG_BP4_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG BP4')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

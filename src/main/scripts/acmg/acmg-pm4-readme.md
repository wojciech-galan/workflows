### **This file describes how acmg-pm4.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PM4 criteria:   
*"Protein length changes as a result of in-frame deletions/insertions in a nonrepeat region or stop-loss variants"*   

The results are given as score (PM4 SCORE described below) and text descriptions.   

To achieve the above goal acmg-pm4.py does the following:   
It checks if given variant leads to one of the following changes (in protein coding transcripts, without errors/warnings added by SnpEff):     
* inframe deletion/insertion within nonrepeat region  
* disruptive inframe deletion/insertion within nonrepeat region  
* stop loss   
If so, PM4 SCORE = 1.0; otherwise PM4 SCORE = 0.0  
Nonrepeat region definition: not specified as simpleRepeat region (UCSC)  

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

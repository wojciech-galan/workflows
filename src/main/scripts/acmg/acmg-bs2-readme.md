### **This file describes how acmg-bs2.py should work**

The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BS2 criteria:  
*"BS2 Observed in a healthy adult individual for a recessive (homozygous), dominant (heterozygous), or X-linked (hemizygous) disorder, with full penetrance expected at an early age".*  

To achieve the above goal acmg-bs2.py does the following:  
For autosomal/X-linked variants  
It checks number of homozygous (alt/alt) individuals in gnomAD exomes database (ISEQ_GNOMAD_EXOMES_nhomalt field)     
* if this number is greater than 4 (4/~100 000 individuals): BS2 SCORE is 1, 
* if count of homozygotes is lower than or equal 4 BS2 SCORE is 0  

If count of homozygotes is not given for gnomAD exomes, scripts checks number 
of alt/alt homozygotes in gnomAD genomes v3 database (ISEQ_GNOMAD_GENOMES_V3_nohomalt):
* if this number is greater than 3 (3/~70 000 individuals): BS2 SCORE is 1, 
* if count of homozygotes is lower than or equal 3 or not given BS2 SCORE is 0 

   
For mitochondrial/Y-chromosome variants  
It checks alt allele count (MITOMAP_AC for mitochondrial, ISEQ_GNOMAD_EXOMES_AC or, if exomes data 
not present, ISEQ_GNOMAD_GENOMES_V3_AC for Y chromosome)   
* if allele count is greater than 2: BS2 SCORE = 1 (2/~50 000 individuals*)  
* if allele count is 0,1,2 or not provided: BS2 SCORE = 0   
  
*MITOMAP: data on about 50 000 mitochondrial genotypes; 1/2 of the gnomAD database (men only)          

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

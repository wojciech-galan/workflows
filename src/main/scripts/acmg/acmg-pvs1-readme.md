#### **This file describes how acmg-pvs1.py should work**

The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG_PVS1 criteria:   
*"PVS1 null variant (nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single or multiexon deletion) in a gene where LOF is a known mechanism of disease"*   

The results are given as score (total PVS1 SCORE described below) and text descriptions.
 
To achieve the above goal acmg-pvs1.py does the following:    
1. First it checks if given variant may lead to loss of protein function 
  (variant must lead to: start lost, stop gained, splice acceptor and donor sites changes, frameshifts),
2. then it checks whether LOF mutation affects at least one protein_coding transcript, 
  for which warnings/errors were not added during annotation, such transcripts are referred below as "reliable":
    * if not, it gives 0, and omits other tests   
    * if so:   
      **A)** confidence of LoF variant is checked 
      (based on VEP CSQ and [loftee](https://github.com/konradjk/loftee/blob/grch38/README.md) annotations). 
      Note, that only transcripts with the same id as the "reliable" ones are analyzed:
       * if at least one transcript passed all loftee filters (HC in LoF field): `variant score is 0.5`  
       * if all transcripts fail at least one filter (LC in LoF field): `variant score is 0.05`
       * if loftee LoF field is empty for all reliable transcripts and variant type is not start lost:
         `variant score is 0.01`
       * if loftee LoF field is empty for all reliable transcripts and variant type for all transcripts is start lost:
         `variant score is 0.2` (this is because loftee does not evaluate start lost variants, on the other side start lost mutation 
         may be rescued by any in-frame AUG codon)
         
      **B)** LoF tolerance is checked for the mutated genes (based on ClinVar and gnomAD data)  
       *  gnomAD: if upper confidence interval of the gnomad LOFOE score is lower than 0.35: `0.4 is added to the gene score`  
       [LOFOE score == calculated for a gene ratio of observed to expected (in neutrality) lof variants present in gnomAD data; 
          low LOFOE score obtained for a gene indicates that disruption of this gene is likely to be harmful, also in heterozygotes]
       * ClinVar:  if at least one pathogenic LoF variant is described in the ClinVar database: `0.025 is added to the gene score`; 
         if more than 2 pathogenic LoF variants are present in ClinVar: `0.025 is added to the gene score`;
         `fraction of pathogenic LoF variants to all LoF variants multiplied by 0.05` is also added to the gene score
    * final score is:
      * sum of scores obtained in **A)** and **B)**, if gene score is greater than 0 and variant score is greater than 0.05
      * half of the variant score, that is score from point **A)**, if gene score is 0
      * 0.1 times gene score + variant score, if variant score lower than or equal 0.05 (LoF predicted by SnpEff only or LC loftee flag)
         
 **Max of the final score is 1.0**   

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

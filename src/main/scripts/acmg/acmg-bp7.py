#!/usr/bin/python3

import pysam
import argparse
from typing import List

__version__ = '1.0.1'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def find_good_transcripts_syn(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing  modification of splice donor/acceptor site; 3) no errors/warnings present#
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                'synonymous' in transcript.variant_type() and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings()) and (
                'ERROR' not in transcript.transcript_warnings())):
            good_transcripts.append(transcript)
    return good_transcripts


def check_mutation_type(transcripts):
    severe_transcripts = find_good_transcripts_ps1(transcripts)
    syn_transcripts = find_good_transcripts_syn(transcripts)
    genes = set()
    if len(severe_transcripts) > 0:
        if len(syn_transcripts) == 0:
            description = "This is not a synonymous variant."
            score = 0
        if len(syn_transcripts) > 0:
            description = "This is a synonymous variant, but it may also lead to more severe changes in some transcripts."
            score = 0
    else:
        if len(syn_transcripts) == 0:
            description = "This is not a synonymous variant."
            score = 0
        if len(syn_transcripts) > 0:
            for transcript in syn_transcripts:
                genes.add(transcript.transcript_gene())
            description = "This is a synonymous variant of the " + ", ".join(
                genes) + " gene(S). It is not predicted to introduce any other, more severe protein level changes."
            score = 1
    return score, description


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BP7_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BP7 (Supporting evidence of benign impact): A positive score is given to synonymous variants. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff ANN field'),
             ('Version', 'SnpEff: the same as in the ANN field')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BP7_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BP7_SCORE"),
             ('Source', 'SnpEff ANN field'),
             ('Version', 'SnpEff: the same as in the SnpEff ANN field')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():

        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            score = 0
            description = "BP7^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"
            description = description.replace(" ", "^")
            record.info["ISEQ_ACMG_BP7_SCORE"] = float(score)
            record.info["ISEQ_ACMG_BP7_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        score, description = check_mutation_type(transcripts)
        description = description.replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_BP7_SCORE"] = float(score)
        record.info["ISEQ_ACMG_BP7_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='annotates vcf with ACMG BP7')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

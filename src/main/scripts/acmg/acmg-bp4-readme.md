### **This file describes how acmg-bp4.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BP4 criteria:   
*"BP4 Multiple lines of computational evidence suggest no impact on gene or gene product (conservation, evolutionary, splicing impact, etc.)"*  

To achieve this aim acmg-bp4.py does the following:   
For each variant (in fact for each ISEQ vcf line) it checks the SIFT score (ISEQ_SIFT4G_MIN) and gives: 
 * 1.0 if this score is greater than or equal 0.05   
 * 0.0 otherwise (lower SIFT score/SIFT score not present)  

The 0.05 threshold was chosen according to PMID: 11337480   
 
[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

### **This file describes how acmg-ps3.py should work**  

The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PS3 criteria:  
*"PS3 Well-established in vitro or in vivo functional studies supportive of a damaging effect on the gene or gene product"*  
The results are given as score (total PS3 SCORE described below) and text descriptions.  

To achieve the above goal acmg-ps3.py does the following:  
It checks if given ISEQ variant leads to a missense change (only protein coding transcripts without errors/warnings are considered).  
Then it compares if identical or similar change (at the protein level) is described in the Uniprot database (categories: MUTAGENESIS) as leading to reduction or hyperactivity of protein   
Uniprot mutations must affect only single  amino acid to be analyzed (start position == end position; and description without the *'when associated with'* phrase).     
  
To find identical/similar changes program compares: gene name, position in protein, reference amino acid and altered amino acid.  

The final PS3 SCORE is:  
* 1.0 for variants which cause amino acid substitution identical to mutation described in the Uniprot mutagenesis section as having negative effect on protein function  
* 0.5 for variants which cause amino acid substitution identical to mutation described in the Uniprot mutagenesis section as leading to protein hyperactivity  
* 0.25 for variants which cause amino acid substitution similar to mutation described in the Uniprot mutagenesis section as having negative effect on protein function  
* 0.125 for for variants which cause amino acid substitution similar to mutation described in the Uniprot mutagenesis section as leading to protein hyperactivity  
* 0.0 for variants without matching Uniprot records and records classified as having no effect, variants not leading to missense mutation   
   
Identical mutation: the same gene, position, reference amino acid and altered amino acid   
Similar mutation: the same gene, position and reference amino acid, but different altered amino acid    
Mutations with data from the functional studies are described in the *uniprot-mutagenesis-from-table.json* file  
Mutation effects in the Uniprot detabase are given only as a natural language description and are classified by the script   
[this classification is far from being perfect ;)]   

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)



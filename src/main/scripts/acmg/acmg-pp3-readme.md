### **This file describes how acmg-pp3.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PP3 criteria:   
*"PP3 Multiple lines of computational evidence support a deleterious effect on the gene or gene product (conservation, evolutionary, splicing impact, etc.)"*   

To achieve this aim acmg-pp3.py does the following:   
For each variant (in fact for each ISEQ vcf line) it checks the SIFT score (ISEQ_SIFT4G_MIN) and gives: 
* 1.0 if this score is less than 0.05   
* 0.0 otherwise (SIFT score bigger than or equal 0.05 or not present)   
  
The 0.05 threshold was chosen according to PMID: 11337480   
 
[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

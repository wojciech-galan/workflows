#!/usr/bin/python3

import pysam
import argparse
from typing import List

__version__ = '1.0.2'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]
    except KeyError:
        return None


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def check_if_in_nonrepeat_region(line):
    result = get_string_info_field_or_none(line, "ISEQ_SIMPLE_REPEAT")
    if result is None:
        score = 1
        description = " It is localised within nonrepaeat region."
    else:
        score = 0
        description = " It is localised within repeat region."
    return score, description


def check_mutation_type(transcripts):
    transcripts = find_good_transcripts_ps1(transcripts)
    n_sl = 0
    n_not_sl = 0
    types = set()
    genes = set()
    for transcript in transcripts:
        gene = transcript.transcript_gene()
        if "stop_lost" in transcript.variant_type():
            n_sl += 1
            types.add("stop lost")
        else:
            if "disruptive_inframe_insertion" in transcript.variant_type():
                n_not_sl += 1
                types.add("disruptive inframe insertion")
                # mutation = transcript.fields[10][2:]
            if "disruptive_inframe_deletion" in transcript.variant_type():
                n_not_sl += 1
                types.add("disruptive inframe deletion")
            if (
                    "inframe_insertion" in transcript.variant_type() and "disruptive_inframe_deletion" not in transcript.variant_type()):
                n_not_sl += 1
                types.add("inframe insertion")
            if (
                    "inframe_deletion" in transcript.variant_type() and "disruptive_inframe_deletion" not in transcript.variant_type()):
                n_not_sl += 1
                types.add("inframe deletion")
    if n_sl > 0:
        sl_score = 1
        not_sl_score = 0
        genes.add(gene)
    elif n_not_sl > 0:
        sl_score = 0
        not_sl_score = 1
        genes.add(gene)
    elif (n_sl == 0) and (n_not_sl == 0):
        sl_score = 0
        not_sl_score = 0
        description = "This variant does not lead to in-frame length changes of any protein."

    if len(genes) > 1:
        description = "This variant leads to in-frame length changes (" + ", ".join(
            types) + ") of the following proteins:" + ", ".join(genes) + "."
    elif len(genes) == 1:
        description = "This variant leads to in-frame length change (" + ", ".join(types) + ") of the " + ''.join(
            genes) + " protein."
    return sl_score, not_sl_score, description


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PM4_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PM4 (Moderate evidence of pathogenicity): A positive score is given to variants causing 1) in-frame deletion/insertion in non repeat region or 2) stop loss. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, UCSC - ISEQ_SIMPLE_REPEAT field'),
             ('Version', 'The same as in the ANN field (SnpEff), the same as in the ISEQ_SIMPLE_REPEAT field (UCSC)')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PM4_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PM4_SCORE"),
             ('Source', 'SnpEff - ANN field, UCSC - ISEQ_SIMPLE_REPEAT field'),
             ('Version', 'The same as in the ANN field (SnpEff), the same as in the ISEQ_SIMPLE_REPEAT field (UCSC)')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():

        try:

            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            score = 0
            description = "PM4^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"

            record.info["ISEQ_ACMG_PM4_SCORE"] = float(score)
            record.info["ISEQ_ACMG_PM4_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        repeat_score, repeat_description = check_if_in_nonrepeat_region(record)
        sl_score, not_sl_score, mut_type_description = check_mutation_type(transcripts)
        if sl_score == 1:
            score = 1
            description = mut_type_description
        elif (sl_score == 0) and (not_sl_score == 0):
            score = 0
            description = mut_type_description
        elif (sl_score == 0) and (not_sl_score == 1):
            score = not_sl_score * repeat_score
            description = mut_type_description + repeat_description
        description = description.replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_PM4_SCORE"] = float(score)
        record.info["ISEQ_ACMG_PM4_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PM4')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()

    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

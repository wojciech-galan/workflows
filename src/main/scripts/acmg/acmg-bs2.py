#!/usr/bin/python3

import pysam
import argparse

__version__ = '1.0.1'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def get_numeric_info_field_or_none(self, field_name: str):
    try:

        element = self.info[field_name]
        if type(element) == tuple:
            element = element[0]
        if element is None:
            return None

        return float(element)
    except (ValueError, KeyError):
        return None


def check_mito_Y(chromosome):
    if ("chrY" in chromosome) or ("chrM" in chromosome):
        return 1
    else:
        return 0


def get_better(exomes, genomes):
    if exomes is None:
        return genomes, "genomes"
    else:
        return exomes, "exomes"


def get_hom_count_nuclear(variant, chr):  # not for Y
    score = 0
    description = ""

    if "chrX" in chr:
        text = "/hemizygotes"
    else:
        text = ""
    hom_count_exomes = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_EXOMES_nhomalt")  # float or none
    hom_count_genomes = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_GENOMES_V3_nhomalt")
    if (hom_count_exomes is None) and (hom_count_genomes is None):
        score = 0
        description = "Data about frequency of homozygotes" + text + " is not provided for this allele by the gnomAD database."
    else:
        hom_count, hom_base = get_better(hom_count_exomes, hom_count_genomes)
        if hom_base == "exomes":
            if hom_count > 4:
                score = 1
                description = "This variant is frequently met in homozygotes" + text + ". GnomAD " + hom_base + " database: " + str(
                    int(hom_count)) + " cases among about 100 thousand genotyped individuals."
            elif hom_count < 5:
                score = 0
                if hom_count == 0:
                    description = "This variant is not observed in homozygotes" + text + " in the gnomAD " + hom_base + " database."
                elif hom_count == 1:
                    description = "This variant is rarely observed in homozygotes" + text + ". GnomAD " + hom_base + " database: only one case among about 100 thousand genotyped individuals."
                else:
                    description = "This variant is rarely observed in homozygotes" + text + ". GnomAD " + hom_base + " database: only " + str(
                        int(hom_count)) + " cases among about 100 thousand genotyped individuals."
        if hom_base == "genomes":
            if hom_count > 3:
                score = 1
                description = "This variant is frequently met in homozygotes" + text + ". GnomAD " + hom_base + " database: " + str(
                    int(hom_count)) + " cases among about 70 thousand genotyped individuals."
            elif hom_count < 4:
                score = 0
                if hom_count == 0:
                    description = "This variant is not observed in homozygotes" + text + " in the gnomAD " + hom_base + " database."
                elif hom_count == 1:
                    description = "This variant is rarely observed in homozygotes" + text + ". GnomAD database: only one case among about 70 thousand genotyped individuals."
                else:
                    description = "This variant is rarely observed in homozygotes" + text + ". GnomAD " + hom_base + " database: only " + str(
                        int(hom_count)) + " cases among about 100 thousand genotyped individuals."

    return score, description


def get_alt_count(variant, chr):
    if "chrY" in chr:
        database = "GnomAD"
        type_chr = "Y-chromosome"
        end = "men."
        hom_count_exomes = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_EXOMES_AC")
        hom_count_genomes = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_GENOMES_V3_AC")
        if (hom_count_exomes is None) and (hom_count_genomes is None):
            hom_count = None
            type_gnomad = ""
        else:
            hom_count, type_gnomad = get_better(hom_count_exomes, hom_count_genomes)
    if "chrM" in chr:
        field = "MITOMAP_AC"
        database = "MITOMAP"
        type_chr = "mitochondrial"
        end = "individuals."
        hom_count = get_numeric_info_field_or_none(variant, field)  # float or none
        type_gnomad = ""

    if hom_count is None:
        score = 0
        description = database + " " + type_gnomad + " database does not provide information about this" + type_chr + " variant frequency."
    elif hom_count > 2:
        score = 1
        description = "This " + type_chr + " variant is relatively frequent. " + database + " " + type_gnomad + " database: " + str(
            int(hom_count)) + " occurrences among about 50 thousand genotyped " + end
    elif hom_count < 3:
        score = 0
        if hom_count == 0:
            description = "This " + type_chr + " variant is not observed in the " + database + " " + type_gnomad + " database."
        elif hom_count > 0:
            description = "This " + type_chr + " variant is rarely observed. " + database + " " + type_gnomad + "database: only " + str(
                int(hom_count)) + " occurrence(s) among about 50 thousand genotyped " + end
    return score, description


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BS2_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BS2 (Strong evidence of benign impact): A positive score is given to variants observed in homozygotes / hemizygotes (frequency above 0.00004). Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source',
              'gnomAD - ISEQ_GNOMAD_EXOMES_nhomalt, ISEQ_GNOMAD_EXOMES_AC and ISEQ_GNOMAD_GENOMES_V3_nhomalt fields, MITMOAP -  ISEQ_MITOMAP_AF field'),
             ('Version',
              'The same as in the ISEQ_GNOMAD_EXOMES_nhomalt field (gnomAD), the same as in the MITOMAP_AF field (MITOMAP)')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BS2_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BS2_SCORE"),
             ('Source',
              'gnomAD - ISEQ_GNOMAD_EXOMES_nhomalt, ISEQ_GNOMAD_EXOMES_AC ISEQ_GNOMAD_GENOMES_V3_nhomalt fields, MITMOAP -  ISEQ_MITOMAP_AF field'),
             ('Version',
              'The same as in the ISEQ_GNOMAD_EXOMES_nhomalt field (gnomAD), the same as in the MITOMAP_AF field (MITOMAP)')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        chr = record.chrom
        function_type = check_mito_Y(chr)

        if function_type == 1:
            score, description = get_alt_count(record, chr)
        else:
            score, description = get_hom_count_nuclear(record, chr)

        description = description.replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_BS2_SCORE"] = float(score)
        record.info["ISEQ_ACMG_BS2_DESCRIPTION"] = description

        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf ACMG BS2')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

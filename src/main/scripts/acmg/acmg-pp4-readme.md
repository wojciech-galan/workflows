### **This file describes how acmg-pp4.py should work**
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG PP4 criteria:   
*"PP4 Patient’s phenotype or family history is highly specific for a disease with a single genetic etiology"*   

To achieve this aim acmg-pp4.py checks `ISEQ_GENES_PANEL_SCORE`.  
This score reflects the gene to phenotype match and can be from 0 to 100%.
The final PP4 score is 0.01 times the maximal panel score (of values obtained by all genes affected by the variant).  
Note, that:  
1) Only panel scores above 25 are added to the input vcf (threshold in the panel generation step);
2) in WGS and WES pipeline panel scores are added only to genes for which the highest impact predicted by SnpEff 
   is MODERATE or worse (SnpEff anno task)  
   
 
[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)

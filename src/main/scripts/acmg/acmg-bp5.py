#!/usr/bin/python3

import pysam
import argparse
from typing import List

__version__ = '1.1.1'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]
    except KeyError:
        return None


def get_clinvar_id(line):
    allele_id = get_string_info_field_or_none(line, "ISEQ_CLINVAR_VARIATION_ID")  # list/none
    if allele_id == None:
        description = "."
    else:
        description = ", ClinVar variation ID: " + allele_id[0] + "."
    return description


def get_clinvar_sign(line, description_allele_id):
    sign = get_string_info_field_or_none(line, "ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE")  # list/none
    if sign == None:
        description = "This genetic variant is not described in the ClinVar database."
        score = 0
    else:
        if 'likely_benign' in sign[0].split(":"):
            description = "This variant is described in the ClinVar database as likely benign" + description_allele_id
            score = 0.75
        elif ('benign' in sign[0].split(":")) or ('benign/likely_benign' in sign[0].split(":")):
            description = "This variant is described in the ClinVar database as benign" + description_allele_id
            score = 1
        else:
            description = "This variant is decribed in the ClinVar database as pathogenic/likely pathogenic or of uncertain impact."
            score = 0
    return score, description


def main(vcf_name, vcf_writer_name):
    # Adding ISEQ_BP5_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BP5_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BP5 (Supporting evidence of benign impact): A positive score is given to variants described as benign or likely benign in ClinVar. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'ClinVar - ISEQ_CLINVAR_VARIATION_ID and ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE fields'),
             ('Version', 'The same as in the ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE field (ClinVar)')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP5_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BP5_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BP5_SCORE"),
             ('Source', 'ClinVar - ISEQ_CLINVAR_ALLELE_ID and ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE fields'),
             ('Version', 'The same as in the ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE field (ClinVar)')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        description_allele_id = get_clinvar_id(record)
        score, description = get_clinvar_sign(record, description_allele_id)
        description = description.replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_BP5_SCORE"] = float(score)
        record.info["ISEQ_ACMG_BP5_DESCRIPTION"] = description

        vcf_writer.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG BP5')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)


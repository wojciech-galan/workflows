#!/usr/bin/env python3

import pysam
import argparse
import json
from operator import itemgetter
from typing import List

__version__ = '2.0.1'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


def check_gnomad_lof(gene):
    description = "GnomAD lof constraint: "
    upper = None
    try:
        gnomad_lof[gene]
    except KeyError:
        description += "Ratio of observed to expected loss of function " \
                       "variants for the {} gene (oe_lof) is not provided.".format(gene)
    else:
        lofoe, upper = float(gnomad_lof[gene][0]), float(gnomad_lof[gene][2])
        if upper > lofoe:
            description += "Ratio of observed to expected loss of function variants " \
                           "for the {} gene (oe_lof) is {:.2f}, with upper confidence bound of {:.2f}".format(gene,
                                                                                                              lofoe,
                                                                                                              upper)
            if upper < 0.35:
                description += ". This indicates that null variants of this gene are likely" \
                               " to be deleterious, also in heterozygotes."
            else:
                description += ". This indicates that null variants of this gene are not likely" \
                               " to be deleterious, at least in heterozygotes."
        else:
            description += "Ratio of observed to expected loss of function variants" \
                           " for the gene (oe_lof) cannot be reliably estimated.".format(gene)
            upper = None
    return [upper, description]


def check_clinvar_lof(gene):
    score = 0.0
    description = "ClinVar record: "
    try:
        clinvar_lof_dict[gene]
    except KeyError:
        description += "Not present."
    else:
        n_patho, n_lof, n_patho_lof = clinvar_lof_dict[gene]  # [0],clinvar_lof_dict[gene][1],clinvar_lof_dict[gene][2]
        if n_patho == 0:
            description += "No pathogenic variants of the are known.".format(gene)
        if n_lof == 0:
            description += " Null variants of the are not known.".format(gene)
        elif (n_patho != 0) and (n_lof != 0):
            if n_patho_lof == 0:
                description += "The gene does not have any known pathogenic loss of function variants.".format(gene)
            if n_patho_lof == 1:  # f=float(n_patho_lof)/n_patho
                score += 0.025
                description += "The {} gene has one known pathogenic loss of function variant.".format(gene)
            elif n_patho_lof > 1:
                score += 0.025
                if n_patho_lof > 2:
                    score += 0.025
                description += "The {} gene has {} known pathogenic loss of function variants.".format(gene,
                                                                                                       n_patho_lof)
            f = float(n_patho_lof) / n_lof
            score += (f * 0.05)
            if f > 0.5:
                description += " The majority of null variants of the" \
                               " {} gene is also pathogenic (fraction: {:.2f}).".format(gene, f)
    return [score, description]


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]
    except KeyError:
        return None


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_id(self):
        return self.fields[6]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def parse_csq_header(header):
    try:
        csq_header_info = header.info["CSQ"]
        description = csq_header_info.record['Description']
        csq_format = description.split("Format:")[1].strip('"').split("|")
        #  print(csq_format)
        csq_dict = get_indexes(csq_format)
        if validate_csq_dict(csq_dict):
            return csq_dict
        else:
            return None
    except (KeyError, IndexError):
        return None


def validate_csq_dict(csq_dictionary):
    return "LoF" in csq_dictionary


def get_indexes(csq_fields_list):
    csq_dict = {}
    for i in csq_fields_list:
        csq_dict[i] = csq_fields_list.index(i)
    return csq_dict


class TranscriptCSQ(object):
    """represents description of one transcript extracted from the CSQ field"""

    def __init__(self, annotation_string: str, index_dictionary):
        super().__init__()
        self.fields = annotation_string.split('|')
        self.index_dict = index_dictionary

    def variant_type(self):
        return self.fields[self.index_dict["Consequence"]]

    def transcript_type(self):
        return self.fields[self.index_dict["BIOTYPE"]]

    def transcript_gene(self):
        return self.fields[self.index_dict["SYMBOL"]]

    def transcript_lof(self):
        return self.fields[self.index_dict["LoF"]]

    def transcript_lof_filter(self):
        return self.fields[self.index_dict["LoF_filter"]]

    def transcript_lof_flags(self):
        return self.fields[self.index_dict["LoF_flags"]]

    def transcript_lof_info(self):
        return self.fields[self.index_dict["LoF_info"]]

    def transcript_id(self):
        return self.fields[self.index_dict["Feature"]]


def check_vep_transcripts(good_transcripts, record, csq_dict):
    try:
        if csq_dict is not None:
            vep_transcripts = [TranscriptCSQ(csq, csq_dict) for csq in record.info["CSQ"]]
        else:
            vep_transcripts = []
    except KeyError:
        vep_transcripts = []

    good_vep_transcripts = pick_vep_transcripts(good_transcripts, vep_transcripts)

    return good_vep_transcripts


def get_genes(transcripts, hc):
    if hc:
        return set([i.transcript_gene() for i in transcripts if i.transcript_lof() == "HC"])
    else:
        return set([i.transcript_gene() for i in transcripts])


def flatten_list(list_of_lists):
    return [item for sublist in list_of_lists for item in sublist]


def get_failed_filters_or_flags(transcripts, filter_or_flag):
    if filter_or_flag == "filter":

        filters_or_flags = flatten_list(
            [i.transcript_lof_filter().split("&") for i in transcripts if i.transcript_lof() == "LC"])
    elif filter_or_flag == "flag":
        filters_or_flags = flatten_list(
            [i.transcript_lof_flags().split("&") for i in transcripts if i.transcript_lof() == "HC"])
    else:
        filters_or_flags = []
    descriptions = [loftee_filters[i] for i in set(filters_or_flags) if i in loftee_filters]
    return descriptions


def get_change_types(transcripts, hc):
    if hc:
        # print(transcripts[0].variant_type())
        conseq_list = [i.variant_type() for i in transcripts if i.transcript_lof() == "HC"]
    else:
        conseq_list = [i.variant_type() for i in transcripts]
    good_types = ["splice_acceptor_variant", "splice_donor_variant", "frameshift_variant", "stop_gained", "start_lost"]
    # print(conseq_list)
    parsed_list = set(
        [" and ".join(sorted([j.replace("_", " ") for j in i.split("&") if j in good_types])) for i in conseq_list])
    return parsed_list


def one_variant_test(line, transcripts, vep_dictionary):
    good_transcripts = find_good_transcripts_lof(transcripts)
    good_vep_transcripts = check_vep_transcripts(good_transcripts, line, vep_dictionary)
    score = 0.0
    if good_transcripts:
        score += check_loftee(good_vep_transcripts)
    description = ""
    genes = set()

    # gene_ids = set()
    if score == 0.0:  ## empty ann good lof transcript list
        description += "Not a loss of function variant"
    else:

        if score == 0.01:  ## empty vep good transcript list or all transcripts without LC,HC
            genes = [get_genes(good_transcripts, hc=False)]
            description += "Low confidence loss of function variant (predicted only by the SnpEff software):" \
                           " {}".format(", ".join(get_change_types(good_transcripts, hc=False)))
        elif score == 0.2:  ## start_lost only variant
            genes = [get_genes(good_transcripts, hc=False)]
            description += "Probable start lost variant"
        elif score == 0.05:  ## only LC
            genes = [get_genes(good_vep_transcripts, hc=False)]
            description += "Low confidence loss of function variant: {}. [Low confidence reason(s): {}]".format(
                ", ".join(
                    get_change_types(good_vep_transcripts, hc=False)), " ".join(
                    get_failed_filters_or_flags(good_vep_transcripts, filter_or_flag="filter")).strip("."))
        else:  ## at least one HC
            genes_HC = get_genes(good_vep_transcripts, hc=True)
            genes_all = get_genes(good_vep_transcripts, hc=False)
            genes = [genes_HC, genes_all - genes_HC]
            flags = get_failed_filters_or_flags(good_vep_transcripts, filter_or_flag="flag")
            description += "High confidence loss of function variant: " \
                           "{}. [Variant passed all Loss-Of-Function Transcript Effect Estimator (LOFTEE) filters]".format(
                ", ".join(get_change_types(good_vep_transcripts, hc=True)))
            if flags:
                description += " Note, that this variant has some warning(s): {}]".format(" ".join(flags).strip("."))
        description += "."

    return [score, genes, description]


def find_good_transcripts_lof(transcript_list):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing stop gain, start loss, frameshift,
     splice acceptor or splice donor site change, 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "start_lost" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or
                "frameshift" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or
                "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def pick_vep_transcripts(good_transcripts_ann, all_transcripts_vep):
    good_ids = [i.transcript_id().split(".")[0] for i in good_transcripts_ann]
    good_transcripts_vep = [i for i in all_transcripts_vep if i.transcript_id() in good_ids]
    return good_transcripts_vep


def check_loftee(good_csq_transcripts):
    loftee = [i.transcript_lof() for i in good_csq_transcripts]
    # print(loftee)
    start_lost_only = [i.variant_type() for i in good_csq_transcripts if "start_lost" in i.variant_type()]
    if "HC" in loftee:
        return 0.5
    elif "LC" in loftee:
        return 0.05
    elif len(loftee) == len(start_lost_only) and loftee:
        return 0.2
    else:
        return 0.01


def check_report_ann(record):
    try:
        return record.info["ISEQ_REPORT_ANN"][0].split("|")[6].split(".")[0]
    except KeyError:
        return None


def one_gene_test(gene):
    score = 0.0
    description = ''
    clinvar_score, clinvar_description = check_clinvar_lof(gene)  # [0],check_clinvar_lof(gene)[1]
    gnomad_lof_upper, gnomad_description = check_gnomad_lof(gene)  # [0],check_gnomad_lof(gene)[1]
    score += clinvar_score
    if score >= 0.06:
        description += "According to ClinVar database," \
                       " LOF mutations of the {} gene are known mechanism of disease. [{}].".format(
            gene, clinvar_description[:-1])
    else:
        description += "According to ClinVar database, the LOF mechanism of disease " \
                       "for the {} gene is not established. [{}].".format(gene, clinvar_description[:-1])
    if gnomad_lof_upper is None:
        description += "GnomAD based information about the null mutations of " \
                       "the {} gene are not sufficient to judge if they are likely to cause disease. [{}]".format(
            gene, gnomad_description[:-1])
    elif gnomad_lof_upper < 0.35:
        score += 0.4
        description += " According to gnomAD database, the LOF mutations of " \
                       "the {} gene are not tolerated. [{}].".format(gene, gnomad_description[:-1])
    else:
        score += 0
        description += " According to gnomAD database, LOF mutations of " \
                       "the {} gene are tolerated. [{}].".format(gene, gnomad_description[:-1])
    # description+=clinvar_description +" " + gnomad_description
    return [score, description]


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PVS1_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PVS1 (Very strong evidence of pathogenicity): A positive score is given to null variants "
              "(nonsense, frameshift, canonical +/-1 or 2 splice sites, initiation codon) "
              "in a gene where LOF is a known mechanism of disease. Full description of the "
              "ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN, VEP - CSQ, ClinVar, gnomAD Constraint'),
             ('Version', 'SnpEff: the same as in the ANN field, VEP: Ensembl102,  ClinVar: 09-12-2020, gnomAD: v2.1.1')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PVS1_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PVS1_SCORE"),
             ('Source', 'SnpEff - ANN, VEP - CSQ, ClinVar, gnomAD Constraint'),
             ('Version', 'SnpEff: the same as in the ANN field, VEP: Ensembl102,  ClinVar: 09-12-2020, gnomAD: v2.1.1')]
    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    csq_header_dict = parse_csq_header(header_vcf)
    for record in vcf_reader.fetch():
        final_score = 0.0
        final_description = ""

        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]
        except:
            final_description += "PVS1^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"

            # description_final = description_final.replace(" ", "^")

            record.info["ISEQ_ACMG_PVS1_SCORE"] = float(final_score)
            record.info["ISEQ_ACMG_PVS1_DESCRIPTION"] = final_description
            vcf_writer.write(record)
            continue

        ## Score from snpeff and loftee vep
        score, genes, description = one_variant_test(record, transcripts, csq_header_dict)
        # final_score += score
        final_description += description
        if score > 0.0:
            if len(genes[0]) > 1:
                final_description += " This variant disturbs the following genes: {}.".format(', '.join(genes[0]))
            elif len(genes[0]) == 1:
                final_description += " This variant disturbs the {} gene.".format(list(genes[0])[0])

            gene_results = [sorted([one_gene_test(i) for i in genes[j]], key=itemgetter(0), reverse=True) for j in
                            range(len(genes))]

            try:
                max_gene_score = gene_results[0][0][0]
                final_description += " {}".format(" ".join([i[1] for i in gene_results[0]]))
            except IndexError:
                max_gene_score = 0.0

            # very improbable case, when variant leads to HC lof
            # in not haploinsufficient gene ans LC lof in haploinsufficient gene
            if not bool(max_gene_score) and len(genes) == 2:
                try:
                    lc_max_gene_score = gene_results[1][0][0]
                    if bool(lc_max_gene_score):
                        final_description += " Note, however, that this variant is also low" \
                                             " confidence null variant of LoF intolerant gene/genes. " + " ".join(
                            [i[1] for i in gene_results[1]])
                except IndexError:
                    pass

            if (score == 0.01 and bool(max_gene_score)) or (score == 0.05 and bool(max_gene_score)):
                max_gene_score = 0.1 * max_gene_score

            if not bool(max_gene_score):
                score = 0.5 * score
                max_gene_score = 0.0

            final_score = score + max_gene_score  # ???? min = 0,; max = 1

        final_description = final_description.replace(" ", "^").replace(",", "*")
        record.info["ISEQ_ACMG_PVS1_SCORE"] = final_score
        record.info["ISEQ_ACMG_PVS1_DESCRIPTION"] = final_description
        vcf_writer.write(record)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='annotates vcf with tsv.gz')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='Input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='Output vcf.gz file')
    parser.add_argument('--gnomad-lof', '-g', metavar='gnomad_lof', type=str, required=True,
                        help='gnomAD constraints table')
    parser.add_argument('--clinvar-lof', '-c', metavar='clinvar_lof', type=str, required=True,
                        help='ClinVar lof dictionary')
    parser.add_argument('--loftee-filters', '-l', metavar='loftee_filters', type=str, required=True,
                        help='File with loftee filters and flags description')
    args = parser.parse_args()
    clinvar_lof_dict = read_json(args.clinvar_lof)
    gnomad_lof = read_json(args.gnomad_lof)
    loftee_filters = read_json(args.loftee_filters)
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

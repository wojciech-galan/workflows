#!/usr/bin/python3

import json
import pysam
import argparse

__version__ = '1.2.0'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def get_string_info_field_or_none(self, field_name: str):
    try:
        return self.info[field_name]
    except KeyError:
        return None


def check_population():
    try:
        population = "_" + args.population
        f = open(args.gnomad_pop)
        try:
            description = " (" + json.load(f)[population[1:]] + ")"
        except KeyError:
            population = ""
            description = ""
        f.close()
    except (IndexError, TypeError):
        population = ""
        description = ""
    return population, description


def check_mean_coverage(line):  # only for rare variants
    result = get_numeric_info_field_or_none(line, "ISEQ_GNOMAD_COV_MEAN")
    if result is not None:
        description = ", (mean coverage: " + format(result, '.2f') +")."
    else:
        description = ""
    return description


def check_fraction_of_samples_with_good_coverage(line):
    result = get_numeric_info_field_or_none(line, "ISEQ_GNOMAD_COV_OVER")
    if result is not None:
        if result >= 0.8:
            score = 1
            description = " It is also located in the genomic region having good coverage"
        else:
            score = 0.25
            description = " It is located in the genomic region having poor coverage"
    else:
        score = 0.25
        description = " It is located in the genomic region for which the coverage data is not provided."
    return score, description


def get_numeric_info_field_or_none(self, field_name: str):
    try:

        element = self.info[field_name]
        if type(element) == tuple:
            element = element[0]
        if element is None:
            return None

        return float(element)
    except (ValueError, KeyError):
        return None


def get_frequency_nuclear(variant, population):
    frequency_exomes = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_EXOMES_AF" + population)
    frequency_genomes = get_numeric_info_field_or_none(variant, "ISEQ_GNOMAD_GENOMES_V3_AF" + population)
    if (frequency_exomes is None) and (frequency_genomes is None):
        return (None, "")
    elif frequency_exomes is None:
        return (frequency_genomes, "genomes")
    else:
        return (frequency_exomes, "exomes")


def get_frequency_mito(variant):
    return get_numeric_info_field_or_none(variant, "MITOMAP_AF")


def frequency_test(frequency, database_type, additional_description):
    if frequency is None:
        description = "This variant is absent from the " + database_type + " database" + additional_description + "."
        score = None
    elif frequency > 0.0001:
        description = "This variant is present in the " + database_type + " database" + additional_description + ", and its frequency ({:.4f}".format(
            frequency) + ") is not extremaly low."
        score = 0
    else:
        description = "This variant is extremely rare in the " + database_type + " database" + additional_description + ", its frequency is {:.6f}".format(
            frequency) + "."
        score = 1
    return score, description


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PM2_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PM2 (Moderate evidence of pathogenicity): A positive score is given to very rare variants (not observed or with frequency lower than 0.0001). Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source',
              'gnomAD - ISEQ_GNOMAD_EXOMES_AF and ISEQ_GNOMAD_GENOMES_V3_AF fields, MITMOAP -  ISEQ_MITOMAP_AF field, gnomAD coverage - ISEQ_GNOMAD_COV_MEAN and ISEQ_GNOMAD_COV_OVER fields'),
             ('Version', 'gnomAD: v2 and v3')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PM2_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PM2_SCORE"),
             ('Source',
              'gnomAD - ISEQ_GNOMAD_EXOMES_AF and ISEQ_GNOMAD_GENOMES_V3_AF fields, MITMOAP -  ISEQ_MITOMAP_AF field, gnomAD coverage - ISEQ_GNOMAD_COV_MEAN and ISEQ_GNOMAD_COV_OVER fields'),
             ('Version', 'gnomAD: v2 and v3')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)
    population, additional_description = check_population()

    for record in vcf_reader.fetch():

        description = ""
        score = 0
        if record.chrom == "chrM":
            frequency = get_frequency_mito(record)
            score, description = frequency_test(frequency, "MITOMAP", additional_description)
            if score is None:
                score = 1
        else:
            frequency, database = get_frequency_nuclear(record, population)
            database_name = "gnomAD " + database
            score, description = frequency_test(frequency, database_name, additional_description)
            if score is None:
                result1 = check_fraction_of_samples_with_good_coverage(record)
                score = result1[0]
                description += result1[1]
            elif score == 1:
                result1 = check_fraction_of_samples_with_good_coverage(record)
                description += result1[1]
            if score >= 0.25:
                description += check_mean_coverage(record)

        description = description.replace(" ", "^").replace(",", "*").replace("^^", "^")
        record.info["ISEQ_ACMG_PM2_SCORE"] = float(score)
        record.info["ISEQ_ACMG_PM2_DESCRIPTION"] = description

        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PM2')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--population', '-p', type=str,
                        help='gnomAD population 3-letter code; possible values can be checked in gnomad-populations.json')
    parser.add_argument('--gnomad-pop', '-g', metavar='gnomad_pop', type=str, help='gnomAD populations json file')

    args = parser.parse_args()
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

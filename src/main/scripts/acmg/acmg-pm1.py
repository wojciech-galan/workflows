#!/usr/bin/python3

import pysam
import argparse
import json
from typing import List

__version__ = '1.0.1'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def divide_by_chromosomes(clinvar_pathogenic_sites):
    chr1, chr2, chr3, chr4, chr5, chr6, chr7, chr8, chr9, chr10, chr11, chr12, chr13, chr14, chr15, chr16, chr17, chr18, chr19, chr20, chr21, chr22, chrX, chrY, chrM, chr_strange = \
        [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []
    for item in clinvar_pathogenic_sites:
        if item[0] == "chr1":
            chr1.append(item)
        elif item[0] == "chr2":
            chr2.append(item)
        elif item[0] == "chr3":
            chr3.append(item)
        elif item[0] == "chr4":
            chr4.append(item)
        elif item[0] == "chr5":
            chr5.append(item)
        elif item[0] == "chr6":
            chr6.append(item)
        elif item[0] == "chr7":
            chr7.append(item)
        elif item[0] == "chr8":
            chr8.append(item)
        elif item[0] == "chr9":
            chr9.append(item)
        elif item[0] == "chr10":
            chr10.append(item)
        elif item[0] == "chr11":
            chr11.append(item)
        elif item[0] == "chr12":
            chr12.append(item)
        elif item[0] == "chr13":
            chr13.append(item)
        elif item[0] == "chr14":
            chr14.append(item)
        elif item[0] == "chr15":
            chr15.append(item)
        elif item[0] == "chr16":
            chr16.append(item)
        elif item[0] == "chr17":
            chr17.append(item)
        elif item[0] == "chr18":
            chr18.append(item)
        elif item[0] == "chr19":
            chr19.append(item)
        elif item[0] == "chr20":
            chr20.append(item)
        elif item[0] == "chr21":
            chr21.append(item)
        elif item[0] == "chr22":
            chr22.append(item)
        elif item[0] == "chrX":
            chrX.append(item)
        elif item[0] == "chrY":
            chrY.append(item)
        elif item[0] == "chrM":
            chrM.append(item)
        else:
            chr_strange.append(item)
    return chr1, chr2, chr3, chr4, chr5, chr6, chr7, chr8, chr9, chr10, chr11, chr12, chr13, chr14, chr15, chr16, chr17, chr18, chr19, chr20, chr21, chr22, chrX, chrY, chrM, chr_strange


chromosome_mapping = {"chr1": 0, "chr2": 1, "chr3": 2, "chr4": 3, "chr5": 4, "chr6": 5, "chr7": 6, "chr8": 7,
                      "chr9": 8, "chr10": 9, "chr11": 10, "chr12": 11, "chr13": 12, "chr14": 13, "chr15": 14,
                      "chr16": 15, "chr17": 16, "ch18": 17, "chr19": 18, "chr20": 19, "chr21": 20, "chr22": 21,
                      "chrX": 22, "chrY": 23, "chrM": 24}


def prepare_uniprot_dict(
        uniprot_functional_sites_dict):  # returns new dict: gene->id, start,ens, seq, type, desc. Only functional regions without benign variation
    prepared_uniprot_dict = {}
    for key in uniprot_functional_sites_dict.keys():
        new_items = []
        key_splitted = key.split("_")
        new_key, uniprot_id = "_".join(key_splitted[0:-1]), key_splitted[-1]  # changed
        changes = uniprot_functional_sites_dict[key]
        for item in changes:  ##changed kt 4 lines
            try:
                benign_variation_score = check_region_for_benign_variants(new_key, int(item[0].replace("~", "")),
                                                                          int(item[1].replace("~", "")))
            except ValueError:
                benign_variation_score = 0
            if benign_variation_score > 0:
                new_item = [uniprot_id] + item
                new_items.append(new_item)
        prepared_uniprot_dict[new_key] = new_items
    return prepared_uniprot_dict


def check_region_for_benign_variants(gene, start, stop):  # used by prepare_uniprot_dict
    try:
        clinvar_protein_changes_dict[gene]
    except KeyError:
        score = 1
    else:
        clinvar_protein_changes = clinvar_protein_changes_dict[gene]
        benign_region_variants = 0
        for item in clinvar_protein_changes:
            if item[2] != "":
                pos = int(item[2])
                sign = item[-1].split(":")  # new
                if ((pos >= start) and (pos <= stop)) and (
                        ("benign" in sign) or ("likely_benign" in sign) or ("benign/likely_benign" in sign)):
                    benign_region_variants += 1
        if benign_region_variants > 0:
            score = 0
        else:
            score = 1
    return score


def find_good_transcripts_interaction(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation affecting residue important for protein-protein interaction or structural conformation, 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "structural_interaction_variant" in transcript.variant_type() or "protein_protein_contact" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def check_interaction_sites(transcripts):  # protein interaction sites
    return find_good_transcripts_interaction(transcripts)


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


def find_aa_changes(transcripts):  # I removed stop lost, start lost, splice acceptor/donor changed kt
    good_transcripts = find_good_transcripts_ps1(transcripts)  # changed kt
    aa_changes_list = []
    for transcript in good_transcripts:
        variant_type = transcript.fields[1]
        aa_field = transcript.fields[10]
        aa_pos = transcript.fields[13].split('/')[0]  # changed kt
        gene = transcript.transcript_gene()
        transcript_id = transcript.fields[6]
        if "missense" in variant_type:
            aa_ref = aa_field[2:5]
            aa_alt = aa_field[-3:]
            vt = "This variant leads to the " + aa_ref + aa_pos + aa_alt + " substitution in the " + gene + " protein, "
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("stop_gained" in variant_type) and ("frameshift" not in variant_type):  # or (sl in variant_type):
            aa_ref = aa_field[2:5]
            aa_alt = '*'
            vt = "This variant introduces pre-mature stop codon. It truncates the " + gene + " protein at the " + aa_pos + " position, "
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "frameshift" in variant_type:
            aa_ref = aa_field[2:5]
            aa_alt = "fs"
            vt = "This variant leads to frameshift (" + aa_alt + "), which starts at the " + aa_pos + " amino acid position of the " + gene + " protein, "
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "disruptive_inframe_insertion" in variant_type:
            try:
                aa_field.split('ins')[1]
            except IndexError:
                aa_ref = aa_field.split('dup')[0][2:]
                aa_alt = 'dup'
                vt = "This variant leads to in-frame insertion of one or more additional amino acids in the " + aa_pos + " position of the " + gene + " protein. It also chamges amino acids surrounding the insertion site, "
                one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
                if one_aa_change not in aa_changes_list:
                    aa_changes_list.append(one_aa_change)
            else:
                aa_ref = aa_field.split('ins')[0][2:]
                aa_alt = aa_field.split('ins')[1]
                vt = "This variant leads to in-frame insertion of one or more additional amino acids in the " + aa_pos + " position of the " + gene + " protein. It also changes amino acids surrounding the insertion site, "
                one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
                if one_aa_change not in aa_changes_list:
                    aa_changes_list.append(one_aa_change)
        if ("inframe_insertion" in variant_type) and ("disruptive_inframe_insertion" not in variant_type):
            aa_ref = aa_field.split('dup')[0][2:]
            aa_alt = 'dup'
            vt = "This variant leads to insertion of one or more additional amino acids in the " + aa_pos + " position of the " + gene + " protein, "
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if ("inframe_deletion" in variant_type) and ("disruptive_inframe_deletion" not in variant_type):
            aa_ref = aa_field.split('del')[0][2:]
            aa_alt = 'del'
            vt = "This variant leads to deletion of one or more amino acids of the " + gene + " protein (at position " + aa_pos + "), "
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
        if "disruptive_inframe_deletion" in variant_type:

            aa_ref = aa_field.split('del')[0][2:]
            aa_alt = 'del'
            vt = "This variant deletes one or more amino acids of the " + gene + " protein (at position " + aa_pos + "). It also changes amino acids surrounding the deletion site, "
            one_aa_change = [gene, aa_ref, aa_alt, aa_pos, transcript_id, vt]
            if one_aa_change not in aa_changes_list:
                aa_changes_list.append(one_aa_change)
    return aa_changes_list


def pick_genes(aa_changes_list):
    try:
        genes = set([i[0] for i in aa_changes_list])
    except (IndexError, ValueError):
        genes = set()
    return genes


def find_changes_in_functinal_region(aa_changes_list, prepared_uniprot_functional_sites_dict,
                                     gene):  # filter: from aa_changes picks only those in functional regions, without benign variation in clinvar
    changes_in_functional_region = []
    try:
        prepared_uniprot_functional_sites_dict[gene]
    except KeyError:
        pass
    else:
        uniprot_list = prepared_uniprot_functional_sites_dict[gene]
        for mut in aa_changes_list:
            for up in uniprot_list:
                try:
                    mut_pos = mut[3]
                    mut_pos = int(mut_pos)
                    mut_ref = mut[1]

                    uniprot_start = up[1]
                    uniprot_start = int(uniprot_start)

                    uniprot_end = up[2]
                    uniprot_end = int(uniprot_end)

                    uniprot_ref = aa_dict[up[3][mut_pos - uniprot_start]]
                except (KeyError, IndexError, ValueError):
                    continue
                else:
                    if (mut[0] == gene) and ((mut_pos >= uniprot_start) and (mut_pos <= uniprot_end)) and (
                            mut_ref == uniprot_ref):
                        useful_uniprot_list = [up[0], up[4], up[5]]
                        change = mut[:] + useful_uniprot_list
                        changes_in_functional_region.append(change)
    return changes_in_functional_region  # to juz bede zmiany prowadzacw do zmian bialka w  funkcjonalnym regionie bez zmiennosci typu 'benign'


def check_if_in_hotspot(line, clinvar_pathogenic_sites_splitted,
                        chr_index):  # pomysl nad podzieleniem clinvar_pathogenic_sites_spliited na jeszcze mniejsze kawalki wewnatrz ktorych bedzie szukanie
    variant_chr = line.chrom
    variant_pos = int(line.pos)
    region_start = variant_pos - 15  # 31bp?
    region_end = variant_pos + 15
    region_patho_count = 0
    for item in clinvar_pathogenic_sites_splitted[chr_index]:
        if (item[0] == variant_chr) and (int(item[1]) <= region_end) and (int(item[1]) >= region_start):
            region_patho_count += 1
    return region_patho_count


def prepare_uniprot_description(functional_sites_changes):
    prepared_description = []
    for item in functional_sites_changes:
        text = item[0] + " " + item[-2].replace("ACT_SITE", "enzyme active site").replace("BINDING",
                                                                                          "binding site").replace(
            "CA_BIND", "calcium binding domain").replace("DNA_BIND", "DNA binding domain").replace("NP_BIND",
                                                                                                   "nucleotide binding domain").replace(
            "SIGNAL", "signal sequence").replace("ZN_FING", "zinc finger motif") + " (" + item[-1] + ")"
        text1 = text.replace("()", "")
        prepared_description.append(text1)
    return prepared_description


def main(vcf_name, vcf_writer_name):
    prepared_uniprot_functional_sites_dict = prepare_uniprot_dict(
        uniprot_functional_sites_dict)  # new dict: gene->id, start,ens, seq, type, desc. Only functional regions without benign variation
    hot_spots_list = divide_by_chromosomes(clinvar_pathogenic_sites)

    # Doddowanie ISEQ_PM1_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_PM1_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "PM1 (Moderate evidence of pathogenicity): A positive scores is given to variants 1) localized in functional protein region without benign variation; 2)  within mutational hot-spot 3) predicted to change protein residues important for structure or protein-protein interactions. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, Uniprot, ClinVar'),
             ('Version', 'The same as in the ANN field (SnpEff), UniProt: 09-12-2020, ClinVar: 09-12-2020')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_PM1_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_PM1_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_PM1_SCORE"),
             ('Source', 'SnpEff - ANN field, Uniprot, ClinVar'),
             ('Version', 'The same as in the ANN field (SnpEff), UniProt: 09-12-2020, ClinVar: 09-12-2020')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        try:

            transcripts = [Transcript(ann) for ann in record.info["ANN"]]

        except:
            score = 0
            description = "PM11^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"

            record.info["ISEQ_ACMG_PM1_SCORE"] = float(score)
            record.info["ISEQ_ACMG_PM1_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        # dla kazdego allelu alt
        # print("sfgsdfgsdfgs")
        aa_changes = find_aa_changes(transcripts)  # changed kt
        # print(aa_changes)
        if len(aa_changes) == 0:
            functional_sites_description = "This variant does not lead to amino acid substitition, deletion, insertion or premature stop codon."  # think about better description
            functional_sites_score = 0
        else:
            genes = pick_genes(aa_changes)
            functional_sites_changes = []
            functional_sites_score = 0
            interaction_residues = check_interaction_sites(transcripts)
            interaction_genes = set([i.transcript_gene() for i in interaction_residues])
            for gene in genes:
                # print(gene)
                functional_sites_changes += find_changes_in_functinal_region(aa_changes,
                                                                             prepared_uniprot_functional_sites_dict,
                                                                             gene)
            if len(functional_sites_changes) == 0:
                functional_sites_description = "This variant does not change any UniProt defined protein functional region, for which benign variation is not known."
                if len(interaction_residues) > 0:
                    functional_sites_description += " But, the altered residues may be critical for structural conformation of the " + ", ".join(
                        interaction_genes) + " gene(s), or may mediate protein-protein interactions."
                    functional_sites_score = 1
                else:
                    functional_sites_score = 0
            elif len(functional_sites_changes) > 0:
                transcripts_affected = set([i[4] for i in functional_sites_changes])
                description_set = set([i[5] for i in functional_sites_changes])
                uniprot_description_set = set(prepare_uniprot_description(functional_sites_changes))
                functional_sites_description = " ".join(description_set) + " (affected transcripts: " + ", ".join(
                    transcripts_affected) + "). Altered residues are located within protein functional region without known benign variation. UniProt record: " + ", ".join(
                    uniprot_description_set) + "."
                functional_sites_score = 1
                if len(interaction_residues) > 0:
                    functional_sites_description += " They may be also important for protein conformation or for its interaction with other proteins."
        try:
            chr_index = chromosome_mapping[record.chrom]
        except KeyError:
            chr_index = 25
        patho_count = check_if_in_hotspot(record, hot_spots_list, chr_index)
        if patho_count > 2:
            hot_spot_score = 1
            if functional_sites_score == 0:
                hot_spot_description = "But, it is located in the mutational hot spot, with " + str(
                    patho_count) + " known pathogenic mutations within the 31 surrounding bp."
            elif functional_sites_score > 0:
                hot_spot_description = "It is also located in the mutational hot spot, with " + str(
                    patho_count) + " known pathogenic mutations within the 31 surrounding bp."
        elif patho_count <= 2:
            hot_spot_score = 0
            if functional_sites_score == 0:
                hot_spot_description = "It is also located outside any known mutational hot spot."
            elif functional_sites_score > 0:
                hot_spot_description = "This variant is located outside any known mutational hot spot."
        description = functional_sites_description + " " + hot_spot_description
        score = max([hot_spot_score, functional_sites_score])

        description = description.replace(" ", "^").replace(";", "*").replace(",", "*").replace("..", ".").replace("^^",
                                                                                                                   "^")
        record.info["ISEQ_ACMG_PM1_SCORE"] = float(score)
        record.info["ISEQ_ACMG_PM1_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG PM1')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--uniprot-func', '-u', metavar='uniprot_func', type=str, required=True,
                        help='UniProt functional sites dictionary')
    parser.add_argument('--aa', '-a', type=str, required=True, help='Amino acids dictionary')
    parser.add_argument('--clinvar-changes', '-c', metavar='clinvar_changes', type=str, required=True,
                        help='ClinVar dictionary describing all protein level changes')
    parser.add_argument('--clinvar-patho', '-p', metavar='clinvar_patho', type=str, required=True,
                        help='File with positions of all pathogenic/likely_pathogenic variants')
    args = parser.parse_args()

    aa_dict = read_json(args.aa)
    uniprot_functional_sites_dict = read_json(args.uniprot_func)
    clinvar_protein_changes_dict = read_json(args.clinvar_changes)
    f_clinvar_patho = open(args.clinvar_patho)
    clinvar_pathogenic_sites = [i.strip().split("\t") for i in f_clinvar_patho.readlines()]
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

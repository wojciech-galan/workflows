#!/usr/bin/python3

import pysam
import json
import argparse
from typing import List

__version__ = '1.1.1'

AUTHOR = "gitlab.com/GlebLavr, gitlab.com/kattom"


def read_json(json_input):
    with open(json_input) as json_file:
        out_dict = json.load(json_file)
        return out_dict


def check_clinvar_ms(gene):  # for genes with pathogenic null variants only
    try:
        patho_ms = clinvar_ms_dict[gene][1]
        if patho_ms == 0:
            description = ", and pathogenic missense mutations for this gene are not known."
            score = 1
        elif patho_ms == 1:
            description = ". Nevertheless, missense mutations in the " + gene + " gene may also lead to disease, as one pathogenic missense variant of this gene is known."
            score = 0.25
        elif patho_ms > 1:
            description = ". Nevertheless, missense mutations in the " + gene + " gene may also lead to disease, as " + str(
                patho_ms) + " pathogenic missense variants of this gene is also known."
            score = 0.05
    except KeyError:
        score = 1
        description = " and missense variants of this gene are not known."
    return score, description


def check_clinvar_lof(gene):  # for all missense variants
    try:
        patho_lof = clinvar_lof_dict[gene][2]
        if patho_lof == 0:
            description = "Contribution of protein truncating variants to disease is not established for the " + gene + " gene, as its null pathogenic mutations are not described in the ClinVar database"
            score = 0.0
        elif patho_lof == 1:
            description = "Protein truncating mutations of the " + gene + " gene may lead to disease: ClinVar database describes one pathogenic null variant of this gene"
            score = 0.5
        elif patho_lof > 1:
            description = "Protein truncating mutations of the " + gene + " gene may lead to disease: ClinVar database describes " + str(
                patho_lof) + " pathogenic null variants of this gene"
            if patho_lof > 2:
                score = 1.0
            else:
                score = 0.5
    except KeyError:
        score = 0
        description = "Contribution of protein truncating variants to disease is not established for the " + gene + " gene, as its null mutations are not described in the ClinVar database"
    return score, description


def check_missense(transcripts):
    good_transcripts = find_good_transcripts_ps1(transcripts)
    n = 0
    genes = set()
    for transcript in good_transcripts:
        if 'missense' in transcript.variant_type():
            genes.add(transcript.transcript_gene())
            n += 1
    if n > 0:
        score = 1
        description = "This is a missense variant"
    else:
        score = 0
        description = "This is not a missense variant."
    return score, description, genes


def find_good_transcripts_ps1(transcript_list: List):
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) with mutation causing protein change, also splice acceptor or donor variants 3) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "missense" in transcript.variant_type() or "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or
                "start_lost" in transcript.variant_type() or "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or "splice_donor" in transcript.variant_type()) and (
                transcript.transcript_type() == "protein_coding") and (
                'WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings()):
            good_transcripts.append(transcript)
    return good_transcripts


class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def main(vcf_name, vcf_writer_name):
    # Doddowanie ISEQ_BP3_SCORE
    vcf_reader = pysam.VariantFile(filename=vcf_name)
    items = [('ID', "ISEQ_ACMG_BP1_SCORE"),
             ('Number', "1"),
             ('Type', "Float"),
             ('Description',
              "BP1 (Supporting evidence of benign impact): A positive score is given to missense variants in genes for which primarily truncating variants are known to cause disease. Full description of the ACMG criteria can be found in PMID: 25741868."),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 09-12-2020')]
    vcf_reader.header.add_meta("INFO", items=items)

    # Doddowanie ISEQ_BP3_DESCRIPTION
    items = [('ID', "ISEQ_ACMG_BP1_DESCRIPTION"),
             ('Number', "1"),
             ('Type', "String"),
             ('Description', "Short explanation for the ISEQ_BP1_SCORE"),
             ('Source', 'SnpEff - ANN field, ClinVar'),
             ('Version', 'SnpEff: the same as in the ANN field, ClinVar: 09-12-2020')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w', header=header_vcf)

    for record in vcf_reader.fetch():
        try:
            transcripts = [Transcript(ann) for ann in record.info["ANN"]]

        except:
            final_score = 0
            description = "BP1^VARIANT^WAS^NOT^ANNOTATED^WITH^SNPEFF"
            record.info["ISEQ_ACMG_BP1_SCORE"] = float(final_score)
            record.info["ISEQ_ACMG_BP1_DESCRIPTION"] = description
            vcf_writer.write(record)
            continue

        alt_score, alt_description, genes = check_missense(transcripts)
        if alt_score == 0:
            final_score = 0
            final_description = alt_description
        else:
            if len(genes) == 1:
                gene = list(genes)[0]
                gene_score_lof, gene_description_lof = check_clinvar_lof(gene)
                if gene_score_lof > 0:
                    gene_score_ms, gene_description_ms = check_clinvar_ms(gene)
                    final_gene_score = (gene_score_lof * gene_score_ms)
                    final_gene_description = "of the " + gene + " gene. " + gene_description_lof + gene_description_ms
                else:
                    final_gene_score = gene_score_lof
                    final_gene_description = "of the " + gene + " gene. " + gene_description_lof + "."  # tutaj jestem
            elif len(genes) > 1:
                final_gene_score = 0
                final_gene_description = "of the following genes: " + ", ".join(genes) + "."
                for gene in list(genes):
                    gene_score_lof, gene_description_lof = check_clinvar_lof(gene)
                    if gene_score_lof > 0:
                        gene_score_ms, gene_description_ms = check_clinvar_ms(gene)
                        final_gene_score = (gene_score_lof * gene_score_ms)
                        final_gene_description += " " + gene_description_lof + gene_description_ms
                    else:
                        final_gene_score = gene_score_lof
                        final_gene_description = " " + gene_description_lof + "."
            final_score = alt_score * (final_gene_score / len(genes))
            final_description = alt_description + " " + final_gene_description

        description = final_description.replace(" ", "^").replace(",", "*")

        record.info["ISEQ_ACMG_BP1_SCORE"] = float(final_score)
        record.info["ISEQ_ACMG_BP1_DESCRIPTION"] = description
        vcf_writer.write(record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Annotates vcf with ACMG BP1')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf.gz file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf.gz file')
    parser.add_argument('--clinvar-ms', '-m', metavar='clinvar_ms', type=str, required=True,
                        help='ClinVar dictionary with missense variant data')
    parser.add_argument('--clinvar-lof', '-l', metavar='clinvar_lof', type=str, required=True,
                        help='ClinVar dictionary with LOF variant data')
    args = parser.parse_args()

    clinvar_ms_dict = read_json(args.clinvar_ms)
    clinvar_lof_dict = read_json(args.clinvar_lof)
    in_vcf = args.input_vcf
    out_vcf = args.output_vcf
    main(in_vcf, out_vcf)

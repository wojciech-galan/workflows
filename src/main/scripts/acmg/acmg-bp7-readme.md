### **This file describes how acmg-bp7.py should work**  
The goal is to evaluate if variants from ISEQ annotated vcf files fulfil ACMG BP7 criteria:  
*"BP7 A synonymous (silent) variant for which splicing prediction algorithms predict no impact to the splice consensus sequence nor the creation of a new splice site AND the nucleotide is not highly conserved"*   

The results are given as score (BP7 SCORE described below) and text descriptions."

To achieve the above goal acmg-bp7.py does the following:
It checks 1) whether given ISEQ variant leads to synonymous changes (only transcripts without WARNINGS/ERRORS added by SnpEff are considered)   
and 2) whether it does not lead to any other more severe protein level change (also in transcripts without WARNINGS/ERRORS added by SnpEff are considered)  
* If so: BP7 SCORE is 1   
* If not: BP7 SCORE is 0   
   
"More severe changes":  
* stop loss/gain  
* splice acceptor/donor modification  
* frameshift  
* missense  
* start loss  
* inframe insertion/deletion  

It does not check conservation.    

[Return to main acmg readme](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/acmg/readme.md)


#!/usr/bin/env python3

__version__ = '1.0.0'

import argparse
import datetime
from typing import List

import pysam


class AnnotationInfo(object):

    def __init__(self, info_id: str, info_number: str, info_type: str, info_description: str, info_source=None,
                 info_version=None, source_field_id=None):
        super().__init__()
        self.info_id = info_id
        self.info_number = info_number
        self.info_type = info_type
        self.info_description = info_description
        self.info_source = info_source
        self.info_version = info_version
        self.source_field_id = source_field_id

    def get_header_info_line(self):
        info_source = '' if None else ',Source="{}"'.format(self.info_source)
        info_version = '' if None else ',Version="{}"'.format(self.info_version)

        return '##INFO=<ID={},Number={},Type={},Description="{}"{}{}>'.format(self.info_id, self.info_number,
                                                                              self.info_type,
                                                                              self.info_description, info_source,
                                                                              info_version)


class _Annotations(object):
    AnnotationsList: List[AnnotationInfo] = list()

    ISEQ_CLINVAR_ALLELE_ID = AnnotationInfo(
        info_id='',
        info_number='A',
        info_type='String',
        info_description='',
        info_source='',
        source_field_id='')
    AnnotationsList.append(ISEQ_CLINVAR_ALLELE_ID)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description="""
    Create VCF file containing several diffrenet NF2 gene variants""")

    parser.add_argument('PATH_TO_FASTA_REF_GZ', type=str, help='Path to GRCh38 bgzipped reference FASTA file')
    parser.add_argument('-v', '--version', action='version', version=__version__)
    args = parser.parse_args()

    ref_fa_gz_path = args.PATH_TO_FASTA_REF_GZ
    ref_fa_gz = pysam.FastaFile(ref_fa_gz_path)

    nf2_variants_vcf = pysam.VariantFile('-', 'w')
    # header
    nf2_variants_vcf.header.add_line("##fileDate=" + datetime.date.today().strftime("%d-%m-%Y"))
    nf2_variants_vcf.header.add_line("##reference=grch38")
    nf2_variants_vcf.header.add_line("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">")
    nf2_variants_vcf.header.add_line("##INFO=<ID=ISEQ_CLINVAR_VARIATION_ID,Number=A,Type=String,Description=\"ClinVar Variation ID of a variation consisted of the variant alone (Type of variation: Variant).\">")
    nf2_variants_vcf.header.add_line("##contig=<ID=chr22>")
    nf2_variants_vcf.header.add_sample('synthetic_sample')

    ## Add NF2 variants


    # "nsv4453783", <DEL>, het
    (dbvar_id, start, len) = ("nsv4453783", 29603988, 134)
    REF = ref_fa_gz.fetch("chr22", start, start + len)
    ALT = REF[0]
    new_record = nf2_variants_vcf.new_record(contig="chr22", start=start, stop=start + len, alleles=(REF, ALT),
                                             id=dbvar_id, qual=None, filter=None, info=None)
    new_record.samples['synthetic_sample']['GT'] = (0, 1)
    nf2_variants_vcf.write(new_record)

    # "rs1555978369", indel, het
    (rs_id, start) = ("rs1555978369", 29604055)
    (REF, ALT) = ("AAGA", "GT")
    new_record = nf2_variants_vcf.new_record(contig="chr22", start=start, stop=start, alleles=(REF, ALT),
                                             id=rs_id, qual=None, filter=None, info=None)
    new_record.info["ISEQ_CLINVAR_VARIATION_ID"] = "547703"
    new_record.samples['synthetic_sample']['GT'] = (0, 1)
    nf2_variants_vcf.write(new_record)

    # "", indel, homo
    (rs_id, start) = (".", 29655494)
    (REF, ALT) = ("TTTACTGTTTTGTAAAAATGATGCATAATTATAAAAGTGGCAAACAATACCAAATTTACTTCATGTGTAGGTTTTTTATTTTGCTCTATTTTTTGGTAGGTAATAAATCTGTATCAGATGACTCCGGAAATGTGGGAGGAGAGAA", "T")
    new_record = nf2_variants_vcf.new_record(contig="chr22", start=start, stop=start, alleles=(REF, ALT),
                                             id=rs_id, qual=None, filter=None, info=None)
    new_record.info["ISEQ_CLINVAR_VARIATION_ID"] = "654267"
    new_record.samples['synthetic_sample']['GT'] = (1, 1)
    nf2_variants_vcf.write(new_record)

    # "nsv3889739", <DUP>
    (dbvar_id, start, len) = ("nsv3889739", 29681432, 175)
    ALT = ref_fa_gz.fetch("chr22", start, start + len)
    REF = ALT[0]
    new_record = nf2_variants_vcf.new_record(contig="chr22", start=start, stop=start + len, alleles=(REF, ALT),
                                             id=dbvar_id, qual=None, filter=None, info=None)
    new_record.samples['synthetic_sample']['GT'] = (0, 1)
    nf2_variants_vcf.write(new_record)

    # "rs755969033", indel, het
    rs_id = "rs755969033"
    start = 29698410
    (REF, ALT) = ("GGCCCAA", "G")
    new_record = nf2_variants_vcf.new_record(contig="chr22", start=start, stop=start, alleles=(REF, ALT),
                                             id=rs_id , qual=None, filter=None, info=None)
    new_record.info["ISEQ_CLINVAR_VARIATION_ID"] = "341174"
    new_record.samples['synthetic_sample']['GT'] = (1, 1)
    nf2_variants_vcf.write(new_record)
    

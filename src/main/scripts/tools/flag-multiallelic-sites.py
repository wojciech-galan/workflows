#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL

signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
  Annotates VCF file with the following fields:
    +  MULTIALLELIC_SITE - Multiallelic site. Determined by counting ALT alleles observed in samples in input VCF
    +  ALTS - ALT allels observed on this site. Determined by counting ALT alleles observed in samples in input VCF
    +  ALTS_pos - Position of ALT alleles observed on this sites in input VCF
    +  ALTS_indices - Index of a corresponding ALT allele from ALTS field (1-based)

 Takes as an stdin VCF file normalized with command: bcftools norm -m +both.

""")

parser.add_argument('-v', '--version', action='version', version=__version__)

args = parser.parse_args()

header_lines = []
header_lines.append("##INFO=<ID=MULTIALLELIC,Number=0,Type=Flag,Description=\"Multiallelic site. Determined by counting ALT alleles observed in samples in input VCF.\">")
header_lines.append("##INFO=<ID=ALTS,Number=.,Type=String,Description=\"ALT allels observed on this site. Determined by counting ALT alleles observed in samples in input VCF.\">")
header_lines.append("##INFO=<ID=ALTS_pos,Number=1,Type=Integer,Description=\"Position of ALT alleles observed on this site. Determined by counting ALT alleles observed in samples in input VCF.\">")
header_lines.append("##INFO=<ID=ALTS_indices,Number=A,Type=String,Description=\"Index of a corresponding ALT allele from ALTS field (1-based).\">")

for raw_line in stdin:
    if raw_line.startswith("##"):
        print(raw_line.strip())
    elif raw_line.startswith("#C"):
        print("\n".join(header_lines))
        print(raw_line.strip())
    else:
        line = raw_line.split("\t", 8)
        ALT = line[4]
        POS = line[1]
        if len(ALT.split(",")) == 1:
             print(raw_line.strip())
        else:
            if line[7] == ".":
                line[7] = ";".join(["MULTIALLELIC", "ALTS=" + ALT, "ALTS_pos=" + POS, "ALTS_indices=" + ",".join([str(x + 1) for x in range(len(ALT.split(",")))])])
            else:
                line[7] = ";".join(["MULTIALLELIC", "ALTS=" + ALT, "ALTS_pos=" + POS, "ALTS_indices=" + ",".join([str(x + 1) for x in range(len(ALT.split(",")))])] + line[7].split(";"))
            print("\t".join(line).strip())

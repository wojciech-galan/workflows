#!/bin/bash
set -e

sample=$1

reference="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa"
agilent="/data/public/intelliseqngs/workflows/resources/intervals/agilent/"
v7_bait="${agilent}/sureselect-human-all-exon-v7/sureselect-human-all-exon-v7.bait.broad-institute-hg38.interval_list"
v7_intervals="${agilent}/sureselect-human-all-exon-v7/sureselect-human-all-exon-v7.target.broad-institute-hg38.interval_list"
v6_bait="${agilent}/sureselect-human-all-exon-v6-r2/sureselect-human-all-exon-v6-r2.bait.broad-institute-hg38.interval_list"
v6_intervals="${agilent}/sureselect-human-all-exon-v6-r2/sureselect-human-all-exon-v6-r2.target.broad-institute-hg38.interval_list"
wgs_interval="/data/public/intelliseqngs/workflows/resources/intervals/broad-institute-wgs-calling-regions/hg38.even.handcurated.20k.broad-institute-hg38.interval_list"

#some test samples:
sample_path="/data/ngs-projects/fichna/per-patient-info/shipment-38-wgs_460-498/run-analysis/results/$sample/$sample.markdup.recalibrated.bam"
#sample_path="/data/ngs-projects/jp2-wes/analysis-13-12-2019/results/$sample/${sample}_filtered.bam"
 
# FOR WES AND WGS #
gatk CollectAlignmentSummaryMetrics \
    -R $reference \
    -I $sample_path \
    -O "${sample}-CollectAlignmentSummaryMetrics.txt"

gatk MarkDuplicates \
    -R $reference \
    -I $sample_path \
    -O "to-remove-${sample}-MarkDuplicates.txt" \
    -M "${sample}-MarkDuplicates.txt"
rm "to-remove-${sample}-MarkDuplicates.txt"


if [ $2 = "wes" ]; then
    gatk CollectHsMetrics \
        --java-options "-Xms4g -Xmx40g" \
        -I $sample_path \
        -O "${sample}-CollectHsMetrics.txt" \
        -R $reference \
        -BI $v6_bait \
        -TI $v6_intervals
fi


if [ $2 = "wgs" ]; then
    gatk CollectWgsMetrics \
        -R $reference \
        -I $sample_path \
        -O "$(basename $sample)-CollectWgsMetrics.txt" \
        --INTERVALS $wgs_interval

    gatk CollectQualityYieldMetrics \
        -R $reference \
        -I $sample_path \
        -O "$(basename $sample)-CollectQualityYieldMetrics.txt"
fi


# za cholere nie pamietam po co dalam basename w nazwach outputow - moze bez sensu
# for sample in {460..465} {495..498}; do echo "bash gatk-bam-metrics.sh ${sample} wgs"; done

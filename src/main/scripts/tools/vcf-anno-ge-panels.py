#!/usr/bin/python3

import argparse
import pysam
import pandas as pd

__version__ = '1.0.0'
AUTHOR = "gitlab.com/MateuszMarynowski"

parser = argparse.ArgumentParser(description='Annotate vcf with panels names derived from Genomics England')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf file')
parser.add_argument('--input-tsv', '-t', metavar='input_tsv', type=str, required=True, help='input tsv file')
parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf file')
args = parser.parse_args()


def main():

    df = pd.read_csv(args.input_tsv, sep="\t", index_col=0)
    genes = df.index.tolist()

    vcf_reader = pysam.VariantFile(filename=args.input_vcf)
    vcf_reader.header.info.add("ISEQ_GE_PANELS", "1", "String",
                               "Panels from Genomics England that contain gene affected by the variant")

    vcf_writer = pysam.VariantFile(args.output_vcf, 'w',
                                   header=vcf_reader.header)

    for record in vcf_reader.fetch():
        try:
            if record.info["ISEQ_GENES_NAMES"][0] in genes:
                record.info["ISEQ_GE_PANELS"] = df.loc[record.info["ISEQ_GENES_NAMES"][0]]["panel"].replace(', ', '^')
            else:
                continue
        except:
            continue
        finally:
            vcf_writer.write(record)


if __name__ == "__main__": main()
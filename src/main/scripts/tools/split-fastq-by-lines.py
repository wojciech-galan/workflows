# !/usr/bin/env python3

VERSION = '1.0.0'
AUTHOR = 'gitlab.com/marysiaa'

import argparse
import gzip
import os

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""SPLITTING FASTQ BY FLOWCELL LINES

  .|||||||||.          .|||||||||.
 |||||||||||||        |||||||||||||
|||||||||||' .\      /. `|||||||||||
`||||||||||_,__o    o__,_||||||||||' """)

parser.add_argument('-v', '--version', action='version', version=VERSION)
parser.add_argument('FASTQ', type=str, help="path to input fastq file; should be in format id_1.fq.gz or id_2.fq.gz, if different use options -id and -n")
parser.add_argument('-id', type=str, nargs='?', help="sample ID or name")
parser.add_argument('-n', type=str, nargs='?', help="pair number")
parser.add_argument('-f', type=str, nargs='?', help="path to directory to save output (create if doesn't exist)")

args = parser.parse_args()

# file name and directory arguments
if args.id is None:
    args.id = args.FASTQ.split(".")[0].split("_")[0]
if args.n is None:
    args.n = args.FASTQ.split(".")[0].split("_")[1]

if args.f is None:
    args.f = ''
else:
    if not os.path.exists(args.f):
        os.makedirs(args.f)
    args.f += '/'


# splitting fastq by lines
with gzip.open(args.FASTQ, 'r') as fh:
    lines = []
    str = ''
    for line in fh:
        lines.append(line.rstrip().decode())
        str += line.decode()
        if len(lines) == 4:
            with open(args.f+args.id+'.'+lines[0].split(":")[2]+"_L"+lines[0].split(":")[3]+'_'+args.n+'.fq', 'a+') as f:
                f.write(str)
            lines = []
            str = ''

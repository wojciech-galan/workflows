import json
import argparse
import os
from typing import Dict, List, Set
from collections import defaultdict


__version__ = "0.0.1"


def args_parser_init():
    """Get user inputs"""
    parser = argparse.ArgumentParser(description='Validates meta.json')
    parser.add_argument('--input-meta-json', type=str, required=True,
                        help='Meta.json file to verify')
    parser.add_argument('--input-inputs-fields-json', type=str, required=True,
                        help='File containing types and obligatory fields for interface inputs meta')
    parser.add_argument('--input-outputs-fields-json', type=str, required=True,
                        help='File containing types and obligatory fields for interface outputs meta')
    parser.add_argument('--input-workflows-fields-json', type=str, required=True,
                        help='File containing types and obligatory fields for interface workflows meta')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()
    return args


def check_json_error(input_json: str) -> Dict:
    """Load json to program. If there's an error print details with traceback"""
    try:
        with open(input_json) as json_file:
            output_dict = json.load(json_file)
            print(f"{input_json} is correctly formatted .json file")
            return output_dict
    except ValueError as ex:
        print(f"{ex.with_traceback(ex.__traceback__)} in {input_json} file!")
        exit()


def add_dictlike_fields(value, value_parameter, fields, fields_list):
    if isinstance(value[value_parameter], dict):
        for subparameter in value[value_parameter].keys():
            subparameter_value = f"{value_parameter}.{subparameter}"
            subparameter_field = list(filter(lambda x: x['field_name'] == subparameter_value, fields_list))
            if subparameter_field:
                subparameter_field = subparameter_field[0]
                fields.append(subparameter_field)


def display_missed_type_message(meta_name, parameter_name, parameter_type, example):
    print(f"Parameter {meta_name} {parameter_name} is {parameter_type.__name__} type but should be "
          f"{type(example).__name__}. Example: {example}")


def fields_types_value_handling(key, value, value_parameter, fields):
    for field in fields:
        example = field['field_example']
        field_name = field['field_name']
        if field_name == "default" \
                and isinstance(example, list) \
                and ("multiselect" not in value["constraints"].keys() or not value["constraints"]['multiselect']):
            example = "str"
        value_parameter_type = type(value[value_parameter])
        if not isinstance(example, value_parameter_type) \
                and not isinstance(value[value_parameter], dict):
            display_missed_type_message(key, field_name, value_parameter_type, example)
        elif isinstance(value[value_parameter], dict) \
                and '.' in field_name:
            subparameter_key = field_name.split('.')[1]
            subparameter = value[value_parameter][subparameter_key]
            subparameter_type = type(subparameter)
            if not isinstance(example, subparameter_type):
                display_missed_type_message(key, field_name, subparameter_type, example)


def check_parameter_types(key: str, value: Dict, fields_list: List[Dict]):
    """Check if each parameter's value is same as defined in fields.json"""
    for value_parameter in value.keys():
        value_type = value["type"]

        fields = list(filter(lambda x: x['field_name'] == value_parameter
                                      and value_type in x['list_of_types_with_this_field'], fields_list))

        add_dictlike_fields(value, value_parameter, fields, fields_list)

        fields_types_value_handling(key, value, value_parameter, fields)


def display_missed_obligatory_parameter_message(name, value):
    print(f"{value} element is set to obligatory in fields.json and was not found in meta.json {name}")


def display_missed_possible_parameter_message(name, parameter):
    print(f"{parameter} is not defined in fields.json and was used in meta.json {name}")


def check_meta_obligatory_parameters(key: str, value: Dict, fields_list: List[Dict]):
    obligatory_parameters = list(filter(lambda x: value["type"] in x["list_of_types_with_this_field"]
                                                  and x["obligatory"], fields_list))

    obligatory_parameter_fields = [parameter['field_name'] for parameter in obligatory_parameters]

    for obligatory_parameter in obligatory_parameter_fields:
        if '.' in obligatory_parameter:
            over_parameter = obligatory_parameter.split('.')[0]
            sub_parameter = obligatory_parameter.split('.')[1]
            if over_parameter in value.keys():
                if sub_parameter not in value[over_parameter].keys():
                    display_missed_obligatory_parameter_message(key, obligatory_parameter)
            else:
                display_missed_obligatory_parameter_message(key, obligatory_parameter)
        else:
            if obligatory_parameter not in value.keys():
                display_missed_obligatory_parameter_message(key, obligatory_parameter)


def check_meta_parameters(key: str, value: Dict, fields_list: List[Dict]):
    """Check if meta parameters given in fields.json
    (general and variant_specific) are present in each value"""
    check_meta_obligatory_parameters(key, value, fields_list)
    check_if_possible_parameters(key, value, fields_list)
    check_parameter_types(key, value, fields_list)


def get_possible_parameters_dict(possible_parameters: Set) -> Dict:
    """Gets all parameters split by \'.\' and returns them as a dict"""
    possible_parameters_dict = defaultdict(list)

    for possible_parameter in possible_parameters.copy():
        if '.' in possible_parameter:
            over_parameter = possible_parameter.split('.')[0]
            sub_parameter = possible_parameter.split('.')[1]
            possible_parameters_dict[over_parameter].append(sub_parameter)
            possible_parameters.remove(possible_parameter)

    return possible_parameters_dict


def get_possible_parameters(fields_list: List[Dict]) -> set:
    return set(parameter['field_name'] for parameter in fields_list)


def check_if_possible_parameters(key, value, fields_list: List[Dict]):
    """Check if parameter is defined in fields.json"""
    parameters_to_check = list(value.keys())
    possible_parameters = get_possible_parameters(fields_list)
    # TODO remove when copy issue is resolved
    possible_parameters.add("copy")

    possible_parameters_dict = get_possible_parameters_dict(possible_parameters)

    for parameter in parameters_to_check:
        if isinstance(value[parameter], dict):
            if parameter not in possible_parameters_dict.keys():
                display_missed_possible_parameter_message(key, parameter)
            elif not all(value_parameter in possible_parameters_dict[parameter] for value_parameter in value[parameter].keys()):
                display_missed_possible_parameter_message(key, f"one or more parameters in {list(value[parameter].keys())}")
        elif parameter not in possible_parameters:
            display_missed_possible_parameter_message(key, parameter)


def get_meta_obligatory_parameters(fields_list: List[Dict]) -> List:
    return [parameter['field_name'] for parameter in fields_list if parameter["obligatory"]]


def check_meta_workflows_parameters(workflows_to_check_dict: Dict, fields_list: List[Dict]):
    """Check if meta workflows parameters given in fields.json
    (general and variant_specific) are present in each value"""
    obligatory_parameter_fields = get_meta_obligatory_parameters(fields_list)
    possible_parameters = get_possible_parameters(fields_list)

    for obligatory_parameter in obligatory_parameter_fields:
        if obligatory_parameter not in workflows_to_check_dict.keys():
            display_missed_obligatory_parameter_message("workflows", obligatory_parameter)

    for workflows_parameter in workflows_to_check_dict.keys():
        if workflows_parameter not in possible_parameters:
            display_missed_possible_parameter_message("workflows", workflows_parameter)

    check_meta_workflows_types(workflows_to_check_dict, fields_list)


def split_variants_parameters(workflows_to_check_dict: Dict) -> Dict:
    """Format from variant_parameter string format to dict"""
    variants_dict = defaultdict(list)

    for key in workflows_to_check_dict.keys():
        variant = key.split('_')[0]
        parameter = key.split('_')[1]
        variants_dict[variant].append(parameter)

    return variants_dict


def check_meta_workflows_variants_types(variant_key: str, variant_value: List, fields_list: List[Dict], workflows_to_check_dict: Dict):
    meta_name = f"workflows {variant_key}"
    for variant_parameter in variant_value:
        check_workflows_parameter_type(meta_name, variant_parameter, fields_list, workflows_to_check_dict[f"{variant_key}_{variant_parameter}"])


def check_workflows_parameter_type(meta_name: str, parameter_name: str, fields_list: List[Dict], parameter_value):
    field = list(filter(lambda x: x['field_name'] == parameter_name, fields_list))
    if field:
        field = field[0]
        example = field['field_example']
        parameter_type = type(parameter_value)

        if not isinstance(example, parameter_type):
            display_missed_type_message(meta_name, parameter_name, parameter_type, example)


def check_meta_workflows_types(workflows_to_check_dict: Dict, fields_list: List[Dict]):
    meta_name = "workflows"
    for workflows_parameter in workflows_to_check_dict.keys():
        check_workflows_parameter_type(meta_name, workflows_parameter, fields_list, workflows_to_check_dict[workflows_parameter])


def check_meta_workflows_variants_parameters(workflows_to_check_dict: Dict, fields_list: List[Dict]):
    """Check if meta workflows parameters given in fields.json
    (general and variant_specific) are present in each value"""
    obligatory_parameter_fields = get_meta_obligatory_parameters(fields_list)
    possible_parameters = get_possible_parameters(fields_list)
    variants_dict = split_variants_parameters(workflows_to_check_dict)

    for variant_key in variants_dict.keys():
        variant_value = variants_dict[variant_key]

        for obligatory_parameter in obligatory_parameter_fields:
            if obligatory_parameter not in variant_value:
                display_missed_obligatory_parameter_message(variant_key, obligatory_parameter)

        for variant_parameter in variant_value:
            if variant_parameter not in possible_parameters:
                display_missed_possible_parameter_message(variant_key, variant_parameter)

        check_meta_workflows_variants_types(variant_key, variant_value, fields_list, workflows_to_check_dict)


def check_if_hidden_are_required(key: str, value: Dict):
    """Check if both parameter hidden and required are true"""
    hidden_required = ["hidden", "required"]
    if all(parameter in value.keys() for parameter in hidden_required) \
            and value["hidden"] and value["required"]:
        print(f"{key}: {value} element is required but it's hidden!")


def check_if_max_bigger_than_min(key: str, value: Dict):
    """Check if min declared in constraints is bigger than max."""
    hidden_required = ["max", "min"]
    if "constraints" in value.keys() \
            and all(parameter in value['constraints'] for parameter in hidden_required) \
            and value['constraints']["max"] < value['constraints']["min"]:
        print(f"{key}: {value} element has max lower than min!")


def check_if_default_validity(key: str, value: Dict, fields_list: List[Dict]):
    """Check if \"default\" value type is the same as declared. Based on types_dict."""
    """The exception is when the type is Array[String] and multiselect is false or doesn't exist."""
    default_dict = list(filter(lambda x: x['field_name'] == "default"
                        and value['type'] in x['list_of_types_with_this_field'], fields_list))
    example = default_dict[0]['field_example']

    # exception Array[String] treated like String
    if "default" in value.keys()\
            and value['type'] == "Array[String]" \
            and not isinstance(value['default'], list) \
            and ("multiselect" not in value["constraints"].keys() or not value["constraints"]['multiselect']):
        example = "str"

    if "default" in value.keys() \
            and not isinstance((value["default"]), type(example)):
        print(f"{key}: {value} element's default value type is {type(value['default']).__name__}"
              f" but should be {type(example).__name__}")


def check_default_file_extension(key: str, value: Dict):
    """Check if extension in \"default\" is same as in declared"""
    if "extension" in value.keys() \
            and "default" in value.keys() \
            and not os.path.splitext(value['default'])[1] in value['extension']:
        print(f"{key}: {value} element's default file extension is not declared!")


def get_inputs_to_check(meta_dict: Dict) -> Dict:
    """Get all dict-type inputs instead of variants"""
    return dict(filter(lambda x: isinstance(x[1], dict)
                and "input" in x[0]
                and "variant" not in x[0], meta_dict.items()))


def get_workflows_dict(meta_dict):
    """Get workflows and workflows variants parameters"""
    return dict(filter(lambda x: "input" not in x[0]
                                 and "output" not in x[0], meta_dict.items()))


def get_workflows_to_check(workflows_dict: Dict) -> Dict:
    """Get workflows without variants parameters"""
    return dict(filter(lambda x: "variant" not in x[0], workflows_dict.items()))


def get_workflows_variants_to_check(workflows_dict: Dict) -> Dict:
    """Get workflows variants parameters"""
    return dict(filter(lambda x: "variant" in x[0], workflows_dict.items()))


def get_outputs_to_check(meta_dict: Dict) -> Dict:
    """Get all outputs parameters"""
    return dict(filter(lambda x: "output" in x[0]
                       and isinstance(x[1], dict), meta_dict.items()))


def check_inputs_validity(key: str, value: Dict, inputs_fields_json: Dict):
    """Perform all meta inputs tests"""
    check_meta_parameters(key, value, inputs_fields_json['general_fields'])
    check_if_hidden_are_required(key, value)
    check_if_max_bigger_than_min(key, value)
    check_default_file_extension(key, value)


def check_inputs_variants(key: str, meta_dict: Dict, value: Dict, inputs_fields_json: Dict):
    """Check if input variants' parameters are valid"""
    input_variants = list(filter(lambda x: "variant" in x and key in x, meta_dict.keys()))
    general_fields_list = inputs_fields_json['general_fields']
    variant_fields_list = inputs_fields_json['variant_specific_fields']
    fields_list = general_fields_list + variant_fields_list

    for input_variant in input_variants:
        input_variant_value = value.copy()
        meta_dict_variant = meta_dict[input_variant]

        for variant_key in meta_dict_variant.keys():
            input_variant_value[variant_key] = meta_dict_variant[variant_key]

        check_meta_parameters(input_variant, input_variant_value, fields_list)

        check_if_hidden_are_required(key, value)
        check_if_max_bigger_than_min(key, value)
        check_default_file_extension(key, value)
        check_if_default_validity(input_variant, input_variant_value, fields_list)


def check_meta_inputs(inputs_to_check_dict: Dict, inputs_fields_json: Dict, meta_dict: Dict):
    """Check inputs and inputs variants"""

    for key in inputs_to_check_dict.keys():
        try:
            value = inputs_to_check_dict[key]
            check_inputs_validity(key, value, inputs_fields_json)
            check_inputs_variants(key, meta_dict, value, inputs_fields_json)
        except Exception as ex:
            print(f"Unexpected error with {key}!")
            print(ex.with_traceback(ex.__traceback__))


def check_meta_workflows(workflows_to_check_dict: Dict, workflows_variants_to_check_dict: Dict, workflows_fields_json: Dict):
    """Check workflows and workflows variants"""
    try:
        general_fields_list = workflows_fields_json['general_fields']
        variant_fields_list = workflows_fields_json['variant_specific_fields']
        check_meta_workflows_parameters(workflows_to_check_dict, general_fields_list)
        check_meta_workflows_variants_parameters(workflows_variants_to_check_dict, variant_fields_list)
    except Exception as ex:
        print(f"Unexpected error with workflows parameters!")
        print(ex.with_traceback(ex.__traceback__))


def check_meta_outputs(outputs_to_check_dict: Dict, outputs_fields_json: Dict):
    """Check outputs"""
    for key in outputs_to_check_dict.keys():
        try:
            value = outputs_to_check_dict[key]
            check_meta_parameters(key, value, outputs_fields_json['general_fields'])
        except Exception as ex:
            print(f"Unexpected error with {key}!")
            print(ex.with_traceback(ex.__traceback__))


if __name__ == "__main__":
    args = args_parser_init()
    meta_dict = check_json_error(args.input_meta_json)
    inputs_fields_json = check_json_error(args.input_inputs_fields_json)
    outputs_fields_json = check_json_error(args.input_outputs_fields_json)
    workflows_fields_json = check_json_error(args.input_workflows_fields_json)

    inputs_to_check_dict = get_inputs_to_check(meta_dict)

    workflows_dict = get_workflows_dict(meta_dict)
    workflows_to_check_dict = get_workflows_to_check(workflows_dict)
    workflows_variants_to_check_dict = get_workflows_variants_to_check(workflows_dict)

    outputs_to_check_dict = get_outputs_to_check(meta_dict)

    check_meta_inputs(inputs_to_check_dict, inputs_fields_json, meta_dict)
    check_meta_workflows(workflows_to_check_dict, workflows_variants_to_check_dict, workflows_fields_json)
    check_meta_outputs(outputs_to_check_dict, outputs_fields_json)

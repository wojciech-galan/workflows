#!/usr/bin/env python3

import argparse
import pysam
from typing import Union  # do klasy

__version__ = '1.2.0'

from pysam import VariantFile, VariantRecord
from collections import defaultdict
import json
from pathlib import Path

def get_panel_genes(json_file=None):

    my_file = Path(json_file)
    
    if my_file.is_file():
        with open(json_file) as json_file:
            data = json.load(json_file)
        result = []
        for gene in data:
            result.append(gene["name"])
        return result
    else:
        pass

def get_variants(filename, genes=None):
    METHOD="ISEQ_CLINVAR_COLLECTION_METHOD"
    SIGNIFICANCE="ISEQ_CLINVAR_SIGNIFICANCE"
    #AGR_SIGNIFICANCE="ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"
    GENE="ISEQ_CLINVAR_GENE_INFO"
    OK_METHOD_SET = { 'clinical_testing', 'clinical_testing/curation', 'clinical_testing/provider_interpretation' }
    OK_SIGNIFICANCE_SET =  { 'likely_pathogenic', 'pathogenic', 'pathogenic/likely_pathogenic'}
    
    if genes != None:  
        genes = set(genes)
        
    vcf_in = VariantFile(filename, index_filename=filename + '.tbi')

    count_all = 0 
    
    for rec in vcf_in.fetch():
        count_all += 1 
        if SIGNIFICANCE in rec.info and METHOD in rec.info and GENE in rec.info and (genes == None or rec.info[GENE][0].split(':')[0] in genes):
            methods = list(rec.info[METHOD][0].split(":"))
            significances = list(rec.info[SIGNIFICANCE][0].split(":"))
            trials = list(zip(methods, significances))
            for method, significance in trials:
                if method in OK_METHOD_SET and significance in OK_SIGNIFICANCE_SET:
                    yield rec
                    break


def compute_genes(filename, gene_names=None):
    
    genes = defaultdict(lambda: { "sum": 0, "count": 0, "10x": 0})

    result = []

    for rec in get_variants(filename, gene_names):
        gene = (rec.info["ISEQ_CLINVAR_GENE_INFO"][0].split(":", maxsplit=1)[0])
        dp = 0
        if "DP" in list((rec.info)):
            dp = rec.info["DP"]
        variant_chance = ((2**dp - 1) / 2**dp)*100
        g = genes[gene]
        g["count"] += 1
        g["sum"] += variant_chance
              
    for G, V in genes.items():
        result.append({
            "gene": G,
            "variant_chance": round(V['sum'] / V['count'], 4),
            "count": V['count']
        })
        
    result = sorted(result, key = lambda i: i['gene']) 
    return result 

def merge_coverage_and_change(vcfpath, coverage_json=None, gene_names=None):
    # Merge computed genes with output json from task bam-panel-coverage.wdl
    
    computed_genes = compute_genes(vcfpath, gene_names)
    
    if coverage_json == None:
        return computed_genes

    my_file = Path(coverage_json)
    with open(coverage_json) as coverage_json:
        data = json.load(coverage_json)
        for gene in data:
            gene["gene"] = gene.pop("gene_name")
     
    return data + computed_genes
    
def merge_properties(properties):
    index = defaultdict(lambda: {})
    for prop in properties:
        for key in prop:
            if key != "gene":
                index[prop["gene"]][key] = prop[key]

    return sorted([{"gene": name, **keys} for name, keys in index.items()], key=lambda i: i['gene'])

def add_variant_count(merged: list) -> list:
    for gene in merged:
        if "count" not in gene.keys():
            gene["count"] = 0
            gene["variant_chance"] = "-"
        else:
            pass
    return merged

def main(vcfpath, coverage_json=None, panelpath=None):
    gene_names = None
    if panelpath != None:
        gene_names = get_panel_genes(panelpath)
    return add_variant_count( merge_properties(merge_coverage_and_change(vcfpath, coverage_json, gene_names)))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Creates coverage statistics for each gene in vcf file - detection chance')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-f', '--vcf', help="Vcf file")
    parser.add_argument('-o', '--output', help="name of the output file with .json extention")
    parser.add_argument('-p', '--panel', help="Panel with genes according to documentation: https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/wdl/modules/vcf-acmg-report/latest/readme.md")
    parser.add_argument('-c', '--coverage_json', help="Output json from task bam-panel-coverage.wdl")
   
    args = parser.parse_args()
    print(args.coverage_json)
    print("processing...")
    result = main(args.vcf, coverage_json=args.coverage_json, panelpath=args.panel)
    print("writing...")
    with open(args.output, 'w') as f:
        json.dump(result, f)
    

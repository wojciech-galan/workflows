#!/usr/bin/env python3

import argparse
import time
from pybiomart import Dataset
from requests.exceptions import HTTPError, ConnectionError
from operator import itemgetter

__version__ = "0.0.2"

host_address = {"102": "http://ensembl.org", "101": "http://aug2020.archive.ensembl.org/",
                "100": "http://apr2020.archive.ensembl.org",
                "99": "http://jan2020.archive.ensembl.org", "98": "http://sep2019.archive.ensembl.org/"}


def attempt_to_get_database_query(max_n_of_attempts: int, input_dataset: str, host: str):
    biomart_available = False
    num_of_attempts = 0
    while not biomart_available:
        try:
            return get_database_query(input_dataset, host)
            biomart_available = True
        except (HTTPError, ConnectionError):
            if num_of_attempts >= max_n_of_attempts:
                raise
            time.sleep(2 ** num_of_attempts)
        num_of_attempts += 1


def get_database_query(input_dataset: str, host: str):
    dataset = Dataset(name=input_dataset,
                      host=host)

    result_table = dataset.query(attributes=['external_gene_name', 'chromosome_name', 'start_position', 'end_position'],
                                 filters={
                                     'chromosome_name': ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
                                                         "13", "14", "15", "16", "17", "18", "19", "20", "21", "22",
                                                         "X", "Y", "MT"], "biotype": ["protein_coding"]})
    # print(dataset.filters)
    list_of_rows = result_table.values
    sorted_by_coord = sorted(list_of_rows, key=itemgetter(1, 2))  # sort on secondary key
    return sorted_by_coord


def analyse_row(one_row):
    return "\t".join(["chr" + str(one_row[1]).replace("MT", "M"), str(int(one_row[2]) - 1), str(one_row[3]),
                      one_row[0]]) + "\n"


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Gets Ensembl biomart data and converts it to bed')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-dataset', '-i', metavar='input_dataset', type=str, default='hsapiens_gene_ensembl',
                        help='Name of required Ensembl dataset; Default: %(default)s')
    parser.add_argument('--max_attempts', type=int, default=2,
                        help='Max number of attempts when retrieving data from biomart.')
    parser.add_argument('--ensembl-release', '-j', metavar='ensembl_release', type=str,
                        default="102",
                        help='Ensembl release, possible values: 98, 99, 100, 101, 102; Default: %(default)s')
    parser.add_argument('-o', '--output', type=str, required=True, help="Path to output bed")
    args = parser.parse_args()
    row_list = attempt_to_get_database_query(args.max_attempts, args.input_dataset, host_address[args.ensembl_release])
    with open(args.output, "w") as f:
        for row in row_list:
            f.write(analyse_row(row))

import gspread
import json
import argparse


__version__ = "0.0.1"


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates .bed file from cosmic database using pybiomart api')
    parser.add_argument('--input-workflow-name', type=str, required=True,
                        help='Workflow\'s name')
    parser.add_argument('--input-credentials-json', type=str, required=True,
                        help='Credentials.json containing data necessary to get access to google developer\'s privileges')
    parser.add_argument('--input-meta-json', type=str, required=True,
                        help='Meta.json file from selected workflow')
    parser.add_argument('--input-google-spreadsheet-key', type=str, required=True,
                        help='A key to access to the google spreadsheet')
    parser.add_argument('--output-meta-json', type=str, required=True,
                        help='An updated meta.json file')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()
    return args


def save_to_json(output_meta_json, meta_json):
    with open(output_meta_json, "w") as json_file:
        json.dump(meta_json, json_file, indent=3)


def get_json_content(input_meta_json):
    with open(input_meta_json) as json_file:
        return json.load(json_file)


def update_groups_parameters(record, meta_group):
    if record['group_description']:
        meta_group['description'] = record['group_description']
    if record['group_hReadableName']:
        meta_group['hReadableName'] = record['group_hReadableName']
    if record['group_rules']:
        meta_group['rules'] = record['group_rules']


def split_different_workflow_variants(record):
    raw_workflow_variant = record['workflow_variant'].split()
    workflow_variant_no_whitespace = "".join(raw_workflow_variant)
    return workflow_variant_no_whitespace.split(',')


def assign_records_to_groups(record, readonly_groupname):
    workflow_variant_list = split_different_workflow_variants(record)

    for workflow_variant_unformatted in workflow_variant_list:
        workflow_variant = workflow_variant_unformatted.split('_')
        readonly_name = "groups"
        readonly_name = update_readonly_name(readonly_name, workflow_variant)

        meta_group = meta_json[readonly_name][readonly_groupname]
        update_groups_parameters(record, meta_group)


def update_meta_groups(inputs, meta_json):
    for index, record in enumerate(inputs):
        readonly_groupname = record['readonly_groupname']
        hReadableName = record['group_hReadableName']
        if readonly_groupname and hReadableName:
            assign_records_to_groups(record, readonly_groupname)

    return meta_json


def update_inputs_parameters(meta_json, record, name):
    meta_input = meta_json[name]

    if record['name']:
        meta_input['name'] = record['name']
    if record['description']:
        meta_input['description'] = record['description']
    if record['default']:
        meta_input['default'] = record['default']
    if record['advanced']:
        meta_input['advanced'] = record['advanced']


def update_readonly_name(readonly_name, workflow_variant):
    if len(workflow_variant) > 1:
        variant_name = workflow_variant[1]
        readonly_name = f"{variant_name}_{readonly_name}"
    return readonly_name


def update_booleans(record):
    for key in record.keys():
        if record[key] == "true":
            record[key] = True
        elif record[key] == "false":
            record[key] = False


def update_meta_inputs(record, inputs, index):
    workflow_variant_list = split_different_workflow_variants(record)

    for workflow_variant_unformatted in workflow_variant_list:
        workflow_variant = workflow_variant_unformatted.split('_')
        readonly_name = f"input_{record['readonly_name']}"
        readonly_name = update_readonly_name(readonly_name, workflow_variant)

        if readonly_name in meta_json:
            update_inputs_parameters(meta_json, record, readonly_name)

        update_booleans(record)


def update_meta_inputs_and_groups(inputs, meta_json):
    inputs_len = len(inputs)
    meta_json = update_meta_groups(inputs, meta_json)

    for index in range(inputs_len):
        record = inputs[index]
        update_meta_inputs(record, inputs, index)

    return meta_json


def update_workflows_parameters(workflow, variant_name, meta_json):
    if workflow['name']:
        meta_json[f"{variant_name}name"] = workflow['name']
    if workflow['description']:
        meta_json[f"{variant_name}description"] = workflow['description']
    if workflow['price']:
        meta_json[f"{variant_name}price"] = str(workflow['price'])
    if workflow['tag']:
        meta_json[f"{variant_name}tag"] = workflow['tag']


def update_meta_workflows(workflows, meta_json):
    for workflow in workflows:
        workflow_variant = workflow['workflow_variant'].split('_')
        variant_name = ""

        if len(workflow_variant) > 1:
            variant_name = f"{workflow_variant[1]}_"

        update_workflows_parameters(workflow, variant_name, meta_json)
    return meta_json


def filter_by_workflow(unfiltered, workflow_name):
    filtered = list()
    for index, record in enumerate(unfiltered):

        if not record['workflow_variant']:
            record['workflow_variant'] = unfiltered[index-1]['workflow_variant']

        workflow_variant_list = split_different_workflow_variants(record)

        for workflow_variant in workflow_variant_list:
            workflow = workflow_variant.split('_')[0]
            if workflow == workflow_name:
                # print(f"first: {workflow}", f"second: {workflow_name}")
                # print(workflow_variant)
                record['workflow_variant'] = workflow_variant
                filtered.append(record)

    return filtered


def update_meta(sheets, meta_json, workflow_name):
    inputs_worksheet = sheets.worksheets()[0]
    inputs = inputs_worksheet.get_all_records()

    workflows_worksheet = sheets.worksheets()[1]
    workflows = workflows_worksheet.get_all_records()

    inputs = filter_by_workflow(inputs, workflow_name)
    workflows = filter_by_workflow(workflows, workflow_name)

    meta_json = update_meta_inputs_and_groups(inputs, meta_json)
    meta_json = update_meta_workflows(workflows, meta_json)

    return meta_json


if __name__ == '__main__':
    args = args_parser_init()
    gc = gspread.service_account(filename=args.input_credentials_json)
    sheets = gc.open_by_key(args.input_google_spreadsheet_key)

    meta_json = get_json_content(args.input_meta_json)
    meta_json = update_meta(sheets, meta_json, args.input_workflow_name)

    save_to_json(args.output_meta_json, meta_json)

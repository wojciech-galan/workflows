#!/usr/bin/python3
import sys
import pysam
import argparse
import gzip

VERSION="0.0.2"
AUTHOR="gitlab.com/marpiech"

if sys.argv[1] == "--version":
    print("annotate-vcf-with-tsv.py version " + VERSION)
    exit(0)

parser = argparse.ArgumentParser(description='annotates vcf with tsv.gz')
parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf or vcf.gz file')
parser.add_argument('--annotation-tsv', '-a', metavar='annotation_tsv', type=str, required=True, help='annotation tsv')
parser.add_argument('--annotation-column', '-c', metavar='annotation_column', type=str, required=True, action='append', help='Index (1-based) of column in tsv for annotation. Default: 3')
parser.add_argument('--annotation-name', '-n', metavar='annotation_name', type=str, required=True, action='append', help='Name of info field')
parser.add_argument('--annotation-description', '-d', metavar='annotation_description', type=str, required=True, action='append', help='Description of info field')
parser.add_argument('--annotation-type', '-t', metavar='annotation_type', type=str, required=True, action='append', help='Type of info field. Deafult: "String"')
parser.add_argument('--annotation-number', '-u', metavar='annotation_number', type=str, required=True, action='append', help='Description of info field. Default: 1')

args = parser.parse_args()



def parse_annotation(line):
    if line == "":
        raise Exception('Empty line')
    splitted_line = line.split('\t')
    try:
        int(splitted_line[1])
    except:
        parsed_line = {"chromosome" : None, "position" : None}
        return parsed_line
    if(len(splitted_line) < 3):
        parsed_line = {"chromosome" : None, "position" : None}
    else:
        parsed_line = {(idx + 1) : key for idx, key in enumerate(splitted_line)}
        parsed_line.update({"chromosome" : splitted_line[0], "position": splitted_line[1]})
    return parsed_line

def compare_positions(position_left, position_right):
#    print(str(position_left) + " " + str(position_right))
    if(position_left["chromosome"] is None):
        return -1
    if(position_right["chromosome"] is None):
        return 1
    try:
        if(int(position_left["chromosome"].strip('chr')) < int(position_right["chromosome"].strip('chr'))):
            return -1
        if(int(position_left["chromosome"].strip('chr')) > int(position_right["chromosome"].strip('chr'))):
            return 1
    except:
        if(position_left["chromosome"] < position_right["chromosome"]):
            return -1
        if(position_left["chromosome"] > position_right["chromosome"]):
            return 1
    try:
        int(position_left["position"])
    except:
        return 1
    try:
        int(position_right["position"])
    except:
        return -1
    if(int(position_left["position"]) < int(position_right["position"])):
        return -1
    if(int(position_left["position"]) > int(position_right["position"])):
        return 1
    return 0


input_vcf = pysam.VariantFile(args.input_vcf, "r")
for i in range(len(args.annotation_name)):
    input_vcf.header.info.add(args.annotation_name[i],args.annotation_number[i],args.annotation_type[i],args.annotation_description[i])
output_vcf = pysam.VariantFile("-", "w", header=input_vcf.header)

annotation_tsv = gzip.open(args.annotation_tsv, 'rt', encoding='utf-8')

try:
    vcf_line = next(input_vcf)
except:
    exit(0)

annotation_line = parse_annotation(annotation_tsv.readline().rstrip())


while True:
    vcf_pos = {"chromosome": vcf_line.chrom, "position": vcf_line.pos}
    annotation_pos = {"chromosome": annotation_line["chromosome"], "position": annotation_line["position"]}
    comparison = compare_positions(vcf_pos, annotation_pos)
    if(comparison < 0): # iterate over vcf
        #print("iterate vcf")
        output_vcf.write(vcf_line)
        try:
            vcf_line = next(input_vcf)
        except:
            break
    if(comparison > 0): # iterate over annotation
        #print("iterate tsv")
        try:
            annotation_line = parse_annotation(annotation_tsv.readline().rstrip())
        except:
            try:
                output_vcf.write(vcf_line)
                vcf_line = next(input_vcf)
            except:
                break
    if(comparison == 0): # annotate and iterate both
        #print("iterate both")
        #print("===: " + str(vcf_pos) + " " + str(annotation_pos) + " COMPARISON: " + str(comparison))
        new_record = vcf_line
        for anno_index in range(len(args.annotation_name)):
            new_record.info[args.annotation_name[anno_index]] = annotation_line[int(args.annotation_column[anno_index])]
        output_vcf.write(new_record)
        try:
            vcf_line = next(input_vcf)
        except:
            break
        try:
            annotation_line = parse_annotation(annotation_tsv.readline().rstrip())
        except:
            try:
                vcf_line = next(input_vcf)
            except:
                break

#annotation_tsv.readline().rstrip()
#annotation_tsv.readline().rstrip()
#print(parse_annotation(annotation_tsv.readline().rstrip()))
#print(parse_annotation(annotation_tsv.readline().rstrip()))
#print(parse_annotation(annotation_tsv.readline().rstrip()))

#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
 """
Translate values of a tab-delimited dictionary using another dictionary (a dictionary in which those values are keys).
Output is printed to *stdout*.

 """)

parser.add_argument('DICTIONARY_PATH', type=str, help="path to a dictionary you want to translate with")
parser.add_argument('-d', type=str, default=',', help="specify delimiter in a dictionary (Default: ,)")
parser.add_argument('-t', type=str, default=',', help="specify delimiter in a dictionary that will be used to "
                                                       "translate (Default: ,)")
parser.add_argument('-o', type=str, default=',', help="specify delimiter in an output dictionary (Default: ,)")
parser.add_argument('-k', action='store_true', help="keep untranslated values (Default: untranslated values are removed)")
parser.add_argument('-v', '--version', action='version', version=__version__)

args = parser.parse_args()
translation_dictionary = args.DICTIONARY_PATH
dictionary_delimiter = args.d
translation_dictionary_delimiter = args.t
output_delimiter = args.o
keep_untranslated_values = args.k

dictionary = {}

value_header = None
line_counter = 0

for line in open(translation_dictionary, 'r'):

    if line.startswith('#') and line_counter == 0:
        line_counter += 1
    elif line.startswith('#') and line_counter == 1:
        value_header = line.strip()
        line_counter = 0
    else:

        record = [x.strip() for x in line.split('\t')]

        key = record[0]
        values = [x.strip() for x in record[1].split(translation_dictionary_delimiter)]

        if key not in dictionary.keys():
            dictionary[key] = values
        else:
            dictionary[key] = dictionary[key] + values


for line in stdin:

    if line.startswith('#') and line_counter == 0:
        print(line.strip())
        print(value_header)
        line_counter += 1

    elif not line.startswith('#'):

        record = [x.strip() for x in line.split('\t')]
        values = [x.strip() for x in record[1].split(dictionary_delimiter)]

        translated_values = []

        if keep_untranslated_values:

            for value in values:

                if value in dictionary.keys():
                    translated_values = translated_values + dictionary[value]
                else:
                    translated_values = translated_values + [value]

            record[1] = output_delimiter.join(sorted([x.strip() for x in list(set(translated_values) - {''})]))

        else:

            at_least_one_value_seen_in_dictionary_keys = False

            for value in values:

                if value in dictionary.keys():
                    translated_values = translated_values + dictionary[value]
                    at_least_one_value_seen_in_dictionary_keys = True

            if at_least_one_value_seen_in_dictionary_keys:
                record[1] = output_delimiter.join(sorted([x.strip() for x in list(set(translated_values) - {''})]))
            else:
                record[1] = '.'

        print('\t'.join(record))

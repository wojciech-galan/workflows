#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
from math import ceil
import os
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(description='Takes an interval_list file (Picard style) as an input and outputs a directory with'
                                         ' smaller interval_list files that have similar sums of lenght.')

parser.add_argument('-v', '--version', action='version', version=__version__)
parser.add_argument('INTERVAL_FILE', type=str, help="path to an interval_file")
parser.add_argument('MAX_NO_GROUPS', type=int, help="maximum number of output interval_list files")
parser.add_argument('-d', type=str, default=None, help="output directory (Default: the directory containing INTERVAL_FILE)")

args = parser.parse_args()

interval_file = args.INTERVAL_FILE
max_no_groups = args.MAX_NO_GROUPS


if not args.d is None:
    output_directory = args.d
else:
    output_directory = os.path.dirname(os.path.abspath(interval_file))


interval_file_name = os.path.basename(interval_file)

def get_header(interval_file):
   header = ""
   for line in open(interval_file, 'r'):
       if line.startswith("@"):
           header += line
       else:
           break
   return header

def calculate_total_length(interval_file):
   total_length = 0
   for line in open(interval_file, 'r'):
       if line.startswith("@"):
           pass
       else:
           start = int(line.split("\t")[1])
           end = int(line.split("\t")[2])
           total_length += end - start
   return total_length


def calculate_length_of_one_group(max_no_groups, total_length):
  return int(ceil(float(total_length)/max_no_groups))


def get_str_with_no_group(group_number, max_no_groups_len):
   group_number_str = str(group_number)
   for i in range(max_no_groups_len - len(group_number_str)):
       group_number_str = "0" + group_number_str
   return group_number_str


def calculate_final_number_of_groups(interval_file, approximate_upper_limit_of_group_length):
   group_number = 1
   group_length = 0
   for line in open(interval_file, 'r'):
       if line.startswith("@"):
           pass
       else:
           start = int(line.split("\t")[1])
           end = int(line.split("\t")[2])
           group_length += end - start
           if group_length >= approximate_upper_limit_of_group_length:
               group_number += 1
               group_length = 0
   final_max_no_groups = group_number
   return final_max_no_groups


def print_group(group_lines, group_number, output_directory, interval_file_name, header, max_no_groups):
   group_number_str = get_str_with_no_group(group_number, len(str(max_no_groups)))
   with open(output_directory + "/" + group_number_str + "_" + interval_file_name, "w") as text_file:
       text_file.write(header + group_lines)

total_length = calculate_total_length(interval_file)
approximate_upper_limit_of_group_length = calculate_length_of_one_group(max_no_groups, total_length)
header = get_header(interval_file)
max_no_groups = calculate_final_number_of_groups(interval_file, approximate_upper_limit_of_group_length)

if not output_directory:
   output_directory = os.path.dirname(interval_file) + "/grouped_" + str(max_no_groups) + "_" + interval_file_name.replace(".interval_list", "")
else:
   output_directory = os.path.abspath(output_directory) + "/grouped_" + str(max_no_groups) + "_" + interval_file_name.replace(".interval_list", "")

if not os.path.exists(output_directory):
   os.makedirs(output_directory)

group_number = 1
group_length = 0
group_lines = ""

for line in open(interval_file, 'r'):
   if line.startswith("@"):
       pass
   else:
       start = int(line.split("\t")[1])
       end = int(line.split("\t")[2])
       group_length += end - start
       group_lines += line
       if group_length >= approximate_upper_limit_of_group_length:
           print_group(group_lines, group_number, output_directory, interval_file_name, header, max_no_groups)
           group_number += 1
           group_length = 0
           group_lines = ""

if group_lines != "":
   print_group(group_lines, group_number, output_directory, interval_file_name, header, max_no_groups)

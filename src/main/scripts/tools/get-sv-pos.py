import argparse


__version__ = '0.0.1'


#vcf_path="/home/kt/short-out.vcf"
#pos_file_path="/home/kt/short-out-pos.tsv"

def get_value_from_info(info, value):
    info_list = info.split(";", maxsplit = 4)[:-1]
    try:
        return [i.split('=')[1] for i in info_list if value == i.split("=")[0]][0]
    except IndexError:
        return ""


def get_bnd_other_breakpoints(alt_value):
    splitted_alt = alt_value.replace("N","").replace("A","").replace("C","").replace(
        "G","").replace("T","").replace("[","").replace("]","").replace(
        "ctg","").replace(">","").replace("<","").replace(":","\t").split(",")
    return splitted_alt

def main(vcf_path, pos_file_path):
    with open(vcf_path) as vcf, open(pos_file_path, "w") as pos_file:
        lines_to_write = []
        for line in vcf:
            if line[0] != "#":
                splitted_line = line.strip().split("\t")
                chrom = splitted_line[0]
                pos = splitted_line[1]
                event = splitted_line[2].split("_")[0]
                alt = splitted_line[4]
                info = splitted_line[7]
                svtype = get_value_from_info(info, "SVTYPE")
                end = get_value_from_info(info, "END")
                identifier = event + "_" + svtype
                positions= []
                positions.append( chrom + "\t" + pos)
                if svtype == "BND":
                    positions += get_bnd_other_breakpoints(alt)
                else:
                    positions.append(chrom + "\t" + end)
                for i in positions:
                    lines_to_write.append(i + "\t" + identifier)
        pos_file.write("\n".join(sorted(set(lines_to_write))))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Extracts positions of breakpoints from sv vcf.")
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input_vcf', required=True, type=str, help='Path to input sv vcf.')
    parser.add_argument('-o', '--breakpoint_list', required=True, type=str,
                        help='Path to output breakpoint list file.')

    args = parser.parse_args()
    main(args.input_vcf, args.breakpoint_list)
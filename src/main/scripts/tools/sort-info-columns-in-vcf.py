#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(description="Sorts INFO column fields of VCF file to appear in the same order as in the header.")
parser.add_argument('-v', '--version', action='version', version=__version__)

args = parser.parse_args()


def sort_info_line(INFO, ordered_fields):

    INFO_dict = {}
    new_INFO = []

    for info in INFO:
        if info.count('=') > 0:
            key = info[:info.index('=')]
        else:
            key = info
        INFO_dict[key] = info

    for field in ordered_fields:
        if field in INFO_dict.keys():
            new_INFO.append(INFO_dict[field])

    return ';'.join(new_INFO)


ordered_fields = []

for line in stdin:

    if line.startswith('##INFO'):
        print(line.strip())
        ordered_fields.append(line[len('##INFO=<ID='):line.index(',')])
    elif line.startswith('#'):
        print(line.strip())
    else:
        line = line.strip().split('\t', 8)
        line[7] = sort_info_line(line[7].split(';'), ordered_fields)
        print('\t'.join(line))

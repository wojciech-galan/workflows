#!/usr/bin/env python3

__version__ = '3.0.0'

import argparse
import csv
import re
import json
import os
from collections import defaultdict
import pandas as pd

WORKDIR = ""

def read_file(sample, metrics: str, prefix='## METRICS'):
    "Reads fragments of the output files from gatk metrics tools. Files are divided into sections with ## in the beginning. Prefix is the header"

    path = os.path.join(WORKDIR, str(sample) + "-" + metrics + ".txt")
    with open(path, mode='r') as infile:
        for line in infile:
            if line.startswith(prefix):
                break
        for line in infile:
            if len(line.strip()) == 0:
                break
            yield line


def parse_value(n):
    if n.isdigit():
        return int(n)
    if re.match(r'^-?\d+(?:\.\d+)?$', n):
        return float(n)
    return n


def file_to_df(sample, chromosome: str, metrics: str, prefix='## METRICS'):
    "Input is generator returned by read_file() and in read_csv is transformed to DataFrame for specific sample and gatk tool. Prefix can be '## HISTROGRAM'."

    reader = csv.reader(read_file(sample, metrics, prefix), delimiter="\t")
    headers = reader.__next__()
    rows = [[parse_value(r) for r in row] for row in reader]
    df = pd.DataFrame(rows)
    df.columns = [metrics + '_' + h for h in headers]

    if "CollectAlignmentSummaryMetrics_CATEGORY" in df.columns:
        return df.loc[(df["CollectAlignmentSummaryMetrics_CATEGORY"] == "PAIR") | (df["CollectAlignmentSummaryMetrics_CATEGORY"] == "UNPAIRED")].reset_index(drop=True)

    return df.reset_index(drop=True)


def concat_metrics(sample: int, chromosome: str, metrics_names: list):
    "Concatenate DataFrames for different metrics from gatk tool for one sample. Prefix can be only prefix='## METRICS'. ## HISTOGRAM form different tools can not be merged."

    dfs = [file_to_df(sample, chromosome,  metrics) for metrics in metrics_names]

    return pd.concat(dfs, axis=1)

#def concat_samples(samples: list, chromosome: str, metric_names: list):
#    "Append to dataframe multiple outputs from function concat_metrics for list of samples."
#
#    dfs = []
#    for s in samples:
#        df = concat_metrics(s, metric_names)
#        dfs.append(df)
#    return pd.concat(dfs)

def output_dict( sample: int, chromosome: str, metrics_names: list) -> list:
    "Select from single-sample-gatk-metrics-dataframe queries and metadata contained in 'error-warning' json, specific to some kit (example: agilent v7)."

    metrics = concat_metrics(sample, chromosome, metrics_names)
    metrics = metrics.to_dict('records')[0]
    return { "chromosome": chromosome, "sample": sample, "metrics": metrics }

def save_json(dictionary, json_name):
    with open(json_name, 'w') as final_json:
        json.dump(dictionary, final_json, indent=2)

def main():
    parser = argparse.ArgumentParser(
        description='Make json with data as a result of gatk-metrics tools for bam-quality check: https://broadinstitute.github.io/picard/picard-metric-definitions.html. Requiered output txt files from this tools as ${sample-name}-${tool-name}.txt. Json will be filled with metadata from another json, specific to sequencing type - kit-json. Additionally there is csv output with statistics from all metrics and all gatk tools: https://broadinstitute.github.io/picard/picard-metric-definitions.html')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {}'.format(__version__))

    parser.add_argument('--metrics', '-m', action="append",
                        required=True, help='list of gatk-metrics tools to use.')

    parser.add_argument('--sample', '-s', metavar='sample id as string', type=str,
                        required=True, help='sample id, can be string or integer')

    parser.add_argument('--workdir', '-w', required=False, default='',
                        help='Path to directory where gatk-metrics txt files are stored. ')
    parser.add_argument('--chromosome', '-c', required=False, default='chr', help="chromosome")
    args = parser.parse_args()

    global WORKDIR
    WORKDIR = args.workdir

    result = output_dict(args.sample, args.chromosome, args.metrics)
    if result:
        save_json(result, f'{args.chromosome}_{args.sample}_final.json')
    else:
        open(f'{args.chromosome}_{args.sample}_final.json', 'a').close()
    ###concat_metrics(args.sample, args.metrics).T.to_csv("{0}_final.tsv".format(args.sample), sep='\t')


if __name__ == "__main__":
    main()

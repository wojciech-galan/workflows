#!/usr/bin/python3
__version__ = '0.0.1'

import argparse
import os
from typing import List
from typing import Tuple

from math import ceil


def get_header(interval_file: str) -> str:
    header = ""
    with open(interval_file, 'r') as f:
        for line in f:
            if line.startswith("@"):
                header += line
            else:
                break
    return header


def calculate_total_length(interval_file: str) -> int:
    total_length = 0
    with open(interval_file, 'r') as f:
        for line in f:
            if not line.startswith("@"):
                start, end = get_start_and_end_from_line(line)
                total_length += end - start
    return total_length


def calculate_length_of_one_piece(no_pieces: int, total_length: int) -> int:
    return int(ceil(total_length / no_pieces))


def calculate_final_number_of_pieces(interval_file: str, approximate_upper_limit_of_piece_length: int) -> int:
    piece_number = 1
    piece_length = 0
    with open(interval_file, 'r') as f:
        for line in f:
            if not line.startswith("@"):
                start, end = get_start_and_end_from_line(line)
                piece_length += (end - start)
                if piece_length >= approximate_upper_limit_of_piece_length:
                    piece_number += 1
                    piece_length = 0
    return piece_number


def write_piece_and_return_fname(piece_lines: str, piece_number: int, interval_file_name: str, header: str,
                                 no_pieces: int, out_dir: str) -> str:
    piece_number_str = '%0*d' % (no_pieces // 10 + 1, piece_number)
    fpath = os.path.join(out_dir, piece_number_str + "_" + interval_file_name)
    with open(fpath, "w") as text_file:
        text_file.write(header + piece_lines)
    return os.path.abspath(fpath)


def create_list_file(paths: List[str], out_dir: str, interval_file_name: str) -> None:
    out_name = "scattered_{}_{}interval_list_files".format(len(paths), interval_file_name.replace("interval_list", ""))
    with open(os.path.join(out_dir, out_name), "w") as text_file:
        text_file.write('\n'.join(paths))


def get_start_and_end_from_line(line: str) -> Tuple[int]:
    line_splitted = line.split("\t", 3)
    return int(line_splitted[1]), int(line_splitted[2])


def split_file_and_return_list_of_splitted_outfiles(interval_file: str, header: str, num_of_pieces: int,
                                                    out_dir: str) -> List[str]:
    interval_file_name = os.path.basename(interval_file) + ".intervals"
    out_paths = []
    piece_number = 1
    piece_length = 0
    piece_lines = ""

    with open(interval_file, 'r') as f:
        for line in f:
            if not line.startswith("@"):
                start, end = get_start_and_end_from_line(line)
                piece_length += end - start
                piece_lines += line
                if piece_length >= approximate_upper_limit_of_piece_length:
                    out_paths.append(write_piece_and_return_fname(piece_lines, piece_number, interval_file_name, header,
                                                                  num_of_pieces, out_dir))
                    piece_number += 1
                    piece_length = 0
                    piece_lines = ""

    if piece_lines:
        out_paths.append(
            write_piece_and_return_fname(piece_lines, piece_number, interval_file_name, header, num_of_pieces, out_dir))
    return out_paths


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='splits interval file into given number of aproximately same size parts')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--interval_file', '-f', type=str, required=True, help='interval file to be splitted ')
    parser.add_argument('--output_dir', '--o', type=str, help='Path to the output directory')
    parser.add_argument('--no_pieces', '-n', type=int, nargs='?', default=3,
                        help='input file will be splitted into that many files')
    parser.add_argument('--do_create_list_file', '-l', action="store_true", help=' create list file')
    parser.add_argument('--print_absolute_paths_of_created_interval_files', '-p', action="store_true",
                        help=' print absolute paths of created interval files')

    arguments = parser.parse_args()

    # interval_file = "sureselect-human-all-exon-v7.covered-padded-200.interval_list"

    output_dir = arguments.output_dir or os.path.dirname(arguments.interval_file)

    total_length = calculate_total_length(arguments.interval_file)
    approximate_upper_limit_of_piece_length = calculate_length_of_one_piece(arguments.no_pieces, total_length)
    header = get_header(arguments.interval_file)
    out_paths = split_file_and_return_list_of_splitted_outfiles(arguments.interval_file, header, arguments.no_pieces,
                                                                output_dir)

    if arguments.do_create_list_file:
        create_list_file(out_paths, output_dir, os.path.basename(arguments.interval_file))

    if arguments.print_absolute_paths_of_created_interval_files:
        for path in out_paths:
            print(path)

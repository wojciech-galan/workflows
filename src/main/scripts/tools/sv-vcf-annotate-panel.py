#!/usr/bin/python3

__version__ = '0.0.1'

import argparse
import json
import pysam


def extract_genes(data_line, gene_field, split_char):
    return data_line.info[gene_field][0].split(split_char)


def prepare_panel(panel_json):
    with open(panel_json, "r") as f:
        panel = json.load(f)
        panel_genes = set()
        panel_dict = {}
        for i in panel:
            panel_genes.add(i["name"])
            panel_dict[i["name"]] = ":".join([i["name"], str(round(i["score"], 2)), i["type"].replace(" ", "^")])
    return panel_genes, panel_dict


def analyze_data_line(line, vcf, panel_dict, panel_genes, filter_type):
    try:
        all_genes = list(set(extract_genes(line, "Gene_name", "|")))
        if bool(panel_genes):

            all_genes_intersect = bool(panel_genes.intersection(all_genes))
            annotations = ""

            for gene in all_genes:
                if all_genes_intersect:
                    try:
                        annotations += panel_dict[gene] + "|"
                    except KeyError:
                        annotations += ""

                    line.info["ISEQ_GENE_PANEL_OVERLAP"] = annotations[:-1]

            vcf.write(line)
        else:
            vcf.write(line)
    except (IndexError, KeyError):
        vcf.write(line)




if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=
                                     '''Annotates and optionally filters sv vcf with gene panel
                                        If panel json file is not given
                                              script removes from vcf file variants with no gene overlap,
                                              except for known pathogenic variants.
                                                                     
                                        If panel json is provided 
                                              script adds annotations ISEQ_GENE_PANEL_OVERLAP.
                                              (only to lines with SV fulfilling given criterion)
                                              ISEQ_GENE_PANEL_OVERLAP - gene_name:score:type| (...) | gene_name:score:type, 
                                              intersection of panel genes with any gene having overlap with SV is checked.
                                                                     
                                                                       
                                               ''')
    parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, help='Input vcf file')
    parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, help='Output vcf.gz file')
    parser.add_argument('--gene-panel', '-p' , metavar='gene_panel', type=str, required=False,
                        help='Gene panel json (output from panel-generate task)')
    parser.add_argument('--filter-type', '-f', metavar='filter_type', type=str, default="any_gene",
                        help='Determines filter to apply. Default="%(default)s"',
                        choices=["panel_overlap", "any_gene", "no_filter"])
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()
    try:
        panelgenes, paneldict = prepare_panel(args.gene_panel)
    except TypeError:
        panelgenes, paneldict = [], {}

    vcf_reader = pysam.VariantFile(filename=args.input_vcf)

    # Annotation adding

    items = [('ID', "ISEQ_GENE_PANEL_OVERLAP"),
                 ('Number', "A"),
                 ('Type', "String"),
                 ('Description',
                  "Keeps names, fitness scores and panel types for all panel genes having overlap with SV, format: name:score:type|(...)|name:sore:type"),
                 ('Source', 'ISEQ'),
                 ('Version',
                  'NA')]

    vcf_reader.header.add_meta("INFO", items=items)

    header_vcf = vcf_reader.header

    vcf_writer_name = args.output_vcf

    vcf_writer = pysam.VariantFile(vcf_writer_name, 'w',
                                   header=header_vcf)

    for record in vcf_reader.fetch():
        analyze_data_line(record, vcf_writer, paneldict, panelgenes, args.filter_type)
    # print(args.filter_type)

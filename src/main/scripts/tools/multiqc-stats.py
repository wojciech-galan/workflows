#!/usr/bin/env python3

import argparse
import json

__version__ = '1.0.1'

def load_json(input_json) -> dict:
    with open(input_json) as file:
        return json.load(file)

def get_json_with_thresholds(stats, thresholds):
    output = {}
    for sample in stats:
        if sample[:10] != "ref_sample":
            sample_stats = {}
            for key, value in stats[sample].items():
                if key in thresholds:
                    if thresholds[key]["operator"] == "less":
                        if float(value) < float(thresholds[key]["error"]):
                            pass_value = "fail"
                        elif float(value) < float(thresholds[key]["warning"]):
                            pass_value = "warning"
                        else:
                            pass_value = "pass"
                    elif thresholds[key]["operator"] == "more":
                        if float(value) > float(thresholds[key]["error"]):
                            pass_value = "fail"
                        elif float(value) > float(thresholds[key]["warning"]):
                            pass_value = "warning"
                        else:
                            pass_value = "pass"
                    else:
                        if value == "pass":
                            pass_value = "True"
                        else:
                            pass_value = "False"
                    sample_stats[key] = {"value": value, "pass": pass_value, "name": thresholds[key]["name"]}
            output[sample] = sample_stats
    return output

def save_json(json_dict, sample_id):
    out = f'{sample_id}_multiqc-stats.json'
    with open(out, 'w') as final_json:
        json.dump(json_dict, final_json, indent=2)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='converts multiqc json into more readable and adds info about passing thresholds from threshold json')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--stats-json', '-j', type=str, required=True, metavar='stats_json', help='output from jq ".report_saved_raw_data.multiqc_general_stats * .report_saved_raw_data.multiqc_fastqc" multiqc_data/${sample_id}_multiqc-data.json')
    parser.add_argument('--thresholds', '-t', type=str, required=True, help='json with thresholds for statistics in input json')
    parser.add_argument('--id', '-i', type=str, required=True, help='sample id for json')
    args = parser.parse_args()

    stats_json = load_json(args.stats_json)
    thresholds_json = load_json(args.thresholds)

    save_json(get_json_with_thresholds(stats_json, thresholds_json), args.id)


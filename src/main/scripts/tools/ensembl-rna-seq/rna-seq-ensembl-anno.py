#!/usr/bin/python3
import argparse
from pybiomart import Server
import pandas as pd
from pathlib import Path
from requests.exceptions import HTTPError, ConnectionError
import time
import json


__version__ = "0.0.1"
AUTHOR = "gitlab.com/olaf.tomaszewski"


def attempt_to_get_database_query(organism_name, version, ensembl_attributes, versions_json, attributes_json):
    ensembl_available = False
    num_of_attempts = 0
    max_n_of_attempts = 2
    while not ensembl_available:
        try:
            return get_database_query(organism_name, version, ensembl_attributes, versions_json, attributes_json)
            ensembl_available = True
        except (HTTPError, ConnectionError):
            if num_of_attempts >= max_n_of_attempts:
                raise
            time.sleep(2 ** num_of_attempts)
        num_of_attempts += 1
    return None


def get_attributes(ensembl_attributes, attributes_json):
    attributes = ['ensembl_transcript_id']

    with open(attributes_json) as attributes_file:
        attributes_dict = json.load(attributes_file)

    for attribute in ensembl_attributes:
        attributes.append(attributes_dict[attribute])

    return attributes


def get_database_query(organism_name, version, ensembl_attributes, versions_json, attributes_json):
    with open(versions_json) as versions_file:
        versions_dict = json.load(versions_file)

    server = Server(versions_dict[version])
    mart = server['ENSEMBL_MART_ENSEMBL']
    dataset = mart[f'{organism_name}_gene_ensembl']
    attributes = get_attributes(ensembl_attributes, attributes_json)

    queried = dataset.query(attributes=attributes)
    return queried


def create_version_catalog_and_export(queried, output_dir, version):
    version_folder = f'{output_dir}/{version}'
    Path(version_folder).mkdir(parents=True, exist_ok=True)
    queried.to_csv(f'{version_folder}/rna_seq_ensembl.csv', sep='\t')


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates .bed file from cosmic database using pybiomart api')
    parser.add_argument('--input-organism-name', '-n', metavar='input_organism_name', type=str, required=True,
                        help='Name of organism compatible with ensembl database')
    parser.add_argument('--input-version', '-s', metavar='input_version', type=str, required=True,
                        help='Version of dataset')
    parser.add_argument('--input-ensembl-attributes', '-a', metavar='input_ensembl_attributes', nargs='*',
                        required=False, help='List of readable attributes for nontechnical')
    parser.add_argument('--input-versions-json', '-j', metavar='input_versions_json', type=str, required=True,
                        help='Release versions in key-value format (version-link)')
    parser.add_argument('--input-attributes-json', '-l', metavar='input_attributes_json', type=str, required=True,
                        help='Attributes in key-value format for nontechnical and technical')
    parser.add_argument('--output-dir', '-o', metavar='output_dir', type=str, required=True,
                        help='Directory where goes output')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--max_attempts', type=int, default=8,
                        help='Max number of attempts when retrieving data from ensembl.')
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = args_parser_init()
    queried = attempt_to_get_database_query(args.input_organism_name, args.input_version,
                                            args.input_ensembl_attributes, args.input_versions_json,
                                            args.input_attributes_json)

    if queried is not None:
        create_version_catalog_and_export(queried, args.output_dir, args.input_version)

#!/usr/bin/env python3

__version__ = '0.0.2'

import argparse
import gzip
from Bio import bgzf
from os import path
import os
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)


def get_header(vcf_gz:str) -> str:
    header_lines = []
    with gzip.open(vcf_gz, 'rb') as f:
        for line in f:
            if line.decode().startswith("#"):
                header_lines.append(line.decode().strip())
            else:
                break
    return '\n'.join(header_lines)


def get_indices_of_lines_in_each_piece (vcf_gz, no_pieces):
    no_lines = sum(1 for line in gzip.open(vcf_gz) if not line.decode().startswith("#"))
    no_lines_in_piece = no_lines // no_pieces
    return [x * no_lines_in_piece for x in range(0, no_pieces)]


if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=
    """
    
    Takes as inputs bgzipped VCF file and:
      - creates smaller bgzipped VCF files 
      - files are saved to specified directory
    
    """)
    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('VCF_GZ', type=str, help="path to VCF file")
    parser.add_argument('NO_PIECES', type=int, help="number of output VCF files")
    parser.add_argument('-d', '--output-directory', dest='output_directory',type=str, default=".", help="output directory (Default: the working directory)")
    parser.add_argument('-n', '--basename', dest='basename', type=str, default=None, help="output vcf basename (Default: input VCF basename)")

    args = parser.parse_args()

    vcf_gz = args.VCF_GZ
    no_pieces = args.NO_PIECES
    output_directory = args.output_directory
    vcf_basename = args.basename

    if vcf_basename is None:
        vcf_basename = os.path.basename(vcf_gz)

    pieces = [("00"+str(i))[-3:] for i in range(0, no_pieces)]
    handlers = [bgzf.BgzfWriter(output_directory + "/" + x + "." + vcf_basename) for x in pieces]

    indices_of_first_lines_in_each_piece = get_indices_of_lines_in_each_piece(vcf_gz, no_pieces)

    header = get_header(vcf_gz).encode()
    for i in range(0, no_pieces):
        handlers[i].write(header+ b'\n')

    index = 0
    no_piece = 0

    for line in gzip.open(vcf_gz, 'rb'):

        if not line.decode().startswith("#"):

            if index >= indices_of_first_lines_in_each_piece[-1]:
                handlers[no_pieces - 1].write(line.decode())
            elif index < indices_of_first_lines_in_each_piece[no_piece + 1]:
                handlers[no_piece].write(line.decode())
            else:
                no_piece += 1
                handlers[no_piece].write(line.decode())

            index += 1

    for handle in handlers:
        handle.close()

#!/usr/bin/env python3
from sys import stdin
import re
from signal import signal, SIGPIPE, SIG_DFL
import argparse
__version__ = '1.0.2'

parser = argparse.ArgumentParser(description='Changes dbNSFP output')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('-d', '--database_version', required = True,  type=str, help='dbNSFP database version, for example v4.1c')
args = parser.parse_args()

signal(SIGPIPE, SIG_DFL)
info_add = ". Commas in original dbNSFP field are replaced by colons. " \
           "More detailed description can be found in" \
           " https://sites.google.com/site/jpopgen/dbNSFP\",Source=\"dbNSFP\",Version=\"{}\">".format(args.database_version)

old_ac = "_AC,Number=A,Type=String"
new_ac = "_AC,Number=A,Type=Integer"
old_af1 = "_AF,Number=A,Type=String"
new_af1 = "_AF,Number=A,Type=Float"
old_af2 = "_AF,Number=A,Type=Integer"
new_af2 = "_AF,Number=A,Type=Float"
old_nr = "GERP___NR,Number=A,Type=String"
new_nr = "GERP___NR,Number=A,Type=Float"
old_rs = "GERP___RS,Number=A,Type=String"
new_rs = "GERP___RS,Number=A,Type=Float"

for raw_line in stdin:
    if raw_line.startswith("#"):
        if raw_line.startswith("##INFO=<ID=dbNSFP"):
            print((re.sub("\">", info_add,
                          re.sub(old_af1, new_af1,
                                 re.sub(old_af2, new_af2,
                                        re.sub(old_nr, new_nr,
                                               re.sub(old_rs,new_rs,
                                                      re.sub(old_ac, new_ac,
                                                             re.sub("Number=.", "Number=A",
                                                                    re.sub("Type=Character", "Type=String",raw_line))))))))).strip())
        else:
            print(raw_line.strip())
    else:
        line = raw_line.split("\t", 8)
        INFO = line[7].strip()
        INFO_colon = []
        for info in INFO.split(';'):
            if info.startswith("dbNSFP"):
                INFO_colon.append(info.replace(",", ":"))
            else:
                INFO_colon.append(info)

        line[7] = ";".join(INFO_colon)

        print(("\t".join(line)).strip())

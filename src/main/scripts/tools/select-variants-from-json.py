#!/user/bin/env python3

import json
import argparse

__version__ = '0.0.2'


def read_json(json_input):
    with open(json_input) as json_file:
        vcf_dict = json.load(json_file)
        return vcf_dict


def get_items_with_key(variant_list, chosen_key):
    return [i for i in variant_list if chosen_key in i.keys() and i[chosen_key]]


def sort_list_by_acmg_score(variant_list):
    list_with_score = get_items_with_key(variant_list, "ISEQ_ACMG_SUMMARY_SCORE")
    return sorted(list_with_score, key=lambda x: x["ISEQ_ACMG_SUMMARY_SCORE"], reverse=True)


def get_all_clivnar_patho(variant_list):
    list_with_clinvar_field = get_items_with_key(variant_list, "ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE")
    patho = [i for i in list_with_clinvar_field if i["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0].lower() == "pathogenic"]
    patho_likely_patho = [i for i in list_with_clinvar_field if i["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0].lower() == "pathogenic/likely_pathogenic"]
    likely_patho = [i for i in list_with_clinvar_field if
                    i["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0].lower() == "likely_pathogenic"]
    return patho + patho_likely_patho + likely_patho


def get_acmg_non_benign(variant_list):
    list_with_acmg_field = get_items_with_key(variant_list, "ISEQ_ACMG_SUMMARY_CLASSIFICATION")
    return [i for i in list_with_acmg_field if
            i["ISEQ_ACMG_SUMMARY_CLASSIFICATION"].lower() in ["pathogenic", "likely^pathogenic", "uncertain"]]


def add_acmg_variants(clinvar, sorted_acmg, n):
    clinvar_len = len(clinvar)
    output_list = clinvar[:]
    for i in sorted_acmg:
        if i not in clinvar:
            output_list.append(i)
    # print("clinvar len: "+ str(clinvar_len))
    variant_number = max([n, clinvar_len])
    restricted_list = output_list[0:variant_number]
    restricted_with_acmg = get_items_with_key(restricted_list, "ISEQ_ACMG_SUMMARY_SCORE")
    restricted_acmg_sorted = sort_list_by_acmg_score(restricted_with_acmg)
    restricted_wo_acmg = [i for i in restricted_list if i not in restricted_with_acmg]
    return restricted_acmg_sorted + restricted_wo_acmg


def sort_sv_variants(variant_list, n):
    list_with_annotsv_rank = get_items_with_key(variant_list, "ACMG_class")
    non_benign = []
    for rank in ["pathogenic", "likely^pathogenic", "uncertain"]:
        picked_list = [i for i in list_with_annotsv_rank if i["ACMG_class"][0].lower() == rank and i["SVTYPE"] != "BND"]
        sorted_picked_list = sorted(picked_list, key=lambda x: x["AnnotSV_ranking_score"], reverse=True)
        non_benign.extend(sorted_picked_list)
    return non_benign[0:n]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Selects variants that should be shown in report.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-json', '-i', type=str, required=True, help='Input json file (with all variants)')
    parser.add_argument('--output-json', '-o', type=str, required=True,
                        help='output json file (with selected variants)')
    parser.add_argument('--variant-number', '-n', type=int, default=50, help='Number of variants to keep')
    parser.add_argument('--sv', '-s', action='store_true', help='Use this flag for json from SV calling')
    args = parser.parse_args()

    # all variants
    vcf_list = read_json(args.input_json)
    if args.sv:
        final_list = sort_sv_variants(vcf_list, args.variant_number)
    else:
        # acmg non benign/likely_benign variants
        acmg_list = get_acmg_non_benign(vcf_list)
        # non benign acmg variants sorted by severity
        sorted_acmg_list = sort_list_by_acmg_score(acmg_list)
        # all pathogenic/likely_pathogenic clivar variants
        clinvar_list = get_all_clivnar_patho(vcf_list)
        # final list: all from clinvar  plus the most severe from acmg list
        final_list = add_acmg_variants(clinvar_list, sorted_acmg_list, args.variant_number)
    # print(len(final_list))

    with open(args.output_json, "w") as f:
        t = json.dumps(final_list)
        f.write(t)

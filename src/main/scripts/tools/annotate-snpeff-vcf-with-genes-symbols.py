#!/usr/bin/env python3

__version__ = '0.0.4'

import argparse
import re
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL

signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
                                 """
                                 The script annotates the VCF provided as stdin:
                                   1. with the highest SnpEff impact
                                   2. with symbols of genes, that a given variant have specified or greater than specified threshold SnpEff impact
                                   3. with ann field that should be displayed in report
                                 
                                 Input VCF must be previously annotated by SnpEff.
                                 """)

parser.add_argument('-v', '--version', action='version', version=__version__)
parser.add_argument('-t', '--impact', dest='impact', type=str, default='MODERATE',
                    choices={'HIGH', 'MODERATE', 'LOW', 'MODIFIER'},
                    help="IMPACT threshold")
parser.add_argument('-g', '--genes-field-name', dest='genes_field_name', type=str, default='ISEQ_GENES_NAMES',
                    help="Name of a field with genes symbols in output VCF file")
parser.add_argument('-i', '--impact-field-name', dest='impact_field_name', type=str, default='ISEQ_HIGHEST_IMPACT',
                    help="Name of a field with highest impact in output VCF file")
parser.add_argument('-r', '--report-ann-field-name', dest='report_ann_field_name', type=str,
                    default='ISEQ_REPORT_ANN',
                    help="Name of a field with highest impact in output VCF file")
parser.add_argument('-s', '--snpeff-version', dest='snpeff_version', type=str, default='5.0c', help="Version of SnpEff")

args = parser.parse_args()

threshold = args.impact
genes_field = args.genes_field_name
impact_field = args.impact_field_name
report_ann_field = args.report_ann_field_name
version = args.snpeff_version

levels = {'HIGH': 0, 'MODERATE': 1, 'LOW': 2, 'MODIFIER': 3}
ranks = {0: 'HIGH', 1: 'MODERATE', 2: 'LOW', 3: 'MODIFIER', 665: "."}


def uniquify(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


for line in stdin:

    if line[0:14] == "##INFO=<ID=ANN":

        info_field = re.sub('##INFO=<ID=ANN.*?\'', '', line)
        info_field = re.sub('\' ">', '', info_field)

        info_field_list = info_field.split(" | ")

        allele_index = [i for i, s in enumerate(info_field_list) if 'Allele' in s][0]
        annotation_impact_index = [i for i, s in enumerate(info_field_list) if 'Annotation_Impact' in s][0]
        gene_name_index = [i for i, s in enumerate(info_field_list) if 'Gene_Name' in s][0]
        errors_index = [i for i, s in enumerate(info_field_list) if 'ERRORS' in s][0]
        biotype_index = [i for i, s in enumerate(info_field_list) if 'Transcript_BioType' in s][0]

        print(line.rstrip())
        print(
            "##INFO=<ID=" + genes_field + ',Number=A,Type=String,Description="Names of genes the variant has at least ' + threshold + ' impact on. Format: gene_symbol : (...) : gene_symbol",Source="SnpEff",Version="' + version + '">''')
        print(
            "##INFO=<ID=" + impact_field + ',Number=A,Type=String,Description="The highest impact of an allele.",Source="SnpEff",Version="' + version + '">''')
        print(
            "##INFO=<ID=" + report_ann_field + ',Number=A,Type=String,Description="SnpEff ANN subfield (transcript) that will be displayed in report. Format: Allele | Annotation | Annotation_Impact | Gene_Name | Gene_ID | Feature_Type | Feature_ID | Transcript_BioType | Rank | HGVS.c | HGVS.p | cDNA.pos / cDNA.length | CDS.pos / CDS.length | AA.pos / AA.length | Distance | ERRORS / WARNINGS / INFO",Source="SnpEff",Version="' + version + '">''')

    elif line[0] == "#":
        print(line.rstrip())

    else:
        # create list
        line_list = line.split('\t', 8)

        # extract INFO field
        if "ANN=" in line_list[7]:

            info_list = line_list[7].split(';')

            # find ANNO field
            where_anno = [i for i, s in enumerate(info_list) if s.startswith("ANN=")]
            where_anno = where_anno[0]

            # extract ANNO filed
            annotations = info_list[where_anno].strip()
            annotations_list = re.sub('^ANN=', '', annotations).split(",")

            ALTs = line_list[4].split(",")

            genes = []
            impacts = []
            report_transcripts = []

            for alt in ALTs:

                alt_genes = []
                alt_impacts = []
                alt_transcripts = []

                for anno in annotations_list:
                    splitted_anno = anno.split('|')
                    if anno[allele_index] == alt and levels[splitted_anno[annotation_impact_index]] <= levels[
                        threshold] and splitted_anno[gene_name_index] != "" and splitted_anno[
                        biotype_index] == "protein_coding" and 'ERROR' not in splitted_anno[
                        errors_index] and 'WARNING' not in splitted_anno[errors_index]:
                        alt_genes.append(splitted_anno[gene_name_index])
                    if splitted_anno[allele_index] == alt and splitted_anno[annotation_impact_index] != "" and \
                            splitted_anno[biotype_index] == "protein_coding" and 'ERROR' not in splitted_anno[
                        errors_index] and 'WARNING' not in splitted_anno[errors_index]:
                        alt_impacts.append(levels[splitted_anno[annotation_impact_index]])
                    if splitted_anno[allele_index] == alt and splitted_anno[
                        biotype_index] == "protein_coding" and 'ERROR' not in splitted_anno[
                        errors_index] and 'WARNING' not in splitted_anno[errors_index]:
                        alt_transcripts.append(anno)
                if not alt_genes:
                    alt_genes.append(".")

                if not alt_impacts:
                    alt_impacts.append(665)

                if not alt_transcripts:
                    alt_transcripts.append(".")

                alt_genes = ':'.join(uniquify(alt_genes))
                alt_impact = ranks[min(alt_impacts)]
                alt_transcripts = alt_transcripts[0]

                genes.append(alt_genes)
                impacts.append(alt_impact)
                report_transcripts.append(alt_transcripts)

            if genes == ["."] and impacts == ["."] and report_transcripts == ["."]:
                print(line.rstrip())

            else:

                genes = ','.join(genes)
                impacts = ','.join(impacts)
                report_transcripts = ','.join(report_transcripts)

                if not genes == ".":
                    info_list[where_anno] = info_list[where_anno].strip() + ";" + genes_field + "=" + genes

                if not impacts == ".":
                    info_list[where_anno] = info_list[where_anno].strip() + ";" + impact_field + "=" + impacts

                if not report_transcripts == ".":
                    info_list[where_anno] = info_list[
                                                where_anno].strip() + ";" + report_ann_field + "=" + report_transcripts

                info = ';'.join(info_list)
                line_list[7] = info
                line = '\t'.join(line_list)
                print(line.rstrip())

        else:
            print(line.strip())

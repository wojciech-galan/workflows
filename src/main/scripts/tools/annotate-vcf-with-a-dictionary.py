#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=
"""

Annotates VCF with a dictionary.

**Dictionary** is defined as a flat file consisted of a VCF-style header (however "Number" field is meaningless
for dictionaries, so it's value should always be a ".") and body with lines consisting of tab delimited pairs
of keys and values.

  + Dictionary format:

      ##INFO=<ID=${KEY_FIELD_NAME},Number=.,Type=(...),Description="(...)",Source="(...)",Version="(...)">
      ##INFO=<ID=${VALUES_FIELD_NAME},Number=.,Type=(...),Description="(...)",Source="(...)",Version="(...)">
      KEY_1   value_1_1,value_1_2,(...),value_1_n
      KEY_2   value_2_1,value_2_2,(...),value_2_k
      (...)
      KEY_N   value_N_1,value_N_2,(...),value_N_m

   + Characters forbidden in keys and values: ^ , ~ # : | ;

**Input VCF** file must contain an INFO field with keys. Allowed format is:

  + one set of colon-delimited keys per allele ("Number=A"):

    INFO_FIELD_WITH_KEYS=key:key:(...):key,key:key:(...):key,(...),key:key:(...):key


**Output VCF** will contain field the follwing field

  + each key is substituted for "^" delimited list of values from the dictionary

  + if the key is absent in the dictionary, that key is substituted with a dot (".")

    FIELD_NAME=value^value^(...)^value:value^value^(...)^value^(...),(...),value^value^(...)^value:value^value^(...)^value:(...):value^value^(...)^value

""")

parser.add_argument('-v', '--version', action='version', version=__version__)
parser.add_argument('DICTIONARY_PATH', type=str, help="path to a dictionary")
parser.add_argument('KEY_COLUMN_NAME', type=str, help="specify name of a VCF INFO field containing keys")
parser.add_argument('FIELD_NAME', type=str, help="specify name of a field that will contain annotation")
parser.add_argument('HEADER', type=str, help="specify header line corresponding to a field containing annotation")

parser.add_argument('-k', '--input-vcf-keys-delimeter', dest='input_vcf_keys_delimiter', type=str, default=':', help="specify delimiter in input VCF INFO field containing keys (Default:\":\")")
parser.add_argument('-d', '--dictionary-values-delimeter', dest='dictionary_delimiter', type=str, default=',', help="specify delimiter in dictionary delimiting values (Default:\",\")")
parser.add_argument('-o', '--output-vcf-values-delimeter', dest='output_vcf_values_delimiter', type=str, default='^', help="specify delimiter in output VCF delimiting values in field with annotation (Default:\"^\")")

args = parser.parse_args()


dictionary_path = args.DICTIONARY_PATH
key_column_name = args.KEY_COLUMN_NAME
field_name = args.FIELD_NAME
header = args.HEADER

dictionary_delimiter = args.dictionary_delimiter
input_vcf_keys_delimiter = args.input_vcf_keys_delimiter
output_vcf_values_delimiter = args.output_vcf_values_delimiter

dictionary = {}

value_header = None
line_counter = 0

for line in open(dictionary_path, 'r'):

    if line.startswith('#') and line_counter == 0:
        line_counter += 1
    elif line.startswith('#') and line_counter == 1:
        value_header = line.strip()
        line_counter = 0
    else:

        record = [x.strip() for x in line.split('\t')]

        key = record[0]
        values = [x.strip() for x in record[1].split(dictionary_delimiter)]

        if key not in dictionary.keys():
            dictionary[key] = values
        else:
            dictionary[key] = dictionary[key] + values


for line in stdin:

    if line.startswith('##'):
        print(line.strip())

    elif line.startswith('#C'):

        print(header)
        print(line.strip())
    else:

        line = [x.strip() for x in line.split('\t', 8)]
        INFO = line[7].strip().split(';')

        alleles_annotations = []

        for info in INFO:

            if info.startswith(key_column_name):

                alleles_keys = info[len(key_column_name) + 1:].split(",")

                for allele_key in alleles_keys:

                    keys = allele_key.split(input_vcf_keys_delimiter)
                    allele_annotation = []

                    for key in keys:

                        if key in dictionary.keys():
                            allele_annotation.append(output_vcf_values_delimiter.join(dictionary[key]))
                        else:
                            allele_annotation.append(".")

                    alleles_annotations.append(input_vcf_keys_delimiter.join(allele_annotation))

        if len(set(input_vcf_keys_delimiter.join(alleles_annotations).split(input_vcf_keys_delimiter)).difference({".", ""})) > 0:

            line[7] += ';' + field_name + '=' + ",".join(alleles_annotations)

        print('\t'.join(line))

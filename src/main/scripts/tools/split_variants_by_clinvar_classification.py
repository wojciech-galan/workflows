#!/usr/bin/python3

import argparse
import json
import re

__version__ = '1.0.0'


def run(arguments):
    outputs = [{'id': 'pathogenic', 'path': arguments.output_pathogenic,
                'classification': ['pathogenic', 'pathogenic/likely_pathogenic']},
               {'id': 'likely-pathogenic', 'path': arguments.output_likely_pathogenic,
                'classification': ['likely_pathogenic']},
               {'id': 'others', 'path': arguments.output_others,
                'classification': ['uncertain_significance', 'likely_benign', 'benign/likely_benign', 'benign',
                                   'undefined', 'conflicting_interpretations_of_pathogenicity', 'affects',
                                   'association', 'association_not_found', 'drug_response', 'not_provided',
                                   'other', 'protective', 'risk_factor', 'undefined']}
               ]

    with open(arguments.input, 'r') as input_file:
        variants = json.load(input_file)

    for variant in variants:
        variant["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"] = [variant["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0] \
            if (variant["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"] and variant["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"][0] != 'not_provided') \
            else "undefined"]

    for output in outputs:
        with open(output['path'], 'w') as output_file:
            data = list(filter(lambda variant: variant['ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE'][0] in output['classification'],
                               variants))
            data = sorted(data, key=lambda variant: variant['ISEQ_ACMG_SUMMARY_SCORE'], reverse=True)
            json.dump(data, output_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='The script splits json based on acmg_summary field into single files')
    parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input', type=str,
                        help="path to input variants file (annotated by acmg_summary and converted to json format)")
    parser.add_argument('--output-pathogenic', type=str, required=True,
                        help='Template with .docx extension with jinja2 code')
    parser.add_argument('--output-likely-pathogenic', type=str, required=True,
                        help='Json file containing sample info')
    parser.add_argument('--output-others', type=str, required=True,
                        help='Json file containing pathogenic variants')

    arguments = parser.parse_args()

    run(arguments)

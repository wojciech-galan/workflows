#!/usr/bin/python3

import os, sys
import json
import argparse
import pysam

__version__ = '1.0.0'
AUTHOR="gitlab.com/MateuszMarynowski"

parser = argparse.ArgumentParser(description='Annotate vcf with disease names derived from Genetics Home Reference')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf file')
parser.add_argument('--input-json', '-j', metavar='input_json', type=str, required=True, help='input json file')
parser.add_argument('--output-vcf', '-o', metavar='output_vcf', type=str, required=True, help='output vcf file')
args = parser.parse_args()


def main():

    with open(args.input_json, 'r') as f:
        ghr_summaries_dict = json.load(f)

    gene_symbol = []
    for i in range(len(ghr_summaries_dict["summaries"]["gene-summary"])):
        gene_symbol.append(ghr_summaries_dict["summaries"]["gene-summary"][i]['gene-symbol'])

    mydict = {}
    for i in range(len(ghr_summaries_dict["summaries"]["gene-summary"])):
        try:
            diseases = ghr_summaries_dict["summaries"]["gene-summary"][i]["related-health-condition-list"]["related-health-condition"]["name"]
            diseases = diseases.replace(" ", "_")
        except:
            try:
                diseases = []
                for j in range(len(ghr_summaries_dict["summaries"]["gene-summary"][i]["related-health-condition-list"]["related-health-condition"])):
                    diseases.append(ghr_summaries_dict["summaries"]["gene-summary"][i]["related-health-condition-list"]["related-health-condition"][j]["name"])
                    diseases[j] = diseases[j].replace(" ", "_")
            except:
                diseases = None
        finally:
            mydict[gene_symbol[i]] = diseases

    vcf_reader = pysam.VariantFile(filename=args.input_vcf)
    vcf_reader.header.info.add("ISEQ_DISEASE_GHR", "1", "String",
                              "Genes annotated with disease names derived from Genetics Home Reference")

    vcf_writer = pysam.VariantFile(args.output_vcf, 'w',
                                   header=vcf_reader.header)

    for record in vcf_reader.fetch():
        try:
            if record.info["ISEQ_GENES_NAMES"][0] in mydict:
                if type(mydict[record.info["ISEQ_GENES_NAMES"][0]]) == list:
                    record.info["ISEQ_DISEASE_GHR"] = '^'.join(mydict[record.info["ISEQ_GENES_NAMES"][0]])
                elif type(mydict[record.info["ISEQ_GENES_NAMES"][0]]) == str:
                    record.info["ISEQ_DISEASE_GHR"] = mydict[record.info["ISEQ_GENES_NAMES"][0]]
                else:
                    continue
            else:
                continue
        except:
            continue
        finally:
            vcf_writer.write(record)


if __name__ == "__main__": main()
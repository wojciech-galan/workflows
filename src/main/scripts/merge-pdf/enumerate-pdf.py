#!/usr/bin/python3
__version__ = '0.2.0'


import PyPDF2
from fpdf import FPDF
import argparse


class CustomPDF(FPDF):
    def footer(self):

        self.set_y(-10)
        self.add_font('Muli', '', '/resources/fonts/Muli-VariableFont_wght.ttf', uni=True)
        self.set_font('Muli', '', 9)

        # Add a page number
        page = str(self.page_no()) + '/{nb}'

        self.cell(0, 0, page, 0, 0, 'C')

def create_pdf(pdf_path:[str]):
    pdf = CustomPDF()
    # Create the special value {nb}
    pdf.alias_nb_pages()
    pdf.add_font('Muli', '', '/resources/fonts/Muli-VariableFont_wght.ttf', uni=True)
    pdf.set_font('Muli', '', 9)
    for i in range(number_of_pages):
        pdf.add_page()

    pdf.set_font('Muli', '', 9)
    pdf.output(pdf_path)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='merges pdf files')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--pdf_file', '-f', type=str, nargs=1, help='pdf file for enumaration of pages')
    parser.add_argument('--output', '-o', type=str, nargs=1, help='name of output file' )
    arguments = parser.parse_args()
    pdfFileObj = open(arguments.pdf_file[0], 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

    number_of_pages = pdfReader.numPages
    watermark_file = "page_numbers.pdf"
    create_pdf(watermark_file)




    with open(arguments.pdf_file[0], "rb") as filehandle_input:
        # read content of the original file
        pdf = PyPDF2.PdfFileReader(filehandle_input)

        with open(watermark_file, "rb") as filehandle_watermark:
            # read content of the watermark
            watermark = PyPDF2.PdfFileReader(filehandle_watermark)
            pdf_writer = PyPDF2.PdfFileWriter()

            for i in range(number_of_pages):
                first_page = pdf.getPage(i)
                first_page_watermark = watermark.getPage(i)
                first_page.mergePage(first_page_watermark)
                pdf_writer.addPage(first_page)

            with open(arguments.output[0], "wb") as filehandle_output:
                # write the watermarked file to the new file
                pdf_writer.write(filehandle_output)

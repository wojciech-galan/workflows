#!/usr/bin/python3

import sys
import argparse
import pandas as pd
import os
import numpy as np
import re

__version__ = '1.0.0'

parser = argparse.ArgumentParser(description='Getting basic statistics (paired-end)')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-path-to-fastqc-data-files', '-f', metavar='input_path_to_fastqc_data_files', type=str, required=True, help='path to fastqc data files')
parser.add_argument('--input-path-to-summary-files', '-s', metavar='input_path_to_summary_files', type=str, required=True, help='path to summary files')
parser.add_argument('--output-file-name', '-o', metavar='output_file_name', type=str, required=True, help='output file name')
args = parser.parse_args()

def main():

    fastqc_data_files = np.sort([f for f in os.listdir(args.input_path_to_fastqc_data_files) if f.endswith(".txt")])
    summary_files = np.sort([f for f in os.listdir(args.input_path_to_summary_files) if f.endswith(".txt")])
    pairs_fastqc = np.reshape(fastqc_data_files, (-1, 2))
    pairs_summary = np.reshape(summary_files, (-1, 2))

    numbers = []
    for i in range(len(fastqc_data_files)):
        numbers.append(fastqc_data_files[i].split(".")[0].split("_")[-2])

    os.chdir(args.input_path_to_summary_files)

    summary_info = []
    with open(summary_files[0]) as f:
        for line in f:
            summary_info.append(line.split("\t")[1])

    words = ["Total Sequences", "Sequences flagged as poor quality", "Sequence length", "%GC"]
    all_info = words + summary_info

    all_info_with_underscore = []
    for i in range(len(all_info)):
        all_info_with_underscore.append(re.sub("\s+", "_", all_info[i].strip()))

    column_names = []
    for j in range(len(all_info)):
        column_names.append(all_info_with_underscore[j].split("\t")[0] + "_" + numbers[0])
        column_names.append(all_info_with_underscore[j].split("\t")[0] + "_" + numbers[1])

    df = pd.DataFrame([])
    for i in range(len(pairs_fastqc)):
        os.chdir("../")
        os.chdir(args.input_path_to_fastqc_data_files)
        with open(pairs_fastqc[i, 0]) as f1, open(pairs_fastqc[i, 1]) as f2:
            lines_1 = f1.readlines()
            values_f1 = []
            for line in lines_1:
                for word in words:
                    if word in line:
                        values_f1.append(line.split()[-1])

            lines_2 = f2.readlines()
            values_f2 = []
            for line in lines_2:
                for word in words:
                    if word in line:
                        values_f2.append(line.split()[-1])

            values_fastqc_file = []
            for j in range(len(values_f1)):
                values_fastqc_file.append(values_f1[j])
                values_fastqc_file.append(values_f2[j])

        os.chdir("../")
        os.chdir(args.input_path_to_summary_files)
        with open(pairs_summary[i, 0]) as f3, open(pairs_summary[i, 1]) as f4:
            values_f3 = []
            for line in f3:
                values_f3.append(line.split("\t")[0])

            values_f4 = []
            for line in f4:
                values_f4.append(line.split("\t")[0])

            values_summary_file = []
            for g in range(len(values_f3)):
                values_summary_file.append(values_f3[g])
                values_summary_file.append(values_f4[g])

            values = values_fastqc_file + values_summary_file

            data = pd.DataFrame(values).T
            data.columns = column_names
            data.rename(index={0: pairs_fastqc[i, 0].split("_" + numbers[0] + "_fastqc")[0]}, inplace=True)
            df = df.append(data, ignore_index=False)
    df.index.name = "Sample_name"
    df.to_excel(args.output_file_name + ".xlsx")
    df.to_csv(args.output_file_name + ".csv")
    
if __name__ == "__main__": main()


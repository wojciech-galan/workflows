#!/usr/bin/python3

import sys
import argparse
import pandas as pd
import os

__version__ = '1.0.0'

parser = argparse.ArgumentParser(description='Concat summary files from hisat2')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('--input-path-to-summary-files', '-i', metavar='input_path_to_summary_files', type=str, required=True, help='path to summary files')
parser.add_argument('--output-summary-file-name', '-o', metavar='output_summary_file_name', type=str, required=True, help='output summary file name')
args = parser.parse_args()

def concat_summary_files(files_path):
    os.chdir(files_path)
    text_files = [f for f in os.listdir(files_path) if f.endswith('.txt')]
    columns = ["reads", "were paired", "aligned concordantly 0 times", "aligned concordantly exactly 1 time",
                "aligned concordantly >1 times", "----", "pairs aligned concordantly 0 times", "aligned discordantly 1 time",
                "----", "pairs aligned 0 times concordantly or discordantly", "mates make up the pairs", "aligned 0 times",
                "aligned exactly 1 time", "aligned >1 times", "overall alignment rate"]
    concatenate_summary_files = pd.concat([pd.read_csv(item, names=[item[:-4]]) for item in text_files], axis = 1).transpose()
    concatenate_summary_files.columns = columns
    for i in range(concatenate_summary_files.shape[0]):
        for j in range(concatenate_summary_files.shape[1]):
            concatenate_summary_files.iloc[i,j] = concatenate_summary_files.iloc[i,j].split(" " + columns[j])[0]
    concatenate_summary_files = concatenate_summary_files.apply(lambda x: x.str.strip())
    concatenate_summary_files.to_excel(args.output_summary_file_name + ".xlsx")
    concatenate_summary_files.to_csv(args.output_summary_file_name + ".csv")
    
def main():
    data_path = os.path.abspath('')
    files_path = os.path.join(data_path, args.input_path_to_summary_files)
    concat_summary_files(files_path)
    
if __name__ == "__main__": main()


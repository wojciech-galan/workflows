## SCRIPTS

Links in table of contents below redirects to GitLab pages corresponding to those sub-directories.

---

* **Table of contents**
  * [acmg](./acmg/readme.md/readme.md): scripts needed to annotate VCF with ACMG score
  * [bco](./bco/readme.md)
  * [reports](./reports/readme.md)
  * [resources-tools](./resources-tools/readme.md): scripts needed to create [resources](./../../../resources/readme.md)
  * [test-file-creation-tools](./test-files-creation-tools/readme.md)
  * [tools](./tools/readme.md): scripts that perform processing of VCF, FASTQ and other typical formats containing genetic data.
  * [util](./util/readme.md)
  * [variant-descriptor](./variant-descriptor/readme.md)
  * [wdltools](./wdltools/readme.md)

---

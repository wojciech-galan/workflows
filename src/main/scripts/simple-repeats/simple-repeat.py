import pysam
import argparse
from typing import Tuple
from typing import Union

__version__ = '0.0.2'


def check_if_present(ucsc_simple_repeat:pysam.TabixFile, chrom:str, pos1:int) -> bool:
    pos = pos1 - 1  # variant position changed to 0-based
    lower_boundary = max([pos - 2000, 1])
    upper_boundary = pos + 1
    try:

        ucsc_simple_repeat.fetch(chrom, lower_boundary, upper_boundary)
    except ValueError:
        return False
    else:
        for row in ucsc_simple_repeat.fetch(chrom, lower_boundary, upper_boundary):
            line = str(row).split("\t")
            start, end = int(line[1]), int(line[2])
            if (pos >= start) and (pos < end):
                return True


def get_variant_location(record:pysam.VariantRecord) -> Tuple[Union[str, int]]:
    # line = str(record).split('\t')
    # return line[0], int(line[1])
    return record.contig, int(record.pos)


if __name__ == '__main__':

    parser = argparse.ArgumentParser("Adds annotation for variants located in UCSC SimpleRepeat regions")
    parser.add_argument('--input', '-i', type=str, required=True, help='Input vcf.gz file, indexed with tabix')
    parser.add_argument('--repeats', '-r', type=str, required=True,
                        help='Simple repeat ucsc bed file, indexed with tabix')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()

    with pysam.TabixFile(args.repeats, "r") as ucsc_simple_repeat, pysam.VariantFile(args.input, "r") as input_vcf:

        items = [('ID', "ISEQ_SIMPLE_REPEAT"),
                 ('Number', "1"),
                 ('Type', "String"),
                 ('Description', "Annotation added for variants in UCSC Simple Repeat region"),
                 ('Source', 'UCSC SimpleRepeat'),
                 ('Version', '28-11-2019')]

        input_vcf.header.add_meta("INFO", items=items)

        with pysam.VariantFile('-', 'w', header=input_vcf.header) as vcf_writer:

            for record in input_vcf.fetch():
                chromosome, pos = get_variant_location(record)
                result = check_if_present(ucsc_simple_repeat, chromosome, pos)
                if result:
                    record.info["ISEQ_SIMPLE_REPEAT"] = "inSimpleRepeat"
                vcf_writer.write(record)

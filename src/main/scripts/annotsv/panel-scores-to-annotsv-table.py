#!/usr/bin/python3

__version__ = '0.0.1'

import argparse
import json
import gzip


def prepare_panel(panel_json):
    with open(panel_json, "r") as f:
        panel = json.load(f)
        pheno_panel = [i for i in panel if i["type"] != "user"]
        sorted_pheno_panel = sorted(pheno_panel, key=lambda i: i["score"], reverse=True)
        panel_dict = {}
        for i in sorted_pheno_panel:
            gene, score, source = i["name"], round(i["score"], 2), i["type"]
            if gene not in panel_dict:
                panel_dict[gene] = [score, source]
    return panel_dict


def get_indexes(first_line, name):
    header_list = first_line.split("\t")
    return header_list.index(name)  ## needed for two fields: "Gene_name" and "Annotation_mode"


def find_common_genes(var_genes, panel):
    return list(set(var_genes).intersection(panel.keys()))


def write_score_field(common_part_genes, panel):
    if bool(common_part_genes):
        return str(max([panel[i][0] for i in common_part_genes]))
    else:
        return "0"


def write_type_field_for_split_line(common_part_genes, panel):
    if bool(common_part_genes):
        return panel[common_part_genes[0]][1]
    else:
        return ""


def analyze_data_line(line, panel_dict, line_type_index, gene_index):
    split_line = line.split("\t")
    sv_genes = split_line[gene_index].strip().split(";")
    common_genes = find_common_genes(sv_genes, panel_dict)
    gene_field = ";".join(common_genes)
    # print(gene_field)
    score_field = write_score_field(common_genes, panel_dict)
    if split_line[line_type_index].strip() == "full":
        type_field = ""
    else:
        type_field = write_type_field_for_split_line(common_genes, panel_dict)
    return gene_field, score_field, type_field


def check_if_compress(filename):
    return filename.endswith(".gz")


def pick_reading_function(filename):
    if filename.endswith(".gz"):
        return gzip.open(filename, 'rt')
    else:
        return open(filename, "r")


def pick_writing_function(filename, compress):
    if compress:
        return gzip.open(filename, 'wb')
    else:
        return open(filename, "w")


def write_new_line(line, field1, field2, field3, compress):
    text = "\t".join([line.strip("\n"), field1, field2, field3]) + "\n"
    if compress:
        return text.encode()
    else:
        return text


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=
                                     '''
                                     Uses gene panel to add information on gene <=> phenotype score to AnnotSV output table.
                                     Only panel records of type phenotype and disease are used. 
                                                                                  
                                      ''')
    parser.add_argument('--annotsv-table', '-i', metavar='annotsv_table', type=str, help='AnnotSV output table')
    parser.add_argument('--output-table', '-o', metavar='output_table', type=str,
                        help='Output tsv file, add .gz prefix if would like to obtain compressed fileq')
    parser.add_argument('--gene-panel', '-p', metavar='gene_panel', type=str, required=False,
                        help='Gene panel json (output from panel-generate task)')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))

    args = parser.parse_args()

    if bool(args.gene_panel):
        panel_dictionary = prepare_panel(args.gene_panel)
    else:
        panel_dictionary = {}

    compress_output = check_if_compress(args.output_table)

    with pick_reading_function(args.annotsv_table) as table, pick_writing_function(args.output_table,
                                                                                   compress_output) as new_table:
        for line in table:
            # print(line)
            if line.startswith("AnnotSV_ID"):
                gene_column_index = get_indexes(line, "Gene_name")
                annotype_column_index = get_indexes(line, "Annotation_mode")
                new_table.write(write_new_line(line, "Panel_genes", "Panel_score", "Panel_type", compress_output))
            else:
                gene_field, score_field, type_field = analyze_data_line(line, panel_dictionary, annotype_column_index,
                                                                        gene_column_index)
                new_table.write(write_new_line(line, gene_field, score_field, type_field, compress_output))

    ## flow: read and filter panel.json, read annotsv table
    ## first table line: find indexes of gene and annotation type fields, write line + names of three panel fields (gene, score, type)
    ## other lines: check interection, find values from panel, write panel fields
    ## end

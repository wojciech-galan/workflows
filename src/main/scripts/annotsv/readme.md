### ACMG ranking (modified AnnotSV v.3.0)  
An original AnnotSV algorithm is described [here](https://www.lbgi.fr/AnnotSV/ranking)  
   
ISEQ changes:  
 * AnnotSV OMIM morbid gene list was replaced with list containing genes for which gnomAD oe_lof upper confidence interval is below 0.35. This was done because AnnotSV treats genes from this list as ESTABLISHED haploinsufficient genes (among with genes  which have ClinGene TS of 3). Description was updated in the final VCF and TSV files.
 * AnnotSV OMIM morbid gene candidates list was replaced with list containing names of OMIM morbid genes. This list is not used for pathogenicity evaluation. Description was updated in the final VCF and TSV files.  
 * Simplified pathogenicity evaluation was performed for other than GAIN and LOSS variants.
 * Annotation is based on modified Ensembl gtf file (protein coding genes only).
 * Custom "best" transcript list is provided in docker (based on the algorithm implemented in the biomart-table-maker.py script) 
 * Exomiser was removed (ISEQ gene panel based on phenotype description, HPO terms and diseases was used instead). 

| category      | description                                                                                                                                                                                                                                                                                                                              | score | ISEQ modifications                                                                                                                                                       |
|:-------------:|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----:|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **LOSS**      |                                                                                                                                                                                                                                                                                                                                          |       |                                                                                                                                                                          |
| 1A            | Contains protein-coding or other known functionally important elements.                                                                                                                                                                                                                                                                  | 0     |                                                                                                                                                                          |
| 1B            | Does not contain protein-coding or other known functionally important elements.                                                                                                                                                                                                                                                          | -0.6  |                                                                                                                                                                          |
| 2A            | Complete overlap of a known pathogenic loss SV.                                                                                                                                                                                                                                                                                          | 1     |                                                                                                                                                                          |
| 2B            | Partial overlap of a known pathogenic loss SV and a)  the observed CNV does not contain a haploinsufficient gene or b) unclear if known causative gene or critical region is affected or c) no specific causative gene or critical region has been established for this known pathogenic Loss SV                                         | 0     |                                                                                                                                                                          |
| 2C -1         | Partial overlap with the 5’ end of an established HI gene (3’ end of the gene not involved) and coding sequence is involved                                                                                                                                                                                                              | 0.9   |                                                                                                                                                                          |
| 2C-2          | Partial overlap with the 5’ end of an established HI gene (3’ end of the gene not involved) and only the 5’ UTR is involved                                                                                                                                                                                                              | 0     |                                                                                                                                                                          |
| 2D-1          | Partial overlap with the 3’ end of an established HI gene (5’ end of the gene not involved) and only the 3’ untranslated region is involved                                                                                                                                                                                              | 0     |                                                                                                                                                                          |
| 2D-2          | Partial overlap with the 3’ end of an established HI gene  (5’ end of the gene not involved) and only the last exon is involved. Other established pathogenic snv/indel have been reported in the observed CNV                                                                                                                           | 0.9   |                                                                                                                                                                          |
| 2D-3          | Partial overlap with the 3’ end of an established HI gene (5’ end of the gene not involved) and only the last exon is involved. No other established pathogenic variants have been reported in the observed CNV.                                                                                                                         | 0.45  |                                                                                                                                                                          |
| 2D-4          | Partial overlap with the 3’ end of an established HI gene  (5’ end of the gene not involved) and it includes other exons in addition to the last exon. Nonsense-mediated decay is expected to occur.                                                                                                                                     | 0.9   |                                                                                                                                                                          |
| 2E-1          | Both breakpoints are within the same HI gene (intragenic CNV; gene-level sequence variant) and variant disrupts the reading frame                                                                                                                                                                                                        | 0.9   |                                                                                                                                                                          |
| 2E-2          | Both breakpoints are within the same HI gene (intragenic CNV; gene-level sequence variant) and  at least one exon is deleted and other established pathogenic snv/indel have been reported in the observed CNV and variant removes >= 10% of protein                                                                                     | 0.45  |                                                                                                                                                                          |
| 2E-3          | Both breakpoints are within the same HI gene  (intragenic CNV; gene-level sequence variant) and at least 1 exon deleted and other established pathogenic snv/indel have been reported in the observed CNV and variant removes < 10% of protein                                                                                           | 0.3   |                                                                                                                                                                          |
| 2E-4          | Both breakpoints are within the same HI gene (intragenic CNV; gene-level sequence variant) and at least one  exon deleted and no established pathogenic snv/indel have been reported in the observed CNV and variant removes > 10% of protein                                                                                            | 0.2   |                                                                                                                                                                          |
| 2F            | Completely contained within an established benign CNV region.                                                                                                                                                                                                                                                                            | -1    |                                                                                                                                                                          |
| 2G            | Overlaps an established benign CNV, but includes additional genomic material.                                                                                                                                                                                                                                                            | 0     |                                                                                                                                                                          |
| 2H            | Two or more HI predictors suggest that at least one gene in the interval is haploinsufficient (gnomAD pLI >=0.9 and DECIPHER HI index <=10%)                                                                                                                                                                                             | 0.15  |                                                                                                                                                                          |
| 3A            | Number of protein-coding genes wholly or partially included in the copy-number loss : 0-24                                                                                                                                                                                                                                               | 0     |                                                                                                                                                                          |
| 3B            | Number of protein-coding genes wholly or partially included in the copy-number loss: 25-34                                                                                                                                                                                                                                               | 0.45  |                                                                                                                                                                          |
| 3C            | Number of protein-coding genes wholly or partially included in the copy-number loss: 35 or more                                                                                                                                                                                                                                          | 0.9   |                                                                                                                                                                          |
| 5F            | Inheritance information is unavailable or uninformative.                                                                                                                                                                                                                                                                                 | 0     | Panel_score for all genes< 30                                                                                                                                            |
| 5G            | Inheritance information is unavailable or uninformative. The patient phenotype is nonspecific, but is consistent with what has been described in similar cases (panel score >= 30).                                                                                                                                                      | 0.1   | Panel_score for any gene >=30 (and less than 45)                                                                                                                         |
| 5H            | Inheritance information is unavailable or uninformative. The patient phenotype is highly specific and consistent with what has been described in similar cases (panel score >= 45).                                                                                                                                                     | 0.3   | panel score for any gene >= 45                                                                                                                                           |
| **GAIN**      |                                                                                                                                                                                                                                                                                                                                          |       |                                                                                                                                                                          |
| 1A            | Contains protein-coding or other known functionally important elements.                                                                                                                                                                                                                                                                  | 0     |                                                                                                                                                                          |
| 1B            | Does not contain protein-coding or any known functionally important elements.                                                                                                                                                                                                                                                            | -0.6  |                                                                                                                                                                          |
| 2A            | The known pathogenic gain SV or known TS gene is fully contained within the observed copy-number gain variant.                                                                                                                                                                                                                           | 1     | ClinGene TS score for was checked for all genes fully contained within the variant. Based on TS==3 and ‘txStart-txEnd’ in Location. Performed only for not 2A variants. |
| 2B            | Partial overlap of a known pathogenic gain SV and a) the observed CNV does not contain the TS gene or critical region for this known pathogenic gain SV or b) unclear if the known causative gene or critical region is affected or c) no specific causative gene or critical region has been established for this known pathogenic SV. | 0     |                                                                                                                                                                          |
| 2C            | Identical in gene content to the established benign copy-number gain variant.                                                                                                                                                                                                                                                            | -1    |                                                                                                                                                                          |
| 2D            | Smaller than established benign copy-number gain, breakpoint(s) does not interrupt protein-coding genes.                                                                                                                                                                                                                                 | -1    |                                                                                                                                                                          |
| 2E            | Smaller than established benign copy-number gain, breakpoint(s) potentially interrupts protein-coding gene.                                                                                                                                                                                                                              | 0     |                                                                                                                                                                          |
| 2F            | Larger than known benign copy-number gain, does not include additional protein-coding genes.                                                                                                                                                                                                                                             | -1    |                                                                                                                                                                          |
| 2G            | Overlaps a benign copy-number gain but includes additional genomic material.                                                                                                                                                                                                                                                             | 0     |                                                                                                                                                                          |
| 2H-1          | HI gene fully contained within observed copy-number gain and patient phenotype is highly specific and consistent with what has been described for this HI gene (panel score >= 45).                                                                                                                                                      | 0.45  | Genes outputed by AnnotSV for the 2H-2 were checked for panel match. If needed, variant was moved to the 2H-1 category and final score and rank was updated.             |
| 2H-2          | Overlap with HI gene and patient phenotype is either inconsistent with what has been described for this HI gene (panel score < 45) or unknown.                                                                                                                                                                                           | 0     | Genes outputed by AnnotSV for the 2H-2 were checked for panel match. If needed, variant was moved to the 2H-1 category and final score and rank was updated.             |
| 2I-1          | Both breakpoints are within the same HI gene  (gene-level sequence variant, possibly resulting in loss of function [LOF]) and disrupts the reading frame.                                                                                                                                                                                | 0.45  |                                                                                                                                                                          |
| 2I-2          | Both breakpoints are within the same HI gene (gene-level sequence variant, possibly resulting in loss of function [LOF]) and patient phenotype is highly specific and consistent with what has been described for this HI gene / morbid gene (panel score >= 45).                                                                        | 0.45  | Genes outputed by AnnotSV for the 2I-3 were checked for panel match. If needed, variant was moved to the 2I-2 category and final score and rank was updated.             |
| 2I-3          | Both breakpoints are within the same HI gene  (gene-level sequence variant, possibly resulting in loss of function [LOF]) and patient phenotype is either inconsistent with what has been described for this HI gene (panel score < 45) OR unknown.                                                                                      | 0     | Genes outputed by AnnotSV for the 2I-3 were checked for panel match. If needed, variant was moved to the 2I-2 category and final score and rank was updated.             |
| 2J            | One breakpoint is within an established HI gene, patient phenotype is either inconsistent with what has been described for this HI gene (panel score < 45 or unknown).                                                                                                                                                                   | 0     | Genes outputed by AnnotSV for the 2J were checked for panel match. If needed, variant was moved to the 2K category and final score and rank was updated.                 |
| 2K            | One breakpoint is within an established HI gene , patient phenotype is highly specific and consistent with what has been described for this HI gene (panel score >= 45).                                                                                                                                                                 | 0.45  | Genes outputed by AnnotSV for the 2J were checked for panel match. If needed, variant was moved to the 2K category and final score and rank was updated.                 |
| 2L            | One or both breakpoints are within gene(s) of no established clinical significance.                                                                                                                                                                                                                                                      | 0     |                                                                                                                                                                          |
| 3A            | Number of protein-coding genes wholly or partially included in the copy-number gain: 0-34                                                                                                                                                                                                                                                | 0     |                                                                                                                                                                          |
| 3B            | Number of protein-coding genes wholly or partially included in the copy-number gain: 35-49                                                                                                                                                                                                                                               | 0.45  |                                                                                                                                                                          |
| 3C            | Number of protein-coding genes wholly or partially included in the copy-number gain: 50 or more                                                                                                                                                                                                                                          | 0.9   |                                                                                                                                                                          |
| 5F            | Inheritance information is unavailable or uninformative.                                                                                                                                                                                                                                                                                 | 0     | 5F category is updated (if needed) to 5G or 5H category. Based on maximal Panel_score                                                                                    |
| 5G            | Inheritance information is unavailable or uninformative. The patient phenotype is nonspecific, but is consistent with what has been described in similar cases (panel score => 30).                                                                                                                                                      | 0.1   | 5F category is updated (if needed) to 5G or 5H category. Based on maximal Panel_score                                                                                    |
| 5H            | Inheritance information is unavailable or uninformative. The patient phenotype is highly specific and consistent with what has been described in similar cases (panel score > = 45).                                                                                                                                                     | 0.15  | 5F category is updated (if needed) to 5G or 5H category. Based on maximal Panel_score                                                                                    |
| **INVERSION** |                                                                                                                                                                                                                                                                                                                                          |       |                                                                                                                                                                          |
| 2I            | At least one breakpoint is within HI gene, possibly resulting in loss of function [LOF])                                                                                                                                                                                                                                                 | 0.45  |  For all genes: 1) HI test: HI ==3 or gnomAD_LoF_intolerant == ”yes” and 2) “exon” in Location and 3) Overlapped_CDS_length > 0                                          |
| 2J            | At least one breakpoint is within an established HI gene, patient phenotype is either inconsistent with what has been described for this HI gene (panel score < 45 or unknown).                                                                                                                                                          | 0     |  For all genes: 1) HI test: HI ==3 or gnomAD_LoF_intolerant == ”yes” and 2) Location != “txStart-txEnd” and 3) Panel_score < 45                                          |
| 2K            | One or both breakpoints is within an established HI gene , patient phenotype is highly specific and consistent with what has been described for this HI gene (panel score >= 45).                                                                                                                                                        | 0.45  |  For all genes: 1) HI test: HI ==3 or gnomAD_LoF_intolerant == ”yes” and 2) Location != “txStart-txEnd” and 3) Panel_score >= 45                                         |
| 2L            | One or both breakpoints are within gene(s) of no established clinical significance.                                                                                                                                                                                                                                                      | 0     | For all genes: 1) failed HI test: HI ==3 or gnomAD_LoF_intolerant == ”yes” and 2) Location != “txStart-txEnd”                                                            |
| 2M            | Both breakpoints outside any known gene                                                                                                                                                                                                                                                                                                  | -0.6  | Empty breakpoint gene list (failed 2I-2L tests)                                                                                                                          |
| 5F            | Inheritance information is unavailable or uninformative.                                                                                                                                                                                                                                                                                 | 0     | Panel_score for all breakpoint genes< 30                                                                                                                                 |
| 5G            | Inheritance information is unavailable or uninformative. The patient phenotype is nonspecific, but is consistent with what has been described in similar cases (panel score >= 30).                                                                                                                                                      | 0.1   | Panel_score for any breakpoint gene >=30 (and less then 45)                                                                                                              |
| 5H            | Inheritance information is unavailable or uninformative. The patient phenotype is highly specific and consistent with what has been described in similar cases (panel score >= 45).                                                                                                                                                     | 0.15  | panel score for any breakpoint gene >= 45                                                                                                                                |
| **ALL OTHER** |                                                                                                                                                                                                                                                                                                                                          |       |                                                                                                                                                                          |
| 2I            | At least one breakpoint is within HI gene , possibly resulting in loss of function [LOF])                                                                                                                                                                                                                                                | 0.45  |  For all genes: 1) HI test: HI ==3 or gnomAD_LoF_intolerant == ”yes” and 2) “exon” in Location                                                                           |
| 2J            | At least one breakpoint is within an established HI gene, patient phenotype is either inconsistent with what has been described for this HI gene (panel score < 60 or unknown).                                                                                                                                                          | 0     |  For all genes: 1) HI test: HI ==3 or gnomAD_LoF_intolerant == ”yes” and Panel_score < 45                                                                                |
| 2K            | One or both breakpoints is within an established HI gene , patient phenotype is highly specific and consistent with what has been described for this HI gene (panel score >= 45).                                                                                                                                                        | 0.45  |  For all genes: 1) HI test: HI ==3 or gnomAD_LoF_intolerant == ”yes” and Panel_score >= 45                                                                               |
| 2L            | One or both breakpoints are within gene(s) of no established clinical significance.                                                                                                                                                                                                                                                      | 0     | All genes fail HI test and breakpoint gene list not empty                                                                                                                |
| 2M            | Both breakpoints outside any known gene                                                                                                                                                                                                                                                                                                  | -0.6  | Empty breakpoint gene list (failed 2I-2L tests)                                                                                                                          |
| 5F            | Inheritance information is unavailable or uninformative.                                                                                                                                                                                                                                                                                 | 0     | Panel_score for all genes< 30                                                                                                                                            |
| 5G            | Inheritance information is unavailable or uninformative. The patient phenotype is nonspecific, but is consistent with what has been described in similar cases (panel score >= 30).                                                                                                                                                      | 0.1   | Panel_score for any gene >=30 (and less then 45)                                                                                                                         |
| 5H            | Inheritance information is unavailable or uninformative. The patient phenotype is highly specific and consistent with what has been described in similar cases (panel score >= 45).                                                                                                                                                     | 0.15  | panel score for any gene >= 45                                                                                                                                           |


**Scoring**: 
* pathogenic 0.99 or more points  
* likely pathogenic 0.90 to 0.98 points  
* variant of uncertain significance 0.89 to -0.89 points  
* likely benign -0.90 to -0.98 points  
* benign -0.99 or fewer points  

   
***
*******

### Scripts in this directory   
* [Panel scores to AnnotSV table (.py)](#panel-scores-to-annotsv-table)  
* [Change AnnotSV ranking (.py)](#change-annotsv-ranking)  
* [AnnotSV TSV to VCF (.py)](#annotsv-tsv-to-vcf)  


___
### 

**Author**:  Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**: [**panel-scores-to-annotsv-table.py 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/annotsv/panel-scores-to-annotsv-table.py)

```bash
usage: panel-scores-to-annotsv-table.py [-h] [--annotsv-table annotsv_table]
                                        [--output-table output_table]
                                        [--gene-panel gene_panel] [-v]

                                     Uses gene panel to add information on gene <=> phenotype score to AnnotSV output table.
                                     Only panel records of type phenotype and disease are used. 
                                                                                  
                                      

optional arguments:
  -h, --help            show this help message and exit
  --annotsv-table annotsv_table, -i annotsv_table
                        AnnotSV output table
  --output-table output_table, -o output_table
                        Output tsv file, add .gz prefix if would like to
                        obtain compressed fileq
  --gene-panel gene_panel, -p gene_panel
                        Gene panel json (output from panel-generate task)
  -v, --version         show program's version number and exit


```

[Return to the table of contents](#table-of-contents)

****
**Author**:  Katarzyna Tomala (https://gitlab.com/kattom)

**Language**: python3

**Current version**: [**change-annotsv-ranking.py 0.1.0**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/annotsv/change-annotsv-ranking.py)

```bash
usage: change-annotsv-ranking.py [-h] [-v] -i INPUT_ANNOTSV -o OUTPUT_SHORT -f
                                 OUTPUT_FULL [-t PANEL_THRESHOLD1]
                                 [-T PANEL_THRESHOLD2]

Changes AnnotSV ranking, modifies format of the output tsv file with panel
info added

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -i INPUT_ANNOTSV, --input_annotsv INPUT_ANNOTSV
                        Path to input AnnotSV output tsv file.
  -o OUTPUT_SHORT, --output_short OUTPUT_SHORT
                        Path to output file, this file contains only "full"
                        rows.
  -f OUTPUT_FULL, --output_full OUTPUT_FULL
                        Path to output file, this file contains all data
  -t PANEL_THRESHOLD1, --panel_threshold1 PANEL_THRESHOLD1
                        Defines threshold score for relatively good gene to
                        phenotype match. Default: 30.0
  -T PANEL_THRESHOLD2, --panel_threshold2 PANEL_THRESHOLD2
                        Defines threshold score for very good gene to
                        phenotype match. Default: 50.0

```

[Return to the table of contents](#table-of-contents)

****
**Author**:  Katarzyna Tomala (https://gitlab.com/kattom), Katarzyna Kolanek (https://gitlab.com/lltw)

**Language**: python3

**Current version**: [**annotsv-tsv-to-vcf.py 2.0.0**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/annotsv/annotsv-tsv-to-vcf.py)

```bash
usage: annotsv-tsv-to-vcf.py [-h] [-v] [-i INPUT_VCF] [-t ANNOTSV_TABLE]
                             [-d DESCRIPTIONS] [-o OUTPUT_VCF]

    Convert the ISEQ modified short TSV output from AnnotSV 3.0 (for GRCh38) to VCF file.

    This script requires a TSV file containing the description of AnnotSV annotations to include in the output VCF file (--descriptions). 
    This file should start with a header with the following columns:

    (1)   Name: "TSV field name"
          Contents: the name of column in the TSV file produced by AnnotSV (modified by change-annotsv-ranking.py).

    (2-6) Names: "ID", "Number", "Type", "Description", "Source", "Version"
          Contents: the description of the contents of a TSV field converted to VCF format, compatible with VCF v4.3 specification.

    (7)   Name: "Python function"
          Contents: the name of the method (from class AnnotationDescription in this script) that should be used to convert the contents of TSV field into contents of VCF INFO field.
    

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -i INPUT_VCF, --input-vcf INPUT_VCF
                        path to the VCF file that was used as an input to
                        AnnotSV
  -t ANNOTSV_TABLE, --annotsv-table ANNOTSV_TABLE
                        path to the short AnnotSV table (full lines only)
  -d DESCRIPTIONS, --descriptions DESCRIPTIONS
                        path to a TSV file containing the description of
                        AnnotSV annotations that will be included in an output
                        VCF
  -o OUTPUT_VCF, --output-vcf OUTPUT_VCF
                        path to the output vcf (with AnnotSV annotations
                        added)


```

[Return to the table of contents](#table-of-contents)
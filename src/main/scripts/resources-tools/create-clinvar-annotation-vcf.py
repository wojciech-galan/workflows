#!/usr/bin/env python3

__version__ = '2.0.1'

import argparse
import datetime
import gzip
from collections import namedtuple
from typing import Dict, List

from pysam import VariantFile

# https://www.ncbi.nlm.nih.gov/clinvar/docs/review_status/
REVIEW_STATUS_TO_GOLD_STARS_COUNT_DICTIONARY = {'practice_guideline': '4',
                                                'reviewed_by_expert_panel': '3',
                                                'criteria_provided,_multiple_submitters,_no_conflicts': '2',
                                                'criteria_provided,_conflicting_interpretations': '1',
                                                'criteria_provided,_single_submitter': '1',
                                                'no_assertion_for_the_individual_variant': '0',
                                                'no_assertion_criteria_provided': '0',
                                                'no_assertion_provided': '0'}


class AnnotationInfo(object):

    def __init__(self, info_id: str, info_number: str, info_type: str, info_description: str, info_source=None,
                 info_version=None, source_field_id=None):
        super().__init__()
        self.info_id = info_id
        self.info_number = info_number
        self.info_type = info_type
        self.info_description = info_description
        self.info_source = info_source
        self.info_version = info_version
        self.source_field_id = source_field_id

    def get_header_info_line(self):
        info_source = '' if None else ',Source="{}"'.format(self.info_source)
        info_version = '' if None else ',Version="{}"'.format(self.info_version)

        return '##INFO=<ID={},Number={},Type={},Description="{}"{}{}>'.format(self.info_id, self.info_number,
                                                                              self.info_type,
                                                                              self.info_description, info_source,
                                                                              info_version)


class _Annotations(object):
    AnnotationsList: List[AnnotationInfo] = list()

    ISEQ_CLINVAR_ALLELE_ID = AnnotationInfo(
        info_id='ISEQ_CLINVAR_ALLELE_ID',
        info_number='A',
        info_type='String',
        info_description='ClinVar Allele ID',
        info_source='ClinVar',
        source_field_id='ALLELEID')
    AnnotationsList.append(ISEQ_CLINVAR_ALLELE_ID)

    ISEQ_CLINVAR_VARIATION_ID = AnnotationInfo(
        info_id='ISEQ_CLINVAR_VARIATION_ID',
        info_number='A',
        info_type='String',
        info_description='ClinVar Variation ID of a variation consisted of the variant alone (Type of variation: Variant).',
        info_source='ClinVar')
    AnnotationsList.append(ISEQ_CLINVAR_VARIATION_ID)

    ISEQ_CLINVAR_VARIANT_TYPE = AnnotationInfo(
        info_id='ISEQ_CLINVAR_VARIANT_TYPE',
        info_number='A',
        info_type='String',
        info_description='ClinVar variant type. Possible values: single_nucleotide_variant, indel, deletion, insertion, '
                         'duplication, inversion, copy_number_gain, copy_number_loss, microsatellite, variation.',
        info_source='ClinVar',
        source_field_id='CLNVC')
    AnnotationsList.append(ISEQ_CLINVAR_VARIANT_TYPE)

    ISEQ_VARIANT_CLINVAR_DISEASES = AnnotationInfo(
        info_id='ISEQ_VARIANT_CLINVAR_DISEASES',
        info_number='A',
        info_type='String',
        info_description='Diseases associated with the variant in ClinVar. Format: disease_name : (...) : disease_name.',
        info_source='ClinVar',
        source_field_id='CLNDN')
    AnnotationsList.append(ISEQ_VARIANT_CLINVAR_DISEASES)

    ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE = AnnotationInfo(
        info_id='ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE',
        info_number='A',
        info_type='String',
        info_description='The overall interpretation of clinical significance of a variation based on aggregating data '
                         'from submitters. Format: clinical_significance : (...) : clinical_significance.',
        info_source='ClinVar',
        source_field_id='CLNSIG')
    AnnotationsList.append(ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE)

    ISEQ_CLINVAR_SIGNIFICANCE = AnnotationInfo(
        info_id='ISEQ_CLINVAR_SIGNIFICANCE',
        info_number='A',
        info_type='String',
        info_description='List of clinical significances of a variation gathered from ClinVar submissions. '
                         'Taken from tab-file  variant_summary.txt.gz. Format: clinical_significance : (...) : clinical_significance.',
        info_source='ClinVar')
    AnnotationsList.append(ISEQ_CLINVAR_SIGNIFICANCE)

    ISEQ_CLINVAR_DATE_LAST_EVALUATED = AnnotationInfo(
        info_id='ISEQ_CLINVAR_DATE_LAST_EVALUATED',
        info_number='A',
        info_type='String',
        info_description='List of the last dates the variation-condition relationship last evaluated by the submitter. '
                         'Taken from tab-file variant_summary.txt.gz. '
                         'The order corresponds to the order in ISEQ_CLINVAR_SIGNIFICANCE. Format: date : (...) : date.',
        info_source='ClinVar')
    AnnotationsList.append(ISEQ_CLINVAR_DATE_LAST_EVALUATED)

    ISEQ_CLINVAR_COLLECTION_METHOD = AnnotationInfo(
        info_id='ISEQ_CLINVAR_COLLECTION_METHOD',
        info_number='A',
        info_type='String',
        info_description='List of the methods by which the submittera obtained the information provided. '
                         'Taken from tab-file variant_summary.txt.gz. '
                         'The order corresponds to the order in ISEQ_CLINVAR_SIGNIFICANCE. '
                         'Format: collection_method : (...) : collection_method.',
        info_source='ClinVar')
    AnnotationsList.append(ISEQ_CLINVAR_COLLECTION_METHOD)

    ISEQ_CLINVAR_REVIEW_STATUS = AnnotationInfo(
        info_id='ISEQ_CLINVAR_REVIEW_STATUS',
        info_number='A',
        info_type='String',
        info_description='Review status for the ClinVar submissions. '
                         'Possible values: criteria_provided_-_conflicting_interpretations, '
                         'criteria_provided_-_multiple_submitters_-_no_conflicts, '
                         'criteria_provided_-_single_submitter, '
                         'no_assertion_criteria_provided, '
                         'no_assertion_provided, '
                         'practice_guideline, '
                         'reviewed_by_expert_panel.',
        info_source='ClinVar',
        source_field_id='CLNREVSTAT')
    AnnotationsList.append(ISEQ_CLINVAR_REVIEW_STATUS)

    ISEQ_CLINVAR_GOLD_STARS = AnnotationInfo(
        info_id='ISEQ_CLINVAR_GOLD_STARS',
        info_number='A',
        info_type='String',
        info_description='Assignment of stars for the ClinVar submissions. '
                         'Based on review status (ISEQ_CLINVAR_REVIEW_STATUS). '
                         'Possible values:  0, 1, 2, 3, 4'
                         'For more information see: https://www.ncbi.nlm.nih.gov/clinvar/docs/review_status/#revstat.',
        info_source='ClinVar')
    AnnotationsList.append(ISEQ_CLINVAR_GOLD_STARS)

    ISEQ_CLINVAR_VARIATION_INFO_INCLUDED = AnnotationInfo(
        info_id='ISEQ_CLINVAR_VARIATION_INFO_INCLUDED',
        info_number='A',
        info_type='String',
        info_description='Information regarding ClinVar Variations including this variant (Types of variation: '
                         'CompoundHeterozygote, Diplotype, Distinct chromosomes, Haplotype, Phase unknown).'
                         'Format: '
                         'variation_id | variation_type |'
                         ' < clinical_significance ^ collection_method ^ date_last_evaluated > '
                         '(...)'
                         ' < clinical_significance ^ collection_method ^ date_last_evaluated > '
                         ': (...) : '
                         'variation_id | variation_type | '
                         '< clinical_significance ^ collection_method ^ date_last_evaluated > '
                         '(...)'
                         ' < clinical_significance ^ collection_method ^ date_last_evaluated >.',
        info_source='ClinVar')
    AnnotationsList.append(ISEQ_CLINVAR_VARIATION_INFO_INCLUDED)

    ISEQ_CLINVAR_GENE_INFO = AnnotationInfo(
        info_id='ISEQ_CLINVAR_GENE_INFO',
        info_number='A',
        info_type='String',
        info_description='Name(s) and ID(s) of genes for the Clinvar variant. '
                         'Format: gene_name : gene_id | (...) | gene_name : gene_id.',
        info_source='ClinVar',
        source_field_id='GENEINFO')
    AnnotationsList.append(ISEQ_CLINVAR_GENE_INFO)


class SubmissionInfo(object):

    # # # Contents of: submission_summary.txt
    # # Overview of interpretation, phenotypes, observations, and methods reported in each current submission
    # # Explanation of the columns in this report
    # VariationID:                   the identifier assigned by ClinVar and used to build the URL,
    #                                namely https://ncbi.nlm.nih.gov/clinvar/VariationID
    # ClinicalSignificance:          interpretation of the variation-condition relationship
    # DateLastEvaluated:             the last date the variation-condition relationship was evaluated by this submitter
    # Description:                   an optional free text description of the basis of the interpretation
    # SubmittedPhenotypeInfo:        the name(s) or identifier(s) submitted for the condition that was interpreted
    #                                relative to the variant
    # ReportedPhenotypeInfo:         the MedGen identifier/name combinations ClinVar uses to report the condition
    #                                that was interpreted. 'na' means there is no public identifer in MedGen for
    #                                the condition.
    # ReviewStatus:                  the level of review for this submission,
    #                                namely http//www.ncbi.nlm.nih.gov/clinvar/docs/variation_report/#review_status
    # CollectionMethod:              the method by which the submitter obtained the information provided
    # OriginCounts:                  the reported origin and the number of observations for each origin
    # Submitter:                     the submitter of this record
    # SCV:                           the accession and current version assigned by ClinVar to the submitted
    #                                interpretation of the variation-condition relationship
    # SubmittedGeneSymbol:           the gene symbol reported in this record
    # ExplanationOfInterpretation:   more details if ClinicalSignificance is 'other' or 'drug response'

    def __init__(self, clinical_significance: str, date_last_evaluated: str, collection_method: str, scv: str):
        super().__init__()
        self.clinical_significance = clinical_significance.lower().replace(' ', '_')
        self.date_last_evaluated = '.' if date_last_evaluated == '-' else datetime.datetime.strptime(
            date_last_evaluated, '%b %d, %Y').strftime('%d-%m-%Y')
        self.collection_method = collection_method.replace(' ', '_').replace(';', '/')
        self.scv = scv


class ClinVarVariationInfo(object):

    def __init__(self, variation_id: str):
        super().__init__()
        self.variation_id = variation_id
        self.submission_info_list: List[SubmissionInfo] = []

    def add_submission_info(self, submission_info: SubmissionInfo):
        self.submission_info_list.append(submission_info)


VariationTypeInfo = namedtuple('VariationTypeInfo', 'VariationID VariationType')


def create_variation_id_submissions_info_dictionary(submission_summary_gz_file: str) -> Dict[str, ClinVarVariationInfo]:
    variation_id_submissions_info_dictionary: Dict[str, ClinVarVariationInfo] = {}

    with gzip.open(submission_summary_gz_file, 'rb') as file:

        for raw_line in file:

            if not raw_line.decode().startswith('#'):

                line = raw_line.decode().strip().split("\t")

                variation_id = line[0]
                clinical_significance = line[1]
                date_last_evaluated = line[2]
                collection_method = line[7]
                scv = line[10]

                submission_info = SubmissionInfo(clinical_significance, date_last_evaluated,
                                                 collection_method, scv)

                try:
                    variation_id_submissions_info_dictionary[variation_id].add_submission_info(submission_info)

                except KeyError:
                    variation_id_submissions_info_dictionary[variation_id] = ClinVarVariationInfo(variation_id)
                    variation_id_submissions_info_dictionary[variation_id].add_submission_info(submission_info)

    return variation_id_submissions_info_dictionary


def create_allele_id_complex_variation_ids_dictionary(variation_allele_id_gz_file) -> Dict[
    str, List[VariationTypeInfo]]:
    allele_id_complex_variation_ids_dictionary: Dict[str, List[VariationTypeInfo]] = {}

    with gzip.open(variation_allele_id_gz_file, 'rb') as file:

        for raw_line in file:

            if not raw_line.decode().startswith('#') \
                    and not raw_line.decode().split("\t", 1)[1].startswith('Variant'):

                line = raw_line.decode().strip().split("\t")

                variation_id = line[0]
                variation_type = line[1].replace(' ', '_')
                allele_id = line[2]

                variation_type_info = VariationTypeInfo(variation_id, variation_type)

                try:
                    allele_id_complex_variation_ids_dictionary[allele_id].append(variation_type_info)
                except:
                    allele_id_complex_variation_ids_dictionary[allele_id] = [variation_type_info]

    return allele_id_complex_variation_ids_dictionary


if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=
    """

Creates VCF database containing ClinVar annotations. Required files:

  * clinvar.vcf.gz
  * variation_allele.txt.gz
  * submission_summary.txt.gz

are released approximately monthly and can be downloaded from ClinVar FTP site (ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/).

NOTES:

1. Database clinvar.vcf.gz should have chromosome naming convention changed to contain chromosomes: chr1, chr2, ..., chr22, chrX, chrY, chrM
2. Database clinvar.vcf.gz should have multiallelic sites split and be left-indel normalized with the use of the command:

    bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any clinvar.vcf.gz | bgzip > clinvar.split.normalized.vcf.gz

3. The clinvar.vcf.gz containes troublesome ASCII characters that are best to be removed. To check which troublesome characters are present the following command can be used:

    zcat clinvar.vcf.gz | grep --color='auto' -P -o "[\\x80-\\xFF]" | sort | uniq

Troublesome characters can be replaced with the use of sed command, for example:

    zcat clinvar.vcf.gz | sed 's/ß/B/g' | sed 's/ã/a/g' | sed 's/ä/a/g' | sed 's/é/e/g' | sed 's/è/e/g' | sed 's/ê/e/g' | sed 's/ë/e/g' | sed 's/ô/o/g' | sed 's/ö/o/g' | sed 's/ú/u/g' | sed 's/ü/u/g' | bgzip > tmp.vcf.gz
    mv tmp.vcf.gz clinvar.vcf.gz

""")

    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('SUBMISSION_SUMMARY_GZ_FILE', type=str, help="path to submission_summary.txt.gz")
    parser.add_argument('VARIATION_ALLELE_GZ_FILE', type=str, help="path to variation_allele.txt.gz")
    parser.add_argument('CLINVAR_VCF_GZ_FILE', type=str, help="path to clinvar.vcf.gz")
    args = parser.parse_args()

    submission_summary_gz_file = args.SUBMISSION_SUMMARY_GZ_FILE
    variation_allele_gz_file = args.VARIATION_ALLELE_GZ_FILE
    clinvar_vcf_gz_path = args.CLINVAR_VCF_GZ_FILE

    # Load:  Variation ID - ClinVarVariationInfo dictionary
    variation_id_submissions_info_dictionary = create_variation_id_submissions_info_dictionary(
        submission_summary_gz_file)

    allele_id_complex_variation_ids_dictionary = create_allele_id_complex_variation_ids_dictionary(
        variation_allele_gz_file)

    # Open original ClinVar VCF database and create ClinVar annotation output file:
    with VariantFile(clinvar_vcf_gz_path) as clinvar_original_vcf, VariantFile('-', 'w') as clinvar_annotation_vcf:

        # CREATE OUTPUT VCF HEADER #
        ############################

        # Add fileDate and reference fields
        clinvar_annotation_vcf.header.add_line("##fileDate=" + datetime.date.today().strftime("%d-%m-%Y"))
        clinvar_annotation_vcf.header.add_line("##reference=grch38")

        # Add INFO fields
        version = [record.value for record in clinvar_original_vcf.header.records if record.key == "fileDate"][0]
        for annotation in _Annotations.AnnotationsList:
            annotation.info_version = version
            clinvar_annotation_vcf.header.add_line(annotation.get_header_info_line())

        # Add contig fields
        for contig in ['chr{}'.format(i) for i in list(range(1, 23)) + ['X', 'Y', 'M']]:
            clinvar_annotation_vcf.header.add_line("##contig=<ID={}>".format(contig))

        # BUILD OUTPUT VCF BODY #
        #########################

        for original_record in clinvar_original_vcf.fetch():

            # Ignore records with no ALT alleles
            if len(original_record.alleles) > 1:

                # Populate RS columns
                try:
                    rs = 'rs' + ';'.join(original_record.info['RS'])
                except KeyError:
                    rs = '.'

                # Get variation ID and Allele ID
                variation_id_variant = original_record.id

                allele_id = str(original_record.info[_Annotations.ISEQ_CLINVAR_ALLELE_ID.source_field_id])

                # Create and populate new ClinVar annotation file record

                new_record = clinvar_annotation_vcf.new_record(
                    contig=original_record.contig, start=original_record.start, stop=original_record.stop,
                    alleles=original_record.alleles,
                    id=rs, qual=original_record.qual, filter=original_record.filter, info=None)

                # Add ISEQ_CLINVAR_ALLELE_ID
                # and ISEQ_CLINVAR_VARIATION_ID fields
                new_record.info[_Annotations.ISEQ_CLINVAR_ALLELE_ID.info_id] = allele_id
                new_record.info[_Annotations.ISEQ_CLINVAR_VARIATION_ID.info_id] = variation_id_variant

                # Add ISEQ_CLINVAR_VARIANT_TYPE field
                try:
                    new_record.info[_Annotations.ISEQ_CLINVAR_VARIANT_TYPE.info_id] = \
                        original_record.info[_Annotations.ISEQ_CLINVAR_VARIANT_TYPE.source_field_id].lower()
                except KeyError:
                    pass

                # Add ISEQ_VARIANT_CLINVAR_DISEASES field
                try:
                    new_record.info[_Annotations.ISEQ_VARIANT_CLINVAR_DISEASES.info_id] = \
                        ':'.join(
                            [x.replace(',', '').replace(':', '-').replace('|', '-').replace('~', '').replace('^', '')
                             for x in
                             ','.join(original_record.info[
                                          _Annotations.ISEQ_VARIANT_CLINVAR_DISEASES.source_field_id]).split(
                                 '|')])
                except KeyError:
                    pass

                # Add ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE field
                try:
                    new_record.info[_Annotations.ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE.info_id] = \
                        ','.join(original_record.info[
                            _Annotations.ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE.source_field_id]).replace(',_',
                                                                                                       ',').replace(',',
                                                                                                                    ':').lower()
                except KeyError:
                    pass

                # Add ISEQ_CLINVAR_SIGNIFICANCE,
                #     ISEQ_CLINVAR_DATE_LAST_EVALUATED
                # and ISEQ_CLINVAR_COLLECTION_METHOD fields
                try:
                    new_record.info[_Annotations.ISEQ_CLINVAR_SIGNIFICANCE.info_id] = \
                        ':'.join([x.clinical_significance for x in
                                  variation_id_submissions_info_dictionary[variation_id_variant].submission_info_list])

                    new_record.info[_Annotations.ISEQ_CLINVAR_DATE_LAST_EVALUATED.info_id] = \
                        ':'.join([x.date_last_evaluated for x in
                                  variation_id_submissions_info_dictionary[variation_id_variant].submission_info_list])

                    new_record.info[_Annotations.ISEQ_CLINVAR_COLLECTION_METHOD.info_id] = \
                        ':'.join([x.collection_method for x in
                                  variation_id_submissions_info_dictionary[variation_id_variant].submission_info_list])
                except KeyError:
                    pass

                # Add ISEQ_CLINVAR_REVIEW_STATUS
                # and ISEQ_CLINVAR_GOLD_STARS field
                try:
                    clinrevstat = ','.join(
                        list(original_record.info[_Annotations.ISEQ_CLINVAR_REVIEW_STATUS.source_field_id]))
                    new_record.info[_Annotations.ISEQ_CLINVAR_REVIEW_STATUS.info_id] = clinrevstat.replace(',_', '_-_')
                    try:
                        new_record.info[_Annotations.ISEQ_CLINVAR_GOLD_STARS.info_id] = \
                            REVIEW_STATUS_TO_GOLD_STARS_COUNT_DICTIONARY[clinrevstat]
                    except KeyError:
                        pass
                except KeyError:
                    pass

                # ADD ISEQ_CLINVAR_VARIATION_INFO_INCLUDED
                try:

                    variation_info = []

                    for variation_type_info in allele_id_complex_variation_ids_dictionary[allele_id]:

                        variation_id = variation_type_info.VariationID
                        variation_type = variation_type_info.VariationType

                        submission_triples = []

                        submission_info: SubmissionInfo
                        for submission_info in variation_id_submissions_info_dictionary[
                            variation_id].submission_info_list:
                            submission_triples.append('^'.join([submission_info.clinical_significance,
                                                                submission_info.collection_method,
                                                                submission_info.date_last_evaluated]))

                        variation_info.append('|'.join([variation_id,
                                                        variation_type,
                                                        '<' + '><'.join(submission_triples) + '>']))

                    new_record.info[_Annotations.ISEQ_CLINVAR_VARIATION_INFO_INCLUDED.info_id] = ':'.join(
                        variation_info).replace(",", "")

                except KeyError:
                    pass

                ## Add ISEQ_CLINVAR_GENE_INFO
                try:
                    new_record.info[_Annotations.ISEQ_CLINVAR_GENE_INFO.info_id] = original_record.info[
                        _Annotations.ISEQ_CLINVAR_GENE_INFO.source_field_id]
                except KeyError:
                    pass

                clinvar_annotation_vcf.write(new_record)

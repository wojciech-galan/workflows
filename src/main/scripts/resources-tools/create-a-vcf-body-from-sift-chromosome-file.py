#!/usr/bin/env python3

__version__ = '0.0.1'

from sys import stdin
import re
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This script is used database consisting of bgzipped VCF files containing SIFT4G scores for GRCh38 reference genome.
It creates a body of VCF file from SIFT4G file for a specified chromosome.

anakin:  /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/scrores/sift4g
""")

parser.add_argument('CHR', type=str, help="name of chromosome")
parser.add_argument('-v', '--version', action='version', version=__version__)

args = parser.parse_args()

CHROM = args.CHR

first = True
prevPOS = '.'
prevALT = '.'


for line in stdin:

    if line.startswith("#") or line.strip().split('\t')[-4] == "NA" or line.strip().split('\t')[-1] == "ref":
        pass
    else:

        line = line.strip().split('\t')
        line = line[0:4] + line[5:11]

        POS = line[0]
        REF = line[1]
        ALT = line[2]
        TRANSCRIPT = line[3]
        GENE_NAME = line[4]
        REGION = line[5]
        HGVS_p = ''.join(['p.', line[6], line[8], line[7]])
        SIFT_SCORE = line[9].strip()

        if (POS == prevPOS and ALT == prevALT):

            # append to line
            scores.append(float(SIFT_SCORE))
            to_be_printed_line += ':' + '|'.join([TRANSCRIPT, GENE_NAME, REGION, HGVS_p, SIFT_SCORE])

            prevPOS = POS
            prevALT = ALT

        else:

            if not first:
                        # print line
                min_score = min(scores)
                to_be_printed_line += ';ISEQ_SIFT4G_MIN=' + str(min_score)
                print(to_be_printed_line)

            # start building next line
            INFO = 'ISEQ_SIFT4G=' + '|'.join([TRANSCRIPT, GENE_NAME, REGION, HGVS_p, SIFT_SCORE])
            to_be_printed_line = '\t'.join([CHROM, POS, '.', REF, ALT, '.', '.', INFO])
            scores = [float(SIFT_SCORE)]

            prevPOS = POS
            prevALT = ALT


        first = False

# print last line
print(to_be_printed_line)

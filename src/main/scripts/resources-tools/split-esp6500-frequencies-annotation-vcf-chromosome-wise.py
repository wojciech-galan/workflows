#!/usr/bin/env python3

import argparse
from Bio import bgzf
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This script is used to split chromosome-wise hg19 ESP6500 annotation VCF lifted over to
GRCh38.
""")

parser.add_argument('OUTPUT_DIR', type=str, help="path to output directory")
args = parser.parse_args()
output_directory = args.OUTPUT_DIR

header = []
header.append("##fileformat=VCFv4.3")
header.append("##FILTER=<ID=PASS,Description=\"All filters passed\">")
header.append("##reference=GRCh38")
header.append("##INFO=<ID=ISEQ_ESP6500_EA_AF,Number=A,Type=Float,Description=\"Alternate allele frequency - ESP6500, European American population (EA)\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("##INFO=<ID=ISEQ_ESP6500_AA_AF,Number=A,Type=Float,Description=\"Alternate allele frequency - ESP6500, African American population (AA)\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("##INFO=<ID=ISEQ_ESP6500_AN,Number=1,Type=Integer,Description=\"Total allele count for - ESP6500\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("##INFO=<ID=ISEQ_ESP6500_AF,Number=A,Type=Float,Description=\"Alternate allele frequency - ESP6500\",Source=\"ESP6500\",Version=\"ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")

chromosomes = ["chr" + str(i) for i in range(1, 23)] + ["chrX", "chrY-and-the-rest"]
handlers = [bgzf.BgzfWriter(output_directory + "/" + x + ".esp6500.vcf.gz") for x in chromosomes]

for handle in handlers:
    handle.write("\n".join(header) + "\n")


for raw_line in stdin:

    if not raw_line.startswith("#"):

        line = raw_line.strip().split("\t")

        CHR = line[0]
        INFO = []

        if CHR in chromosomes:
            handlers[chromosomes.index(CHR)].write('\t'.join(line) + "\n")
        else:
            handlers[chromosomes.index("chrY-and-the-rest")].write('\t'.join(line) + "\n")


for handle in handlers:
    handle.close()

#!/usr/bin/python3

__version__ = '0.0.3'

import gzip
import argparse
import json
from typing import List, Dict
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

def calculate_frameshift(ref,alt):
    """calculates frameshift phase (number of nucleotides)"""
    dif = len(ref) -len(alt)
    shift = str(dif%3)
    return "+"+shift

    
class Transcript(object):
    """represents description of one transcript extracted from the ANN field"""
    """arguments: text = raw vcf line, alt = alt allele; transcript_index = transcript position in ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def find_good_transcripts(transcript_list: List, gene: str) -> List:
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) no warnings and errors 3) changing protein product 4) within provided gene
    returns list of the ANN field records for 'reliable' transcripts only"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or "start_lost"
                in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or
                "splice_donor" in transcript.variant_type() or "missense" in transcript.variant_type() or
                "stop_lost" in transcript.variant_type() or "inframe_insertion" in transcript.variant_type() or
                "inframe_deletion" in transcript.variant_type()) and \
                (transcript.transcript_type() == "protein_coding") and \
                ('WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings())\
                and (gene in transcript.transcript_gene()):
            good_transcripts.append(transcript)
    return good_transcripts

def process_line(line: str) -> List:
    line_splitted = line.strip().split("\t")
    info = [i.split('=') for i in line_splitted[7].split(";")]
    ref = line_splitted[3]
    alt = line_splitted[4]
    info_fields = {}
    for i in info:
        info_fields[i[0]] = i[1]
    return [ref, alt, info_fields]


def read_file(input_vcf: str) -> List:
    with gzip.open(input_vcf, "rt") as f:
        return f.readlines()

def make_clinvar_gene_dict(vcf_lines: List) -> Dict:
    clinvar_dict = {}
    for line in vcf_lines:
        if line[0] != '#':
            ref, alt, info_fields = process_line(line)
            try:
                significance = info_fields["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"].replace("conflicting_interpretations_of_pathogenicity","uncertain_significance")
            except KeyError:
                significance = "not_provided"
            try:
                ann = info_fields["ANN"].split(",")
                genes = [i.split(':')[0] for i in info_fields["ISEQ_CLINVAR_GENE_INFO"].split('|')]
            except KeyError:
                continue
            else:
                for gene in genes:
                    aa_changes_list =[]
                    transcripts = [Transcript(item) for item in ann]
                    good_transcripts = find_good_transcripts(transcripts, gene)
                    for transcript in good_transcripts:
                        variant_type = transcript.fields[1]
                        aa_field = transcript.fields[10]
                        aa_pos = transcript.fields[13].split('/')[0]
                        transcript_id = transcript.fields[6]
                        if "missense" in variant_type:
                            aa_ref = aa_field[2:5]
                            aa_alt = aa_field[-3:]
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if ("stop_gained" in variant_type) and ("frameshift" not in variant_type):
                            aa_ref = aa_field[2:5]
                            aa_alt = '*'
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if ("start_lost" in variant_type):
                            aa_ref = aa_field[2:5]
                            aa_alt = "?"
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if ("stop_lost" in variant_type):
                            aa_ref = aa_field[2:5]
                            aa_alt = '?*'
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if ("frameshift" in variant_type):
                            aa_ref = aa_field[2:5]
                            aa_alt = calculate_frameshift(ref, alt) + "fs"
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if "disruptive_inframe_insertion" in variant_type:
                            try:
                                aa_field.split('ins')[1]
                            except IndexError:
                                aa_ref = aa_field.split('dup')[0][2:]
                                aa_alt = 'dup'
                                one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                                if one_aa_change not in aa_changes_list:
                                    aa_changes_list.append(one_aa_change)
                            else:
                                aa_ref = aa_field.split('ins')[0][2:]
                                aa_alt = aa_field.split('ins')[1]
                                one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                                if one_aa_change not in aa_changes_list:
                                    aa_changes_list.append(one_aa_change)
                        if ("inframe_insertion" in variant_type) and ("disruptive_inframe_insertion" not in variant_type):
                            aa_ref = aa_field.split('dup')[0][2:]
                            aa_alt = 'dup'
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if ("inframe_deletion" in variant_type) and ("disruptive_inframe_deletion" not in variant_type):
                            aa_ref = aa_field.split('del')[0][2:]
                            aa_alt = 'del'
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if "disruptive_inframe_deletion" in variant_type:
                            aa_ref = aa_field.split('del')[0][2:]
                            aa_alt = 'del'
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if "splice_acceptor" in variant_type:
                            aa_ref = transcript.fields[9].split('.')[1]
                            aa_alt = ""
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                        if "splice_donor" in variant_type:
                            aa_ref = transcript.fields[9].split('.')[1]
                            aa_alt = ""
                            one_aa_change = [aa_ref, aa_alt, aa_pos, transcript_id, significance]
                            if one_aa_change not in aa_changes_list:
                                aa_changes_list.append(one_aa_change)
                    if len(aa_changes_list) > 0:
                        try:
                            clinvar_dict[gene]+=aa_changes_list
                        except KeyError:
                            clinvar_dict[gene] = aa_changes_list
    return clinvar_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Prepares json file needed for acmg annotations, "clinvar-protein-changes-dictionary.json": '
                                                 'gene => list of protein level changes;'
                                                 'protein level changes are given as follow: '
                                                 ' [[aa_ref1, aa_alt1, aa_pos1, transcript_id1, significance1],'
                                                 ' [aa_ref2, aa_alt2, aa_pos2, transcript_id2, significance2],[...],...] ')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input', '-i', type=str, help="Path to annotated with SnpEff Clinvar vcf.gz file, unzipped input can be also piped from stdin")
    parser.add_argument('--output', '-o', required=True, type=str, help="Path to output json file")
    args = parser.parse_args()
    if args.input:
        input_vcf = read_file(args.input)
        clinvar_gene_changes_dict = make_clinvar_gene_dict(input_vcf)
    else:
        clinvar_gene_changes_dict = make_clinvar_gene_dict(stdin)
    output_json=open(args.output, "w")
    t1=json.dumps(clinvar_gene_changes_dict)
    output_json.write(t1)
    output_json.close()


#usage: python3 /home/kt/acmg/workflows/resources/acmg_files/clinvar_gene_dict.py -i /data/public/intelliseqngs/workflows/resources/acmg/clinvar/20-02-2020/clinvar.variant-level-annotated-with-snpEff.vcf -o /data/public/intelliseqngs/workflows/resources/acmg/clinvar/20-02-2020/clinvar-protein-changes.json



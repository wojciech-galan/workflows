#!/usr/bin/python3

__version__ = '0.0.2'


import argparse
from typing import Dict, List
import json
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)
import gzip

class Transcript(object):
    """represents description of one transcript extracted from the ANN field
    arguments: one item from the ANN field"""

    def __init__(self, annotation_string: str):
        super().__init__()
        self.fields = annotation_string.split('|')

    def variant_type(self):
        return self.fields[1]

    def transcript_type(self):
        return self.fields[7]

    def transcript_gene(self):
        return self.fields[3]

    def transcript_warnings(self):
        return self.fields[15]

    def transcript_gene_id(self):
        return self.fields[4]


def find_good_transcripts(transcript_list: List, gene: str) -> List:
    """for each variant extracts 'reliable' transcripts:
    criteria: 1) protein coding transcript, 2) no warnings and errors
    returns list of the ANN field records for 'reliable' transcripts only; they may be for different genes"""
    good_transcripts = []
    for transcript in transcript_list:
        if (
                "stop_gained" in transcript.variant_type() or "frameshift" in transcript.variant_type() or "start_lost"
                in transcript.variant_type() or "splice_acceptor" in transcript.variant_type() or
                "splice_donor" in transcript.variant_type()) and \
                (transcript.transcript_type() == "protein_coding") and \
                ('WARNING' not in transcript.transcript_warnings() and 'ERROR' not in transcript.transcript_warnings())\
                and (gene in transcript.transcript_gene()):
            good_transcripts.append(transcript)
    return good_transcripts


def process_line(line: str) -> Dict:
    line_splitted = line.strip().split("\t")
    info = [i.split('=') for i in line_splitted[7].split(";")]
    info_fields = {}
    for i in info:
        info_fields[i[0]] = i[1]
    return info_fields

def read_file(input_vcf: str) -> List:
    with gzip.open(input_vcf, "rt") as f:
        return f.readlines()


def make_clinvar_gene_dict(vcf_lines: List) -> Dict:
    clinvar_dict = {}
    for line in vcf_lines:
        if line[0] != '#':
            info_fields = process_line(line)
            try:
                significance = info_fields["ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE"].replace("conflicting_interpretations_of_pathogenicity","uncertain_significance").split(":")
            except KeyError:
                significance = []
            try:
                genes = [i.split(':')[0] for i in info_fields["ISEQ_CLINVAR_GENE_INFO"].split('|')]
                ann = info_fields["ANN"].split(",")
            except KeyError:
                continue #?
            else:
                for gene in genes:
                    n_lof = 0
                    n_patho = 0
                    n_lof_patho = 0
                    transcripts = [Transcript(item) for item in ann]
                    good_transcripts = find_good_transcripts(transcripts,gene)
                    if len(good_transcripts) > 0:
                        n_lof = 1
                    if ('pathogenic' in significance) or ('pathogenic/likely_pathogenic' in significance): # maybe counting also likely_patho would be better idea
                        n_patho = 1
                    if (n_lof == 1) and (n_patho == 1):
                        n_lof_patho = 1
                    if gene not in clinvar_dict.keys():
                        clinvar_dict[gene] = [n_patho, n_lof, n_lof_patho]
                    else:
                        clinvar_dict[gene][0]+=n_patho
                        clinvar_dict[gene][1]+=n_lof
                        clinvar_dict[gene][2]+=n_lof_patho
    return clinvar_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Prepares json file needed for acmg annotations, "clinvar-lof-dictionary.json":\n'
                                              'gene name => [number of all pathogenic variants in clinvar, number of all loss of function variants, number of pathogenic loss of function variants]')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str, help="Path to annotated with SnpEff ClinVar vcf.gz file, unzipped input may be also piped from stdin")
    parser.add_argument('-o', '--output', type=str, required=True, help="Path to output json file")
    args = parser.parse_args()
    if args.input:
        input_vcf = read_file(args.input)
        clinvar_gene_lof_dict = make_clinvar_gene_dict(input_vcf)
    else:
        clinvar_gene_lof_dict = make_clinvar_gene_dict(stdin)
    output_json = open(args.output, "w")
    t1=json.dumps(clinvar_gene_lof_dict)
    output_json.write(t1)
    output_json.close()
#usage python3 /home/kt/acmg/workflows/resources/acmg_files/clinvar_gene_dict_lof.py -i /data/public/intelliseqngs/workflows/resources/acmg/clinvar/20-02-2020/clinvar.variant-level-annotated-with-snpEff.vcf -o /data/public/intelliseqngs/workflows/resources/acmg/clinvar/20-02-2020/clinvar-lof.json

#!/usr/bin/env python3

__version__ = '0.0.1'

import argparse
from Bio import bgzf
from sys import stdin,stderr
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)


parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This script creates chromosome-wise (chr1, ..., chr22, chrX, chrY-and-the-rest),
bgzippef VCFs, containing frequency annotation based on gnomAD genomes v3 frequencies
database.
""")

parser.add_argument('OUTPUT_DIR', type=str, help="path to output directory")
parser.add_argument('-v', '--version', action='version', version=__version__)
args = parser.parse_args()


args = parser.parse_args()
output_directory = args.OUTPUT_DIR



header = []
header.append("##fileformat=VCFv4.3")
header.append("##reference=Homo_sapiens_assembly38.fasta")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AC,Number=A,Type=Integer,Description=\"Alternate allele count for samples - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AN,Number=1,Type=Integer,Description=\"Total number of alleles in samples -  gnomAD genomes\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_nhomalt,Number=A,Type=Integer,Description=\"Count of homozygous individuals in samples - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_popmax,Number=A,Type=String,Description=\"Population with maximum AF -  gnomAD genomes\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_popmax_AF,Number=A,Type=Float,Description=\"Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_nhomalt_popmax,Number=A,Type=String,Description=\"Count of homozygous individuals in the population with the maximum allele frequency - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_ami,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Amish ancestry (ami) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_afr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_amr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_asj,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_eas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_fin,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_nfe,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_sas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of South Asian ancestry (sas) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_AF_oth,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of uncertain ancestry - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_lcr,Number=0,Type=Flag,Description=\"Variant falls within a low complexity region - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_nonpar,Number=0,Type=Flag,Description=\"Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_GENOMES_V3_FILTER_STATUS,Number=A,Type=String,Description=\"Filter status. Format: filter_status : (...) : filter_status. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (failed random forest filtering thresholds of 0.2634762834546574, 0.22213813189901457 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD v3\",Source=\"gnomAD\",Version=\"gnomAD v3\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")


fields_order = ['ISEQ_GNOMAD_GENOMES_V3_AC', 'ISEQ_GNOMAD_GENOMES_V3_AN', 'ISEQ_GNOMAD_GENOMES_V3_AF', 'ISEQ_GNOMAD_GENOMES_V3_nhomalt', 'ISEQ_GNOMAD_GENOMES_V3_popmax', 'ISEQ_GNOMAD_GENOMES_V3_popmax_AF', 'ISEQ_GNOMAD_GENOMES_V3_nhomalt_popmax', 'ISEQ_GNOMAD_GENOMES_V3_AF_ami', 'ISEQ_GNOMAD_GENOMES_V3_AF_afr', 'ISEQ_GNOMAD_GENOMES_V3_AF_amr', 'ISEQ_GNOMAD_GENOMES_V3_AF_asj', 'ISEQ_GNOMAD_GENOMES_V3_AF_eas', 'ISEQ_GNOMAD_GENOMES_V3_AF_fin', 'ISEQ_GNOMAD_GENOMES_V3_AF_nfe', 'ISEQ_GNOMAD_GENOMES_V3_AF_sas', 'ISEQ_GNOMAD_GENOMES_V3_AF_oth', "ISEQ_GNOMAD_GENOMES_V3_lcr", "ISEQ_GNOMAD_GENOMES_V3_nonpar", "ISEQ_GNOMAD_GENOMES_V3_FILTER_STATUS"]


fields_dictionary = {
    "AC":                       "ISEQ_GNOMAD_GENOMES_V3_AC",
    "AN":                       "ISEQ_GNOMAD_GENOMES_V3_AN",
    "AF":                       "ISEQ_GNOMAD_GENOMES_V3_AF",
    "nhomalt":                  "ISEQ_GNOMAD_GENOMES_V3_nhomalt",
    "popmax":                   "ISEQ_GNOMAD_GENOMES_V3_popmax",
    "AF_popmax":                "ISEQ_GNOMAD_GENOMES_V3_popmax_AF",
    "nhomalt_popmax":           "ISEQ_GNOMAD_GENOMES_V3_nhomalt_popmax",
    "AF_ami":                   "ISEQ_GNOMAD_GENOMES_V3_AF_ami",
    "AF_afr":                   "ISEQ_GNOMAD_GENOMES_V3_AF_afr",
    "AF_amr":                   "ISEQ_GNOMAD_GENOMES_V3_AF_amr",
    "AF_asj":                   "ISEQ_GNOMAD_GENOMES_V3_AF_asj",
    "AF_eas":                   "ISEQ_GNOMAD_GENOMES_V3_AF_eas",
    "AF_fin":                   "ISEQ_GNOMAD_GENOMES_V3_AF_fin",
    "AF_nfe":                   "ISEQ_GNOMAD_GENOMES_V3_AF_nfe",
    "AF_sas":                   "ISEQ_GNOMAD_GENOMES_V3_AF_sas",
    "AF_oth":                   "ISEQ_GNOMAD_GENOMES_V3_AF_oth",
    "lcr":                      "ISEQ_GNOMAD_GENOMES_V3_lcr",
    "nonpar":                   "ISEQ_GNOMAD_GENOMES_V3_nonpar"

}

popmax_list = ["afr", "amr", "asj", "eas", "fin", "nfe", "sas"]

chromosomes = ["chr" + str(i) for i in range(1, 23)] + ["chrX", "chrY"]
handlers = [bgzf.BgzfWriter(output_directory + "/" + x + ".gnomad-genomes-v3.vcf.gz") for x in chromosomes]


for handle in handlers:
    handle.write("\n".join(header) + "\n")


for raw_line in stdin:

    if not raw_line.startswith("#"):

        try:
            line = raw_line.strip().split("\t")
            ALT = line[4].split(",")
            FILTER = line[6]
            CHR = line[0]

            INFO_tuples = [
                (fields_dictionary[x.split("=")[0]], x.split("=")[1]) if "=" in x else (fields_dictionary[x], None) for x in
                line[7].split(";") if
                ("=" in x and x.split("=")[0] in fields_dictionary.keys()) or x in fields_dictionary.keys()]


            AF_list = [(x.split("=")[0].replace("AF_",""), x.split("=")[1].split(",")) for x in line[7].split(";") if
                                             x.split("=")[0] in ["AF_" + y for y in popmax_list]]


            nhomalt_dict = dict((k.replace("nhomalt_", ""), v) for k, v in
                                           [(x.split("=")[0], x.split("=")[1].split(",")) for x in line[7].split(";") if
                                             x.split("=")[0] in ["nhomalt_" + y for y in popmax_list]])

            if FILTER.count("AC0") == 0:

                popmax = ["." for i in range(0, len(ALT))]
                popmax_af = [0 for i in range(0, len(ALT))]
                nhomalt_popmax = [0 for i in range(0, len(ALT))]

                change = False

                for alt_index in range(0, len(ALT)):

                    AF_popmax_list = [float(x[1][alt_index]) for x in AF_list]

                    if not len(AF_popmax_list) == 0:

                        change = True

                        popmax_af[alt_index] = max(AF_popmax_list)
                        max_value_indices = [index for index, value in enumerate(AF_popmax_list) if value == popmax_af[alt_index]]
                        popmax[alt_index] = ":".join([AF_list[x][0] for x in max_value_indices])
                        nhomalt_popmax[alt_index] = ":".join([nhomalt_dict[x][alt_index] for x in popmax[alt_index].split(":")])

                if change:

                    INFO_tuples.append(("ISEQ_GNOMAD_GENOMES_V3_popmax", ",".join(popmax)))
                    INFO_tuples.append(("ISEQ_GNOMAD_GENOMES_V3_popmax_AF", ",".join([str(x) for x in popmax_af])))
                    INFO_tuples.append(("ISEQ_GNOMAD_GENOMES_V3_nhomalt_popmax", ",".join(nhomalt_popmax)))

            INFO_tuples_sorted = sorted(INFO_tuples, key=lambda x: fields_order.index(x[0]))

            line[7] = ";".join(["=".join([x, y]) if y is not None else x for (x, y) in INFO_tuples_sorted])
            line[6] = "."

            if CHR in chromosomes:
                handlers[chromosomes.index(CHR)].write('\t'.join(line) + "\n")
            else:
                handlers[chromosomes.index("chrY")].write('\t'.join(line) + "\n")
        except:
            print(line, file = stderr)


for handle in handlers:
    handle.close()

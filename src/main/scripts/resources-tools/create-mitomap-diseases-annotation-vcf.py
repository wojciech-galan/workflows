#!/usr/bin/env python3

__version__ = '1.0.1'

import argparse
import datetime
from typing import List

from pysam import VariantFile

#   README: MITOMAP_DISEASES_ABBREVIATIONS
#   --------------------------------------
#
#   Dictionary of MITOMAP abbreviations of diseases was created by copying the
#   content from: http://mitomap.org/foswiki/bin/view/MITOMAP/DiseaseList, with the following amendments applied:
#
#   - item "LD: Leigh Disease (or LS: Leigh Syndrome)" was split into two records: "LD: Leigh Disease"
#     and "LS: Leigh Syndrome"
#   - item "MERME: MERRF/MELAS, Lactic Acidosis, and Stroke-like episodes overlap disease" was changed to
#     "MERME: Myoclonic Epilepsy and Ragged Red Muscle Fibers/Mitochondrial Encephalomyopathy, Lactic Acidosis,
#     and Stroke-like episodes overlap disease"
#   - item "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa; alternate phenotype at this locus is
#     reported as Leigh Disease" was changed to "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa"
#   - item "Multisystem Mitochondrial Disorder (myopathy, encephalopathy, blindness, hearing loss,
#     peripheral neuropathy)" was removed
#
#   Additionally, the following items were added:
#
#   - HCM : Hypertrophic CardioMyopathy
#   - PEO: Progressive External Ophthalmoplegia
#   - PD: Parkinson Disease
#   - FSHD: Facioscapulohumeral Muscular Dystrophy
#   - MIDM: Maternally Inherited Diabetes Mellitus
#   - MDD: Major Depressive Disorder
#   - BD: Bipolar Disorder
#   - SZ: Schizophrenia
#   - BSN: Bilateral Striatal Necrosis
#   - MR: Mental Retardation
#   - DM: Diabetes Mellitus
#   - NSHL: Non-Syndromic Hearing Loss
#   - RP: Retinitis Pigmentosa
#   - CM: Cardiomiopathy
#
#  The resource for this file (http://mitomap.org/foswiki/bin/view/MITOMAP/DiseaseList) was last updated on 15-09-2015.
MITOMAP_DISEASES_ABBREVIATIONS = {
    "AD": "Alzheimer's_Disease",
    "ADPD": "Alzheimer's_Disease_and_Parkinsons's_Disease",
    "AMD": "Age-Related_Macular_Degeneration",
    "AMDF": "Ataxia_Myoclonus_and_Deafness",
    "AMegL": "Acute_Megakaryoblastic_Leukemia",
    "BD": "Bipolar_Disorder",
    "BSN": "Bilateral_Striatal_Necrosis",
    "CIPO": "Chronic_Intestinal_Pseudoobstruction_with_myopathy_and_Ophthalmoplegia",
    "CM": "Cardiomiopathy",
    "CPEO": "Chronic_Progressive_External_Ophthalmoplegia",
    "DEAF": "Maternally_inherited_DEAFness_or_aminoglycoside-induced_DEAFness",
    "DEMCHO": "Dementia_and_Chorea",
    "DM": "Diabetes_Mellitus",
    "DMDF": "Diabetes_Mellitus_&_DeaFness",
    "ESOC": "Epilepsy_Strokes_Optic_atrophy_&_Cognitive_decline",
    "EXIT": "Exercise_Intolerance",
    "FBSN": "Familial_Bilateral_Striatal_Necrosis",
    "FICP": "Fatal_Infantile_Cardiomyopathy_Plus_a_MELAS-associated_cardiomyopathy",
    "FSGS": "Focal_Segmental_Glomerulosclerosis",
    "FSHD": "Facioscapulohumeral_muscular_dystrophy",
    "GER": "Gastrointestinal_Reflux",
    "HCM": "Hypertrophic_CardioMyopathy",
    "KSS": "Kearns_Sayre_Syndrome",
    "LD": "Leigh_Disease",
    "LDYT": "Leber's_hereditary_optic_neuropathy_and_DYsTonia",
    "LHON": "Leber_Hereditary_Optic_Neuropathy",
    "LIMM": "Lethal_Infantile_Mitochondrial_Myopathy",
    "LS": "Leigh_Syndrome",
    "LVNC": "Left_Ventricular_Noncompaction",
    "Longevity": "Long_life",
    "MDD": "Major_Depressive_Disorder",
    "MDM": "Myopathy_and_Diabetes_Mellitus",
    "MELAS": "Mitochondrial_Encephalomyopathy_Lactic_Acidosis_and_Stroke-like_episodes",
    "MEPR": "Myoclonic_Epilepsy_and_Psychomotor_Regression",
    "MERME": "Myoclonic_Epilepsy_and_Ragged_Red_Muscle_Fibers/"
             "Mitochondrial_Encephalomyopathy_Lactic_Acidosis_and_Stroke-like_episodes_overlap_disease",
    "MERRF": "Myoclonic_Epilepsy_and_Ragged_Red_Muscle_Fibers",
    "MEc": "Mitochondrial_Encephalocardiomyopathy",
    "MEm": "Mitochondrial_Encephalomyopathy",
    "MHCM": "Maternally_Inherited_Hypertrophic_CardioMyopathy",
    "MI": "Myocardial_Infarction",
    "MICM": "Maternally_Inherited_Cardiomyopathy",
    "MIDD": "Maternally_Inherited_Diabetes_and_Deafness",
    "MIDM": "Maternally_inherited_diabetes_mellitus",
    "MILS": "Maternally_Inherited_Leigh_Syndrome",
    "MM": "Mitochondrial_Myopathy",
    "MMC": "Maternal_Myopathy_and_Cardiomyopathy",
    "MR": "Mental_Retardation",
    "NAION": "Nonarteritic_Anterior_Ischemic_Optic_Neuropathy",
    "NARP": "Neurogenic_muscle_weakness_Ataxia_and_Retinitis_Pigmentosa",
    "NIDDM": "Non-Insulin_Dependent_Diabetes_Mellitus",
    "NRTI-PN": "Antiretroviral_Therapy-Associated_Peripheral_Neuropathy",
    "NSHL": "Non-Syndromic_Hearing_Loss",
    "OAT": "Oligoasthenoteratozoospermia",
    "PD": "Parkinson_Disease",
    "PEG": "Pseudoexfoliation_Glaucoma",
    "PEM": "Progressive_Encephalopathy",
    "PEO": "Progressive_External_Ophthalmoplegia",
    "PME": "Progressive_Myoclonus_Epilepsy",
    "POAG": "Primary_Open_Angle_Glaucoma",
    "RP": "Retinitis_Pigmentosa",
    "RTT": "Rett_Syndrome",
    "SIDS": "Sudden_Infant_Death_Syndrome",
    "SNHL": "Sensorineural_Hearing_Loss",
    "SZ": "Schizophrenia"
}


SUB_ABBREVIATIONS_LIST = ['AD', 'AMD', 'AMDF', 'HCM', 'LD', 'LS', 'MI', 'MM', 'PEO']


def _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, sub_abbreviation, sup_abbreviation):

    if disease.count(sup_abbreviation) > 0:
        disease.replace(sup_abbreviation, MITOMAP_DISEASES_ABBREVIATIONS[sup_abbreviation])
    disease = disease.replace(sub_abbreviation, MITOMAP_DISEASES_ABBREVIATIONS[sub_abbreviation])

    return disease


def _replace_abbreviations_in_disease_name(disease, abbreviation):

    # AD < ADPD
    # AMD < AMDF
    # BSN < FBSN
    # DM < DMDF | MDM | MIDM | NIDDM
    # HCM < MHCM
    # LD < LDYT
    # LS < MILS
    # MI < MICM | MIDD | MILS
    # MM < MMC | LIMM
    # PD < ADPD
    # PEO < CPEO
    # RP < NARP

    # AD < ADPD
    if abbreviation == 'AD':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'AD', 'ADPD')

    # AMD < AMDF
    elif abbreviation == 'AMD':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'AMD', 'AMDF')

    # BSN < FBSN
    elif abbreviation == 'BSN':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'BSN', 'FBSN')

    # DM < DMDF|MDM|MIDM|NIDDM
    elif abbreviation == 'DM':
        if disease.count('DMDF') > 0:
            disease.replace('DMDF', MITOMAP_DISEASES_ABBREVIATIONS['DMDF'])
        elif disease.count('MDM') > 0:
            disease.replace('MDM', MITOMAP_DISEASES_ABBREVIATIONS['MDM'])
        elif disease.count('MIDM') > 0:
            disease.replace('MIDM', MITOMAP_DISEASES_ABBREVIATIONS['MIDM'])
        elif disease.count('NIDDM') > 0:
            disease.replace('MIDDM', MITOMAP_DISEASES_ABBREVIATIONS['NIDDM'])
        disease.replace('DM', MITOMAP_DISEASES_ABBREVIATIONS['DM'])

    # HCM < MHCM
    elif abbreviation == 'HCM':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'HCM', 'MHCM')

    # LD < LDYT
    elif abbreviation == 'LD':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'LD', 'LDYT')

    # LS < MILS
    elif abbreviation == 'LS':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'LS', 'MILS')

    # MI < MICM|MIDD|MILS
    elif abbreviation == 'MI':
        if disease.count('MICM') > 0:
            disease.replace('MICM', MITOMAP_DISEASES_ABBREVIATIONS['MICM'])
        elif disease.count('MIDD') > 0:
            disease.replace('MIDD', MITOMAP_DISEASES_ABBREVIATIONS['MIDD'])
        elif disease.count('MILS') > 0:
            disease.replace('MILS', MITOMAP_DISEASES_ABBREVIATIONS['MILS'])
        disease.replace('MI', MITOMAP_DISEASES_ABBREVIATIONS['MI'])

    # MM < MMC|LIMM
    elif abbreviation == 'MM':
        if disease.count('MMC') > 0:
            disease.replace('MMC', MITOMAP_DISEASES_ABBREVIATIONS['MMC'])
        elif disease.count('LIMM') > 0:
            disease.replace('LIMM', MITOMAP_DISEASES_ABBREVIATIONS['LIMM'])
        disease.replace('MM', MITOMAP_DISEASES_ABBREVIATIONS['MM'])

    # PEO < CPEO
    elif abbreviation == 'PEO':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'PEO', 'COPEO')

    # RP < NARP
    elif abbreviation == 'RP':
        disease = _replace_sub_abbreviation_having_only_one_sup_abbreviation(disease, 'RP', 'NARP')

    return disease


def _format_disease_name(disease):

    for abbreviation in MITOMAP_DISEASES_ABBREVIATIONS.keys():

        if disease.count(abbreviation) > 0:
            if abbreviation in SUB_ABBREVIATIONS_LIST:
                disease = _replace_abbreviations_in_disease_name(disease, abbreviation)
            else:
                disease = disease.replace(abbreviation, MITOMAP_DISEASES_ABBREVIATIONS[abbreviation])

    return disease


class AnnotationInfo(object):

    def __init__(self, info_id: str, info_number: str, info_type: str, info_description: str, info_source=None,
                 info_version=None, source_field_id=None):
        super().__init__()
        self.info_id = info_id
        self.info_number = info_number
        self.info_type = info_type
        self.info_description = info_description
        self.info_source = info_source
        self.info_version = info_version
        self.source_field_id = source_field_id

    def get_header_info_line(self):
        info_source = '' if None else ',Source="{}"'.format(self.info_source)
        info_version = '' if None else ',Version="{}"'.format(self.info_version)

        return '##INFO=<ID={},Number={},Type={},Description="{}"{}{}>'.format(self.info_id, self.info_number,
                                                                              self.info_type,
                                                                              self.info_description, info_source,
                                                                              info_version)


class _Annotations(object):
    AnnotationsList: List[AnnotationInfo] = list()

    MITOMAP_DISEASE = AnnotationInfo(
        info_id='MITOMAP_DISEASE',
        info_number='A',
        info_type='String',
        info_description='Putative Disease Association - MITOMAP. Format: disease : (...) : disease',
        info_source='MITOMAP',
        source_field_id='Disease')
    AnnotationsList.append(MITOMAP_DISEASE)

    MITOMAP_DISEASE_STATUS = AnnotationInfo(
        info_id='MITOMAP_DISEASE_STATUS',
        info_number='A',
        info_type='String',
        info_description='Disease Association Status - MITOMAP',
        info_source='MITOMAP',
        source_field_id='DiseaseStatus')
    AnnotationsList.append(MITOMAP_DISEASE_STATUS)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=
    """
    The script modifies original MITOMAP diseases.vcf file downloaded from
    MITOMAP  Resources site (https://mitomap.org/foswiki/bin/view/MITOMAP/Resources)
    
    The script performs the following actions:
        - formats contents of Disease field:
             + changes its name to MITOMAP_DISEASE
             + replaces disease abbreviations with disease names
             + replaces +, &, / with ,
             + replaces whitspaces with _
             + changes the format od field to: disease : (...) : disease
             + changes the number of values to: one per allele (Number=A)
        - formats contents of DiseaseStatus field:
            + changes its name to MITOMAP_DISEASE_STATUS
            + replaces whitspaces with _
            + changes the number of values to: one per allele (Number=A)
        - removes remaining INFO fields
        - changes chromosome naming convention from MT to chrM.
        - removes information about reference allels (as it is not well handled with VCF format and there are only
          several reference allels in the whole MITOMAP database)
    """)

    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('MITOMAP_DISEASES_VCF', type=str,
                        help="path to MITOMAP database diseases.vcf. File must have multiallelic sites split and "
                             "must be left-indel normalized")

    args = parser.parse_args()

    # mitomap_diseases_vcf_path = "/home/ltw/Intelliseq/workflows/src/main/scripts/resources-tools/disease.vcf" #args.MITOMAP_DISEASES_VCF
    mitomap_diseases_vcf_path = args.MITOMAP_DISEASES_VCF


    # Open original MITOMAP database and create MITOMAP annotation output file:
    with VariantFile(mitomap_diseases_vcf_path) as mitomap_original_vcf, \
            VariantFile('-','w') as mitomap_annotation_vcf:

        # CREATE OUTPUT VCF HEADER #
        ############################

        # Add fileDate and reference fields
        mitomap_annotation_vcf.header.add_line("##fileDate=" + datetime.date.today().strftime("%d-%m-%Y"))
        mitomap_annotation_vcf.header.add_line("##reference=grch38")

        # Add INFO fields
        version = [record.value for record in mitomap_original_vcf.header.records if record.key == "fileDate"][0]
        for annotation in _Annotations.AnnotationsList:
            annotation.info_version = version
            mitomap_annotation_vcf.header.add_line(annotation.get_header_info_line())

        # Add contig fields
        mitomap_annotation_vcf.header.add_line("##contig=<ID=chrM>")

        # BUILD OUTPUT VCF BODY #
        #########################

        for original_record in mitomap_original_vcf.fetch():

            REF = original_record.alleles[0]
            ALTS = original_record.alleles[1:]

            if not (len(ALTS) == 1 and REF == ALTS[0]):

                if REF in ALTS:
                    original_record.alleles = tuple(
                        [original_record.alleles[0]] + [x for x in original_record.alleles[1:] if x != REF])

                # Create and populate new MITOMAP annotation file record
                new_record = mitomap_annotation_vcf.new_record(
                    contig="chrM", start=original_record.start, stop=original_record.stop,
                    alleles=original_record.alleles,
                    id=original_record.id, qual=original_record.qual, filter=original_record.filter, info=None)

                # Add MITOMAP_DISEASE
                # and MITOMAP_DISEASE_STATUS fields

                original_diseases = ','.join(original_record.info[_Annotations.MITOMAP_DISEASE.source_field_id]) \
                    .replace(',-', ',').replace('-/-', ',').split('|')
                original_diseases_statuses = ','.join(
                    original_record.info[_Annotations.MITOMAP_DISEASE_STATUS.source_field_id]) \
                    .replace(' ', '_').replace(',', '').split('|')

                diseases = []
                diseases_statuses = []

                for alt_index in range(len(ALTS)):

                    if ALTS[alt_index] != REF:

                        original_diseases_alt = [x.strip() for x in
                                                 original_diseases[alt_index].replace('&', ',').replace('+', ',').replace(
                                                     '/', ',').split(',')]

                        for index in range(len(original_diseases_alt)):

                            disease = original_diseases_alt[index]

                            try:
                                original_diseases_alt[index] = MITOMAP_DISEASES_ABBREVIATIONS[disease]
                            except KeyError:
                                original_diseases_alt[index] = _format_disease_name(disease)

                        diseases.append(":".join(original_diseases_alt))
                        diseases_statuses.append(original_diseases_statuses[alt_index])

                new_record.info[_Annotations.MITOMAP_DISEASE.info_id] = tuple(diseases)
                new_record.info[_Annotations.MITOMAP_DISEASE_STATUS.info_id] = tuple(diseases_statuses)

                mitomap_annotation_vcf.write(new_record)

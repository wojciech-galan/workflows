#!/usr/bin/python3

__version__ = '1.0.1'

import requests
import json
import argparse
from typing import Dict, List
#from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)


def get_uniprot_mutagenesis_data(accession: str) -> List:
    requestURL = "https://www.ebi.ac.uk/proteins/api/features?offset=0&size=100&accession="+accession+"&organism=Homo%20sapiens&types=MUTAGEN"
    r = requests.get(requestURL, headers={ "Accept" : "application/json"})
    if not r.ok:
        #r.raise_for_status()
        result = []
    else: 
        responseBody = r.text
        uniprot_data = json.loads(responseBody)
        try:
            all_features = uniprot_data[0]["features"]
            result = [[i["begin"],i["end"],i["alternativeSequence"],i["description"]] for i in all_features if i["category"] == "MUTAGENESIS"]
        except (KeyError,IndexError):
            result = []
    return result    
    #print(result)


def get_uniprot_sequence_data(accession: str, begin: int, end: int) -> str:
    requestURL = "https://www.ebi.ac.uk/proteins/api/features?offset=0&size=100&accession="+accession+"&organism=Homo%20sapiens&categories=SEQUENCE_INFORMATION"
    r = requests.get(requestURL, headers={ "Accept" : "application/json"})
    if not r.ok:
        #r.raise_for_status()
        result = ""
    else: 
        responseBody = r.text
        uniprot_data = json.loads(responseBody)
        try:
            result = uniprot_data[0]["sequence"][begin-1:end]
        except (KeyError,IndexError):
            result = ""
    return result    


def make_final_list(lst: List,text: str) -> List:
   return lst[0:2]+[text]+lst[2:]


def main(human_genes: str, uniprot_output: str) -> None:
    with open(uniprot_output, "w") as f1, open(human_genes) as f2:
        for line in f2:
            gene, accession = line.strip().split("\t")
            # print(gene)
            print(accession)
            data = get_uniprot_mutagenesis_data(accession)
            print(data)
            if data != []:
                try:
                    final_data = [ make_final_list(i,get_uniprot_sequence_data(accession,int(i[0]),int(i[1]))) for i  in data]
                except (TypeError , IndexError,  ValueError ):
                    final_data = [ make_final_list(i,"") for i  in data]
                text = '"'+gene+'_'+accession+'"'+' : '+str(final_data)+'\n'
                f1.write(text)
        print("all genes checked!")


if __name__=='__main__' :
    parser = argparse.ArgumentParser(
        description='Prepares json file needed for annotations with UniProt functional data: '
                    'gene_uniprotID => [[start1, end1, ref_eq1, alt_seq1, description1], [start2, end2, ref_seq2, alt_seq2, description2}, ...]')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--input', type=str, required=True, help="Path to tab deliminated file with gene_name and UniProtID")
    parser.add_argument('-o', '--output', type=str, required=True, help="Path to output json file")
    args = parser.parse_args()
    main(args.input, args.output)

#quotes in the otput file must be changed with the following command:
#sed 's/\[\x27/\[\"/g' uniprot_mutagenesis.json | sed 's/\x27, \x27/\", \"/g' | sed 's/\x27\]/\"\]/g' | sed 's/\x27, \"/\", \"/g' > uniprot_mutagenesis_temp.json
#mv uniprot_mutagenesis_temp.json uniprot_mutagenesis.json 
#awk '$2 != "" ' human_genes_temp.txt | sort | uniq > human-genes-uniprot-ids.txt
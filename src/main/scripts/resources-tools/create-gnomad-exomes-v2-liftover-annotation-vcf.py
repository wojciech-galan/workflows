#!/usr/bin/env python3

__version__ = '0.0.1'


import argparse
from Bio import bgzf
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
This script creates chromosome-wise (chr1, ..., chr22, chrX, chrY-and-the-rest),
bgzippef VCFs, containing frequency annotation based on gnomAD genomes v2 liftover
frequencies database for exomes.
""")

parser.add_argument('OUTPUT_DIR', type=str, help="path to output directory")
parser.add_argument('-v', '--version', action='version', version=__version__)
args = parser.parse_args()


args = parser.parse_args()
output_directory = args.OUTPUT_DIR


header = []
header.append("##fileformat=VCFv4.3")
header.append("##reference=Homo_sapiens_assembly38.fasta")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AC,Number=A,Type=Integer,Description=\"Alternate allele count for samples - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AN,Number=1,Type=Integer,Description=\"Total number of alleles in samples - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_nhomalt,Number=A,Type=Integer,Description=\"Count of homozygous individuals in samples - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples in the controls subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_nhomalt,Number=A,Type=Integer,Description=\"Count of homozygous individuals in samples in the controls subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples in the non_neuro subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt,Number=A,Type=Integer,Description=\"Count of homozygous individuals in samples in the non_neuro subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples in the non_cancer subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt,Number=A,Type=Integer,Description=\"Count of homozygous individuals in samples in the non_cancer subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_popmax,Number=A,Type=String,Description=\"Population with maximum AF - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_popmax_AF,Number=A,Type=Float,Description=\"Alternate allele frequency in samples - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_nhomalt_popmax,Number=A,Type=Integer,Description=\"Count of homozygous individuals in the population with the maximum allele frequency - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_popmax,Number=A,Type=String,Description=\"Population with maximum AF in the controls subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_popmax,Number=A,Type=Float,Description=\"Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the controls subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_nhomalt_popmax,Number=A,Type=Integer,Description=\"Count of homozygous individuals in the population with the maximum allele frequency in the controls subset - gnomAD exomes\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_popmax,Number=A,Type=String,Description=\"Population with maximum AF in the non_neuro subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_popmax,Number=A,Type=Float,Description=\"Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_neuro subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt_popmax,Number=A,Type=Integer,Description=\"Count of homozygous individuals in the population with the maximum allele frequency in the non_neuro subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_popmax,Number=A,Type=String,Description=\"Population with maximum AF in the non_cancer subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_popmax,Number=A,Type=Float,Description=\"Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_cancer subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt_popmax,Number=A,Type=Integer,Description=\"Count of homozygous individuals in the population with the maximum allele frequency in the non_cancer subset - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_afr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_afr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of African-American ancestry in the controls subset (afr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_afr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of African-American ancestry in the non_neuro subset (afr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_afr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of African-American ancestry in the non_cancer subset (afr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_amr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_amr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Latino ancestry in the controls subset (amr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_amr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Latino ancestry in the non_neuro subset (amr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_amr,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Latino ancestry in the non_cancer subset (amr) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_asj,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_asj,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the controls subset (asj) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_asj,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_neuro subset (asj) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_asj,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_cancer subset (asj) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_eas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_eas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of East Asian ancestry in the controls subset (eas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_eas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of East Asian ancestry in the non_neuro subset (eas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_eas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of East Asian ancestry in the non_cancer subset (eas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations,Number=A,Type=String,Description=\"Alternate allele frequencies in samples of Korean ancestry (kor), Japanese ancestry (jap), non-Korean, non-Japanese East Asian ancestry (oea) in all samples, in the controls subset, in the non_neuro subset and in the non_cancer subset: 'Allele | subpopulation_name : AF_eas_kor : controls_AF_eas_kor : non_neuro_AF_eas_kor : non_cancer_AF_eas_kor : | subpopulation_name : AF_eas_jpn : controls_AF_eas_jpn : non_neuro_AF_eas_jpn : non_cancer_AF_eas_jpn : | subpopulation_name : AF_eas_oea : controls_AF_eas_oea : non_neuro_AF_eas_oea : non_cancer_AF_eas_oea' - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_fin,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_fin,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Finnish ancestry in the controls subset (fin) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_fin,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Finnish ancestry in the non_neuro subset (fin) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_fin,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of Finnish ancestry in the non_cancer subset (fin) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_nfe,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_nfe,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of non-Finnish European ancestry in the controls subset (nfe) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_nfe,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of non-Finnish European ancestry in the non_neuro subset (nfe) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_nfe,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of non-Finnish European ancestry in the non_cancer subset (nfe) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations,Number=A,Type=String,Description=\"Alternate allele frequencies in samples of  Bulgarian ancestry (bgr), Estonian ancestry (est), North-Western European ancestry (nwe), Southern European ancestry (seu), Swedish ancestry (swe), non-Finnish but otherwise indeterminate European ancestry ancestry (onf) in all samples, in the controls subset, in the non_neuro subset: 'Allele | subpopulation_name : AF_nfe_bgr : controls_AF_nfe_bgr : non_neuro_AF_nfe_bgr : non_cancer_AF_nfe_bgr : | subpopulation_name : AF_nfe_est : controls_AF_nfe_est : non_neuro_AF_nfe_est : non_cancer_AF_nfe_est : | subpopulation_name : AF_nfe_nwe : controls_AF_nfe_nwe : non_neuro_AF_nfe_nwe : non_cancer_AF_nfe_nwe : | subpopulation_name : AF_nfe_seu : controls_AF_nfe_seu : non_neuro_AF_nfe_seu : non_cancer_AF_nfe_seu : | subpopulation_name : AF_nfe_swe : controls_AF_nfe_swe : non_neuro_AF_nfe_swe : non_cancer_AF_nfe_swe : | subpopulation_name : AF_nfe_onf : controls_AF_nfe_onf : non_neuro_AF_nfe_onf : non_cancer_AF_nfe_onf' - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_sas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of South Asian ancestry (sas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_sas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of South Asian ancestry in the controls subset (sas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_sas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of South Asian ancestry in the non_neuro subset (sas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_sas,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of South Asian ancestry in the non_cancer subset (sas) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_AF_oth,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of uncertain ancestry (oth) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_controls_AF_oth,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of uncertain ancestry in the controls subset (oth) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_neuro_AF_oth,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of uncertain ancestry in the non_neuro subset (oth) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_non_cancer_AF_oth,Number=A,Type=Float,Description=\"Alternate allele frequency in samples of uncertain ancestry in the non_cancer subset (oth) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_segdup,Number=0,Type=Flag,Description=\"Variant falls within a segmental duplication region - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_lcr,Number=0,Type=Flag,Description=\"Variant falls within a low complexity region - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_decoy,Number=0,Type=Flag,Description=\"Variant falls within a reference decoy region - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_nonpar,Number=0,Type=Flag,Description=\"Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("##INFO=<ID=ISEQ_GNOMAD_EXOMES_FILTER_STATUS,Number=A,Type=String,Description=\"Filter status. Format: filter_status : (...) : filter_status.. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (Failed random forest filtering thresholds of 0.055272738028512555, 0.20641025579497013 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD exomes\",Source=\"gnomAD\",Version=\"gnomAD v2.1.1, lifted over to GRCh38\">")
header.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")



chromosomes = ["chr" + str(i) for i in range(1, 23)] + ["chrX", "chrY-and-the-rest"]
handlers = [bgzf.BgzfWriter(output_directory + "/" + x + ".gnomad-exomes.vcf.gz") for x in chromosomes]



fields_order = ["ISEQ_GNOMAD_EXOMES_AC",
                "ISEQ_GNOMAD_EXOMES_AN",
                "ISEQ_GNOMAD_EXOMES_AF",
                "ISEQ_GNOMAD_EXOMES_nhomalt",
                "ISEQ_GNOMAD_EXOMES_controls_AF",
                "ISEQ_GNOMAD_EXOMES_controls_nhomalt",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF",
                "ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF",
                "ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt",
                "ISEQ_GNOMAD_EXOMES_popmax",
                "ISEQ_GNOMAD_EXOMES_popmax_AF",
                "ISEQ_GNOMAD_EXOMES_nhomalt_popmax",
                "ISEQ_GNOMAD_EXOMES_controls_popmax",
                "ISEQ_GNOMAD_EXOMES_controls_AF_popmax",
                "ISEQ_GNOMAD_EXOMES_controls_nhomalt_popmax",
                "ISEQ_GNOMAD_EXOMES_non_neuro_popmax",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_popmax",
                "ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt_popmax",
                "ISEQ_GNOMAD_EXOMES_non_cancer_popmax",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_popmax",
                "ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt_popmax",
                "ISEQ_GNOMAD_EXOMES_AF_afr",
                "ISEQ_GNOMAD_EXOMES_controls_AF_afr",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_afr",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_afr",
                "ISEQ_GNOMAD_EXOMES_AF_amr",
                "ISEQ_GNOMAD_EXOMES_controls_AF_amr",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_amr",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_amr",
                "ISEQ_GNOMAD_EXOMES_AF_asj",
                "ISEQ_GNOMAD_EXOMES_controls_AF_asj",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_asj",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_asj",
                "ISEQ_GNOMAD_EXOMES_AF_eas",
                "ISEQ_GNOMAD_EXOMES_controls_AF_eas",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_eas",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_eas",
                "ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations",
                "ISEQ_GNOMAD_EXOMES_AF_fin",
                "ISEQ_GNOMAD_EXOMES_controls_AF_fin",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_fin",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_fin",
                "ISEQ_GNOMAD_EXOMES_AF_nfe",
                "ISEQ_GNOMAD_EXOMES_controls_AF_nfe",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_nfe",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_nfe",
                "ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations",
                "ISEQ_GNOMAD_EXOMES_AF_sas",
                "ISEQ_GNOMAD_EXOMES_controls_AF_sas",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_sas",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_sas",
                "ISEQ_GNOMAD_EXOMES_AF_oth",
                "ISEQ_GNOMAD_EXOMES_controls_AF_oth",
                "ISEQ_GNOMAD_EXOMES_non_neuro_AF_oth",
                "ISEQ_GNOMAD_EXOMES_non_cancer_AF_oth",
                "ISEQ_GNOMAD_EXOMES_segdup",
                "ISEQ_GNOMAD_EXOMES_lcr",
                "ISEQ_GNOMAD_EXOMES_decoy",
                "ISEQ_GNOMAD_EXOMES_nonpar",
                "ISEQ_GNOMAD_EXOMES_FILTER_STATUS"]

fields_dictionary = {
    "AC":                           "ISEQ_GNOMAD_EXOMES_AC",
    "AN":                           "ISEQ_GNOMAD_EXOMES_AN",
    "AF":                           "ISEQ_GNOMAD_EXOMES_AF",
    "nhomalt":                      "ISEQ_GNOMAD_EXOMES_nhomalt",
    "controls_AF":                  "ISEQ_GNOMAD_EXOMES_controls_AF",
    "controls_nhomalt":             "ISEQ_GNOMAD_EXOMES_controls_nhomalt",
    "non_neuro_AF":                 "ISEQ_GNOMAD_EXOMES_non_neuro_AF",
    "non_neuro_nhomalt":            "ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt",
    "non_cancer_AF":                "ISEQ_GNOMAD_EXOMES_non_cancer_AF",
    "non_cancer_nhomalt":           "ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt",
    "popmax":                       "ISEQ_GNOMAD_EXOMES_popmax",
    "AF_popmax":                    "ISEQ_GNOMAD_EXOMES_popmax_AF",
    "nhomalt_popmax":               "ISEQ_GNOMAD_EXOMES_nhomalt_popmax",
    "controls_popmax":              "ISEQ_GNOMAD_EXOMES_controls_popmax",
    "controls_AF_popmax":           "ISEQ_GNOMAD_EXOMES_controls_AF_popmax",
    "controls_nhomalt_popmax":      "ISEQ_GNOMAD_EXOMES_controls_nhomalt_popmax",
    "non_neuro_popmax":             "ISEQ_GNOMAD_EXOMES_non_neuro_popmax",
    "non_neuro_AF_popmax":          "ISEQ_GNOMAD_EXOMES_non_neuro_AF_popmax",
    "non_neuro_nhomalt_popmax":     "ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt_popmax",
    "non_cancer_popmax":            "ISEQ_GNOMAD_EXOMES_non_cancer_popmax",
    "non_cancer_AF_popmax":         "ISEQ_GNOMAD_EXOMES_non_cancer_AF_popmax",
    "non_cancer_nhomalt_popmax":    "ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt_popmax",
    "AF_afr":                       "ISEQ_GNOMAD_EXOMES_AF_afr",
    "controls_AF_afr":              "ISEQ_GNOMAD_EXOMES_controls_AF_afr",
    "non_neuro_AF_afr":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_afr",
    "non_cancer_AF_afr":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_afr",
    "AF_amr":                       "ISEQ_GNOMAD_EXOMES_AF_amr",
    "controls_AF_amr":              "ISEQ_GNOMAD_EXOMES_controls_AF_amr",
    "non_neuro_AF_amr":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_amr",
    "non_cancer_AF_amr":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_amr",
    "AF_asj":                       "ISEQ_GNOMAD_EXOMES_AF_asj",
    "controls_AF_asj":              "ISEQ_GNOMAD_EXOMES_controls_AF_asj",
    "non_neuro_AF_asj":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_asj",
    "non_cancer_AF_asj":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_asj",
    "AF_eas":                       "ISEQ_GNOMAD_EXOMES_AF_eas",
    "controls_AF_eas":              "ISEQ_GNOMAD_EXOMES_controls_AF_eas",
    "non_neuro_AF_eas":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_eas",
    "non_cancer_AF_eas":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_eas",
    "AF_fin":                       "ISEQ_GNOMAD_EXOMES_AF_fin",
    "controls_AF_fin":              "ISEQ_GNOMAD_EXOMES_controls_AF_fin",
    "non_neuro_AF_fin":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_fin",
    "non_cancer_AF_fin":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_fin",
    "AF_nfe":                       "ISEQ_GNOMAD_EXOMES_AF_nfe",
    "controls_AF_nfe":              "ISEQ_GNOMAD_EXOMES_controls_AF_nfe",
    "non_neuro_AF_nfe":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_nfe",
    "non_cancer_AF_nfe":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_nfe",
    "AF_sas":                       "ISEQ_GNOMAD_EXOMES_AF_sas",
    "controls_AF_sas":              "ISEQ_GNOMAD_EXOMES_controls_AF_sas",
    "non_neuro_AF_sas":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_sas",
    "non_cancer_AF_sas":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_sas",
    "AF_oth":                       "ISEQ_GNOMAD_EXOMES_AF_oth",
    "controls_AF_oth":              "ISEQ_GNOMAD_EXOMES_controls_AF_oth",
    "non_neuro_AF_oth":             "ISEQ_GNOMAD_EXOMES_non_neuro_AF_oth",
    "non_cancer_AF_oth":            "ISEQ_GNOMAD_EXOMES_non_cancer_AF_oth",
    "segdup":                       "ISEQ_GNOMAD_EXOMES_segdup",
    "lcr":                          "ISEQ_GNOMAD_EXOMES_lcr",
    "decoy":                        "ISEQ_GNOMAD_EXOMES_decoy",
    "nonpar":                       "ISEQ_GNOMAD_EXOMES_nonpar"

}

EAS_subpopulations = ["AF_eas_kor",
                        "controls_AF_eas_kor",
                        "non_neuro_AF_eas_kor",
                        "non_cancer_AF_eas_kor",
                        "AF_eas_jpn",
                        "controls_AF_eas_jpn",
                        "non_neuro_AF_eas_jpn",
                        "non_cancer_AF_eas_jpn",
                        "AF_eas_oea",
                        "controls_AF_eas_oea",
                        "non_neuro_AF_eas_oea",
                        "non_cancer_AF_eas_oea"]

NFE_subpopulations = ["AF_nfe_bgr",
                        "controls_AF_nfe_bgr",
                        "non_neuro_AF_nfe_bgr",
                        "non_cancer_AF_nfe_bgr",
                        "AF_nfe_est",
                        "controls_AF_nfe_est",
                        "non_neuro_AF_nfe_est",
                        "non_cancer_AF_nfe_est",
                        "AF_nfe_nwe",
                        "controls_AF_nfe_nwe",
                        "non_neuro_AF_nfe_nwe",
                        "non_cancer_AF_nfe_nwe",
                        "AF_nfe_seu",
                        "controls_AF_nfe_seu",
                        "non_neuro_AF_nfe_seu",
                        "non_cancer_AF_nfe_seu",
                        "AF_nfe_swe",
                        "controls_AF_nfe_swe",
                        "non_neuro_AF_nfe_swe",
                        "non_cancer_AF_nfe_swe",
                        "AF_nfe_onf",
                        "controls_AF_nfe_onf",
                        "non_neuro_AF_nfe_onf",
                        "non_cancer_AF_nfe_onf"]


def change_scientific_0_to_ordinary_0(x):
    if x == "0.00000e+00":
        return "0"
    else:
        return x


for handle in handlers:
    handle.write("\n".join(header) + "\n")

for raw_line in stdin:

    if not raw_line.startswith("#"):

        line = raw_line.strip().split("\t")
        ALT = line[4].split(",")
        FILTER = line[6]
        CHR = line[0]

        INFO_tuples = [
            (fields_dictionary[x.split("=")[0]], x.split("=")[1]) if "=" in x else (fields_dictionary[x], None) for x in
            line[7].split(";") if
            ("=" in x and x.split("=")[0] in fields_dictionary.keys()) or x in fields_dictionary.keys()]

        EAS_subpopulations_dict = dict((k, v) for k, v in
                                       [(x.split("=")[0], x.split("=")[1]) for x in line[7].split(";") if
                                        x.split("=")[0] in EAS_subpopulations])
        NFE_subpopulations_dict = dict((k, v) for k, v in
                                       [(x.split("=")[0], x.split("=")[1]) for x in line[7].split(";") if
                                        x.split("=")[0] in NFE_subpopulations])

        ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations = []
        ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations = []

        for alt_index in range(0, len(ALT)):

            AF_eas_subpopulations_ALT = ALT[alt_index]
            AF_nfe_subpopulations_ALT = ALT[alt_index]

            for index in range(0, len(EAS_subpopulations)):

                if index % 4 == 0:
                    AF_eas_subpopulations_ALT += "|" + EAS_subpopulations[index][-3:] + ":"
                else:
                    AF_eas_subpopulations_ALT += ":"

                if EAS_subpopulations[index] in EAS_subpopulations_dict.keys():

                    try:
                        AF_eas_subpopulations_ALT += change_scientific_0_to_ordinary_0(EAS_subpopulations_dict[EAS_subpopulations[index]].split(",")[alt_index])
                    except IndexError:
                        AF_eas_subpopulations_ALT += "."

                else:
                    AF_eas_subpopulations_ALT += "."

            for index in range(0, len(NFE_subpopulations)):

                if index % 4 == 0:
                    AF_nfe_subpopulations_ALT += "|" + NFE_subpopulations[index][-3:] + ":"
                else:
                    AF_nfe_subpopulations_ALT += ":"

                if NFE_subpopulations[index] in NFE_subpopulations_dict.keys():

                    try:
                        AF_nfe_subpopulations_ALT += change_scientific_0_to_ordinary_0(NFE_subpopulations_dict[NFE_subpopulations[index]].split(",")[alt_index])
                    except IndexError:
                        AF_nfe_subpopulations_ALT += "."

                else:
                    AF_nfe_subpopulations_ALT += "."

            ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations.append(AF_eas_subpopulations_ALT)
            ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations.append(AF_nfe_subpopulations_ALT)

        INFO_tuples.append(("ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations", ",".join(ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations)))
        INFO_tuples.append(("ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations", ",".join(ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations)))
        INFO_tuples.append(("ISEQ_GNOMAD_EXOMES_FILTER_STATUS", FILTER.replace(";", ":")))

        INFO_tuples_sorted = sorted(INFO_tuples, key=lambda x: fields_order.index(x[0]))

        line[7] = ";".join([x + "=" + change_scientific_0_to_ordinary_0(y) if y is not None else x for (x,y) in INFO_tuples_sorted])
        line[6] = "."

        if CHR in chromosomes:
            handlers[chromosomes.index(CHR)].write('\t'.join(line) + "\n")
        else:
            handlers[chromosomes.index("chrY-and-the-rest")].write('\t'.join(line) + "\n")

for handle in handlers:
    handle.close()

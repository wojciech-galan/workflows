#!/usr/bin/env python3

from sys import stdin

for raw_line in stdin:

    if raw_line.startswith("##INFO=<ID=RS_INFO"):
        print(raw_line.strip())
        print("##FILTER=<ID=NovelAllele,Description=\"Alternative allele present in the sample on a given position is not present in any in dbSNP record\">")
        print("##FILTER=<ID=ComplexGenotype,Description=\"Genotype of a sample consists of ALT alleles of two different rsID\">")
    elif raw_line.startswith("#"):
        print(raw_line.strip())
    else:

        line = raw_line.strip().split("\t")

        if line[4] != ".":

            GENOTYPE = line[9]
            genotype = GENOTYPE.split(':')[0]
            INFO = line[7]
            RS_INFO = [info.replace('RS_INFO=', '') for info in INFO.split(";") if info.startswith("RS_INFO=")][0].split('|')
            ALTs = set(line[4].split(','))

            if genotype == "0/0":
                line[2] = ';'.join([x.split(':')[0] for x in RS_INFO])

            elif INFO.count("rs") == 1:

                rs_ALTs = set(RS_INFO[0].split(':')[2].split('^'))

                if not ALTs.issubset(rs_ALTs):
                    if line[6] == "LowQual":
                        line[6] = "LowQual;NovelAllele"
                    else:
                        line[6] = "NovelAllele"
                else:
                    line[2] = RS_INFO[0].split(':')[0]

            elif {genotype[0], genotype[2]}.issubset({'0', '1'}):

                    REF = line[3]

                    dbSNP_rs_IDs = [x.split(':')[0] for x in RS_INFO]
                    dbSNP_REFs = [x.split(':')[1] for x in RS_INFO]
                    dbSNP_ALTs = [x.split(':')[2] for x in RS_INFO]

                    for i in range(0, len(dbSNP_REFs)):
                        if REF == dbSNP_REFs[i]:
                            if ALTs.issubset(set(dbSNP_ALTs[i].split('^'))):
                                line[2] = ';'.join([line[2] + dbSNP_rs_IDs[i]]).replace('.', '')

                    if line[2] == '.':
                        if line[6] == "LowQual":
                            line[6] = "LowQual;NovelAllele"
                        else:
                            line[6] = "NovelAllele"

            else:

                if line[6] == "LowQual":
                    line[6] = "LowQual;ComplexGenotype"
                else:
                    line[6] = "ComplexGenotype"


            print("\t".join(line))

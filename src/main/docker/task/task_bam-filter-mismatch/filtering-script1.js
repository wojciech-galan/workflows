// script removing reads with high fraction of mismatches

function accept(rec) {
     if (rec.getReadUnmappedFlag()) return true;
     var cigar = rec.getCigar();
     if (cigar == null ) return true;

     var all = 0;
     var mismatch = 0;
     // mn total  mismatches length (including indels, excluding soft-clipped bases)
     var nm = rec.getAttribute("NM");
     if ( nm != null ) {mismatch += nm; }
     var i;
     var elements = rec.getCigarLength();
     for (i = 0; i < elements; i++) {
           var ce = cigar.getCigarElement(i);
           var len = ce.getLength();
           if ( ce.getOperator().name() != "S" && ce.getOperator().name() != "H") { all += len; }
           if ( ce.getOperator().name() == "D") { mismatch += 1; mismatch -= len; }
           if ( ce.getOperator().name() == "I" ) { mismatch+=1; mismatch -= len; }
           //print(all);
           //print(mismatch);
     }

     return mismatch/all < THRESHOLD;

 }

 accept(record);


import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-htseq-count@1.0.2/src/main/wdl/tasks/rna-seq-htseq-count/rna-seq-htseq-count.wdl" as rna_seq_htseq_count_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-abund-aggreg@1.0.0/src/main/wdl/tasks/rna-seq-abund-aggreg/rna-seq-abund-aggreg.wdl" as rna_seq_abund_aggreg_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.1/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow rna_seq_abundance {

  Array[File] bams
  Array[File] bams_bais
  File ensembl_gtf

  String stranded = "no" # Inne możliwe wartości: yes, reverse
  String mode = "union" # Inne możliwe wartości: intersection-strict and intersection-nonempty
  String nonunique = "none" # Inne możliwe wartości: all

  String analysis_id = "no_id_provided"

  String module_name = "rna_seq_abundance"
  String module_version = "1.0.3"

  #1
  scatter (index in range(length(bams))) {
      call rna_seq_htseq_count_task.rna_seq_htseq_count {
          input:
              bam = bams[index],
              bam_bai = bams_bais[index],
              ensembl_gtf = ensembl_gtf,
              stranded = stranded,
              mode = mode,
              nonunique = nonunique,
              index = index
      }
  }

  #2
  call rna_seq_abund_aggreg_task.rna_seq_abund_aggreg {
      input:
          htseq_count_gene = rna_seq_htseq_count.sample_tbl_htseq_count_gene_level,
          htseq_count_exon = rna_seq_htseq_count.sample_tbl_htseq_count_exon_level,
          analysis_id = analysis_id
  }

  # Merge bco, stdout, stderr files
  Array[File] bco_tasks = [rna_seq_abund_aggreg.bco]
  Array[File] stdout_tasks = [rna_seq_abund_aggreg.stdout_log]
  Array[File] stderr_tasks = [rna_seq_abund_aggreg.stderr_log]

  Array[Array[File]] bco_scatters = [bco_tasks, rna_seq_htseq_count.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, rna_seq_htseq_count.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, rna_seq_htseq_count.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
      input:
          bco_array = bco_array,
          stdout_array = stdout_array,
          stderr_array = stderr_array,
          module_name = module_name,
          module_version = module_version
  }

  output {

    File htseq_count_gene_level = rna_seq_abund_aggreg.htseq_count_gene_level
    File htseq_count_exon_level = rna_seq_abund_aggreg.htseq_count_exon_level

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}

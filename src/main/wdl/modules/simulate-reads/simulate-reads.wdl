import "https://gitlab.com/intelliseq/workflows/raw/simulate-neat@1.0.0/src/main/wdl/tasks/simulate-neat/simulate-neat.wdl" as simulate_neat_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-mem@1.2.0/src/main/wdl/tasks/fq-bwa-mem/latest/fq-bwa-mem.wdl" as fq_bwa_mem_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.3.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow simulate_reads_workflow {

  meta {
    name: 'simulate-reads'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2020 Intelliseq'
    description: '## This module simulates reads with NEAT-genReads and if FASTQ simulation was requested, performs BWA MEM aligment of generated FASTQ.'
    changes: '{"1.0.0": "new version of tasks"}'

    input_simulated_sample_name: '{"name": "Simulated sample name", "type": "String", "description": "Simulated sample name."}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "values": ["hg38", "grch38"], "default": "hg38", "description": "Version of reference genome that was used to generate gVCF file"}'
    input_chromosome: '{"name": "Chromosome","type": "String","required": "false","constraints": {"values": ["chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11","chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chr20","chr21","chr22","chrX","chrY-and-the-rest"]},"description": "Restrict reference genome to specified chromosoms (read will be simulated only from this chromosome)."}'

    input_vcf_gz_with_variants_to_insert: '{"name": "Bgzippped VCF file with mandatory variants","type": "File","required": "false", "extension": [".vcf.gz"] ,"description": "Bgzippped VCF file with mandatory variants. variants specified in the VCF filee will be inserted into simulated sample"}'
    input_user_defined_targeted_regions_bed_file: '{"name": "Targeted regions BED file (user defined)","type": "File","required": "false","description": "User defined BED file containing regions to generate reads from. If not specified, predefined targeted regions will be used. Default coverage for targeted regions is 98% of avarage coverage, default coverage outside targeted regions is 2% of avarage coverage."}'
    input_predefined_targeted_regions: '{"name": "Predefined targeted regions","type": "String","constraints": {"values": ["whole-reference-sequence","sureselect-human-all-exon-v6-r2","sureselect-human-all-exon-v6-utr-r2","sureselect-human-all-exon-v7"]},"default": "sureselect-human-all-exon-v6-r2","description": "Predefined sets of regions to to generate reads from. If provided, Targeted regions BED file (user defined) will be used instead of predefined set. Default coverage for targeted regions is 98% of avarage coverage, default coverage outside targeted regions is 2% of avarage coverage."}'
    input_off_target_coverage_scalar: '{"name": "Off-target coverage","type": "Float","default": "0.02","constraints": {"min": "0","max": "1"},"description": "Average off-target coverage as a percent of average coverage"}'

    input_ploidy: '{"name": "Ploidy","type": "Int","default": "2","constraints": {"min": "1"},"description": "Ploidy"}'
    input_read_length: '{"name": "Read length","type": "Int","default": "150","constraints": {"min": "20"},"description": "Read length."}'
    input_average_coverage: '{"name": "Average coverage","type": "Float","default": "150","constraints": {"min": "0"},"description": "Average coverage."}'
    input_simulate_paired_end_reads: '{"name": "Simulate paired end reads","type": "Boolean","default": "true","description": "Simulate paired end reads."}'
    input_paired_end_fragment_lenghth_mean: '{"name": "Paired end fragment lenghth mean","type": "Int","required": "false","default": "false","description": "Paired end fragment lenghth mean. Applicable only if pair-end option is set true."}'
    input_paired_end_fragment_lenghth_std: '{"name": "Paired end fragment lenghth standard deviation","type": "Float","required": "false","default": "false","description": "Paired end fragment lenghth standard deviation. Applicable only if pair-end option is set true."}'

    input_empirical_fragment_length_distribution: '{"name": "Empirical fragment length distribution (Advanced)", "type": "String", "constraints": {"values": ["wes-pe150-20m-150gb", "wgs-50x-pcr-free-150gb"]}, "required": "false", "description": "Select model of empirical fragment length distribution. Applicable only if pair-end option is set true. If specified, paired end fragment length mean and deviation will be ignored."}'
    input_empirical_GC_coverage_bias_distribution: '{"name": "Empirical GC coverage bias distribution (Advanced)", "type": "String", "constraints": {"values": ["wes-pe150-20m-150gb", "wgs-50x-pcr-free-150gb"]}, "required": "false", "description": "Select model of empirical GC coverage bias distribution."}'

    input_output_golden_bam_file: '{"name": "Output golden BAM file","type": "Boolean","default": "false","description": "Output golden BAM file"}'
    input_output_golden_vcf_file: '{"name": "Output golden VCF file","type": "Boolean","default": "true","description": "Output golden VCF file"}'
    input_output_fasta_instead_of_fastq: '{"name": "Output FASTA instead of FASTQ","type": "Boolean","default": "false","description": "Output FASTA instead of FASTQ"}'
    input_bypass_fastq_generation: '{"name": "Bypass FASTQ generation","type": "Boolean","default": "false","description": "Bypass FASTQ generation"}'
    input_rng_seed_value: '{"name": "RNG seed value","type": "Int","default": "1","description": "RNG seed value (Identical RNG value should produce identical runs of the program)."}'


    input_no_simulate_neat_jobs: '{"name": "(Advanced) Number of NEAT-genRead jobs","type": "Int","default": "1","description": "(Advanced) Number of NEAT-genRead jobs."}'
    input_no_threads: '{"name": "(Advanced) Number of threads", "type": "Int", "default": "8", "description": "(Advanced) Number of threads to run BWA MEM and samtools sort."}'
    input_bwa_mem_arguments: '{"name": "(Advanced) BWA MEM arguments", "type": "String", "required": "false", "default":"-K 100000000 -v 3 ", "description": "(Advanced) Overwrite BWA MEM arguments. Arguments \'-t number of threads\' and \'-R read group header line\' cannot be overwritten by this option."}'


  }

  String module_name = "simulate-reads"
  String module_version = "1.0.0"

  String simulated_sample_name = 'no_name_provided'
  String reference_genome = "hg38"
  String? chromosome

  # NEAT-genReads setting

  File? vcf_gz_with_variants_to_insert
  File? user_defined_targeted_regions_bed_file
  String predefined_targeted_regions = "agilent-sureselect-human-all-exon-v6-r2"
  Float off_target_coverage_scalar = 0.0

  String? empirical_fragment_length_distribution
  String? empirical_GC_coverage_bias_distribution

  Int ploidy = 2
  Int read_length = 150
  Float average_coverage = 150
  Boolean simulate_paired_end_reads = true
  Int paired_end_fragment_lenghth_mean = 300
  Int paired_end_fragment_lenghth_std = 30

  Int rng_seed_value = 1
  Boolean output_golden_bam_file = false
  Boolean output_golden_vcf_file = true
  Boolean output_fasta_instead_of_fastq = false
  Boolean bypass_fastq_generation = false

  Int no_simulate_neat_jobs = 1

  # BWA-MEM settings
  Int fq_bwa_mem_no_threads = 8
  String bwa_mem_arguments = "-K 100000000 -v 3 -Y "
  String RG_PL = "NEAT-genReads"


  # Run NEAT-genReads
  call simulate_neat_task.simulate_neat {
    input:
      simulated_sample_name = simulated_sample_name,
      reference_genome = reference_genome,
      chromosome = chromosome,
      vcf_gz_with_variants_to_insert = vcf_gz_with_variants_to_insert,
      user_defined_targeted_regions_bed_file = user_defined_targeted_regions_bed_file,
      predefined_targeted_regions = predefined_targeted_regions,
      off_target_coverage_scalar = off_target_coverage_scalar,
      empirical_fragment_length_distribution = empirical_fragment_length_distribution,
      empirical_GC_coverage_bias_distribution = empirical_GC_coverage_bias_distribution,
      ploidy = ploidy,
      read_length = read_length,
      average_coverage = average_coverage,
      simulate_paired_end_reads = simulate_paired_end_reads,
      paired_end_fragment_lenghth_mean = paired_end_fragment_lenghth_mean,
      paired_end_fragment_lenghth_std = paired_end_fragment_lenghth_std,
      rng_seed_value = rng_seed_value,
      output_golden_bam_file = output_golden_bam_file,
      output_golden_vcf_file = output_golden_vcf_file,
      output_fasta_instead_of_fastq = output_fasta_instead_of_fastq,
      bypass_fastq_generation = bypass_fastq_generation,
      no_jobs = no_simulate_neat_jobs
  }

  # Run BWA MEM
  if (defined(simulate_neat.fastq_1_gz)) {
    call fq_bwa_mem_task.fq_bwa_mem {
      input:
        fastq_1 = simulate_neat.fastq_1_gz,
        fastq_2 = simulate_neat.fastq_2_gz,
        sample_id = simulated_sample_name,
        chromosome = chromosome,
        reference_genome = reference_genome,
        bwa_mem_arguments = bwa_mem_arguments,
        RG_PL = RG_PL,
        no_threads = fq_bwa_mem_no_threads
    }
  }

  Array[File] bco_tasks = select_all([simulate_neat.bco, fq_bwa_mem.bco])
  Array[File] stdout_tasks = select_all([simulate_neat.stdout_log, fq_bwa_mem.stdout_log])
  Array[File] stderr_tasks = select_all([simulate_neat.stderr_log, fq_bwa_mem.stderr_log])

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_tasks,
        stdout_array = stdout_tasks,
        stderr_array = stderr_tasks,
        module_name = module_name,
        module_version = module_version
  }


  output {

    # neat-genreads outputs

    File? fastq_1_gz = simulate_neat.fastq_1_gz
    File? fastq_2_gz = simulate_neat.fastq_2_gz

    File? golden_bam = simulate_neat.golden_bam
    File? golden_bam_bai = simulate_neat.golden_bam_bai

    File? golden_vcf_gz = simulate_neat.golden_vcf_gz
    File? golden_vcf_gz_tbi = simulate_neat.golden_vcf_gz_tbi

    File? fasta_1_gz = simulate_neat.fasta_1_gz
    File? fasta_2_gz = simulate_neat.fasta_2_gz

    # neat-genreads output fastqs aligned with aligment-bwa-mem

    File? markdup_bam = fq_bwa_mem.lane_markdup_bam
    File? markdup_bam_bai = fq_bwa_mem.lane_markdup_bam_bai

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco
  }

}

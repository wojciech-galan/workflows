import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.2.0/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-split-chroms@1.1.0/src/main/wdl/tasks/bam-split-chroms/bam-split-chroms.wdl" as bam_split_chroms_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-metrics@1.3.0/src/main/wdl/tasks/bam-metrics/bam-metrics.wdl" as bam_metrics_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-metrics-concat@1.2.0/src/main/wdl/tasks/bam-metrics-concat/bam-metrics-concat.wdl" as bam_metrics_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/report-bam-qc@1.0.2/src/main/wdl/tasks/report-bam-qc/latest/report-bam-qc.wdl" as report_bam_qc_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow bam_qc {
  
  File? interval_list
  File? target
  File? bait
  File? quality_json
  String reference_genome = "grch38-no-alt" #or hg38


  String kit = "exome-v7" # Possible values "exome-v6", "exome-v7", "genome", "target"
  Boolean run_resources_kit_for_genome = (kit == "genome" && !defined(interval_list))
  Boolean run_resources_kit_for_exome = ((kit == "exome-v7" || kit == "exome-v6") && ( !defined(target) || !defined(bait)))

  File bam
  File bai

  String module_name = "bam_qc"
  String module_version = "2.3.5"
  String sample_id = "no_id_provided"
  String bam_metrics_java_options = "-Xms4g -Xmx63g" # use on anakin "-Xms4g -Xmx8g"

  if(run_resources_kit_for_genome || run_resources_kit_for_exome) {
       call resources_kit_task.resources_kit {
           input:
              kit = kit,
              reference_genome = reference_genome
      }

      if (kit == "genome"){
        File interval_resources = resources_kit.interval_list[0]
      }

      if(kit == "exome-v7" || kit == "exome-v6"){
        File target_resources = resources_kit.target[0]
        File bait_resources = resources_kit.bait[0]
      }
  }
    if (defined(interval_list) || defined(interval_resources)){
        File interval_file = select_first([interval_list, interval_resources])
    }

    if((defined(target_resources) || defined(target)) && (defined(bait_resources) || defined(bait))) {
        File target_file = select_first([target, target_resources])
        File bait_file = select_first([bait, bait_resources])
    }

  call bam_split_chroms_task.bam_split_chroms{
    input:
        bam = bam,
        bai = bai,
        unmapped = true
  }

  Array[String] chromosomes = ["chr1", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr2", "chr20", "chr21", "chr22", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr-unmapped", "chrX", "chrY-and-the-rest"]
  Array[Int] chromosome_indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]

  scatter(chromosome_index in chromosome_indices){
    call bam_metrics_task.bam_metrics {
        input:
            sample_id = sample_id,
            chromosome = chromosomes[chromosome_index],
            intervals = interval_file,
            target = target_file,
            bait = bait_file,
            kit = kit,
            bam = bam_split_chroms.chrom_bams[chromosome_index],
            reference_genome = reference_genome,
            java_options = bam_metrics_java_options,
            index = chromosome_index
    }
  }

  call bam_metrics_concat_task.bam_metrics_concat{
    input:
        final_json = bam_metrics.final_json,
        sample_id = sample_id,
        kit = kit,
        kit_json = quality_json
        }

  call report_bam_qc_task.report_bam_qc {
    input:
        coverage_stats_json = bam_metrics_concat.metrics_json,
        output_name = "${sample_id}-final-report-coverage-stats"
  }

########
#Merge bco, stdout, stderr files

  Array[File] bco_tasks = select_all([resources_kit.bco, bam_split_chroms.bco, report_bam_qc.bco])
  Array[File] stdout_tasks = select_all([resources_kit.stdout_log, bam_split_chroms.stdout_log, report_bam_qc.stdout_log])
  Array[File] stderr_tasks = select_all([resources_kit.stderr_log, bam_split_chroms.stderr_log, report_bam_qc.stderr_log])

  Array[Array[File]] bco_scatters = [bco_tasks, bam_metrics.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, bam_metrics.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, bam_metrics.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

########

  output {
    File metrics_json = bam_metrics_concat.metrics_json

    File coverage_report_pdf = report_bam_qc.coverage_report_pdf
    File coverage_report_docx = report_bam_qc.coverage_report_docx

    File stdout_log = bco_merge.stdout_log
    File bco = bco_merge.bco
    File stderr_log = bco_merge.stderr_log
  }
}

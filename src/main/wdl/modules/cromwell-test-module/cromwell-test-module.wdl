import "https://gitlab.com/intelliseq/workflows/raw/cromwell-test-task@1.0.0/src/main/wdl/tasks/cromwell-test-task/cromwell-test-task.wdl" as cromwell_test_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow cromwell_test_module {

  meta {
      keywords: '{"keywords": ["some", "keywords"]}'
      name: 'cromwell_test_module'
      author: 'https://gitlab.com/MateuszMarynowski'
      copyright: 'Copyright 2019 Intelliseq'
      description: 'Cromwell test module'
      changes: '{"1.0.0": "no changes"}'

      input_test_string: '{"index": 1, "name": "Test string", "type": "String", "default": "test", "description": "Enter a string"}'
      input_sec_to_wait: '{"index": 2, "name": "Seconds to wait", "type": "Int", "default": 3600, "description": "Enter the number of seconds to wait"}'
      input_n_scatter: '{"index": 3, "name": "Number of scatter", "type": "Int", "default": 10, "description": "Enter the number of scatter to be performed"}'

      output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
      output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
      output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  File? test_file
  String test_string = "test"
  Int sec_to_wait = 3600
  Int n_scatter = 10

  String module_name = "cromwell_test_module"
  String module_version = "1.0.0"

  call cromwell_test_task.cromwell_test_task {
      input:
          test_string = test_string,
          sec_to_wait = sec_to_wait
  }

  call cromwell_test_task.cromwell_test_task as cromwell_test_task_second {
      input:
          test_string = test_string,
          sec_to_wait = sec_to_wait,
          test_file = cromwell_test_task.results
  }

  scatter(i in range(n_scatter)) {
    call cromwell_test_task.cromwell_test_task as cromwell_test_task_scatter {
        input:
            test_string = test_string,
            sec_to_wait = sec_to_wait,
            test_file = cromwell_test_task_second.results,
            index = i
    }
  }

  # Merge bco, stdout, stderr files
  Array[File] bco_tasks = [cromwell_test_task.bco, cromwell_test_task_second.bco]
  Array[File] stdout_tasks = [cromwell_test_task.stdout_log, cromwell_test_task_second.stdout_log]
  Array[File] stderr_tasks = [cromwell_test_task.stderr_log, cromwell_test_task_second.stderr_log]

  Array[Array[File]] bco_scatters = [bco_tasks, cromwell_test_task_scatter.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, cromwell_test_task_scatter.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, cromwell_test_task_scatter.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
      input:
          bco_array = bco_array,
          stdout_array = stdout_array,
          stderr_array = stderr_array,
          module_name = module_name,
          module_version = module_version
  }

  output {
      File results = cromwell_test_task_second.results
      File stdout_log = bco_merge.stdout_log
      File stderr_log = bco_merge.stderr_log
      File bco = bco_merge.bco
  }
}

import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.0.1/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/interval-group@1.1.1/src/main/wdl/tasks/interval-group/interval-group.wdl" as interval_group_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-combine@1.0.2/src/main/wdl/tasks/gvcf-combine/gvcf-combine.wdl" as gvcf_combine_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-call-variants@1.1.4/src/main/wdl/tasks/gvcf-call-variants/gvcf-call-variants.wdl" as gvcf_call_variants_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.2.1/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.2.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow gvcf_joint_genotyping_workflow {

  meta {
    keywords: '{"keywords": ["gvcf", "genotyping"]}'
    name: 'GVCF joint genotyping'
    author: 'https://gitlab.com/lltw, https://gitlab.com/marysiaa'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Joint genotyping of gvcfs'
    changes: '{"1.0.1": "update tasks versions with new dockers - non-root permission to resources, add meta"}'


    input_gvcf_gz:'{"name": "Gvcfs gz", "type": "Array[File]", "description": "Array of gVCF files"}'
    input_gvcf_gz_tbi: '{ "name": "Gvcfs indexes", "type": "Array[File]", "description": "Array of gVCF files indexes"}'

    input_interval_file: '{"name": "Inteval file", "type": "File", "extension": [".interval_list"], "description": "File with list of genome intervals to be analyzed"}'
    input_sample_id: '{"name": "Sample ID", "type": "String", "description": "Sample identifier."}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "values": ["hg38", "grch38-no-alt"], "default": "hg38", "description": "Reference genome options: hg38 and grch38-no-alt"}'


    output_vcf_gz:'{"name": "Vcf gz","type": "File", "description": " bgzipped VCF file"}'
    output_vcf_gz_tbi: '{ "name": "Vcf index", "type": "File", "description": " VCF file index"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    }


  String module_name = "gvcf_joint_genotyping"
  String module_version = "latest"

  String sample_id = "no_id_provided"
  String reference_genome = "hg38" # or grch38-no-alt

  Array[File] gvcf_gz
  Array[File] gvcf_gz_tbi

  File? interval_list
  String kit = "exome-v7"
  Boolean is_interval_list_not_defined = !defined(interval_list)
  Int max_no_pieces_to_scatter_an_interval_file = 2

  if(is_interval_list_not_defined) {
    call resources_kit_task.resources_kit {
      input:
      kit = kit
    }
  }
  File interval_file = select_first([interval_list, resources_kit.interval_list])


  call interval_group_task.interval_group {
    input:
      interval_file = interval_file,
      max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file
  }


  scatter (index in range(length(interval_group.grouped_calling_intervals))) {
    call gvcf_combine_task.gvcf_combine {
      input:
        index = index,
        gvcf_gz = gvcf_gz,
        gvcf_gz_tbi = gvcf_gz_tbi,
        sample_id = sample_id,
        reference_genome = reference_genome,
        intervals = interval_group.grouped_calling_intervals[index]
    }

    call gvcf_call_variants_task.gvcf_call_variants {
      input:
        index = index,
        gvcf_gz = gvcf_combine.combined_gvcf_gz,
        gvcf_gz_tbi = gvcf_combine.combined_gvcf_gz_tbi,
        sample_id = sample_id,
        reference_genome = reference_genome,
        intervals = interval_group.grouped_calling_intervals[index],
        gatk_additional_parm = "--allow-old-rms-mapping-quality-annotation-data"
    }
  }

  call vcf_concat_task.vcf_concat {
    input:
      vcf_gz = gvcf_call_variants.vcf_gz,
      vcf_gz_tbi = gvcf_call_variants.vcf_gz,
      vcf_basename = sample_id
  }

  # Merge bco, stdout, stderr files
    Array[File] bco_tasks = [interval_group.bco, vcf_concat.bco]
    Array[File] stdout_tasks = [interval_group.stdout_log, vcf_concat.stdout_log]
    Array[File] stderr_tasks = [interval_group.stderr_log, vcf_concat.stderr_log]

    Array[Array[File]] bco_scatters = [bco_tasks, gvcf_combine.bco, gvcf_call_variants.bco]
    Array[Array[File]] stdout_scatters = [stdout_tasks, gvcf_combine.stdout_log, gvcf_call_variants.stdout_log]
    Array[Array[File]] stderr_scatters = [stderr_tasks, gvcf_combine.stderr_log, gvcf_call_variants.stderr_log]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)


    call bco_merge_task.bco_merge {
      input:
          bco_array = bco_array,
          stdout_array = stdout_array,
          stderr_array = stderr_array,
          module_name = module_name,
          module_version = module_version
    }

  output {

    File vcf_gz = vcf_concat.concatenated_vcf_gz
    File vcf_gz_tbi = vcf_concat.concatenated_vcf_gz_tbi

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}

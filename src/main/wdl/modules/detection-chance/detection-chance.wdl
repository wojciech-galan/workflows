import "https://gitlab.com/intelliseq/workflows/raw/resource-clinvar@1.0.3/src/main/wdl/tasks/resource-clinvar/resource-clinvar.wdl" as resource_clinvar_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-genotype-by-vcf@2.0.2/src/main/wdl/tasks/gvcf-genotype-by-vcf/gvcf-genotype-by-vcf.wdl" as gvcf_genotype_by_vcf_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-panel-coverage@1.0.5/src/main/wdl/tasks/bam-panel-coverage/bam-panel-coverage.wdl" as bam_panel_coverage_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-detect-stats@2.1.2/src/main/wdl/tasks/vcf-detect-stats/vcf-detect-stats.wdl" as vcf_detect_stats_task
import "https://gitlab.com/intelliseq/workflows/raw/report-detect-stats@1.3.2/src/main/wdl/tasks/report-detect-stats/report-detect-stats.wdl" as report_detect_stats_task

workflow detection_chance {

meta {
    keywords: '{"keywords": ["detection chence", "coverage", "clinvar", "clinvar-clinical-significance"]}'
    name: 'detection_chance'
    author: 'https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Creating report with detection chance statistics for every gene presented in gene-panel.'
    changes: '{"1.4.0" : "new gvcf-genotype-by-vcf version", "1.3.13": "Update version of resource-clinvar","1.3.12": "fix wrong meta + some better description", "1.3.11": "Update vcf-detect-stats", "1.3.10": "Update bam-panel-coverage version - fixed empty jsons", "1.3.9": "Updated meta"}'

    input_sample_id: '{"index": 1, "name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Sample identifier"}'
    input_bam: '{"index":2, "name": "bam file", "type": "File", "description": "Bam file with name containing: markdup.recalibrated or filtered -  outputs from: 1. sample-id_markdup.recalibrated.bam from fq_bwa_align module: recalibrated_markdup_bam = bam_concat_recalib.bam, 2. sample-id_filtered.bam from bam_filter_contam module: bam_remove_mates.filtered_bam, or 3. one of those is a germline output (if you start from fastqs): final_bam = bam_to_var_calling. NEVER USE sample-id_realigned-haplotypecaller.bam from bam_varcalling module: haplotype_caller_bam = bam_concat.bam, also a germline output: final_realigned_bam = bam_varcalling.haplotype_caller_bam.", "extension": [".bam"]}'
    input_bai: '{"index": 3, "name": "bai file", "type": "File", "description": "Bai index file for provided bam.", "extension": [".bai"]}'

    input_sample_gvcf_gz: '{"index": 4, "name": "sample_gvcf_gz", "type": "File", "extension": [".gvcf.gz", ".g.vcf.gz"], "description": "Choose gvcf file."}'
    input_sample_gvcf_gz_tbi: '{"index": 5, "name": "sample_gvcf_gz_tbi", "type": "File", "extension": [".gvcf.gz.tbi", ".g.vcf.gz.tbi"], "description": "Index for gvcf file."}'
    input_panel_json: '{"index": 6, "name": "panel_json", "type": "File", "extension": [".json"], "description": "Panel with genes in json format."}'

    output_genotyped_vcf_gz: '{"name": "genotyped_vcf_gz", "type": "File", "copy": "True", "description": "Vcf genotyped by interval vcf"}'

    output_pdf_report: '{"name": "report_detection_chance_pdf", "type": "File", "copy": "True", "description": "report with coverage statistics metrics"}'
    output_html_report: '{"name": "report_detection_chance_html", "type": "File", "copy": "True", "description": "report with coverage statistics metrics"}'
    output_docx_report: '{"name": "report_detection_chance_docx", "type": "File", "copy": "True", "description": "report with coverage statistics metrics"}'
    output_odt_report: '{"name": "report_detection_chance_odt", "type": "File", "copy": "True", "description": "report with coverage statistics metrics"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

    String module_name = "detection_chance"
    String module_version = "1.4.0"
    String sample_id = "no_id_provided"

    # Needed to bam-panel-coverage
    File bam
    File bai

      # sample gvcf
    File sample_gvcf_gz
    File sample_gvcf_gz_tbi

     # gene panel - the same as for target-seq
    File? panel_json

      # optional inputs for gvcf-genotype-by-vcf
    File? interval_vcf_gz_tbi
    File? interval_bed_gz
    File? interval_bed_gz_tbi
      # vcf used for genotyping (for example clinvar)
    String genotyping_databese = "clinvar"

    call resource_clinvar_task.resource_clinvar

    call gvcf_genotype_by_vcf_task.gvcf_genotype_by_vcf {
      input:
        sample_id = sample_id,
        sample_gvcf_gz = sample_gvcf_gz,
        sample_gvcf_gz_tbi = sample_gvcf_gz_tbi,

        interval_vcf_gz = resource_clinvar.clinvar_vcf_gz,
        interval_vcf_gz_tbi = resource_clinvar.clinvar_vcf_gz_tbi

    }

    call bam_panel_coverage_task.bam_panel_coverage {
      input:
        bam = bam,
        bai = bai,
        panel_json = panel_json
    }

    call vcf_detect_stats_task.vcf_detect_stats {
      input:
        sample_id = sample_id,
        panel_json = panel_json,
        genotyped_vcf_gz = gvcf_genotype_by_vcf.genotyped_vcf_gz,
        panel_coverage_json = bam_panel_coverage.panel_coverage_json
    }

    call report_detect_stats_task.report_detect_stats {
      input:
        sample_id = sample_id,
        detection_stats_json = vcf_detect_stats.detection_stats_json
    }
      ########
      #Merge bco, stdout, stderr files

        Array[File] bco_array = [resource_clinvar.bco, gvcf_genotype_by_vcf.bco, vcf_detect_stats.bco, report_detect_stats.bco, bam_panel_coverage.bco]
        Array[File] stdout_array = [resource_clinvar.stdout_log, gvcf_genotype_by_vcf.stdout_log, vcf_detect_stats.stdout_log, report_detect_stats.stdout_log, bam_panel_coverage.stdout_log]
        Array[File] stderr_array = [resource_clinvar.stderr_log, gvcf_genotype_by_vcf.stderr_log, vcf_detect_stats.stderr_log, report_detect_stats.stderr_log, bam_panel_coverage.stderr_log]

        call bco_merge_task.bco_merge {
          input:
              bco_array = bco_array,
              stdout_array = stdout_array,
              stderr_array = stderr_array,
              module_name = module_name,
              module_version = module_version
        }


  output {

    File clinvar_vcf_gz = resource_clinvar.clinvar_vcf_gz
    File clinvar_vcf_gz_tbi = resource_clinvar.clinvar_vcf_gz_tbi

    File genotyped_vcf_gz = gvcf_genotype_by_vcf.genotyped_vcf_gz

    File detection_stats_json = vcf_detect_stats.detection_stats_json

    File detection_chance_report_pdf = report_detect_stats.detection_chance_report_pdf
    File detection_chance_report_odt = report_detect_stats.detection_chance_report_odt
    File detection_chance_report_docx = report_detect_stats.detection_chance_report_docx
    File detection_chance_report_html = report_detect_stats.detection_chance_report_html

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco



  }

}

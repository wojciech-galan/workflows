import "https://gitlab.com/intelliseq/workflows/raw/gtc-to-vcf@1.1.1/src/main/wdl/tasks/gtc-to-vcf/gtc-to-vcf.wdl" as gtc_to_vcf_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-qc@1.0.2/src/main/wdl/tasks/vcf-qc/vcf-qc.wdl" as vcf_qc_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.1.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow gtc_to_vcf_qc {

  File bpm_manifest_file
  File csv_manifest_file
  File egt_cluster_file

  Array[File] gtc_files
  Boolean adjust_clusters = true

  File sample_info_json
  File parameters_json
  String sample_id ="no_id_provided"

  String module_name = "gtc_to_vcf_qc"
  String module_version = "1.1.1"


 call gtc_to_vcf_task.gtc_to_vcf {
   input:
       bpm_manifest_file = bpm_manifest_file,
       csv_manifest_file = csv_manifest_file,
       egt_cluster_file = egt_cluster_file,
       gtc_files = gtc_files,
       adjust_clusters = adjust_clusters,
       sample_id = sample_id
 }

 call vcf_qc_task.vcf_qc {
   input:
       vcf = gtc_to_vcf.vcf_gz,
       sample_id = sample_id,
       sample_info_json = sample_info_json,
       parameters_json = parameters_json
 }


  # Merge bco, stdout, stderr files
 Array[File] bco_array = [gtc_to_vcf.bco, vcf_qc.bco]
 Array[File] stdout_array = [gtc_to_vcf.stdout_log, vcf_qc.stdout_log]
 Array[File] stderr_array = [gtc_to_vcf.stderr_log, vcf_qc.stderr_log]

 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_array,
       stdout_array = stdout_array,
       stderr_array = stderr_array,
       module_name = module_name,
       module_version = module_version
 }


  output {

    Array[File] vcfs_gz = gtc_to_vcf.per_sample_vcfs_gz
    Array[File] vcfs_gz_tbi = gtc_to_vcf.per_sample_vcfs_gz_tbi

    File perIndividual_vcf_qc_json = vcf_qc.perIndividual_vcf_qc_json
    File qc_json = vcf_qc.perIndividual_vcf_qc_json
    File perMarker_vcf_qc_json = vcf_qc.perMarker_vcf_qc_json
    Array[File] vcf_qc_images = vcf_qc.vcf_qc_images

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}

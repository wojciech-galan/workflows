### List of plots

Plots produced by module gtc-to-vcf-qc:

* id-sexcheck-perIndividual_report.jpg
* id-het_and_miss-perIndividual_report.jpg
* id-relatedness-perIndividual_report.jpg
* id-maf-perMarkerQC_report.jpg
* id-hwe-perMarkerQC_report.jpg
* id-snp_missingness-perMarkerQC_report.jpg


Also, there is one image containing three last plots from the list above combined: id-perMarkerQC_report.jpg.

### Plots' description (from Plink's vignette: https://meyer-lab-cshl.github.io/plinkQC/articles/plinkQC.html)

#### Individuals with discordant sex information - id-sexcheck-perIndividual_report.jpg

The identification of individuals with discordant sex information helps to detect sample mix-ups and samples with very poor genotyping rates. For each sample, the homozygosity rates across all X-chromosomal genetic markers are computed and compared with the expected rates (typically $<$0.2 for females and $>$0.8 for males). For samples where the assigned sex (PEDSEX; provided in the sample_info_json file) contradicts the sex inferred from the homozygosity rates (SNPSEX), it should be checked that the sex was correctly recorded (genotyping often occurs at different locations as phenotyping and misrecording might occur). Samples with discordant sex information that is not accounted for should be removed from the study. Identifying individuals with discordant sex information is implemented in check_sex. It finds individuals whose SNPSEX != PEDSEX. Optionally, an extra data.frame with sample IDs and sex can be provided to double check if external and PEDSEX data (often processed at different centers) match. If a mismatch between PEDSEX and SNPSEX was detected, by SNPSEX == Sex, PEDSEX of these individuals can optionally be updated. check_sex depicts the X-chromosomal heterozygosity (SNPSEX) of the samples split by their (PEDSEX).

![checkSex](https://meyer-lab-cshl.github.io/plinkQC/articles/checkSex.png)


#### Individuals with outlying missing genotype and/or heterozygosity rates - id-het_and_miss-perIndividual_report.jpg

The identification of individuals with outlying missing genotype and/or heterozygosity rates helps to detect samples with poor DNA quality and/or concentration that should be excluded from the study. Typically, individuals with more than 3-7% of their genotype calls missing are removed. Outlying heterozygosity rates are judged relative to the overall heterozygosity rates in the study, and individuals whose rates are more than a few standard deviations (sd) from the mean heterozygosity rate are removed. A typical quality control for outlying heterozygosity rates would remove individuals who are three sd away from the mean rate. Identifying related individuals with outlying missing genotype and/or heterozygosity rates is implemented in check_het_and_miss. It finds individuals that have genotyping and heterozygosity rates that fail the set thresholds and depicts the results as a scatter plot with the samples’ missingness rates on x-axis and their heterozygosity rates on the y-axis.

![checkHetImiss](https://meyer-lab-cshl.github.io/plinkQC/articles/checkHetImiss.png)

#### Related individuals - id-relatedness-perIndividual_report.jpg

Depending on the future use of the genotypes, it might required to remove any related individuals from the study. Related individuals can be identified by their proportion of shared alleles at the genotyped markers (identity by descend, IBD). Standardly, individuals with second-degree relatedness or higher will be excluded. Identifying related individuals is implemented in check_relatedness. It finds pairs of samples whose proportion of IBD is larger than the specified highIBDTh. Subsequently, for pairs of individual that do not have additional relatives in the dataset, the individual with the greater genotype missingness rate is selected and returned as the individual failing the relatedness check. For more complex family structures, the unrelated individuals per family are selected (e.g. in a parents-offspring trio, the offspring will be marked as fail, while the parents will be kept in the analysis).

![checkRelatedness](https://meyer-lab-cshl.github.io/plinkQC/articles/checkRelatedness.png)

#### Markers with low minor allele frequency - id-maf-perMarkerQC_report.jpg

Markers with low minor allele count are often removed as the actual genotype calling (via the calling algorithm) is very difficult due to the small sizes of the heterozygote and rare-homozygote clusters. Identifying markers with low minor allele count is implemented in check_maf. It calculates the minor allele frequencies for all variants.

![maf](https://meyer-lab-cshl.github.io/plinkQC/articles/maf.png)

#### Markers with deviation from HWE - id-hwe-perMarkerQC_report.jpg

Markers with strong deviation from HWE might be indicative of genotyping or genotype-calling errors. As serious genotyping errors often yield very low p-values (in the order of 10−50), it is recommended to choose a reasonably low threshold to avoid filtering too many variants (that might have slight, non-critical deviations). Identifying markers with deviation from HWE is implemented in check_hwe. It calculates the observed and expected heterozygote frequencies per SNP and computes the deviation of the frequencies from Hardy-Weinberg equilibrium (HWE) by HWE exact test.

![hwe](https://meyer-lab-cshl.github.io/plinkQC/articles/hwe.png)

#### Markers with excessive missingness rate - id-snp_missingness-perMarkerQC_report.jpg

Markers with excessive missingness rate are removed as they are considered unreliable. Typically, thresholds for marker exclusion based on missingness range from 1%-5%.

![hwe](https://meyer-lab-cshl.github.io/plinkQC/articles/snpmissingness.png)

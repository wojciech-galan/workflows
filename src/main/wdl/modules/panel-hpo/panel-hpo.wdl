import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@1.7.1/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/report-panel@1.2.4/src/main/wdl/tasks/report-panel/report-panel.wdl" as report_panel_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow panel_hpo {

  meta {
    keywords: '{"keywords": ["panel", "generate", "hpo", "report"]}'
    tag: 'Utilities'
    name: 'Custom gene panel generator'
    price: '10'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Generates gene panel based on HPO terms, diseases names or user defined genes.'
    changes: '{"1.6.8": "Change version of panel-generate and outputs", "1.6.7": "all panels added as output from panel-generate task and report-panel task updated to new task template", "1.6.6": "update score in panels", "1.6.5": "update price in meta", "1.6.4": "deleted None from panel_names values"}'

    groups: '{"gene_panel": {"description": "Fill in at least 1 input below to generate the gene panel", "min_inputs": 1}}'

    input_sample_id: '{"index": 1, "name": "Sample ID", "type": "String", "description": "Enter a sample name (or identifier)"}'
    input_hpo_terms: '{"index": 2, "name": "HPO terms", "type": "String", "groupname": "gene_panel", "description": "Enter HPO terms to narrow your search/analysis results (separate HPO terms with comma, for example: HP:0004942, HP:0011675)"}'
    input_genes: '{"index": 3, "name": "Genes names", "type": "String", "groupname": "gene_panel", "description": "Enter gene names to narrow your search/analysis results (separate gene names with comma, for example: HTT, FBN1)"}'
    input_diseases: '{"index": 4, "name": "Diseases", "type": "String", "groupname": "gene_panel", "description": "Enter disease names to narrow your search/analysis results (separate diseases names with comma; each disease name should be just a keyword, for example for Marfan Syndrome only Marfan should be written, for Ehlers-Danlos Syndrome: Ehlers-Danlos; other proper diseases names for example: Osteogenesis imperfecta, Tay-sachs, Hemochromatosis, Brugada, Canavan, etc.)"}'
    input_phenotypes_description: '{"index": 5, "name": "Description of patient phenotypes", "type": "String", "groupname": "gene_panel", "description": "Enter description of patient phenotypes"}'
    input_panel_names: '{"index": 6, "name": "Gene panel", "type": "Array[String]", "groupname": "gene_panel", "description": "Select gene panels", "constraints": {"values": ["ACMG_Incidental_Findings", "COVID-19_research", "Cancer_Germline", "Cardiovascular_disorders", "Ciliopathies", "Dermatological_disorders", "Dysmorphic_and_congenital_abnormality_syndromes","Endocrine_disorders", "Gastroenterological_disorders", "Growth_disorders","Haematological_and_immunological_disorders", "Haematological_disorders", "Hearing_and_ear_disorders", "Metabolic_disorders", "Neurology_and_neurodevelopmental_disorders","Ophthalmological_disorders", "Rare_Diseases", "Renal_and_urinary_tract_disorders","Respiratory_disorders", "Rheumatological_disorders", "Skeletal_disorders","Tumour_syndromes"], "multiselect": true}}'

    output_panel: '{"name": "Panel", "type": "File", "description": "JSON format genes with scores"}'
    output_phenotypes: '{"name": "phenotypes", "type": "File", "copy": "True", "description": "Hpo names taken from hpo ids"}'
    output_panel_report_pdf: '{"name": "Panel report PDF", "type": "File", "description": "PDF report created from panel"}'
    output_panel_report_odt: '{"name": "Panel report ODT", "type": "File", "description": "ODT report created from panel"}'
    output_panel_report_docx: '{"name": "Panel report Docx", "type": "File", "description": "Docx report created from panel"}'
    output_panel_report_html: '{"name": "Panel report HTML", "type": "File", "description": "HTML report created from panel"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  String module_name = "panel_hpo"
  String module_version = "1.6.8"

  String? hpo_terms
  String? genes
  String? diseases
  String? sample_id
  String? phenotypes_description
  Array[String]? panel_names


  call panel_generate_task.panel_generate{
    input:
       sample_id = sample_id,
       hpo_terms = hpo_terms,
       genes = genes,
       diseases = diseases,
       phenotypes_description = phenotypes_description,
       panel_names = panel_names
  }

  call report_panel_task.report_panel{
    input:
       sample_id = sample_id,
       panel_json = panel_generate.panel,
       panel_inputs_json = panel_generate.inputs
  }

  Array[File] bco_array = [panel_generate.bco, report_panel.bco]
  Array[File] stdout_array = [panel_generate.stdout_log, report_panel.stdout_log]
  Array[File] stderr_array = [panel_generate.stderr_log, report_panel.stderr_log]

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  output {
    File panel = panel_generate.panel
    File inputs = panel_generate.inputs
    File all_panels = panel_generate.all_panels

    File panel_report_pdf = report_panel.panel_report_pdf
    File panel_report_odt = report_panel.panel_report_odt
    File panel_report_docx = report_panel.panel_report_docx
    File panel_report_html = report_panel.panel_report_html

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}

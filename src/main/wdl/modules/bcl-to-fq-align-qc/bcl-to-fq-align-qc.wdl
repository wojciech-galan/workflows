import "https://gitlab.com/intelliseq/workflows/raw/bcl-to-fq@1.0.3/src/main/wdl/tasks/bcl-to-fq/bcl-to-fq.wdl" as bcl_to_fq_task
import "https://gitlab.com/intelliseq/workflows/raw/qc-fq-fastqc@2.0.0/src/main/wdl/tasks/qc-fq-fastqc/qc-fq-fastqc.wdl" as qc_fq_fastqc_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bowtie2@1.1.0/src/main/wdl/tasks/fq-bowtie2/fq-bowtie2.wdl" as fq_bowtie2_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-mark-dup@1.0.0/src/main/wdl/tasks/bam-mark-dup/bam-mark-dup.wdl" as bam_mark_dup_task
import "https://gitlab.com/intelliseq/workflows/raw/qc-multiqc@2.0.0/src/main/wdl/tasks/qc-multiqc/qc-multiqc.wdl" as qc_multiqc_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow bcl_to_fq_align_qc {

  File raw_data_tar_gz
  Boolean paired = false
  String sample_id = "no_id_provided"
  File? thresholds_json
  Boolean run_bowtie1 = false

  String module_name = "bcl_to_fq_align_qc"
  String module_version = "1.3.0"


  call bcl_to_fq_task.bcl_to_fq {
    input:
      raw_data_tar_gz = raw_data_tar_gz,
      paired = paired
    }


  if (!paired) {
  scatter (index in range(length(bcl_to_fq.fastqs))) {

    String sample_id_fastq = basename(bcl_to_fq.fastqs[index], ".fastq.gz")

    call qc_fq_fastqc_task.qc_fq_fastqc {
      input:
          fastq = bcl_to_fq.fastqs[index],
          index = index
    }

    # 1. Aligment by bowtie2 or bowtie
    call fq_bowtie2_task.fq_bowtie2{
        input:
        fastq_single_end = bcl_to_fq.fastqs[index],
        sample_id = sample_id_fastq,
        index = index,
        run_bowtie1 = run_bowtie1
    }

    # 2. Mark duplicates, sort, index
    Array[File] bams = [fq_bowtie2.bam]
    call bam_mark_dup_task.bam_mark_dup{
        input:
        bams = bams,
        sample_id = sample_id_fastq,
        index = index
    }

  }
  }


  if (paired) {
  scatter (index in range(length(bcl_to_fq.fastqs_1))) {

    String sample_id_fastq_paired = basename(bcl_to_fq.fastqs_1[index], "_1.fastq.gz")

    call qc_fq_fastqc_task.qc_fq_fastqc as qc_fq_fastqc_1 {
      input:
          fastq = bcl_to_fq.fastqs_1[index],
          index = index
    }
    call qc_fq_fastqc_task.qc_fq_fastqc as qc_fq_fastqc_2 {
      input:
          fastq = bcl_to_fq.fastqs_2[index],
          index = index
    }

    # 1. Aligment by bowtie2 or bowtie
    call fq_bowtie2_task.fq_bowtie2 as fq_bowtie2_paired {
        input:
        fastq_paired_1 = bcl_to_fq.fastqs_1[index],
        fastq_paired_2 = bcl_to_fq.fastqs_2[index],
        sample_id = sample_id_fastq_paired,
        index = index,
        run_bowtie1 = run_bowtie1
    }

    # 2. Mark duplicates, sort, index
    Array[File] bams_paired = [fq_bowtie2_paired.bam]
    call bam_mark_dup_task.bam_mark_dup as bam_mark_dup_paired {
        input:
        bams = bams_paired,
        sample_id = sample_id_fastq_paired,
        index = index
    }
  }
  }


  Array[Array[File]] qc_input_scatters = select_all([qc_fq_fastqc.fastqc_zip, qc_fq_fastqc_1.fastqc_zip, qc_fq_fastqc_2.fastqc_zip, fq_bowtie2.bowtie_stderr, fq_bowtie2_paired.bowtie_stderr, bam_mark_dup.metrics_file, bam_mark_dup_paired.metrics_file])
  Array[File] qc_input = flatten(qc_input_scatters)

  call qc_multiqc_task.qc_multiqc {
    input:
        qc_results = qc_input,
        sample_id = sample_id,
        thresholds_json = thresholds_json
  }
 
 Array[File] bco_tasks = [bcl_to_fq.bco, qc_multiqc.bco]
 Array[File] stdout_tasks = [bcl_to_fq.stdout_log, qc_multiqc.stdout_log]
 Array[File] stderr_tasks = [bcl_to_fq.stderr_log, qc_multiqc.stderr_log]

 Array[Array[File]] bco_scatters = select_all([bco_tasks, qc_fq_fastqc.bco, qc_fq_fastqc_1.bco, qc_fq_fastqc_2.bco, fq_bowtie2.bco, fq_bowtie2_paired.bco, bam_mark_dup.bco, bam_mark_dup_paired.bco])
 Array[Array[File]] stdout_scatters = select_all([stdout_tasks, qc_fq_fastqc.stdout_log, qc_fq_fastqc_1.stdout_log, qc_fq_fastqc_2.stdout_log, fq_bowtie2.stdout_log, fq_bowtie2_paired.stdout_log, bam_mark_dup.stdout_log, bam_mark_dup_paired.stdout_log])
 Array[Array[File]] stderr_scatters = select_all([stderr_tasks, qc_fq_fastqc.stderr_log, qc_fq_fastqc_1.stderr_log, qc_fq_fastqc_2.stderr_log, fq_bowtie2.stderr_log, fq_bowtie2_paired.stderr_log, bam_mark_dup.stderr_log, bam_mark_dup_paired.stderr_log])

 Array[File] bco_array = flatten(bco_scatters)
 Array[File] stdout_array = flatten(stdout_scatters)
 Array[File] stderr_array = flatten(stderr_scatters)

 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_array,
       stdout_array = stdout_array,
       stderr_array = stderr_array,
       module_name = module_name,
       module_version = module_version
 }

  output {

    File report_html = qc_multiqc.report_html
    File multiqc_stats_json = qc_multiqc.multiqc_stats_json
    File mutliqc_data_json = qc_multiqc.mutliqc_data_json
    File multiqc_general_stats_txt = qc_multiqc.multiqc_general_stats_txt
    File multiqc_sources = qc_multiqc.multiqc_sources

    Array[File] fastqc_zip = flatten(select_all([qc_fq_fastqc.fastqc_zip, qc_fq_fastqc_1.fastqc_zip, qc_fq_fastqc_2.fastqc_zip]))

    Array[File] fastq_single_end = bcl_to_fq.fastqs
    Array[File] fastqs_1 = bcl_to_fq.fastqs_1
    Array[File] fastqs_2 = bcl_to_fq.fastqs_2
    Array[File] barcode_fastqs = bcl_to_fq.barcode_fastqs

    Array[File] bam = select_first([bam_mark_dup.mark_dup_bam, bam_mark_dup_paired.mark_dup_bam])
    Array[File] bai = select_first([bam_mark_dup.mark_dup_bai, bam_mark_dup_paired.mark_dup_bai])

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}

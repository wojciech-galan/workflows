import "https://gitlab.com/intelliseq/workflows/raw/resources-pharmcat@1.0.2/src/main/wdl/tasks/resources-pharmcat/resources-pharmcat.wdl" as resources_pharmcat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-uniq@1.1.0/src/main/wdl/tasks/vcf-uniq/vcf-uniq.wdl" as vcf_uniq_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-genotype-by-vcf@2.0.2/src/main/wdl/tasks/gvcf-genotype-by-vcf/gvcf-genotype-by-vcf.wdl" as gvcf_genotype_by_vcf_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-pharmcat@1.4.4/src/main/wdl/tasks/pgx-pharmcat/latest/pgx-pharmcat.wdl" as pgx_pharmcat_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-astrolabe@1.1.1/src/main/wdl/tasks/pgx-astrolabe/pgx-astrolabe.wdl" as pgx_astrolabe_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-cyrius@1.0.1/src/main/wdl/tasks/pgx-cyrius/pgx-cyrius.wdl" as pgx_cyrius_task
import "https://gitlab.com/intelliseq/workflows/-/raw/pgx-aldy@1.1.1/src/main/wdl/tasks/pgx-aldy/pgx-aldy.wdl" as pgx_aldy_task
import "https://gitlab.com/intelliseq/workflows/-/raw/pgx-merge-genotypes@1.1.0/src/main/wdl/tasks/pgx-merge-genotypes/pgx-merge-genotypes.wdl" as pgx_merge_genotypes_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.1.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow pgx_genotyping_report {

  String module_name = "pgx_genotyping_report"
  String module_version = "2.1.0"

  String sample_id = "no_id_provided"

  # sample gvcf
  File gvcf_gz
  File gvcf_gz_tbi

  File? bam
  File? bai

  Boolean run_vcf_uniq = false
  Boolean is_bam_defined = defined(bam)

  call resources_pharmcat_task.resources_pharmcat {
    }

  if(run_vcf_uniq) {
    call vcf_uniq_task.vcf_uniq {
      input:
        gvcf_gz = gvcf_gz,
        sample_id = sample_id
     }
  }

  File gvcf_to_genotype = select_first([vcf_uniq.gvcf_uniq_gz, gvcf_gz])
  File gvcf_to_genotype_tbi = select_first([vcf_uniq.gvcf_uniq_gz_tbi, gvcf_gz_tbi])

  call gvcf_genotype_by_vcf_task.gvcf_genotype_by_vcf {
    input:
      sample_id = sample_id,
      sample_gvcf_gz = gvcf_to_genotype,
      sample_gvcf_gz_tbi = gvcf_to_genotype_tbi,
      interval_vcf_gz = resources_pharmcat.pharmcat_vcf_gz,
      interval_vcf_gz_tbi = resources_pharmcat.pharmcat_vcf_gz_tbi,
      interval_bed_gz = resources_pharmcat.pharmcat_bed_gz,
      interval_bed_gz_tbi = resources_pharmcat.pharmcat_bed_gz_tbi
    }

  call pgx_pharmcat_task.pgx_pharmcat {
    input:
      sample_id = sample_id,
      sample_vcf_gz = gvcf_genotype_by_vcf.genotyped_vcf_gz
      }

if(is_bam_defined) {
    call pgx_astrolabe_task.pgx_astrolabe {
      input:
        gvcf_gz = gvcf_to_genotype,
        bam = bam,
        bai = bai,
        sample_id = sample_id
    }

    call pgx_cyrius_task.pgx_cyrius {
      input:
        bam = bam,
        bai = bai,
        sample_id = sample_id
     }

     call pgx_aldy_task.pgx_aldy {
        input:
          bam = bam,
          bai = bai,
          sample_id = sample_id
       }

    call pgx_merge_genotypes_task.pgx_merge_genotypes {
      input:
        astrolabe_tsv = pgx_astrolabe.cnv_tsv,
        aldy_tsv = pgx_aldy.out_txt,
        pharmcat_json = pgx_pharmcat.pgx_report_json,
        cyrius_tsv = pgx_cyrius.cyrius_tsv,
        sample_id = sample_id

        }
  }

  # Merge bco, stdout, stderr files
 Array[File] bco_array = select_all([resources_pharmcat.bco, gvcf_genotype_by_vcf.bco, pgx_pharmcat.bco, pgx_astrolabe.bco, pgx_aldy.bco, pgx_merge_genotypes.bco])
 Array[File] stdout_array = select_all([resources_pharmcat.stdout_log, gvcf_genotype_by_vcf.stdout_log, pgx_pharmcat.stdout_log, pgx_astrolabe.stdout_log, pgx_aldy.stdout_log, pgx_merge_genotypes.stdout_log])
 Array[File] stderr_array = select_all([resources_pharmcat.stderr_log, gvcf_genotype_by_vcf.stderr_log, pgx_pharmcat.stderr_log, pgx_astrolabe.stderr_log, pgx_aldy.stderr_log, pgx_merge_genotypes.stderr_log])

 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_array,
       stdout_array = stdout_array,
       stderr_array = stderr_array,
       module_name = module_name,
       module_version = module_version
 }

  output {

    File pgx_report_html = pgx_pharmcat.pgx_report_html
    File pgx_report_json = pgx_pharmcat.pgx_report_json
    File pgx_matcher_html = pgx_pharmcat.pgx_matcher_html
    File pgx_matcher_json = pgx_pharmcat.pgx_matcher_json

    File genotyped_vcf_gz = gvcf_genotype_by_vcf.genotyped_vcf_gz

    File? cnv_tsv = pgx_astrolabe.cnv_tsv
    File? verbose_file = pgx_astrolabe.verbose_file
    File? novel_file = pgx_astrolabe.novel_file

    File? cyrius_tsv = pgx_cyrius.cyrius_tsv
    File? cyrius_json = pgx_cyrius.cyrius_json

    File? out_aldy = pgx_aldy.out_aldy

    File? tsv = pgx_merge_genotypes.tsv
    File? json = pgx_merge_genotypes.json

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco
  }
}


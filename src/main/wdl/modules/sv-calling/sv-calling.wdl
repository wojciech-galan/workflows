import "https://gitlab.com/intelliseq/workflows/raw/bam-get-contigs@1.0.0/src/main/wdl/tasks/bam-get-contigs/bam-get-contigs.wdl" as bam_get_contigs_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-smoove@1.0.2/src/main/wdl/tasks/sv-smoove/sv-smoove.wdl" as sv_smoove_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-gsort@1.0.2/src/main/wdl/tasks/vcf-gsort/vcf-gsort.wdl" as vcf_gsort_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-duphold@1.0.1/src/main/wdl/tasks/sv-duphold/sv-duphold.wdl" as sv_duphold_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-typer@1.0.0/src/main/wdl/tasks/sv-typer/sv-typer.wdl" as sv_typer_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-filter@1.0.2/src/main/wdl/tasks/sv-filter/sv-filter.wdl" as sv_filter_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-annotsv@2.0.0/src/main/wdl/tasks/sv-annotsv/sv-annotsv.wdl" as sv_annotsv_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-chrom@1.0.3/src/main/wdl/tasks/vcf-split-chrom/latest/vcf-split-chrom.wdl" as vcf_split_chrom_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-vep@1.1.0/src/main/wdl/tasks/vcf-anno-vep/vcf-anno-vep.wdl" as vcf_anno_vep_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.2.1/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-anno-panel@1.1.1/src/main/wdl/tasks/sv-anno-panel/sv-anno-panel.wdl" as sv_anno_panel_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-annot-filter@1.2.2/src/main/wdl/tasks/sv-annot-filter/sv-annot-filter.wdl" as sv_annot_filter_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-breaks-pos@1.0.0/src/main/wdl/tasks/sv-breaks-pos/sv-breaks-pos.wdl" as sv_breaks_pos_task
import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@1.6.10/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@1.0.2/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/raw/igv_screenshots@2.0.0/src/main/wdl/tasks/igv-screenshots/igv-screenshots.wdl" as igv_screenshots_task
import "https://gitlab.com/intelliseq/workflows/raw/sv-report@1.0.0/src/main/wdl/tasks/sv-report/sv-report.wdl" as sv_report_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow sv_calling {

  meta {
    keywords: '{"keywords": ["SV", "calling", "smoove/lumpy"]}'
    name: 'Structural variant analysis'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Structural variant calling and annotation'
    changes: '{"1.2.4": "new igvoskreenshots task", "1.2.3": "Changed default filtering", "1.2.2": "Updated sv-annot-filter task", "1.2.1": "Panel filter bug fix (wrong vcf field name)", "1.2.0" :"New AnnotSV with ACMG ranking", "1.1.8": "new versions of the vcf-to-tsv-or-json and igv tasks", "1.1.7": "grch38-no-alt as default reference, small changes in meta", "1.1.6": "changes in meta, updated bco", "1.1.4": "experiments with meta", "1.1.3": "changes in meta two more contigs removed from analysis", "1.1.2": "changes in meta and new smoove version", "1.1.1": "igv sv true fix", "1.1.0": "lumpy run with smoove"}'

    groups: '{"gene_panel": {"description": "Fill in at least 1 input below if you want to generate the gene panel", "min_inputs": 0}, "hard_filter": {"description": "These options define which structural variants should be treated as reliable","advanced": true}, "report_filter": {"description": "These options allow to filter variants based on their annotations, retained variants will be described in detail in report", "advanced": true}}'
    input_sample_id: '{"index":1, "name": "Sample ID", "type": "String", "default": "no-id", "description": "Sample ID."}'
    input_bam: '{"index": 2, "name": "Bam file", "required": true, "type": "File", "extension": [".bam"], "description": "Input WGS bam file, should be coordinate sorted."}'
    input_bai: '{"index": 3, "name": "Bai index", "required": true, "type": "File", "extension": [".bam.bai", ".bai"], "description": "Index for the bam file."}'
    input_exclude_lcr: '{"index": 12, "name": "Exclude LCR?", "advanced": true, "type": "Boolean", "default": true, "description": "This option decides whether exclude low complexity regions from lumpy/smoove analysis"}'
    input_lcr_bed_source: '{"index": 13, "name": "LCR bed", "type": "Array[String]", "advanced": true, "default": "hall-lab", "constraints": {"values": ["hall-lab","btu356"], "multiselect": "false"},"description": "This option decides which low complexity regions bam should be used by smoove/lumpy. Sources: https://github.com/hall-lab/speedseq/raw/master/annotations/exclude.cnvnator_100bp.GRCh38.20170403.bed (hall-lab), https://doi.org/10.1093/bioinformatics/btu356, Supplementary materials (btu365). Note, that this option works only if exclude_lcr option is set as true"}'
    input_exclude_chroms: '{"index": 14, "name": "Exclude some chromosomes?", "advanced": true, "type": "Boolean", "default": true, "description": "This option decides whether exclude non canonical chromosomes from lumpy/smoove analysis"}'
    input_chroms_string: '{"index": 15, "name": "Chroms string", "advanced": true, "type": "String", "default": "chrM,~chrUn,~random,~:,~decoy,~_alt,chrEBV,hs38d1,chrMT", "description": "Comma-delimited list of chromosomes - SVs with either end in chromosomes from this list will be ignored. If chromosome name starts with ~ it is treated as a regular expression to exclude. Note, that all contigs which are not present in the chosen reference genome fasta file (but are present in analyzed bam) must be excluded. Note, that this option works only if exclude_chroms option is set as true"}'
    input_reference_genome: '{"index": 16, "name": "Reference genome (smoove, duphold)", "advanced": true, "default": "grch38-no-alt", "type": "Array[String]", "constraints": {"values": ["hg38", "grch38-no-alt"], "multiselect": "false"}, "description": "Version of the reference genome that will be used by lupmy/smoove and duphold programms, should be the same as used for alignment"}'
    input_vcf_gz: '{"index": 4, "name": "Vcf file (duphold)", "type": "File",  "extension": [".vcf.gz"], "description": "Vcf file with sample SNP/INDELs data used for heterozygosity annotation (duphold)"}'
    input_vcf_gz_tbi: '{"index": 5, "name": "Tbi index (duphold)", "type": "File",  "extension": [".vcf.gz.tbi", ".vcf.gz.csi"], "description": "Index for the SNP/INDELs vcf file (duphold)"}'
    input_min_qual: '{"index": 17, "name": "Min qual (sv-filter)", "type": "Float", "advanced": true, "groupname": "hard_filter", "default": 30, "description": "Minimal qual of valid variant (sv-filter)"}'
    input_del_cov_max: '{"index": 18, "name": "Deletion coverage max (sv-filter)", "groupname": "hard_filter", "type": "Float", "advanced": true, "default": 0.7, "description": "Maximal coverage of valid deletion (compared to +/-1000bp flanking regions and to genome bins of similar GC content)(sv-filter)"}'
    input_dup_cov_min: '{"index": 19, "name": "Duplication coverage min (sv-filter)", "groupname": "hard_filter", "type": "Float", "default": 1.3, "advanced": true, "description": "Minimal coverage of valid duplication (compared to +/-1000bp flanking regions and to genome bins of similar GC content) (sv-filter)"}'
    input_min_snp_count: '{"index": 20, "name": "Min SNP count (sv-filter)", "type": "Int", "default": 4, "groupname": "hard_filter", "advanced": true,  "description": "Minimal number of SNP/INDELs in deletion region to apply heterozygosity filter (sv-filter)"}'
    input_het_max: '{"index": 21, "name": "Heterozygosity max (sv-filter)", "type": "Float", "default": 0.25, "groupname": "hard_filter", "advanced": true, "description": "Maximal heterozygosity of valid deletion (sv-filter)"}'
    input_hpo_terms: '{"index": 8, "name": "HPO terms", "type": "String", "groupname": "gene_panel", "description": "Enter HPO terms to narrow your search/analysis results (separate HPO terms with comma, for example: HP:0004942, HP:0011675)"}'
    input_diseases: '{"index": 9, "name": "Diseases", "type": "String", "groupname": "gene_panel","description": "Enter disease names to narrow your search/analysis results (separate diseases names with comma; each disease name should be just a keyword, for example for Marfan Syndrome only Marfan should be written, for Ehlers-Danlos Syndrome: Ehlers-Danlos; other proper diseases names for example: Osteogenesis imperfecta, Tay-sachs, Hemochromatosis, Brugada, Canavan, etc.)"}'
    input_phenotypes_description: '{"index": 10, "name": "Description of patient phenotypes", "type": "String", "groupname": "gene_panel", "description": "Enter description of patient phenotypes"}'
    input_panel_names: '{"index": 11, "name": "Gene panel", "type": "Array[String]", "groupname": "gene_panel", "description": "Select gene panels", "constraints": {"values": ["None", "ACMG_Incidental_Findings", "COVID-19_research", "Cancer_Germline", "Cardiovascular_disorders", "Ciliopathies", "Dermatological_disorders", "Dysmorphic_and_congenital_abnormality_syndromes", "Endocrine_disorders", "Gastroenterological_disorders", "Growth_disorders", "Haematological_and_immunological_disorders", "Haematological_disorders", "Hearing_and_ear_disorders", "Metabolic_disorders", "Neurology_and_neurodevelopmental_disorders", "Ophthalmological_disorders", "Rare_Diseases", "Renal_and_urinary_tract_disorders", "Respiratory_disorders", "Rheumatological_disorders", "Skeletal_disorders", "Tumour_syndromes"], "multiselect": true}}'
    input_promoter_size:  '{"index": 25, "name": "Promoter size (AnnotSV)", "type": "Int", "constraints": {"min": 0}, "advanced": true, "default": 500, "description": "Number of bases upstream from the transcription start site (AnnotSV annotation)"}'
    input_include_ci: '{"index": 22, "name": "Include CI? (AnnotSV)", "type": "String", "advanced": true, "constraints": {"values": ["yes", "no"]}, "default": "yes", "description": "This option decides whether variant should be extended by its confidence intervals"}'
    input_panel_threshold1: '{"index": 23, "name": "Panel threshold1 (AnnotSV)", "default": 30.0, "type": "Float", "description": "Panel score threshold for good gene phenotype match"}'
    input_panel_threshold2: '{"index": 24, "name": "Panel threshold2 (AnnotSV)", "default": 45.0, "type": "Float", "description": "Panel score threshold for very good gene phenotype match"}'
    input_gene_panel: '{"name": "Gene panel (sv-annot-filter)", "hidden": true, "type": "File", "description": "Gene panel json. Output of the panel-generate task"}'
    input_all_gene_panel: '{"name": "Full gene panel (AnnotSV)", "hidden": true, "type": "File", "description": "Gene panel json. Output of the panel-generate task"}'
    input_apply_rank_filter: '{"index": 26, "name": "Apply rank filtering? (sv-annot-filter)", "advanced": true, "type": "Boolean", "default": true, "advanced": true, "groupname": "report_filter", "description": "Determines whether SV variants should be filtered depending on their predicted effect."}'
    input_rank_threshold: '{"index": 27, "name": "Rank threshold (sv-annot-filter)", "type": "Array[String]", "advanced": true, "groupname": "report_filter", "constraints": {"values": ["likely_benign", "uncertain", "likely_pathogenic", "pathogenic"]}, "default": "likely_pathogenic", "description": "SV with rank equal to given value or more severe will be kept. This option works only if apply_rank_filter is set as true"}'
    input_softclip: '{"index": 28, "name": "Soft clip? (IGV)", "advanced": true, "default": false, "type": "Boolean", "description": "Decides whether show soft clipped bases (IGV). This option works only if create_pictures option is set as true"}'
    input_range: '{"index": 29, "name": "Range (IGV)", "default": 300, "advanced": true, "type": "Int", "description": "Determines width of the variant surrounding genomic region that will be shown on picture (in bp) (IGV). This option works only if create_pictures option is set as true"}'
    input_create_pictures: '{"index": 7, "name": "Create pictures? (IGV)", "default": true, "type": "Boolean", "description": "Decides whether IGV pictures of the reported SV breakpoints should be created (IGV)"}'
    input_max_igv_picture_processing_time_in_ms: '{"name": "Max picture processing time", "hidden":true, "default": 15000, "type": "Int", "description": "Determines time in which the image should be created."}'
    input_sample_sex: '{"index": 6, "name": "Sample sex", "constraints": { "values": ["M","F","U"], "multiselect": false}, "default": "U","type": "Array[String]", "description": "Sample sex: M - male, F - female, U - unknown"}'

    output_sv_vcf_gz: '{"name": "SV vcf gz", "type": "File", "copy": true, "description": "Genotyped and filtered structural variant vcf"}'
    output_sv_vcf_gz_tbi: '{"name": "SV vcf gz tbi", "type": "File", "copy": true, "description": "Index for output sv vcf"}'
    output_annotated_tsv_gz: '{"name": "AnnotSV tsv output (gzipped)", "copy": true, "type": "Array[File]", "description": "Array with annotated structural variants tsv (gzipped) - output of AnnotSV."}'
    output_annotated_sv_vcf_gz: '{"name": "Annotated SV vcf", "copy": true, "type": "File", "description": "Annotated structural variants vcf (bgzipped) - modified and shortened output of AnnotSV."}'
    output_annotated_sv_vcf_gz_tbi: '{"name": "Index of the annotated vcf", "copy": true, "type": "File", "description": "Index for the annotated SV vcf file."}'
    output_user_gene_file: '{"name": "User gene file", "copy": true, "type": "File", "description": "File with names of candidate genes."}'
    output_bam_stats_json: '{"name": "Bam statistics json", "type": "File", "copy": true, "description": "Json file with alignment statistics"}'
    output_annotated_filtered_sv_vcf_gz: '{"name": "Annotated and filtered SV vcf (bgzipped)", "copy": true, "type": "File", "description": "Annotated and filtered SV vcf (bgzipped)."}'
    output_annotated_filtered_sv_vcf_gz_tbi: '{"name": "Index of the annotated and filtered SV vcf", "copy": true, "type": "File", "description": "Index for the annotated and filtered SV vcf."}'
    output_igv_pngs: '{"name": "Igv screenshots images", "type": "File", "description": "Set of images with igv screenshots generated for SV breakends positions"}'
    output_igv_html: '{"name": "Igv screenshots html files", "type": "File", "description": "Set of html files with igv screenshots generated for SV breakends positions"}'
    output_csv_full_report: '{"name": "CSV report", "type": "File", "description": "Converted to CSV annotated SV vcf"}'
    output_sv_report_pdf: '{"name": "SV report in pdf format", "type": "File", "description": "SV report in pdf format"}'
    output_sv_report_html: '{"name": "SV report in html format", "type": "File", "description": "SV report in html format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": true, "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": true, "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": true, "description": "Biocompute object"}'

  }

  String module_name = "sv_calling"
  String module_version = "1.2.4"
  String sample_id = "no_id_provided"

  File bam
  File bai
  File? all_gene_panel
  File? gene_panel

  ## File with info on short variants may be used by duphold to add info about heterozygous sites
  ## Use if possible, as it allows for spurious deletion removal (by duphold, outside smoove task)
  File? vcf_gz
  File? vcf_gz_tbi


  ## Low complexity regions may be excluded from smoove analysis,
  ## Two files with such regions are provided (in docker) and can be chosen "hall-lab" or "btu356"
  ## "hall-lab" (https://github.com/hall-lab/speedseq/raw/master/annotations/exclude.cnvnator_100bp.GRCh38.20170403.bed)
  ## "btu356" (https://doi.org/10.1093/bioinformatics/btu356, Supplementary materials)
  Boolean exclude_lcr = true
  String lcr_bed_source = "hall-lab"

  ## Some chromosomes may be excluded from smoove analysis
  Boolean exclude_chroms = true
  String chroms_string = "chrM,~chrUn,~random,~:,~decoy,~_alt,chrEBV,hs38d1,chrMT"
  Boolean run_svtyper_by_smoove = true
  Boolean run_duphold_by_smoove = if (defined(vcf_gz) && defined(vcf_gz_tbi)) then false else true

  ## genome used for alignment ("hg38" or "grch38-no-alt"), used in sv-duphold and smoove tasks
  String reference_genome = "grch38-no-alt"

  ## Filtering options
  Float min_qual = 30
  Float del_cov_max = 0.7
  Float dup_cov_min = 1.3
  Int min_snp_count = 4
  Float het_max = 0.25

  ## AnnotSV options and files
  Int promoter_size = 500
  Int sv_minimum_size = 50
  Float panel_threshold1 = 30.0
  Float panel_threshold2 = 45.0
  String include_ci = "yes"   # other option "no"


  ## VEP options
  String other_options = "--no_intergenic --coding_only --hgvs --symbol --biotype --uniprot --total_length --gene_phenotype --overlaps --protein --domains"
  String transcript_filtering = "--exclude_predicted --gencode_basic --pick_allele_gene --pick_order canonical,biotype,mane,appris,tsl,ccds,length --transcript_filter 'biotype match protein_coding'"
  String transcript_info = "--appris --tsl --xref_refseq --mane"

  ## Generate panel inputs
  String? hpo_terms
  String? genes
  String? diseases
  String? phenotypes_description
  Array[String]? panel_names

  Boolean create_panel = (defined(hpo_terms) || defined(genes) || defined(diseases) || defined(phenotypes_description) || defined(panel_names))

  ## Annotation filtering options
  ## Task sv-anno-panel adds panel annotations to vcf
  ## Task sv-annot-filter filters vcf on panel (turned off) and impact (turned on)
  Boolean apply_panel_filter = false
  Boolean apply_rank_filter = true
  String rank_threshold = "likely_pathogenic"

  ## IGV options
  Boolean create_pictures = false
  Int range = 300
  Boolean softclip = false
  Int igv_track_height = 250
  Int max_igv_picture_processing_time_in_ms = 15000

  ## Report options
  Boolean create_report = true
  String sample_sex = "U"

  # i. Extract contig info from bam
  call bam_get_contigs_task.bam_get_contigs {
    input:
      bam = bam,
      bai = bai
  }

  # ii. Call variants with smoove/lumpy
  call sv_smoove_task.sv_smoove {
    input:
      bam = bam,
      bai = bai,
      exclude_lcr = exclude_lcr,
      lcr_bed_source = lcr_bed_source,
      exclude_chroms = exclude_chroms,
      chroms_string = chroms_string,
      run_duphold = run_duphold_by_smoove,
      run_svtyper = run_svtyper_by_smoove,
      sample_id = sample_id,
      reference_genome = reference_genome
  }

  # iii. Run duphold (adds annotations useful for filtering spurious variants)
  if (!run_duphold_by_smoove) {
  call sv_duphold_task.sv_duphold {
    input:
      bam = bam,
      bai = bai,
      svvcf_gz = sv_smoove.sv_vcf_gz,
      svvcf_gz_tbi = sv_smoove.sv_vcf_gz_csi,
      snpvcf_gz = vcf_gz,
      snpvcf_gz_tbi = vcf_gz_tbi,
      reference_genome = reference_genome,
      sample_id = sample_id
  }
  }


  # iv. Run svtyper (added to allow genotyping after running other callers)
  if (!run_svtyper_by_smoove) {
  call sv_typer_task.sv_typer {
    input:
      bam = bam,
      bai = bai,
      svvcf_gz = select_first([sv_duphold.duphold_vcf, sv_smoove.sv_vcf_gz]),
      svvcf_gz_tbi = select_first([sv_duphold.duphold_tbi, sv_smoove.sv_vcf_gz_csi]),
      sample_id = sample_id
  }

  # v. Sort svtyper vcf
  call vcf_gsort_task.vcf_gsort as svtyper_gsort {
    input:
      vcf_gz = sv_typer.gt_svvcf_gz,
      contig_tab = bam_get_contigs.contig_tab
  }
  }


 # vi. Remove low confidence variants
 call sv_filter_task.sv_filter {
   input:
      svvcf = select_first([svtyper_gsort.sorted_vcf_gz, sv_duphold.duphold_vcf, sv_smoove.sv_vcf_gz]),
      svvcf_tbi = select_first([svtyper_gsort.sorted_vcf_gz_tbi, sv_duphold.duphold_tbi, sv_smoove.sv_vcf_gz_csi]),
      min_qual = min_qual,
      del_cov_max = del_cov_max,
      dup_cov_min = dup_cov_min,
      del_cov_gcbin_max = del_cov_max,
      dup_cov_gcbin_min = dup_cov_min,
      min_snp_count = min_snp_count,
      het_max = het_max,
      sample_id = sample_id

 }

 if (sv_filter.vcf_not_empty) {
# vii. Create panel (optionally)
  if (create_panel) {
    call panel_generate_task.panel_generate {
      input:
        hpo_terms = hpo_terms,
        genes = genes,
        diseases = diseases,
        phenotypes_description = phenotypes_description,
        panel_names = panel_names,
        sample_id = sample_id
    }
  }

  if (create_panel || defined(gene_panel)) {
  File? panel_to_use = select_first([gene_panel, panel_generate.panel])
  }

  if (create_panel || defined(all_gene_panel)) {
  File? full_panel = select_first([all_gene_panel, panel_generate.all_panels])
  }

# viii. Split vcf into chromosome wise vcf files ## update version of this task
 call vcf_split_chrom_task.vcf_split_chrom {
   input:
     vcf_basename = sample_id,
     vcf_gz = sv_filter.sv_vcf_gz,
     vcf_gz_tbi = sv_filter.sv_vcf_gz_tbi
 }

 # ix. Add vep annotations
 Array[File] chromosome_vcfs = vcf_split_chrom.chromosomes_vcf_gz
 Array[File] chromosome_tbis = vcf_split_chrom.chromosomes_vcf_gz_tbi

 scatter (index in range(length(chromosome_vcfs))) {
   String string_to_remove = "-" + sample_id + ".*"
   String chrom_from_file_name = sub(basename(chromosome_vcfs[index]), string_to_remove, "")
   call vcf_anno_vep_task.vcf_anno_vep {
     input:
        index = index,
        chromosome = chrom_from_file_name,
        sample_id = sample_id,
        vcf_gz = chromosome_vcfs[index],
        vcf_gz_tbi = chromosome_tbis[index],
        add_loftee = false,
        other_options = other_options,
        transcript_filtering = transcript_filtering,
        transcript_info = transcript_info
    }
 }

 # x. Concatenate chromosome vcf file
 call vcf_concat_task.vcf_concat {
    input:
      vcf_gz = vcf_anno_vep.anno_vcf_gz,
      vcf_gz_tbi = vcf_anno_vep.anno_vcf_gz_tbi,
      vcf_basename = sample_id
 }

 # xi. AnnotSV annotation
 call sv_annotsv_task.sv_annotsv {
   input:
      sv_vcf_gz = vcf_concat.concatenated_vcf_gz,
      sv_vcf_gz_tbi = vcf_concat.concatenated_vcf_gz_tbi,
      panel_file = full_panel,
      promoter_size = promoter_size,
      sv_minimum_size = sv_minimum_size,
      panel_threshold1 = panel_threshold1,
      panel_threshold2 = panel_threshold2,
      include_ci = include_ci,
      sample_id = sample_id
 }

 # xii. Sort AnnotSV vcf
 call vcf_gsort_task.vcf_gsort as annotsv_gsort {
    input:
      vcf_gz = sv_annotsv.annotated_vcf_gz,
      contig_tab = bam_get_contigs.contig_tab
  }


  # xiii. Annotate with panel scores
  call sv_anno_panel_task.sv_anno_panel {
   input:
      vcf_gz = annotsv_gsort.sorted_vcf_gz,
      vcf_gz_tbi = annotsv_gsort.sorted_vcf_gz_tbi,
      gene_panel = panel_to_use,
      sample_id = sample_id
 }

  # xiv. Apply filters (gene panel, frequency, ISEQ classification)
  call sv_annot_filter_task.sv_annot_filter {
    input:
      vcf_gz = sv_anno_panel.panel_anno_vcf_gz,
      vcf_gz_tbi = sv_anno_panel.panel_anno_vcf_gz_tbi,
      apply_panel_filter = (defined(panel_to_use) && apply_panel_filter),
      apply_rank_filter = apply_rank_filter,
      rank_threshold = rank_threshold,
      sample_id = sample_id
 }

 # xv. generate json from annotated and filtered vcf
 call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv as create_filtered_json {
    input:
      vcf_gz = sv_annot_filter.filtered_vcf_gz,
      vcf_gz_tbi = sv_annot_filter.filtered_vcf_gz_tbi,
      sample_id = sample_id,
      output_formats = 'json',
      sv_analysis = true,
      split_ANN_field = false,
      split_ISEQ_REPORT_ANN_field = false
 }

 if (create_pictures) {
   # xvi. Create list of positions for IGV
   call sv_breaks_pos_task.sv_breaks_pos {
     input:
        vcf_gz = sv_annot_filter.filtered_vcf_gz,
        sample_id = sample_id
   }

   # xvii. IGV screenshots
   call igv_screenshots_task.igv_screenshots {
     input:
        positions_list = sv_breaks_pos.position_list,
        sample_bam = bam,
        sample_bai = bai,
        sample_id = sample_id,
        range = range,
        softclip = softclip,
        track_height = igv_track_height,
        max_picture_processing_time_in_ms = max_igv_picture_processing_time_in_ms
   }

   Int igv_output_length = length(igv_screenshots.compressed_igv_pngs)
    if(igv_output_length != 0 ) {
        File pictures_gz = igv_screenshots.compressed_igv_pngs[0]
    }
 }
 }

 # xviii. generate csv from full annotated vcf/ or empty genotyped vcf
 call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv as create_full_tsv {
   input:
     vcf_gz = select_first([sv_anno_panel.panel_anno_vcf_gz, sv_filter.sv_vcf_gz]),
     vcf_gz_tbi = select_first([sv_anno_panel.panel_anno_vcf_gz_tbi, sv_filter.sv_vcf_gz_tbi]),
     sample_id = sample_id,
     output_formats = 'tsv',
     sv_analysis = true,
     split_ANN_field = false,
     split_ISEQ_REPORT_ANN_field = false,
     default_value = "NA"

 }

 if (create_report) {
 # xix Generate report
 call sv_report_task.sv_report {
  input:
    sv_table = create_full_tsv.output_tsv,
    sample_id = sample_id,
    sample_sex = sample_sex
 }
 }

 # xx. Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([bam_get_contigs.bco, sv_smoove.bco,  sv_duphold.bco, sv_typer.bco, svtyper_gsort.bco, sv_filter.bco,
                                      vcf_split_chrom.bco, vcf_concat.bco, sv_annotsv.bco, annotsv_gsort.bco, panel_generate.bco, sv_anno_panel.bco, sv_annot_filter.bco,
                                      create_filtered_json.bco, sv_breaks_pos.bco, igv_screenshots.bco, create_full_tsv.bco, sv_report.bco])
  Array[File] stdout_tasks = select_all([bam_get_contigs.stdout_log, sv_smoove.stdout_log, sv_duphold.stdout_log, sv_typer.stdout_log, svtyper_gsort.stdout_log,
                                         sv_filter.stdout_log, vcf_split_chrom.stdout_log, vcf_concat.stdout_log, sv_annotsv.stdout_log,  annotsv_gsort.stdout_log,
                                         panel_generate.stdout_log, sv_anno_panel.stdout_log, sv_annot_filter.stdout_log, create_filtered_json.stdout_log,
                                         sv_breaks_pos.stdout_log, igv_screenshots.stdout_log, create_full_tsv.stdout_log, sv_report.stdout_log])
  Array[File] stderr_tasks = select_all([bam_get_contigs.stderr_log, sv_smoove.stderr_log, sv_duphold.stderr_log, sv_typer.stderr_log, svtyper_gsort.stderr_log,
                                        sv_filter.stderr_log, vcf_split_chrom.stderr_log, vcf_concat.stderr_log, sv_annotsv.stderr_log, annotsv_gsort.stderr_log,
                                        panel_generate.stderr_log, sv_anno_panel.stderr_log, sv_annot_filter.stderr_log, create_filtered_json.stderr_log,
                                        sv_breaks_pos.stdout_log, igv_screenshots.stderr_log, create_full_tsv.stderr_log, sv_report.stderr_log])

  Array[Array[File]] bco_scatters = select_all([bco_tasks, vcf_anno_vep.bco])
  Array[Array[File]] stdout_scatters = select_all([stdout_tasks, vcf_anno_vep.stdout_log])
  Array[Array[File]] stderr_scatters = select_all([stderr_tasks, vcf_anno_vep.stderr_log])

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)



 call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }



 output {

    File sv_vcf = sv_filter.sv_vcf_gz
    File sv_vcf_tbi = sv_filter.sv_vcf_gz_tbi
    File? bam_stats_json = sv_typer.bam_stats_json

    File? annotated_sv_vcf_gz = sv_anno_panel.panel_anno_vcf_gz
    File? annotated_sv_vcf_gz_tbi = sv_anno_panel.panel_anno_vcf_gz_tbi

    Array[File]? annotsv_tsv_gz = sv_annotsv.full_annotsv_tsv_gz

    File? annotated_filtered_sv_vcf_gz = sv_annot_filter.filtered_vcf_gz
    File? annotated_filtered_sv_vcf_gz_tbi = sv_annot_filter.filtered_vcf_gz_tbi

    File? filtered_json = create_filtered_json.output_json
    File? tsv_full_report = create_full_tsv.output_tsv

    File? igv_pngs = pictures_gz

    File? sv_report_pdf = sv_report.report_pdf
    File? sv_report_html = sv_report.report_html

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}

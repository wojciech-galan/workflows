import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-panel@1.1.2/src/main/wdl/tasks/vcf-filter-panel/vcf-filter-panel.wdl" as vcf_filter_panel_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp3@1.1.0/src/main/wdl/tasks/vcf-acmg-bp3/vcf-acmg-bp3.wdl" as vcf_acmg_bp3_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp5@1.1.2/src/main/wdl/tasks/vcf-acmg-bp5/vcf-acmg-bp5.wdl" as vcf_acmg_bp5_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp7@1.1.0/src/main/wdl/tasks/vcf-acmg-bp7/vcf-acmg-bp7.wdl" as vcf_acmg_bp7_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp1@1.1.2/src/main/wdl/tasks/vcf-acmg-bp1/vcf-acmg-bp1.wdl" as vcf_acmg_bp1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-ba1@1.1.0/src/main/wdl/tasks/vcf-acmg-ba1/vcf-acmg-ba1.wdl" as vcf_acmg_ba1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bs2@1.1.0/src/main/wdl/tasks/vcf-acmg-bs2/vcf-acmg-bs2.wdl" as vcf_acmg_bs2_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm1@1.1.2/src/main/wdl/tasks/vcf-acmg-pm1/vcf-acmg-pm1.wdl" as vcf_acmg_pm1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm2@1.0.3/src/main/wdl/tasks/vcf-acmg-pm2/vcf-acmg-pm2.wdl" as vcf_acmg_pm2_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm5@1.1.3/src/main/wdl/tasks/vcf-acmg-pm5/vcf-acmg-pm5.wdl" as vcf_acmg_pm5_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-bp4@1.1.0/src/main/wdl/tasks/vcf-acmg-bp4/vcf-acmg-bp4.wdl" as vcf_acmg_bp4_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pp2@1.1.2/src/main/wdl/tasks/vcf-acmg-pp2/vcf-acmg-pp2.wdl" as vcf_acmg_pp2_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-ps1@1.1.2/src/main/wdl/tasks/vcf-acmg-ps1/vcf-acmg-ps1.wdl" as vcf_acmg_ps1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-ps3@1.1.2/src/main/wdl/tasks/vcf-acmg-ps3/vcf-acmg-ps3.wdl" as vcf_acmg_ps3_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pp3@1.1.0/src/main/wdl/tasks/vcf-acmg-pp3/vcf-acmg-pp3.wdl" as vcf_acmg_pp3_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pm4@1.1.0/src/main/wdl/tasks/vcf-acmg-pm4/vcf-acmg-pm4.wdl" as vcf_acmg_pm4_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pvs1@2.0.1/src/main/wdl/tasks/vcf-acmg-pvs1/vcf-acmg-pvs1.wdl" as vcf_acmg_pvs1_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-pp4@1.0.0/src/main/wdl/tasks/vcf-acmg-pp4/vcf-acmg-pp4.wdl" as vcf_acmg_pp4_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-summary@1.2.0/src/main/wdl/tasks/vcf-acmg-summary/vcf-acmg-summary.wdl" as vcf_acmg_summary_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@1.0.2/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/-/raw/json-prioritize-var@1.0.0/src/main/wdl/tasks/json-prioritize-var/json-prioritize-var.wdl" as json_prioritize_var_task
import "https://gitlab.com/intelliseq/workflows/-/raw/igv-screenshots@3.0.0/src/main/wdl/tasks/igv-screenshots/igv-screenshots.wdl" as igv_screenshots_task
import "https://gitlab.com/intelliseq/workflows/-/raw/report-acmg@2.2.11/src/main/wdl/tasks/report-acmg/report-acmg.wdl"  as report_acmg_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow vcf_acmg_report {
    meta {
        name: 'VCF ACMG report'
        author: 'https://gitlab.com/gleblavr'
        copyright: 'Copyright 2019 Intelliseq'
        description: 'Adds ACMG annotations, generates report'
        changes: '{"2.1.7": "python script refactor in report-acmg task", "2.1.6": "division into two reports (germline, carrier_screening), corrections in the carrier_screening report", "2.1.5": "Updated methods in Carrier Screening workflows", "2.1.4": "two unnecessary tasks removed (vcf-var-desc and vcf-to-csv)", "2.1.3": "updated pvs1 script (fixed bug with two LoF gene names)","2.1.2": "resolved problem in report-acmg task when the Qual field has NULL value", "2.1.1": "changed report-acmg.wdl - inheritance fields fix, visual corrections in the report", "2.1.0": "pp4 added", "2.0.0": "Changed optional/required inputs, new method descriptions in the acmg report", "1.4.3": "New igv-screenshots and report tasks", "1.4.2": "new PVS1 script (required VEP and loftee annotation)", "1.4.1": "update header in docx report template", "1.4.0": "targeted sequencing report", "1.3.1": "rewriting a repetitive part of code into a bash script in report-acmg task", "1.3.0": "changes in report-acmg task (new report template), vcf-to-json-or-tsv task updated, json-prioritize-var task added and input waiting_time increased to 100000", "1.2.7": "resources updated, python scripts reformatted, moved from latest dir, new bco-merge", "1.2.6": "updated igv.min.js (igv screenshots task)", "1.2.5": "correction in counting time difference between zones", "1.2.4": "optional panel filtering (even if panel given)"}'

        input_vcf_gz: '{"name": "Variant file", "type": "File",  "extension": [".vcf"], "description": "Output form variant calling module"}'
        input_vcf_gz_tbi: '{"name": "Variant file index", "type": "File",  "extension": [".vcf.gz"], "description": "Output form variant calling module"}'
        input_sample_info_json: '{"name": "Patient and sample data", "type": "File", "extension": [".json"], "description": "Patient and sample data in json format, must include attributes (even if without values): name, surname, sex, birthdate, pesel, ID, material, sequencing_type, sequencing_platform, sending_date, raport_date, doctor_name, doctor_surname", "required": "False"}'
        input_panel_json: '{"name": "Panel data", "type": "File", "extension": [".json"], "description": "Panel data in json format, must include attributes (even if without values): genes_number, genes"}'
        input_panel_inputs_json: '{"name": "List of inputs including phenotypes names", "type": "File", "extension": [".json"], "description": "Add json file with inputs including phenotypes names used to generate gene panel. You can prepare this file using workflow Gene panel generator"}'
        input_bam: '{"name": "Sample bam", "type": "File", "extension": [".bam"], "description": "Output from alignment module"}'
        input_bai: '{"name": "Sample bai", "type": "File", "extension": [".bai"], "description": "Output from alignment module"}'
        input_realigned_bam: '{"name": "Sample realigned bam", "type": "File", "extension": [".bam"], "description": "Output from HaplotypeCaller (variant-calling module)"}'
        input_realigned_bai: '{"name": "Sample realigned bai", "type": "File", "extension": [".bai"], "description": "Output from HaplotypeCaller (variant-calling module)"}'
        input_other_bams: '{"name": "Other bams", "type": "Array[File]", "extension": [".bam"], "description": "Bams files used for igv-screenshots comparison between samples, default: empty array"}'
        input_other_bais: '{"name": "Other bais", "type": "Array[File]", "extension": [".bai"], "description": "Bais files needed for igv-screenshots comparison between samples, default: empty array"}'
        input_sample_id: '{"name": "Sample ID", "type": "String"}'
        input_genome_or_exome: '{"name": "Sample type", "type": "String", "default": "exome", "constraints": {"values": ["exome", "genome"]}, "description": "Select exome or genome (refers to the input sample)"}'
        input_waiting_time: '{"name": "Waiting time", "default": "10000", "type": "Int", "description": "Determines time between loading of the html file by google-chrome and screenshot, make it longer if the final png pictures are not fully loaded"}'

        output_annotated_acmg_vcf_gz: '{"name": "Variant file annotated with ACMG criteria", "type": "File", "copy": "True", "description": "Vcf file with ACMG annotations added"}'
        output_annotated_acmg_vcf_gz_tbi:  '{"name": "ACMG annotated vcf index", "type": "File", "copy": "True", "description": "ACMG annotated vcf file index"}'
        output_ang_pdf_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, pdf format, English version"}'
        output_ang_html_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, html format, English version"}'
        output_ang_docx_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, docx format, English version"}'
        output_ang_odt_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, odt format, English version"}'
        output_pdf_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, pdf format, Polish version"}'
        output_html_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, html format, Polish version"}'
        output_docx_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, docx format, Polish version"}'
        output_odt_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, odt format, Polish version"}'
        output_igv_screenshots_tar_gz: '{"name": "Igv screenshots", "type": "File", "description": "Set of html files with igv screenshots generatd for a list of alignment positions."}'
        output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
        output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Module biocompute object"}'
        output_bco_report: '{"name": "Biocompute object report", "type": "File", "copy": "True", "description": "Module biocompute object report"}'
        output_converted_vcf_to_csv: '{"name": "Converted vcf to csv","type": "File", "copy": "True", "description": "CSV file with vcf data"}'
    }

    ## ClinVar and Uniprot update: 09-12-2020

    String module_name = "vcf_acmg_report"
    String module_version = "2.1.7"
    File vcf_gz
    File vcf_gz_tbi
    Array[File]? bams
    Boolean is_bams_defined = defined(bams)
    Array[File]? bams_bais
    File? panel_json
    Boolean provided_panel_json = if defined(panel_json) then true else false
    File? sample_info_json
    File? panel_inputs_json
    File? genes_bed_target_json
    Int timezoneDifference = 0
    String? analysisSpecifier
    String analysis_start = "vcf"
    String analysis_group = "germline"
    Boolean apply_panel_filter = true
    String? sample_id
    String basename = basename(vcf_gz, ".vcf.gz")
    String identifier = select_first([sample_id, basename])
    String genome_or_exome = "exome"
    Int max_igv_picture_processing_time_in_ms = 100000

    ## filtering is optional
    call vcf_filter_panel_task.vcf_filter_panel {
        input:
            vcf_gz = vcf_gz,
            vcf_gz_tbi = vcf_gz_tbi,
            panel_json = panel_json,
            apply_filter = apply_panel_filter
    }

    call vcf_acmg_bp3_task.vcf_acmg_bp3 {
        input:
            vcf = vcf_filter_panel.filtered_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_bp1_task.vcf_acmg_bp1 {
        input:
            vcf = vcf_acmg_bp3.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_bp5_task.vcf_acmg_bp5 {
        input:
            vcf = vcf_acmg_bp1.annotated_acmg_vcf_gz,
            sample_id = identifier
    }
    call vcf_acmg_bp7_task.vcf_acmg_bp7 {
        input:
            vcf = vcf_acmg_bp5.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_ba1_task.vcf_acmg_ba1 {
        input:
            vcf = vcf_acmg_bp7.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_bs2_task.vcf_acmg_bs2 {
        input:
            vcf = vcf_acmg_ba1.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_pm1_task.vcf_acmg_pm1 {
        input:
            sample_id = identifier,
            vcf = vcf_acmg_bs2.annotated_acmg_vcf_gz
    }

    call vcf_acmg_pm2_task.vcf_acmg_pm2 {
        input:
            vcf = vcf_acmg_pm1.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_ps1_task.vcf_acmg_ps1 {
        input:
            vcf = vcf_acmg_pm2.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_bp4_task.vcf_acmg_bp4 {
        input:
            vcf = vcf_acmg_ps1.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_pp2_task.vcf_acmg_pp2 {
        input:
            vcf = vcf_acmg_bp4.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_pm5_task.vcf_acmg_pm5 {
        input:
            vcf = vcf_acmg_pp2.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_ps3_task.vcf_acmg_ps3 {
        input:
            sample_id = identifier,
            vcf = vcf_acmg_pm5.annotated_acmg_vcf_gz
    }

    call vcf_acmg_pp3_task.vcf_acmg_pp3 {
        input:
            vcf = vcf_acmg_ps3.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_pm4_task.vcf_acmg_pm4 {
        input:
            vcf = vcf_acmg_pp3.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    call vcf_acmg_pvs1_task.vcf_acmg_pvs1 {
        input:
            vcf = vcf_acmg_pm4.annotated_acmg_vcf_gz,
            sample_id = identifier
    }

    if (provided_panel_json) {
        call vcf_acmg_pp4_task.vcf_acmg_pp4 {
            input:
                vcf = vcf_acmg_pvs1.annotated_acmg_vcf_gz,
                sample_id = identifier
       }
    }

    call vcf_acmg_summary_task.vcf_acmg_summary {
        input:
            vcf_gz = select_first([vcf_acmg_pp4.annotated_acmg_vcf_gz, vcf_acmg_pvs1.annotated_acmg_vcf_gz]),
            sample_id = identifier
    }

    call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv {
        input:
            vcf_gz = vcf_acmg_summary.annotated_acmg_vcf_gz,
            vcf_gz_tbi = vcf_acmg_summary.annotated_acmg_vcf_gz_tbi,
            split_ANN_field = true,
            split_CSQ_field = true,
            split_ISEQ_REPORT_ANN_field = true,
            output_formats = "json tsv",
            sv_analysis = false
    }

    call json_prioritize_var_task.json_prioritize_var {
        input:
            var_json = vcf_to_json_or_tsv.output_json,
            var_num = 50
    }

    if (is_bams_defined) {
        call igv_screenshots_task.igv_screenshots {
            input:
                positions_list = json_prioritize_var.position_list,
                bams = bams,
                bams_bais = bams_bais,
                sample_id = identifier,
                max_picture_processing_time_in_ms = max_igv_picture_processing_time_in_ms
        }
        Int igv_output_length = length(igv_screenshots.compressed_igv_pngs)
        if(igv_output_length != 0 ) {
            File pictures_gz = igv_screenshots.compressed_igv_pngs[0]
        }
    }

    call report_acmg_task.report_acmg {
        input:
            var_json = json_prioritize_var.selected_var_json,
            sample_info_json = sample_info_json,
            pictures_gz = pictures_gz,
            panel_json = panel_json,
            panel_inputs_json = panel_inputs_json,
            genes_bed_target_json = genes_bed_target_json,
            timezoneDifference = timezoneDifference,
            analysisSpecifier = analysisSpecifier,
            output_name = "report",
            sample_id = identifier,
            genome_or_exome = genome_or_exome,
            analysis_start = analysis_start,
            analysis_group = analysis_group
    }

    Array[File] bco_array = select_all([vcf_filter_panel.bco, vcf_acmg_ba1.bco, vcf_acmg_bp1.bco, vcf_acmg_bp3.bco,
                                       vcf_acmg_bp4.bco, vcf_acmg_bp5.bco, vcf_acmg_bp7.bco, vcf_acmg_bs2.bco,
                                       vcf_acmg_pm1.bco, vcf_acmg_pm4.bco, vcf_acmg_pm2.bco , vcf_acmg_pm5.bco,
                                       vcf_acmg_pp2.bco, vcf_acmg_pp3.bco, vcf_acmg_ps1.bco,  vcf_acmg_ps3.bco,
                                       vcf_acmg_pvs1.bco, vcf_acmg_pp4.bco, vcf_acmg_summary.bco, report_acmg.bco, igv_screenshots.bco])
    Array[File] stdout_array = select_all([vcf_filter_panel.stdout_log, vcf_acmg_ba1.stdout_log, vcf_acmg_bp1.stdout_log,
                                           vcf_acmg_bp3.stdout_log, vcf_acmg_bp4.stdout_log, vcf_acmg_bp5.stdout_log,
                                           vcf_acmg_bp7.stdout_log, vcf_acmg_bs2.stdout_log, vcf_acmg_pm1.stdout_log,
                                           vcf_acmg_pm4.stdout_log, vcf_acmg_pm2.stdout_log , vcf_acmg_pm5.stdout_log,
                                           vcf_acmg_pp2.stdout_log, vcf_acmg_pp3.stdout_log, vcf_acmg_ps1.stdout_log,
                                           vcf_acmg_ps3.stdout_log, vcf_acmg_pvs1.stdout_log, vcf_acmg_pp4.stdout_log,
                                           vcf_acmg_summary.stdout_log, report_acmg.stdout_log, igv_screenshots.stdout_log])
    Array[File] stderr_array = select_all([vcf_filter_panel.stderr_log, vcf_acmg_ba1.stderr_log, vcf_acmg_bp1.stderr_log,
                                           vcf_acmg_bp3.stderr_log, vcf_acmg_bp4.stderr_log, vcf_acmg_bp5.stderr_log,
                                           vcf_acmg_bp7.stderr_log, vcf_acmg_bs2.stderr_log, vcf_acmg_pm1.stderr_log,
                                           vcf_acmg_pm4.stderr_log, vcf_acmg_pm2.stderr_log , vcf_acmg_pm5.stderr_log,
                                           vcf_acmg_pp2.stderr_log, vcf_acmg_pp3.stderr_log, vcf_acmg_ps1.stderr_log,
                                           vcf_acmg_ps3.stderr_log, vcf_acmg_pvs1.stderr_log, vcf_acmg_pp4.stderr_log,
                                           vcf_acmg_summary.stderr_log, report_acmg.stderr_log, igv_screenshots.stderr_log])

    call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {
        File tsv_report = vcf_to_json_or_tsv.output_tsv
        File annotated_acmg_vcf_gz = vcf_acmg_summary.annotated_acmg_vcf_gz
        File annotated_acmg_vcf_gz_tbi = vcf_acmg_summary.annotated_acmg_vcf_gz_tbi
#        File docx_report = report_acmg.variants_report_docx
#        File pdf_report = report_acmg.variants_report_pdf
        File ang_docx_report = report_acmg.variants_report_ang_docx
        File ang_pdf_report = report_acmg.variants_report_ang_pdf
        File? igv_screenshots_tar_gz = pictures_gz
        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco
    }

}

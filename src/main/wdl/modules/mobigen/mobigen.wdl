import "https://gitlab.com/intelliseq/workflows/raw/genotype-gvcf-on-dbsnp-positions@1.0.1/src/main/wdl/tasks/genotype-gvcf-on-dbsnp-positions/genotype-gvcf-on-dbsnp-positions.wdl" as genotype_gvcf_on_dbsnp_positions_task
import "https://gitlab.com/intelliseq/workflows/raw/mobigen@1.0.32/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/imputing-vcf-preproc@1.0.0/src/main/wdl/tasks/imputing-vcf-preproc/latest/imputing-vcf-preproc.wdl" as imputing_vcf_preproc_task
import "https://gitlab.com/intelliseq/workflows/raw/phasing-eagle@1.0.3/src/main/wdl/tasks/phasing-eagle/phasing-eagle.wdl" as phasing_eagle_task
import "https://gitlab.com/intelliseq/workflows/raw/imputing-beagle@1.0.0/src/main/wdl/tasks/imputing-beagle/imputing-beagle.wdl" as imputing_beagle_task
import "https://gitlab.com/intelliseq/workflows/raw/mobigen-models@1.0.2/src/main/wdl/tasks/mobigen-models/mobigen-models.wdl" as mobigen_models_task
import "https://gitlab.com/intelliseq/workflows/raw/mobigen@1.0.13/src/main/wdl/tasks/mobigen-plot/latest/mobigen-plot.wdl" as mobigen_plot_task
import "https://gitlab.com/intelliseq/workflows/raw/report-mobigen@1.0.1/src/main/wdl/tasks/report-mobigen/report-mobigen.wdl" as mobigen_report_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow mobigen_workflow {

  meta {
    keywords: '{"keywords": ["mobigen", "polygenic risk scores"]}'
    name: 'WGS polygenic risk scores full report'
    price: '120'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Identifies polygenic risk variants; generates full consumer report; for whole genome sequencing data (WGS).'
    changes: '{"1.1.8-without-some-vitalleo-content": "removed some vitalleo-specific content", "1.1.8": "add sample_info_json input to report", "1.1.7": "changed reference genome name", "1.1.6": "new eagle, beagle, and genotyping tasks versions", "1.1.5": "add price to meta", "1.1.4": "new version of the phasing-eagle task (more cpu and memory)", "1.1.2": "new version of the phasing-eagle task, dots in file extensions", "1.1.1": "new version of the mobigen-models task", "1.1.0": "mobigen module gets tag 1.1.0, all the task if updated should have its names as tags, not next mobigen version"}'
    tag: 'Consumer WES/WGS'

    input_sample_id: '{"index": 1, "name": "Sample ID", "type": "String", "description": "Enter a sample name (or identifier)"}'
    input_gvcf_gz: '{"index": 2, "name": "gVCF file (bgzipped)", "type": "File", "extension": [".g.vcf.gz"], "description": "Add sample information file in .g.vcf.gz format (bgzipped)"}'
    input_gvcf_gz_tbi: '{"index": 3, "name": "gVCF file (bgzipped) index", "type": "File", "extension": [".g.vcf.gz.tbi"], "description": "Add tabix index of bgzipped gvcf file in .g.vcf.gz.tbi format"}'
    input_reference_genome: '{"hidden": true, "name": "Reference genome", "type": "String", "values": ["broad-institute-hg38", "grch38-no-alt"], "default": "grch38-no-alt", "description": "Version of reference genome that was used to generate gVCF file"}'
    input_sample_info_json: '{"advanced": true, "required": false, "name": "Patient and sample information", "type": "File", "extension": [".json"], "description": "Add patient and sample data in json format"}'


    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": true, "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": true, "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": true, "description": "Biocompute object"}'
    output_report_pdf: '{"name": "Report in PDF", "type": "Array[File]", "copy": true, "description": "Polygenic risk score report in .pdf format"}'
    output_report_odt: '{"name": "Report in ODT", "type": "Array[File]", "copy": true, "description": "Polygenic risk score report in .odt format"}'
    output_report_docx: '{"name": "Report in DOCX", "type": "Array[File]", "copy": true, "description": "Polygenic risk score report in .docx format"}'
    output_report_html: '{"name": "Report in HTML", "type": "Array[File]", "copy": true, "description": "Polygenic risk score report in .html format"}'
  }

  String module_name = "mobigen"
  String module_version = "1.1.8-without-some-vitalleo-content"

  File gvcf_gz
  File gvcf_gz_tbi

  # Patient information
  File? sample_info_json
  Int timezoneDifference = 0

  String sample_id = "no_id_provided"
  String reference_genome

  Int num_of_processes = 8
  String population = "nfe"


  # 1. Genotype gVCF on positions from dbSNP database.

  Array[String] chromosomes = ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]

  scatter (chromosome in chromosomes) {
    call genotype_gvcf_on_dbsnp_positions_task.genotype_gvcf_on_dbsnp_positions {
      input:
        gvcf_gz = gvcf_gz,
        gvcf_gz_tbi = gvcf_gz_tbi,
        basename = sample_id,
        chromosome = chromosome,
        reference_genome = reference_genome
    }
  }

  call vcf_concat_task.vcf_concat {
    input:
     vcf_gz = genotype_gvcf_on_dbsnp_positions.genotyped_on_dbsnp_vcf_gz,
     vcf_gz_tbi = genotype_gvcf_on_dbsnp_positions.genotyped_on_dbsnp_vcf_gz_tbi,
     vcf_basename = sample_id
  }

  # 2. Preprocess the obtained vcf for later steps

  call imputing_vcf_preproc_task.imputing_vcf_preproc {
      input:
        in_file = vcf_concat.concatenated_vcf_gz,
        processes = num_of_processes,
        axiom_pmda = false
  }

  # 3. Phasing

  scatter (path in imputing_vcf_preproc.out_files){
        String file_name = basename(path)
        String chrom = 'chr' + sub(file_name, '.vcf', '')
        call phasing_eagle_task.phasing_eagle {
            input:
            vcf = path,
            chrom = chrom
        }
    }

  # 3. Imputing

  call imputing_beagle_task.imputing_beagle {
    input:
      phased_files = phasing_eagle.phased_vcf_gz,
      threads = num_of_processes,
      chromosomes = chromosomes
  }

  # 4. Models

  call mobigen_models_task.mobigen_models {
    input:
      imputed_files = imputing_beagle.out_files,
      curated_initial_vcf_paths = phasing_eagle.bgzipped_initial_vcf,
      population_name = population,
      trait_directories = "vitalleo_traits"
  }

#  5. Report
  scatter (model_output in mobigen_models.out_files){
    call mobigen_plot_task.mobigen_plot{
      input:
        input_json = model_output
    }

    call mobigen_report_task.report_mobigen{
      input:
        pictures = mobigen_plot.out_files,
        json = model_output,
        sample_id = sample_id,
        sample_info_json = sample_info_json,
        timezoneDifference = timezoneDifference
    }
  }

# Merge bco, stdout, stderr files
  Array[File] bco_tasks = [vcf_concat.bco, imputing_vcf_preproc.bco, imputing_beagle.bco, mobigen_models.bco]
  Array[File] stdout_tasks = [vcf_concat.stdout_log, imputing_vcf_preproc.stdout_log, imputing_beagle.stdout_log, mobigen_models.stdout_log]
  Array[File] stderr_tasks = [vcf_concat.stderr_log, imputing_vcf_preproc.stderr_log, imputing_beagle.stderr_log, mobigen_models.stderr_log]

  Array[Array[File]] bco_scatters = [bco_tasks, genotype_gvcf_on_dbsnp_positions.bco, phasing_eagle.bco, mobigen_plot.bco, report_mobigen.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, genotype_gvcf_on_dbsnp_positions.stdout_log, phasing_eagle.stdout_log, mobigen_plot.stdout_log, report_mobigen.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, genotype_gvcf_on_dbsnp_positions.stderr_log, phasing_eagle.stderr_log, mobigen_plot.stderr_log, report_mobigen.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  output {

  #Array[String] fastqc_status = generate_fastqc_report.fastqc_status
  #Array[File] fastqc_1_report_html = generate_fastqc_report.fastqc_1_report_html
  #Array[File] fastqc_2_report_html = generate_fastqc_report.fastqc_2_report_html
  #Array[File] fastqc_1_report_zip = generate_fastqc_report.fastqc_1_report_zip
  #Array[File] fastqc_2_report_zip = generate_fastqc_report.fastqc_2_report_zip
  #Array[Array[File]] fastqc_statistics_txt = quality_check_fastqc.fastqc_files
  #Array[File] fastqc_1_report_image = generate_fastqc_report.fastqc_1_report_image
  #Array[File] fastqc_2_report_image = generate_fastqc_report.fastqc_2_report_image

    Array[File] report_pdf = report_mobigen.mobigen_report_pdf
    Array[File] report_odt = report_mobigen.mobigen_report_odt
    Array[File] report_docx = report_mobigen.mobigen_report_docx
    Array[File] report_html = report_mobigen.mobigen_report_html

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco
  }
}

import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.0.3/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-mem@2.0.1/src/main/wdl/tasks/fq-bwa-mem/fq-bwa-mem.wdl" as fq_bwa_mem_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-filter-mismatch@3.0.0/src/main/wdl/tasks/bam-filter-mismatch/bam-filter-mismatch.wdl" as bam_filter_mismatch_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-mark-dup@1.0.1/src/main/wdl/tasks/bam-mark-dup/bam-mark-dup.wdl" as bam_mark_dup_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-concat@1.1.0/src/main/wdl/tasks/bam-concat/bam-concat.wdl" as bam_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-seq-grouping@1.0.1/src/main/wdl/tasks/bam-seq-grouping/bam-seq-grouping.wdl" as bam_seq_grouping_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-bsqr@1.1.0/src/main/wdl/tasks/bam-bsqr/bam-bsqr.wdl" as bam_bsqr_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-gather-bsqr@1.1.0/src/main/wdl/tasks/bam-gather-bsqr/bam-gather-bsqr.wdl" as bam_gather_bsqr_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-base-recalib@1.1.0/src/main/wdl/tasks/bam-base-recalib/bam-base-recalib.wdl" as bam_base_recalib_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow fq_bwa_align {

  meta {
  keywords: '{"keywords": ["fastq", "bam", "alignment"]}'
  name: 'Alignment'
  author: 'https://gitlab.com/marysiaa , https://gitlab.com/marpiech, https://gitlab.com/kattom'
  copyright: 'Copyright 2019 Intelliseq'
  description: 'Align reads with bwa-mem'
  changes: '{"1.5.4": "fq-organize and bam-mark-dup with adjustable disks size", "1.5.3": "new version of fq-organize task", "1.5.2": "fq-bwa-mem with set -o -e pipefail (2.0.1)", "1.5.1": "new version of fq-organize task", "1.5.0": "duplicates marked with Picard, mismatch filtering added", "1.4.2": "Works with grch38-no-alt reference, latest dir removed"}'

  input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Enter a sample name (or identifier)"}'
  input_fastqs: '{"name": "Fastq files", "type": "Array[File]", "extension": [".fq.gz", ".fastq.gz"], "description": "Choose list of paired gzipped fastq files both left and right [.fq.gz or .fastq.gz]"}'
  input_fastqs_left: '{"hidden":"true", "name": "First (left) fastq files", "type": "Array[File]", "extension": [".fq.gz", ".fastq.gz"], "description": "Choose first (left) fastq files"}'
  input_fastqs_right: '{"hidden":"true", "name": "Second (right) fastq files", "type": "Array[File]", "extension": [".fq.gz", ".fastq.gz"], "description": "Choose second (right) fastq files"}'
  input_add_bam_filter: '{"name": "Add bam filter?", "type": "Boolean", "default": true, "description": "Decides whether bam filtering on contamination should be applied"}'
  input_filter_choice: '{"name": "Filter choice", "type": "String", "constraints": {"choices": ["1", "2", "3"]}, "default": "3", "description": "Which filters should be applied: 1 - removing reads with high fraction of mismatches only, 2 - removing reads soft-clipped from both ends and with mapped part shorter than 33, 3 - both filters"}'
  input_mismatch_threshold: '{"name": "Mismatch threshold (for filters 1 and 3)", "type": "Float", "constraints": {"min": 0.0, "max": 1.0}, "default": 0.1, "description": "Reads with fraction of mismatches/indels bigger than this value are removed"}'
  input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"choices": ["hg38", "grch38-no-alt"]}, "default": "grch38-no-alt", "description": "Reference genome"}'

  output_recalibrated_markdup_bam: '{"name": "Markdup bam", "type": "File", "description": "Aligned bam file"}'
  output_recalibrated_markdup_bai: '{"name": "Markdup bai", "type": "File", "description": "Index for the aligned bam file"}'
  output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
  output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
  output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  String module_name = "fq_bwa_align"
  String module_version = "1.5.4"

  String reference_genome = "grch38-no-alt" #or "hg38"

  Array[File]? fastqs_left
  Array[File]? fastqs_right

  Array[File]? fastqs
  Boolean is_fastqs_defined = defined(fastqs)

  String sample_id = "no_id_provided"

  ## Give false to turn off contamination filtering
  Boolean add_bam_filter = true
  ## Which filter should be applied?:
  ## 1 - removing reads with high fraction of mismatches
  ## 2 - removing reads which are both very short and with soft-clipped at both ends
  ## 3 - combining both filter types (1+2)
  String filter_choice = "3"
  ## Set mismatch fraction that is not accepted (for filters 1 and 3)
  Float mismatch_threshold = 0.1

  ## Set as none to disable optical duplicates marking, do not provide anything otherwise
  String? read_name_regex

  String java_options = "-Xmx14g"


  # 0. Organize fastqs
  if(is_fastqs_defined) {
      call fq_organize_task.fq_organize {
          input:
              fastqs = fastqs,
              paired = true
      }
  }

  Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
  Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])

  # 1. Align reads
  scatter (index in range(length(fastqs_1))) {
      call fq_bwa_mem_task.fq_bwa_mem {
          input:
              fastq_1 = fastqs_1[index],
              fastq_2 = fastqs_2[index],
              sample_id = sample_id,
              reference_genome = reference_genome,
              index = index
      }
  }
  # 2. Filter out contaminated reads (optionally)
  if(add_bam_filter) {
      scatter (index in range(length(fastqs_1))) {
          call bam_filter_mismatch_task.bam_filter_mismatch {
              input:
                  bam = fq_bwa_mem.bwa_unsorted_bam[index],
                  mismatch_threshold = mismatch_threshold,
                  filter_choice = filter_choice,
                  index = index
          }
     }
  }

  Array[File] bams_to_md = select_first([bam_filter_mismatch.filtered_bam, fq_bwa_mem.bwa_unsorted_bam])

  # 3. Mark duplicates, merge and sort
  call bam_mark_dup_task.bam_mark_dup {
      input:
          bams = bams_to_md,
          sample_id = sample_id,
          read_name_regex = read_name_regex,
          md_java_options = java_options,
          sort_java_options = java_options
  }


  # 4. Generate the recalibration model by interval
  call bam_seq_grouping_task.bam_seq_grouping {
      input:
          reference_genome = reference_genome
  }

  Array[Array[String]] sequence_grouping = read_tsv(bam_seq_grouping.sequence_grouping_txt)
  Array[Array[String]] sequence_grouping_with_unmapped = read_tsv(bam_seq_grouping.sequence_grouping_with_unmapped_txt)

  scatter (index in range(length(sequence_grouping))) {
      call bam_bsqr_task.bam_bsqr {
          input:
              index = index,
              sequence_group_interval = sequence_grouping[index],
              markdup_bam = bam_mark_dup.mark_dup_bam,
              markdup_bai = bam_mark_dup.mark_dup_bai,
              sample_id = sample_id
      }
  }

  # 6. Merge the recalibration reports resulting from by-interval recalibration
  call bam_gather_bsqr_task.bam_gather_bsqr {
      input:
          partial_recalibration_reports = bam_bsqr.partial_recalibration_report,
          sample_id = sample_id
  }

  # 7. Perform base recalibration
  scatter (index in range(length(sequence_grouping_with_unmapped))) {
      call bam_base_recalib_task.bam_base_recalib {
          input:
              index = index,
              sequence_group_interval = sequence_grouping_with_unmapped[index],
              markdup_bam = bam_mark_dup.mark_dup_bam,
              markdup_bai = bam_mark_dup.mark_dup_bai,
              recalibration_report = bam_gather_bsqr.recalibration_report,
              sample_id = sample_id
      }
  }

  # 8. Merge the recalibrated BAM files resulting from by-interval recalibration
  call bam_concat_task.bam_concat as bam_concat_recalib {
      input:
          interval_bams = bam_base_recalib.partial_recalibrated_markdup_bam,
          sample_id = sample_id,
          bam_name = "recalibrated-markdup"
  }

  # 9. Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([fq_organize.bco, bam_mark_dup.bco, bam_concat_recalib.bco, bam_seq_grouping.bco, bam_gather_bsqr.bco])
  Array[File] stdout_tasks = select_all([fq_organize.stdout_log, bam_mark_dup.stdout_log, bam_concat_recalib.stdout_log,
                                         bam_seq_grouping.stdout_log, bam_gather_bsqr.stdout_log])
  Array[File] stderr_tasks = select_all([fq_organize.stderr_log, bam_mark_dup.stderr_log, bam_concat_recalib.stderr_log,
                                         bam_seq_grouping.stderr_log, bam_gather_bsqr.stderr_log])

  Array[Array[File]] bco_scatters = select_all([bco_tasks, bam_filter_mismatch.bco, fq_bwa_mem.bco, bam_bsqr.bco, bam_base_recalib.bco])
  Array[Array[File]] stdout_scatters = select_all([stdout_tasks, bam_filter_mismatch.stdout_log, fq_bwa_mem.stdout_log, bam_bsqr.stdout_log, bam_base_recalib.stdout_log])
  Array[Array[File]] stderr_scatters = select_all([stderr_tasks, fq_bwa_mem.stderr_log, bam_filter_mismatch.stderr_log, bam_bsqr.stderr_log, bam_base_recalib.stderr_log])

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  output {
    File recalibrated_markdup_bam = bam_concat_recalib.bam
    File recalibrated_markdup_bai = bam_concat_recalib.bai

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-multiallelic@1.1.0/src/main/wdl/tasks/vcf-split-multiallelic/vcf-split-multiallelic.wdl" as vcf_split_multiallelic_task
import "https://gitlab.com/intelliseq/workflows/-/raw/vcf-filter-clinical-test@1.0.0/src/main/wdl/tasks/vcf-filter-clinical-test/vcf-filter-clinical-test.wdl" as vcf_filter_clinical_test_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-split-chrom@1.1.1/src/main/wdl/tasks/vcf-split-chrom/vcf-split-chrom.wdl" as vcf_split_chrom_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-freq@2.1.0/src/main/wdl/tasks/vcf-anno-freq/vcf-anno-freq.wdl" as vcf_anno_freq_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-freq@1.3.1/src/main/wdl/tasks/vcf-filter-freq/vcf-filter-freq.wdl" as vcf_filter_freq_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-snpeff@1.2.3/src/main/wdl/tasks/vcf-anno-snpeff/vcf-anno-snpeff.wdl" as vcf_anno_snpeff_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-filter-snpeff@1.1.0/src/main/wdl/tasks/vcf-filter-snpeff/vcf-filter-snpeff.wdl" as vcf_filter_snpeff_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-vep@1.1.0/src/main/wdl/tasks/vcf-anno-vep/vcf-anno-vep.wdl" as vcf_anno_vep_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-dbsnp@1.2.0/src/main/wdl/tasks/vcf-anno-dbsnp/vcf-anno-dbsnp.wdl" as vcf_anno_dbsnp_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-scores@1.1.0/src/main/wdl/tasks/vcf-anno-scores/vcf-anno-scores.wdl" as vcf_anno_scores_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-gnomadcov@1.3.0/src/main/wdl/tasks/vcf-anno-gnomadcov/vcf-anno-gnomadcov.wdl" as vcf_anno_gnomadcov_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-dbnsfp@1.2.1/src/main/wdl/tasks/vcf-anno-dbnsfp/vcf-anno-dbnsfp.wdl" as vcf_anno_dbnsfp_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.3.0/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-func-var@1.1.2/src/main/wdl/tasks/vcf-anno-func-var/vcf-anno-func-var.wdl" as vcf_anno_func_var_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-func-gene@1.2.2/src/main/wdl/tasks/vcf-anno-func-gene/vcf-anno-func-gene.wdl" as vcf_anno_func_gene_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-repeat@1.0.3/src/main/wdl/tasks/vcf-anno-repeat/vcf-anno-repeat.wdl" as vcf_anno_repeat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-ghr@1.0.1/src/main/wdl/tasks/vcf-anno-ghr/vcf-anno-ghr.wdl" as vcf_anno_ghr_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-cosmic@1.0.5/src/main/wdl/tasks/vcf-anno-cosmic/vcf-anno-cosmic.wdl" as vcf_anno_cosmic_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-ge-panels@1.0.0/src/main/wdl/tasks/vcf-anno-ge-panels/vcf-anno-ge-panels.wdl" as vcf_anno_ge_panels_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno-civic@1.0.0/src/main/wdl/tasks/vcf-anno-civic/vcf-anno-civic.wdl" as vcf_anno_civic_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-json-or-tsv@0.0.3/src/main/wdl/tasks/vcf-to-json-or-tsv/vcf-to-json-or-tsv.wdl" as vcf_to_json_or_tsv_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow vcf_anno {

    meta {
        keywords: '{"keywords": ["vcf", "annotation"]}'
        name: 'Vcf annotation'
        author: 'https://gitlab.com/marpiech'
        copyright: 'Copyright 2019 Intelliseq'
        description: 'Vcf annotation'
        changes: '{"1.12.0": "Added CIViC annotation task", "1.11.1": "cosmic anno changed to 0-based and Number=. to allow multiallelic records", "1.11.0": "preparing the module to run the carrier screening pipeline, added new task vcf-filter-clinical-test", "1.10.6": "fix regex in Y-and-the rest in vcf-split-chrom","1.10.5": "new version of vcf-anno-dbnsfp with snpsift bug fixed, changed vcf merge tool (bcftools -> MergeVcfs (picrad))", "1.10.4": "vcf-anno-func-gene resources fixed", "1.10.3": "meta updated", "1.10.0": "chroms in scatter taken from file name, new dbSNP resources (154), new dbNSFP resources (4.1c), set -e -o pipefail fixes, all latest dir removed" , "1.9.0": "VEP and loftee added", "1.8.8": "Changed vcf-anno-snpeff (transcript filtering added)", "1.8.7": "Changed vcf-anno-snpeff (Format added to ISEQ_REPORT_ANN field)", "1.8.6": "New resources in freq and gnomadcov", "1.8.5" : "Updated vcf-anno-snpeff, vcf-anno-func-var, vcf-anno-func-gene and vcf-anno-gnomadcov tasks", "1.8.4": "Added vcf-to-json-or-tsv task", "1.8.2": "Typo fix", "1.8.1": "Added missing index", "1.8.0": "Optional filtering on snpEff and frequencies"}'

        input_vcf_gz: '{"name": "Vcf file", "type": "File", "extension": [".vcf.gz"], "description": "Input vcf file."}'
        input_vcf_gz_tbi: '{"name": "Tbi index", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Index for the vcf file."}'
        input_vcf_basename: '{"name": "Vcf basename", "type": "String", "default": "no_input_provided", "description": "Sample ID."}'
        input_vcf_anno_freq_genome_or_exome: '{"name": "Source of the frequencies annotation", "type": "Array[String]", "default": "exome", "advanced": true , "constraints": {"values": ["genome", "exome"]}, "description": "Decides which frequencies resources should be used for annotation; give: *genome* for WGS and custom target sequencing, *exome* for WES."}'
        input_gnomad_coverage_genome_or_exome: '{"name": "Source of the coverage annotation", "type": "Array[String]", "default": "exome", "advanced": true, "constraints": {"values": ["genome", "exome"]}, "description": "Decides which coverage resources should be used for annotation; give: *genome* for WGS and custom target sequencing, *exome* for WES."}'
        input_panel_sequencing: '{"name": "Panel sequencing?", "type": "Boolean", "default": false, "description": "Set as true for custom targeted sequencing experiment, which will turn off frequency and snpEff effect filtering"}'

        output_annotated_and_filtered_vcf:'{"name": "vcf.gz", "type": "File", "copy": "True", "description": "Variants"}'
        output_annotated_and_filtered_vcf_tbi: '{"name": "vcf.gz.tbi", "type": "File", "copy": "True", "description": "Variants index"}'
        output_tsv_from_vcf: '{"name": "tsv", "type": "File", "copy": "True", "description": "Output file annotated_and_filtered_vcf converted to TSV file"}'
        output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
        output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
        output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    }

    String module_name = "vcf_anno"
    String module_version = "1.12.0"

    File vcf_gz
    File vcf_gz_tbi

    String vcf_basename = "no_input_provided"

    String vcf_anno_freq_genome_or_exome = "exome"
    String gnomad_coverage_genome_or_exome = "exome"

    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin
    String snpeff_database = "grch38.102"

    Boolean panel_sequencing = false
    Boolean carrier_screening = false

    String impact_vcf_anno_snpeff = if (panel_sequencing) then "MODIFIER" else "MODERATE"
    String impact_vcf_filter_snpeff = "MODERATE"
    Float threshold_frequency = 0.05

    # VEP options
    Boolean add_loftee = true
    String vep_other_options = "--no_intergenic --hgvs --symbol --biotype --uniprot --total_length"
    String vep_transcript_filtering = "--exclude_predicted --gencode_basic"
    String vep_transcript_info = "--appris --tsl --xref_refseq --mane"

    Boolean apply_freq_filter = if (panel_sequencing || carrier_screening) then false else true
    Boolean apply_snpeff_filter = if (panel_sequencing || carrier_screening) then false else true
    Boolean apply_clinical_filter = if (carrier_screening) then true else false

    # i. Left-normalize indels, split and mark mulitallelic sites
    call vcf_split_multiallelic_task.vcf_split_multiallelic {
        input:
            vcf_gz = vcf_gz,
            vcf_gz_tbi = vcf_gz_tbi,
            vcf_basename = vcf_basename
    }

    # ii. Annotate VCF functional annotation (variant level)
    call vcf_anno_func_var_task.vcf_anno_func_var {
        input:
            vcf_gz = vcf_split_multiallelic.normalized_vcf_gz,
            vcf_gz_tbi = vcf_split_multiallelic.normalized_vcf_gz_tbi,
            vcf_basename = vcf_basename
    }

    # iii. Filter VCF by clinical testing
    if (apply_clinical_filter) {
        call vcf_filter_clinical_test_task.vcf_filter_clinical_test {
            input:
                vcf_gz = vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz,
                vcf_gz_tbi = vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz_tbi,
                vcf_basename = vcf_basename
        }
    }

    File vcf_to_split = select_first([vcf_filter_clinical_test.filtered_vcf_gz, vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz])
    File vcf_tbi_to_split = select_first([vcf_filter_clinical_test.filtered_vcf_gz_tbi, vcf_anno_func_var.annotated_with_functional_annotations_variant_level_vcf_gz_tbi])

    # iv. Split VCF by chromosomes
    call vcf_split_chrom_task.vcf_split_chrom {
        input:
            vcf_gz = vcf_to_split,
            vcf_gz_tbi = vcf_tbi_to_split,
            vcf_basename = vcf_basename
    }

    Array[File] chromosome_vcfs = vcf_split_chrom.chromosomes_vcf_gz
    Array[File] chromosome_tbis = vcf_split_chrom.chromosomes_vcf_gz_tbi


    scatter (index in range(length(chromosome_vcfs))) {
    String string_to_remove = "-" + vcf_basename + ".*"
    String chrom_from_file_name = sub(basename(chromosome_vcfs[index]), string_to_remove, "")

        # v. Annotate VCFs with frequencies
        call vcf_anno_freq_task.vcf_anno_freq {
            input:
                vcf_gz = vcf_split_chrom.chromosomes_vcf_gz[index],
                vcf_gz_tbi = vcf_split_chrom.chromosomes_vcf_gz_tbi[index],
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                genome_or_exome = vcf_anno_freq_genome_or_exome,
                index = index

        }

        # vi. Filter VCFs by frequencies
        if (apply_freq_filter) {
            call vcf_filter_freq_task.vcf_filter_freq {
                input:
                    vcf_basename = vcf_basename,
                    chromosome = chrom_from_file_name,
                    vcf_gz = vcf_anno_freq.annotated_with_frequencies_vcf_gz,
                    vcf_gz_tbi = vcf_anno_freq.annotated_with_frequencies_vcf_gz_tbi,
                    gnomad_af_popmax_threshold = threshold_frequency,
                    mitomap_af_threshold = threshold_frequency,
                    index = index
           }
        }

        File vcf_to_snpeff = select_first([vcf_filter_freq.filtered_by_frequencies_vcf_gz,vcf_anno_freq.annotated_with_frequencies_vcf_gz])
        File vcf_tbi_to_snpeff = select_first([vcf_filter_freq.filtered_by_frequencies_vcf_gz_tbi,vcf_anno_freq.annotated_with_frequencies_vcf_gz_tbi])

        # vii. Annotate VCFs with SnpEff and gene names
        call vcf_anno_snpeff_task.vcf_anno_snpeff {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_to_snpeff,
                vcf_gz_tbi = vcf_tbi_to_snpeff,
                snpeff_database = snpeff_config_vcf_anno_snpeff,
                impact = impact_vcf_anno_snpeff,
                index = index,
                snpeff_java_mem = snpeff_java_mem,
                snpeff_database = snpeff_database
        }

        # viii. Filter VCFs by SnpEff impact
        if (apply_snpeff_filter) {
            call vcf_filter_snpeff_task.vcf_filter_snpeff {
                input:
                    vcf_basename = vcf_basename,
                    chromosome = chrom_from_file_name,
                    vcf_gz = vcf_anno_snpeff.annotated_with_snpeff_vcf_gz,
                    vcf_gz_tbi = vcf_anno_snpeff.annotated_with_snpeff_vcf_gz_tbi,
                    impact = impact_vcf_filter_snpeff,
                    index = index
           }
        }

        File vcf_to_vep = select_first([vcf_filter_snpeff.filtered_by_snpeff_impact_vcf_gz, vcf_anno_snpeff.annotated_with_snpeff_vcf_gz])
        File vcf_tbi_to_vep = select_first([vcf_filter_snpeff.filtered_by_snpeff_impact_vcf_gz_tbi, vcf_anno_snpeff.annotated_with_snpeff_vcf_gz_tbi])

        # ix. Annotate vcf with VEP and loftee
        call vcf_anno_vep_task.vcf_anno_vep {
            input:
                vcf_gz = vcf_to_vep,
                vcf_gz_tbi = vcf_tbi_to_vep,
                sample_id = vcf_basename,
                chromosome = chrom_from_file_name,
                add_loftee = add_loftee,
                other_options = vep_other_options,
                transcript_filtering = vep_transcript_filtering,
                transcript_info = vep_transcript_info,
                index = index
        }

        # x. Annotate VCFs by dbSNP
        call vcf_anno_dbsnp_task.vcf_anno_dbsnp {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_anno_vep.anno_vcf_gz,
                vcf_gz_tbi = vcf_anno_vep.anno_vcf_gz_tbi,
                index = index
        }

        # xi. Annotate VCF with pathogenicity prediction scores, evolutionary
        #    conservation etc.
        call vcf_anno_scores_task.vcf_anno_scores {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_anno_dbsnp.annotated_with_dbsnp_rsids_vcf_gz,
                vcf_gz_tbi = vcf_anno_dbsnp.annotated_with_dbsnp_rsids_vcf_gz_tbi,
                index = index
        }

        # xii. Annotate VCFs with gnomad coverage
        call vcf_anno_gnomadcov_task.vcf_anno_gnomadcov {
            input:
                vcf_gz = vcf_anno_scores.annotated_with_scores_vcf_gz,
                vcf_gz_tbi = vcf_anno_scores.annotated_with_scores_vcf_gz_tbi,
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                genome_or_exome = gnomad_coverage_genome_or_exome,
                index = index
        }

        # xiii. Annotate VCFs by dbNSFP
        call vcf_anno_dbnsfp_task.vcf_anno_dbnsfp {
            input:
                vcf_basename = vcf_basename,
                chromosome = chrom_from_file_name,
                vcf_gz = vcf_anno_gnomadcov.annotated_with_gnomad_coverage_vcf_gz,
                vcf_gz_tbi = vcf_anno_gnomadcov.annotated_with_gnomad_coverage_vcf_gz_tbi,
                index = index

        }
    }

    # xiv. Concatenate VCFs
    call vcf_concat_task.vcf_concat {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_dbnsfp.annotate_vcf_with_dbnsfp_vcf_gz,
            vcf_gz_tbi = vcf_anno_dbnsfp.annotate_vcf_with_dbnsfp_vcf_gz_tbi
    }

    # xv. Annotate VCF functional annotation (gene) level
    call vcf_anno_func_gene_task.vcf_anno_func_gene {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_concat.concatenated_vcf_gz,
            vcf_gz_tbi = vcf_concat.concatenated_vcf_gz_tbi
    }

    # xvi. Annotate VCF simple repeats
    call vcf_anno_repeat_task.vcf_anno_repeat {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_func_gene.annotated_with_functional_annotations_variant_level_vcf_gz,
            vcf_gz_tbi = vcf_anno_func_gene.annotated_with_functional_annotations_variant_level_vcf_gz_tbi
    }

    # xvii. Annotate VCF ghr
    call vcf_anno_ghr_task.vcf_anno_ghr {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz,
            vcf_gz_tbi = vcf_anno_repeat.annotated_with_simple_repeats_vcf_gz_tbi
    }

    # xviii. Annotate VCF ge panels
    call vcf_anno_ge_panels_task.vcf_anno_ge_panels {
        input:
            vcf_basename = vcf_basename,
            vcf_gz = vcf_anno_ghr.annotated_with_ghr_vcf_gz,
            vcf_gz_tbi = vcf_anno_ghr.annotated_with_ghr_vcf_gz_tbi
    }

    # xix. Annotate VCF cosmic
    call vcf_anno_cosmic_task.vcf_anno_cosmic {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_anno_ge_panels.annotated_with_ge_panels_vcf_gz,
            vcf_gz_tbi = vcf_anno_ge_panels.annotated_with_ge_panels_vcf_gz_tbi
    }

    # xx. Annotate VCF civic
    call vcf_anno_civic_task.vcf_anno_civic {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_anno_cosmic.annotated_vcf_gz,
            vcf_gz_tbi = vcf_anno_cosmic.annotated_vcf_gz_tbi
    }

    # xxi. Convert VCF to TSV
    call vcf_to_json_or_tsv_task.vcf_to_json_or_tsv {
        input:
            sample_id = vcf_basename,
            vcf_gz = vcf_anno_civic.annotated_vcf,
            vcf_gz_tbi = vcf_anno_civic.annotated_vcf_tbi,
            output_format = 'tsv',
            split_ANN_field = false,
            split_CSQ_field = false
    }

    # Merge bco, stdout, stderr files
    Array[File] bco_tasks = select_all([vcf_split_multiallelic.bco, vcf_anno_func_var.bco, vcf_filter_clinical_test.bco,
                            vcf_split_chrom.bco, vcf_concat.bco, vcf_anno_func_gene.bco, vcf_anno_repeat.bco, vcf_anno_ghr.bco,
                            vcf_anno_ge_panels.bco, vcf_anno_cosmic.bco, vcf_anno_civic.bco, vcf_to_json_or_tsv.bco])
    Array[File] stdout_tasks = select_all([vcf_split_multiallelic.stdout_log, vcf_anno_func_var.stdout_log,
                               vcf_filter_clinical_test.stdout_log, vcf_split_chrom.stdout_log, vcf_concat.stdout_log,
                               vcf_anno_func_gene.stdout_log, vcf_anno_repeat.stdout_log, vcf_anno_ghr.stdout_log,
                               vcf_anno_ge_panels.stdout_log, vcf_anno_cosmic.stdout_log, vcf_anno_civic.stdout_log,
                               vcf_to_json_or_tsv.stdout_log])
    Array[File] stderr_tasks = select_all([vcf_split_multiallelic.stderr_log, vcf_anno_func_var.stderr_log,
                               vcf_filter_clinical_test.stderr_log, vcf_split_chrom.stderr_log, vcf_concat.stderr_log,
                               vcf_anno_func_gene.stderr_log, vcf_anno_repeat.stderr_log, vcf_anno_ghr.stderr_log,
                               vcf_anno_ge_panels.stderr_log, vcf_anno_cosmic.stderr_log, vcf_anno_cosmic.stderr_log,
                               vcf_to_json_or_tsv.stderr_log])

    Array[Array[File]] bco_scatters = [bco_tasks, vcf_anno_freq.bco, select_all(vcf_filter_freq.bco), vcf_anno_snpeff.bco, select_all(vcf_filter_snpeff.bco), vcf_anno_vep.bco,
                                      vcf_anno_dbsnp.bco, vcf_anno_scores.bco, vcf_anno_gnomadcov.bco, vcf_anno_dbnsfp.bco]
    Array[Array[File]] stdout_scatters = [stdout_tasks, vcf_anno_freq.stdout_log, select_all(vcf_filter_freq.stdout_log), vcf_anno_snpeff.stdout_log, select_all(vcf_filter_snpeff.stdout_log),
                                          vcf_anno_vep.stdout_log, vcf_anno_dbsnp.stdout_log, vcf_anno_scores.stdout_log, vcf_anno_gnomadcov.stdout_log, vcf_anno_dbnsfp.stdout_log]
    Array[Array[File]] stderr_scatters = [stderr_tasks, vcf_anno_freq.stderr_log, select_all(vcf_filter_freq.stderr_log), vcf_anno_snpeff.stderr_log, select_all(vcf_filter_snpeff.stderr_log),
                                          vcf_anno_vep.stderr_log, vcf_anno_dbsnp.stderr_log, vcf_anno_scores.stderr_log, vcf_anno_gnomadcov.stderr_log, vcf_anno_dbnsfp.stderr_log]

    Array[File] bco_array = flatten(bco_scatters)
    Array[File] stdout_array = flatten(stdout_scatters)
    Array[File] stderr_array = flatten(stderr_scatters)

     call bco_merge_task.bco_merge {
        input:
            bco_array = bco_array,
            stdout_array = stdout_array,
            stderr_array = stderr_array,
            module_name = module_name,
            module_version = module_version
    }

    output {

        File annotated_and_filtered_vcf = vcf_anno_civic.annotated_vcf
        File annotated_and_filtered_vcf_tbi = vcf_anno_civic.annotated_vcf_tbi

        File tsv_from_vcf = vcf_to_json_or_tsv.converted_file

        File stdout_log = bco_merge.stdout_log
        File stderr_log = bco_merge.stderr_log
        File bco = bco_merge.bco

    }

}

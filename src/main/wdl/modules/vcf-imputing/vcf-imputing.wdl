import "https://gitlab.com/intelliseq/workflows/raw/vcfs-merge@1.0.1/src/main/wdl/tasks/vcfs-merge/vcfs-merge.wdl" as vcfs_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/imputing-vcf-preproc@1.0.2/src/main/wdl/tasks/imputing-vcf-preproc/latest/imputing-vcf-preproc.wdl" as imputing_vcf_preproc_task
import "https://gitlab.com/intelliseq/workflows/raw/phasing-eagle@1.0.2/src/main/wdl/tasks/phasing-eagle/phasing-eagle.wdl" as phasing_eagle_task
import "https://gitlab.com/intelliseq/workflows/raw/imputing-beagle@2.0.0/src/main/wdl/tasks/imputing-beagle/imputing-beagle.wdl" as imputing_beagle_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-concat@1.3.2/src/main/wdl/tasks/vcf-concat/vcf-concat.wdl" as vcf_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-beagle-postproc@1.0.0/src/main/wdl/tasks/vcf-beagle-postproc/vcf-beagle-postproc.wdl" as vcf_beagle_postproc_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-to-sites-tsv@1.0.2/src/main/wdl/tasks/vcf-to-sites-tsv/vcf-to-sites-tsv.wdl" as vcf_to_sites_tsv_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-beagle-merge@1.0.3/src/main/wdl/tasks/vcf-beagle-merge/vcf-beagle-merge.wdl" as vcf_beagle_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task



workflow vcf_imputing {


  String module_name = "vcf-imputing"
  String module_version = "1.2.2"

  Array[File] vcfs_gz
  Boolean gtc_sites_only = true

  String sample_id = "no_id_provided"
  String reference_genome = "grch38-no-alt-analysis-set"

  Int num_of_processes = 8

  # 0. Merge vcfs
  call vcfs_merge_task.vcfs_merge {
      input:
          vcfs_gz = vcfs_gz
  }

  # 1. Preprocess the obtained vcf for later steps

  call imputing_vcf_preproc_task.imputing_vcf_preproc {
      input:
          in_file = vcfs_merge.vcf_gz,
          processes = num_of_processes,
          axiom_pmda = false,
          filter = false
  }

 # 2. Phasing

  scatter (path in imputing_vcf_preproc.out_files){
      String file_name = basename(path)
      String chrom = 'chr' + sub(file_name, '.vcf', '')
      call phasing_eagle_task.phasing_eagle {
          input:
              vcf = path,
              chrom = chrom
      }
  }

  # 3. Imputing

  call imputing_beagle_task.imputing_beagle {
      input:
          phased_files = phasing_eagle.phased_vcf_gz,
          threads = num_of_processes,
          prodia_reference = true
  }

  if (gtc_sites_only) {
  # 4. Prepare chromosome-wise tsv-files (with information on variant position and alleles)
      call vcf_to_sites_tsv_task.vcf_to_sites_tsv {
          input:
              vcf_gz = vcfs_merge.vcf_gz,
              vcf_gz_tbi = vcfs_merge.vcf_gz_tbi
      }

      #Array[File] tsv_files = vcf_to_sites_tsv.tsv_gz
      #Array[File] tsv_tbi_files = vcf_to_sites_tsv.tsv_gz_tbi


  # 5a. Change Beagle output and select gtc variants only
      scatter (index in range(length(imputing_beagle.out_files))){
           String beagle_file_name_a = basename(imputing_beagle.out_files[index])
           String chrom_a = sub(beagle_file_name_a, '_imputed.vcf.gz', '')
           call vcf_beagle_postproc_task.vcf_beagle_postproc as shorten_beagle_output {
               input:
                   beagle_vcf = imputing_beagle.out_files[index],
                   sites_tsv = vcf_to_sites_tsv.tsv_file,
                   sites_tsv_tbi = vcf_to_sites_tsv.tbi_file,
                   chrom = chrom_a,
                   index = index
          }
      }

 }

 if (!gtc_sites_only) {
 # 5b. Change Beagle output
     scatter (index in range(length(imputing_beagle.out_files))){
         String beagle_file_name_b = basename(imputing_beagle.out_files[index])
         String chrom_b = sub(beagle_file_name_b, '_imputed.vcf.gz', '')
         call vcf_beagle_postproc_task.vcf_beagle_postproc as modify_beagle_output {
             input:
                 beagle_vcf = imputing_beagle.out_files[index],
                 chrom = chrom_b,
                 index = index
        }
     }
 }

  # 6. Merge chromosome-wise vcfs

  call vcf_concat_task.vcf_concat {
      input:
          vcf_gz = select_first([shorten_beagle_output.out_vcf, modify_beagle_output.all_sites_out_vcf]),
          vcf_basename = sample_id,
          beagle_merge = true
  }

  # 7. Merge gtc and imputed vcfs

  call vcf_beagle_merge_task.vcf_beagle_merge {
      input:
          gtc_vcf = vcfs_merge.vcf_gz,
          gtc_vcf_tbi = vcfs_merge.vcf_gz_tbi,
          beagle_vcf = vcf_concat.concatenated_vcf_gz,
          beagle_vcf_tbi = vcf_concat.concatenated_vcf_gz_tbi,
          sample_id = sample_id
  }


  # Merge bco, stdout, stderr files
  Array[File] bco_tasks = select_all([imputing_vcf_preproc.bco, imputing_beagle.bco, vcf_to_sites_tsv.bco,
                                     vcf_concat.bco, vcf_beagle_merge.bco])
  Array[File] stdout_tasks = select_all([imputing_vcf_preproc.stdout_log, imputing_beagle.stdout_log,
                                       vcf_to_sites_tsv.stdout_log, vcf_concat.stdout_log, vcf_beagle_merge.stdout_log])
  Array[File] stderr_tasks = select_all([imputing_vcf_preproc.stderr_log, imputing_beagle.stderr_log,
                                       vcf_to_sites_tsv.stderr_log, vcf_concat.stderr_log, vcf_beagle_merge.stderr_log])

  Array[Array[File]] bco_scatters = select_all([bco_tasks, phasing_eagle.bco,
                                              shorten_beagle_output.bco, modify_beagle_output.bco])
  Array[Array[File]] stdout_scatters = select_all([stdout_tasks, phasing_eagle.stdout_log,
                                                 shorten_beagle_output.stdout_log, modify_beagle_output.stdout_log])
  Array[Array[File]] stderr_scatters = select_all([stderr_tasks, phasing_eagle.stderr_log,
                                                 shorten_beagle_output.stderr_log, modify_beagle_output.stderr_log])

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }


  output {

    File imputed_vcf_gz = vcf_beagle_merge.merged_vcf
    File imputed_vcf_gz_tbi = vcf_beagle_merge.merged_vcf_tbi
    Array[File] imputed_vcfs_gz = vcf_beagle_merge.vcfs_gz    
    Array[File] imputed_vcfs_gz_tbi = vcf_beagle_merge.vcfs_gz_tbi

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}


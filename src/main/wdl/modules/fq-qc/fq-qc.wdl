import "https://gitlab.com/intelliseq/workflows/raw/qc-fq-fastqc@2.0.1/src/main/wdl/tasks/qc-fq-fastqc/qc-fq-fastqc.wdl" as qc_fq_fastqc_task
import "https://gitlab.com/intelliseq/workflows/raw/qc-multiqc@2.0.1/src/main/wdl/tasks/qc-multiqc/qc-multiqc.wdl" as qc_multiqc_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow fq_qc {

  String module_name = "fq_qc"
  String module_version = "1.6.0"

  Array[File] fastqs

  File? sample_info_json
  String sample_id = "no_id_provided" #raport filename
  String? analysis_type  #exome, genome, target, rna-seq
  String? sample_name #change fastq filename for fastqc

  scatter (index in range(length(fastqs))) {
    call qc_fq_fastqc_task.qc_fq_fastqc {
      input:
          fastq = fastqs[index],
          index = index,
          sample_name = sample_name
    }
  }

  Array[File] fastqc_zips = flatten(qc_fq_fastqc.fastqc_zip)

  call qc_multiqc_task.qc_multiqc {
    input:
        qc_results = fastqc_zips,
        sample_id = sample_id,
        analysis_type = analysis_type,
        sample_info_json = sample_info_json
  }


########
#Merge bco, stdout, stderr files

  Array[File] bco_tasks = [qc_multiqc.bco]
  Array[File] stdout_tasks = [qc_multiqc.stdout_log]
  Array[File] stderr_tasks = [qc_multiqc.stderr_log]

  Array[Array[File]] bco_scatters = [bco_tasks, qc_fq_fastqc.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, qc_fq_fastqc.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, qc_fq_fastqc.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stdout_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }



  output {
    Array[File] fastqc_zip = fastqc_zips

    File multiqc_report = qc_multiqc.report_html
    File mutliqc_data_json = qc_multiqc.mutliqc_data_json

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}

import "https://gitlab.com/intelliseq/workflows/raw/bam-split-chroms@1.0.0/src/main/wdl/tasks/bam-split-chroms/bam-split-chroms.wdl" as bam_split_chroms_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-filter-mismatch@2.0.1/src/main/wdl/tasks/bam-filter-mismatch/bam-filter-mismatch.wdl" as bam_filter_mismatch_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-concat@1.1.0/src/main/wdl/tasks/bam-concat/bam-concat.wdl" as bam_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bam-remove-mates@1.0.2/src/main/wdl/tasks/bam-remove-mates/bam-remove-mates.wdl" as bam_remove_mates_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task

workflow bam_filter_contam {

  meta {
  keywords: '{"keywords": ["contamination", "bam", "filtering", "mismatch"]}'
  name: 'Bam contamination filter'
  author: 'https://gitlab.com/kattom'
  copyright: 'Copyright 2019-2020 Intelliseq'
  description: 'Removes from bam files reads with multiple mismatches/indels and their mates'
  changes: '{"1.0.6": "new version of bam-filter-mismatch task and bco-merge"}'

  input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Sample identifier"}'
  input_bam: '{"name": "Bam file", "type": "File", "extension": [".bam"], "description": "Bam file"}'
  input_bai: '{"name": "Bai file", "type": "File", "extension": [".bai"], "description": "Index for the bam"}'
  input_mismatch_threshold: '{"name": "Mismatch threshold", "type": "Float", "constraints": {"min": 0.0, "max": 1.0}, "default": 0.1, "description": "Reads with fraction of mismatches greater than or equal this value are discarded"}'

  output_filtered_bam: '{"name": "Filtered bam", "type": "File", "description": "Bam after multiple mismatch filter"}'
  output_filtered_bai: '{"name": "Filtered bai", "type": "File", "description": "Filtered bam index"}'
  output_removed_reads: '{"name": "Removed reads", "type": "File", "copy": "True", "description": "File with names of the removed read pairs"}'
  output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
  output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
  output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  String module_name = "bam_filter_contam"
  String module_version = "1.0.6"

  File bam
  File bai

  String sample_id = "no_id_provided"
  Float mismatch_threshold = 0.1


   ### Divide bam into chromosome-wise bams

   call bam_split_chroms_task.bam_split_chroms {
      input:
      bam = bam,
      bai = bai
    }


  # Remove reads with multiple mismatches

  scatter (index in range(length(bam_split_chroms.chrom_bams))) {
    call bam_filter_mismatch_task.bam_filter_mismatch {
      input:
        bam = bam_split_chroms.chrom_bams[index],
        mismatch_threshold = mismatch_threshold,
        index = index
     }
    }


  # Gather filtered bam files - to avoid sorting, prior to gathering, files from the output array are rearranged to be in chr1, chr2, chr3 ... chrY-and-the-rest order

   Array[File] fb = bam_filter_mismatch.filtered_bam
   Array[File] fb_rearranged = [fb[10], fb[14], fb[15], fb[16], fb[17], fb[18], fb[19], fb[20], fb[21], fb[0], fb[1], fb[2], fb[3], fb[4], fb[5], fb[6], fb[7], fb[8], fb[9], fb[11], fb[12], fb[13], fb[22], fb[23]]

   call bam_concat_task.bam_concat {
    input:
      interval_bams = fb_rearranged,
      sample_id = sample_id,
      bam_name = "partially-filtered",
      just_gather = true
     # need_sort = "yes"
  }

   # Remove mates of the filtered reads

   call bam_remove_mates_task.bam_remove_mates {
    input:
      sample_id = sample_id,
      bam = bam_concat.bam,
      read_lists = bam_filter_mismatch.removed_reads
  }

   # Merge bco, stdout, stderr files
   Array[File] bco_tasks = [bam_split_chroms.bco, bam_concat.bco, bam_remove_mates.bco]
   Array[File] stdout_tasks = [bam_split_chroms.stdout_log, bam_concat.stdout_log, bam_remove_mates.stdout_log]
   Array[File] stderr_tasks = [bam_split_chroms.stderr_log, bam_concat.stderr_log, bam_remove_mates.stderr_log]

  Array[Array[File]] bco_scatters = [bco_tasks, bam_filter_mismatch.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, bam_filter_mismatch.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, bam_filter_mismatch.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_merge_task.bco_merge {
    input:
        bco_array = bco_array,
        stdout_array = stdout_array,
        stderr_array = stderr_array,
        module_name = module_name,
        module_version = module_version
  }

  output {
    File filtered_bam = bam_remove_mates.filtered_bam
    File filtered_bai = bam_remove_mates.filtered_bai
    File removed_reads = bam_remove_mates.removed_reads

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }
}
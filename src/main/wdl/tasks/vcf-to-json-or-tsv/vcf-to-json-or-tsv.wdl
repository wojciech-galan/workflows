workflow vcf_to_json_or_tsv_workflow {

  meta {
    keywords: '{"keywords": ["VCF", "json", "tsv"]}'
    name: 'vcf_to_json_or_tsv'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Converts vcf into tsv or json'
    changes: '{"1.0.2": "Meta fix, added empty json and tsv file to run on gc", "1.0.1": "fixed TypeError when processing empty (no variants) vcf", "1.0.0": "added possibility to split ISEQ_REPORT_ANN field and return json and tsv simultaneously", "0.0.4": "fixed missing commas in json output", "0.0.3": "small meta fix and g in sed", "0.0.2": "END tag renamed for sv vcf"}'

    input_vcf_gz: '{"index": 1, "name": "Vcf gz", "type": "File", "extension": [".vcf.gz"], "description": "Vcf file"}'
    input_vcf_gz_tbi: '{"index": 2, "name": "Vcf gz tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Tabix vcf index file"}'
    input_sample_id: '{"index": 3, "name": "Sample id", "type": "String", "description": "Identifier of sample"}'
    input_output_formats: '{"index": 4, "name": "Output format", "type": "String", "constraints": {"values": ["json", "tsv"]}, "description": "Output format"}'
    input_split_ANN_field: '{"index": 5, "name": "Split ANN field", "type": "Boolean", "description": "Whether to split ANN file or not"}'
    input_split_CSQ_field: '{"index": 6, "name": "Split CSQ field", "type": "Boolean", "description": "Whether to split CSQ file or not"}'
    split_ISEQ_REPORT_ANN_field: '{"index": 7, "name": "Split ISEQ_REPORT_ANN field", "type": "Boolean", "description": "Whether to split ISEQ_REPORT_ANN file or not"}'
    input_default_value: '{"index": 8, "name": "Default for missing values", "type": "String", "description": "Default for missing values"}'
    input_sv_analysis: '{"index": 9, "name": "SV analysis", "type": "Boolean", "description": "Whether vcf is from structural variant analysis"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_converted_file: '{"name": "Converted file", "type": "File", "copy": "True", "description": "Output file"}'

  }

  call vcf_to_json_or_tsv

}

task vcf_to_json_or_tsv {

  String task_name = "vcf_to_json_or_tsv"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-to-json-or-tsv:1.0.1"

  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no_id_provided"
  String output_formats = 'json tsv'
  Boolean split_ANN_field = true
  Boolean split_CSQ_field = true
  Boolean split_ISEQ_REPORT_ANN_field = true
  Boolean sv_analysis = false
  String? default_value
  String split_ANN_field_string = if split_ANN_field then "--split-ANN-field" else ""
  String split_CSQ_field_string = if split_CSQ_field then "--split-CSQ-field" else ""
  String split_ISEQ_REPORT_ANN_field_string = if split_CSQ_field then "--split-ISEQ_REPORT_ANN-field" else ""
  String default_value_string = if defined(default_value) then "--default-value " + default_value else ""
  String output_name_without_extension = sample_id + "_converted"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   if ${sv_analysis}; then
      zcat ${vcf_gz} | sed 's/\bEND\b/ISEQPYSAMEND/' | bgzip > tmp.vcf.gz
      tabix -p vcf tmp.vcf.gz
   else
      mv ${vcf_gz} tmp.vcf.gz
      mv ${vcf_gz_tbi} tmp.vcf.gz.tbi
   fi


   ## create empty files (optional outputs cause problems on gc)
   touch ${output_name_without_extension}.json
   touch ${output_name_without_extension}.tsv


   python3 /intelliseqtools/vcf_to_json_or_tsv.py --input-vcf tmp.vcf.gz \
                                                  --output-name ${output_name_without_extension} \
                                                  --output-extensions ${output_formats} \
                                                  ${split_ANN_field_string} \
                                                  ${split_CSQ_field_string} \
                                                  ${split_ISEQ_REPORT_ANN_field_string} \
                                                  ${default_value_string}


   if ${sv_analysis}; then
    for extension in tsv json; do
      if [[ -f ${output_name_without_extension}.$extension ]]; then
       sed -i 's/ISEQPYSAMEND/END/g' ${output_name_without_extension}.$extension
      fi
    done
   fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File output_json = "${output_name_without_extension}.json"
    File output_tsv = "${output_name_without_extension}.tsv"
  }

}

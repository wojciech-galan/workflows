workflow bam_split_chroms_workflow {

  meta {
    keywords: '{"keywords": ["bam", "split", "chromosomes"]}'
    name: 'Bam split by chromosomes'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Divides bam file into chromosome wise bams'
    changes: '{"latest": "no changes"}'

    input_bam: '{"name": "Bam", "type": "File", "description": "Input bam file"}'
    input_bai: '{"name": "Bai", "type": "File", "description": "Index for the input bam file"}'

    output_chrom_bams: '{"name": "Chromosome bams", "type": "Array[File]", "copy": "True", "description": "Chromosome wise bam files"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_split_chroms

}

task bam_split_chroms {

  String task_name = "bam_split_chroms"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.1"
  File bam
  File bai
  Boolean unmapped = false


  command <<<
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    set -e -o pipefail
    nice_chroms=$( samtools view -H ${bam} | awk -F '\t' '$1 == "@SQ" {gsub("SN:","", $0); print $2}' | grep -w 'chr[1-9]\|chr[1-2][0-9]\|chrX' )
    ugly_chroms=$( samtools view -H ${bam} | awk -F '\t' '$1 == "@SQ" {gsub("SN:","", $0); print $2}' | grep -wv 'chr[1-9]\|chr[1-2][0-9]\|chrX' )

    echo $nice_chroms | sed 's/ /\n/g' | xargs -i -n 1 -P 24  bash -c "samtools view -h ${bam} {} | samtools view -b > {}.bam"
    samtools view -h ${bam} $ugly_chroms | samtools view -b > chrY-and-the-rest.bam

    if ${unmapped}; then
      samtools view -f 12 -h ${bam} | samtools view -f 12 -b > chr-unmapped.bam
    fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    Array[File] chrom_bams = glob( "chr*.bam")
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

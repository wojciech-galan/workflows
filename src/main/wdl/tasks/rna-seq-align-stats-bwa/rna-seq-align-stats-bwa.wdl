workflow rna_seq_align_stats_bwa_workflow {

  meta {
    keywords: '{"keywords": ["concat bwa align stats"]}'
    name: 'rna_seq_count_concat'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Concatenate bwa align stats'
    changes: '{"1.0.0": "no changes"}'

    input_bwa_align_stats: '{"name": "bwa_align_stats", "type": "Array[File]", "description": "stats files from bwa alignement"}'

    output_stats_concat_excel_file: '{"name": "Stats xlsx", "type": "File", "copy": "True", "description": "Number of alignments for each FLAG type in .xlsx format"}'
    output_stats_concat_tsv_file: '{"name": "Stats tsv", "type": "File", "copy": "True", "description": "Number of alignments for each FLAG type in .xlsx format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_align_stats_bwa

}

task rna_seq_align_stats_bwa {

  Array[File] bwa_align_stats
  String analysis_id = "no_id_provided"

  String task_name = "rna_seq_align_stats_bwa"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-align-stats-bwa:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir stats_files
    cp ${sep=" " bwa_align_stats} stats_files

    python3 /intelliseqtools/bwa-align-stats-concat.py --input-path-to-bwa-align-stats-files "stats_files" \
                                                       --output-file-name ${analysis_id}-stats_concat



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stats_concat_excel_file = "stats_files/${analysis_id}-stats_concat.xlsx"
    File stats_concat_tsv_file = "stats_files/${analysis_id}-stats_concat.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

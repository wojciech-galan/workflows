workflow find_ibd_regions_workflow {

  meta {
    keywords: '{"keywords": ["identity by descent", "run of homozygosity"]}'
    name: 'Finf ibd regions'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Identifies runs of homozygosity'
    changes: '{"latest": "no changes"}'

    input_vcf_gz : '{"name": "Vcf gz", "type": "File", "description": "Chromosome wise VCF file, bgzipped and indexed"}'
    input_vcf_gz_tbi : '{"name": "index for input vcf", "type": "File", "description": "Tabix index for the input vcf file"}'
    input_basename : '{"name": "Sample ID", "type": "String", "description": "Sample id"}'
    input_chr : '{"name": "Chromosome", "type": "String", "values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17","chr18", "chr19", "chr20", "chr21", "chr22", "chrX"], "description": "Chromosome name"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_roh_table: '{"name": "Runs of homozygosity table", "type": "File", "copy": "True", "description": "Txt file with information about roh regions"}'
    output_ibd_bed: '{"name": "Runs of homozygosity regions bed", "type": "File", "copy": "True", "description": "Bed file with information about roh regions"}'
  }

  call find_ibd_regions

}

task find_ibd_regions {

  String task_name = "find_ibd_regions"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String chr
  String docker_image = "intelliseqngs/task_find-ibd-regions:1.0.0-" + chr
  File vcf_gz
  File vcf_gz_tbi

  String basename

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  vcf_dir=$(dirname "${vcf_gz}")
  tbi_name=$(basename "${vcf_gz_tbi}")
  if [ ! -f $vcf_dir/$tbi_name ]; then
    ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
  fi

  ###  filtering out variants within simple repeat regions and finding roh regions
  bcftools roh -G30 -m /resources/genetic-maps/hg38/${chr}.genetic-map-hg38.txt \
                --AF-tag ISEQ_GNOMAD_GENOMES_V3_AF \
                --exclude 'INFO/ISEQ_SIMPLE_REPEAT == "inSimpleRepeat"' \
                 ${vcf_gz} > ${chr}-${basename}_roh-table.txt

  ### creating region bed file with roh regions description
  paste <( awk '$1 =="RG"' ${chr}-${basename}_roh-table.txt | cut -f3-5 |awk '$2 = $2 -1' | awk '$3 = $3 -1' | sed 's/ /\t/g' )  \
  <( paste -d ';' <( awk '$1 =="RG"' ${chr}-${basename}_roh-table.txt | cut -f6 | sed "s/^/ISEQ_IBD_REGION_LENGTH=/" ) \
  <( awk '$1 =="RG"' ${chr}-${basename}_roh-table.txt | cut -f7 | sed "s/^/ISEQ_IBD_REGION_MARKERS=/" ) \
  <( awk '$1 =="RG"' ${chr}-${basename}_roh-table.txt | cut -f8 | sed "s/^/ISEQ_IBD_REGION_QUAL=/" ))  > ${chr}-${basename}_ibd-region.bed

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                          --task-name-with-index ${task_name_with_index} \
                                          --task-version ${task_version} \
                                          --task-docker ${docker_image}

  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File roh_table = "${chr}-${basename}_roh-table.txt"
    File ibd_bed = "${chr}-${basename}_ibd-region.bed"
  }

}

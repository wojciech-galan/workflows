workflow rna_seq_htseq_count_workflow
{
  call rna_seq_htseq_count
}

task rna_seq_htseq_count {

  File bam
  File bam_bai
  File ensembl_gtf
  String sample_id = basename(bam, ".bam")

  String stranded = "no" # Inne możliwe wartości: yes, reverse
  String mode = "union" # Inne możliwe wartości: intersection-strict and intersection-nonempty
  String nonunique = "none" # Inne możliwe wartości: all

  String task_name = "rna_seq_htseq_count"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/htseq-count:1.0.2"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    htseq-count --stranded ${stranded} --mode ${mode} --nonunique ${nonunique} --format=bam --order=pos \
      --samout=${sample_id}-gene-level.sam --type=gene --idattr=gene_id ${bam} ${ensembl_gtf} > ${sample_id}-gene-level.tsv

    htseq-count --stranded ${stranded} --mode ${mode} --nonunique ${nonunique} --format=bam --order=pos \
      --samout=${sample_id}-exon-level.sam --type=exon --idattr=exon_id ${bam} ${ensembl_gtf} > ${sample_id}-exon-level.tsv

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File sample_tbl_htseq_count_gene_level = "${sample_id}-gene-level.tsv"
    File sample_tbl_htseq_count_exon_level = "${sample_id}-exon-level.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

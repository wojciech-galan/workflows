workflow rna_seq_qc_stats_workflow {

  meta {

    keywords: '{"keywords": ["concat qc stats"]}'
    name: 'rna_seq_qc_stats'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Getting basic statistics (paired-end)'
    changes: '{"1.0.3": "update to new template"}'

    input_zip_files: '{"name": "zip_files", "type": "Array[Array[File]]", "description": "zip files (paired-end)"}'

    output_basic_statistics_excel: '{"name": "basic_statistics_excel", "type": "File", "copy": "true", "description": "basic statistics in Excel"}'
    output_basic_statistics_csv: '{"name": "basic_statistics_csv", "type": "File", "copy": "true", "description": "basic statistics in CSV"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call rna_seq_qc_stats

}

task rna_seq_qc_stats {

  Array[Array[File]] zip_files
  Array[File] zip_files_flatten = flatten(zip_files)
  String output_file_name = "basic_statistics"

  String task_name = "rna_seq_qc_stats"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-qc-stats:1.1.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir zip
    ln -s ${sep=" " zip_files_flatten} zip
    unzip zip/\*.zip -d unzipped_fastqc
    cd unzipped_fastqc
    mkdir ../fastqc_data
    mkdir ../summary
    for dir in *     # list directories in the form "/unzipped_fastqc/sample/"
    do
        mv $dir/fastqc_data.txt ../fastqc_data/$dir.txt
        mv $dir/summary.txt ../summary/$dir.txt
    done

    cd ..

    python3 /intelliseqtools/fastqc-basic-statistics-rna-seq.py --input-path-to-fastqc-data-files "fastqc_data" \
                                                                --input-path-to-summary-files "summary" \
                                                                --output-file-name ${output_file_name}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File basic_statistics_excel = "summary/${output_file_name}.xlsx"
    File basic_statistics_csv = "summary/${output_file_name}.csv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

### MultiQC metrics
Metrics from MultiQC json output (with some exemplary values). Ones included in multiqc_stats_json file (qc-multiqc task output) are marked with `#INCLUDE`.


```bash
1. multiqc_general_stats
        "Picard_mqc-generalstats-picard-PERCENT_DUPLICATION": 0.020246,
        "Bowtie 2 / HiSAT2_mqc-generalstats-bowtie_2_hisat2-overall_alignment_rate": 99.08, #(or "Bowtie 1_mqc-generalstats-bowtie_1-reads_aligned_percentage") #INCLUDE 
        "FastQC_mqc-generalstats-fastqc-percent_duplicates": 6.126778539056062, #INCLUDE
        "FastQC_mqc-generalstats-fastqc-percent_gc": 41.0, #INCLUDE
        "FastQC_mqc-generalstats-fastqc-avg_sequence_length": 36.0, #INCLUDE
        "FastQC_mqc-generalstats-fastqc-percent_fails": 0.0, 
        "FastQC_mqc-generalstats-fastqc-total_sequences": 6353880.0 #INCLUDE

2. multiqc_fastqc
        "Filename": "samle-name.fastq.gz",
        "File type": "Conventional base calls",
        "Encoding": "Sanger / Illumina 1.9",
        "Total Sequences": 6353880.0,
        "Sequences flagged as poor quality": 0.0,
        "Sequence length": 36.0,
        "%GC": 41.0,
        "total_deduplicated_percentage": 93.87322146094394, 
        "avg_sequence_length": 36.0,
        "basic_statistics": "pass",
        "per_base_sequence_quality": "pass", #INCLUDE
        "per_tile_sequence_quality": "pass", 
        "per_sequence_quality_scores": "pass", #INCLUDE
        "per_base_sequence_content": "warn", 
        "per_sequence_gc_content": "warn",
        "per_base_n_content": "pass", 
        "sequence_length_distribution": "pass", 
        "sequence_duplication_levels": "pass", 
        "overrepresented_sequences": "pass", #INCLUDE
        "adapter_content": "pass" #INCLUDE

3. multiqc_bowtie2
        "total_reads": 6353880,
        "unpaired_total": 6353880,
        "unpaired_aligned_none": 58509,
        "unpaired_aligned_one": 4372755,
        "unpaired_aligned_multi": 1922616, 
        "overall_alignment_rate": 99.08

4. multiqc_picard_dups
        "LIBRARY": "Unknown Library",
        "UNPAIRED_READS_EXAMINED": 6295371.0,
        "READ_PAIRS_EXAMINED": 0.0,
        "SECONDARY_OR_SUPPLEMENTARY_RDS": 0.0,
        "UNMAPPED_READS": 58509.0,
        "UNPAIRED_READ_DUPLICATES": 127453.0,
        "READ_PAIR_DUPLICATES": 0.0,
        "READ_PAIR_OPTICAL_DUPLICATES": 0.0,
        "PERCENT_DUPLICATION": 0.020246,
        "ESTIMATED_LIBRARY_SIZE": "",
        "READS_IN_DUPLICATE_PAIRS": 0.0,
        "READS_IN_UNIQUE_PAIRS": 0.0,
        "READS_IN_UNIQUE_UNPAIRED": 6167918.0,
        "READS_IN_DUPLICATE_PAIRS_OPTICAL": 0.0,
        "READS_IN_DUPLICATE_PAIRS_NONOPTICAL": 0.0,
        "READS_IN_DUPLICATE_UNPAIRED": 127453.0,
        "READS_UNMAPPED": 58509.0

```

Additional metrics computed in task from fastqc data (all included):


```bash
"FastQC_additional-minimum_mean_quality_score":28.709965180858106,
"FastQC_additional-percent_reads_with_quality_over_30":0.859227,
"FastQC_additional-lower_20_percent_GC_bucket":0.00345594,
"FastQC_additional-upper_20_percent_GC_bucket":0.0020793

```


### MultiQC metrics thresholds file

Only metrics included in thresholds_json will appear in output multiqc_stats_json file. Default thresholds_json:


```bash
{
 "Bowtie 2 / HiSAT2_mqc-generalstats-bowtie_2_hisat2-overall_alignment_rate": {"warning":"80","error":"75","operator":"less","name":"bowtie_2_hisat2-overall_alignment_rate"},
 "Bowtie 1_mqc-generalstats-bowtie_1-reads_aligned_percentage": {"warning":"80","error":"75","operator":"less","name":"bowtie_1-overall_alignment_rate"},
 "FastQC_mqc-generalstats-fastqc-percent_duplicates": {"warning":"10","error":"20","operator":"more","name":"fastqc-percent_duplicates"},
 "FastQC_mqc-generalstats-fastqc-total_sequences": {"warning":"10000000","error":"8000000","operator":"less","name":"fastqc-total_sequences"},
 "FastQC_additional-minimum_mean_quality_score": {"warning":"29","error":"27","operator":"less","name":"fastqc_additional-minimum_mean_quality_score"},
 "FastQC_additional-percent_reads_with_quality_over_30": {"warning":"80","error":"75","operator":"less","name":"fastqc_additional-percent_reads_with_quality_over_30"},
 "FastQC_additional-lower_20_percent_GC_bucket": {"warning":"0.9","error":"1.3","operator":"more","name":"fastqc_additional-lower_20_percent_GC_bucket"},
 "FastQC_additional-upper_20_percent_GC_bucket": {"warning":"1.6","error":"2.0","operator":"more","name":"fastqc_additional-upper_20_percent_GC_bucket"},
 "FastQC_mqc-generalstats-fastqc-avg_sequence_length": {"warning":"20", "error":"10","operator":"less","name":"fastqc-avg_sequence_length"},
 "per_base_sequence_quality": {"operator":"else","name":"fastqc-per_base_sequence_quality"},
 "per_sequence_quality_scores": {"operator":"else","name":"fastqc-per_sequence_quality_scores"},
 "overrepresented_sequences": {"operator":"else","name":"fastqc-overrepresented_sequences"},
 "adapter_content": {"operator":"else","name":"fastqc-avg_sequence_length"}
}
```

For example: for chosen value if the thresholds file contains `"warning":"80","error":"75","operator":"less"` it means that warning will be returned if the value is less than 80 and error when less than 75.

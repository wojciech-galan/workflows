workflow qc_multiqc_workflow
{
  call qc_multiqc
}

task qc_multiqc {

  Array[File] qc_results
  String sample_id = "no_id_provided"

  String? analysis_type #exome, genome, target, rna-seq
  File? sample_info_json
  Boolean create_config = if (defined(analysis_type) && analysis_type != "rna-seq") then true else false
  Boolean exome = if (defined(analysis_type) && analysis_type == "exome") then true else false
  Boolean genome = if (defined(analysis_type) && analysis_type == "genome") then true else false

  File? thresholds_json
  Boolean thresholds_json_provided = defined(thresholds_json)

  String task_name = "qc_multiqc"
  String task_version = "2.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/multiqc:1.1.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   #1. Move QC results and reference files to one directory for MultiQC

   mkdir qc-results
   ln -s ${sep=' ' qc_results} qc-results
   if ${exome}; then
       ln -s /reference-fastq/wes/* qc-results
   elif ${genome}; then
       ln -s /reference-fastq/wgs/* qc-results
   fi


   #2. Create multiqc config file - add sample info if provided and hide button for reference files

   if ${create_config}; then
      python3 /intelliseqtools/multiqc-create-config.py \
        --type ${analysis_type} \
        ${"--sample-info " + sample_info_json} \
        --info-dict /resources/multiqc-config-sample-info-dict.json
   fi


   #3. Run MultiQC, move&rename results and remove ref_sample data from files

   multiqc qc-results

   mv multiqc_report.html ${sample_id}_multiqc-report.html
   for i in  multiqc_data/* ; do
      NAME=$(basename $i | sed "s/_/-/g")
      mv $i multiqc_data/${sample_id}_$NAME
   done

   for i in "_multiqc-fastqc" "_multiqc-general-stats" "_multiqc-sources" ; do
      grep -v "ref_sample" multiqc_data/${sample_id}$i.txt > tmp.txt
      cat tmp.txt > multiqc_data/${sample_id}$i.txt
   done


  #4. Prepare data for multiqc-stats.py

   LIST=$(ls qc-results | grep ".zip" | grep -v "ref_sample" | sed "s/_fastqc.zip//g")
   echo '{' > fastqc-stats.json
   for item in $LIST; do
        unzip qc-results/$item"_fastqc.zip"
        MIN_MEAN_QC=$(grep -A100 ">>Per base sequence quality" $item"_fastqc"/fastqc_data.txt | grep -B100 ">>END_MODULE" | grep "^[0-9]" | cut -f2 | sort -n | head -1)
        PERCENT_QC30=$(grep -A50 ">>Per sequence quality scores" $item"_fastqc"/fastqc_data.txt | grep -B50 ">>END_MODULE" | grep "^[0-9]" | awk '{a+=$2}($1>=30){b+=$2} END {print 100*b/a}')
        PERCENT_GC20=$(grep -A110 ">>Per sequence GC content" $item"_fastqc"/fastqc_data.txt | grep -B110 ">>END_MODULE" | grep "^[0-9]" | awk '{a+=$2}($1<=20){b+=$2}($1>=80){c+=$2} END {print  ",\"FastQC_additional-lower_20_percent_GC_bucket\":" 100*b/a ",\"FastQC_additional-upper_20_percent_GC_bucket\":" 100*c/a}')
        echo "\""$item"\":{\"FastQC_additional-minimum_mean_quality_score\":"$MIN_MEAN_QC",\"FastQC_additional-percent_reads_with_quality_over_30\":"$PERCENT_QC30 $PERCENT_GC20 "}">> fastqc-stats.json
        echo "," >> fastqc-stats.json
        rm -r $item"_fastqc"
   done
   sed -i '$s/,/}/' fastqc-stats.json


   jq ".report_saved_raw_data.multiqc_general_stats * .report_saved_raw_data.multiqc_fastqc" multiqc_data/${sample_id}_multiqc-data.json > multiqc_stats.json
   jq -s '.[0] * .[1]' fastqc-stats.json multiqc_stats.json > stats.json

  if ${thresholds_json_provided}; then
      thresholds_json=${thresholds_json}
  else
      thresholds_json=/resources/multiqc-thresholds.json
  fi


  #5. Compute additional stats and create multiqc-stats.json with multiqc-stats.py

  python3 /intelliseqtools/multiqc-stats.py \
    --stats-json stats.json \
    --thresholds $thresholds_json \
    --id ${sample_id}


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File report_html = "${sample_id}_multiqc-report.html"
    File mutliqc_data_json = "multiqc_data/${sample_id}_multiqc-data.json"
    File multiqc_general_stats_txt = "multiqc_data/${sample_id}_multiqc-general-stats.txt"
    File multiqc_stats_json = "${sample_id}_multiqc-stats.json"
    File multiqc_sources = "multiqc_data/${sample_id}_multiqc-sources.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

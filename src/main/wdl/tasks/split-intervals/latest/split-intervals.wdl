# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  split_intervals
#  *********
#
#  Info:
#  -----
#    Name: split_intervals
#    Version: v0.1
#    Status: Prototype
#    Last edited: 12-07-19
#    Last edited by: <Macel Mroczek>
#    Author(s):
#      + <Marcel Mroczek> <mroczekmarcel@intelliseq.pl>, <https://gitlab.com/mremre>
#    Maintainer(s):
#      + <Marcel Mroczek> <mroczekmarcel@intelliseq.pl>, <https://gitlab.com/mremre>
#    Copyright: Copyright 2019 Intelliseq
#    Licence: All rights reserved
#
#  Description:
#  -----------
#    https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php
#
#  Changes from vy.y:  (OPTIONAL)
#  -------------------
#    <Changes introduced since the last version of the task>
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow split_intervals_workflow {
  Array[String] Chromosomes = ["1","2","3,4","5,6","7,8","9,10,11","12,13,14","15,16,17,18","19,20,21,22","X,Y,M"]
  scatter (chromosome in Chromosomes) {
    call split_intervals {
      input: chromosome=chromosome
    }
}
}

  task split_intervals {

    String chromosome
    File interval_list
    String name

    String task_name = "split_intervals"
    String task_ver = "v0.1"



  #  Int n = length(chromosomes)
  command <<<
    echo ${chromosome} | sed 's/,/ /g' > k;
    smallpart=$(cat k);
    for j in $smallpart; do
      cat ${interval_list} |  sed -n "/chr$j\:/p" >> ${name}"_"${chromosome}.intervals;
    done

    UBUNTU_VER=`awk '/PRETTY_NAME/ {print $2}' /etc/*release`
    UBUNTU=`echo '"ubuntu":"'$UBUNTU_VER',GPL"'`

    echo "\""${task_name}"\""":{\"task-version\":\""${task_ver}"\","$UBUNTU"}" > task-info.json


      >>>

      runtime {

        docker: docker_image
        memory: "500M"
        cpu: "1"
        maxRetries: 2

      }

  output {
    File wyjsce="${name}_${chromosome}.intervals"
  }
}

workflow bam_base_recalib_workflow {

  meta {
    keywords: '{"keywords": ["bam"]}'
    name: 'Bam base recalibration'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Perform bam base quality scores recalibration'
    changes: '{"latest": "no changes"}'

    input_lane_markdup_bam: '{"name": "Markdup bam", "type": "File", "extension": [".bam"], "description": "Bam with duplicated reads marked"}'
    input_lane_markdup_bai: '{"name": "Markdup bai", "type": "File", "extension": [".bai"], "description": "Bam index file"}'
    input_recalibration_report: '{"name": "Recalibration report", "type": "File", "extension": [".csv"], "description": "Recalibration report"}'

    output_partial_recalibrated_markdup_bam: '{"name": "Markdup bam", "type": "File", "description": "Recalibrated bam"}'
    output_partial_recalibrated_markdup_bai: '{"name": "Markdup bai", "type": "File", "description": "Recalibrated bam index"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bam_base_recalib

}

task bam_base_recalib {

  String task_name = "bam_base_recalib"
  String task_version = "1.1.0"
  Int? index = 1
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0-hg38:1.0.0"

  File markdup_bam
  File markdup_bai
  File recalibration_report
  String sample_id = "no_id_provided"
  Array[String] sequence_group_interval = ["chr1"]
  String java_opt = "-Xms3000m"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   echo ${sep=" " sequence_group_interval} | sed 's/ /\n/g' | sed 's/\t/\n/g' > my.intervals

   gatk --java-options ${java_opt} \
       ApplyBQSR \
      -R /resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa \
      -I ${markdup_bam} \
      -O ${index}-${sample_id}_markdup-recalibrated.bam \
      -L my.intervals \
      -bqsr ${recalibration_report} \
      --static-quantized-quals 10 --static-quantized-quals 20 --static-quantized-quals 30 \
      --add-output-sam-program-record false \
      --create-output-bam-md5 \
      --use-original-qualities

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File partial_recalibrated_markdup_bam = "${index}-${sample_id}_markdup-recalibrated.bam"
    File partial_recalibrated_markdup_bai = "${index}-${sample_id}_markdup-recalibrated.bai"

    File stderr_log = stderr()
    File stdout_log = stdout()
    File bco = "bco.json"

  }

}

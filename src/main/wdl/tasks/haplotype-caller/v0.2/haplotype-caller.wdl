# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: haplotype_caller
#  version: latest
#  authors:
#    - <https://gitlab.com/lltw>
#  copyright: Copyright 2019 Intelliseq
#  description: >
#    no description
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow haplotype_caller_workflow {
  call haplotype_caller
}

task haplotype_caller {

  Int index
  File recalibrated_markdup_bam
  File recalibrated_markdup_bai
  String sample_id
  File interval_list
  Float? contamination

  # Inputs with defaults

  # Docker image
  String docker_image = "intelliseqngs/gatk:4.1.2.0_hg38_v1.0"

  # Tools runtime settings, paths etc.
  String gatk_path = "/gatk/gatk"
  String java_mem = "-Xms16000m"
  String java_opt = select_first(["-XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10"])

  # Task name and task version
  String task_name = "haplotype_caller"
  String task_ver = "v0.1"
  command <<<

    set -e

    # Prepare reference genome
#    REF_FASTA=`ls *.fa` &>> ${index}.${sample_id}.call-variants-with-haplotype-caller.stdout.stderr.log
#    PATH_TO_FASTA=`realpath $REF_FASTA`
#    DICT=`ls *.dict`
#    cat $DICT | sed "s&path_to_fasta&$PATH_TO_FASTA&g" > tmp.dict
#    mv tmp.dict $DICT
    #gunzip /resources/reference-genome/Homo_sapiens_assembly38.fa.gz

    ${gatk_path} --java-options "${java_mem} ${java_opt}" \
      HaplotypeCaller \
      -R /resources/reference-genome/Homo_sapiens_assembly38.fa \
      -I ${recalibrated_markdup_bam} \
      -L ${interval_list} \
      -O ${index}.${sample_id}.g.vcf.gz \
      -contamination ${default=0 contamination} -ERC GVCF 2>&1 | tee ${index}.${sample_id}.haplotypecaller-gvcf.stdout.stderr.log

      JAVA_VER=`java -version 2>&1 | awk '/openjdk version/ {print $3}' | tr -d '"'`
      JAVA=`echo '"java_openjdk":"'$JAVA_VER',GPLv2"'`
      CONDA_VER=`conda info | awk '/conda version/ {print $4}'`
      CONDA=`echo '"miniconda":"'$CONDA_VER',BSD 3-Clause"'`
      UBUNTU_VER=`awk '/PRETTY_NAME/ {print $2}' /etc/*release`
      UBUNTU=`echo '"ubuntu":"'$UBUNTU_VER',GPL"'`
      REFGEN=`echo '"reference_genome":"GRCh38"'`

      echo "\""${task_name}"\""":{\"task-version\":\""${task_ver}"\","$JAVA","$CONDA","$UBUNTU","$REFGEN"}" > task-info.json



  >>>

  runtime {
    docker: docker_image
    maxRetries: 3
    memory: "32G"
    cpu: "4"
  }

  output {
    File gvcf_gz = "${index}.${sample_id}.g.vcf.gz"
    File gvcf_gz_tbi = "${index}.${sample_id}.g.vcf.gz.tbi"
    # Logs
    File haplotypecaller_gvcf_stderr_log = "${index}.${sample_id}.haplotypecaller-gvcf.stdout.stderr.log"
    # Task info
    File task_info = "task-info.json"
  }

}

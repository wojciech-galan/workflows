workflow vcf_acmg_ps1_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "PS1", "variant", "pathogenic"]}'
    name: 'Vcf ACMG PS1'
    author: 'https://gitlab.com/gleblavr , https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Evaluates whether variant fulfils ACMG PS1 criterion, adds annotation'
    changes: '{"1.1.2": "Clinvar resources update, reformatted python script"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf"], "description": "Input annotated vcf"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True","type": "File", "description": "Vcf with ACMG PS1 annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_acmg_ps1

}

task vcf_acmg_ps1 {

  String task_name = "vcf_acmg_ps1"
  String task_version = "1.1.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-ps1:1.1.1"
  File vcf
  String sample_id = "no-id"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  bgzip -d ${vcf} -c > tmp.vcf

  clinvar_protein_changes_dictionary=$( ls /resources/clinvar/*/clinvar-protein-changes-dictionary.json )

  python3 /intelliseqtools/acmg-ps1.py \
        --input-vcf tmp.vcf \
        --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz \
        --clinvar-changes $clinvar_protein_changes_dictionary

  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

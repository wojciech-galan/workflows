workflow sv_typer_workflow {

  meta {
    keywords: '{"keywords": ["sv", "genotyping"]}'
    name: 'sv_typer'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Structural variants genotyping'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_bam: '{"name": "Bam", "type": "File", "description": "Alignment file"}'
    input_bai: '{"name": "Bai", "type": "File", "description": "Bam index file"}'
    input_svvcf_gz: '{"name": "SV vcf", "type": "File", "description": "Structural variants vcf file"}'
    input_svvcf_gz_tbi: '{"name": "SV vcf tbi", "type": "File", "description": "Index for sv vcf file"}'

    output_gt_svvcf_gz: '{"name": "Genotyped sv vcf", "type": "File", "copy": "True", "description": "Genotyped structural variant vcf"}'
    output_bam_stats_json: '{"name": "Bam statistics json", "type": "File", "copy": "True", "description": "Json file with alignment statistics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_typer

}

task sv_typer {

  String task_name = "sv_typer"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/svtyper:1.0.0"

  String sample_id ="no-id"
  File bam
  File bai
  File svvcf_gz
  File svvcf_gz_tbi

  # Skip genotyping if SV contains valid reads greater than this threshold
  Int max_reads  = 1000

  # Number of SVs to process in a single batch
  Int batch_size = 1000

  # Number of cpu cores to use
  Int core  = 2

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   zcat ${svvcf_gz} > sv.vcf

    svtyper-sso \
     --core ${core} \
     --batch_size ${batch_size} \
     --max_reads ${max_reads} \
     -i sv.vcf \
     -B ${bam} \
     -l ${sample_id}_bam-stats.json \
      > ${sample_id}_genotyped-sv.vcf

    bgzip ${sample_id}_genotyped-sv.vcf

    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File gt_svvcf_gz = "${sample_id}_genotyped-sv.vcf.gz"
    File bam_stats_json = "${sample_id}_bam-stats.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
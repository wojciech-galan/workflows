workflow resources_kit_workflow {

  meta {
    keywords: '{"keywords": ["interval file"]}'
    name: 'resources_kit'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'interval files preparation'
    changes: '{"1.2.0": "quality chec jsons out", "1.0.18": "Fix bug intevals to intervals", "1.0.17": "Fix bug", "1.0.16": "Add pgx intervals", "1.0.14": "mitochondrial intervals removed (will be analyzed separately)", "1.0.13" : "Works with grch38-no-alt ref genome, wgs intervals with chrM, fixed baits and targets for grch38-no-alts", "1.0.12": "Add new version of wgs.json in docker image, docker image updated", "1.0.2": "changed interval list extension to .interval_list, added bait and target outputs, new docker", "1.0.1": "new docker - non-root permission to resources"}'

    input_kit: '{"name": "Kit", "type": "String", "required": "True", "constraints": {"values": ["genome", "exome-v6", "exome-v7", "custom_pgx"]}, "description": ""}'

    output_interval_list: '{"name": "Interval list", "type": "Array[File]", "description": ""}'
    output_bait: '{"name": "Bait", "type": "Array[File]", "description": ""}'
    output_target: '{"name": "Target", "type": "Array[File]", "description": ""}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call resources_kit

}

task resources_kit {

  String kit = "genome" # can be: "exome-v7", exome-v6, custom_pgx
  String task_name = "resources_kit"
  String task_version = "1.1.1"
  Int? index
  String reference_genome = "grch38-no-alt" # or "hg38"
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_resources-kit:1.2.0"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    if [ ${kit} = genome ];  then
       cp /genome/interval-list intervals.interval_list
       chmod 666 intervals.interval_list

    elif [ ${kit} = custom_pgx ]; then
      cp /pgx/pgx-intervals-2mln-padded_custom-regions.interval_list intervals.interval_list

    elif [ "${kit}" = "exome-v6" ] || [ "${kit}" = "exome-v7" ]
       then
          cp /${kit}/${reference_genome}.interval-list intervals.interval_list
          chmod 666 intervals.interval_list
          cp /${kit}/${reference_genome}.bait bait.interval_list
          chmod 666 bait.interval_list
          cp /${kit}/${reference_genome}.target target.interval_list
          chmod 666 target.interval_list
    fi

  chmod 666 intervals.interval_list

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    Array[File] interval_list = glob("intervals.interval_list")
    Array[File] target = glob("target.interval_list")
    Array[File] bait = glob("bait.interval_list")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
  }

}

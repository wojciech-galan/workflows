workflow panel_generate_workflow {

  meta {
    keywords: '{"keywords": ["gene", "hpo"]}'
    name: 'Panel generate'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generates gene panel based on HPO terms and user defined genes'
    changes: '{"1.7.1": "fixed a bug with the phenotype description (removal of apostrophes)", "1.7.0": "Phenotypes as ", "1.6.10": "All panels added as output", "1.6.9": "Gene panels resources updated, now score is 75 for user defined genes, 50 for genes associated with the specified diseases and 30 for genes from panel. Moreover, if the score from the phenotype is higher, the score is increased", "1.6.8": "deleted None from panel_names values", "1.6.5": "panel names update"}'

    groups: '{"gene_panel": {"description": "Fill in at least 1 input below to generate the gene panel", "min_inputs": 1}}'

    input_sample_id: '{"index": 1, "name": "Sample ID", "type": "String", "description": "Enter a sample name (or identifier)"}'
    input_hpo_terms: '{"index": 2, "name": "HPO terms", "type": "String", "groupname": "gene_panel", "description": "Enter HPO terms to narrow your search/analysis results (separate HPO terms with comma, for example: HP:0004942, HP:0011675)"}'
    input_genes: '{"index": 3, "name": "Genes names", "type": "String", "groupname": "gene_panel", "description": "Enter gene names to narrow your search/analysis results (separate gene names with comma, for example: HTT, FBN1)"}'
    input_diseases: '{"index": 4, "name": "Diseases", "type": "String", "groupname": "gene_panel", "description": "Enter disease names to narrow your search/analysis results (separate diseases names with comma; each disease name should be just a keyword, for example for Marfan Syndrome only Marfan should be written, for Ehlers-Danlos Syndrome: Ehlers-Danlos; other proper diseases names for example: Osteogenesis imperfecta, Tay-sachs, Hemochromatosis, Brugada, Canavan, etc.)"}'
    input_phenotypes_description: '{"index": 5, "name": "Description of patient phenotypes", "type": "String", "groupname": "gene_panel", "description": "Enter description of patient phenotypes"}'
    input_panel_names: '{"index": 6, "name": "Gene panel", "type": "Array[String]", "groupname": "gene_panel", "description": "Select gene panels", "constraints": {"values": ["ACMG_Incidental_Findings", "COVID-19_research", "Cancer_Germline", "Cardiovascular_disorders", "Ciliopathies", "Dermatological_disorders", "Dysmorphic_and_congenital_abnormality_syndromes","Endocrine_disorders", "Gastroenterological_disorders", "Growth_disorders","Haematological_and_immunological_disorders", "Haematological_disorders", "Hearing_and_ear_disorders", "Metabolic_disorders", "Neurology_and_neurodevelopmental_disorders","Ophthalmological_disorders", "Rare_Diseases", "Renal_and_urinary_tract_disorders","Respiratory_disorders", "Rheumatological_disorders", "Skeletal_disorders","Tumour_syndromes"], "multiselect": true}}'

    output_panel: '{"name": "panel", "type": "File", "copy": "True", "description": "Genes panel"}'
    output_phenotypes: '{"name": "phenotypes", "type": "File", "copy": "True", "description": "Hpo names taken from hpo ids"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call panel_generate

}

task panel_generate {

  String sample_id = "no_id_provided"
  String? hpo_terms
  String? genes
  String? diseases
  String dollar = "$"
  String? phenotypes_description
  Array[String]? panel_names

  String task_name = "panel_generate"
  String task_version = "1.7.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_panel-generate:0.0.9"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    # -1. Run the service
    WORKDIR=$(pwd)
    cd /tools/explorare/1.0.0
    java -Xmx4G -jar explorare.jar > /tmp/explorare.log &
    ITER=0
    MAXITER=300
    while ! grep -m1 'Started ExplorareServer' < /tmp/explorare.log; do
        sleep 1
        echo "Waiting for explorare... $ITER"
        let "ITER=ITER+1"
        if [ "$ITER" -gt "$MAXITER" ]; then
            break
    fi
    done
    cd $WORKDIR

    ### Removal of apostrophes from the phenotype description
    DESCRIPTION_TO_BE_CHANGED="${phenotypes_description}"
    PHENOTYPES_DESCRIPTION=$(sed "s#'##g" <<< $DESCRIPTION_TO_BE_CHANGED)

    # 0. Get hpo terms from phenotypes description
    parsedDescription=$(curl --request POST \
     --url 'http://localhost:8082/parse-text' \
     --header 'content-type: application/json' \
     --data '{"query": "'"$PHENOTYPES_DESCRIPTION"'"}')

    hpoTermsFromDescription=$(echo $parsedDescription | jq -r '.hpoTerms[]? | .id')
    hpoTermsConcat="${hpo_terms} $hpoTermsFromDescription"

    # 1. hpo terms
    hpo=$(echo $hpoTermsConcat | sed -r 's/(HP:[[:digit:]]+)[^[:digit:][:alpha:]]*/"\1",/g' | sed 's/,$//' | awk '{print "["$0"]"}')

    curl --request POST \
     --url 'http://localhost:8082/get-genes' \
     --header 'content-type: application/json' \
     --data '{"hpoTerms": '"$hpo"',"threshold": 0.25}' > curl.output

    if grep -q "error" curl.output; then
        cat curl.output >&2
        echo "Probably something wrong with HPO ids" $hpo >&2
        exit 1
    fi

    cat curl.output  | jq -c '[ .[] +{"type":"phenotype"} ]' > phenotype-panel.json

    # 2. user definded genes
    split_genes=$(echo ${genes} | sed 's/, /\n/g' | sed 's/,/\n/g')
    genes_json="["
    for gene in $split_genes
    do
      genes_json=$genes_json'{"name":"'$gene'","score":75.0,"type":"user"},'
    done
    genes_json=$(echo $genes_json] | sed 's/\(.*\),/\1/')
    echo "$genes_json" > user-panel.json

    # 3. diseases genes
    split_diseases=$(echo ${diseases} | sed 's/, /\n/g' | sed 's/,/\n/g' | sed 's/ /%20/g')

    for disease_name in $split_diseases
    do
      disease_name_filename=$(echo $disease_name | sed 's/%20/-/g')
      url_path="http://localhost:8082/get-genes-by-disease-keyword?firstLetters=$disease_name"

      curl --request GET \
        --url $url_path \
        | jq -c '[{"name":.[]}]' | jq --arg type "$disease_name_filename" -c '[ .[] + {score:50.0, $type} ]' > $disease_name_filename-panel.json
    done

    # 4. Getting selected panels from docker

    is_panel_selected=true

    for panel_name in ${sep=' ' panel_names}
    do
      if [ $panel_name = "None" ]
      then
      is_panel_selected=false
      fi
    done

    if [ $is_panel_selected = true ]
    then
    for panel_name in ${sep=' ' panel_names}
    do
      cp /resources/${dollar}panel_name.json ${dollar}panel_name-panel.json
      chmod 666 ${dollar}panel_name-panel.json
    done
    fi

    # 5. merging panels
    jq -s 'add' *-panel.json > all_panels.json
    jq -s 'add' *-panel.json | jq -M 'sort_by(.score)|reverse|unique_by(.name)|sort_by(.score)|reverse' | jq -c . > ${sample_id}_panel.json

    # 6. Getting hpo names from hpo terms

    unformattedHpoNames=$(curl --request POST \
     --url 'http://localhost:8082/get-hpo-names-by-id' \
     --header 'content-type: application/json' \
     --data '{"hpoTerms": '"$hpo"', "threshold": 0.25}')

    readarray -t hpoNames < <(echo $unformattedHpoNames | jq -r '.[].name' )
    readarray -t hpoIds < <(echo $unformattedHpoNames | jq -r '.[].id' )

    outputNameId="["
    for ((i=0;i<${dollar}{#hpoIds[@]};++i))
    do
      outputNameId=$outputNameId'"'${dollar}{hpoNames[i]}' '${dollar}{hpoIds[i]}'",'
    done
    outputNameId=$(echo $outputNameId] | sed 's/\(.*\),/\1/')

   echo '{"hpo_terms": "${hpo_terms}", "genes": "${genes}", "diseases": "${diseases}", "phenotypes_description": "'"$PHENOTYPES_DESCRIPTION"'", "panel_names": "${sep=', ' panel_names}", "phenotypes": '"$outputNameId"'}' > panel_inputs.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File panel = "${sample_id}_panel.json"
    File inputs = "panel_inputs.json"
    File all_panels = "all_panels.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow imputing_vcf_preproc_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "imputing"]}'
    name: 'Vcf preprocessing for imputing'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Prepares vcf file for imputing'
    changes: '{"1.0.2": "vcf filtering made optional, minor fixes in meta"}'

    input_in_file: '{"name": "Vcf file (gzipped)", "type": "File", "extension": [".vcf.gz"], "description": "Vcf input file"}'
    input_axiom_pmda: '{"name": "Axiom pmda", "type": "Boolean", "default": false,"description": "Controls whether to use annotations for the Axiom PMDA genotyping array"}'
    input_processes: '{"name": "Processes", "type": "Int", "default": "8", "description": "Number of processes run in parallel", "required":false}'
    input_filter: '{"name": "Filter", "type": "Boolean", "default": true, "description": "Determines whether vcf is to be filtered or not", "required":false}'

    output_out_files:  '{"name": "Output vcf files", "type": "Array[File]", "copy": "True", "description": "Array of output chromosome-wise vcf files."}'
    output_curated_input_vcf:  '{"name": "Curated input vcf", "type": "File", "copy": "True", "description": "Curated vcf file that is later used by mobigen package."}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call imputing_vcf_preproc

}

task imputing_vcf_preproc {

  String task_name = "imputing_vcf_preproc"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  File in_file
  Boolean axiom_pmda = false
  Int processes = 8
  Boolean filter = true
  String axiom_input_string = if axiom_pmda then "--axiom_input" else ""
  String annot_file = if axiom_pmda then "--annot_file /resources/miscellaneous/array-annotation/array-annotation/Axiom_PMDA.na36.r6.a8.annot.csv" else ""
  String docker_image = if axiom_pmda then "intelliseqngs/task_imputing-vcf-preproc:1.1.2-axiom-pmda" else "intelliseqngs/task_imputing-vcf-preproc:1.1.2"
  String filtering_string = if filter then "--filter" else ""

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/after-start.sh)

  vcf_preprocessing ${in_file} $(pwd) --log_dir $(pwd) --jobs ${processes} --input_compressed ${axiom_input_string} ${annot_file}
  chmod 644 $(pwd)/*.vcf

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: processes
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    Array[File] out_files = glob("{[1-9]*.vcf,X.vcf}")
    File curated_input_vcf = "curated_input_vcf.vcf"

  }

}

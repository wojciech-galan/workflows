workflow bed_to_interval_list_workflow {

  meta {
    keywords: '{"keywords": ["intervals"]}'
    name: 'bed_to_interval_list'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Creates custom intervals from the user provided bed and/or for the specified gene list'
    changes: '{"1.2.0": "Getting nemes of genes with bedtools, only protein coding genes in docker gene bed file", "1.1.0": "Getting a list of genes from the interval_list files", "1.0.1": "separate mt and nuclear intervals"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "Sample identifier"}'
    input_bed_file: '{"name": "Bed file", "extension": [".bed", ".bed.gz"], "type": "File", "description": "Bed with custom regions"}'
    input_gene_list: '{"name": "Gene list", "type": "Selectable", "description": "Gene list", "constraints": {"values": ["http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/inputs/selectable/ensembl-genes/101/ensembl-genes.json"], "mutliselect": true}}'
    input_padding: '{"name": "Padding", "type": "Int", "default": 1000, "description": "Padding (in bp)"}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "default": "grch38-no-alt", "constraints": {"values": ["hg38", "grch38-no-alt"]}, "description": "Reference genome that will be used for alignment"}'

    output_nuclear_interval_file: '{"name": "Autosomal Interval file", "type": "File", "copy": "True", "description": "Custom interval list file (nuclear)"}'
    output_mt_interval_file: '{"name": "MT interval file", "type": "File", "copy": "True", "description": "Custom interval list file (mitochondrial)"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bed_to_interval_list

}

task bed_to_interval_list {

  String task_name = "bed_to_interval_list"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bed-to-interval-list:1.0.2"

  String sample_id = "no_id_provided"
  File? bed
  Array[String]? gene_list
  Int padding = 1000
  String reference_genome = "grch38-no-alt"
  String ref_genome_dir = if (reference_genome == "grch38-no-alt") then "v1" else "v2"
  String ensembl_release = "102"

  Boolean is_bed_provided = defined(bed)
  Boolean is_gene_list_given = defined(gene_list)
  String concat_command = if (is_bed_provided && is_gene_list_given) then "--ACTION CONCAT" else ""
  String gene_list_command = if (is_gene_list_given) then "-I intervals-from-gene-list.interval_list" else ""
  String bed_command = if (is_bed_provided) then "-I intervals-from-bed.interval_list" else ""

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   # bed file is given

   if  ${is_bed_provided}; then
      if echo ${bed} | grep -q '\.gz$' ${bed}; then
          tabix -p bed ${bed}
      fi

      bedtools intersect \
           -a /resources/ensembl-genes/release-${ensembl_release}/ensembl-genes.bed \
            -b ${bed} -wa  | cut -f4 > custom-genes-from-bed.txt

      gatk BedToIntervalList \
         -I ${bed} \
         -O intervals-from-bed.interval_list \
         -SD /resources/reference-genome-${ref_genome_dir}/${reference_genome}/*.dict \
         --UNIQUE

   fi

   # gene list is given

   if ${is_gene_list_given}; then
      for i in ${sep=' ' gene_list}; do
         echo "$i" >> custom-genes.txt
      done

      python3 /intelliseqtools/pick-gene-intervals.py \
         --ensembl-bed /resources/ensembl-genes/release-${ensembl_release}/ensembl-genes.bed \
         --gene-list custom-genes.txt \
         --output custom-genes.bed

       gatk BedToIntervalList \
         -I custom-genes.bed \
         -O intervals-from-gene-list.interval_list \
         -SD /resources/reference-genome-${ref_genome_dir}/${reference_genome}/*.dict \
         --UNIQUE

   fi

   gatk IntervalListTools \
       --PADDING ${padding} \
       ${concat_command} \
       --SORT true \
       --UNIQUE true \
       ${bed_command} \
       ${gene_list_command} \
       -O ${sample_id}_custom-regions.interval_list


   # Split interval file into autosomal and mitochondrial files
   awk '/^@/||$1!="chrM"' ${sample_id}_custom-regions.interval_list > ${sample_id}_custom-regions-nuclear.interval_list
   awk '/^@/||$1=="chrM"' ${sample_id}_custom-regions.interval_list > ${sample_id}_custom-regions-mt.interval_list


   # Check if mt interval list is not empty (to skip mt-varcall in target germline)
   if grep -q '^chrM' ${sample_id}_custom-regions-mt.interval_list; then
       echo true > mt-regions.txt
   else
       echo false > mt-regions.txt
   fi


   # Get genes from given gene-list files
   if [ -s custom-genes.txt ] || [ -s custom-genes-from-bed.txt ];then
       cat custom-genes*.txt | sort | uniq > final-gene-list.txt
       while read f; do echo $f | sed 's/^/name=/' | jo >> tmp.txt; done<final-gene-list.txt
       cat tmp.txt | jo -a > genes_bed_target_json.json
   else
       echo "[]" > genes_bed_target_json.json
   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File nuclear_interval_file = "${sample_id}_custom-regions-nuclear.interval_list"
    File mt_interval_file = "${sample_id}_custom-regions-mt.interval_list"
    File genes_bed_target_json = "genes_bed_target_json.json"
    Boolean non_empty_mt_intervals = read_boolean("mt-regions.txt")
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

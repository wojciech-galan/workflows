workflow vcf_prepare_workflow {

  meta {
    keywords: '{"keywords": ["prepare vcf"]}'
    name: 'vcf_prepare'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.0.2": "pipefail restored, tbi is not required, input vcf may be uncompressed, removal of lines without any alt allele", "1.0.1": "pipefail set in the middle of the task", "1.0.0": "no changes"}'

    input_vcf: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz", ".vcf"], "description": "Vcf file to be annotated"}'
    input_tbi: '{"name": "Input vcf tabix index", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'

    output_prepared_vcf_gz: '{"name": "Prepared vcf", "type": "File", "copy": true, "description": "Vcf prepared"}'
    output_prepared_vcf_gz_tbi: '{"name": "Prepared vcf tabix index", "type": "File", "copy": true, "description": "Index for vcf prepared"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_prepare

}

task vcf_prepare {

  File vcf
  File? tbi
  Boolean defined_tbi = if defined(tbi) then true else false
  String map_file_path = "/resources/assembly_report/GRCh38.p13/map-file.tsv"
  String vcf_basename = "no_id_provided"

  String task_name = "vcf_prepare"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-prepare:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}
   ## check if vcf file is bgzipped, if so check  for index and create one if not present

   if [[ ${vcf} = *.vcf.gz ]]  && ! ${defined_tbi};then
       mv ${vcf} ${vcf_basename}_input.vcf.gz
       tabix -p vcf ${vcf_basename}_input.vcf.gz
   elif [[ ${vcf} = *.vcf ]];then
       bgzip ${vcf} -c > ${vcf_basename}_input.vcf.gz
       tabix -p vcf ${vcf_basename}_input.vcf.gz
   else
       mv ${vcf} ${vcf_basename}_input.vcf.gz
       mv ${tbi} ${vcf_basename}_input.vcf.gz.tbi
   fi


   ## split multiallelic sites, rename chromosomes and remove lines with all ref/ref genotypes
   bcftools norm -m -any ${vcf_basename}_input.vcf.gz \
   | bcftools view --min-ac=1 \
   | bcftools annotate --rename-chrs ${map_file_path}  -o ${vcf_basename}_renamed-chrs.vcf.gz -O z
   tabix -p vcf ${vcf_basename}_renamed-chrs.vcf.gz

   ## remove ISEQ annotations
   annot_to_remove=$( tabix -H ${vcf_basename}_renamed-chrs.vcf.gz | grep -o 'ISEQ_[A-Za-z0-9_]*,\|ANN,\|NMD,\|CSQ,\|LOF,\|MITOMAP_[A-Za-z0-9_]*,\|Phylo[A-Za-z0-9_]*,\|Phast[A-Za-z0-9_]*,\|dbNSFP_[A-Za-z0-9_]*,' | sed 's/^/INFO\//' | tr '\n' ' ' | sed 's/ //g' | sed 's/,$//'  || [[ $? == 1 ]] )
   if [ -z $annot_to_remove ];then
       mv ${vcf_basename}_renamed-chrs.vcf.gz ${vcf_basename}_prepared.vcf.gz
       mv ${vcf_basename}_renamed-chrs.vcf.gz.tbi ${vcf_basename}_prepared.vcf.gz.tbi
   else
       bcftools annotate -x $annot_to_remove ${vcf_basename}_renamed-chrs.vcf.gz \
       | grep -v '##SnpEff' \
       | bgzip > ${vcf_basename}_prepared.vcf.gz; tabix -p vcf ${vcf_basename}_prepared.vcf.gz
   fi


   ## validate vcf file
   gatk ValidateVariants -V ${vcf_basename}_prepared.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {
    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2
  }

  output {
    File prepared_vcf_gz = "${vcf_basename}_prepared.vcf.gz"
    File prepared_vcf_gz_tbi = "${vcf_basename}_prepared.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
  }
}
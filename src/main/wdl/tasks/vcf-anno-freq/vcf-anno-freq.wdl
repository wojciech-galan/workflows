workflow vcf_anno_freq_workflow {

  meta {
    keywords: '{"keywords": ["annotation", "frequencies"]}'
    name: 'vcf_anno_freq'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## annotate vcf with frequencies \n  Annotates VCF restricted to one of the following regions: chr1, chr2, (...), chr22, chrX and chrY-and-the-rest with allele frequency information from the following databases:  \n    - 1000 Genomes \n    - ESC6500 \n    - ExAC \n    - gnomAD exomes \n    - gnomAD genomes or gnomAD genomes restricted to exome calling interval \n    - Mitomap \n  IMPORTANT NOTES: \n    1. VCF must be resticted to the one of the following regions chr1, chr2, (...), chr22, chrX or chrY-and-the-rest \n    2. The input bgzipped VCF must have: normalized indels (bcftools norm), split multiallelic sites (bcftools norm --multiallelics -any) \n  If these requirments are not met, some variants may not be annotated correctly. Normalization and multiallelic sites splitting can be done with the use of normalize-indels-mark-and-split-multiallelic-sites-in-vcf task. \n    3. The task outputs bgzipped VCF with multiallelic sites split. It is recommened to normalize indels and collapse multiallelic sites with normalize-and-collapse-multiallelic-sites-in-vcf task on the final annotated and/or filtered VCF file.'
    changes: '{"2.1.0": "set -eo pipefail, latest removed"}'

    input_vcf_basename: '{"name": "vcf basename", "type": "String", "description": "Sample ID."}'
    input_genome_or_exome: '{"name": "genome or exome", "type": "String", "default": "genome", "constraints": {"values": ["exome", "genome"]}, "description": " "}'
    input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]}, "description": ""}'
    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file"}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'

    output_annotated_with_frequencies_vcf_gz : '{"name": "annoted with frequencies vcf files", "type": "Array[File]", "description": ""}'
    output_annotated_with_frequencies_vcf_gz_tbi : '{"name": "annoted with frequencies vcf files indexes", "type": "Array[File]", "description": ""}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_anno_freq

}

task vcf_anno_freq {
  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename
  String chromosome

  String genome_or_exome = "genome"
  String frequency_database_gz = "/resources/frequencies/merged-frequencies-" + genome_or_exome + "/3.0.0/" + chromosome + ".frequencies.vcf.gz"


  String task_name = "vcf_anno_freq"
  String task_version = "2.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-freq-" + genome_or_exome + ":3.0.0-" + chromosome

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  vcf_dir=$(dirname "${vcf_gz}")
  tbi_name=$(basename "${vcf_gz_tbi}")
  if [ ! -f $vcf_dir/$tbi_name ]; then
    ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
  fi

  bcftools annotate --columns INFO --annotation ${frequency_database_gz} ${vcf_gz} -o ${chromosome}-${vcf_basename}_anno-freq.vcf.gz -O z
  tabix -p vcf ${chromosome}-${vcf_basename}_anno-freq.vcf.gz


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {


    docker: docker_image
    memory: "4G"
    cpu: "2"
    maxRetries: 2

  }

  output {
    File annotated_with_frequencies_vcf_gz = "${chromosome}-${vcf_basename}_anno-freq.vcf.gz"
    File annotated_with_frequencies_vcf_gz_tbi = "${chromosome}-${vcf_basename}_anno-freq.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

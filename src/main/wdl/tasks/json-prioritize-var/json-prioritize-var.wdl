workflow json_prioritize_var_workflow {

  meta {
    keywords: '{"keywords": ["variant selection", "report"]}'
    name: 'json_prioritize_var'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Prioritizes variants, creates variant position list.'
    changes: '{"1.0.0": "no changes"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "Sample identifier"}'
    input_var_json: '{"name": "Variant json", "type": "File", "extension":[".json"], "description": "ACMG variant json file, created by vcf-to-json-or-tsv task"}'
    input_var_num: '{"name": "Variant number", "type": "Int", "description": "Maximal number of displayed variants"}'

    output_selected_var_json: '{"name": "Selected variant json", "type": "File", "description": "Json file with variants that will be shown in report"}'
    output_position_list: '{"name": "Position list", "type": "File", "description": "List of positions of the selected variants (for IGV screenshots)"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call json_prioritize_var

}

task json_prioritize_var {

  String task_name = "json_prioritize_var"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_json-prioritize-var:1.0.0"

  String sample_id = "no_id_provided"
  File var_json
  Int var_num = 20

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   python3 /intelliseqtools/select-variants-from-json.py \
    --input-json ${var_json} \
    --output-json ${sample_id}_variants-to-report.json \
    --variant-number ${var_num}


    jq '.[] | .CHROM , .POS , .ISEQ_GENES_NAMES[0]' ${sample_id}_variants-to-report.json \
    | sed 's/\"//g' | paste -d ' ' - - - >> ${sample_id}_list-of-positions.txt


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File selected_var_json = "${sample_id}_variants-to-report.json"
    File position_list = "${sample_id}_list-of-positions.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

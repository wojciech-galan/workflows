workflow resource_clinvar_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'resource_clinvar'
    author: 'https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Task providing vcf from clinvar database, annotated with submission-summary.txt (clinical sgnificance based on clinical data) '
    changes: '{"1.0.3": "Remove latest directory for this task", "1.0.1": "Update clinvar data."}'
    output_clinvar_vcf_gz: '{"name": "clinvar_vcf_gz", "type": "File", "description": "vcf from clinvar database, annotated with submission-summary.txt"}'
    output_clinvar_vcf_gz_tbi: '{"name": "clinvar_vcf_gz_tbi", "type": "File"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call resource_clinvar

}

task resource_clinvar {

  String task_name = "resource_clinvar"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/resource-clinvar:0.1.3"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  cp /resources/clinvar-compare-acmg/*/* .
  

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File clinvar_vcf_gz = "clinvar-annotation-file.vcf.gz"
    File clinvar_vcf_gz_tbi = "clinvar-annotation-file.vcf.gz.tbi"
    File clinvar_bed_gz = "clinvar-annotation-file.bed.gz"
    File clinvar_bed_gz_tbi = "clinvar-annotation-file.bed.gz.tbi"

  }

}

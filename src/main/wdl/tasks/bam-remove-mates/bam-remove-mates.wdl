workflow bam_remove_mates_workflow {

  meta {
    keywords: '{"keywords": ["bam", "filtering by read name"]}'
    name: 'Bam remove mates'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Removes from the bam file mates of reads filtered out due to high number of mismatches. May be also used for filtering any bam by read name.'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_read_lists: '{"name": "Reads list", "type": "Array[File]", "description": "Files with names of reads to be removed from the bam file"}'
    input_bam: '{"name": "Bam file", "type": "File", "description": "Input bam"}'

    output_filtered_bam : '{"name": "Filtered bam", "type": "File", "copy": "True", "description": "Bam file without reads pairs with multiple mismatches"}'
    output_filtered_bai : '{"name": "Filtered bam index", "type": "File", "copy": "True", "description": "Index for the filtered bam"}'
    output_removed_reads: '{"name": "Removed reads", "type": "File", "copy": "True", "description": "File with names of the removed pairs of reads"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_remove_mates

}

task bam_remove_mates {

  String task_name = "bam_remove_mates"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0:1.0.0"

  String sample_id = "no-id"
  File bam
  Array[File] read_lists
  String java_opt = "-Xms2000m"
  String gatk_jar = "/gatk/gatk-package-4.1.7.0-local.jar"

  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  set -e -o pipefail

  cat ${sep=' ' read_lists } > ${sample_id}_removed-reads.txt

  ## removing mates of the filtered out reads
  if grep -q '^' ${sample_id}_removed-reads.txt
  then
       java ${java_opt} -jar ${gatk_jar} FilterSamReads \
            -I ${bam} \
            -O ${sample_id}_filtered.bam \
            --FILTER excludeReadList \
            --READ_LIST_FILE ${sample_id}_removed-reads.txt
  else
       mv ${bam} ${sample_id}_filtered.bam
  fi

  java ${java_opt} -jar ${gatk_jar} BuildBamIndex -I ${sample_id}_filtered.bam


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File filtered_bam = "${sample_id}_filtered.bam"
    File filtered_bai = "${sample_id}_filtered.bai"
    File removed_reads = "${sample_id}_removed-reads.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

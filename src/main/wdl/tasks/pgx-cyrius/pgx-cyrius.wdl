workflow pgx_cyrius_workflow {

  meta {
    keywords: '{"keywords": ["CYP2D6", "pharmacogenomics", "genotype"]}'
    name: 'pgx-cyrius'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.0.1": "memory from 1G to 2G"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_bam: '{"name": "bam", "type": "File", "extension": [".bam"], "description": "Bam file, markdup.recalibrated or filtered.", "required": "true"}'
    input_bai: '{"name": "bai", "type": "File", "extension": [".bai"], "description": "Index for bam file", "required": "true"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call pgx_cyrius

}

task pgx_cyrius {

  File bam
  File bai
  String sample_id = "no-id-provided"

  String task_name = "pgx_cyrius"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_pgx-cyrius:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   readlink -f ${bam}> manifest

   python /tools/Cyrius/1.1/star_caller.py -m manifest -g 38 -p ${sample_id}-cyrius -o .

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File cyrius_json = "${sample_id}-cyrius.json"
    File cyrius_tsv = "${sample_id}-cyrius.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

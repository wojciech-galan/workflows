workflow bam_concat_workflow {

  meta {
    keywords: '{"keywords": ["bam", "merge", "sort"]}'
    name: 'Concatenate bams'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Concatenate bam files, optionally also sorts the merged output file'
    changes: '{"1.1.1": "added validation level choice, and merge seq dict option (for MergeSamFiles tool only)"}'

    input_interval_bams : '{"name": "Bam files", "type": "Array", "description": "Interval bam files"}'
    input_sample_id : '{"name": "Sample ID", "type": "String", "description": "Output bam file basename"}'
    input_bam_name : '{"name": "String added to output bam name", "type": "String", "description": "This string will be added to the output file name (after sample id, before extension)"}'
    input_need_sort : '{"name": "Need sort?", "type": "String", "description": "This option defines whether to sort the output merged bam file. Possible values: no, yes."}'
    input_just_gather: '{"name": "Just gather?", "type": "Boolean", "description": "This option defines whether to use gathering (aka concatenating) or merging"}'    
    input_merge_seq_dict: '{"name": "Merge sequence dictionaries?", "type": "Boolean", "default": false, "description": "This option defines whether merge sequence dictionaries (not compatible with just_father option)"}'
    input_validation_level: '{"name": "Validation level", "type": "String", "default": "STRICT", "constraints": {"choices": ["STRICT", "LENIENT", "SILENT"]}, "description": "This option defines bam validation level"}'

    output_bam: '{"name": "Concatenated  bam", "type": "File", "copy": "True", "description": "Concatenated and coordinate sorted bam file"}'
    output_bai: '{"name": "Bai file", "type": "File", "copy": "True", "description": "Index file for the concatenated bam "}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bam_concat

}

task bam_concat {

  String task_name = "bam_concat"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0:1.0.0"

  Array[File] interval_bams
  Int num_files = length(interval_bams)
  String sample_id = "sample_id"

  String bam_name = "merged"
  String need_sort = "no"
  String gather_bam_output = if need_sort == "no" then "${sample_id}_${bam_name}.bam" else "${sample_id}_unsorted.bam"

  Int compression_level = 5
  String java_opt = "-Xms4000m"
  String gatk_jar = "/gatk/gatk-package-4.1.7.0-local.jar"
  Boolean just_gather = false
  String gatk_tool = if (just_gather) then "GatherBamFiles" else "MergeSamFiles"
  Boolean merge_seq_dict = false
  String merge_seq_command = if(merge_seq_dict && !just_gather) then "--MERGE_SEQUENCE_DICTIONARIES true" else ""
  String validation_level = "STRICT" # other options "LENIENT", "SILENT"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # Do not merge if there is only 1 file
  if [[ ${num_files} == 1 ]]
  then
      cp ${sep=" " interval_bams} ${gather_bam_output}

  else
      java -Dsamjdk.compression_level=${compression_level} ${java_opt} -jar ${gatk_jar} \
          ${gatk_tool} \
          -I ${sep=' -I ' interval_bams} \
          -O ${gather_bam_output} \
          --VALIDATION_STRINGENCY ${validation_level} \
          ${merge_seq_command}
  fi


  # Sort only if necessary

  if [ "${need_sort}" != "no" ]
  then
      java -Dsamjdk.compression_level=${compression_level} ${java_opt} -jar ${gatk_jar} \
          SortSam \
          -I ${gather_bam_output} \
          -O ${sample_id}_${bam_name}.bam \
          --SORT_ORDER coordinate \
          --VALIDATION_STRINGENCY ${validation_level}
  fi

  java -Dsamjdk.compression_level=${compression_level} ${java_opt} -jar ${gatk_jar} \
      BuildBamIndex \
      -I ${sample_id}_${bam_name}.bam \
      --VALIDATION_STRINGENCY ${validation_level}

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}

  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File bam = "${sample_id}_${bam_name}.bam"
    File bai = "${sample_id}_${bam_name}.bai"
  }

}

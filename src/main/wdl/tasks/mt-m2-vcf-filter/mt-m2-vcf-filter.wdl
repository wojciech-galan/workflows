workflow mt_m2_vcf_filter_workflow {

  meta {
    keywords: '{"keywords": ["mitochondrion", "variant filtering"]}'
    name: 'Mutect2 vcf filter (in mitochondrial mode)'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Applies mitochondrial variants filtering'
    changes: '{"1.0.1": "min alt reads filter added"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_m2_raw_vcf: '{"name": "Mutect2 raw vcf", "type": "File", "extension": [".vcf.gz"], "description": "Raw mutect2 mitochondrial vcf"}'
    input_m2_raw_vcf_tbi: '{"name": "Mutect2 raw vcf tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Index for the raw m2 mitochondrial vcf"}'
    input_mutect2_stats: '{"name": "Mutect2 stats", "type": "File", "extension": [".txt", ".stats"], "description": "File with mutect2 variant calling metrics"}'
    input_mt_target_intervals: '{"name": "MT target intervals", "type": "File", "extension": [".interval_list"], "description": "Custom mitochondrial intervals"}'
    input_genome_or_exome: '{"name": "Genome or exome", "type": "String", "constraints": {"choices": ["genome", "exome", "target"]},"default": "genome", "description": "Sample identifier"}'
    input_max_alt_allele_count: '{"name": "Max alt allele count", "type": "Int", "default": 4, "description": "Maximal number of alleles per site"}'
    input_contamination: '{"name": "Contamination", "type": "Float", "default": 0.0, "description": "Contamination estimate (from haplochecker)"}'
    input_vaf_filter_threshold: '{"name": "VAF filter threshold", "type": "Float", "default": 0.05, "description": "Minimal frequency of the outputed mitochondrial variant"}'
    input_f_score_beta: '{"name": "F score beta", "type": "Float", "default": 1.0, "description": "The relative weight of recall to precision"}'
    input_min_alt_reads: '{"name": "Minimum alt reads", "type": "Int", "default": 2, "description": "Minimum unique (i.e. deduplicated) reads supporting the alternate allele"}'

    output_filtered_mt_vcf_gz: '{"name": "Filtered MT vcf gz", "type": "File", "copy": "True", "description": "Filtered mitochondrial vcf"}'
    output_filtered_mt_vcf_gz_tbi: '{"name": "Filtered MT vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the filtered mitochondrial vcf"}'
    output_rejected_mt_vcf_gz: '{"name": "Rejected MT vcf gz", "type": "File", "copy": "True", "description": "Vcf with mitochondrial variants that did not pass applied filters"}'
    output_rejected_mt_vcf_gz_tbi: '{"name": "Rejected MT vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the rejected mitochondrial vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call mt_m2_vcf_filter

}

task mt_m2_vcf_filter {

  String task_name = "mt_m2_vcf_filter"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_mt-varcall-mutect2:1.0.0"

  String java_options = "-Xmx2500m"
  String broad_resources_version = "v0"
  String reference_path = "/resources/broad-institute-hg38-mt-reference/"+ broad_resources_version

  String sample_id = "no_id_provided"
  File m2_raw_vcf
  File m2_raw_vcf_tbi
  File mutect2_stats
  File? mt_target_intervals
  String genome_or_exome = "genome"

  Int max_alt_allele_count = 4
  Float contamination = 0.0
  Float autosomal_coverage = if (genome_or_exome == "genome") then 30 else 0
  Float vaf_filter_threshold = 0.05
  Float f_score_beta = 1.0
  Int min_alt_reads = 2


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk --java-options "${java_options}" \
        FilterMutectCalls \
        -V ${m2_raw_vcf} \
        -R "${reference_path}/Homo_sapiens_assembly38.chrM.fasta" \
        -O filtered1.vcf \
        --stats ${mutect2_stats} \
        --max-alt-allele-count ${max_alt_allele_count} \
        --mitochondria-mode \
        --min-allele-fraction ${vaf_filter_threshold} \
        --f-score-beta ${f_score_beta} \
        --unique-alt-read-count ${min_alt_reads} \
        --contamination-estimate ${contamination} \
        ${"-L " + mt_target_intervals}

        ##--autosomal-coverage ${autosomal_coverage}


   gatk VariantFiltration \
        -V filtered1.vcf \
        -O filtered2.vcf \
        --mask ${reference_path}/blacklist_sites.hg38.chrM.bed \
        --mask-name "blacklisted_site"


   # Hard filter and correct invalid info description
    awk '/^#/||$7=="PASS"' filtered2.vcf | sed 's/INFO=<ID=AS_SB_TABLE,Number=1/INFO=<ID=AS_SB_TABLE,Number=./' | bgzip > ${sample_id}_mt-filtered.vcf.gz
    awk '/^#/||$7!="PASS"' filtered2.vcf|  sed 's/INFO=<ID=AS_SB_TABLE,Number=1/INFO=<ID=AS_SB_TABLE,Number=./' | bgzip  > ${sample_id}_mt-rejected.vcf.gz
    tabix -p vcf ${sample_id}_mt-filtered.vcf.gz
    tabix -p vcf ${sample_id}_mt-rejected.vcf.gz



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "3.5G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File filtered_mt_vcf_gz = "${sample_id}_mt-filtered.vcf.gz"
    File filtered_mt_vcf_gz_tbi = "${sample_id}_mt-filtered.vcf.gz.tbi"

    File rejected_mt_vcf_gz = "${sample_id}_mt-rejected.vcf.gz"
    File rejected_mt_vcf_gz_tbi = "${sample_id}_mt-rejected.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow report_acmg_workflow {

  meta {
    keywords: '{"keywords": []}'
    name: 'Report variants from VCF'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## Creates ACMG report'
    changes: '{"1.2.4": "correction in counting time difference between zones", "1.2.3": "version update"}'

    input_variants_pathogenic: '{"name": "variants_pathogenic", "type": "File", "extension": [".json"], "description": "Json file with pathogenic variants"}'
    input_variants_likely_pathogenic: '{"name": "variants_likely_pathogenic", "type": "File", "extension": [".json"], "description": "Json file with likely pathogenic variants"}'
    input_variants_uncertain: '{"name": "variants_uncertain", "type": "File", "extension": [".json"], "description": "Json file with uncertain variants"}'
    input_sample_info_json: '{"name": "sample_info_json", "type": "File", "extension": [".json"], "description": "Patient and sample data in json format, must include attributes (even if without values): name, surname, sex, birthdate, pesel, ID, material, sequencing_type, sequencing_platform, sending_date, raport_date, doctor_name, doctor_surname", "required": "False"}'
    input_panel_json: '{"name": "panel_json", "type": "File", "extension": [".json"], "description": "Panel data in json format, must include attributes (even if without values): genes_number, genes", "required": "False"}'
    input_pictures_gz: '{"name": "pictures_gz", "type": "File", "description": "Pictures gz", "advanced":"true"}'
    input_language: '{"name": "language", "type": "String", "description": "Report language", "advanced":"true"}'

    output_variants_report_pdf: '{"name": "variants_report_pdf", "type": "File", "copy": "True", "description": "Report of pathogenic, likely pathogenic and uncertain variants in pdf file"}'
    output_variants_report_odt: '{"name": "variants_report_odt", "type": "File", "copy": "True", "description": "Report of pathogenic, likely pathogenic and uncertain variants in odt file"}'
    output_variants_report_docx: '{"name": "variants_report_docx", "type": "File", "copy": "True", "description": "Report of pathogenic, likely pathogenic and uncertain variants in docx file"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call report_acmg

}

task report_acmg {
  File variants_pathogenic
  File variants_likely_pathogenic
  File variants_uncertain

  File? sample_info_json
  File? panel_json
  File? phenotypes_json
  File? pictures_gz
  Int timezoneDifference = 0
  String? analysisSpecifier
  Float file_size =  size(pictures_gz, "M")
  String output_name = "report-variants-from-vcf"
  String language = "pl"
  String sample_id = "no_id_provided"
  String genome_or_exome = "exome"


  String task_name = "report_acmg"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/reports:3.5.5"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  # Create empty json files as input to generate-report.sh if were not provided in wdl inputs.
  [ -z "${panel_json}" ] && echo "{}" > panel.json || cat ${panel_json} > panel.json
  [ -z "${sample_info_json}" ] && echo "{}" > sample_info.json || cat ${sample_info_json} > sample_info.json
  [ -z "${phenotypes_json}" ] && echo "{}" > phenotypes.json || cat ${phenotypes_json} > phenotypes.json

  LENGTH_DATE=$(jq '.raport_date | length' sample_info.json)
  if [[ "$LENGTH_DATE" == 0 ]]
  then
      CURRENT_TIME=$(echo $(($(date +%s%N)/1000000)))
      DIFFERENCE=$(((CURRENT_TIME+(${timezoneDifference}))/1000))
      DATE=$(date -d @$DIFFERENCE +"%Y-%m-%d, %H:%M")
      jq -r --arg RAPORT_DATE "$DATE" '.raport_date = $RAPORT_DATE' sample_info.json > sample_info.json.tmp && cat sample_info.json.tmp > sample_info.json
  fi

  LENGTH_ID=$(jq '.ID | length' sample_info.json)
  if ([ ! -z "${analysisSpecifier}" ] && [[ "$LENGTH_ID" == 0 ]])
  then
      jq -r --arg ID "${analysisSpecifier}" '.ID = $ID' sample_info.json > sample_info.json.tmp && cat sample_info.json.tmp > sample_info.json
  fi

  # Prepare picture.json with paths to png files
  mkdir pictures
  if [ -z "${pictures_gz}" ] ; then
    echo '{}' > picture.json
    echo '[]' > picture-generate-report-sh.json
  else
    tar xvzf ${pictures_gz} -C pictures

      touch picture.txt
      echo "[" > picture-generate-report-sh.json
      for png in pictures/*.png
      do
        # Create png_name extracting chr and pos from png file name (png_name="chr15-48497301")
        png_name=$(echo $png | sed 's/.*\(chr.*-[0-9]*\)-igv-screenshot.png/\1/');
        # Get the path to png file
        png_path=$(realpath $png);
        png_basename=$(basename $png);
        # Save png_name and path to picture.txt ({"chr15-48497301":"/home/test/test/Pictures/test_chr15-48497301-igv-screenshot.png"})
        echo \"$png_name\":\"$png_basename\" >> picture.txt
        echo "{\"path\" : \""$png_path"\"},">> picture-generate-report-sh.json
      done

      # Replace new lines with commas, add {}
      cat picture.txt | tr "\n" "," | sed 's/^"/{"/' | sed 's/",$/"}/' > picture.json

      sed -i '$ s/.$//' picture-generate-report-sh.json
      echo "]" >> picture-generate-report-sh.json
  fi

  jq -c '[.[] | select((."type"=="phenotype") or (."type"=="user"))]' panel.json > panel_phenotypes.json
  jq -c '[.[] | select((."type"!="phenotype") and (."type"!="user"))]' panel.json > panel_diseases.json

  bash /intelliseqtools/generate-report.sh \
  --json patient=sample_info.json \
  --json phenotypes=phenotypes.json \
  --json panel_phenotypes=panel_phenotypes.json \
  --json panel_diseases=panel_diseases.json \
  --json sample=sample_info.json \
  --json variants_uncertain=${variants_uncertain} \
  --json variants_pathogenic=${variants_pathogenic} \
  --json variants_likely_pathogenic=${variants_likely_pathogenic} \
  --json dict=/intelliseqtools/templates/report-${language}-v1/dict.json \
  --json seq_type=/intelliseqtools/templates/report-${language}-v1/${genome_or_exome}-${language}.json \
  --json picture=picture.json \
  --template /intelliseqtools/templates/report-${language}-v1/content.jinja \
  --name ${sample_id}_${language}-${output_name} \
  --pictures picture-generate-report-sh.json

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: if (file_size > 20) then "32G" else "8G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File variants_report_pdf = "${sample_id}_${language}-${output_name}.pdf"
    File variants_report_odt = "${sample_id}_${language}-${output_name}.odt"
    File variants_report_docx = "${sample_id}_${language}-${output_name}.docx"
    File variants_report_html = "${sample_id}_${language}-${output_name}.html"


    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

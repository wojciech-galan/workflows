workflow report_acmg_workflow
{
  call report_acmg
}

task report_acmg {

  File var_json
  File? sample_info_json
  File? pictures_gz
  File? panel_json
  File? panel_inputs_json
  File? genes_bed_target_json

  Int timezoneDifference = 0
  String? analysisSpecifier

  String output_name = "report-variants-from-vcf"
  Array[String] languages = ["ang"] #for now only "ang" is available
  String sample_id = "no_id_provided"
  String genome_or_exome = "exome"
  String analysis_start = "fastq"
  String analysis_group = "germline" # possible: "carrier_screening" and "germline"
  String outputs = "outputs"
  String template = if analysis_group == "germline" then "acmg" else "carrier-screening"

  String task_name = "report_acmg"
  String task_version = "2.2.11"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_acmg:1.1.14"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ${outputs}

    # Prepare sample-info.json
    bash /intelliseqtools/prepare-sample-info-json.sh --sample-info-json "${sample_info_json}" \
                                                      --timezoneDifference ${timezoneDifference} \
                                                      --sampleID "${sample_id}"

    # Prepare picture.json with paths to png files
    mkdir pictures
    if [ -z "${pictures_gz}" ] ; then
        echo '[]' > picture-generate-report-sh.json
    else
        tar xvzf ${pictures_gz} -C pictures

        echo "[" > picture-generate-report-sh.json
        for png in pictures/*.png
        do
            # Create png_name extracting chr and pos from png file name (png_name="chr15-48497301")
            png_name=$(echo $png | sed 's/.*\(chr.*-[0-9]*\).*-igv-screenshot.png/\1/');
            # Get the path to png file
            png_path=$(realpath $png);
            # Save png_name and path to picture-generate-report-sh.json ({"chr15-48497301":"/home/test/test/Pictures/test_chr15-48497301-igv-screenshot.png"})
            echo "{\"$png_name\" : \""$png_path"\"},">> picture-generate-report-sh.json
        done

        sed -i '$ s/.$//' picture-generate-report-sh.json
        echo "]" >> picture-generate-report-sh.json
    fi

    # create empty files for report
    if [ -z "${panel_json}" ] ; then
        echo '[]' > panel.json
    else
        cat ${panel_json} > panel.json
    fi

    if [ -z "${panel_inputs_json}" ] ; then
        echo '[]' > panel_inputs.json
    else
        cat ${panel_inputs_json} > panel_inputs.json
    fi

    if [ -z "${genes_bed_target_json}" ] ; then
        echo '[]' > genes_bed_target.json
    else
        cat ${genes_bed_target_json} > genes_bed_target.json
    fi

    if [[ "${analysis_group}" == "germline" ]]; then
      python3 /intelliseqtools/split-variants-by-acmg-classification.py --input ${var_json} \
                                                                        --output-benign benign.json \
                                                                        --output-likely-benign likely-benign.json \
                                                                        --output-uncertain uncertain.json \
                                                                        --output-likely-pathogenic likely-pathogenic.json \
                                                                        --output-pathogenic pathogenic.json \
                                                                        --output-undefined undefined.json

      vcf_jsons=$(echo "pathogenic.json likely-pathogenic.json uncertain.json likely-benign.json benign.json undefined.json")
    fi
    if [[ "${analysis_group}" == "carrier_screening" ]]; then
          python3 /intelliseqtools/split_variants_by_clinvar_classification.py --input ${var_json} \
                                                                               --output-pathogenic pathogenic.json \
                                                                               --output-likely-pathogenic likely-pathogenic.json \
                                                                               --output-others others.json
      vcf_jsons=$(echo "pathogenic.json likely-pathogenic.json others.json")
    fi

    for language in ${sep=' ' languages}
    do
        python3 /intelliseqtools/reports/templates/script/report-acmg.py    --template "/resources/report-${template}-template.docx" \
                                                                            --vcf-jsons $vcf_jsons \
                                                                            --other-jsons "sample_info.json" \
                                                                                          "panel.json" \
                                                                                          "panel_inputs.json" \
                                                                                          "genes_bed_target.json" \
                                                                                          "picture-generate-report-sh.json" \
                                                                                          "/resources/content.json" \
                                                                                          "/resources/images/images-width.json" \
                                                                            --analysis-type ${genome_or_exome} \
                                                                            --analysis-start ${analysis_start} \
                                                                            --analysis-group ${analysis_group} \
                                                                            --output-filename "${outputs}/${sample_id}_$language-${output_name}.docx"

        soffice --headless \
          --convert-to pdf ${outputs}/${sample_id}_$language-${output_name}.docx \
          --outdir ${outputs}
    done

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File variants_report_ang_pdf = "${outputs}/${sample_id}_ang-${output_name}.pdf"
    File variants_report_ang_docx = "${outputs}/${sample_id}_ang-${output_name}.docx"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow graph_generator_workflow
{
  call graph_generator
}

task graph_generator {

  String wdl_path
  String wdl_path_basename = basename(wdl_path, ".wdl")

  String task_name = "graph_generator"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_graph-generator:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      ### wget wdl
      wget -O ${wdl_path_basename}.wdl ${wdl_path}

      ### create .dot file using womtool
      java -jar /resources/womtool-58.jar graph ${wdl_path_basename}.wdl > ${wdl_path_basename}.dot

      ### create .png and .svg files using graphviz
      dot -Tpng ${wdl_path_basename}.dot -o ${wdl_path_basename}.png
      dot -Tsvg ${wdl_path_basename}.dot -o ${wdl_path_basename}.svg

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File wdl_graph_png = "${wdl_path_basename}.png"
    File wdl_graph_svg = "${wdl_path_basename}.svg"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

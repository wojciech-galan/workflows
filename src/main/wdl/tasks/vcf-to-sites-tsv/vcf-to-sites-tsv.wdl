workflow vcf_to_sites_tsv_workflow
{
  call vcf_to_sites_tsv
}

task vcf_to_sites_tsv {

  String task_name = "vcf_to_sites_tsv"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.5"

  File vcf_gz
  File vcf_gz_tbi

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   bcftools query -f '%CHROM\t%POS\t%REF\t%ALT\n' ${vcf_gz} | uniq | bgzip -c > gtc-sites.tsv.gz

   tabix -s1 -b2 -e2 gtc-sites.tsv.gz



   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: 4
    maxRetries: 2

  }

  output {

    File tsv_file = "gtc-sites.tsv.gz"
    File tbi_file = "gtc-sites.tsv.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

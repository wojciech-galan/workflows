workflow sv_lumpy_workflow {

  meta {
    keywords: '{"keywords": ["sv", "lumpy", "wgs"]}'
    name: 'sv_lumpy'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Structural variants calling with lumpy'
    changes: '{"1.0.1": "32G memory"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "Sample identifier"}'
    input_bam: '{"name": "Bam", "type": "File", "description": "Coordinate sorted input bam"}'
    input_splitters_bam: '{"name": "Splitters bam", "type": "File", "description": "Coordinate sorted input bam with splitter reads"}'
    input_discordants_bam: '{"name": "Discordants bam", "type": "File", "description": "Coordinate sorted input bam with discordant reads"}'
    input_exclude_lcr: '{"name": "Exclude LCR", "type": "Boolean", "description": "This option decides whether exclude low complexity regions from analysis"}'
    input_lcr_bed_source: '{"name": "LCR bed", "type": "String", "constraints": {"values": ["hall-lab","btu356"]},"description": "This option decides which low complexity regions bam should be used"}'

    output_lumpy_vcf_gz: '{"name": "Lumpy vcf", "type": "File", "copy": "True", "description": "Lumpy output vcf file"}'
    output_lumpy_vcf_gz_tbi: '{"name": "Lumpy vcf tbi", "type": "File", "copy": "True", "description": "Index for lumpy output vcf file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_lumpy

}

task sv_lumpy {

  String task_name = "sv_lumpy"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/lumpy:1.0.0"
  File bam
  File? splitters_bam
  File? discordants_bam
  String sample_id = "no-id"

  # This decided whether exclude low complexity regions from analysis
  Boolean exclude_lcr = false

  # This decides which source of excluded regions should be used:
  # "hall-lab" (https://github.com/hall-lab/speedseq/raw/master/annotations/exclude.cnvnator_100bp.GRCh38.20170403.bed) or
  # "btu356" (https://doi.org/10.1093/bioinformatics/btu356, Supplementary materials)
  String lcr_bed_source = "hall-lab"

  String lcr_bed_source_dir = if lcr_bed_source == "hall-lab" then "hall-lab-hg38-03-04-2017" else "hg38-btu356"
  String LCR_bed = if (exclude_lcr) then "-x /resources/low-complexity-regions/" + lcr_bed_source_dir + "/LCR.bed" else ""

  # This allows for bypassing samblaster and using input bam files with splitted and discordnats reads instead
  # samblaster is not applied only if both files (splitters_bam and discordants_bam) are defined
  String use_other_bams = if defined(splitters_bam) && defined(discordants_bam) then "yes" else "no"

  command <<<

    set -e -o pipefail

    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    if [ ${use_other_bams} = "yes" ]
    then
        other_bam_command="-S ${splitters_bam} -D ${discordants_bam}"
    else
       other_bam_command=""
    fi

    lumpyexpress \
    -B ${bam} \
    -o ${sample_id}_lumpy.vcf \
    ${LCR_bed} $other_bam_command

    bgzip ${sample_id}_lumpy.vcf


    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File lumpy_vcf_gz = "${sample_id}_lumpy.vcf.gz"
    #File lumpy_vcf_gz_tbi = "${sample_id}_lumpy.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

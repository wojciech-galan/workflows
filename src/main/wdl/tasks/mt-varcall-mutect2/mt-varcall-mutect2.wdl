workflow mt_varcall_mutect2_workflow {

  meta {
    keywords: '{"keywords": ["mitochondrion", "mutect2", "variant calling"]}'
    name: 'mt_varcall_mutect2'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Calls mitochondrial variants with mutec2, optionally lifts-over shifted vcf and mutec2 bamout'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_mt_bam: '{"name": "MT bam", "type": "File", "extension": [".bam"], "description": "Mitochondrial bam (realigned to normal or shifted reference)"}'
    input_mt_bai: '{"name": "MT bai", "type": "File", "extension": [".bai"], "description": "Index for the mitochondrial bam"}'
    input_shifted_bam: '{"name": "Shifted bam?", "type": "Booleans", "default": false, "description": "Is the input bam from the shifted alignment?"}'

    output_mt_vcf_gz: '{"name": "MT vcf gz", "type": "File", "copy": "True", "description": "Mitochondrial vcf"}'
    output_mt_vcf_gz_tbi: '{"name": "MT vcf gz tbi", "type": "File", "copy": "True", "description": "Index for mitochondrial vcf"}'
    output_mutec2_bam: '{"name": "Mutec2 bam", "type": "File", "copy": "True", "description": "Mutect2 bam (with assembled haplotypes and locally realigned reads)"}'
    output_mutec2_bai: '{"name": "Mutec2 bai", "type": "File", "copy": "True", "description": "Index for mutect2 bam"}'
    output_mutec2_stats: '{"name": "Mutec2 stats", "type": "File", "copy": "True", "description": "File with mutect2 variant calling metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call mt_varcall_mutect2

}

task mt_varcall_mutect2 {

  String task_name = "mt_varcall_mutect2"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_mt-varcall-mutect2:1.0.0"

  Boolean shifted_bam = false
  String broad_resources_version = "v0"
  String reference_path = "/resources/broad-institute-hg38-mt-reference/"+ broad_resources_version
  String bam_type = if (shifted_bam) then "-shiftedback" else ""
  String calling_intervals = if (shifted_bam) then "-L chrM:8025-9144" else "-L chrM:576-16024"

  String java_options = "-Xmx5000m"
  String sample_id = "no_id_provided"
  File mt_bam
  File mt_bai


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   if ${shifted_bam};then
       mt_fasta="${reference_path}/Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta"
   else
       mt_fasta="${reference_path}/Homo_sapiens_assembly38.chrM.fasta"
   fi


   gatk --java-options "${java_options}" Mutect2 \
        -R $mt_fasta \
        -I ${mt_bam} \
        -O out.vcf.gz \
        --bam-output bamout.bam \
        ${calling_intervals} \
        --annotation StrandBiasBySample \
        --mitochondria-mode \
        --max-reads-per-alignment-start 75 \
        --max-mnp-distance 0


   if ${shifted_bam};then
      CrossMap.py bam -a ${reference_path}/ShiftBack.chain bamout.bam  out
      mv out.sorted.bam ${sample_id}_mt${bam_type}-mutect2.bam
      mv out.sorted.bam.bai ${sample_id}_mt${bam_type}-mutect2.bai

      gatk LiftoverVcf \
      -I out.vcf.gz \
      -O ${sample_id}_mt${bam_type}.vcf.gz \
      -R "${reference_path}/Homo_sapiens_assembly38.chrM.fasta" \
      --CHAIN ${reference_path}/ShiftBack.chain \
      --REJECT reject.vcf

   else
      mv bamout.bam ${sample_id}_mt${bam_type}-mutect2.bam
      mv bamout.bai ${sample_id}_mt${bam_type}-mutect2.bai
      mv out.vcf.gz ${sample_id}_mt${bam_type}.vcf.gz
      mv out.vcf.gz.tbi ${sample_id}_mt${bam_type}.vcf.gz.tbi
   fi

   mv out.vcf.gz.stats ${sample_id}_mt${bam_type}-stats.txt

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "6G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File mt_vcf_gz = "${sample_id}_mt${bam_type}.vcf.gz"
    File mt_vcf_gz_tbi = "${sample_id}_mt${bam_type}.vcf.gz.tbi"
    File mutec2_bam = "${sample_id}_mt${bam_type}-mutect2.bam"
    File mutec2_bai = "${sample_id}_mt${bam_type}-mutect2.bai"
    File mutec2_stats = "${sample_id}_mt${bam_type}-stats.txt"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

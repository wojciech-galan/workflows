workflow collect_hs_metrics_workflow {

  meta {
    name: 'Collect coverage metrics'
    author: 'https://gitlab.com/mremre, https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Collects coverage metrics using gatk and samtools.'
    changes: '{"latest": "no changes"}'
    input_intervals: '{"name": "intervals", "type": "File", "extension": [".interval_list"], "description": "interval list"}'
    input_bam: '{"name": "bam", "type": "File", "extension": [".bam"], "description": "Alignment result"}'
    input_bai: '{"name": "bai", "type": "File", "extension": [".bai"], "description": "Alignment result index"}'
    output_final_json: '{"name": "final_json.json", "type": "File", "copy": "True", "description": "json with coverage statistics metrics"}'
    output_simple_json: '{"name": "simple_json.json", "type": "File", "copy": "True", "description": "json with human readable coverage statistics metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call collect_hs_metrics

}

task collect_hs_metrics {

  File intervals
  File bam_file
  File bai_file
  String sample_id = "no_id_provided"


  String task_name = "collect_hs_metrics"
  String task_version = "latest"
  String docker_image = "intelliseqngs/collect-hs-metrics:v0.2"

  command <<<
  task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/after-start.sh)

  # GATK metrics
  gatk CollectHsMetrics \
    -BI ${intervals} \
    -I ${bam_file} \
    -O output_hs_metrics.txt \
    -TI ${intervals}

  gatk CollectAlignmentSummaryMetrics \
    -I ${bam_file} \
    -O output_collect_alignment.txt \
    -R /resources/Homo_sapiens_assembly/broad-institute-hg38/Homo_saiens_assembly38.fa

  # samtools duplication rate

  # flagstat=$(samtools flagstat ${bam_file}) - lepiej przypisać do zmiennej żeby się nie liczyło 2 raz to samo, ale nie działa
  #total=$($flagstat | grep "total" | cut -d " " -f1)
  #duplicates=$($flagstat | grep "duplicates" | cut -d " " -f1)

  total=$(samtools flagstat ${bam_file} | grep "total" | cut -d " " -f1)
  duplicates=$(samtools flagstat ${bam_file} | grep "duplicates" | cut -d " " -f1)
  dup_rate=$(echo "scale=2 ; ($duplicates / $total)*100" | bc )


  # make final json
  python3 > gatk_metrics.json <<EOF
  import json, pandas as pd

  df_collect_alignment = pd.read_csv('output_collect_alignment.txt', sep="\t", comment='#').T[2]
  df_collect_alignment["MAPPING_RATE_ON_GENOME"] = (df_collect_alignment["PF_READS_ALIGNED"]/df_collect_alignment["TOTAL_READS"])*100
  result_ca = [{"name":i[0], "value": i[1]} for i in df_collect_alignment.to_dict().items()]

  df_hs_metrics = pd.read_csv('output_hs_metrics.txt', sep="\t", comment='#').T[0]
  df_hs_metrics["CAPTURE_SPECIFICITY"] = (df_hs_metrics["ON_TARGET_BASES"]/df_hs_metrics["PF_BASES_ALIGNED"])*100
  result_hs = [{"name":i[0], "value": i[1]} for i in df_hs_metrics.to_dict().items()]

  result = result_ca + result_hs

  print(json.dumps(result, indent=2))
  EOF

  cat gatk_metrics.json | jq '. += [{"name": "DUPLICATION_RATE","value":"'"$dup_rate"'"}]' | jq '. += [{"name": "SAMPLE_ID","value":"'"${sample_id}"'"}]' > final_json.json
  
  # make simple json
  python3 > simple_json.json <<EOF
  import json

  metadata = {
    "TOTAL_READS": {
      "name": "Number of raw reads",
      "warning": 1000000,
      "error": 100000
    },
    "PF_UQ_READS_ALIGNED": {
      "name": "Total effective reads",
      "warning": 10000,
      "error": 1000
    },
    "PF_BASES": {
      "name": "Number of raw bases",
      "warning": 100000000,
      "error": 1000000
    },
    "PF_BASES_ALIGNED": {
      "name": "Total effective bases",
      "warning": 1000000,
      "error": 10000
    },
    "MAPPING_RATE_ON_GENOME": {
      "name": "Mapping rate on genome [%]",
      "warning": 70,
      "error": 50
    },
    "MEAN_TARGET_COVERAGE": {
      "name": "Mean sequencing depth on target",
      "warning": 20,
      "error": 10
    },
    "CAPTURE_SPECIFICITY": {
      "name": "Caputure specificity [%]",
      "warning": 20,
      "error": 10
    },
    "TARGET_TERRITORY": {
      "name": "Initial bases on target",
      "warning": 100000,
      "error": 10000
    },
    "ON_TARGET_BASES": {
      "name": "Effective sequences in target",
      "warning": 10000000,
      "error": 10000
    },
    "DUPLICATION_RATE": {
      "name": "Duplication rate on genome [%]",
      "warning": 40,
      "error": 60
    }
  }

  data = json.load(open("final_json.json", 'r') )

  # Add measure, value, error and warnings to simple_json
  result = []
  for name in metadata:
    for record in data:
        if record["name"] == name:
            result.append({
              "name": metadata[name]["name"],
              "value": round(float(record["value"]),2),
              "warning": metadata[name]["warning"],
              "error": metadata[name]["error"] 
            })
            break

  print(json.dumps(result, indent=2))
  EOF
  

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/before-finish.sh)
  >>>

  runtime {

    maxRetries: 3
    docker: docker_image
    memory: "1G"
    cpu: "1"

  }

  output {

    File final_json="final_json.json"
    File simple_json="simple_json.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

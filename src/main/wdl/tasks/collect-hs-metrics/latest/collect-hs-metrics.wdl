workflow collect_hs_metrics_workflow {

  meta {
    keywords: '{"keywords": ["gatk", "samtools", "coverage metrics"]}'
    name: 'Collect coverage metrics'
    author: 'https://gitlab.com/mremre, https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Collects coverage metrics using gatk and samtools to 2 jsons (with whole statistics and with human readable statistics with some additional info).'
    changes: '{"latest": "no changes"}'
    input_intervals: '{"name": "intervals", "type": "File", "extension": [".interval_list"], "description": "interval list"}'
    input_bam: '{"name": "bam", "type": "File", "extension": [".bam"], "description": "Alignment result"}'
    input_bai: '{"name": "bai", "type": "File", "extension": [".bai"], "description": "Alignment result index"}'
    error_warning_json_coverage: '{"name": "json", "type": "File", "extension": [".json"], "description": "Json with Description, error and warnings to the report"}'
    output_final_json: '{"name": "final_json.json", "type": "File", "copy": "True", "description": "json with coverage statistics metrics"}'
    output_simple_json: '{"name": "simple_json.json", "type": "File", "copy": "True", "description": "json with human readable coverage statistics metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call collect_hs_metrics

}

task collect_hs_metrics {

  File intervals
  File bam_file
  File bai_file
  File? error_warning_json_coverage

  String sample_id = "no_id_provided"


  String task_name = "collect_hs_metrics"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String task_version = "latest"
  String docker_image = "intelliseqngs/collect-hs-metrics:v0.4"

  command <<<

  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/after-start.sh)

  set -e

  # GATK metricss
  gatk CollectHsMetrics \
    -BI ${intervals} \
    -I ${bam_file} \
    -O output_hs_metrics.txt \
    -TI ${intervals}

  gatk CollectAlignmentSummaryMetrics \
    -I ${bam_file} \
    -O output_collect_alignment.txt \
    -R /resources/Homo_sapiens_assembly/broad-institute-hg38/Homo_saiens_assembly38.fa

  # samtools duplication rate

  # flagstat=$(samtools flagstat ${bam_file}) - lepiej przypisać do zmiennej żeby się nie liczyło 2 raz to samo, ale nie działa
  #total=$($flagstat | grep "total" | cut -d " " -f1)
  #duplicates=$($flagstat | grep "duplicates" | cut -d " " -f1)

  total=$(samtools flagstat ${bam_file} | grep "total" | cut -d " " -f1)
  duplicates=$(samtools flagstat ${bam_file} | grep "duplicates" | cut -d " " -f1)
  dup_rate=$(echo "scale=2 ; ($duplicates / $total)*100" | bc )

  # make json with metrics from Gatk
  # this is place to calculate some additional metrics using the onses created by tools (to chagne to %, divide ect.)
python3 > gatk_metrics.json <<EOF
import json, pandas as pd

df_collect_alignment = pd.read_csv('output_collect_alignment.txt', sep="\t", comment='#').T[2]
df_collect_alignment["MAPPING_RATE_ON_GENOME"] = (df_collect_alignment["PF_READS_ALIGNED"]/df_collect_alignment["TOTAL_READS"])*100
result_ca = [{"name":i[0], "value": i[1]} for i in df_collect_alignment.to_dict().items()]


df_hs_metrics = pd.read_csv('output_hs_metrics.txt', sep="\t", comment='#').T[0]
df_hs_metrics["CAPTURE_SPECIFICITY"] = (df_hs_metrics["ON_TARGET_BASES"]/df_hs_metrics["PF_BASES_ALIGNED"])*100
df_hs_metrics["PF_UQ_READS_ALIGNED_PERCENT"] = (df_hs_metrics["PF_UQ_READS_ALIGNED"]/df_hs_metrics["TOTAL_READS"])*100
result_hs = [{"name":i[0], "value": i[1]} for i in df_hs_metrics.to_dict().items()]

result = result_ca + result_hs

print(json.dumps(result, indent=2))
EOF

  # Add metrics from samtools to above json (gatk_metrics.json) and create final_json.json

  cat gatk_metrics.json | jq '. += [{"name": "DUPLICATION_RATE","value":"'"$dup_rate"'"}]' > final_json.json

  # this python script will take some template with erors, warnings (can be "not provided"), description and add them to final json
  # Additionally json template will determine which metrics from final_json keep in simple_report.
  python3 /opt/tools/python-scripts/collect-hs-metrics.py \
    final_json.json \
    ${default="/opt/tools/coverage-statisttics-error-warning-not-provided.json" error_warning_json_coverage} \
    > simple_json.json

  set +e
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File final_json="final_json.json"
    File simple_json="simple_json.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

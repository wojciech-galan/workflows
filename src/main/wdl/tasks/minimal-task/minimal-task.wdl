workflow minimal_task_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'minimal_task'
    author: 'https://gitlab.com/Monika Krzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"2.0.3": "remove output", "2.0.2": "working optional input in gcloud", "2.0.1": "Added exit code and exit info"}'
    input_sample_id: '{"name": "Sample id", "type": "String", "description": "identifier of sample"}'
    input_exit_code: '{"name": "Exit code", "type": "Int", "default": 0, "description": "Exit code which task returns"}'
    input_exit_info: '{"name": "Exit info", "type": "String", "description": "Exit info which task returns"}'

    output_user_error_message: '{"name": "User error message", "type": "File", "copy": "True", "description": "Optional file with exit info message"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call minimal_task

}

task minimal_task {

  String task_name = "minimal_task"
  String task_version = "2.0.3"

  Int exit_code = 0
  String? exit_info
  String user_error_message_filename = "user-error-message.txt"

  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    if [ ${exit_code} -ne 0 ] ; then
      echo ${exit_info} > ${user_error_message_filename}
      exit ${exit_code}
    fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }
}

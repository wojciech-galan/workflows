workflow phasing_eagle_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "phasing", "imputing"]}'
    name: 'Phasing with eagle software'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Performs phasing with Eagle software'
    changes: '{"1.0.3": "fixed resources location", "1.0.2": "even more threads and memory", "1.0.0": "more threads and memory"}'

    input_chrom: '{"name": "Chromosome number", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX"]}, "description": "Chromosome number"}'
    input_vcf: '{"name": "Input file", "type": "File", "description": "Vcf file corresponding to the chromosome"}, "constraints": {"extension": [".vcf.gz"]}'

    output_bgzipped_initial_vcf: '{"name": "Bgzipped initial file", "type": "File", "copy": "True", "description": "Bgzipped initial file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_phased_vcf_gz: '{"name": "Output file", "type": "File", "copy": "True", "description": "Phased, gzipped vcf"}'

  }

  call phasing_eagle

}

task phasing_eagle {

  String task_name = "phasing_eagle"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  String chrom
  String chrom_num = sub(chrom, 'chr', '')
  String docker_image = "intelliseqngs/task_phasing-eagle:3.0.0-" + chrom
  String phased_prefix = "${chrom_num}_phased"
  Int threads = 4

  File vcf
  String vcf_filename = basename(vcf)

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  bgzip -c ${vcf} > ${vcf_filename}.gz
  tabix -fp vcf ${vcf_filename}.gz
  eagle    --allowRefAltSwap \
           --geneticMapFile /resources/genetic_map/hg38/genetic_map_hg38_withX.txt.gz \
           --vcfRef /resources/reference_data/hg38/${chrom_num}.bcf \
           --vcfTarget ${vcf_filename}.gz \
           --outPrefix ${phased_prefix} \
           --numThreads ${threads}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: threads
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File phased_vcf_gz = "${phased_prefix}.vcf.gz"
    File bgzipped_initial_vcf = "${vcf_filename}.gz"

  }

}

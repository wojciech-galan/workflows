workflow gvcf_merge_workflow {

  meta {
    keywords: '{"keywords": ["gvcf", "merge"]}'
    name: 'gvcf_merge'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Merging array of gVCFs into one'
    changes: '{"latest": "no changes"}'


    input_sample_id: '{"name": "sample id", "type": "String", "description": "Identifier of sample"}'
    input_gvcf_gzs:'{"name": "gvcf gzs", "type": "Array[File]", "description": "Bgzipped gVCF files"}'
    input_gvcf_gz_tbis: '{ "name": "gvcf index", "type": "Array[File]", "description": "gVCF files indexes"}'

    output_gvcf_gz:'{"name": "gvcf gz","type": "File", "description": "merged bgzipped gVCF file"}'
    output_gvcf_gz_tbi: '{ "name": "gvcf index", "type": "File", "description": "merged gVCF file index"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call gvcf_merge

}

task gvcf_merge {

  String task_name = "gvcf_merge"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.6.0:1.0.0"

  String sample_id

  Array[File] gvcf_gzs
  Array[File] gvcf_gz_tbis

  # Tools runtime settings, paths etc.
  String java_opt = "-Xms3000m"
  String gatk_path = "/gatk/gatk"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  set -e
  ${gatk_path} --java-options ${java_opt} \
    MergeVcfs \
    --INPUT ${sep=' --INPUT ' gvcf_gzs} \
    --OUTPUT ${sample_id}.g.vcf.gz

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File gvcf_gz = "${sample_id}.g.vcf.gz"
    File gvcf_gz_tbi = "${sample_id}.g.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow sv_anno_panel_workflow {

  meta {
    keywords: '{"keywords": ["SV", "annotation", "gene panel"]}'
    name: 'sv_anno_panel'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Annotates SV vcf with gene panel'
    changes: '{"1.1.1": "panel command in different way","1.1.0": "Works with new annotsv version, filtering options removed"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "SV vcf gz", "type": "File", "description": "AnnotSV annotated vcf (Gene_name field must be present)"}'
    input_vcf_gz_tbi: '{"name": "SV vcf gz", "type": "File", "description": "Index for the annotated SV vcf"}'
    input_gene_panel: '{"name": "Gene panel", "type": "File", "description": "Gene panel json. Output of the panel-generate task"}'

    output_panel_anno_vcf_gz: '{"name": "Panel annotated vcf gz", "type": "File", "copy": "True", "description": "SV vcf annotated with gene panel"}'
    output_panel_anno_vcf_gz_tbi: '{"name": "Panel annotated vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the annotated SV vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_anno_panel

}

task sv_anno_panel {

  String task_name = "sv_anno_panel"
  String task_version = "1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_sv-anno-panel:1.1.0"

  String sample_id = "no-id"
  File vcf_gz
  File vcf_gz_tbi
  File? gene_panel


   command <<<

   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   set -e -o pipefail

   python3 /intelliseqtools/sv-vcf-annotate-panel.py \
       --input-vcf ${vcf_gz} \
       --output-vcf ${sample_id}_panel-annotated-sv.vcf.gz \
       ${'--gene-panel ' + gene_panel}


   tabix -p vcf ${sample_id}_panel-annotated-sv.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File panel_anno_vcf_gz = "${sample_id}_panel-annotated-sv.vcf.gz"
    File panel_anno_vcf_gz_tbi = "${sample_id}_panel-annotated-sv.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }
}

workflow sv_filter_workflow {

  meta {
    keywords: '{"keywords": ["sv", "filter", "quality"]}'
    name: 'Structural variants filter'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Splits multiallelic sv, removes low quality and probably spurious structural variants from vcf'
    changes: '{"1.0.2": "empty output test", "1.0.1": "bcftools command fixed"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_svvcf: '{"name": "SV vcf", "type": "File",  "description": "Genotyped structural variant vcf"}'
    input_sample_svvcf_tbi: '{"name": "SC vcf tbi", "type": "File", "description": "Index file"}'
    input_min_qual: '{"name": "Min qual", "type": "Float", "description": "Minimal qual of valid variant"}'
    input_del_cov_max: '{"name": "Deletion coverage max", "type": "Float", "description": "Maximal coverage of valid deletion (compared to +/-1000bp flanking regions)"}'
    input_dup_cov_min: '{"name": "Duplication coverage min", "type": "Float", "description": "Minimal coverage of valid duplication (compared to +/-1000bp flanking regions)"}'
    input_del_cov_gcbin_max: '{"name": "Deletion coverage max", "type": "Float", "description": "Maximal coverage of valid deletion (compared to genome bins with similar GC content)"}'
    input_dup_cov_gcbin_min: '{"name": "Duplication coverage min", "type": "Float", "description": "Minimal coverage of valid duplication (compared to genome bins with similar GC content)"}'
    input_min_snp_count: '{"name": "Min SNP count", "type": "Int", "description": "Minimal number of SNP/INDELs in deletion region to apply heterozygosity filter"}'
    input_het_max: '{"name": "Heterozygosity max", "type": "Float", "description": "Maximal heterozygosity od valid deletion"}'

    output_sv_vcf_gz: '{"name": "SV vcf gz", "type": "File", "copy": "True", "description": "Filtered structural variant vcf"}'
    output_sv_vcf_gz_tbi: '{"name": "SV vcf gz tbi", "type": "File", "copy": "True", "description": "Index for output sv vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_filter

}

task sv_filter {

  String task_name = "sv_filter"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.3"

  String sample_id = "no-id"
  File svvcf
  File svvcf_tbi

  # Minimal QUAL of valid variant
  Float min_qual = 30

  # Maximal depth of valid deletion, relative to +/-1000bp flanking regions
  Float del_cov_max = 0.7

  # Maximal depth of valid deletion, relative to bins in the genome with similar GC-content
  Float del_cov_gcbin_max = 0.7

  # Minimal depth of valid duplication, relative to +/-1000bp flanking regions
  Float dup_cov_min = 1.3

  # Minimal depth of valid duplication, relative to bins in the genome with similar GC-content
  Float dup_cov_gcbin_min = 1.3

  # Minimal number of SNP/INDELs in deleted region to apply heterozygosity filter
  Int min_snp_count = 4

  # Maximal fraction of heterozygous short variants in deletion region (is 0.25 ok?)
  Float het_max = 0.25

  command <<<

   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   bcftools norm -N --multiallelic -any ${svvcf} | \
   bcftools filter -e '((INFO/SVTYPE == "DEL" & FORMAT/DHFFC > ${del_cov_max}) | (INFO/SVTYPE == "DUP" & FORMAT/DHFFC < ${dup_cov_min}))' | \
   bcftools filter -e '((INFO/SVTYPE == "DEL" & FORMAT/DHBFC > ${del_cov_gcbin_max}) | (INFO/SVTYPE == "DUP" & FORMAT/DHBFC < ${dup_cov_gcbin_min}))' | \
   bcftools filter -e 'QUAL < ${min_qual}' | \
   bcftools filter -e 'GT ~"\." | GT == "ref" ' -O z -o ${sample_id}_partially-filtered-sv.vcf.gz

   tabix -p vcf ${sample_id}_partially-filtered-sv.vcf.gz

   if  tabix -H ${svvcf} | grep -q 'DHGT'
   then
       echo "GT filtering..."
       bcftools filter -e '(INFO/SVTYPE == "DEL") & ((FORMAT/DHGT[*:0] + FORMAT/DHGT[*:1] + FORMAT/DHGT[*:2]) >= ${min_snp_count}) & ((FORMAT/DHGT[*:1] / (FORMAT/DHGT[*:0] + FORMAT/DHGT[*:1] + FORMAT/DHGT[*:2])) > ${het_max})' \
                    ${sample_id}_partially-filtered-sv.vcf.gz \
                    -O z -o ${sample_id}_genotyped-filtered-sv.vcf.gz
       tabix -p vcf ${sample_id}_genotyped-filtered-sv.vcf.gz
   else
       echo "not filtering"
       mv ${sample_id}_partially-filtered-sv.vcf.gz ${sample_id}_genotyped-filtered-sv.vcf.gz
       mv ${sample_id}_partially-filtered-sv.vcf.gz.tbi ${sample_id}_genotyped-filtered-sv.vcf.gz.tbi
   fi

   ## Check is ouptut vcf is not empty (to finish module work)
   if zgrep -q -v '^#' ${sample_id}_genotyped-filtered-sv.vcf.gz; then
      echo "true" > vcf_is_not_empty.txt
   else
      echo "false" > vcf_is_not_empty.txt
   fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File sv_vcf_gz = "${sample_id}_genotyped-filtered-sv.vcf.gz"
    File sv_vcf_gz_tbi = "${sample_id}_genotyped-filtered-sv.vcf.gz.tbi"
    Boolean vcf_not_empty = read_boolean("vcf_is_not_empty.txt")
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }
}
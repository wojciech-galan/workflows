workflow vcf_acmg_pm4_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "PM4", "variant", "pathogenic"]}'
    name: 'Vcf ACMG PM4'
    author: 'https://gitlab.com/gleblavr , https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Evaluates whether variant fulfils ACMG PM4 criterion, adds annotation'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf"], "description": "Input annotated vcf"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True","type": "File", "description": "Vcf with ACMG PM4 annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call vcf_acmg_pm4

}

task vcf_acmg_pm4 {

  String task_name = "vcf_acmg_pm4"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-pm4:1.0.0"
  File vcf
  String sample_id = "no-id"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.0/after-start.sh)

  bgzip -d ${vcf} -c > tmp.vcf

  python3 /intelliseqtools/acmg-pm4.py \
          --input-vcf tmp.vcf \
          --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz

 ## check if all data lines present
  lines_before=$( grep -c -v '^#' tmp.vcf )
  lines_after=$( zcat ${sample_id}_annotated-with-acmg.vcf.gz | grep -c -v '^#' )
  [[ $lines_before != $lines_after ]] && echo "Not all variants present in output vcf" && exit 1

  tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.0/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow vcf_filter_panel_workflow {

  meta {
    keywords: '{"keywords": ["acmg", "vcf", "genes"]}'
    name: 'Vcf filter panel'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## Annotates vcf with panel fitness scores, optionally applies gene panel filter'
    changes: '{"1.1.1": "Panel scores as string"}'

    input_vcf_gz: '{"name": "Vcf gz", "type": "File", "extension": [".vcf.gz"], "description": "annotated vcf"}'
    input_vcf_gz_tbi: '{"name": "Vcf gz tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "index for annotaated vcf"}'
    input_apply_filter: '{"name": "Apply filter?", "type": "Boolean", "description": "Decides whether apply filtering for gene panel"}'
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
 
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_filtered_vcf_gz: '{"name": "Filtered vcf gz", "type": "File", "description": "VCF annotated with panel scores and (optionally) only with variants within genes from panel"}'
    output_filtered_vcf_gz_tbi: '{"name": "filtered_vcf_gz_tbi", "type": "File", "description": "Index for the filtered vcf"}'
 
  }

  call vcf_filter_panel

}

task vcf_filter_panel {

  File vcf_gz
  File vcf_gz_tbi
  File? panel_json
  Boolean apply_filter = true
  String filtering_option = if (apply_filter) then "" else "--skip_filter"
  Boolean panel_defined = defined(panel_json)

  String task_name = "vcf_filter_panel"
  String task_version = "1.1.2"
  String docker_image = "intelliseqngs/task_vcf-filter-panel:1.1.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  String sample_id = "no_id_provided"
  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  zcat ${vcf_gz} | grep "^#" > temp.vcf
  zcat ${vcf_gz} | grep -v "^#" | grep -v "MULTIALLELIC" >> temp.vcf

  set -e -o pipefail

  if ${panel_defined}; then
    cat temp.vcf | python3 /intelliseqtools/filter-vcf-by-gene-panel.py ${panel_json} ${filtering_option} | bgzip > ${sample_id}_filtered.vcf.gz
  else
    bgzip -c temp.vcf > ${sample_id}_filtered.vcf.gz
  fi

  tabix -p vcf ${sample_id}_filtered.vcf.gz 

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File filtered_vcf_gz = "${sample_id}_filtered.vcf.gz"
    File filtered_vcf_gz_tbi = "${sample_id}_filtered.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

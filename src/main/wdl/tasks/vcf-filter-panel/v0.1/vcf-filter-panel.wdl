workflow vcf_filter_panel_workflow {

  meta {
    name: 'vcf_filter_panel'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## Filtering vcf for gene panel \n Generic text for task'
    output_stdout_log: '{name: "Standard out", type: "File", copy: "True", description: "Console output"}'
    output_stderr_err: '{name: "Standard err", type: "File", copy: "True", description: "Console stderr"}'
    output_bco: '{name: "Biocompute object", type: "File", copy: "True", description: "Biocompute object"}'
  }

  call vcf_filter_panel

}

task vcf_filter_panel {

  File vcf_gz
  File vcf_gz_tbi
  File? panel_json
  Boolean panel_defined = defined(panel_json)

  String task_name = "vcf_filter_panel"
  String task_version = "latest"
  String docker_image = "intelliseqngs/ubuntu-toolbox:18.04_v0.10"

  command <<<
  task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/after-start.sh)

  zcat ${vcf_gz} | grep "^#" > temp.vcf
  zcat ${vcf_gz} | grep -v "^#" | grep -v MULTIALLELIC >> temp.vcf

  if ${panel_defined}; then
    cat temp.vcf | python3 /opt/tools/python_scripts-0.12/filter-vcf-by-gene-panel.py ${panel_json} | bgzip > filtered.vcf.gz
  else
    bgzip temp.vcf > filtered.vcf.gz
  fi

  tabix -p vcf filtered.vcf.gz

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/before-finish.sh)
  >>>

  runtime {

    maxRetries: 3
    docker: docker_image
    memory: "1G"
    cpu: "1"

  }

  output {

    File filtered_vcf_gz = "filtered.vcf.gz"
    File filtered_vcf_gz_tbi = "filtered.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

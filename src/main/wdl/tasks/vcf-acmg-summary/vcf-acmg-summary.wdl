workflow vcf_acmg_summary_workflow {

  meta {
    keywords: '{"keywords": ["ACMG", "vcf", "summary"]}'
    name: 'Vcf ACMG summary'
    author: 'https://gitlab.com/gleblavr , https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Summarize ACMG results, adds annotation'
    changes: '{"1.2.0": "PP4 category added", "1.1.0": "Python script reformatted, latest removed"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf: '{"name": "Vcf", "type": "File", "extension": [".vcf"], "description": "Input annotated vcf"}'

    output_annot_vcf: '{"name": "Annotated vcf", "copy": "True","type": "File", "description": "Vcf with ACMG summary annotation"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_acmg_summary

}

task vcf_acmg_summary {

  String task_name = "vcf_acmg_summary"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-summary:1.2.0"
  File vcf_gz
  String sample_id = "no-id"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  zcat ${vcf_gz} > myvcf

  python3 /intelliseqtools/acmg-summary.py -i myvcf > ${sample_id}_annotated-with-acmg-summary.vcf

  bgzip -c  ${sample_id}_annotated-with-acmg-summary.vcf >  ${sample_id}_annotated-with-acmg-summary.vcf.gz
  tabix -p vcf ${sample_id}_annotated-with-acmg-summary.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg-summary.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg-summary.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

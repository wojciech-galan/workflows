workflow vcf_anno_dbnsfp_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "vcf-annoattion", "dbnsbp", "snpSift"]}'
    name: 'vcf_anno_dbnsfp'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Annotating vcf with dbnsnp database'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_vcf_gz: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"], "description": "Chromosome for analysis"}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "no_id_provided", "description": "Sample ID"}'

    output_annotate_vcf_with_dbnsfp_vcf_gz :'{"name": "annotated vcf", "type": "File"}'
    output_annotate_vcf_with_dbnsfp_vcf_gz_tbi :'{"name": "annotared vcf index", "type": "File"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
   
  }

  call vcf_anno_dbnsfp

}

task vcf_anno_dbnsfp {

  String task_name = "vcf_anno_dbnsfp"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  
  # Input VCF file must:
  # - be bgzipped
  # - have left-normalized indels (bcftools norm)
  # - multiallelic sites split (bcftools norm --multiallelics -any)")
  File vcf_gz
  File vcf_gz_tbi

  String chromosome
  String docker_image = "intelliseqngs/task_vcf-anno-dbnsfp:" + "1.0.0-" + chromosome

  String vcf_basename = "no_id_provided"

  String python_out_mod = "/intelliseqtools/change-dbnsfp-output.py"
  String vcf_prefix = chromosome + "-" + vcf_basename

  String snpsift_java_mem = "-Xmx4g"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/after-start.sh)

  set -e pipefail

  java -jar ${snpsift_java_mem} /tools/snpEff/SnpSift.jar dbnsfp -v -db \
  \/resources/dbnsfp/dbNSFP4.0a_variant.${chromosome}.gz ${vcf_gz} \
  | python3 ${python_out_mod} > ${vcf_prefix}_annotated-with-dbnsfp.vcf 

  bgzip ${vcf_prefix}_annotated-with-dbnsfp.vcf
  tabix -p vcf ${vcf_prefix}_annotated-with-dbnsfp.vcf.gz

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    
    File annotate_vcf_with_dbnsfp_vcf_gz = "${vcf_prefix}_annotated-with-dbnsfp.vcf.gz"
    File annotate_vcf_with_dbnsfp_vcf_gz_tbi = "${vcf_prefix}_annotated-with-dbnsfp.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

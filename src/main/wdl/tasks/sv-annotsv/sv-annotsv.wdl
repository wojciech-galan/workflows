workflow sv_annotsv_workflow {

  meta {
    keywords: '{"keywords": ["sv", "annotation"]}'
    name: 'Structural variant annotation'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Annotates structural variants with AnnotSV v3.0 (with ISEQ modifications).'
    changes: '{"2.0.0": "New annotSV version, with ACMG ranking", "1.1.0": "optional outputs as array","1.0.1": "dealing with input empty vcf"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_sv_vcf_gz: '{"name": "Structural variants VCF (bgzipped)", "type": "File", "description": "Bgzipped VCF file containing structural variants information"}'
    input_sv_vcf_gz_tbi: '{"name": "Structural variants VCF (bgzipped) index", "type": "File", "description": "Tabix index of bgzipped VCF file containing structural variants information"}'
    input_reference_genome:  '{"name": "Reference genome", "type": "String", "constraints": {"values": ["GRCh38"]}, "default": "GRCh38", "description": "Reference genome"}'
    input_promoter_size:  '{"name": "Promoter size", "type": "Int", "constraints": {"min": 0}, "default": 500, "description": "Number of bases upstream from the transcription start site."}'
    input_sv_minimum_size:  '{"name": "Minimum SV size", "type": "Int", "constraints": {"min": 0}, "default": 50, "description": "SV minimum size (in bp)."}'
    input_include_ci: '{"name": "Include CI?", "type": "String", "constraints": {"values": ["yes", "no"]}, "default": "yes", "description": "This option decides whether variant should be extended by its confidence intervals"}'
    input_panel_file: '{"name": "Panel json file", "type": "File", "description": "Json file with HPO, phenotypes, diseases genes information (output from panel-generate task: all-panel.json)."}'
    input_panel_threshold1: '{"name": "Panel threshold1", "default": 30.0, "type": "Float", "description": "Panel score threshold for good gene phenotype match"}'
    input_panel_threshold2: '{"name": "Panel threshold2", "default": 45.0, "type": "Float", "description": "Panel score threshold for very good gene phenotype match"}'
    input_size_of_vcf_chunk: '{"name": "(Advanced) Size of VCF chunk", "type": "String" , "default": "200M", "description": "(Advanced) AnnotSV is able to process only VCFs with limited size, therefore the input VCF must be divided into smaller VCF chunks before annotating it with AnnotSV. If the task fails with error - max size for a Tcl value (2147483647 bytes) exceeded or - try to set the lower size. The size argument is an integer and optional unit (example: 10K is 10*1024). Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000)."}'

    output_full_annotsv_tsv: '{"name": "AnnotSV TSV OUTPUT (gzipped)", "copy": "True", "type": "Array[File]", "description": "Array of structural variants TSV (gzipped) - output of AnnotSV. Note that this may be empty array or array with only one output file."}'
    output_annotated_vcf_gz: '{"name": "AnnotSV VCF OUTPUT (bgzipped)", "copy": "True", "type": "Array[File]", "description": "Structural variants VCF (bgzipped) - modified and shortened output of AnnotSV."}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call sv_annotsv

}

task sv_annotsv {

  String task_name = "sv_annotsv"
  String task_version = "2.0.0"

  File sv_vcf_gz
  File sv_vcf_gz_tbi
  File? panel_file
  String sample_id = "no_id_provided"

  # Inputs with defaults
  String reference_genome = "GRCh38"
  String ensembl_version = "102"

  Float panel_threshold1 = 30.0
  Float panel_threshold2 = 45.0
  Int promoter_size = 500
  Int sv_minimum_size = 50
  String include_ci = "yes" # other option "no"

  Boolean is_panel_file_defined = defined(panel_file)

  String size_of_vcf_chunk = "200M"
  Int max_no_jobs_in_parallel = 8

   # Tools runtime settings, paths etc.

  String annotsv_instalation_date = "03-01-2021"
  String annotsv_env_path = "/tools/AnnotSV/" + annotsv_instalation_date
  String annotsv_path = "/tools/AnnotSV/" + annotsv_instalation_date + "/bin/AnnotSV"

  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/annotsv-grch38:1.0.2"

  command <<<

    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    set -e -o pipefail

  # Input vcf with variants

   if zgrep -v -m1 "^#" ${sv_vcf_gz};then

      # Split input VCF into chunks not exceding 2GB
      printf "spliting input SV VCF into chunks... \n"
      tabix -H ${sv_vcf_gz} > header
      zcat ${sv_vcf_gz} | grep -v "^#" | split -C ${size_of_vcf_chunk} -d --additional-suffix="-chunk.vcf" --filter=' cat header - | bgzip > $FILE.gz'

      # set ANNOTSV variable
      export ANNOTSV=${annotsv_env_path}

      # Annotate chunks with AnnotSV
      printf "annotating chunks with AnnotSV and xargs... \n"
      index=0
      indices=""
      for chunk_vcf_gz in x*-chunk.vcf.gz; do
         mkdir "$index"-chunk
         mv "$chunk_vcf_gz" "$index"-chunk/"$index".vcf.gz
         indices="$indices"" ""$index"
         index=$((index + 1))
      done

      echo $indices | sed 's/ /\n/g' | xargs -i -n 1 -P ${max_no_jobs_in_parallel} bash -c  \
       " ${annotsv_path} \
          -genomeBuild ${reference_genome} \
          -promoterSize ${promoter_size} \
          -SVminSize ${sv_minimum_size} \
          -outputDir {}-chunk/ \
          -SvinputFile {}-chunk/{}.vcf.gz \
          -outputFile {}.annotsv.tsv \
          -tx ENSEMBL \
          -includeCI ${include_ci} \
          -txFile /resources/ensembl/${ensembl_version}/transcripts.txt " \
          2> annotsv.stderr > annotsv.stdout

      cat annotsv.stderr >&2
      cat annotsv.stdout

      if grep -q "couldn't fork child process: not enough memory" annotsv.stderr || grep -q "max size for a Tcl value (2147483647 bytes) exceeded" annotsv.stderr ; then
          printf "Task FAILED with errors\n"
          exit 1
      else

         # Concatenate outputs
          printf "concatenating outputs... \n"

          head -1 0-chunk/0.annotsv.tsv > ${sample_id}_annotsv.tsv
          for index in $indices; do
             sed 1d "$index"-chunk/"$index".annotsv.tsv  >> ${sample_id}_annotsv.tsv
          done
          gzip ${sample_id}_annotsv.tsv

          if ${is_panel_file_defined};then
              mv ${panel_file}  panel.json
          else
              echo '[]' > panel.json
          fi


          python3 /intelliseqtools/panel-scores-to-annotsv-table.py \
              -i ${sample_id}_annotsv.tsv.gz \
              -o ${sample_id}_annotsv-with-panel.tsv.gz \
              -p panel.json


          python3 /intelliseqtools/change-annotsv-ranking.py \
               -i ${sample_id}_annotsv-with-panel.tsv.gz \
               -o ${sample_id}_annotsv-short-table.tsv.gz \
               -f ${sample_id}_annotsv-full-table.tsv.gz \
               -t ${panel_threshold1} \
               -T ${panel_threshold2}

          python3 /intelliseqtools/annotsv-tsv-to-vcf.py \
                -t ${sample_id}_annotsv-short-table.tsv.gz \
                -i ${sv_vcf_gz} \
                -d /resources/annotsv-annotations-description/2.0.0/annotsv-annotations-description.tsv  \
                -o ${sample_id}_annotated-with-annotsv.vcf

          bgzip  ${sample_id}_annotated-with-annotsv.vcf




          # Finish
          printf "Finish!\n"

      fi

    # No variants in vcf file
    else
    mv ${sv_vcf_gz} ${sample_id}_annotated-with-annotsv.vcf.gz
      printf "No variants found\n"



    fi



    bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File annotated_vcf_gz = "${sample_id}_annotated-with-annotsv.vcf.gz"

    # this is trick to allow for optional outputs on gloud
    Array[File] short_annotsv_tsv_gz = glob("${sample_id}_annotsv-short-table.tsv.gz")
    Array[File] full_annotsv_tsv_gz = glob("${sample_id}_annotsv-full-table.tsv.gz")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
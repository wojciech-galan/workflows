workflow vcf_qc_workflow {


  call vcf_qc

}

task vcf_qc {

  File vcf
  File sample_info_json
  File parameters_json
  String sample_id ="no_id"

  String task_name = "vcf_qc"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-qc:1.0.5"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   jq -r '.[] | ["0", .name, .sex ] | @tsv' ${sample_info_json} > sex_info.tsv
   jq -r '["IID, POP_ID"],( .[] | [ .name, .population ] )| @tsv' ${sample_info_json} > population_info.tsv
   jq -r '.[] | [.parameter, .value]| @tsv' ${parameters_json} > parameters.tsv

   if grep -c 'imissTh' parameters.tsv; then
        imissTh=$(grep 'imissTh' parameters.tsv | cut -f2)
   else imissTh=0.01; fi
   if grep -c 'hetTh' parameters.tsv; then
        hetTh=$(grep 'hetTh' parameters.tsv | cut -f2)
   else hetTh=3; fi
   if grep -c 'highIBDTh' parameters.tsv; then
        highIBDTh=$(grep 'highIBDTh' parameters.tsv | cut -f2)
   else highIBDTh=0.5; fi


   /intelliseqtools/plink --vcf ${vcf} --make-bed --keep-allele-order --const-fid 0 --update-sex sex_info.tsv --split-x 'hg38' --out ${sample_id}
   #outputs: ${sample_id}.bed ${sample_id}.bim ${sample_id}.fam

   /intelliseqtools/plink --pca --bed ${sample_id}.bed --bim ${sample_id}.bim --fam ${sample_id}.fam --out ${sample_id}

   Rscript /intelliseqtools/plinkQC.R --id ${sample_id} --pwd $PWD --imissTh $imissTh --hetTh $hetTh --highIBDTh $highIBDTh
   cat qc.tsv | jq -s  --slurp --raw-input --raw-output 'split("\n") | .[1:-1] | map(split("\t")) |
        map({"id": .[0],
             qc:{"sex":{ value:.[1],
             "pass": .[2]},
             "miss":{ value:.[3],
             "pass": .[4]},
             "het":{ value:.[5],
             "pass": .[6]}},
   })' > "${sample_id}-perIndividual_vcf-qc.json"

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File perIndividual_vcf_qc_json = "${sample_id}-perIndividual_vcf-qc.json"
    File perMarker_vcf_qc_json = "${sample_id}-perMarker_vcf-qc.json"
    Array[File] vcf_qc_images = glob("*.jpg")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow bam_merge_workflow {

  meta {
    keywords: '{"keywords": ["merge-bams", "bam", "samtools"]}'
    name: 'Bam merge'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Merge BAMS from differents lanes'
    changes: '{"latest": "no changes"}'

    input_lane_markdup_bams: '{"name": "Lane markdup bam", "type": "Array[File]", "extension": [".bam"], "description": "Array of bams to merge into one bam and create its index (it can be one bam file)."}'
    input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Identifier of sample"}'

    output_markdup_bam: '{"name": "Markdup bam", "type": "File", "description": "Merged BAM"}'
    output_markdup_bai: '{"name": "Markdup bai", "type": "File", "description": "Index of merged BAM"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call bam_merge

}

task bam_merge {

  Array[File] lane_markdup_bams

  String sample_id = "no_id_provided"
  Int num_files = length(lane_markdup_bams)

  String task_name = "bam_merge"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-19.04:1.0.0"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/after-start.sh)

  # Do not merge if there is only 1 file
  if [[ ${num_files} == 1 ]]; then
    cp ${sep=" " lane_markdup_bams} ${sample_id}_markdup.bam
  else
    samtools merge -pc ${sample_id}_markdup.bam ${sep=" " lane_markdup_bams}
  fi

  samtools index ${sample_id}_markdup.bam ${sample_id}_markdup.bai

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }
  output {

    File markdup_bam = "${sample_id}_markdup.bam"
    File markdup_bai = "${sample_id}_markdup.bai"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

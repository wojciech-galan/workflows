workflow mt_merge_mutect2_outputs_workflow {

  meta {
    keywords: '{"keywords": ["mitochondrion", "mutect2"]}'
    name: 'mt_merge_mutec2_outputs'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Merges files produced by mutect2 (from shifted and normal analyses)'
    changes: '{"1.0.1": "changed output files names"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_mutect_vcfs: '{"name": "Mutect vcfs", "type": "Array[File]", "extension": [".vcf.gz"], "description": "MT vcfs produced by mutect2"}'
    input_mutect_bams: '{"name": "Mutect bams", "type": "Array[File]", "extension": [".bam"], "description": "MT realigned bams produced by mutect2"}'
    input_mutect_stats: '{"name": "Mutect stats", "type": "Array[File]", "description": "Mutect2 variant calling metrics file"}'


    output_merged_mt_vcf_gz: '{"name": "Merged MT vcf gz", "type": "File", "description": "Merged mitochondrial vcf, may be restricted to chosen intervals (for targeted sequencing)"}'
    output_merged_mt_vcf_gz_tbi: '{"name": "Merged MT vcf gz tbi", "type": "File", "description": "Merged mitochondrial vcf index"}'
    output_merged_m2_mt_bam: '{"name": "Merged mutect2 MT bam", "type": "File", "description": "Merged mitochondrial realigned mutect2  bam"}'
    output_merged_m2_mt_bai: '{"name": "Merged mutect2 MT bai", "type": "File", "description": "Index for the merged mitochondrial mutect2 bam"}'
    output_merged_m2_stats: '{"name": "Merged mutect2 stats", "type": "File", "description": "Merged mutect2 variant calling metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call mt_merge_mutect2_outputs

}

task mt_merge_mutect2_outputs {

  String task_name = "mt_merge_mutect2_outputs"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.9.0:1.0.0"

  String sample_id = "no_id_provided"
  Array[File] mutect_vcfs
  Array[File] mutect_bams
  Array[File] mutect_stats

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   # merge vcfs (backshifted and normal)
   gatk MergeVcfs \
      -I ${sep=' -I ' mutect_vcfs} \
      -O ${sample_id}_merged-mt-mutect2.vcf.gz


   # merge mutect2 realigned bams (backshifted and normal)
   gatk MergeSamFiles \
      -I ${sep=' -I ' mutect_bams} \
      -O ${sample_id}_merged-mt-mutect2.bam \
      --VALIDATION_STRINGENCY SILENT \
      --CREATE_INDEX true


   # merge mutect2 stats (from backshifted and normal varcalling)
   gatk MergeMutectStats \
       --stats ${sep=' --stats ' mutect_stats} \
        -O ${sample_id}_raw-mutect2-stats.txt


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {
    File merged_mt_vcf_gz = "${sample_id}_merged-mt-mutect2.vcf.gz"
    File merged_mt_vcf_gz_tbi = "${sample_id}_merged-mt-mutect2.vcf.gz.tbi"
    File merged_m2_mt_bam = "${sample_id}_merged-mt-mutect2.bam"
    File merged_m2_mt_bai = "${sample_id}_merged-mt-mutect2.bai"
    File merged_m2_stats = "${sample_id}_raw-mutect2-stats.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

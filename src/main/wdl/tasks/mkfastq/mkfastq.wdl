workflow mkfastq_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'mkfastq'
    author: 'https://gitlab.com/mremre'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'mkfastq - old task without test left just in case'
    changes: '{"latest": "no changes"}'


    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call mkfastq

}

task mkfastq {

  File bcl_tar
  File? csv

  String task_name = "mkfastq"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/mkfastq:0.0.2"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"

  #task inputs "mkfastq_workflow.mkfastq.bcl_tar": "/home/marcel/191207_ST-E00144_1055_AH3T37CCX2.tar.gz"

  tar -xvf ${bcl_tar}

  export PATH=/opt/tools/longranger-2.2.2:$PATH

  longranger mkfastq --id=191207_ST-E00144_1055_AH3T37CCX2 \
                    --run=191207_ST-E00144_1055_AH3T37CCX2
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()

  }

}

workflow vcf_anno_dbsnp_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "dbsnp"]}'
    name: 'Vcf annotation with dbsnp rsID'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'The task adds a VCF field ISEQ_DBSNP_RS, containing dbsnp rsIDs'
    changes: '{"1.2.0": "dbsnp build 154, removed latest dir"}'

    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file, should have splitted multiallelic sites and normalized indels."}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID"}'
    input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]} , "description": ""}'

    output_annotated_with_dbsnp_rsids_vcf_gz : '{"name": "Annotated with rsids vcf", "type": "File", "description": "Vcf file with rsiDs added."}'
    output_annotated_with_dbsnp_rsids_vcf_gz_tbi : '{"name": "Annotated with rsids vcf tbi", "type": "File", "description": "Index for the output vcf file."}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_dbsnp

}

task vcf_anno_dbsnp {

  String task_name = "vcf_anno_dbsnp"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String chromosome
  String docker_image = "intelliseqngs/task_vcf-anno-dbsnp:1.1.0-" + chromosome

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename="no_id_provided"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  vcf_dir=$(dirname "${vcf_gz}")
  tbi_name=$(basename "${vcf_gz_tbi}")
  if [ ! -f $vcf_dir/$tbi_name ]; then
    ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
  fi

  dbsnp_database=$( ls /resources/*/*/${chromosome}*.vcf.gz )

  bcftools annotate --columns INFO --annotation $dbsnp_database ${vcf_gz} -o ${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz -O z

  tabix -p vcf ${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz


  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: "2"
    maxRetries: 2

  }

  output {
    File annotated_with_dbsnp_rsids_vcf_gz = "${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz"
    File annotated_with_dbsnp_rsids_vcf_gz_tbi = "${chromosome}-${vcf_basename}_anno-dbsnp.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

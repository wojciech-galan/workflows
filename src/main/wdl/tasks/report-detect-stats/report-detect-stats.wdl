workflow report_detect_stats_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'report_detect_stats'
    author: 'https://gitlab.com/MonikaKrzyżanowska'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_detection_stats_json: '{"name": "detection_stats_json", "type": "File", "description": "Json with detection chance for each gene genotyped by intervals"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

    output_detection_chance_report_pdf: '{"name": "detection_chance_report_pdf", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in pdf file"}'
    output_detection_chance_report_odt: '{"name": "detection_chance_report_odt", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in odt file"}'
    output_detection_chance_report_docx: '{"name": "detection_chance_report_docx", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in docx file"}'
    output_detection_chance_report_html: '{"name": "detection_chance_report_html", "type": "File", "copy": "true", "description": "Detection chance for each gene from panel report in html file"}'
    
    
  }

  call report_detect_stats

}

task report_detect_stats {

  String task_name = "report_detect_stats"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/reports:3.5.2"

  File detection_stats_json 
  String sample_id = "no_id_provided"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  /intelliseqtools/generate-report.sh --json genes=${detection_stats_json} --template /intelliseqtools/templates/detection-chance/content.jinja --name ${sample_id}_detection-chance-report

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    File detection_chance_report_pdf = "${sample_id}_detection-chance-report.pdf"
    File detection_chance_report_odt = "${sample_id}_detection-chance-report.odt"
    File detection_chance_report_docx = "${sample_id}_detection-chance-report.docx"
    File detection_chance_report_html = "${sample_id}_detection-chance-report.html"

  }

}

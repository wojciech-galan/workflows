workflow vcf_anno_func_var_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "annotation", "variant"]}'
    name: 'Variant annotation with functional data'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.1.2": "set -eo pipefail fix", "1.1.1": "tbi fix", "1.1.0": "Clinvar and mitomap update, latest dir removed"}'

    input_vcf_gz: '{"name": "Input vcf", "type": "String","extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf index", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"], "description": "Chromosome for analysis"}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "no_id_provided", "description": "Sample ID"}'

    output_annotated_with_functional_annotations_variant_level_vcf_gz :'{"name": "annotated vcf", "type": "File"}'
    output_annotated_with_functional_annotations_variant_level_vcf_gz_tbi :'{"name": "annotated vcf index", "type": "File"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_func_var

}

task vcf_anno_func_var {

  String task_name = "vcf_anno_func_var"
  String task_version = "1.1.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-func-var:1.1.0"

  File vcf_gz
  File vcf_gz_tbi
  String vcf_basename = "no_id_provided"
  String? chromosome
  Boolean chromosome_defined = defined(chromosome)
  String vcf_prefix = if chromosome_defined then chromosome + "-" + vcf_basename else vcf_basename

  command <<<

  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  all_variant_level_vcf_gz=$( ls /resources/*/*/*.vcf.gz )

  bcftools annotate \
      --columns INFO \
      --annotation $all_variant_level_vcf_gz \
       ${vcf_gz} -o ${vcf_prefix}_annotated-with-functional-annotations-variant-level.vcf.gz -O z

  tabix -p vcf ${vcf_prefix}_annotated-with-functional-annotations-variant-level.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_with_functional_annotations_variant_level_vcf_gz = "${vcf_prefix}_annotated-with-functional-annotations-variant-level.vcf.gz"
    File annotated_with_functional_annotations_variant_level_vcf_gz_tbi = "${vcf_prefix}_annotated-with-functional-annotations-variant-level.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

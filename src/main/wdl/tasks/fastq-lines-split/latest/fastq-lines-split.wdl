# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  Fastq lines split
#  *********
#
#  Info:
#  -----
#    Name: fastq_lines_split
#    Version: v0.1
#    Status: Beta
#    Last edited: 03-07-2019
#    Last edited by: Maria Paleczny
#    Author(s):
#      + Maria Paleczny <maria.paleczny@intelliseq.pl> https://gitlab.com/marysiaa
#    Maintainer(s):
#      + Maria Paleczny <maria.paleczny@intelliseq.pl> https://gitlab.com/marysiaa
#    Copyright: Copyright 2019 Intelliseq
#    Licence: All rights reserved
#
#  Description:
#  -----------
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow fastq_lines_split_workflow { call fastq_lines_split }

task fastq_lines_split {

  #@Input(name = "Fastq", desc = "Fastq file to split into fastq files by line")
  File fastq

  # Inputs with defaults
  #@Input(name = "Sample id")
  String sample_id = "p"
  #@Input(name = "Pair number", type = "enum", values = ["1", "2"])
  String pair_number = "1"

  # Tools runtime settings, paths etc.
  #@Input(name = "Maximum number of jobs in parallel", desc = "Parallel bgzipping files", min = 1, max = 100)
  String max_no_jobs_in_parallel = "24"

  # Docker image
  String docker_image = "intelliseqngs/ubuntu-toolbox:18.04_v0.5"

  # Task name and task version
  String task_name = "fastq_lines_split"
  String task_ver = "v0.1"

  command <<<

  python3 /opt/tools/split-fastq-by-lines.py ${fastq} -id ${sample_id} -n ${pair_number}
  parallel -j ${max_no_jobs_in_parallel} bgzip ::: *.fq


  # Task info #
  PARALLEL_VER=`parallel --version 2>&1 | head -1`
  PARALLEL=`echo '"parallel":"'$PARALLEL_VER',GPLv3+"'`
  PYTHON3_VER=`python3 --version | awk '{print $2}'`
  PYTHON3=`echo '"python":"'$PYTHON3_VER',OSS"'`
  UBUNTU_VER=`awk '/PRETTY_NAME/ {print $2}' /etc/*release`
  UBUNTU=`echo '"ubuntu":"'$UBUNTU_VER',GPL"'`
  echo "\""${task_name}"\""":{\"task-version\":\""${task_ver}"\","$PARALLEL","$PYTHON3","$UBUNTU"}" > task-info.json

  # Task info # <-- end
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2
    
  }

  output {
    Array[File] fastqs = glob("*.fq.gz")

    # Logs
    File stderr_log = stderr()
    File stdout_log = stdout()

    # Task info
    File task_info = "task-info.json"
  }

}

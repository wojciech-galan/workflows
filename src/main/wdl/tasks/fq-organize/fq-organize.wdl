workflow fq_organize_workflow {

  meta {
    keywords: '{"keywords": ["fastq", "strand"]}'
    name: 'fq_organize'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    description: "Separete paired-end FASTQ files into first and second strand files and split into smaller files if needed"
    changes: '{"2.0.3": "added variable memory and disk size", "2.0.2": "fixed problems with fastq.gz suffix when analysing unpaired samples", "2.0.1": "fixed problems with boolean variables, execution time optimization, fixed problems with unpaired reads", "2.0.0": "task rewritten to be able to accept broader range of possible inputs, consume more resources and better handle edge cases", "1.1.2": "bug repair", "1.1.1": "comments added"}'
    input_fastqs: '{"name": "Fastqs", "type": "Array[File]", "extension": [".fq.gz", ".fastq.gz"], "description": "Array of paired fastq files"}'
    input_paired: '{"name": "Paired", "type": "Boolean", "description": "This argument determines whether the reads are paired or not"}'
    input_number_of_reads_per_file: '{"name": "Max number of reads per fasqt file", "type": "Int", "constraints": {"min": "0", "max": "4000000"}, "default": "4000000", "description": "Reads per fq file for bwa alignment"}'

    output_fastqs_1: '{"name": "Fastqs 1", "type": "Array[File]", "copy": "True", "description": "Array of first strand fastq files"}'
    output_fastqs_2: '{"name": "Fastqs 2", "type": "Array[File]", "copy": "True", "description": "Array of second strand fastq files"}'
    output_samples_ids: '{"name": "samples_ids", "type": "Array[String]", "description": "identifiers of samples"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call fq_organize

}

task fq_organize {

  String task_name = "fq_organize"
  String task_version = "2.0.3"
  Int? index
  Int number_of_reads_per_file = 40000000
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_fq-organize:2.0.0"

  Array[File] fastqs
  Boolean paired
  Boolean split_files = true
  Array[String] possible_left_suffixes = ['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz', '1.clean.fq.gz']
  Array[String] possible_right_suffixes = ['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz', '2.clean.fq.gz']
  Array[String] possible_unpaired_suffixes = ['fq.gz', 'fastq.gz']
  Int num_of_processes = 4
  Int input_len = length(fastqs)
  Int input_size = if input_len >0 then ceil(size(fastqs[0], "GB")) else 1
  Int disk_size = 3 * input_len * input_size + 20
  String memory_string = if split_files then "3G" else "1G"

  String dollar = "$"

  command <<<
    set -e -o pipefail
    bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    function split_files {
      find ../$1 -name "*gz" | xargs -n 1 -P ${num_of_processes} bash -c 'zcat $0 | split -l ${number_of_reads_per_file} -e --additional-suffix=_1_`basename $0` --numeric-suffixes=1 --suffix-length=5'
      for f in *gz; do
        mv $f `echo $f | sed 's/^x//' | sed 's/.gz${dollar}//'`
      done
      ls | xargs -i -P ${num_of_processes} -n 1 bash -c 'gzip -4 {}'
    }
    export -f split_files

    mkdir fastqs_1
    mkdir fastqs_2
    mkdir fastqs_1_split
    mkdir fastqs_2_split

    if ${paired} ; then
      echo "Making pairs and change suffix to .fq.gz"
      python /intelliseqtools/make_pairs.py \
                    --fastqs ${sep=' ' fastqs} \
                    --outdir1 fastqs_1 \
                    --outdir2 fastqs_2 \
                    --possible_left_suffixes ${sep=' ' possible_left_suffixes} \
                    --possible_right_suffixes ${sep=' ' possible_right_suffixes}

      python /intelliseqtools/get_sample_names.py \
                    --fastqs fastqs_1/* \
                    --possible_suffixes ${sep=' ' possible_left_suffixes} > samples_ids

    else
      for f in ${sep=" " fastqs}; do
        fname=$( basename $f )
        ln $f fastqs_1/"${dollar}{fname/fastq.gz/fq.gz}" # change suffix to .fq.gz
      done
      python /intelliseqtools/get_sample_names.py \
                    --fastqs ${sep=' ' fastqs} \
                    --possible_suffixes ${sep=' ' possible_unpaired_suffixes} > samples_ids
    fi

    if ${split_files} ; then
      echo "Splitting files from fastqs_1"
      cd fastqs_1_split
       nohup bash -c 'split_files fastqs_1' &
       mypid=$!
      cd ..

      if ${paired} ; then
        echo "Splitting files from fastqs_2"
        cd fastqs_2_split
        split_files fastqs_2
        cd ..
      fi
      wait $mypid

    else
      ln fastqs_1/* fastqs_1_split
      if ${paired} ; then
        ln fastqs_2/* fastqs_2_split
      fi
    fi

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                             --task-name-with-index ${task_name_with_index} \
                                             --task-version ${task_version} \
                                             --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: memory_string
    cpu: num_of_processes
    maxRetries: 1
    disks: "local-disk " + disk_size + " LOCAL"
  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    Array[File] fastqs_1 = glob("fastqs_1_split/*.fq.gz")
    Array[File]? fastqs_2 = glob("fastqs_2_split/*.fq.gz")
    Array[String] samples_ids = read_lines("samples_ids")

  }

}

workflow vcf_varcall_metrics_workflow {

  meta {
    keywords: '{"keywords": ["variant calling", "metrics", "vcf"]}'
    name: 'Variant calling metrics'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Collects variant calling metrics'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "Vcf gz", "type": "String", "description": "Input vcf file (bgzipped)"}'
    input_vcf_gz_tbi: '{"name": "Vcf gz tbi", "type": "String", "description": "Input vcf index"}'
    input_interval_list: '{"name": "Interval list", "type": "String", "description": "Interval list (should be the same as used for variant calling)"}'
    input_ref_genome: '{"name": "Reference genome", "type": "String", "constraints": {"values": ["hg38", "grch38-no-alt"]},"description": "Reference genome"}'

    output_detail_metrics_file: '{"name": "Detail metrics file", "type": "File", "copy": "True", "description": "File with variant calling detailed metrics"}'
    output_summary_metrics_file: '{"name": "Summary metrics file", "type": "File", "copy": "True", "description": "File with variant calling summary metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_varcall_metrics

}

task vcf_varcall_metrics {

  String task_name = "vcf_varcall_metrics"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  String ref_genome = "hg38"
  String docker_image = "intelliseqngs/task_vcf-varcall-metrics-" + ref_genome + ":1.0.0"

  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no-id"
  File? interval_list
  Boolean defined_interval_list = defined(interval_list)

  String dbsnp_vcf = "/resources/broad-institute-references-hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf.gz"

  String java_options = "-Xmx6g -Xms6g"
  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   ref_genome_dict=$(ls /resources/*/*/*.dict)

   gatk --java-options "${java_options}" \
      CollectVariantCallingMetrics \
        --INPUT ${vcf_gz} \
        --DBSNP ${dbsnp_vcf} \
        --SEQUENCE_DICTIONARY $ref_genome_dict \
        --OUTPUT ${sample_id} \
        --THREAD_COUNT 8 \
        ${true='--TARGET_INTERVALS' false='' defined_interval_list} ${interval_list}

   mv ${sample_id}.variant_calling_detail_metrics ${sample_id}_variant-calling-detail-metrics
   mv ${sample_id}.variant_calling_summary_metrics ${sample_id}_variant-calling-summary-metrics

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "7G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File detail_metrics_file = "${sample_id}_variant-calling-detail-metrics"
    File summary_metrics_file = "${sample_id}_variant-calling-summary-metrics"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
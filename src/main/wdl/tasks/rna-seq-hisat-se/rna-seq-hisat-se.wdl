workflow rna_seq_hisat_se_workflow {

  meta {
    keywords: '{"keywords": ["alignment"]}'
    name: 'rna_seq_hisat_se'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Alignment with hisat2'
    changes: '{"1.0.6": "new docker image wich properly determine docker size on google cloud", "1.0.4": "update to new template, hisat2 newest version (2.2.1)"}'
    
    input_fastq_1: '{"name": "fastq_1", "type": "File", "description": "Fastq file 1 (single-end)"}'
    input_ref_genome_index: '{"name": "ref_genome_index", "type": "Array[File]", "description": "reference genome indexed"}'
    input_splicesites_file: '{"name": "splicesites_file", "type": "File", "description": "splicesites file"}'

    output_bam_file: '{"name": "bam_file", "type": "File", "copy": "True", "description": "bam file"}'
    output_bam_bai_file: '{"name": "bam_bai_file", "type": "File", "copy": "True", "description": "bam bai file"}'
    output_summary: '{"name": "summary", "type": "File", "copy": "True", "description": "summary file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call rna_seq_hisat_se

}

task rna_seq_hisat_se {

  File fastq_1
  Array[File] ref_genome_index
  File splicesites_file
  String genome_basename = "no_basename"

  Int num_cpu = 4
  String sample_id = "no_id_provided"

  String task_name = "rna_seq_hisat_se"
  String task_version = "1.0.6"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/hisat2:1.2.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir ${genome_basename}
    ln -s ${sep=" " ref_genome_index} ${genome_basename}

    hisat2 -t --known-splicesite-infile ${splicesites_file} --dta-cufflinks -x ${genome_basename}/${genome_basename} \
    --rna-strandness FR -U ${fastq_1} -p ${num_cpu} --summary-file ${sample_id}.txt \
    | samtools sort -@ ${num_cpu} -O BAM -o ${sample_id}.bam
    samtools index ${sample_id}.bam ${sample_id}.bam.bai

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: num_cpu
    maxRetries: 2

  }

  output {

    File bam_file = "${sample_id}.bam"
    File bam_bai_file = "${sample_id}.bam.bai"
    File summary = "${sample_id}.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

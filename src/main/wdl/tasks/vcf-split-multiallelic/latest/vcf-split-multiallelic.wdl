workflow vcf_split_multiallelic_workflow {

  meta {
    keywords: '{"keywords": ["vcf-normalisation", "multiallelic sites", "variants", "bcftools"]}'
    name: 'vcf_split_multiallelic'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Normalize indels mark and split multiallelic sites in vcf'
    changes: '{"latest": "no changes"}'

    input_vcf_basename: '{"name": "vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID."}'
    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file"}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
    input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]},"required": "False" , "description": ""}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "constraints": {"values": ["broad-institute-hg38", "grch38-no-alt-analysis-set"]}, "description": ""}'
    
    output_normalized_vcf_gz : '{"name": "Vcf files", "type": "Array[File]", "description": "vcf files"}'
    output_normalized_vcf_gz_tbi : '{"name": "Vcf files indexes", "type": "Array[File]", "description": "vcf files indexes"}'
  
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    
  }

  call vcf_split_multiallelic

}

task vcf_split_multiallelic {

  String task_name = "vcf_split_multiallelic"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  
  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename = "no_id_provided"
  Array[String] reference_genome = ["hg38", "Homo_sapiens_assembly38"] # another option: Array[String] reference_genome = ["grch38-no-alt", "GRCh38.no_alt_analysis_set"]  
  String docker_version = "1.0.0"

  String? chromosome # Set region to which input VCF is restricted. The chr* prefix will be added to VCF basename.", values = ["chr1" ... "chr22", "chrX", "chrY-and-the-rest"])
  Boolean chromosome_defined = defined(chromosome)
  String vcf_prefix = if chromosome_defined then chromosome + "." + vcf_basename else vcf_basename

  # During updating dockerimage to this task remeber to make version for each reference genome and apply here
  String docker_image = "intelliseqngs/task_vcf-split-multiallelic-" + reference_genome[0] + ":" +  docker_version
  # Tools runtime settings, paths etc.
  String flag_multiallelic_sites_py = "/intelliseqtools/flag-multiallelic-sites.py"
  String reference_genome_fasta = "/resources/${reference_genome[1]}.fa"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  # testing purposes zcat ${vcf_gz} | grep -v "#" | wc -l  > nr_varinats_in_input.txt
  set -e pipefail

  # Tbi could be in different directory than vcf file, the symlink below fixes it
  vcf_dir=$(dirname "${vcf_gz}")
  tbi_name=$(basename "${vcf_gz_tbi}")
  if [ ! -f $vcf_dir/$tbi_name ]; then
    ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
  fi

  zcat ${vcf_gz} \
      | bcftools norm --fasta-ref ${reference_genome_fasta} --multiallelics +any 2>> bcftools.stderr_log \
      | python3 ${flag_multiallelic_sites_py} 2>> bcftools.stderr_log \
      | bcftools norm --fasta-ref ${reference_genome_fasta} --multiallelics -any 2>> bcftools.stderr_log \
      | bgzip > ${vcf_prefix}_normalized.vcf.gz 2>> bcftools.stderr_log

  cat bcftools.stderr_log >&2

  # Bcftools norm returns exit 0 even if it has failed. The code below fixes it
  if grep -q "^\[E::" bcftools.stderr_log; then
    exit 1
  fi

  tabix -p vcf ${vcf_prefix}_normalized.vcf.gz

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2
  }

  output {

    File normalized_vcf_gz = "${vcf_prefix}_normalized.vcf.gz"
    File normalized_vcf_gz_tbi = "${vcf_prefix}_normalized.vcf.gz.tbi"

    
    # testing purposes File nr_varinats_in_input = "nr_varinats_in_input.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

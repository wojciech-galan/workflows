workflow report_bam_qc_workflow {

  meta {
    keywords: '{"keywords": ["reports"]}'
    name: 'report_bam_qc'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.1.0": "reports refactoring"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_coverage_stats_json: '{"name": "coverage_stats_json", "type": "File", "extension": [".json"]}'

    output_coverage_report_pdf: '{"name": "coverage_report_pdf", "type": "File", "copy": "true", "description": "Coverage statistics report in pdf file"}'
    output_coverage_report_odt: '{"name": "coverage_report_odt", "type": "File", "copy": "true", "description": "Coverage statistics report in odt file"}'
    output_coverage_report_docx: '{"name": "coverage_report_docx", "type": "File", "copy": "true", "description": "Coverage statistics report in docx file"}'
    output_coverage_report_html: '{"name": "coverage_report_html", "type": "File", "copy": "true", "description": "Coverage statistics report in html file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call report_bam_qc

}

task report_bam_qc {

  File coverage_stats_json
  String output_name = "bam-qc-report"
  File? sample_info_json
  String sample_id = "no_id_provided"
  Int timezoneDifference = 0
  String? analysisSpecifier
  String outputs = "outputs"

  String task_name = "report_bam_qc"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/report_bam-qc:1.0.1"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.0/after-start.sh)

   mkdir ${outputs}

    bash /intelliseqtools/prepare-sample-info-json.sh \
      --sample-info-json "${sample_info_json}" \
      --timezoneDifference 0 \
      --analysisSpecifier "no_specifier"

      /intelliseqtools/reports/templates/script/report-bam-qc.py \
        --input-template "/resources/bam-qc-template.docx" \
        --input-coverage-stats-json "${coverage_stats_json}" \
        --input-sample-info-json "sample_info.json" \
        --input-path-to-images "/resources/images" \
        --input-name ${sample_id}-${output_name} \
        --output-dir "${outputs}"

     soffice --headless \
      --convert-to pdf ${outputs}/${sample_id}-${output_name}.docx \
      --outdir ${outputs}

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.0/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File coverage_report_pdf = "${outputs}/${sample_id}-${output_name}.docx"
    File coverage_report_docx = "${outputs}/${sample_id}-${output_name}.pdf"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow vcf_anno_snpeff_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "snpEff"]}'
    name: 'vcf_anno_snpeff'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Annotates vcf with snpEff'
    changes: '{"1.2.3": "set -eo pipefail", "1.2.2": "Strange transcript skipped", "1.2.1": "Format added to ISEQ_REPORT_ANN field header description ", "1.2.0": "New snpEff 5.0c and snpEff database version (GRCh38.102), added ISEQ_REPORT_ANN annotation"}'

    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file, should have splitted multiallelic sites and normalized indels."}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "no_id_provided", "description": "sampleID"}'
    input_annotation_mode : '{"name": "Annotation mode", "type": "String", "default": "all", "description": "SnpEff annotation mode - possible values: 1) all (all transcripts added; default used in targetseq) 2) clinvar (this mode should be chosen to annotate clinvar vcf - acmg resources preparation)"}'
    input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]},"required": "False" , "description": ""}'
    input_snpeff_database : '{"name": "snpEff database name (lowercase)", "type": "String", "default": "grch38.100", "description": "Version of the Ensembl database used by snpEff"}'
    input_genes_symbol_field_name : '{"name": "Genes symbol field name", "type": "String", "description": "Name of the info field with names of all genes affected by a given variant, default is ISEQ_GENES_NAMES"}'
    input_impact_symbol_field_name : '{"name": "Impact field name", "type": "String", "description": "Name of the info field keeping highest impact a given variant is predicted by SnpEff to have, default is ISEQ_HIGHEST_IMPACT" }'
    input_report_ann_field_name : '{"name": "Report ANN field name", "type": "String", "description": "Name of the info field keeping ANN transcript that will be displayed in report, default is ISEQ_REPORT_ANN" }'
    input_impact : '{"name": "Impact", "type": "String", "default": "MODIFIER",  "constraints": {"values":  ["HIGH", "MODERATE", "LOW", "MODIFIER"]}, "description": "Minimal impact (for a gene to be listed in the ISEQ_GENES_NAMES field), default is MODIFIER"}'

    output_annotated_with_snpeff_vcf_gz : '{"name": "Annotated with snpEff vcf", "type": "File", "description": "Output vcf file with snpEff annotations added (bgzipped)."}'
    output_annotated_with_snpeff_vcf_gz_tbi : '{"name": "Annotated with snpEff vcf tbi", "type": "File", "description": "Index file for the annotated output vcf file (created with tabix)."}'
    output_snpeff_genes_txt : '{"name": "SnpEff genes", "type": "File", "description": "SnpEff genes table."}'
    output_snpeff_summary_html : '{"name": "SnpEff summary", "type": "File", "description": "SnpEff summary html table."}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_snpeff

}

task vcf_anno_snpeff {

  String task_name = "vcf_anno_snpeff"
  String task_version = "1.2.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  String snpeff_database = "grch38.102"
  String docker_image = "intelliseqngs/snpeff-"+ snpeff_database +":1.0.2"

  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename = "no_id_provided"

  String annotation_mode = "all"
  String annotation_options = if annotation_mode == "all" then "" else "-onlyProtein -strict"

  String? chromosome
  Boolean chromosome_defined = defined(chromosome)
  String vcf_prefix = if chromosome_defined then chromosome + "-" + vcf_basename else vcf_basename

  String genes_symbol_field_name = "ISEQ_GENES_NAMES"
  String impact_symbol_field_name = "ISEQ_HIGHEST_IMPACT"
  String report_ann_field_name = "ISEQ_REPORT_ANN"
  String impact = "MODIFIER"

  String annotate_snpeff_vcf_with_genes_symbols_py = "/intelliseqtools/annotate-snpeff-vcf-with-genes-symbols.py"

  String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin

  command <<<

   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   set -e -o pipefail

   snpeff_jar=$( ls /tools/*/*/snpEff/snpEff.jar )
   snpeff_database=$( echo ${snpeff_database} | sed 's/grc/GRC/' )

   zcat ${vcf_gz} | sed "s/chrM\t/chrMT\t/g" \
      | java "${snpeff_java_mem}" -jar $snpeff_jar -verbose $snpeff_database ${annotation_options} \
      | sed "s/chrMT\t/chrM\t/g" \
      | sed '/^chrM\t/ { s,WARNING_TRANSCRIPT_INCOMPLETE,,g }' > snpeff.vcf

   if grep -q "<ID=ANN" snpeff.vcf; then
      echo "File annotated with snpeff"
    else
      sed -i "s/#CHROM/##INFO=<ID=ANN,Number=.,Type=String,Description=\"Functional annotations: 'Allele | Annotation | Annotation_Impact | Gene_Name | Gene_ID | Feature_Type | Feature_ID | Transcript_BioType | Rank | HGVS.c | HGVS.p | cDNA.pos \/ cDNA.length | CDS.pos \/ CDS.length | AA.pos \/ AA.length | Distance | ERRORS \/ WARNINGS \/ INFO'\">\n#CHROM/" snpeff.vcf
      sed -i "s/#CHROM/##INFO=<ID=LOF,Number=A,Type=String,Description=\"Predicted loss of function effects for this variant. Format: 'Gene_Name | Gene_ID | Number_of_transcripts_in_gene | Percent_of_transcripts_affected'\">\n#CHROM/" snpeff.vcf
      sed -i "s/#CHROM/##INFO=<ID=NMD,Number=A,Type=String,Description=\"Predicted nonsense mediated decay effects for this variant. Format: 'Gene_Name | Gene_ID | Number_of_transcripts_in_gene | Percent_of_transcripts_affected'\">\n#CHROM/" snpeff.vcf
    fi

   cat snpeff.vcf | \
   python3 ${annotate_snpeff_vcf_with_genes_symbols_py} \
       --impact ${impact} \
       --genes-field-name ${genes_symbol_field_name} \
       --impact-field-name ${impact_symbol_field_name} \
       --report-ann-field-name ${report_ann_field_name} \
   | sed "s/INFO' /INFO'/g" \
   | sed "s/##INFO=<ID=LOF,Number=.,/##INFO=<ID=LOF,Number=A,/g" \
   | sed "s/##INFO=<ID=NMD,Number=.,/##INFO=<ID=NMD,Number=A,/g" \
   | bgzip > ${vcf_prefix}_annotated-with-snpeff.vcf.gz

    mv snpEff_genes.txt ${vcf_prefix}_snpeff-genes.txt
    mv snpEff_summary.html ${vcf_prefix}_snpeff-summary.html

   tabix ${vcf_prefix}_annotated-with-snpeff.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: "2"
    maxRetries: 2

  }

  output {

    File annotated_with_snpeff_vcf_gz = "${vcf_prefix}_annotated-with-snpeff.vcf.gz"
    File annotated_with_snpeff_vcf_gz_tbi = "${vcf_prefix}_annotated-with-snpeff.vcf.gz.tbi"
    File snpeff_genes_txt = "${vcf_prefix}_snpeff-genes.txt"
    File snpeff_summary_html = "${vcf_prefix}_snpeff-summary.html"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

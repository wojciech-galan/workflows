#### Notes:  
1. bcftools gtc2vcf plugin does not write genotypes to the output vcf file (when input gtc file is not clustered)  
2. genotypes for the unclustered vcf are added by the [genotype-gtc2vcf.py](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/tools/genotype-gtc2vcf.py) script:   
  * THETA value is checked for each variant/sample
  * meanTHETA and devTHETA values are checked for each variant and genotype type  (0/0, 0/1 and 1/1)   
  * if sample THETA value falls within meanTHETA+/-3xdevTHETA intervals for one and only one genotype at a given site, this genotype ia assigned to this sample  
  * the idea was taken from here: [freeseek/gtc2vcf](https://github.com/freeseek/gtc2vcf#plot-variants)   
3. Output vcf has 4780 replicated records with the same position and alleles, but different ids, info and format fields
   (together: 10009 lines, as some records are repeated more than once). 
   TSV mapping files (bgzipped, indexed) with `CHROM, POS, REF, ALT, primary_rsid, all_rsids` columns
   for all and repeated position/alt_allele pairs are located on anakin:  
   ```bash
   ### all position/alt pairs:
   /data/public/prodia/ids-mapping.tsv.gz
   /data/public/prodia/ids-mapping.tsv.gz.tbi
   
   ## subset containing only repeated variant info:
   /data/public/prodia/repeated-variants-ids-mapping.tsv.gz
   /data/public/prodia/repeated-variants-ids-mapping.tsv.gz.tbi
      ```     
   Note that in the above files variant id that was added to imputing panel is given in the 5th column; all equivalent ids 
   are given in the 6th column (separated with `;`).  
   
---
#### Test1  
#### Comparison of genotypes obtained from the clustered and unclustered gtc files  
Clustered and unclustered gtc files were converted to vcf files (bcftools +gtc2vcf) with the same manifest/cluster files and `--adjust-clusters` option used.    
paths on anakin:
```bash
### gtc files
/data/test/large-data/gtc/clustered_204800980122_R01C02.gtc  
/data/test/large-data/gtc/unclustered_204800980122_R01C02.gtc  

### manifest and cluster files  
/data/public/prodia/ASA-24v1-0_A1_ClusterFile.egt  
/data/public/prodia/ASA-24v1-0_A2.bpm  
/data/public/prodia/ASA-24v1-0_A2.csv
```
Vcf file generated from the clustered gtc file by the gtc2vcf software was already genotyped.    
Vcf file generated from the unclustered gtc lacked genotypes after the conversion step and was genotyped with our python script.    

#### Results:
1. For both gtc files:
   * 599 ids were removed during gtc to vcf conversion (lacking info in `Chr`  and `MapInfo` columns in the `ASA-24v1-0_A2.csv` manifest file), 
     [ids list](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/wdl/tasks/gtc-to-vcf/unmapped-ids.txt)
   * 1 variant was removed during indel normalization step (not matching the reference): `11:118939941`   
2. For 12122 from 658584  sites (1.84%) genotype fields were different for vcfs obtained from the clustered and unclustered gtc files, of those:  
 * in 9620 cases our script failed to assign genotype to variant that was genotyped in the clustered vcf   
 * in 2475 cases our script assigned genotype for variant that lacked genotype in the clustered vcf  
 * in 27 cases assigned genotypes differed between both vcfs  

---  
#### Test2  
##### Checking consistency of genotypes obtained for the same variant with different probes (for the unclustered gtc file)  
Lines with information on repeated variants were extracted from the vcf file (10009 lines/probes, with 4780 markers)  
and filtered to keep only variants having assigned non-missing 
genotype at least twice (9672 lines/probes and 4625 variants). Of those only 12 variants had inconsistent genotypes
(different values obtained for different probes):

| CHROM | POS       | REF | ALT | IDS                                                                                           | GT                |
|:-----:|:---------:|:---:|:---:|:---------------------------------------------------------------------------------------------:|:-----------------:|
| chr13 | 102222124 | C   | T   | rs9300720<br>13:102874474                                                                     | 0/0<br>1/1        |
| chr19 | 40843708  | T   | C   | 19:41349613_CNV_CYP2A6<br>19:41349613_CNV_CYP2A6_Ilmndup1                                     | 0/0<br>0/1        |
| chr19 | 40843740  | T   | C   | 19:41349645_CNV_CYP2A6<br>19:41349645_CNV_CYP2A6_Ilmndup1<br>19:41349645_CNV_CYP2A6_Ilmndup2  | 0/0<br>0/1<br>0/0 |
| chr19 | 40878769  | G   | T   | 19:41384674_CNV_CYP2A7<br>19:41384674_CNV_CYP2A7_Ilmndup1<br>19:41384674_CNV_CYP2A7_Ilmndup2  | 0/0<br>0/1<br>0/1 |
| chr19 | 40879870  | C   | A   | 19:41385775_CNV_CYP2A7<br>19:41385775_CNV_CYP2A7_Ilmndup1<br>19:41385775_CNV_CYP2A7_Ilmndup2  | ./.<br>1/1<br>0/1 |
| chr19 | 40880516  | C   | A   | 19:41386421_CNV_CYP2A7_Ilmndup2<br>19:41386421_CNV_CYP2A7<br>19:41386421_CNV_CYP2A7_Ilmndup1  | 0/1<br>./.<br>0/0 |
| chr19 | 40880580  | G   | T   | 19:41386485_CNV_CYP2A7<br>19:41386485_CNV_CYP2A7_Ilmndup2<br>19:41386485_CNV_CYP2A7_Ilmndup1  | 0/1<br>./.<br>1/1 |
| chr19 | 40880590  | G   | T   | 19:41386495_CNV_CYP2A7<br>19:41386495_CNV_CYP2A7_Ilmndup1<br>19:41386495_CNV_CYP2A7_Ilmndup2  | 0/0<br>0/1<br>0/1 |
| chr19 | 40992603  | T   | C   | 19:41498508_CNV_CYP2B6<br>19:41498508_CNV_CYP2B6_Ilmndup2<br>19:41498508_CNV_CYP2B6_Ilmndup1  | 0/0<br>1/1<br>1/1 |
| chr22 | 23958448  | G   | T   | 22:24300635_CNV_GSTT2B<br>22:24300635_CNV_GSTT2B_Ilmndup2<br>22:24300635_CNV_GSTT2B_Ilmndup3  | 0/1<br>./.<br>1/1 |
| chr22 | 42126138  | T   | G   | 22:42522143_CNV_CYP2D6<br>22:42522143_CNV_CYP2D6_Ilmndup1<br>22:42522143_CNV_CYP2D6_Ilmndup2  | ./.<br>0/1<br>0/0 |
| chrX  | 6302433   | T   | C   | rs7885458<br>exm-rs7885458                                                                    | 0/0<br>1/1        |

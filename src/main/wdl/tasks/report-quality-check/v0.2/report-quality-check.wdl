workflow report_quality_check_workflow {

  meta {
    name: 'report_quality_check'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## report_variants_from_vcf \n Generic text for task'
    changes: '{latest: "no changes"}'

    input_qc_1_jsons: '{name: "qc_1_jsons", type: "Array[File]",  *constraints: {extension: ["json"]}}'
    input_qc_2_jsons: '{name: "qc_2_jsons", type: "Array[File]",  *constraints: {extension: ["json"]}}'

    output_quality_check_report_pdf: '{name: "quality_check_report_pdf", type: "File", copy: "True", description: "Report of quality check results in pdf file", *constraints: {extension: ["pdf"]}}'
    output_quality_check_report_odt: '{name: "quality_check_report_odt", type: "File", copy: "True", description: "Report of quality check results in odt file", *constraints: {extension: ["odt"]}}'
    output_quality_check_report_docx: '{name: "quality_check_report_docx", type: "File", copy: "True", description: "Report of quality check results in docx file", *constraints: {extension: ["docx"]}}'
    output_stdout_log: '{name: "Standard out", type: "File", copy: "True", description: "Console output"}'
    output_stderr_err: '{name: "Standard err", type: "File", copy: "True", description: "Console stderr"}'
    output_bco: '{name: "Biocompute object", type: "File", copy: "True", description: "Biocompute object"}'
  }

  call report_quality_check

}

task report_quality_check {


  Array[File] qc_1_jsons
  Array[File] qc_2_jsons

  String output_name = "report-quality-check"

  String task_name = "report_quality_check"
  String task_version = "latest"
  String docker_image = "intelliseqngs/reports:v0.3"

  command <<<
  task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/after-start.sh)

  printf "{\
    \"task-name\":\"${task_name}\",\
    \"task-version\":\"${task_version}\",\
    \"docker-image\":\"${docker_image}\",\
    \"resources\":$RESOURCES,\
    \"tools\":$TOOLS\
    }" | sed 's/ //g' > bco.json


  echo "[" > qc_1.json
  for file in ${sep=' ' qc_1_jsons}  ; do
      echo "{\"filename\":\""$(basename $file)"\","  >> qc_1.json
      echo "\"QC\":" >> qc_1.json
      cat $file >> qc_1.json
      echo "}," >> qc_1.json
  done
  sed -i '$ s/.$//' qc_1.json
  echo "]" >> qc_1.json

  echo "[" > qc_2.json
  for file in ${sep=' ' qc_2_jsons}  ; do
      echo "{\"filename\":\""$(basename $file)"\","  >> qc_2.json
      echo "\"QC\":" >> qc_2.json
      cat $file >> qc_2.json
      echo "}," >> qc_2.json
  done
  sed -i '$ s/.$//' qc_2.json
  echo "]" >> qc_2.json


  /opt/tools/generate-report.sh --json qc1=qc_1.json,qc2=qc_2.json --template /opt/tools/templates/qc-template-v1/content.xml --name ${output_name}


  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/before-finish.sh)
  >>>

  runtime {

    maxRetries: 3
    docker: docker_image
    memory: "1G"
    cpu: "1"

  }

  output {

    File quality_check_report_pdf = "${output_name}.pdf"
    File quality_check_report_odt = "${output_name}.odt"
    File quality_check_report_docx = "${output_name}.docx"
    File quality_check_report_html = "${output_name}.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow report_quality_check_workflow {

  meta {
    keywords: '{"keywords": ["quality check report"]}'
    name: 'report_quality_check'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Report variants from vcf \n'
    changes: '{"latest": "no changes"}'

    input_qc_1_jsons: '{"name": "qc_1_jsons", "type": "Array[File]", "extension": [".json"]}'
    input_qc_2_jsons: '{"name": "qc_2_jsons", "type": "Array[File]", "extension": [".json"]}'

    output_quality_check_report_pdf: '{"name": "quality check report pdf", "type": "File", "copy": "True", "description": "Report of quality check results in pdf file"}'
    output_quality_check_report_odt: '{"name": "quality check report odt", "type": "File", "copy": "True", "description": "Report of quality check results in odt file"}'
    output_quality_check_report_docx: '{"name": "quality check_report_docx", "type": "File", "copy": "True", "description": "Report of quality check results in docx file"}'
    output_quality_check_report_html: '{"name": "quality check report html", "type": "File", "copy": "True", "description": "Report of quality check results in html file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call report_quality_check

}

task report_quality_check {


  Array[File] qc_1_jsons
  Array[File] qc_2_jsons
  String sample_id = "no_id_provided"

  String output_name = "report_quality_check"

  String task_name = "report_quality_check"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String task_version = "latest"
  String docker_image = "intelliseqngs/reports:1.0.0"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/after-start.sh)

  echo "[" > qc_1.json
  for file in ${sep=' ' qc_1_jsons}  ; do
      echo "{\"filename\":\""$(basename $file)"\","  >> qc_1.json
      echo "\"QC\":" >> qc_1.json
      cat $file >> qc_1.json
      echo "}," >> qc_1.json
  done
  sed -i '$ s/.$//' qc_1.json
  echo "]" >> qc_1.json

  echo "[" > qc_2.json
  for file in ${sep=' ' qc_2_jsons}  ; do
      echo "{\"filename\":\""$(basename $file)"\","  >> qc_2.json
      echo "\"QC\":" >> qc_2.json
      cat $file >> qc_2.json
      echo "}," >> qc_2.json
  done
  sed -i '$ s/.$//' qc_2.json
  echo "]" >> qc_2.json


  /opt/tools/generate-report.sh --json qc1=qc_1.json,qc2=qc_2.json --template /opt/tools/templates/qc-template-v2/content.jinja --name ${sample_id}_${output_name}

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File quality_check_report_pdf = "${sample_id}_${output_name}.pdf"
    File quality_check_report_odt = "${sample_id}_${output_name}.odt"
    File quality_check_report_docx = "${sample_id}_${output_name}.docx"
    File quality_check_report_html = "${sample_id}_${output_name}.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}


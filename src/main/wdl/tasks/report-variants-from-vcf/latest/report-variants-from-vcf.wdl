workflow report_variants_from_vcf_workflow {

  meta {
    name: 'report_variants_from_vcf'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## report_variants_from_vcf \n Generic text for task'
    changes: '{"latest": "no changes"}'

    input_variants_pathogenic: '{"name": "variants_pathogenic", "type": "File", "extension": [".json"], "description": "Json file with pathogenic variants"}'
    input_variants_likely_pathogenic: '{"name": "variants_likely_pathogenic", "type": "File", "extension": [".json"], "description": "Json file with likely pathogenic variants"}'
    input_variants_uncertain: '{"name": "variants_uncertain", "type": "File", "extension": [".json"], "description": "Json file with uncertain variants"}'
    input_patient_json: '{"name": "patient_json", "type": "File", "extension": [".json"], "description": "Patient data in json format, must include attributes (even if without values): name, surname, sex, birthdate and pesel", "required": false}'
    input_panel_json: '{"name": "panel_json", "type": "File", "extension": [".json"], "description": "Panel data in json format, must include attributes (even if without values): genes_number, genes", "required": false}'
    input_sample_json: '{"name": "sample_json", "type": "File", "extension": [".json"], "description": "Patient data in json format, must include attributes (even if without values): ID, material, sequencing_type, sequencing_platform, sending_date, raport_date, doctor_name, doctor_surname", "required": false}'

    output_variants_report_pdf: '{"name": "variants_report_pdf", "type": "File", "copy": "True", "description": "Report of pathogenic, likely pathogenic and uncertain variants in pdf file"}'
    output_variants_report_odt: '{"name": "variants_report_odt", "type": "File", "copy": "True", "description": "Report of pathogenic, likely pathogenic and uncertain variants in odt file"}'
    output_variants_report_docx: '{"name": "variants_report_docx", "type": "File", "copy": "True", "description": "Report of pathogenic, likely pathogenic and uncertain variants in docx file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Console output"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call report_variants_from_vcf

}

task report_variants_from_vcf {

  File variants_pathogenic
  File variants_likely_pathogenic
  File variants_uncertain

  File? patient_json
  File? sample_json
  File? panel_json
  File? phenotypes_json
  File? pictures_gz
  String output_name = "report-variants-from-vcf"
  String language = "pl"
  String sample_id = "no_id_provided"

  String task_name = "report_variants_from_vcf"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/reports:2.0.1"
  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/after-start.sh)

  # Create empty json files as input to generate-report.sh if were not provided in wdl inputs.
  [ -z "${panel_json}" ] && echo "{}" > panel.json || cat ${panel_json} > panel.json
  [ -z "${patient_json}" ] && echo "{}" > patient.json || cat ${patient_json} > patient.json
  [ -z "${sample_json}" ] && echo "{}" > sample.json || cat ${sample_json} > sample.json
  [ -z "${phenotypes_json}" ] && echo "{}" > phenotypes.json || cat ${phenotypes_json} > phenotypes.json

  # Prepare picture.json with paths to png files
  mkdir pictures
  if [ -z "${pictures_gz}" ] ; then
    echo '{}' > picture.json
    echo '[]' > picture-generate-report-sh.json
  else
    tar xvzf ${pictures_gz} -C pictures

      touch picture.txt
      echo "[" > picture-generate-report-sh.json
      for png in pictures/*.png
      do
        # Create png_name extracting chr and pos from png file name (png_name="chr15-48497301")
        png_name=$(echo $png | sed 's/.*\(chr.*-[0-9]*\)-igv-screenshot.png/\1/');
        # Get the path to png file
        png_path=$(realpath $png);
        png_basename=$(basename $png);
        # Save png_name and path to picture.txt ({"chr15-48497301":"/home/test/test/Pictures/test_chr15-48497301-igv-screenshot.png"})
        echo \"$png_name\":\"$png_basename\" >> picture.txt
        echo "{\"path\" : \""$png_path"\"},">> picture-generate-report-sh.json
      done

      # Replace new lines with commas, add {}
      cat picture.txt | tr "\n" "," | sed 's/^"/{"/' | sed 's/",$/"}/' > picture.json

      sed -i '$ s/.$//' picture-generate-report-sh.json
      echo "]" >> picture-generate-report-sh.json
  fi

  jq -c '[.[] | select((."type"=="phenotype") or (."type"=="user"))]' panel.json > panel_phenotypes.json
  jq -c '[.[] | select((."type"!="phenotype") and (."type"!="user"))]' panel.json > panel_diseases.json

  bash /intelliseqtools/generate-report.sh \
  --json patient=patient.json \
  --json phenotypes=phenotypes.json \
  --json panel_phenotypes=panel_phenotypes.json \
  --json panel_diseases=panel_diseases.json \
  --json sample=sample.json \
  --json variants_uncertain=${variants_uncertain} \
  --json variants_pathogenic=${variants_pathogenic} \
  --json variants_likely_pathogenic=${variants_likely_pathogenic} \
  --json dict=/intelliseqtools/templates/report-${language}-v1/dict.json \
  --json picture=picture.json \
  --template /intelliseqtools/templates/report-${language}-v1/content.jinja \
  --name ${sample_id}_${language}-${output_name} \
  --pictures picture-generate-report-sh.json

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.0/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File variants_report_pdf = "${sample_id}_${language}-${output_name}.pdf"
    File variants_report_odt = "${sample_id}_${language}-${output_name}.odt"
    File variants_report_docx = "${sample_id}_${language}-${output_name}.docx"
    File variants_report_html = "${sample_id}_${language}-${output_name}.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

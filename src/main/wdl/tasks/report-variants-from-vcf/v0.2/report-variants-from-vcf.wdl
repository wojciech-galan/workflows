workflow report_variants_from_vcf_workflow {

  meta {
    name: 'report_variants_from_vcf'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## report_variants_from_vcf \n Generic text for task'
    changes: '{latest: "no changes"}'

    input_vcf_gz: '{name: "vcf_gz", type: "File",  *constraints: {extension: ["vcf.gz"]}}'
    input_patient_json: '{name: "patient_json", type: "File",  *constraints: {extension: ["json"]}, *description: "Patient data in json format, must include attributes (even if without values): name, surname, sex, birthdate and pesel"}'
    input_panel_json: '{name: "panel_json", type: "File",  *constraints: {extension: ["json"]}, *description: "Panel data in json format, must include attributes (even if without values): genes_number, genes"}'
    input_sample_json: '{name: "sample_json", type: "File",  *constraints: {extension: ["json"]}, *description: "Patient data in json format, must include attributes (even if without values): ID, material, sequencing_type, sequencing_platform, sending_date, raport_date, doctor_name "}'

    output_variants_report_pdf: '{name: "variants_report_pdf", type: "File", copy: "True", description: "Report of pathogenic, likely pathogenic and uncertain variants in pdf file", *constraints: {extension: ["pdf"]}}'
    output_variants_report_odt: '{name: "variants_report_odt", type: "File", copy: "True", description: "Report of pathogenic, likely pathogenic and uncertain variants in odt file", *constraints: {extension: ["odt"]}}'
    output_variants_report_docx: '{name: "variants_report_docx", type: "File", copy: "True", description: "Report of pathogenic, likely pathogenic and uncertain variants in docx file", *constraints: {extension: ["docx"]}}'
    output_stdout_log: '{name: "Standard out", type: "File", copy: "True", description: "Console output"}'
    output_stderr_err: '{name: "Standard err", type: "File", copy: "True", description: "Console stderr"}'
    output_bco: '{name: "Biocompute object", type: "File", copy: "True", description: "Biocompute object"}'
  }

  call report_variants_from_vcf

}

task report_variants_from_vcf {

  File vcf_gz

  File? patient_json
  File? panel_json
  File? sample_json
  File? phenotypes_json
  String output_name = "report-variants-from-vcf"

  String task_name = "report_variants_from_vcf"
  String task_version = "latest"
  String docker_image = "intelliseqngs/reports:v0.3"

  command <<<
  [ -z "${panel_json}" ] && echo "{}" > panel.json || cat ${panel_json} > panel.json
  [ -z "${patient_json}" ] && echo "{}" > patient.json || cat ${patient_json} > patient.json
  [ -z "${sample_json}" ] && echo "{}" > sample.json || cat ${sample_json} > sample.json
  [ -z "${phenotypes_json}" ] && echo "{}" > phenotypes.json || cat ${phenotypes_json} > phenotypes.json

  task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/after-start.sh)

  printf "{\
    \"task-name\":\"${task_name}\",\
    \"task-version\":\"${task_version}\",\
    \"docker-image\":\"${docker_image}\",\
    \"resources\":$RESOURCES,\
    \"tools\":$TOOLS\
    }" | sed 's/ //g' > bco.json


  zcat -f ${vcf_gz} | /opt/tools/vcftojson.py > variants.json
  /opt/tools/split-variants-by-acmg-classification.py variants.json
  touch list-of-positions

  for i in pathogenic likely-pathogenic uncertain
  do
    sed 's/,]/]/g' variants-$i-1.json | jq -c 'sort_by(.ISEQ_ACMG_SUMMARY_SCORE|tonumber)|reverse' > variants-$i.json
    jq '.[] | .CHROM , .POS , .ISEQ_GENES_NAMES' variants-$i.json | sed 's/\"//g' | paste -d ' ' - - - >> list-of-positions
  done

  jq -c '[.[] | select((.type=="phenotype") or (.type=="user"))]' panel.json > panel_phenotypes.json
  jq -c '[.[] | select((.type!="phenotype") and (.type!="user"))]' panel.json > panel_diseases.json


  /opt/tools/generate-report.sh --json patient=patient.json,phenotypes=phenotypes.json,panel_phenotypes=panel_phenotypes.json,panel_diseases=panel_diseases.json,sample=sample.json,variants_uncertain=variants-uncertain.json,variants_pathogenic=variants-pathogenic.json,variants_likely_pathogenic=variants-likely-pathogenic.json,dict=/opt/tools/templates/report-pl-v1/dict.json --template /opt/tools/templates/report-pl-v1/content.xml --name ${output_name}


  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/before-finish.sh)
  >>>

  runtime {

    maxRetries: 3
    docker: docker_image
    memory: "1G"
    cpu: "1"

  }

  output {

    File variants_report_pdf = "${output_name}.pdf"
    File variants_report_odt = "${output_name}.odt"
    File variants_report_docx = "${output_name}.docx"
    File variants_report_html = "${output_name}.html"
    File list_of_positions = "list-of-positions"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

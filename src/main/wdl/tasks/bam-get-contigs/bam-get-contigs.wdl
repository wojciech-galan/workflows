workflow bam_get_contigs_workflow {

  meta {
    keywords: '{"keywords": ["bam", "contigs", "gsort input"]}'
    name: 'get_contigs'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Extracts contigs names and lenghts from indexed bam or vcf file; creates contig list input for gsort'
    changes: '{"latest": "no changes"}'

    input_bam: '{"name": "Bam", "type": "File", "description": "Bam file"}'
    input_bai: '{"name": "Bai", "type": "File", "description": "Bam index file"}'

    output_contig_tab: '{"name": "Contig tab", "type": "File", "copy": "True", "description": "Tab deliminated file with contigs names and lengths"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_get_contigs

}

task bam_get_contigs {

  String task_name = "bam_get_contigs"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.3"

  File bam
  File bai
  String basename = basename(bam, ".bam")

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   samtools view -H ${bam} | grep '^@SQ' | cut -f 2,3 | sed 's/SN://' | sed 's/LN://' > ${basename}-contig.tab

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File contig_tab = "${basename}-contig.tab"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

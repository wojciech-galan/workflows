workflow rna_seq_concat_summary_se_workflow {

  meta {
    keywords: '{"keywords": ["concatenate"]}'
    name: 'rna_seq_concat_summary_se'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Concatenate summary files from hisat2 (single-end)'
    changes: '{"1.0.3": "update to new template"}'

    input_summary_files: '{"name": "summary_files", "type": "Array[File]", "description": "summary files (single-end)"}'

    output_summary_excel_file: '{"name": "summary_excel_file", "type": "File", "copy": "true", "description": "summary file in Excel"}'
    output_summary_csv_file: '{"name": "summary_csv_file", "type": "File", "copy": "true", "description": "summary file in CSV"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_concat_summary_se

}

task rna_seq_concat_summary_se {

  Array[File] summary_files
  String summary_file_name = "summary"

  String task_name = "rna_seq_concat_summary_se"
  String task_version = "1.0.3"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-concat-summary-se:1.1.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir hisat_summary_files
    ln -s ${sep=" " summary_files} hisat_summary_files

    python3 /intelliseqtools/concat-summary-files-single-end-rna-seq.py --input-path-to-summary-files "hisat_summary_files" \
                                                                        --output-summary-file-name ${summary_file_name}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File summary_excel_file = "hisat_summary_files/${summary_file_name}.xlsx"
    File summary_csv_file = "hisat_summary_files/${summary_file_name}.csv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

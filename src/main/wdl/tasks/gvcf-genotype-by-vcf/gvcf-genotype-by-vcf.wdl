workflow gvcf_genotype_by_vcf_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'gvcf_genotype_by_vcf'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{"2.0.2": "added bcftools error control", "2.0.1": "interval_bed input added","2.0.0": "new docker image, task fails if any of the commands fail, removed genotyping_databese variable"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_sample_gvcf_gz: '{"name": "sample_gvcf_gz", "type": "File", "extension": [".gvcf.gz", ".g.vcf.gz"], "description": "Sample data"}'
    input_sample_gvcf_gz_tbi: '{"name": "sample_gvcf_gz_tbi", "type": "File", "extension": [".g.vcf.gz.tbi", ".gvcf.gz.tbi"], "description": "Index for sample data"}'
    input_interval_vcf_gz: '{"name": "interval_vcf_gz", "type": "File", "description": "Interval file with annotations info and loci to genotype", "extension": [".vcf.gz"]}'
    input_interval_vcf_gz_tbi: '{"name": "interval_vcf_gz_tbi", "type": "File", "description": "Index for interval_vcf_gz", "extension": [".vcf.gz.tbi"]}'
    input_interval_bed_gz: '{"name": "interval_bed_gz", "type": "File", "description": "Bed interval file - optional.", "extension": [".bed.gz"], "required": false}'
    input_interval_bed_gz_tbi: '{"name": "interval_bed_gz_tbi", "type": "File", "description": "Index for interval_bed_gz.", "extention": [".bed.gz.tbi"], "required": false}'

    output_genotyped_vcf_gz: '{"name": "genotyped_vcf_gz", "type": "File", "copy": "True", "description": "Vcf genotyped by interval vcf"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call gvcf_genotype_by_vcf

}

task gvcf_genotype_by_vcf {

  String task_name = "gvcf_genotype_by_vcf"
  String task_version = "2.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0-hg38:1.0.1"
  String genotype_gvcfs_java_options = "-Xmx5g -Xms5g"
  String gatk_path = "/gatk/gatk"

  String sample_id = "no_id_provided"

  # vcf used for genotyping (if no bed provided) and merging (for example clinvar)
  File interval_vcf_gz
  File interval_vcf_gz_tbi

  # bed used for genotyping
  File? interval_bed_gz
  File? interval_bed_gz_tbi

  File intervals = select_first([interval_bed_gz, interval_vcf_gz])

  Boolean merge_annotations = true

  # sample gvcf
  File sample_gvcf_gz
  File sample_gvcf_gz_tbi

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  # 1. GENOTYPING sample_gvcf_gz WITH interval_bed_gz

  ${gatk_path} --java-options "${genotype_gvcfs_java_options}" \
      GenotypeGVCFs \
      --allow-old-rms-mapping-quality-annotation-data \
      --lenient \
      --include-non-variant-sites \
      --intervals ${intervals} \
      --variant ${sample_gvcf_gz} \
      --output genotyped-sample.vcf.gz \
      --reference /resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa


  # 2. MERGING interval_vcf_gz WITH genotyped-sample.vcf.gz (TO CONTAIN ISEQ_CLINVAR_GENE_INFO)

  if [ "${merge_annotations}" = true ]; then
    bcftools merge genotyped-sample.vcf.gz ${interval_vcf_gz} | bgzip > temp.vcf.gz  2>> bcftools.stderr_log


  # 3. REMOVING irrelevant sample from interval_vcf_gz (syntetic_homo_sample IN CLINVAR)

    sample_name=$(bcftools query -l ${sample_gvcf_gz})
    bcftools view -s $sample_name temp.vcf.gz | bgzip > ${sample_id}_genotyped-by-vcf.vcf.gz  2>> bcftools.stderr_log
  else
    cp genotyped-sample.vcf.gz ${sample_id}_genotyped-by-vcf.vcf.gz

  fi


  cat bcftools.stderr_log >&2

  if grep -q "^\[E::" bcftools.stderr_log; then
    exit 1
  fi


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "6G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File genotyped_vcf_gz = "${sample_id}_genotyped-by-vcf.vcf.gz"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

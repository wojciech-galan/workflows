workflow vcf_anno_func_gene_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "annotations", "variant", "clinvar", "HPO"]}'
    name: 'annotate_vcf_with_functional_annotations_variant_level'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## annotate_vcf_with_functional_annotations_variant_level \n Annotates VCF with variant level annotation from the following databases:\n  - ClinVar \n - MITOMAP \n   More information about databases used can be found in documentation of the following docker images: intelliseqngs/vcf-annotation:functional-annotations-variant-level-grch38_v0.1'
    changes: '{"1.2.2":"resources fix","1.2.1": "set-e -o pipefail fix", "1.2.0": "latest dir removed, resources update"}'

    input_vcf_gz: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz"], "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf", "type": "String", "extension": [".vcf.gz.tbi"], "description": "Index for vcf file to be annotated"}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"], "description": "Chromosome for analysis"}'
    input_vcf_basename : '{"name": "Vcf basename", "type": "String", "default": "noidprovided", "description": "Sample ID"}'

    output_annotated_with_functional_annotations_variant_level_vcf_gz : '{"name": "annotated vcf", "type": "File", "copy": "True", "description": "Annotated vcf"}'
    output_annotated_with_functional_annotations_variant_level_vcf_gz_tbi : '{"name": "annotared vcf index","type": "File", "copy": "True", "description": "Vcf index"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_anno_func_gene

}

task vcf_anno_func_gene {

  String task_name = "vcf_anno_func_gene"
  String task_version = "1.2.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-func-gene:1.1.1"


  # Input VCF file must: be bgzipped, have left-normalized indels (bcftools norm), multiallelic sites split (bcftools norm --multiallelics -any)")
  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename="no_id_provided"

  String? chromosome
  Boolean chromosome_defined = defined(chromosome)
  String vcf_prefix = if chromosome_defined then chromosome + "_" + vcf_basename else vcf_basename

  
# Tools runtime settings, paths etc.
  String clinvar_diseases_gene_level_dictionary = "/resources/clinvar-diseases.dictionary"
  String hpo_gene_symbol_to_diseases_dictionary = "/resources/gene-symbol-to-diseases.dictionary"
  String hpo_gene_symbol_to_phenotypes_dictionary = "/resources/gene-symbol-to-phenotypes.dictionary"
  String hpo_gene_symbol_to_modes_of_inheritance_dictionary = "/resources/gene-symbol-to-modes-of-inheritance.dictionary"

  String genes_symbol_field_name = "ISEQ_GENES_NAMES"
  String clinvar_diseases_gene_level_field_name = "ISEQ_CLINVAR_DISEASES"
  String hpo_gene_symbol_to_diseases_field_name = "ISEQ_HPO_DISEASES"
  String hpo_gene_symbol_to_phenotypes_field_name = "ISEQ_HPO_PHENOTYPES"
  String hpo_gene_symbol_to_modes_of_inheritance_field_name = "ISEQ_HPO_INHERITANCE"

  String clinvar_diseases_gene_level_description = "Names of diseases associated with genes that overlap the allele (source: ClinVar). Names of diseases associated with a given gene are '^' - delimited. Grups of diseases associated with genes are ':' - delimited. The order of those groups is the same as the order of genes in " + genes_symbol_field_name + " field."
  String hpo_gene_symbol_to_diseases_description = "Names of diseases associated with genes that overlap the allele (source: Human Phenotye Ontology (HPO)). Names of diseases associated with a given gene are '^' - delimited. Grups of diseases associated with genes are ':' - delimited. The order of those groups is the same as the order of genes in " + genes_symbol_field_name + " field."
  String hpo_gene_symbol_to_phenotypes_description = "Names of phenotypes associated with genes that overlap the allele (source: Human Phenotye Ontology (HPO)). Phenotypes associated with a given gene are '^' - delimited. Grups of phenotypes associated with genes are ':' - delimited. The order of those groups is the same as the order of genes in " + genes_symbol_field_name + " field."
  String hpo_gene_symbol_to_modes_of_inheritance_description = "Modes of inheritance associated with genes that overlap the allele (source: Human Phenotye Ontology (HPO)). Modes of inheritance associated with a given gene are '^' - delimited. Grups of modes of inheritance associated with genes are ':' - delimited. The order of those groups is the same as the order of genes in " + genes_symbol_field_name + " field."

  String annotate_vcf_with_a_dictionary_py = "/intellisetools/annotate-vcf-with-a-dictionary.py"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  #    IMPORTANT NOTES:
  #     1. The input bgzipped VCF must have:
  #          - normalized indels (bcftools norm)
  #          - split multiallelic sites (bcftools norm --multiallelics -any)
  #        If these requirments are not met, some variants may not be annotated correctly. Normalization and multiallelic sites splitting can be done with the use of normalize-indels-mark-and-split-multiallelic-sites-in-vcf task.
  #     2. The task outputs bgzipped VCF with multiallelic sites split. It is recommened to normalize indels and collapse multiallelic sites with normalize-and-collapse-multiallelic-sites-in-vcf task on the final annotated and/or filtered VCF file.

    # ClinVar - diseases #
    CLINVAR_DATABASE_VERSION="$(cat ${clinvar_diseases_gene_level_dictionary} | grep '##INFO=<ID=${clinvar_diseases_gene_level_field_name},' | grep -o -P 'Version=".*?"')"
    CLINVAR_DISEASES_INFO_HEADER='##INFO=<ID=${clinvar_diseases_gene_level_field_name},Number=A,Type=String,Description="'"${clinvar_diseases_gene_level_description}"'",Source="ClinVar",'"$CLINVAR_DATABASE_VERSION"'>'
    # HPO - diseases #
    HPO_DATABASE_VERSION="$(cat ${hpo_gene_symbol_to_diseases_dictionary} | grep '##INFO=<ID=${hpo_gene_symbol_to_diseases_field_name},' | grep -o -P 'Version=".*?"')"
    HPO_DISEASES_HEADER='##INFO=<ID=${hpo_gene_symbol_to_diseases_field_name},Number=A,Type=String,Description="'"${hpo_gene_symbol_to_diseases_description}"'",Source="HPO",'"$HPO_DATABASE_VERSION"'>'

    # HPO - phenotypes #
    HPO_DATABASE_VERSION="$(cat ${hpo_gene_symbol_to_phenotypes_dictionary} | grep '##INFO=<ID=${hpo_gene_symbol_to_phenotypes_field_name},' | grep -o -P 'Version=".*?"')"
    HPO_PHENOTYPES_HEADER='##INFO=<ID=${hpo_gene_symbol_to_phenotypes_field_name},Number=A,Type=String,Description="'"${hpo_gene_symbol_to_phenotypes_description}"'",Source="HPO",'"$HPO_DATABASE_VERSION"'>'

    # HPO - inheritance #
    HPO_DATABASE_VERSION="$(cat ${hpo_gene_symbol_to_modes_of_inheritance_dictionary} | grep '##INFO=<ID=${hpo_gene_symbol_to_modes_of_inheritance_field_name},' | grep -o -P 'Version=".*?"')"
    HPO_INHERITANCE_HEADER='##INFO=<ID=${hpo_gene_symbol_to_modes_of_inheritance_field_name},Number=A,Type=String,Description="'"${hpo_gene_symbol_to_modes_of_inheritance_description}"'",Source="HPO",'"$HPO_DATABASE_VERSION"'>'

    zcat ${vcf_gz} \
      | python3 ${annotate_vcf_with_a_dictionary_py} \
      ${clinvar_diseases_gene_level_dictionary} \
      ${genes_symbol_field_name} \
      ${clinvar_diseases_gene_level_field_name} \
      "$CLINVAR_DISEASES_INFO_HEADER" \
        | python3 ${annotate_vcf_with_a_dictionary_py} \
        ${hpo_gene_symbol_to_diseases_dictionary} \
        ${genes_symbol_field_name}  \
        ${hpo_gene_symbol_to_diseases_field_name} \
        "$HPO_DISEASES_HEADER" \
          | python3 ${annotate_vcf_with_a_dictionary_py} \
            ${hpo_gene_symbol_to_phenotypes_dictionary} \
            ${genes_symbol_field_name}  \
            ${hpo_gene_symbol_to_phenotypes_field_name} \
            "$HPO_PHENOTYPES_HEADER" \
              | python3 ${annotate_vcf_with_a_dictionary_py} \
              ${hpo_gene_symbol_to_modes_of_inheritance_dictionary} \
              ${genes_symbol_field_name}  \
              ${hpo_gene_symbol_to_modes_of_inheritance_field_name} \
              "$HPO_INHERITANCE_HEADER" \
                | bgzip > ${vcf_prefix}_annotated-with-functional-annotations-gene-level.vcf.gz

  tabix -p vcf ${vcf_prefix}_annotated-with-functional-annotations-gene-level.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                               --task-name-with-index ${task_name_with_index} \
                                               --task-version ${task_version} \
                                               --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_with_functional_annotations_variant_level_vcf_gz = "${vcf_prefix}_annotated-with-functional-annotations-gene-level.vcf.gz"
    File annotated_with_functional_annotations_variant_level_vcf_gz_tbi = "${vcf_prefix}_annotated-with-functional-annotations-gene-level.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow vcf_filter_snpeff_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "filter", "impact"]}'
    name: 'vcf_filter_snpeff'
    author: 'https://gitlab.com/lltw, https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Exlude variants from the VCF file with the maxium impact deleteriousness on a trancipt as predicted by SnpEff lower than specified value. The level of impacts are as follows: \n - HIGH   \n - MODERATE \n - LOW \n - MODIFIER \n More information about SnpEff annotation can be found here: http://snpeff.sourceforge.net/VCFannotationformat_v1.0.pdf \n IMPORTANT NOTES:  1. An input VCF must be a produced by annotate_with_snpeff v0.1 task. 2. The output of the task is a bgzipped VCF file with multiallelic sites split. To correctly collapse multiallelic sites, the task normalize-and-collapse-multiallelic-sites-in-vcf should be used. The task can correctly collapse ANN field on multiallelic sites. It should be run as the last task in a pipeline annotatig and filtering a VCF file.'
    changes: '{"1.1.0": "remove also variants with no impact given, remove latest dir"}'

    input_vcf_basename: '{"name": "vcf basename", "type": "String", "default": "no_input_provided", "description": "Sample ID."}'
    input_chromosome: '{"name": "chromosome", "type": "String", "constraints": {"values":  ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY-and-the-rest"]},"required": "False" , "description": ""}'
    input_vcf_gz : '{"name": "Vcf file", "type": "File", "description": "Input vcf file"}'
    input_vcf_gz_tbi : '{"name": "Index file", "type": "File", "description": "Index for the input vcf file (generated with tabix)."}'
    input_impact_symbol_field_name : '{"name": "Impact field name", "type": "String", "description": "Name of the info field keeping highest impact a given variant is predicted by SnpEff to have, deafult is ISEQ_HIGHEST_IMPACT" }'
    input_impact : '{"name": "Impact", "type": "String", "default": "MODERATE",  "constraints": {"values":  ["HIGH", "MODERATE", "LOW", "MODIFIER"]}, "description": "Minimal impact (for a gene to be listed in the ISEQ_GENES_NAMES field, default is MODIFIER"}'


    output_filtered_by_snpeff_impact_vcf_gz : '{"name": "filtered by snpeff impact vcf files", "type": "File", "description": ""}'
    output_filtered_by_snpeff_impact_vcf_gz_tbi : '{"name": "filtered by snpeff impact vcf files indexes", "type": "File", "description": ""}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call vcf_filter_snpeff

}

task vcf_filter_snpeff {

  File vcf_gz
  File vcf_gz_tbi

  String vcf_basename = "no_input_provided"

  String? chromosome
  String vcf_prefix = if defined(chromosome) then chromosome + "-" + vcf_basename else vcf_basename

  # Inputs with defaults
  String impact_symbol_field_name = "ISEQ_HIGHEST_IMPACT"
  String impact = "MODERATE"

  String task_name = "vcf_filter_snpeff"
  String task_version = "1.1.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.5"

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   # Tbi could be in different directory than vcf file, the symlink below fixes it
        vcf_dir=$(dirname "${vcf_gz}")
        tbi_name=$(basename "${vcf_gz_tbi}")
        if [ ! -f $vcf_dir/$tbi_name ]; then
            ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
        fi

   if [ "${impact}" == "HIGH" ]; then

     bcftools filter ${vcf_gz} \
       --exclude "(INFO/${impact_symbol_field_name} == 'MODERATE') || (INFO/${impact_symbol_field_name} == 'LOW') || (INFO/${impact_symbol_field_name} == 'MODIFIER') || (INFO/${impact_symbol_field_name}='.')" \
       -o ${vcf_prefix}_filtered-by-snpeff-impact.vcf.gz -O z


   elif [ "${impact}" == "MODERATE" ];  then

     bcftools filter ${vcf_gz} \
        --exclude "(INFO/${impact_symbol_field_name} == 'LOW') || (INFO/${impact_symbol_field_name} == 'MODIFIER') || (INFO/${impact_symbol_field_name} = '.')" \
        -o ${vcf_prefix}_filtered-by-snpeff-impact.vcf.gz -O z


  elif [ "${impact}" == "LOW" ]; then

    bcftools filter ${vcf_gz} \
      --exclude "(INFO/${impact_symbol_field_name} == 'MODIFIER') || (INFO/${impact_symbol_field_name} = '.')" \
      -o ${vcf_prefix}_filtered-by-snpeff-impact.vcf.gz -O z

  else

    bcftools filter ${vcf_gz} \
      --exclude "(INFO/${impact_symbol_field_name} = '.')" \
      -o ${vcf_prefix}_filtered-by-snpeff-impact.vcf.gz -O z

  fi

  tabix -p vcf ${vcf_prefix}_filtered-by-snpeff-impact.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File filtered_by_snpeff_impact_vcf_gz = "${vcf_prefix}_filtered-by-snpeff-impact.vcf.gz"
    File filtered_by_snpeff_impact_vcf_gz_tbi = "${vcf_prefix}_filtered-by-snpeff-impact.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

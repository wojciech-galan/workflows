workflow vcf_anno_vep_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "VEP", "loftee"]}'
    name: 'Vcf vep annotation'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Annotates vcf with vep and loftee'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "Vcf", "type": "File", "extension": [".vcf.gz"], "description": "Input vcf file"}'
    input_vcf_gz_tbi: '{"name": "Vcf tbi", "type": "File", "extension": [".vcf.gz.tbi"], "description": "Index for the input vcf file"}'
    input_add_loftee: '{"name": "Add loftee?", "type": "Boolean", "description": "This option defines whether to add loftee annotation"}'

    output_anno_vcf_gz: '{"name": "Annotated vcf", "type": "File", "description": "Vcf file with vep annotation added"}'
    output_anno_vcf_gz_tbi: '{"name": "Annotated vcf tbi", "type": "File", "description": "Index for the output vcf file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True",   "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'


  }

  call vcf_anno_vep

}

task vcf_anno_vep {

  String task_name = "vcf_anno_vep"
  String task_version = "latest"

  String sample_id = "no-id"
  File vcf_gz
  File vcf_gz_tbi

  String chromosome
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/vep:1.0.0-" + chromosome


  Boolean add_loftee = true

  String other_options = "--no_intergenic --hgvs --symbol --biotype --uniprot --total_length"
  String transcript_filtering = "--exclude_predicted --gencode_basic"
  String transcript_info = "--appris --tsl --xref_refseq --mane"

  ## loftee command
  String plugins = "/opt/vep/.vep/Plugins"
  String ancestor_fa = "/opt/vep/.vep/human_ancestor_seq/human_ancestor.fa.gz"
  String conservation_file = "/opt/vep/.vep/conservation/loftee.sql"
  String gerp_data = "/opt/vep/.vep/conservation/gerp_conservation_scores.homo_sapiens.GRCh38.bw"
  String loftee_command = "--plugin LoF,loftee_path:" + plugins +",human_ancestor_fa:" + ancestor_fa +",conservation_file:" + conservation_file +",gerp_bigwig:" + gerp_data
  String loftee = if (add_loftee) then loftee_command else ""

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/after-start.sh)

  # check if input file is not empty
  #if zcat ${vcf_gz} | grep -q '^'; then

     /opt/vep/src/ensembl-vep/vep \
         --cache \
         --offline \
         --format vcf --vcf \
         --force_overwrite \
         --dir_cache /opt/vep/.vep/ \
         --dir_plugins ${plugins} \
         --input_file ${vcf_gz} \
         --compress_output bgzip \
         --output_file ${chromosome}-${sample_id}_annotated-with-vep.vcf.gz \
         ${other_options} ${transcript_filtering} ${transcript_info} ${loftee}

  #else
  #   mv  ${vcf_gz}  ${chromosome}-${sample_id}_annotated-with-vep.vcf.gz
  #fi

  tabix -p vcf ${chromosome}-${sample_id}_annotated-with-vep.vcf.gz

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.1.2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "3G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File anno_vcf_gz = "${chromosome}-${sample_id}_annotated-with-vep.vcf.gz"
    File anno_vcf_gz_tbi = "${chromosome}-${sample_id}_annotated-with-vep.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

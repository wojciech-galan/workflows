workflow meta_test_workflow {

    meta {

        comment__1:   '########################################################################################'
        comment__2:   '################## WDL VARIANTS ########################################################'
        comment__3:   '########################################################################################'

        comment__4:   'Each WDL can be represented in IntelliseqFlow as one or more different workflows.       '
        comment__5:   'In the meta section these workflows are called WDL variants. If you want to create two  '
        comment__6:   'or more variants of WDL, you need to add prefix variant1_, variant2_, ect. to each field'
        comment__7:   'in the meta that you want to adjust to your variants of WDL. If you do not add this     '
        comment__8:   'prefix to any field, there is only one WDL variant. If you add the prefix only for some '
        comment__9:   'fields, the rest of them are assigned to all variants of WDL. For example:              '

        variant1_name: 'meta_test1'
        variant2_name: 'meta_test2'
        variant1_description: 'Testing markdown.\\n # Heading1 \\n #### Heading4 \\n __bold1__ **bold2** *italic1* _italic2_ ***ib1*** ___ib2___ \\n > cytat \\n - kropeczki \\n -kropeczki? \\n * First item \\n * Second item \\n + First item \\n + Second item \\n a jak wyglada `kod jednolinijkowy` tu? \\n ```a taki z trzema \\n i nowa linia?``` \\n linia pod spodem \\n \\n --- \\n \\n i jeszcze [I am an inline-style link] (https://www.biostars.org/p/95803/)'
        variant2_description: 'Meta test variant 2'
        keywords: '{"keywords": ["some", "keywords"]}'
        author: 'https://gitlab.com/'
        copyright: 'Copyright 2019 Intelliseq'
        changes: '{"latest": "no changes"}'
        tag: 'Do not use this filed if the workflow is not released'

        input_markdown: '{"name": "Markdown test", "type": "String", "description": "Testing markdown.\\n # Heading1 \\n #### Heading4 \\n __bold1__ **bold2** *italic1* _italic2_ ***ib1*** ___ib2___ \\n > cytat \\n - kropeczki \\n -kropeczki? \\n * First item \\n * Second item \\n + First item \\n + Second item \\n a jak wyglada `kod jednolinijkowy` tu? \\n ```a taki z trzema \\n i nowa linia?``` \\n linia pod spodem \\n \\n --- \\n \\n i jeszcze [I am an inline-style link] (https://www.biostars.org/p/95803/)"}'

        comment__10:  'It creates two workflows: meta_test1 and meta_test2 with different descriptions but     '
        comment__11:  'with the same remaining fields. Prefixes can also be added to inputs.                   '

        comment__12:  '########################################################################################'
        comment__13:  '################## INPUTS ##############################################################'
        comment__14:  '########################################################################################'

        comment__15:  '########################################################################################'
        comment__16:  '# Obligatory fields for all kind of types:                                              '
        comment__17:  '########################################################################################'
        comment__18:  '#   - "name": "Example name"                                                            '
        comment__19:  '#   - "type": "Example type"                                                            '
        comment__20:  '#   - "description": "Example description"                                              '

        comment__21:  '########################################################################################'
        comment__22:  '# List of types of inputs in IntelliseqFlow:                                            '
        comment__23:  '########################################################################################'
        comment__24:  '# Type: String                                                                          '
        comment__25:  '#   Type in wdl input: String                                                           '
        comment__26:  '#   Obligatory fields:                                                                  '
        comment__27:  '#     - "name", "type", "description"                                                   '
        comment__28:  '#   Optional fields:                                                                    '
        comment__29:  '#     - "default": "default value"                                                      '
        comment__30:  '#     - "constraints": {"notAllowedCharacters": []}                                     '
        comment__301: '#     - "constraints": {"maxLength": int}                                              '
        comment__31:  '#   Example:                                                                            '
        input_string_input1: '{"name": "String", "type": "String", "description": "Enter string"}'
        input_string_input2: '{"name": "String with default", "type": "String", "description": "Enter string", "default": "default value"}'
        input_string_input3: '{"name": "String with constraints (notAllowedCharacters)", "type": "String", "description": "Enter string", "constraints": {"notAllowedCharacters": [" ", "#"]}}'
        input_string_input4: '{"name": "String with default and constraints (notAllowedCharacters)", "type": "String", "description": "Enter string", "default": "default_value", "constraints": {"notAllowedCharacters": [" ", "#"]}}'
        input_string_input5: '{"name": "String with constraints (maxLength)", "type": "String", "description": "Enter string", "constraints": {"maxLength": 10}}'
        comment__32:  '# Type: Array[String]                                                                   '
        comment__33:  '#   Type in wdl input:                                                                  '
        comment__34:  '#     String (if "multiselect": false) or Array[String] (if "multiselect": true)        '
        comment__35:  '#   Obligatory fields:                                                                  '
        comment__36:  '#     - "name", "type", "description"                                                   '
        comment__37:  '#     - "constraints": {"values": ["value1", "value1"]}                                 '
        comment__39:  '#   Optional fields:                                                                    '
        comment__40:  '#     - "default": ["default value1", "default value2"]                                 '
        comment__38:  '#     - "constraints": {"multiselect": true}                                            '
        comment__41:  '#           (Default is false, if you do not put this field in meta it is set to false.)'
        comment__42:  '#   Example:                                                                            '
        input_array_string_input1: '{"name": "Array string with constraints (multiselect true)", "type": "Array[String]", "description": "Select values", "constraints": {"values": ["None", "value1", "value2"], "multiselect": true}}'
        input_array_string_input2: '{"name": "Array string with default", "type": "Array[String]", "description": "Select values", "default": ["value1"], "constraints": {"values": ["None", "value1", "value2"]}}'
        input_array_string_input2_with_mutlifalse: '{"name": "Array string with default and constraints (multiselect false)", "type": "Array[String]", "description": "Select values", "default": ["value1"], "constraints": {"values": ["None", "value1", "value2"], "multiselect": false}}'
        input_array_string_input2_with_mutlifalse_copy: '{"name": "Array string with default and constraints (multiselect false) COPY", "type": "Array[String]", "description": "Select values", "default": ["value1"], "constraints": {"values": ["None", "value1", "value2"], "multiselect": false}}'
        input_array_string_input3: '{"name": "Array string with default and constraints (multiselect true)", "type": "Array[String]", "description": "Select values", "default": ["value1"], "constraints": {"values": ["None", "value1", "value2"], "multiselect": true}}'
        comment__43:  '# Type: File                                                                            '
        comment__44:  '#   Type in wdl input: File                                                             '
        comment__45:  '#   Obligatory fields:                                                                  '
        comment__46:  '#     - "name", "type", "description"                                                   '
        comment__47:  '#     - "extension": [".txt", ".csv", ".tsv"]                                        '
        comment__48:  '#   Optional fields:                                                                    '
        comment__49:  '#     - "default": "default-file-name.ext" <----- NOT IMPLEMENTED YET                   '
        comment__50:  '#   Example:                                                                            '
        input_file_input1: '{"name": "File", "type": "File", "description": "Select file", "extension": [".txt", ".csv", ".tsv", ".json"]}'
        input_file_input2: '{"name": "File with default", "type": "File", "description": "Select file", "extension": [".txt", ".csv", ".tsv", ".json"], "default": "sample-properties.json"}'
        comment__51:  '# Type: Array[File]                                                                     '
        comment__52:  '#   Type in wdl input: Array[File]                                                      '
        comment__53:  '#   Obligatory fields:                                                                  '
        comment__54:  '#     - "name", "type", "description"                                                   '
        comment__55:  '#     - "extension": [".txt", ".csv", ".tsv"]                                        '
        comment__56:  '#   Optional fields:                                                                    '
        comment__57:  '#     - "paired": true (It means that you have to add even number of files.             '
        comment__58:  '#        Default is false, if you do not put this field in meta it is set to false.)    '
        comment__59:  '#     - "default": ["default-file-name.ext", "default-file-name.ext"] <----- NOT IMPLEMENTED YET'
        comment__60:  '#   Example:                                                                            '
        input_array_file_input1: '{"name": "Array file", "type": "Array[File]", "description": "Select files", "extension": [".txt", ".csv", ".tsv", ".json"]}'
        input_array_file_input2: '{"name": "array file with default", "type": "Array[File]", "description": "Select files", "extension": [".txt", ".csv", ".tsv", ".json"], "default": ["sample-properties.json"]}'
        input_array_file_input3: '{"name": "array file with paired", "type": "Array[File]", "description": "Select files", "extension": [".txt", ".csv", ".tsv", ".json"], "paired": true}'
        comment__61:  '# Type: Boolean                                                                         '
        comment__62:  '#   Type in wdl input: Boolean                                                          '
        comment__63:  '#   Obligatory fields:                                                                  '
        comment__64:  '#     - "name", "type", "description"                                                   '
        comment__65:  '#     - "default": true/false                                                           '
        comment__66:  '#   Example:                                                                            '
        input_boolean_input1: '{"name": "Boolean input1", "type": "Boolean", "description": "Check if use this option", "default": true}'
        input_boolean_input2: '{"name": "Boolean input2", "type": "Boolean", "description": "Check if use this option", "default": false}'
        comment__67:  '# Type: Float                                                                           '
        comment__68:  '#   Type in wdl input: Float                                                            '
        comment__69:  '#   Obligatory fields:                                                                  '
        comment__70:  '#     - "name", "type", "description"                                                   '
        comment__71:  '#   Optional fields:                                                                    '
        comment__72:  '#     - "default": 0.1                                                                  '
        comment__73:  '#     - "constraints": {"min": 0.0, "max": 1.0}                                         '
        comment__74:  '#   Example:                                                                            '
        input_float_input1: '{"name": "Float", "type": "Float", "description": "Enter float"}'
        input_float_input2: '{"name": "Float with default", "type": "Float", "description": "Enter float", "default": 0.1}'
        input_float_input3: '{"name": "Float with default and constraints (min and max)", "type": "Float", "description": "Enter float", "default": 0.1, "constraints": {"min": 0.0, "max": 1.0}}'
        comment__75:  '# Type: Int                                                                             '
        comment__76:  '#   Type in wdl input: Int                                                              '
        comment__77:  '#   Obligatory fields:                                                                  '
        comment__78:  '#     - "name", "type", "description"                                                   '
        comment__79:  '#   Optional fields:                                                                    '
        comment__80:  '#     - "default": 1                                                                    '
        comment__81:  '#     - "constraints": {"min": 0, "max": 2}                                             '
        comment__82:  '#   Example:                                                                            '
        input_int_input1: '{"name": "Int", "type": "Int", "description": "Enter Int"}'
        input_int_input2: '{"name": "Int with default", "type": "Int", "description": "Enter Int", "default": 0}'
        input_int_input3: '{"name": "Int with default and constraints (min and max)", "type": "Int", "description": "Enter Int", "default": 0, "constraints": {"min": 0, "max": 2}}'
        comment__83:  '# Type: Selectable  <- NOT IMPLEMENTED YED                                                           '
        comment__84:  '#   Type in wdl input:                                                                  '
        comment__85:  '#        String (if "multiselect": false) or Array[String] (if "multiselect": true)     '
        comment__86:  '#   Obligatory fields:                                                                  '
        comment__87:  '#     - "name", "type", "description"                                                   '
        comment__88:  '#     - "constraints": {"values": ["path-to-json-with-values.json"]}                    '
        comment__89:  '#   Optional fields:                                                                    '
        comment__90:  '#     - "constraints": {"multiselect": true}                                            '
        comment__91:  '#        (Default is false, if you do not put this field in meta it is set to false.)   '
        comment__92:  '#   Example:                                                                            '
        input_selectable_input1: '{"name": "Selectable", "type": "Selectable", "description": "Select values", "constraints": {"values": ["https://gitlab.com/intelliseq/workflows/raw/dev/resources/inputs/selectable/hpo.json"]}}'
        input_selectable_input2: '{"name": "Selectable with constraints (multiselect)", "type": "Selectable", "description": "Select values", "constraints": {"values": ["https://gitlab.com/intelliseq/workflows/raw/dev/resources/inputs/selectable/hpo.json"], "multiselect": true}}'

        comment__93:  '########################################################################################'
        comment__94:  '# Additional features - additional fields                                               '
        comment__95:  '########################################################################################'
        comment__96:  '# For all types of inputs you can add the following fields:                             '
        comment__97:  '#   - "index": Int (It sorts inputs in interface)                                       '
        comment__98:  '#       Example:                                                                        '
        input_index1: '{"name": "Input with index = 1", "type": "String", "description": "Enter value"}'
        input_index2: '{"name": "Input with index = 2", "type": "String", "description": "Enter value"}'
        input_index38: '{"name": "Input with index = 38", "type": "String", "description": "Enter value"}'
        comment__99:  '#   - "advanced": true (Default is false, if you do not put this field in meta it is set'
        comment__100: '#     to false. Input with this field will be displayed in separated tab in interface.) '
        comment__101: '#       Example:                                                                        '
        input_advanced_input1: '{"name": "Advanced input1", "type": "String", "description": "Enter value", "advanced": true}'
        input_advanced_input2: '{"name": "Advanced input2", "type": "String", "description": "Enter value", "advanced": true}'
        comment__102: '#   - "hidden": true (Default is false, if you do not put this field in meta it is set  '
        comment__103: '#     to false. Input with this field are not available in interface.)                  '
        comment__104: '#       Example:                                                                        '
        input_hidden_input1: '{"name": "Hidden input1", "type": "String", "description": "Enter value", "hidden": true}'
        input_hidden_input2: '{"name": "Hidden input2", "type": "String", "description": "Enter value", "hidden": true}'
        comment__105: '#   - "required": true (Default is false, if you do not put this field in meta it is set'
        comment__106: '#     to false. Input with this field are required.)                  '
        comment__107: '#       Example:                                                                        '
        input_required_input1: '{"name": "Required input1", "type": "String", "description": "Enter value", "required": true}'
        input_required_input2: '{"name": "Required input2", "type": "String", "description": "Enter value", "required": true}'

        comment__108: '########################################################################################'
        comment__109: '# Additional features - groups                                                          '
        comment__110: '########################################################################################'
        comment__111: '# To group set of inputs you need to create field "groups" with the following structure:'
        groups: '{"group1": {"hReadableName": "Nice name gr1", "description": "Description of group 1", "rules": "Rules for group 1", "minInputs": 1}, "group2": {"hReadableName": "Nice name gr2", "description": "Description of group 2", "rules": "Rules for group 2", "minInputs": 2}}'
        comment__112: '# where group1 and group2 are the names of the groups.                                  '
        comment__113: '# Then add field group with proper group name to any input.                             '
        comment__114: '# Example:                                                                              '
        input_input1_in_group1: '{"name": "Input 1 in group 1", "type": "String", "description": "Enter string", "groupname": "group1"}'
        input_input2_in_group1: '{"name": "Input 2 in group 1", "type": "String", "description": "Enter string", "groupname": "group1"}'
        input_input1_in_group2: '{"name": "Input 1 in group 2", "type": "String", "description": "Enter string", "groupname": "group2"}'
        input_input2_in_group2: '{"name": "Input 2 in group 2", "type": "String", "description": "Enter string", "groupname": "group2"}'

        comment__1141:'# To change fields in inputs/groups in specyfic variant add new value to this fields    '
        comment__1142:'# Example (hide group 1 in variant 2 - you need to add "hidden": true to all inputs in  '
        comment__1143:'# this group and set minInputs to 0!!!                                                  '
        variant2_groups: '{"group1": {"minInputs": 0}}'
        variant2_input_input1_in_group1: '{"hidden": true, "name": "Input 1 in group 1", "type": "String", "description": "Enter string", "groupname": "group1"}'
        variant2_input_input2_in_group1: '{"hidden": true, "name": "Input 2 in group 1", "type": "String", "description": "Enter string", "groupname": "group1"}'


        comment__115: '########################################################################################'
        comment__116: '################## OUTPUTS #############################################################'
        comment__117: '########################################################################################'

        output_stdout_log: '{"name": "Standard out", "type": "File", "copy": true, "description": "Standard out"}'
        output_stderr_log: '{"name": "Standard err", "type": "File", "copy": true, "description": "Standard error"}'
        output_bco: '{"name": "Biocompute object", "type": "File", "copy": true, "description": "Biocompute object"}'

    }

    call meta_test

}

task meta_test {

    String? string_markdown

    String? string_input1
    String? string_input2
    String? string_input3
    String? string_input4
    String? string_input5

    Array[String]? array_string_input1
    String? array_string_input2
    String? input_array_string_input2_with_mutlifalse
    String? input_array_string_input2_with_mutlifalse_copy
    Array[String]? array_string_input3

    File? file_input1
    File? file_input2

    Array[File]? array_file_input1
    Array[File]? array_file_input2
    Array[File]? array_file_input3

    Boolean? boolean_input1
    Boolean? boolean_input2

    Float? float_input1
    Float? float_input2
    Float? float_input3

    Int? int_input1
    Int? int_input2
    Int? int_input3

    String? selectable_input1
    Array[String]? selectable_input2

    String? index1
    String? index2
    String? index38

    String? advanced_input1
    String? advanced_input2

    String? hidden_input1
    String? hidden_input2

    String? required_input1
    String? required_input2

    String? input1_in_group1
    String? input2_in_group1
    String? input1_in_group2
    String? input2_in_group2

    String task_name = "meta_test"
    String task_version = "2.1.1"
    Int? index
    String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
    String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.3"

    command <<<
        set -e -o pipefail
        bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


        bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
        --task-name-with-index ${task_name_with_index} \
        --task-version ${task_version} \
        --task-docker ${docker_image}
    >>>

    runtime {

        docker: docker_image
        memory: "1G"
        cpu: "1"
        maxRetries: 2

    }

    output {

        File stdout_log = stdout()
        File stderr_log = stderr()
        File bco = "bco.json"

    }

}

workflow bam_metrics_workflow {

  meta {
    keywords: '{"keywords": ["gatk", "samtools", "coverage metrics"]}'
    name: 'bam_metrics'
    author: 'https://gitlab.com/mremre, https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Collects coverage metrics using gatk and samtools to 2 jsons (with whole statistics and with human readable statistics with some additional info).'
    changes: '{"1.3.0": "target possible", "1.2.7": "possible genome choice", "1.2.6": "changed ram"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_chromosome: '{"name": "chromosome", "type": "String", "description": "number of chromosome"}'
    input_intervals: '{"name": "intervals", "type": "File", "extension": [".interval_list"], "description": "interval list"}'
    input_target: '{"name": "target", "type": "File", "extension": [".interval_list"], "description": "target"}'
    input_bait: '{"name": "bait", "type": "File", "extension": [".interval_list"], "description": "bait"}'
    input_bam: '{"name": "bam", "type": "File", "extension": [".bam"], "description": "Alignment result"}'
    input_kit: '{"name": "Kit", "type": "String", "required": "True", "constraints": {"values": ["exome-v6", "exome-v7", "genome"]}, "description": ""}'

    output_final_json: '{"name": "final_json.csv", "type": "File", "copy": "True", "description": "json with coverage statistics metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_metrics

}

task bam_metrics {

  String chromosome = "chr1"
  File? intervals
  File? target
  File? bait

  File bam

  String kit
  String reference_genome = "grch38-no-alt"
  String sample_id = "no_id_provided"
  String java_options = "-Xms4g -Xmx63g"

  String task_name = "bam_metrics"
  String task_version = "1.3.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-metrics-"+ reference_genome + ":1.0.0"

  command <<<
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    set -e
#    -o pipefail nie jest użyte, bo przerywało trwanie taska przy | head -1

    # sprawdzenie, czy bam nie jest pusty
    bam_content=$(samtools view ${bam} | head -1)

    if [ -z "$bam_content" ]
    then
       touch ${chromosome}_${sample_id}_final.json
    else
       # for WES, target and  WGS #
        gatk CollectAlignmentSummaryMetrics \
            --java-options "${java_options}" \
            -R /resources/reference-genome.fa \
            --INPUT ${bam} \
            --OUTPUT "${sample_id}-CollectAlignmentSummaryMetrics.txt"

        gatk MarkDuplicates \
            --java-options "${java_options}" \
            -R /resources/reference-genome.fa \
            --INPUT ${bam} \
            --OUTPUT "to-remove-${sample_id}-MarkDuplicates.txt" \
            -M "${sample_id}-MarkDuplicates.txt"

        rm "to-remove-${sample_id}-MarkDuplicates.txt"

        if [ ${kit} = "genome" ] || [ ${kit} = "target" ]; then
            gatk CollectWgsMetrics \
            --java-options "${java_options}" \
                -R /resources/reference-genome.fa \
                --INPUT ${bam} \
                --OUTPUT "${sample_id}-CollectWgsMetrics.txt" \
                --INTERVALS ${intervals}

            gatk CollectQualityYieldMetrics \
            --java-options "${java_options}" \
                -R /resources/reference-genome.fa \
                --INPUT ${bam} \
                --OUTPUT "${sample_id}-CollectQualityYieldMetrics.txt"

            python3 /intelliseqtools/bam-metrics.py     --metrics CollectAlignmentSummaryMetrics \
                                                        --metrics MarkDuplicates \
                                                        --metrics CollectQualityYieldMetrics \
                                                        --metrics CollectWgsMetrics \
                                                        --chromosome ${chromosome} \
                                                        --sample ${sample_id} \
                                                        --workdir "."

        elif [ ${kit} = "exome-v7" ] || [ ${kit} = "exome-v6" ]; then
            gatk CollectHsMetrics \
            --java-options "${java_options}" \
                --INPUT ${bam} \
                --OUTPUT "${sample_id}-CollectHsMetrics.txt" \
                -R /resources/reference-genome.fa \
                -BI ${bait} \
                -TI ${target}


            python3 /intelliseqtools/bam-metrics.py     --metrics CollectAlignmentSummaryMetrics \
                                                        --metrics MarkDuplicates \
                                                        --metrics CollectHsMetrics \
                                                        --chromosome ${chromosome} \
                                                        --sample ${sample_id} \
                                                        --workdir "."

        fi
    fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "64G"
    cpu: "8"
    maxRetries: 2

  }

  output {
    File final_json = "${chromosome}_${sample_id}_final.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }
}

workflow vcf_split_site_only_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "sites-only"]}'
    name: 'vcf_split_site_only'
    author: 'https://gitlab.com/lltw https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Removes genotype info from vcf, divides vcf file into chunks'
    changes: '{"latest": "no changes"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_vcf_gz: '{"name": "Vcf file", "type": "File", "description": "Vcf file (bgzipped)"}'
    input_vcf_gz_tbi: '{"name": "Vcf tbi", "type": "File", "description": "Index for the vcf file"}'
    input_vcf_array_length: '{"name": "Vcf array length", "type": "Int", "description": "Decides into how many chunks input vcf should be divided (for apply VQRS task)"}'

    output_sites_only_vcf_gz: '{"name": "Sites only vcf", "type": "File", "copy": "True", "description": "Vcf file with genotype data dropped"}'
    output_sites_only_vcf_gz_tbi: '{"name": "Sites only vcf tbi", "type": "File", "copy": "True", "description": "Index for the sites only vcf file"}'
    output_split_vcf_gz: '{"name": "Splitted vcf files", "type": "Array[File]", "copy": "True", "description": "Partial vcf files"}'
    output_split_vcf_gz_tbi: '{"name": "Splitted vcf tbi", "type": "Array[File]", "copy": "True", "description": "Partial vcf files indexes"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_split_site_only

}

task vcf_split_site_only {

  String task_name = "vcf_split_site_only"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  File vcf_gz
  File vcf_gz_tbi
  String sample_id = "no-id"

  Int vcf_array_length = 20

  String docker_image = "intelliseqngs/task_vcf-split-site-only:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   # Remove genotypes from vcf file
   bcftools view -G ${vcf_gz} -O z -o  ${sample_id}_sites-only.vcf.gz
   tabix -p vcf ${sample_id}_sites-only.vcf.gz

   # Split vcf into smaller ones
   python3 /intelliseqtools/split-vcf-file.py \
       ${vcf_gz} ${vcf_array_length} -n ${sample_id}.vcf.gz

   ls [0-9]*${sample_id}.vcf.gz | xargs -i -n 1 -P ${vcf_array_length} bash -c "tabix -f -p vcf {}"


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "2G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File sites_only_vcf_gz = "${sample_id}_sites-only.vcf.gz"
    File sites_only_vcf_gz_tbi = "${sample_id}_sites-only.vcf.gz.tbi"

    Array[File] split_vcf_gz = glob("[0-9]*${sample_id}.vcf.gz")
    Array[File] split_vcf_gz_tbi = glob("[0-9]*${sample_id}.vcf.gz.tbi")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }
}

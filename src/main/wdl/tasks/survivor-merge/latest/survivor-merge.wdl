workflow survivor_merge_workflow {

  meta {
    name: 'survivor_merge'
    author: 'https://gitlab.com/lltw'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'survivor_merge \n Generic text for task'
    changes: '{"latest": "no changes"}'

    input_sv_vcf_gzs: '{"name": "Array of structural variants VCF (bgzipped)", "type": "File", "extension": [".vcf.gz"], "description": "Array of bgzipped VCF files containing structural variants information for non-overlapping set of samples. Files should be sorted."}'
    input_sv_vcf_basename: '{"name": "Files basename", "type": "String", "description": "Basename of output files"}'
    input_min_sv_size: '{"name": "Minimum size of SV", "type": "Int", "constraints": {"min": "1"}, "default": "1", "description": "Minimum size of SV to be taken into account. Default: 50"}'

    input_max_distance_between_breakpoints: '{"name": "Maximum distance between breakpoints", "type": "Int", "constraints": {"min": 0}, "default": 500, "description": "Maximum distance between breakpoints (begin_1 vs begin_2, end_1 vs end_2) to merge SVs together. Default: 500 bp"}'
    input_min_no_supporting_calles: '{"name": "Minimum number of supporting callers", "type": "Int", "constraints": {"min": 1}, "default": "1", "description": "Minimum number of supporting caller. Default: 1"}'

    input_use_type: '{"name": "Take the type of SV into account", "type": "Boolean", "default": "true", "description": "Take the type of SV into account. Default: true"}'
    input_use_strand: '{"name": "Take the strands of SVs into account", "type": "Boolean", "default": "false", "description": "Take the strands of SVs into account. Default: false"}'
    input_estimate_distance: '{"name": "Estimate distance based on the size", "type": "Boolean", "default": "true", "description": "Estimate distance based on the size. Default: true"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call survivor_merge

}

task survivor_merge {

  Array[File] sv_vcf_gzs
  String sv_vcf_basename

  Int max_distance_between_breakpoints = 500
  Int min_no_supporting_calles = 1
  Int min_sv_size = 50

  Boolean use_type = true
  Boolean use_strand = false
  Boolean estimate_distance = true

  String task_name = "survivor_merge"
  String task_version = "latest"
  String docker_image = "intelliseqngs/survivor:merge_v0.1"

  command <<<
    task_name="${task_name}"; task_version="${task_version}"; task_docker="${docker_image}"
    source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/after-start.sh)

    parallel mv ::: ${sep=" " sv_vcf_gzs} ::: .
    parallel gunzip -f ::: *vcf.gz

    ls *vcf > sample_files

    SURVIVOR merge \
      sample_files \
      ${max_distance_between_breakpoints} \
      ${min_no_supporting_calles} \
      ${true='1' false='0' use_type} \
      ${true='1' false='0' use_strand} \
      ${true='1' false='0' estimate_distance} \
      ${min_sv_size} \
      ${sv_vcf_basename}.survivor-merged.vcf

     bgzip ${sv_vcf_basename}.survivor-merged.vcf
     tabix -p vcf ${sv_vcf_basename}.survivor-merged.vcf.gz

    source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File survivor_merged_sv_vcf_gz = "${sv_vcf_basename}.survivor-merged.vcf.gz"
    File survivor_merged_sv_vcf_gz_tbi = "${sv_vcf_basename}.survivor-merged.vcf.gz"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

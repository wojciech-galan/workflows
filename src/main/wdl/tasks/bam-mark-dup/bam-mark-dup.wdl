workflow bam_mark_dup_workflow {

  meta {
    keywords: '{"keywords": ["BAM", "duplicated reads"]}'
    name: 'bam_mark_dup'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Marks optical and PCR duplicated in bam file, sorts output bam'
    changes: '{"1.0.1": "adjusted disk size and new gatk version"}'

    input_sample_id: '{"name": "Sample id", "type": "String", "description": "Sample identifier"}'
    input_bams: '{"name": "Bams", "type": "Array[File]", "extension": [".bam"], "description": "Input bam files, query sorted"}'

    output_mark_dup_bam: '{"name": "Duplicates marked bam", "type": "File", "copy": "True", "description": "Bam file with marked duplicates, coordinate sorted"}'
    output_mark_dup_bai: '{"name": "Duplicates marked bai", "type": "File", "copy": "True", "description": "Index for the duplicates marked bam"}'
    output_metrics_file: '{"name": "Metrics file", "type": "File", "copy": "True", "description": "File with duplication metrics"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_mark_dup

}

task bam_mark_dup {

  String task_name = "bam_mark_dup"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0:1.0.0"

  String sample_id = "no_id_provided"
  Array[File] bams
  Int input_len = length(bams)
  Int input_size = if input_len > 0 then ceil(size(bams[0], "GB")) else 1
  Int disk_size =  5 * input_len * input_size + 100

  ## set as none to disable optical duplicates marking, do not provide anything otherwise
  String? read_name_regex
  String md_java_options = "-Xmx14g"
  String sort_java_options = "-Xmx15g"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk --java-options "${md_java_options}" \
       MarkDuplicates \
      -I ${sep=' -I ' bams} \
      -O ${sample_id}_dup-marked.bam \
      --METRICS_FILE ${sample_id}_markdup-metrics.txt \
      --VALIDATION_STRINGENCY SILENT \
      ${"--READ_NAME_REGEX " + read_name_regex} \
      --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
      --ASSUME_SORT_ORDER "queryname" \
      --CLEAR_DT false \
      --ADD_PG_TAG_TO_READS false


  gatk --java-options ${sort_java_options} \
      SortSam \
      -I ${sample_id}_dup-marked.bam \
      -O ${sample_id}_sorted-dup-marked.bam \
      --SORT_ORDER "coordinate" \
      --CREATE_INDEX true \
      --CREATE_MD5_FILE true \
      --MAX_RECORDS_IN_RAM 300000


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "16G"
    cpu: 4
    maxRetries: 2
    disks: "local-disk " + disk_size + " LOCAL"

  }

  output {

    File mark_dup_bam = "${sample_id}_sorted-dup-marked.bam"
    File mark_dup_bai = "${sample_id}_sorted-dup-marked.bai"
    File metrics_file = "${sample_id}_markdup-metrics.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow bed_liftover_workflow
{
  call bed_liftover
}

task bed_liftover {

  File bed
  String output_name = "hg19ToHg38_liftover_" + sub(basename(bed), ".gz", "")

  String task_name = "bed_liftover"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bed-liftover:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   CrossMap.py bed /resources/hg19ToHg38.over.chain.gz ${bed} ${output_name}
  
   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File bed_liftover = "${output_name}"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

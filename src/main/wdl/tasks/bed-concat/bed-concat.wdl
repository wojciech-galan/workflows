workflow bed_concat_workflow {

  meta {
    keywords: '{"keywords": ["bed", "concatenate"]}'
    name: 'Concatenate and compress bed files, optionally applies sorting'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Concatenates beds'
    changes: '{"latest": "no changes"}'
    input_chr_beds: '{"name": "Bed files", "type": "Array", "extension": [".bed", ".bed.gz"],"description": "Chromosome-wise bed files or bgzipped bed files (ifbgzipped must have.gz extension)"}'
    input_basename: '{"name": "Output file basename", "type": "String", "description": "Output bed file basename"}'
    input_need_sort: '{"name": "Need sort?", "type": "Boolean", "description": "Decides whether apply sorting (lexical on first column and numerical on second)"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_bed_gz: '{"name": "Concatenated bed", "type": "File", "description": "Genome-wise bed file (bgzipped)"}'
    output_bed_gz_tbi: '{"name": "Concatenated bed tbi", "type": "File", "description": "Concatenated bed index"}'
  }

  call bed_concat

}

task bed_concat {

  String task_name = "bed_concat"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-toolbox-20.04:2.0.4"
  Array[File] chr_beds
  Int array_length = length(chr_beds)
  Boolean need_sort = false
  String sort_command = if (need_sort) then "| sort -k1,1 -k2n" else ""
  String basename = "no-id"
  File first_bed = select_first(chr_beds)
  String first_bed_name = basename(first_bed)

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  if [ ${array_length} -eq 1 ] && !${need_sort} && echo ${first_bed_name} | grep -q '.gz'; then
     mv ${first_bed}  ${basename}_ibd-regions.bed.gz
  else
     if echo ${first_bed_name} | grep -q '.gz';then
        zcat ${sep=" " chr_beds} ${sort_command} | bgzip > ${basename}_ibd-regions.bed.gz
     else
        cat ${sep=" " chr_beds}  ${sort_command} | bgzip > ${basename}_ibd-regions.bed.gz
     fi
  fi

  tabix -p bed ${basename}_ibd-regions.bed.gz

 bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                          --task-name-with-index ${task_name_with_index} \
                                          --task-version ${task_version} \
                                          --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File bed_gz = "${basename}_ibd-regions.bed.gz"
    File bed_gz_tbi = "${basename}_ibd-regions.bed.gz.tbi"
  }

}

workflow rna_seq_count_concat_workflow {

  meta {
    keywords: '{"keywords": ["concat count reads files"]}'
    name: 'rna_seq_count_concat'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Generic text for task'
    changes: '{"1.0.2": "cp instead of ln -s"}'

    input_count_reads: '{"name": "count_reads", "type": "Array[File]", "description": "count reads files from HTSeq"}'

    output_count_concat_excel_file: '{"name": "Counts xlsx", "type": "File", "copy": "True", "description": "Number of mapped reads in .xlsx format"}'
    output_count_concat_tsv_file: '{"name": "Counts tsv", "type": "File", "copy": "True", "description": "Number of mapped reads in .tsv format"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_count_concat

}

task rna_seq_count_concat {

  Array[File] count_reads
  String analysis_id = "no_id_provided"

  String task_name = "rna_seq_count_concat"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_rna-seq-count-concat:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir count_files
    cp ${sep=" " count_reads} count_files

    python3 /intelliseqtools/count-concat.py --input-path-to-count-files "count_files" \
                                             --output-file-name ${analysis_id}-count_concat


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File count_concat_excel_file = "count_files/${analysis_id}-count_concat.xlsx"
    File count_concat_tsv_file = "count_files/${analysis_id}-count_concat.tsv"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

workflow gvcf_call_variants_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "genotyping", "gatk"]}'
    name: 'gvcf_call_variants'
    author: 'https://gitlab.com/lltw, https://gitlab.com/marysiaa'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'genotype_gvcf'
    changes: '{"1.2.0" : "genotype also GenomicsDB", "1.1.4" : "new docker - non-root permission to resources", "1.1.3": "add additional_parm"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_gvcf_gz:'{"name": "gvcf gzs","type": "File", "description": "Bgzipped gVCF file"}'
    input_gvcf_gz_tbi: '{ "name": "gvcf index", "type": "File", "description": "gVCF files index"}'
    input_interval_list: '{ "name": "interval list", "type": "String", "required": "False", "description": ""}'
    input_interval_padding: '{ "name": "interval padding", "type": "String", "required": "False", "description": ""}'
    input_gatk_additional_parm: '{ "name": "gatk additional parameter", "type": "String", "required": "False", "description": "additional parameter(s) to include in gatk GenotypeGVCFs"}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "values": ["hg38", "grch38-no-alt"], "default": "hg38", "description": "Reference genome options: hg38 and grch38-no-alt"}'

    output_vcf_gz:'{"name": "vcf gz","type": "File", "description": "Bgzipped VCF file"}'
    output_vcf_gz_tbi: '{ "name": "gvcf index", "type": "File", "description": "VCF file index"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call gvcf_call_variants

}

task gvcf_call_variants {

  File? gvcf_gz
  File? gvcf_gz_tbi
  File? genomics_db_tar

  Boolean is_genomics_db_tar_provided = defined(genomics_db_tar)
  String genomics_db_name = if is_genomics_db_tar_provided then basename(genomics_db_tar, ".tar") else ""

  String sample_id = "no_id_provided"
  String reference_genome = "hg38" # or grch38-no-alt
  String gatk_additional_parm = ''

  File? interval_list
  Boolean intervals_defined = defined(interval_list)
  String intervals_arg = if intervals_defined then "--intervals " else ""

  String? interval_padding
  String interval_padding_defined = select_first([interval_padding, ""])
  String interval_padding_arg = if defined(interval_padding) then "--interval_padding " + interval_padding_defined else ""

  String gatk_path = "/gatk/gatk"
  String genotype_gvcfs_java_options = if is_genomics_db_tar_provided then "-Xms8g" else "-Xmx5g -Xms5g"

  String task_name = "gvcf_call_variants"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0-" + reference_genome + ":1.0.0"

  command <<<

  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  if ${is_genomics_db_tar_provided}
  then
        tar -xf ${genomics_db_tar}

        ${gatk_path} --java-options "${genotype_gvcfs_java_options}" \
        GenotypeGVCFs  ${intervals_arg} ${interval_list} ${interval_padding_arg} ${gatk_additional_parm} \
          --reference /resources/Homo_sapiens_assembly38.fa \
          --annotate-with-num-discovered-alleles true \
          --variant gendb://${genomics_db_name} \
          --output ${sample_id}.vcf.gz

  else
      ${gatk_path} --java-options "${genotype_gvcfs_java_options}" \
      GenotypeGVCFs  ${intervals_arg} ${interval_list} ${interval_padding_arg} ${gatk_additional_parm} \
        --reference /resources/Homo_sapiens_assembly38.fa \
        --annotate-with-num-discovered-alleles true \
        --variant ${gvcf_gz} \
        --output ${sample_id}.vcf.gz
  fi

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "10G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File vcf_gz = "${sample_id}.vcf.gz"
    File vcf_gz_tbi = "${sample_id}.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

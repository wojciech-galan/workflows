workflow report_coverage_stats_workflow {

  meta {
    keywords: '{"keywords": ["coverage_reports"]}'
    name: 'report_coverage_stats'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: '## report_coverage_stats \n Generic text for task'
    changes: '{"latest": "no changes"}'

    input_coverage_stats_json: '{"name": "coverage_stats_json", "type": "File", "extension": [".json"]}'

    output_coverage_report_pdf: '{"name": "coverage_report_pdf", "type": "File", "copy": "true", "description": "Coverage statistics report in pdf file"}'
    output_coverage_report_odt: '{"name": "coverage_report_odt", "type": "File", "copy": "true", "description": "Coverage statistics report in odt file"}'
    output_coverage_report_docx: '{"name": "coverage_report_docx", "type": "File", "copy": "true", "description": "Coverage statistics report in docx file"}'
    output_coverage_report_html: '{"name": "coverage_report_html", "type": "File", "copy": "true", "description": "Coverage statistics report in html file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "true", "description": "Console output"}'
    output_stderr_err: '{"name": "Standard err", "type": "File", "copy": "true", "description": "Console stderr"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "true", "description": "Biocompute object"}'
  }

  call report_coverage_stats

}

task report_coverage_stats {

  File coverage_stats_json

  String output_name = "report-coverage-stats"

  String task_name = "report_coverage_stats"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String task_version = "latest"
  String docker_image = "intelliseqngs/reports:1.0.1"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/after-start.sh)

  /opt/tools/generate-report.sh --json coverage=${coverage_stats_json} --template /opt/tools/templates/coverage-v2/content.jinja --name ${output_name}

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/v2/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File coverage_report_pdf = "${output_name}.pdf"
    File coverage_report_odt = "${output_name}.odt"
    File coverage_report_docx = "${output_name}.docx"
    File coverage_report_html = "${output_name}.html"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

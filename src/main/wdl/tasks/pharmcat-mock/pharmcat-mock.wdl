workflow pharmcat_mock_workflow {

  meta {
    keywords: '{"keywords": ["some", "keywords"]}'
    name: 'pharmcat_mock'
    author: 'https://gitlab.com/olaf.tomaszewski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Generic text for task'
    changes: '{}'

    output_pgx_report_html: '{"name": "PGx report", "type": "File", "description": "PharmCAT pharmacogenomics report in .html format"}'
    output_pgx_report_json: '{"name": "PGx report json", "type": "File", "description": "Data from pharmacogenomics report in .json format"}'
    output_pgx_matcher_html: '{"name": "PGx matcher report", "type": "File", "description": "Report with allele matcher results."}'
    output_pgx_matcher_json: '{"name": "PGx matcher json", "type": "File", "description": "json with allele matcher results."}'

    output_cnv_tsv: '{"name": "cnv_tsv", "type": "File", "description": "TSV text file with genotyping data from pgx tool, specialised for Copy Number Variants (CNV)"}'
    output_verbose_data: '{"name": "verbose_data", "type": "File", "description": "Text file with genotyping data from pgx tool, specialised for Copy Number Variants (CNV)"}'
    output_novel_data: '{"name": "novel_data", "type": "File", "description": "Text file with genotyping data from pgx tool, specialised for Copy Number Variants (CNV)"}'

    output_cyrius_tsv: '{"name": "cyrius_tsv", "type": "File", "description": "TSV text file with genotyping data from Cyrius"}'
    output_cyrius_json: '{"name": "cyrius_json", "type": "File", "description": "JSON text file with genotyping data from Cyrius"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call pharmcat_mock

}

task pharmcat_mock {

  String task_name = "pharmcat_mock"
  String task_version = "1.0.0"
  String sample_id = "mock_id"
  String genotyping_database = "mock_database"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir sampledir
    touch sampledir/${sample_id}_pgx.report.html
    touch sampledir/${sample_id}_pgx.report.json
    touch sampledir/${sample_id}_pgx.matcher.html
    touch sampledir/${sample_id}_pgx.call.json

    touch ${sample_id}_genotyped-by-${genotyping_database}.vcf.gz

    touch "${sample_id}_astrolabe-out.tsv"
    touch "${sample_id}_astrolabe-verbose.txt"
    touch "${sample_id}_astrolabe-novel.txt"

    touch "${sample_id}-cyrius.json"
    touch "${sample_id}-cyrius.tsv"

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {
    File pgx_report_html = "sampledir/${sample_id}_pgx.report.html"
    File pgx_report_json = "sampledir/${sample_id}_pgx.report.json"
    File pgx_matcher_html = "sampledir/${sample_id}_pgx.matcher.html"
    File pgx_matcher_json = "sampledir/${sample_id}_pgx.call.json"

    File genotyped_vcf_gz = "${sample_id}_genotyped-by-${genotyping_database}.vcf.gz"

    File cnv_tsv = "${sample_id}_astrolabe-out.tsv"
    File verbose_file = "${sample_id}_astrolabe-verbose.txt"
    File novel_file = "${sample_id}_astrolabe-novel.txt"

    File cyrius_json = "${sample_id}-cyrius.json"
    File cyrius_tsv = "${sample_id}-cyrius.tsv"


    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

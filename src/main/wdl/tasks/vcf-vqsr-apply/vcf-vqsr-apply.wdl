workflow vcf_vqsr_apply_workflow {

  meta {
    keywords: '{"keywords": ["VQSR", "filter"]}'
    name: 'Apply VQS recalibration'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: 'Applies VQSR recalibration.\nParameters set as in GATK4 pipeline (08-08-2020) https://github.com/gatk-workflows/gatk4-germline-snps-indels'
    changes: '{"latest": "no changes"}'

    input_vcf_gz: '{"name": "VCF gz", "type": "File", "description": "VCF file (bgzipped)"}'
    input_vcf_gz_tbi: '{"name": "VCF gz", "type": "File", "description": "VCF file index"}'
    input_indels_recalibration: '{"name": "Indels recalibration vcf", "type": "File", "description": "VCF file with VQSLD values for indels"}'
    input_indels_recalibration_idx: '{"name": "Indels recalibration vcf idx", "type": "File", "description": "Index for the indels recalibration vcf"}'
    input_snps_recalibration: '{"name": "SNPs recalibration vcf", "type": "File", "description": "VCF file with VQSLD values for SNPs"}'
    input_snps_recalibration_idx: '{"name": "SNPs recalibration vcf idx", "type": "File", "description": "Index for the SNPs recalibration vcf"}'
    input_indels_tranches: '{"name": "Indels tranches", "type": "File", "description": "VQSR tranches file for indels"}'
    input_snps_tranches: '{"name": "SNPs tranches", "type": "File", "description": "VQSR tranches file for SNPs"}'
    input_indel_filter_level: '{"name": "INDEL VQSR threshold", "type": "Float", "description": "Threshold value for VQSR recalibrated INDELs"}'
    input_snp_filter_level: '{"name": "SNP VQSR threshold", "type": "Float", "description": "Threshold value for VQSR recalibrated SNPs"}'
    input_use_allele_specific_annotations: '{"name": "Use allele specific annotations?", "type": "Boolean", "description": "Decides whether use allele specific annotations during VQSR recalibration"}'

    output_vqsr_recalib_vcf_gz: '{"name": "VQSR recalibrated vcf gz", "type": "File", "copy": "True", "description": "VQSR recalibtated vcf file (bgzipped)"}'
    output_vqsr_recalib_vcf_gz_tbi: '{"name": "VQSR recalibrated vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the VQSR recalibtated vcf file"}'
    output_vqsr_filtered_vcf_gz: '{"name": "VQSR recalibrated filtered vcf gz", "type": "File", "copy": "True", "description": "VQSR recalibtated and filtered vcf file (bgzipped)"}'
    output_vqsr_filtered_vcf_gz_tbi: '{"name": "VQSR recalibrated filtered vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the VQSR recalibtated and filtered vcf file"}'
    output_vqsr_rejected_vcf_gz: '{"name": "VQSR rejected vcf gz", "type": "File", "copy": "True", "description": "VCF with variants that did not passed VQSR filtered (bgzipped)"}'
    output_vqsr_rejected_vcf_gz_tbi: '{"name": "VQSR rejected vcf gz tbi", "type": "File", "copy": "True", "description": "Index for the VQSR rejected sites vcf file"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_vqsr_apply

}

task vcf_vqsr_apply {

  String task_name = "vcf_vqsr_apply"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.1.7.0:1.0.4"

  File vcf_gz
  File vcf_gz_tbi
  File indels_recalibration
  File indels_recalibration_idx
  File snps_recalibration
  File snps_recalibration_idx
  File indels_tranches
  File snps_tranches

  String vcf_basename = basename(vcf_gz, ".vcf.gz")

  # Filtering parameters
  Float indel_filter_level = 99.0
  Float snp_filter_level = 99.7
  Boolean use_allele_specific_annotations = true

  String java_options = "-Xms5g"


  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   # Tbi could be in different directory than vcf file, the symlink below fixes it
   vcf_dir=$(dirname "${vcf_gz}")
   tbi_name=$(basename "${vcf_gz_tbi}")
   if [ ! -f $vcf_dir/$tbi_name ]; then
      ln -s ${vcf_gz_tbi} $vcf_dir/$tbi_name
   fi


    gatk --java-options "${java_options}" \
        ApplyVQSR \
        -O tmp.indel.recalibrated.vcf \
        -V ${vcf_gz} \
        --recal-file ${indels_recalibration} \
        ${true='--use-allele-specific-annotations' false='' use_allele_specific_annotations} \
        --tranches-file ${indels_tranches} \
        --truth-sensitivity-filter-level ${indel_filter_level} \
        --create-output-variant-index true \
        -mode INDEL

    gatk --java-options "${java_options}" \
        ApplyVQSR \
        -O ${vcf_basename}_vqsr-recalibrated.vcf.gz \
        -V tmp.indel.recalibrated.vcf \
        --recal-file ${snps_recalibration} \
        ${true='--use-allele-specific-annotations' false='' use_allele_specific_annotations} \
        --tranches-file ${snps_tranches} \
        --truth-sensitivity-filter-level ${snp_filter_level} \
        --create-output-variant-index true \
        -mode SNP

   zcat ${vcf_basename}_vqsr-recalibrated.vcf.gz | awk '/^#/||$7=="PASS"' | bgzip >  ${vcf_basename}_vqsr-filtered.vcf.gz
   zcat ${vcf_basename}_vqsr-recalibrated.vcf.gz | awk '/^#/||$7!="PASS"' | bgzip > ${vcf_basename}_vqsr-rejected.vcf.gz

   tabix -p vcf ${vcf_basename}_vqsr-filtered.vcf.gz
   tabix -p vcf ${vcf_basename}_vqsr-rejected.vcf.gz

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "7G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File vqsr_recalib_vcf_gz = "${vcf_basename}_vqsr-recalibrated.vcf.gz"
    File vqsr_recalib_vcf_gz_tbi = "${vcf_basename}_vqsr-recalibrated.vcf.gz.tbi"

    File vqsr_filtered_vcf_gz = "${vcf_basename}_vqsr-filtered.vcf.gz"
    File vqsr_filtered_vcf_gz_tbi = "${vcf_basename}_vqsr-filtered.vcf.gz.tbi"

    File vqsr_rejected_vcf_gz = "${vcf_basename}_vqsr-rejected.vcf.gz"
    File vqsr_rejected_vcf_gz_tbi = "${vcf_basename}_vqsr-rejected.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

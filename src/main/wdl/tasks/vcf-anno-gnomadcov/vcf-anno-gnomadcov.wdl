workflow vcf_anno_gnomadcov_workflow {

  meta {
    keywords: '{"keywords": ["vcf", "gnomad", "vcf annotation"]}'
    name: 'vcf_anno_gnomadcov'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Annotate vcf with gnomad v3 coverage'
    changes: '{"1.3.0": "latest dir removed", "1.2.0": "Exome made by different intervals, new Y-and-the-rest in genome (with MT)."}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_vcf_gz: '{"name": "Input vcf", "type": "String", "constraints": {"extension": [".vcf.gz"]}, "description": "Vcf file to be annotated"}'
    input_vcf_gz_tbi: '{"name": "Input vcf index", "type": "String", "constraints": {"extension": [".vcf.gz.tbi"]}, "description": "Index for vcf file to be annotated"}'
    input_chromosome: '{"name": "Chromosome", "type": "String", "constraints": {"values": ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"]}, "description": "Chromosome for analysis"}'
    input_genome_or_exome: '{"name": "Gnomad exome or genome", "type": "String", "values": ["exome", "genome"]}'

    output_annotated_with_gnomad_coverage_vcf_gz: '{"name": "Annotated vcf", "type": "File", "copy": "True", "description": "Vcf annotated with gnomad coverage"}'
    output_annotated_with_gnomad_coverage_vcf_gz_tbi: '{"name": "Annotated vcf tabix index", "type": "File", "copy": "True", "description": "Index for vcf annotated with gnomad coverage"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call vcf_anno_gnomadcov

}

task vcf_anno_gnomadcov {

  String task_name = "vcf_anno_gnomadcov"
  String task_version = "1.3.0"
  String vcf_basename = "no_id_provided"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  String chromosome # exmple: chr15
  String genome_or_exome = "exome"
  String docker_image = "intelliseqngs/task_vcf-anno-gnomadcov-" + genome_or_exome + ":1.3.0-" + chromosome

  # Tools runtime settings, paths etc.
  String tsv_file = "/resources/gnomad-coverage/v3/" + chromosome + ".coverage.tsv.gz"

  File vcf_gz
  File vcf_gz_tbi

  command <<<
  set -e -o pipefail
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


  python3 /intelliseqtools/annotate-vcf-with-tsv.py --input-vcf ${vcf_gz} --annotation-tsv /resources/gnomad-coverage/v3/${chromosome}.coverage.tsv.gz \
    --annotation-column 3 \
    --annotation-name ISEQ_GNOMAD_COV_MEAN \
    --annotation-description "Gnomad v3 mean coverage" \
    --annotation-type String \
    --annotation-number 1 \
    --annotation-column 4 \
    --annotation-name ISEQ_GNOMAD_COV_OVER \
    --annotation-description "Gnomad v3 fraction with minimum one coverage" \
    --annotation-type String \
    --annotation-number 1 | bgzip -c > ${chromosome}-${vcf_basename}_anno-cov.vcf.gz
  tabix -p vcf ${chromosome}-${vcf_basename}_anno-cov.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File annotated_with_gnomad_coverage_vcf_gz = "${chromosome}-${vcf_basename}_anno-cov.vcf.gz"
    File annotated_with_gnomad_coverage_vcf_gz_tbi = "${chromosome}-${vcf_basename}_anno-cov.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

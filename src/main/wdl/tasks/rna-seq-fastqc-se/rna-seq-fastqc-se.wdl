workflow rna_seq_fastqc_se_workflow {

  meta {
    keywords: '{"keywords": ["FastQC"]}'
    name: 'rna_seq_fastqc_se'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'A quality control tool for high throughput sequence data.'
    changes: '{"1.0.4": "update to new template"}'

    input_fastq_1: '{"name": "fastq_1", "type": "File", "description": "Fastq file 1 (single-end)"}'

    output_zip_files: '{"name": "zip_files", "type": "Array[File]", "copy": "True", "description": "zip files from FastQC"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call rna_seq_fastqc_se

}

task rna_seq_fastqc_se {

  File fastq_1

  String sample_id = "no_id_provided"
  Int num_cpu = 4


  String task_name = "rna_seq_fastqc_se"
  String task_version = "1.0.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/fastqc:1.2.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    mkdir quality-check
    cd quality-check/

    ## FastQC
    zcat ${fastq_1} | fastqc -t ${num_cpu} stdin:${sample_id}

    cd ..

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "8G"
    cpu: num_cpu
    maxRetries: 2

  }

  output {

    Array[File] zip_files = glob("quality-check/*.zip")

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

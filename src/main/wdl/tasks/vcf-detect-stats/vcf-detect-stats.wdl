workflow vcf_detect_stats_workflow {

  meta {
    keywords: '{"keywords": ["detection-chance", "genotyping", "clinvar", "coverage"]}'
    name: 'vcf_detect_stats'
    author: 'https://gitlab.com/moni.krzyz'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'vcf_detect_stats \n Generic text for task'
    changes: '{"2.1.0": "Update docker image."}'
    
    input_sample_id: '{"name": "sample id", "type": "String", "description": "identifier of sample"}'
    input_genotyped_vcf_gz: '{"name": "genotyped_vcf_gz", "extension": [".vcf.gz"], "type": "File", "copy": "True", "description": "Vcf genotyped by interval vcf"}'
    input_panel_json: '{"name": "panel_json", "type": "File",  "extension": [".json"], "description": "Panel data in json format, must include attributes (even if without values): genes_number, genes"}'
    input_panel_coverage_json: '{"name": "panel_coverage_json", "type": "File", "copy": "True", "description": "Json with [%] of bases with coverage > 10."}'
  
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_detection_stats_json: '{"name": "detection_stats_json", "type": "File", "description": "Json with detection chance for each gene genotyped by intervals"}'
    
  }

  call vcf_detect_stats

}

task vcf_detect_stats {

  String task_name = "vcf_detect_stats"
  String task_version = "2.1.2"

  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-detect-stats:2.1.0"

  File genotyped_vcf_gz
  File? panel_json
  File? panel_coverage_json
  
  String sample_id = "no_id_provided"

  command <<<
  set -e
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  tabix -p vcf ${genotyped_vcf_gz}

  python3 /intelliseqtools/vcf-detect-stats.py \
    --vcf ${genotyped_vcf_gz} \
    -p "${panel_json}" \
    -o "${sample_id}_detection-stats.json" \
    -c "${panel_coverage_json}"

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"
    File detection_stats_json = "${sample_id}_detection-stats.json"

  }

}

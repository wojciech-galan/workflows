# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: run_germline_cnv_caller_cohort_mode
#  authors:
#    - https://gitlab.com/lltw,
#  copyright: Copyright 2019 Intelliseq
#  description: >
#    WDL implementation of GTAK4 GermlineCNVCaller in COHORT mode.
#    Overview:
#    Calls copy-number variants in germline samples given their counts and the output of DetermineGermlineContigPloidy
#  changes:
#    latest:
#      - no changes
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow run_germline_cnv_caller_cohort_mode_workflow { call run_germline_cnv_caller_cohort_mode }

task run_germline_cnv_caller_cohort_mode {

# @Input (name = "HDF5 count files")
  Array[File] counts_hdf5

# @Input (name = "Germline contig ploidy calls", desc = "Compressed directory containg contig ploidy calls. This file is an output of 'determine_germline_contig_ploidy_cohort_mode' task")
  File germline_contig_ploidy_cohort_calls_tar_gz

# @Input (name = "Interval list")
  File interval_list

# @Input(name = "Sample ID")
  String cohort_name = "no_cohort_name_provided"

# Docker image
  String docker_image = "broadinstitute/gatk:4.1.3.0"

  # Tools runtime settings, paths etc.
  String gatk_path = "/gatk/gatk"
  String java_opt = "-Xms4000m"

  # Task name and task version
  String task_name = "run_germline_cnv_caller_cohort_mode"
  String task_version = "latest"

  command <<<

    ### start time
    starttime=$(date +%s)

    mkdir germline-contig-ploidy-calls
    tar -xzf ${germline_contig_ploidy_cohort_calls_tar_gz} -C germline-contig-ploidy-calls --strip-components=1

    ${gatk_path} --java-options ${java_opt} \
      GermlineCNVCaller \
      --run-mode COHORT \
      --intervals ${interval_list} \
      --interval-merging-rule OVERLAPPING_ONLY \
      --contig-ploidy-calls germline-contig-ploidy-calls \
      --input ${sep=' --input ' counts_hdf5} \
      --output . \
      --output-prefix ${cohort_name}

    tar -zcvf ${cohort_name}-germline-cnv-caller-calls.tar.gz ${cohort_name}-calls
    tar -zcvf ${cohort_name}-germline-cnv-caller-model.tar.gz ${cohort_name}-model
    tar -zcvf ${cohort_name}-germline-cnv-caller-tracking.tar.gz ${cohort_name}-tracking

    ### bioobject
    if [ -e "/resources.bioobject.json" ]; then RESOURCES=$(cat /resources.bioobject.json); else RESOURCES="[]"; fi
    if [ -e "/tools.bioobject.json" ]; then TOOLS=$(cat /tools.bioobject.json); else TOOLS="[]"; fi
    printf CPU=$(lscpu | grep '^CPU(s)' | grep -o '[0-9]*')
    printf MEMORY=$(cat /proc/meminfo | grep MemTotal | grep -o '[0-9]*' |  awk '{ print $1/1024/1024 ; exit}')
    finishtime=$(date +%s)
    printf TASKTIME=$((finishtime-starttime))

    printf "{\
      \"task-name\":\"${task_name}\",\
      \"task-version\":\"${task_version}\",\
      \"docker-image\":\"${docker_image}\",\
      \"cpu\":\"$CPU\",\
      \"memory\":\"$MEMORY\",\
      \"task-processing-time\":\"$TASKTIME\",\
      \"resources\":$RESOURCES,\
      \"tools\":$TOOLS\
      }" | sed 's/ //g' > bioobject.json

  >>>

  runtime {

    docker: docker_image
    memory: "500M"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File germline_cnv_caller_calls_tar_gz = "${cohort_name}-germline-cnv-caller-calls.tar.gz"
    File germline_cnv_caller_model_tar_gz = "${cohort_name}-germline-cnv-caller-model.tar.gz"
    File germline_cnv_caller_tracking_tar_gz = "${cohort_name}-germline-cnv-caller-tracking.tar.gz"

    # @Output(required=true,directory="/run_germline_cnv_caller_cohort_mode",filename="stdout.log")
    File stdout_log = stdout()
    # @Output(required=true,directory="/run_germline_cnv_caller_cohort_mode",filename="stderr.log")
    File stderr_log = stderr()
    # @Output(required=true,directory="/run_germline_cnv_caller_cohort_mode",filename="bioobject.json")
    File bioobject = "bioobject.json"

  }

}

workflow vcf_anno_civic_workflow
{
  call vcf_anno_civic
}

task vcf_anno_civic {

  String task_name = "vcf_anno_civic"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-anno-civic:1.0.0"

  String sample_id = "no_id_provided"
  File vcf_gz
  File vcf_gz_tbi
  String civic_vcf = "/resources/civic/*/civic-hg38.vcf.gz"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   bcftools annotate ${vcf_gz} \
      -a ${civic_vcf} \
      -c INFO/ISEQ_CIVIC_VT:=INFO/VT,INFO/ISEQ_CIVIC_CSQ:=INFO/CSQ \
      -o ${sample_id}_annotated.vcf.gz -O z
   tabix -p vcf ${sample_id}_annotated.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File annotated_vcf = "${sample_id}_annotated.vcf.gz"
    File annotated_vcf_tbi = "${sample_id}_annotated.vcf.gz.tbi"
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

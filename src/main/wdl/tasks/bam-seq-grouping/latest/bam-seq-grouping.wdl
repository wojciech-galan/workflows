workflow bam_seq_grouping_workflow {

  meta {
    keywords: '{"keywords": ["bam"]}'
    name: 'bam_seq_grouping'
    author: 'https://gitlab.com/marysiaa'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Create sequence grouping files \n Create sequence grouping files from hg38 reference genome'
    changes: '{"latest": "no changes"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'

  }

  call bam_seq_grouping

}

task bam_seq_grouping {

  String task_name = "bam_seq_grouping"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String reference_genome = 'hg38' # another available option: "grch38-no-alt"
  String docker_image = "intelliseqngs/task_bam-seq-grouping-"+ reference_genome +":1.0.0"

  command <<<
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/after-start.sh)

  cat /resources/sequence_grouping.txt > sequence_grouping.txt
  cat /resources/sequence_grouping.txt > sequence_grouping_with_unmapped.txt
  echo "" >> sequence_grouping_with_unmapped.txt
  echo "unmapped" >> sequence_grouping_with_unmapped.txt

  source <(curl -s https://gitlab.com/intelliseq/workflows/raw/dev/src/main/scripts/bco/1.0.1/before-finish.sh)
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File sequence_grouping_txt = "sequence_grouping.txt"
    File sequence_grouping_with_unmapped_txt = "sequence_grouping_with_unmapped.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

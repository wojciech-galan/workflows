workflow pileup_calc_contam_workflow
{
  call pileup_calc_contam
}

task pileup_calc_contam {

  String task_name = "pileup_calc_contam"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/gatk-4.2.0.0:1.0.0"

  String sample_id = "no_id_provided"
  File tumor_table
  File? normal_table
  String java_mem = "-Xmx3500m"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}


   gatk --java-options "${java_mem}" CalculateContamination\
        -I ${tumor_table} \
        -O ${sample_id}_contamination.table \
        --tumor-segmentation ${sample_id}_segments.table \
         ${"--matched-normal " + normal_table}


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File contamination_table = "${sample_id}_contamination.table"
    File tumor_segments_table = "${sample_id}_segments.table"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

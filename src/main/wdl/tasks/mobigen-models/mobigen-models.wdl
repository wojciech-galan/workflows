workflow mobigen_models_workflow {

  meta {
    keywords: '{"keywords": ["mobigen", "polygenic risc score"]}'
    name: 'Polygenic risk score models'
    author: 'https://gitlab.com/wojciech-galan'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Calculates polygenic risk scores and classifies patient to one of possible categories'
    changes: '{"1.0.2": "removed some vitalleo-specific content from models\' outputs"}'

    input_curated_initial_vcf_paths: '{"name": "Initial vcf", "type": "Array[File]", "description": "Gzipped files in vcf format (one for each chromosome) containing patient genotype"}'
    input_imputed_files: '{"name": "Imputed files", "type": "Array[File]", "description": "Files in vcf format containing imputed missing genotypes"}'

    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
    output_out_files: '{"name": "Output files", "type": "Array[File]", "copy": "True", "description": "Output files in JSON format"}'
    output_imputed_files_and_indices: '{"name": "Indexed imputed files", "type": "Array[File]", "copy": "True", "description": "Imputed files indexed on rsid"}'
    output_initial_files_and_indices: '{"name": "Indexed initial files", "type": "Array[File]", "copy": "True", "description": "Initial files indexed on rsid"}'

  }

  call mobigen_models

}

task mobigen_models {

  String task_name = "mobigen_models"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name

  Array [File] imputed_files
  Array[File] curated_initial_vcf_paths
  String population_name = "nfe"
  String log_file_name = "mobigen-models.log"
  String trait_directories = "vitalleo_traits pgs_traits"
  String docker_image = "intelliseqngs/mobigen-models:2.0.2"
  Int num_of_processes = 8

  #todo $3 "!~ /;rs/" excludes lines containing valiants with multiple ids. They should be split instead of excluding
  command <<<
  set -e
  task_name="${task_name}"; task_name_with_index="${task_name_with_index}"; task_version="${task_version}"; task_docker="${docker_image}"
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  mkdir imputed
  for file in ${sep=' ' imputed_files}; do
    echo $(date) "Splitting file $file";
    zgrep -v "^#" $file | awk -F"\t" '{OFS=FS}{$1=substr($3, length($3)-2)}{$2=substr($3,3); if($3 ~ /rs/ && $3 !~ /;rs/) print>>"imputed/"$1}';
  done
  zgrep "^#" $file > header;
  find imputed -type f | xargs -n1 -P${num_of_processes} -I % sh -c 'echo "sorting and makind index for %"; sort -k1,1V -k2,2n % > %_sorted; cat header %_sorted > %.vcf; rm %_sorted; rm %; bgzip %.vcf; bcftools index %.vcf.gz'

  mkdir initial
  for file in ${sep=' ' curated_initial_vcf_paths}; do
    echo $(date) "Splitting file $file";
    zgrep -v "^#" $file | awk -F"\t" '{OFS=FS}{$1=substr($3, length($3)-2)}{$2=substr($3,3); if($3 ~ /rs/ && $3 !~ /;rs/) print>>"initial/"$1}';
  done
  zgrep "^#" $file > header;
  find initial -type f | xargs -n1 -P${num_of_processes} -I % sh -c 'echo "sorting and makind index for %"; sort -k1,1V -k2,2n % > %_sorted; cat header %_sorted > %.vcf; rm %_sorted; rm %; bgzip %.vcf; bcftools index %.vcf.gz'

  mobigen --imputed_files imputed/*vcf.gz \
         --log_file ${log_file_name} \
         --curated_initial_vcfs initial/*vcf.gz \
         --traits_dirs ${trait_directories}

  printf "\n--------------------------------\n${log_file_name} content:\n" >/dev/stderr
  cat ${log_file_name} > /dev/stderr
  printf "\n--------------------------------\n" >/dev/stderr

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "4G"
    cpu: num_of_processes
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

    Array[File] out_files = glob("*.sample.json")
    Array[File] imputed_files_and_indices = glob("imputed/*vcf.gz*")
    Array[File] initial_files_and_indices = glob("initial/*vcf.gz*")
  }

}

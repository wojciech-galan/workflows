workflow vcf_acmg_pp4_workflow
{
  call vcf_acmg_pp4
}

task vcf_acmg_pp4 {

  String task_name = "vcf_acmg_pp4"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_vcf-acmg-pp4:1.0.0"

  File vcf
  String sample_id = "no-id"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   python3 /intelliseqtools/acmg-pp4.py \
      --input-vcf ${vcf} \
      --output-vcf ${sample_id}_annotated-with-acmg.vcf.gz

   tabix -p vcf ${sample_id}_annotated-with-acmg.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File annotated_acmg_vcf_gz = "${sample_id}_annotated-with-acmg.vcf.gz"
    File annotated_acmg_vcf_gz_tbi = "${sample_id}_annotated-with-acmg.vcf.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

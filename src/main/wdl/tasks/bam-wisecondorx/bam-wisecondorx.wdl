workflow bam_wisecondorx_workflow
{
  call bam_wisecondorx
}

task bam_wisecondorx {

  File bam
  File bai

  String sample_id = 'no_id_provided'

  String task_name = "bam_wisecondorx"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/task_bam-wisecondorx:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   WisecondorX convert ${bam} ${sample_id}.npz
   WisecondorX predict --bed --plot ${sample_id}.npz /resources/reference.npz ${sample_id}

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    Array[File] plots = glob("${sample_id}.plots/*")
    File aberrations = "${sample_id}_aberrations.bed"
    File bins = "${sample_id}_bins.bed"
    File segments = "${sample_id}_segments.bed"
    File statistics = "${sample_id}_statistics.txt"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

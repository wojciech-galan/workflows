workflow sv_smoove_workflow {

  meta {
    keywords: '{"keywords": ["SV", "calling"]}'
    name: 'SV smoove'
    author: 'https://gitlab.com/kattom'
    copyright: 'Copyright 2020 Intelliseq'
    description: 'Calls structural variants with lumpy/smoove. Optionally genotypes and adds  duphold annotations'
    changes: '{"1.0.2": "two strange contigs excluded", "1.0.1": "bcftools index forced"}'

    input_sample_id: '{"name": "sample id", "type": "String", "description": "Sample identifier"}'
    input_bam: '{"name": "Bam", "type": "File", "extension": [".bam"], "description": "Coordinate sorted input bam"}'
    input_bai: '{"name": "Bai", "type": "File", "extension": [".bam.bai", ".bai"], "description": "Index for the input bam"}'
    input_exclude_lcr: '{"name": "Exclude LCR", "type": "Boolean", "description": "This option decides whether exclude low complexity regions from analysis"}'
    input_lcr_bed_source: '{"name": "LCR bed", "type": "String", "values": ["hall-lab","btu356"],"description": "This option decides which low complexity regions bam should be used"}'
    input_exclude_chroms: '{"name": "Exclude some chromosomes?", "type": "Boolean", "description": "This option decides whether exclude non canonical chromosomes from analysis"}'
    input_run_duphold: '{"name": "Run duphold?", "type": "Boolean", "description": "This option decides whether add duphold annotations (changes in coverage)"}'
    input_reference_genome: '{"name": "Reference genome", "type": "String", "values": ["hg38", "grch38-no-alt"], "description": "Version of the reference genome, should be the same as version used for alignment"}'
    input_run_svtyper: '{"name": "Run svtyper?", "type": "Boolean", "description": "This option decides whether perform genotyping"}'

    output_sv_vcf_gz: '{"name": "SV vcf gz", "type": "File", "copy": "True", "description": "Vcf file with structural variants identified by lumpy/smoove"}'
    output_sv_vcf_gz_csy: '{"name": "SV vcf gz csi", "type": "File", "copy": "True", "description": "SV vcf file index"}'
    output_stdout_log: '{"name": "Standard out", "type": "File", "copy": "True", "description": "Standard out"}'
    output_stderr_log: '{"name": "Standard err", "type": "File", "copy": "True", "description": "Standard error"}'
    output_bco: '{"name": "Biocompute object", "type": "File", "copy": "True", "description": "Biocompute object"}'
  }

  call sv_smoove

}

task sv_smoove {

  String task_name = "sv_smoove"
  String task_version = "1.0.2"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String reference_genome = "hg38"
  String docker_image = "intelliseqngs/smoove-" + reference_genome + ":1.0.0"

  File bam
  File bai
  String sample_id = "no-id"
  # This decided whether exclude low complexity regions from analysis
  Boolean exclude_lcr = true

  # This decides which source of excluded regions should be used:
  # "hall-lab" (https://github.com/hall-lab/speedseq/raw/master/annotations/exclude.cnvnator_100bp.GRCh38.20170403.bed) or
  # "btu356" (https://doi.org/10.1093/bioinformatics/btu356, Supplementary materials)
  String lcr_bed_source = "hall-lab"

  String lcr_bed_source_dir = if lcr_bed_source == "hall-lab" then "hall-lab-hg38-03-04-2017" else "hg38-btu356"
  String LCR_command = if (exclude_lcr) then "--exclude /resources/low-complexity-regions/" + lcr_bed_source_dir + "/LCR.bed" else ""

  # This decides whether exclude "strange" chromosomes from analysis and defines "strange" chromosomes
  Boolean exclude_chroms = true
  String chroms_string = "chrM,~chrUn,~random,~:,~decoy,~_alt,chrEBV,hs38d1,chrMT"
  String chroms_command = if (exclude_chroms) then "--excludechroms "+ chroms_string else ""

  # This decides whether run duphold annotation (only changes in coverage)
  Boolean run_duphold = false
  String duphold_command = if (run_duphold) then "--duphold" else ""

   # This decides whether run genotyping (svtyper)
  Boolean run_svtyper = true
  String svtyper_command = if (run_svtyper) then "--genotype --removepr" else ""


  command <<<
  ## set -eo pipefail is not used intentionally (due to index problem)
  bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

  ref_genome=$( ls /resources/reference-genomes/*/*.fa )

  smoove call ${bam} \
         --fasta $ref_genome \
         --name ${sample_id} \
         ${svtyper_command} \
         ${duphold_command} \
         ${chroms_command} \
         ${LCR_command} \
         -p 4 2> smoove.stderr


  if grep -q 'panic:' smoove.stderr; then
      echo "Smoove error occured"
      cat smoove.stderr >> stderr
      exit 1
  fi
  cat smoove.stderr >> stderr
  bcftools index -f ${sample_id}-smoove*.vcf.gz

  bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: "4"
    maxRetries: 2

  }

  output {

    File sv_vcf_gz = glob("${sample_id}-smoove*vcf.gz")[0]
    File sv_vcf_gz_csi = glob("${sample_id}-smoove*vcf.gz.csi")[0]
    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}

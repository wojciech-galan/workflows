# Germline Targeted Sequencing Pipeline

import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@1.1.1/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@1.2.4/src/main/wdl/tasks/panel-generate/latest/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/resources-panels@1.0.8/src/main/wdl/tasks/resources-panels/resources-panels.wdl" as resources_panels_task
import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.0.1/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-qc@1.3.1/src/main/wdl/modules/fq-qc/fq-qc.wdl" as fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.4.0/src/main/wdl/modules/fq-bwa-align/latest/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-filter-contam@1.0.3/src/main/wdl/modules/bam-filter-contam/bam-filter-contam.wdl" as bam_filter_contam_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling@1.1.0/src/main/wdl/modules/bam-varcalling/bam-varcalling.wdl" as bam_varcalling_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno@1.5.0/src/main/wdl/modules/vcf-anno/latest/vcf-anno.wdl" as vcf_anno_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-report@1.1.0/src/main/wdl/modules/vcf-acmg-report/latest/vcf-acmg-report.wdl" as vcf_acmg_report_module
#import "https://gitlab.com/intelliseq/workflows/raw/coverage-statistics@1.0.0/src/main/wdl/modules/coverage-statistics/latest/coverage-statistics.wdl" as coverage_statistics_module
#import "https://gitlab.com/intelliseq/workflows/raw/detection-chance@1.2.0/src/main/wdl/modules/detection-chance/latest/detection-chance.wdl" as detection_chance_module
import "https://gitlab.com/intelliseq/workflows/raw/sex-check@1.0.2/src/main/wdl/modules/sex-check/sex-check.wdl" as sex_check_module
import "https://gitlab.com/intelliseq/workflows/raw/pdf-merge@1.1.1/src/main/wdl/tasks/pdf-merge/latest/pdf-merge.wdl" as pdf_merge_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task
import "https://gitlab.com/intelliseq/workflows/-/raw/report-bco@1.0.1/src/main/wdl/tasks/report-bco/latest/report-bco.wdl" as report_bco_task

workflow target_seq {

  meta {
    name: 'Germline target-seq workflow'
    author: 'https://gitlab.com/marpiech'
    copyright: 'Copyright 2019-2020 Intelliseq'
    description: '## Workflow for identification of pathogenic variants\nValidated for gene panels (including hereditary cancer) and whole exome data. Generates diagnostic report. CE certified pending.'
    released: 'true'

    input_sample_id: '{"name": "Sample id", "type": "String", "default": "no_id_provided", "description": "Enter a sample name (or identifier)"}'
    input_fastqs: '{"name": "Fastq files", "type": "Array[File]", "extension": [".fq.gz", ".fastq.gz"], "description": "Choose list of paired gzipped fastq files both left and right [.fq.gz or .fastq.gz]"}'
    input_fastqs_left: '{"hidden":"true", "name": "First (left) fastq files", "type": "Array[File]", "extension": [".fq.gz", ".fastq.gz"], "description": "Choose first (left) fastq files"}'
    input_fastqs_right: '{"hidden":"true", "name": "Second (right) fastq files", "type": "Array[File]", "extension": [".fq.gz", ".fastq.gz"], "description": "Choose second (right) fastq files"}'

    input_hpo_terms: '{"hidden":"true", "name": "HPO terms", "type": "String", "description": "Enter HPO terms to narrow your search/analysis results (separate HPO terms with non-alphanumeric characters, for example: HP:0004942, HP:0011675)"}'
    input_genes: '{"hidden":"true", "name": "Genes names", "type": "String", "description": "Enter gene names to narrow your search/analysis results (separate gene names with non-alphanumeric characters, for example: HTT, FBN1)"}'
    input_diseases: '{"hidden":"true", "name": "Diseases", "type": "String", "description": "Enter disease names to narrow your search/analysis results (separate diseases names with comma; each disease name should be just a keyword, for example for Marfan Syndrome only Marfan should be written, for Ehlers-Danlos Syndrome: Ehlers-Danlos; other proper diseases names for example: Osteogenesis imperfecta, Tay-sachs, Hemochromatosis, Brugada, Canavan, etc.)"}'

    input_panel_json: '{"hidden":"true", "name": "Genes panel", "type": "File", "extension": [".json"], "description": "Add json file with genes panel to narrow your search/analysis results. You can prepare this file using workflow Gene panel genetor"}'
    input_phenotypes_json: '{"hidden":"true", "name": "List of phenotypes names", "type": "File", "extension": [".json"], "description": "Add json file with phenotypes names used to genetate gene panel. You can prepare this file using workflow Gene panel genetor"}'

    input_genome_or_exome: '{"hidden":"true", "name": "Sample type", "type": "String", "default": "exome", "constraints": {"values": ["exome", "genome"]}, "description": "Select exome or genome (refers to the input sample)"}'
    input_kit: '{"hidden":"true", "name": "Kit", "type": "String", "default": "exome-v7", "constraints": {"values": ["exome-v6", "exome-v7", "genome"]}, "description": "Choose comprehensive exome kit from exome-v6 for sure-select exome v6 or exome-v7 for sure-select exome v7. For WGS sample choose genome"}'
    input_interval_list: '{"hidden":"true", "name": "Intervals",  "type": "File", "extension": [".interval_list"], "description": "List of genomic intervals. Default sure-select exome v7"}'

    input_patient_json: '{"hidden":"true", "name": "Patient information", "type": "File", "extension": [".json"], "description": "Add patient data in json format, must include attributes (even if without values): name, surname, sex, birthdate and pesel"}'
    input_sample_json: '{"hidden":"true", "name": "Sample information ", "type": "File", "extension": [".json"], "description": "Add sample data in json format, must include attributes (even if without values): ID, material, sequencing_type, sequencing_platform, sending_date, raport_date, doctor_name"}'

    input_other_bams: '{"hidden":"true", "name": "Other bams", "type": "Array[File]", "extension": [".bam"], "description": "Bams files used for igv-screenshots comparison"}'
    input_other_bais: '{"hidden":"true", "name": "Other bais", "type": "Array[File]", "extension": [".bai"], "description": "Bais files needed for igv-screenshots comparison"}'

    input_add_bam_filter: '{"hidden":"true", "name": "Add bam filter?", "type": "Boolean", "default": "true", "description": "This option decides whether to apply bam filtering on contamination. Default: true"}'
    input_mismatch_threshold: '{"hidden":"true", "name": "Mismatch threshold", "type": "Float", "constraints": {"min": "0.0", "max": "1.0"}, "default": "0.1", "description": "Reads with fraction of mismatches greater than or equal this value are discarded"}'


    output_fastqc_1_zips: '{"name": "FastQC output zip", "type": "Array[File]", "copy": "True", "description": "FastQC results for the first fastq files"}'
    output_fastqc_2_zips: '{"name": "FastQC output zip", "type": "Array[File]", "copy": "True", "description": "FastQC results for the second fastq files"}'
    output_quality_check_pdf: '{"name": "Quality check pdf", "type": "File", "copy": "True", "description": "Fastq quality check report in pdf format"}'
    output_quality_check_html: '{"name": "Quality check html", "type": "File", "copy": "True", "description": "Fastq quality check report in html format"}'

    output_final_bam: '{"name": "bam", "type": "File", "copy": "True", "description": "Alignment result"}'
    output_final_bai: '{"name": "bai", "type": "File", "copy": "True", "description": "Alignment result index"}'

    output_gvcf_gz: '{"name": "g.vcf.gz", "type": "File", "copy": "True", "description": "Genotyping result"}'
    output_gvcf_gz_tbi: '{"name": "g.vcf.gz.tbi", "type": "File", "copy": "True", "description": "Genotyping result index"}'
    output_vcf_gz: '{"name": "vcf.gz", "type": "File", "copy": "True", "description": "Variants"}'
    output_vcf_gz_tbi: '{"name": "vcf.gz.tbi", "type": "File", "copy": "True", "description": "Variants index"}'
    output_final_realigned_bam: '{"name": "realigned_bam", "type": "File", "copy": "True", "description": "realigned_bam"}'
    output_final_realigned_bai: '{"name": "realigned_bai", "type": "File", "copy": "True", "description": "realigned_bai"}'

    output_annotated_vcf: '{"name": "annotated.vcf.gz", "type": "File", "copy": "True", "description": "Annotation result"}'
    output_annotated_vcf_tbi: '{"name": "annotated.vcf.gz.tbi", "type": "File", "copy": "True", "description": "Annotation result index"}'

    output_ang_pdf_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, pdf format, English version"}'
    output_ang_html_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, html format, English version"}'
    output_ang_docx_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, docx format, English version"}'
    output_ang_odt_report: '{"name": "Report from genetic analysis in English", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, odt format, English version"}'

    output_pdf_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, pdf format, Polish version"}'
    output_html_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, html format, Polish version"}'
    output_docx_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, docx format, Polish version"}'
    output_all_reports_pdf: '{"name": "All pdf reports from genetic analysis", "type": "Array[File]", "copy": "True", "description": "All pdf reports from genetic analysis"}'
    output_odt_report: '{"name": "Report from genetic analysis in Polish", "type": "File", "copy": "True", "description": "Report with results of the genetic analysis, odt format, Polish version"}'
    output_csv: '{"name": "csv", "type": "File", "copy": "True", "description": "Table with variants"}'
    output_igv_screenshots_tar_gz: '{"name": "igv screenshots tar", "type": "File", "copy": "True", "description": "Igv screenshots of alignment at variant positions"}'

    output_simple_coverage_stats_report_pdf: '{"name": "pdf", "type": "File", "copy": "True", "description": "pdf human readeble coverage statistics report"}'
    output_coverage_stats_report_pdf: '{"name": "pdf", "type": "File", "copy": "True", "description": "pdf coverage statistics report based on gatk nad samtools"}'
    output_full_report_pdf: '{"name": "pdf", "type": "File", "copy": "True", "description": "all pdf reports merged into one pdf file"}'

  }


  String sample_id = "no_id_provided"

  Array[File]? fastqs
  Boolean is_fastqs_defined = defined(fastqs)
  Array[File]? fastqs_left
  Array[File]? fastqs_right

  File? panel_json
  File? phenotypes_json
  Boolean is_panel_json_defined = defined(panel_json)
  Boolean is_phenotypes_json_defined = defined(phenotypes_json)
  String? hpo_terms
  String? genes
  String? diseases
  Boolean is_hpo_terms_or_genes_or_diseases_defined = (defined(hpo_terms) || defined(genes) || defined(diseases))

  File? patient_json
  File? sample_json

  File? error_warning_json_qc # It needs to be add to meta section
  File? error_warning_json_coverage # It needs to be add to meta section

  File? interval_list
  String genome_or_exome = "exome"
  String kit = "exome-v7"
  String kit_choice = if (genome_or_exome == "genome") then "genome" else kit
  Boolean is_interval_list_not_defined = !defined(interval_list)

  Array[File]? other_bams = []
  Array[File]? other_bais = []
  Boolean add_bam_filter = true
  Float mismatch_threshold = 0.1

  String pipeline_name = "target_seq"
  String pipeline_version = "1.19.1"

  # 1.  Organize fastqs
  if(is_fastqs_defined) {
    call fq_organize_task.fq_organize {
      input:
      fastqs = fastqs
    }
  }
  Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
  Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])

  if(is_hpo_terms_or_genes_or_diseases_defined) {
    call panel_generate_task.panel_generate {
      input:
      sample_id = sample_id,
      hpo_terms = hpo_terms,
      genes = genes,
      diseases = diseases
    }
  }

  # 2. Prepare gene panel
  if((!is_phenotypes_json_defined || !is_panel_json_defined) && !is_hpo_terms_or_genes_or_diseases_defined) {
    call resources_panels_task.resources_panels {
    }
  }

  File gene_panel_json = select_first([panel_json, panel_generate.panel, resources_panels.panel])

  File gene_phenotypes_json = select_first([phenotypes_json, panel_generate.phenotypes, resources_panels.phenotypes])

  # 3. Fill interval_list if not declared
  if(is_interval_list_not_defined) {
    call resources_kit_task.resources_kit {
      input:
      kit = kit_choice
    }
  }
  File interval_file = select_first([interval_list, resources_kit.interval_list])

  # 4. Check quality of fastq files
  call fq_qc_module.fq_qc {
    input:
      sample_id = sample_id,
      fastqs_left = fastqs_1,
      fastqs_right = fastqs_2
  }

  # 5. Align bam files
  call fq_bwa_align_module.fq_bwa_align {
    input:
      fastqs_left = fastqs_1,
      fastqs_right = fastqs_2,
      sample_id = sample_id
  }

  # 6. Discarde reads with fraction of mismatches greater than or equal than mismatch_threshold
  if(add_bam_filter) {
    call bam_filter_contam_module.bam_filter_contam {
      input:
        bam = fq_bwa_align.recalibrated_markdup_bam,
        bai = fq_bwa_align.recalibrated_markdup_bai,
        sample_id = sample_id,
        mismatch_threshold = mismatch_threshold
    }
  }

  File bam_to_variant_calling = select_first([bam_filter_contam.filtered_bam, fq_bwa_align.recalibrated_markdup_bam])
  File bai_to_variant_calling = select_first([bam_filter_contam.filtered_bai, fq_bwa_align.recalibrated_markdup_bai])

  # 7. Call variants
  call bam_varcalling_module.bam_varcalling {
    input:
      input_bam = bam_to_variant_calling,
      input_bai = bai_to_variant_calling,
      sample_id = sample_id,
      interval_list = interval_file
  }

  # 8. Annotate and filter variants
  call vcf_anno_module.vcf_anno {
    input:
      vcf_gz = bam_varcalling.vcf_gz,
      vcf_gz_tbi = bam_varcalling.vcf_gz_tbi,
      gnomad_coverage_genome_or_exome = genome_or_exome,
      vcf_basename = sample_id
  }

  # 9. Annotate variants acroding to ACMG recomendation
  call vcf_acmg_report_module.vcf_acmg_report {
    input:
      panel_json = gene_panel_json,
      sample_json = sample_json,
      phenotypes_json = gene_phenotypes_json,
      patient_json = patient_json,
      vcf_gz = vcf_anno.annotated_and_filtered_vcf,
      vcf_gz_tbi = vcf_anno.annotated_and_filtered_vcf_tbi,
      bam = bam_to_variant_calling,
      bai = bai_to_variant_calling,
      realigned_bam = bam_varcalling.haplotype_caller_bam,
      realigned_bai = bam_varcalling.haplotype_caller_bai,
      other_bams = other_bams,
      other_bais = other_bais,
      sample_id = sample_id
  }

  # 10. Estimate detection chance
#  call detection_chance_module.detection_chance {
#   input:
#     sample_gvcf_gz = bam_varcalling.gvcf_gz,
#     sample_gvcf_gz_tbi = bam_varcalling.gvcf_gz_tbi,
#     panel_json = gene_panel_json
#  }

  # 11. Check quality of bam files
#  call coverage_statistics_m.coverage_statistics {
#    input:
#      intervals = interval_file,
#      bam_file = fq_bwa_align.filtered_bam,
#      bai_file = fq_bwa_align.filtered_bai,
#      error_warning_json_coverage = error_warning_json_coverage
#  }

  # 12. Genetic sex verification
  call sex_check_module.sex_check {
    input:
      bam = fq_bwa_align.recalibrated_markdup_bam,
      bai = fq_bwa_align.recalibrated_markdup_bai,
      vcf_gz = bam_varcalling.vcf_gz,
      vcf_gz_tbi = bam_varcalling.vcf_gz_tbi,
      sample_id = sample_id
        }

  # 13. Merge pdf reports
  Array[File] reports_pdf = [vcf_acmg_report.pdf_report, fq_qc.quality_check_report_pdf, sex_check.report_pdf]
  Array[File] reports_ang_pdf = [vcf_acmg_report.ang_pdf_report, fq_qc.quality_check_report_pdf, sex_check.report_pdf]

  call pdf_merge_task.pdf_merge {
    input:
      sample_id = sample_id,
      pdf = reports_pdf,
      ang_pdf = reports_ang_pdf
  }

  # 14. Merge BCO and prepare report pdf
  Array[File] bcos_module = select_all([fq_qc.bco, fq_bwa_align.bco, bam_varcalling.bco, vcf_anno.bco, bam_filter_contam.bco, sex_check.bco])
  Array[File] stdout_module = select_all([fq_qc.stdout_log, fq_bwa_align.stdout_log, bam_varcalling.stdout_log, vcf_anno.stdout_log, bam_filter_contam.stdout_log, sex_check.stdout_log])
  Array[File] stderr_module = select_all([fq_qc.stderr_log, fq_bwa_align.stderr_log, bam_varcalling.stderr_log, vcf_anno.stderr_log, bam_filter_contam.stderr_log, sex_check.stderr_log])

  call bco_merge_task.bco_merge as bco_merge_pipeline {
    input:
        bco_array = bcos_module,
        stdout_array = stdout_module,
        stderr_array = stderr_module,
        pipeline_name = pipeline_name,
        pipeline_version = pipeline_version
  }

  call report_bco_task.report_bco as report_bco_pipeline {
    input:
       bco_json = bco_merge_pipeline.bco
  }

  output {

    # 4. Check quality of fastq files
    Array[File] fastqc_1_zips = fq_qc.fastqc_1_zips
    Array[File] fastqc_2_zips = fq_qc.fastqc_2_zips
    File quality_check_pdf = fq_qc.quality_check_report_pdf
    File quality_check_html = fq_qc.quality_check_report_html

    # 5./6. Align (and filter) bam files
    File final_bam = bam_to_variant_calling
    File final_bai = bai_to_variant_calling

    # 7. Call variants
    File gvcf_gz = bam_varcalling.gvcf_gz
    File gvcf_gz_tbi = bam_varcalling.gvcf_gz_tbi

    File vcf_gz = bam_varcalling.vcf_gz
    File vcf_gz_tbi = bam_varcalling.vcf_gz_tbi

    File final_realigned_bam = bam_varcalling.haplotype_caller_bam
    File final_realigned_bai = bam_varcalling.haplotype_caller_bai

    # 8. Annotate and filter variants
    File annotated_vcf = vcf_anno.annotated_and_filtered_vcf
    File annotated_vcf_tbi = vcf_anno.annotated_and_filtered_vcf_tbi

    # 9. Annotate variants acroding to ACMG recomendation
    File annotated_acmg_vcf_gz = vcf_acmg_report.annotated_acmg_vcf_gz
    File annotated_acmg_vcf_gz_tbi = vcf_acmg_report.annotated_acmg_vcf_gz_tbi
    File igv_screenshots_tar_gz = vcf_acmg_report.igv_screenshots_tar_gz
    File csv_report = vcf_acmg_report.csv_report

    File docx_report = vcf_acmg_report.docx_report
    File pdf_report = vcf_acmg_report.pdf_report
    File html_report = vcf_acmg_report.html_report
    File odt_report = vcf_acmg_report.odt_report

    File ang_docx_repor = vcf_acmg_report.ang_docx_repor
    File ang_pdf_report = vcf_acmg_report.ang_pdf_report
    File ang_html_report = vcf_acmg_report.ang_html_report
    File ang_odt_report = vcf_acmg_report.ang_odt_report

    # 10. Estimate detection chance
#    File detection_chance_report_pdf = detection_chance.detection_chance_report_pdf
#    File detection_chance_report_odt = detection_chance.detection_chance_report_odt
#    File detection_chance_report_docx = detection_chance.detection_chance_report_docx
#    File detection_chance_report_html = detection_chance.detection_chance_report_html

    # 11. Check quality of bam files
#    File coverage_stats_report_pdf = coverage_statistics.coverage_report_pdf
#    File simple_coverage_stats_report_pdf = coverage_statistics.simple_coverage_report_pdf

    # 12. Genetic sex verification
    File sex_check_report_html = sex_check.report_html
    File sex_check_report_pdf = sex_check.report_pdf

    # 13. Merge pdf reports
    File full_report_pdf = pdf_merge.full_report_pdf
    File full_report_ang_pdf = pdf_merge.full_report_ang_pdf
    Array[File] all_reports_pdf = pdf_merge.all_reports_pdf

    File bco_report_pdf = report_bco_pipeline.bco_report_pdf
    File bco_report_odt = report_bco_pipeline.bco_report_odt
    File bco_report_docx = report_bco_pipeline.bco_report_docx
    File bco_report_html = report_bco_pipeline.bco_report_html

    # 14. Merge BCO and prepare report pdf
    File bco = bco_merge_pipeline.bco
    File stdout_log = bco_merge_pipeline.stdout_log

  }
}

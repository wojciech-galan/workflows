import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.0.1/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/bed-to-interval-list@1.2.0/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl" as bed_to_interval_list_task
import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@1.7.1/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.2.0/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-qc@1.4.8/src/main/wdl/modules/fq-qc/fq-qc.wdl" as fq_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.5.3/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/sv-calling@1.2.4/src/main/wdl/modules/sv-calling/sv-calling.wdl" as sv_calling_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling@1.3.4/src/main/wdl/modules/bam-varcalling/bam-varcalling.wdl" as bam_varcalling_module
import "https://gitlab.com/intelliseq/workflows/raw/mt-varcall@1.0.3/src/main/wdl/modules/mt-varcall/mt-varcall.wdl" as mt_varcall_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-report@2.1.6/src/main/wdl/modules/vcf-acmg-report/vcf-acmg-report.wdl" as vcf_acmg_report_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno@1.12.0/src/main/wdl/modules/vcf-anno/vcf-anno.wdl" as vcf_anno_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-qc@2.3.5/src/main/wdl/modules/bam-qc/bam-qc.wdl" as bam_qc_module
import "https://gitlab.com/intelliseq/workflows/raw/detection-chance@1.4.0/src/main/wdl/modules/detection-chance/detection-chance.wdl" as detection_chance_module
import "https://gitlab.com/intelliseq/workflows/raw/sex-check@1.0.2/src/main/wdl/modules/sex-check/sex-check.wdl" as sex_check_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-var-filter@1.0.2/src/main/wdl/modules/vcf-var-filter/vcf-var-filter.wdl" as vcf_var_filter_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.0.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow germline {

    String pipeline_name = "germline"
    String pipeline_version = "1.19.5"

    String sample_id = "no_id_provided"
    String reference_genome = "grch38-no-alt"

    # Provide fastq files if you would like to start analysis from the beginning
    Array[File]? fastqs
    Boolean is_fastqs_defined = defined(fastqs)
    Array[File]? fastqs_left
    Boolean is_fastqs_left_defined = defined(fastqs_left)
    Array[File]? fastqs_right

    # Provide bam file if you would like to start analysis from variant calling
    File? bam
    File? bam_bai

    # Gene panel
    String? hpo_terms
    String? genes
    String? diseases
    String? phenotypes_description
    Array[String]? panel_names
    Boolean is_input_for_panel_generate_defined = (defined(hpo_terms) || defined(genes) || defined(diseases) || defined(phenotypes_description) || defined(panel_names))

    # Patient information
    File? sample_info_json

    # Report information (time zone difference and ID)
    Int timezoneDifference = 0
    String? analysisSpecifier

    File? error_warning_json_qc # It needs to be add to meta section
    File? error_warning_json_coverage # It needs to be add to meta section

    String genome_or_exome = "exome" # Possible values "exome", "genome", "target"
    String analysis_start = "fastq"
    String analysis_group = "germline"

    # for WGS or WES
    String kit = "exome-v7" # Possible values "exome-v6", "exome-v7", "genome", "targeted"
    String kit_choice = if (genome_or_exome == "genome") then "genome" else kit

    # for targeted-sequencing
    File? target_bed
    Array[String]? target_genes
    Int padding = 1000
    Boolean panel_sequencing = if (genome_or_exome == "target") then true else false
    String freq_and_coverage_source = if (genome_or_exome == "exome") then "exome" else "genome"
    Boolean apply_panel_filter = if (genome_or_exome == "target") then false else true

    Int max_no_pieces_to_scatter_an_interval_file = 6
    Array[File]? other_bams = []
    Array[File]? other_bais = []

    ## Bam filtering options (within fq-bwa-align module)
    Boolean add_bam_filter = true
    ## Which filter should be applied?:
    ## 1 - removing reads with high fraction of mismatches
    ## 2 - removing reads which are both very short and with soft-clipped at both ends
    ## 3 - combining both filter types (1+2)
    String filter_choice = "3"
    ## Set mismatch fraction that is not accepted (for filters 1 and 3)
    Float mismatch_threshold = 0.1

    # sv parameters
    Boolean run_sv_calling = false
    Boolean add_snp_data_to_sv = true
    Float del_cov_max = 0.75
    Float dup_cov_min = 1.3
    Boolean exclude_lcr = true
    Boolean exclude_chroms = true
    Float panel_threshold1 = 30.0
    Float panel_threshold2 = 45.0
    String include_ci = "yes"   ## other option "no"
    Boolean apply_rank_filter = true
    String rank_threshold = "likely_pathogenic" ## other options: "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"

    # AD filtering
    Float ad_binom_threshold = 0.01
    Boolean apply_ad_filter = false

    # mt filtering
    Int max_alt_allele_count = 4
    Float vaf_filter_threshold = 0.05
    Float f_score_beta = 1.0
    Int min_alt_reads = 2

    # run modules/tasks
    Boolean run_resources_kit = true
    Boolean run_fq_organize = true
    Boolean run_fq_qc = true
    Boolean run_fq_bwa_align = true
    Boolean run_bam_varcalling = true
    Boolean run_vcf_var_filter = true
    Boolean run_vcf_anno = true
    Boolean run_vcf_acmg_report = true
    Boolean run_detection_chance = true
    Boolean run_sex_check = true
    Boolean run_bam_qc = true
    Boolean run_bco = true
    Boolean run_mt_varcall = true

    # Carrier screening
    Boolean carrier_screening = false

    # Java options for bam_metrics and bam_gatk_hc
    String java_mem_options =  "-Xms4g -Xmx63g" # use "-Xms4g -Xmx8g" on anakin

    # Java options fo snpeff
    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin

    # 1. Prepare gene panel or use user defined
    if(is_input_for_panel_generate_defined) {
        call panel_generate_task.panel_generate {
            input:
                sample_id = sample_id,
                hpo_terms = hpo_terms,
                genes = genes,
                diseases = diseases,
                phenotypes_description = phenotypes_description,
                panel_names = panel_names
        }
    }

    if (genome_or_exome != "target" && run_resources_kit) {
        # 2a. Prepare interval_list for WES/WGS analysis
        call resources_kit_task.resources_kit {
            input:
                kit = kit_choice,
                reference_genome = reference_genome
        }

        if(kit_choice != "genome") {
            File? bait_file = resources_kit.bait[0]
            File? target_file = resources_kit.target[0]

        }
        File wes_wgs_interval_list = resources_kit.interval_list[0]

    }

    # 2b. Prepare interval list for targeted analysis
    if (genome_or_exome == "target") {
        call bed_to_interval_list_task.bed_to_interval_list {
            input:
                bed = target_bed,
                gene_list = target_genes,
                reference_genome = reference_genome,
                padding = padding,
                sample_id = sample_id
        }
        File target_interval_list = bed_to_interval_list.nuclear_interval_file
        File target_mt_interval_list = bed_to_interval_list.mt_interval_file
        File genes_bed_target_json = bed_to_interval_list.genes_bed_target_json
        Boolean mt_list_not_empty = bed_to_interval_list.non_empty_mt_intervals
    }

    File interval_file = select_first([target_interval_list, wes_wgs_interval_list])

    # Start analysis form fastq file
    if(is_fastqs_defined || is_fastqs_left_defined) {
        if(is_fastqs_defined && run_fq_organize) {
            # 3. Organise fastq files
            call fq_organize_task.fq_organize {
                input:
                    fastqs = fastqs,
                    paired = true,
                    possible_left_suffixes = ['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz', '1.clean.fq.gz'],
                    possible_right_suffixes = ['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz', '2.clean.fq.gz']
            }
        }

        Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
        Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])

        # 4. Check quality of fastq files
        if(run_fq_qc) {
            call fq_qc_module.fq_qc {
                input:
                    sample_id = sample_id,
                    sample_info_json = sample_info_json,
                    error_warning_json_qc = error_warning_json_qc,
                    fastqs = fastqs
            }
        }

        # 5. Align reads
        if(run_fq_bwa_align) {
            call fq_bwa_align_module.fq_bwa_align {
                input:
                    fastqs_left = fastqs_1,
                    fastqs_right = fastqs_2,
                    sample_id = sample_id,
                    reference_genome = reference_genome,
                    add_bam_filter = add_bam_filter,
                    filter_choice = filter_choice,
                    mismatch_threshold = mismatch_threshold

            }
        }
    }

    # Start analysis from variant calling
    File bam_to_var_calling = select_first([fq_bwa_align.recalibrated_markdup_bam, bam])
    File bai_to_var_calling = select_first([fq_bwa_align.recalibrated_markdup_bai, bam_bai])

    # 6. Call variants
    if(run_bam_varcalling) {
        call bam_varcalling_module.bam_varcalling {
            input:
                input_bam = bam_to_var_calling,
                input_bai = bai_to_var_calling,
                sample_id = sample_id,
                interval_list = interval_file,
                max_no_pieces_to_scatter_an_interval_file = max_no_pieces_to_scatter_an_interval_file,
                haplotype_caller_java_mem = java_mem_options
        }
    }

    # 7. Filter variants
    if(run_vcf_var_filter) {
        call vcf_var_filter_module.vcf_var_filter {
            input:
                vcf_gz = bam_varcalling.vcf_gz,
                vcf_gz_tbi = bam_varcalling.vcf_gz_tbi,
                sample_id = sample_id,
                interval_list = interval_file,
                apply_ad_filter = apply_ad_filter,
                ad_binom_threshold = ad_binom_threshold,
                analysis_type = "exome"  ## this should turn off the VQSR filtering (and run hard filters)
        }
    }


    # 8. Call mitochondrial variants

    if ((run_mt_varcall && genome_or_exome != "target") || (run_mt_varcall && genome_or_exome == "target" && mt_list_not_empty)) {
        call mt_varcall_module.mt_varcall {
            input:
                bam = bam_to_var_calling,
                bai = bai_to_var_calling,
                mt_intervals = target_mt_interval_list,
                genome_or_exome = genome_or_exome,
                sample_id = sample_id,
                max_alt_allele_count = max_alt_allele_count,
                vaf_filter_threshold = vaf_filter_threshold,
                f_score_beta = f_score_beta,
                min_alt_reads = min_alt_reads,
                hc_vcf_gz = vcf_var_filter.filtered_vcf_gz,
                hc_bamout = bam_varcalling.haplotype_caller_bam
        }
    }

    # 9. Annotate and filter variants
    if(run_vcf_anno) {
        call vcf_anno_module.vcf_anno {
            input:
                vcf_gz = select_first([mt_varcall.allcontigs_vcf_gz, vcf_var_filter.filtered_vcf_gz]),
                vcf_gz_tbi = select_first([mt_varcall.allcontigs_vcf_gz_tbi, vcf_var_filter.filtered_vcf_gz_tbi]),
                vcf_anno_freq_genome_or_exome = freq_and_coverage_source,
                gnomad_coverage_genome_or_exome = freq_and_coverage_source,
                vcf_basename = sample_id,
                panel_sequencing = panel_sequencing,
                carrier_screening = carrier_screening,
                snpeff_java_mem = snpeff_java_mem
        }
    }

    File caller_bamout = select_first([mt_varcall.allcontigs_bamout, bam_varcalling.haplotype_caller_bam])
    File caller_bamout_bai = select_first([mt_varcall.allcontigs_bamout_bai, bam_varcalling.haplotype_caller_bai])

    # 10. Annotate variants according to ACMG recommendation
    if(run_vcf_acmg_report) {
        call vcf_acmg_report_module.vcf_acmg_report {
            input:
                panel_json = panel_generate.panel,
                panel_inputs_json = panel_generate.inputs,
                genes_bed_target_json = genes_bed_target_json,
                sample_info_json = sample_info_json,
                timezoneDifference = timezoneDifference,
                analysisSpecifier = analysisSpecifier,
                vcf_gz = vcf_anno.annotated_and_filtered_vcf,
                vcf_gz_tbi = vcf_anno.annotated_and_filtered_vcf_tbi,
                bams = [bam_to_var_calling, caller_bamout],
                bams_bais = [bai_to_var_calling, caller_bamout_bai],
                sample_id = sample_id,
                genome_or_exome = genome_or_exome,
                analysis_start = analysis_start,
                analysis_group = analysis_group,
                apply_panel_filter = apply_panel_filter
        }
    }

    # 11. Call structural variants
    if (add_snp_data_to_sv) {
        File? vcf_to_sv = vcf_var_filter.filtered_vcf_gz
        File? vcf_tbi_to_sv = vcf_var_filter.filtered_vcf_gz_tbi
    }

    if(run_sv_calling) {
        call sv_calling_module.sv_calling {
            input:
                del_cov_max = del_cov_max,
                dup_cov_min = dup_cov_min,
                create_pictures = true,
                exclude_lcr = exclude_lcr,
                exclude_chroms = exclude_chroms,
                bam = bam_to_var_calling,
                bai = bai_to_var_calling,
                vcf_gz = vcf_to_sv,
                vcf_gz_tbi = vcf_tbi_to_sv,
                gene_panel = panel_generate.panel,
                all_gene_panel = panel_generate.all_panels,
                panel_threshold1 = panel_threshold1,
                panel_threshold2 = panel_threshold2,
                apply_rank_filter =apply_rank_filter,
                rank_threshold =rank_threshold,
                include_ci =include_ci,
                sample_id = sample_id,
                reference_genome = reference_genome
        }
    }

    # 12. Estimate detection chance
    if (run_detection_chance) {
        call detection_chance_module.detection_chance {
            input:
                sample_id = sample_id,
                sample_gvcf_gz = bam_varcalling.gvcf_gz,
                sample_gvcf_gz_tbi = bam_varcalling.gvcf_gz_tbi,
                panel_json = panel_generate.panel,
                bam = bam_to_var_calling,
                bai = bai_to_var_calling
        }
    }

    # 13. Check quality of bam files
    if (run_bam_qc) {
        call bam_qc_module.bam_qc {
            input:
                sample_id = sample_id,
                bam = fq_bwa_align.recalibrated_markdup_bam,
                bai = fq_bwa_align.recalibrated_markdup_bai,
                interval_list = interval_file,
                target = target_file,
                bait = bait_file,
                kit = kit_choice,
                reference_genome = reference_genome,
                bam_metrics_java_options = java_mem_options
        }
    }

    # 14. Genetic sex verification
    if (run_sex_check) {
        call sex_check_module.sex_check {
            input:
                bam = bam_to_var_calling,
                bai = bai_to_var_calling,
                vcf_gz = bam_varcalling.vcf_gz,
                vcf_gz_tbi = bam_varcalling.vcf_gz_tbi,
                sample_id = sample_id
        }
    }

    # 15. Merge BCO and prepare report pdf
    if (run_bco) {
        Array[File] bcos_module = select_all([panel_generate.bco, resources_kit.bco, bed_to_interval_list.bco, fq_organize.bco, fq_qc.bco, fq_bwa_align.bco,
                                             bam_varcalling.bco, vcf_var_filter.bco, vcf_anno.bco, vcf_acmg_report.bco, sv_calling.bco,
                                             sex_check.bco, detection_chance.bco, bam_qc.bco])
        Array[File] stdout_module = select_all([panel_generate.stdout_log,  resources_kit.stdout_log, bed_to_interval_list.stdout_log,  fq_organize.stdout_log,
                                               fq_qc.stdout_log, fq_bwa_align.stdout_log, bam_varcalling.stdout_log, vcf_var_filter.stdout_log,
                                               vcf_anno.stdout_log, vcf_acmg_report.stdout_log, sv_calling.stdout_log, sex_check.stdout_log, detection_chance.stdout_log, bam_qc.stdout_log ])
        Array[File] stderr_module = select_all([panel_generate.stderr_log,  resources_kit.stderr_log, bed_to_interval_list.stderr_log, fq_organize.stderr_log,
                                               fq_qc.stderr_log, fq_bwa_align.stderr_log,  bam_varcalling.stderr_log, vcf_var_filter.stderr_log,
                                               vcf_anno.stderr_log, vcf_acmg_report.stderr_log, sv_calling.stderr_log, sex_check.stderr_log, detection_chance.stderr_log, bam_qc.stderr_log])

        call bco_module.bco {
            input:
                bco_array = bcos_module,
                stdout_array = stdout_module,
                stderr_array = stderr_module,
                pipeline_name = pipeline_name,
                pipeline_version = pipeline_version,
                sample_id = sample_id
        }
    }

    output {


        ## FINAL REPORTS
        File? ang_pdf_report = vcf_acmg_report.ang_pdf_report
        File? ang_docx_report = vcf_acmg_report.ang_docx_report


        ## QUALITY CHECK REPORTS
        # fastq
        Array[File]? fastqc_zips = fq_qc.fastqc_zip
        File? quality_check_report_ang_pdf = fq_qc.quality_check_report_ang_pdf
        File? quality_check_report_pl_pdf = fq_qc.quality_check_report_pl_pdf

        # bam
        File? coverage_report_pdf = bam_qc.coverage_report_pdf
        File? coverage_report_docx = bam_qc.coverage_report_docx

        # detection chance
        File? detection_chance_report_pdf = detection_chance.detection_chance_report_pdf
        File? detection_chance_report_docx = detection_chance.detection_chance_report_docx

        # genetic sex
        File? sex_check_report_docx = sex_check.report_docx
        File? sex_check_report_pdf = sex_check.report_pdf


        ## ALIGNMENT RESULTS
        File? final_bam = bam_to_var_calling
        File? final_bai = bai_to_var_calling


        ## SNPs AND INDELs VARIANT CALLING FILES
        # gvcf
        File? gvcf_gz = bam_varcalling.gvcf_gz
        File? gvcf_gz_tbi = bam_varcalling.gvcf_gz_tbi

        # genotyped and filtered vcf
        File? vcf_gz = select_first([mt_varcall.allcontigs_vcf_gz, vcf_var_filter.filtered_vcf_gz])
        File? vcf_gz_tbi = select_first([mt_varcall.allcontigs_vcf_gz_tbi, vcf_var_filter.filtered_vcf_gz_tbi])

        # caller bamouts
        File? final_realigned_bam = caller_bamout
        File? final_realigned_bai = caller_bamout_bai


        ## REJECTED VARIANTS (SNPs and INDELs)
        # autosomal
        Array[File]?  rejected_variants_vcfs = vcf_var_filter.rejected_variants_vcfs
        Array[File]?  rejected_variants_tbis = vcf_var_filter.rejected_variants_tbis
        File? detail_metrics_file = vcf_var_filter.detail_metrics_file
        File? summary_metrics_file = vcf_var_filter.summary_metrics_file

        # mitochondrial
        File? mt_rejected_vcf_gz = mt_varcall.mt_rejected_vcf_gz
        File? mt_rejected_vcf_gz_tbi = mt_varcall.mt_rejected_vcf_gz_tbi
        File? mt_contamination_report = mt_varcall.contamination_report


        ## ANNOTATED VCFs and TSV
        # annotation module
        File? annotated_vcf = vcf_anno.annotated_and_filtered_vcf
        File? annotated_vcf_tbi = vcf_anno.annotated_and_filtered_vcf_tbi
        File? tsv_from_vcf = vcf_anno.tsv_from_vcf

        # acmg
        File? annotated_acmg_vcf_gz = vcf_acmg_report.annotated_acmg_vcf_gz
        File? annotated_acmg_vcf_gz_tbi = vcf_acmg_report.annotated_acmg_vcf_gz_tbi
        File? igv_screenshots_tar_gz = vcf_acmg_report.igv_screenshots_tar_gz
        File? tsv_report = vcf_acmg_report.tsv_report


        ## SV CALLING
        # vcf
        File? annotated_sv_vcf_gz = sv_calling.annotated_sv_vcf_gz
        File? annotated_sv_vcf_gz_tbi = sv_calling.annotated_sv_vcf_gz_tbi

        # reports
        File? sv_tsv = sv_calling.tsv_full_report
        File? sv_igv_pngs = sv_calling.igv_pngs
        File? sv_report_pdf = sv_calling.sv_report_pdf
        File? sv_report_html = sv_calling.sv_report_html


        ## BCO
        #bco, stdout, stderr
        File? bco = bco.bco_merged
        File? stdout_log = bco.stdout_log
        File? stderr_log = bco.stderr_log

        #bco report (pdf, docx, html)
        File? bco_report_pdf = bco.bco_report_pdf
        File? bco_report_html = bco.bco_report_html

        #bco table (csv)
        File? bco_table_csv = bco.bco_table_csv



    }
}

import "https://gitlab.com/intelliseq/workflows/raw/panel-generate@1.7.0/src/main/wdl/tasks/panel-generate/panel-generate.wdl" as panel_generate_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-prepare@1.0.2/src/main/wdl/tasks/vcf-prepare/vcf-prepare.wdl" as vcf_prepare_task
import "https://gitlab.com/intelliseq/workflows/raw/vcf-anno@1.12.0/src/main/wdl/modules/vcf-anno/vcf-anno.wdl" as vcf_anno_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-acmg-report@2.1.5/src/main/wdl/modules/vcf-acmg-report/vcf-acmg-report.wdl" as vcf_acmg_report_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.0.1/src/main/wdl/modules/bco/bco.wdl" as bco_module
import "https://gitlab.com/intelliseq/workflo
workflow vcf_to_acmg_report {

    String sample_id = "no_id_provided"
    File vcf
    File? tbi
    Array[File]? bams
    Array[File]? bams_bais

    # Gene panel
    String? hpo_terms
    String? genes
    String? diseases
    String? phenotypes_description
    Array[String]? panel_names
    Boolean is_input_for_panel_generate_defined = (defined(hpo_terms) || defined(genes) || defined(diseases) || defined(phenotypes_description) || defined(panel_names))

    # Patient information
    File? sample_info_json

    # Report information (time zone difference and ID)
    Int timezoneDifference = 0
    String? analysisSpecifier

    String genome_or_exome = "exome" # Possible values "exome", "genome", "target"
    String analysis_start = "vcf"

    Boolean apply_panel_filter = if (genome_or_exome == "target") then false else true
    Boolean panel_sequencing = if (genome_or_exome == "target") then true else false
    String freq_and_coverage_source = if (genome_or_exome == "exome") then "exome" else "genome"

    # Java options fo snpeff
    String snpeff_java_mem = "-Xmx31g" # give "-Xmx8g" on anakin

    String pipeline_name = "vcf_to_acmg_report"
    String pipeline_version = "1.1.8"


    # 1. Prepare the given vcf to run vcf-anno
    call vcf_prepare_task.vcf_prepare {
        input:
            vcf = vcf,
            tbi = tbi,
            vcf_basename = sample_id
    }

    # 2. Prepare gene panel
    if(is_input_for_panel_generate_defined) {
        call panel_generate_task.panel_generate {
            input:
                sample_id = sample_id,
                hpo_terms = hpo_terms,
                genes = genes,
                diseases = diseases,
                phenotypes_description = phenotypes_description,
                panel_names = panel_names
        }
    }

    # 3. Annotate and filter variants
    call vcf_anno_module.vcf_anno {
        input:
            vcf_gz = vcf_prepare.prepared_vcf_gz,
            vcf_gz_tbi = vcf_prepare.prepared_vcf_gz_tbi,
            vcf_anno_freq_genome_or_exome = freq_and_coverage_source,
            gnomad_coverage_genome_or_exome = freq_and_coverage_source,
            vcf_basename = sample_id,
            panel_sequencing = panel_sequencing,
            snpeff_java_mem = snpeff_java_mem
    }

    # 4. Annotate variants according to ACMG recommendation
    call vcf_acmg_report_module.vcf_acmg_report {
        input:
            panel_json = panel_generate.panel,
            panel_inputs_json = panel_generate.inputs,
            sample_info_json = sample_info_json,
            timezoneDifference = timezoneDifference,
            analysisSpecifier = analysisSpecifier,
            vcf_gz = vcf_anno.annotated_and_filtered_vcf,
            vcf_gz_tbi = vcf_anno.annotated_and_filtered_vcf_tbi,
            bams = bams,
            bams_bais = bams_bais,
            genome_or_exome = genome_or_exome,
            analysis_start = analysis_start,
            apply_panel_filter = apply_panel_filter,
            sample_id = sample_id,
    }

    # 5. Merge BCO and prepare report pdf
    Array[File] bcos_module = select_all([panel_generate.bco, vcf_prepare.bco, vcf_anno.bco, vcf_acmg_report.bco])
    Array[File] stdout_module = select_all([panel_generate.stdout_log, vcf_prepare.stdout_log, vcf_anno.stdout_log, vcf_acmg_report.stdout_log])
    Array[File] stderr_module = select_all([panel_generate.stderr_log, vcf_prepare.stderr_log, vcf_anno.stderr_log, vcf_acmg_report.stderr_log])

    call bco_module.bco as merge_bcos {
        input:
            bco_array = bcos_module,
            stdout_array = stdout_module,
            stderr_array = stderr_module,
            pipeline_name = pipeline_name,
            pipeline_version = pipeline_version,
            sample_id = sample_id
    }

    output {
        # 1. Annotate and filter variants
        File? annotated_vcf = vcf_anno.annotated_and_filtered_vcf
        File? annotated_vcf_tbi = vcf_anno.annotated_and_filtered_vcf_tbi
        File? tsv_from_vcf = vcf_anno.tsv_from_vcf

        # 2. Annotate variants according to ACMG recommendation
        File? annotated_acmg_vcf_gz = vcf_acmg_report.annotated_acmg_vcf_gz
        File? annotated_acmg_vcf_gz_tbi = vcf_acmg_report.annotated_acmg_vcf_gz_tbi
        File? igv_screenshots_tar_gz = vcf_acmg_report.igv_screenshots_tar_gz
        File? tsv_report = vcf_acmg_report.tsv_report

        # 3. Report files
        File? ang_pdf_report = vcf_acmg_report.ang_pdf_report
        File? ang_docx_report = vcf_acmg_report.ang_docx_report

        # 4. Merge BCO and prepare report pdf
        #bco, stdout, stderr
        File? bco = merge_bcos.bco_merged
        File? stdout_log = merge_bcos.stdout_log
        File? stderr_log = merge_bcos.stderr_log

        #bco report (pdf, html)
        File? bco_report_pdf = merge_bcos.bco_report_pdf
        File? bco_report_html = merge_bcos.bco_report_html

        #bco table (csv)
        File? bco_table_csv = merge_bcos.bco_table_csv
    }
}
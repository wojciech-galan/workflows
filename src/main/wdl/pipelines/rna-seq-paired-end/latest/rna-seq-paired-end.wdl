import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.0.1/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-custom-hisat@1.0.1/src/main/wdl/tasks/rna-seq-custom-hisat/rna-seq-custom-hisat.wdl" as rna_seq_custom_hisat_task
import "https://gitlab.com/intelliseq/workflows/-/raw/rna-seq-ensembl-hisat@1.0.1/src/main/wdl/tasks/rna-seq-ensembl-hisat/rna-seq-ensembl-hisat.wdl" as rna_seq_ensembl_hisat_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-hisat@1.0.6/src/main/wdl/tasks/rna-seq-hisat/rna-seq-hisat.wdl" as rna_seq_hisat_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-cuffquant@1.0.5/src/main/wdl/tasks/rna-seq-cuffquant/rna-seq-cuffquant.wdl" as rna_seq_cuffquant_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-cuffnorm@1.0.6/src/main/wdl/tasks/rna-seq-cuffnorm/rna-seq-cuffnorm.wdl" as rna_seq_cuffnorm_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-concat-summary@1.0.3/src/main/wdl/tasks/rna-seq-concat-summary/rna-seq-concat-summary.wdl" as rna_seq_concat_summary_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-fastqc@1.0.9/src/main/wdl/tasks/rna-seq-fastqc/rna-seq-fastqc.wdl" as rna_seq_fastqc_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-qc-stats@1.0.3/src/main/wdl/tasks/rna-seq-qc-stats/rna-seq-qc-stats.wdl" as rna_seq_qc_stats_task
import "https://gitlab.com/intelliseq/workflows/raw/rna-seq-fastqc-overrep@1.0.3/src/main/wdl/tasks/rna-seq-fastqc-overrep/rna-seq-fastqc-overrep.wdl" as rna_seq_fastqc_overrep_task
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.0.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow rna_seq_paired_end {

  meta {
    name: 'RNA-seq paired-end'
    price: '80'
    author: 'https://gitlab.com/MateuszMarynowski'
    copyright: 'Copyright 2019 Intelliseq'
    description: 'Data analysis workflow for RNA-seq (paired-end) research.'
    changes: '{"1.12.8": "memory now depends on the number of files in rna-seq-cuffnorm", "1.12.7": "possible suffixes in fq-organize run", "1.12.6": "new version of fq-organize task", "1.12.4": "add price to meta", "1.12.1": "deleted default id in meta"}'
    tag: 'Research'

    input_analysis_id: '{"index": 1, "name": "Analysis id", "type": "String", "description": "Enter a analysis name (or identifier)"}'
    input_fastqs: '{"index": 2, "name": "Fastq files", "paired":"true", "type": "Array[File]", "extension": [".fq.gz",".fastq.gz"], "description": "Select two paired gzipped fastq files, both left and right [.fq.gz or .fastq.gz] for each sample. You need to provide at least 2 samples"}'
    input_fastqs_left: '{"hidden":"true", "name": "Left fastq files", "type": "Array[File]", "extension": [".fq.gz"], "description": "Select first (left) fastq files"}'
    input_fastqs_right: '{"hidden":"true", "name": "Right fastq files ", "type": "Array[File]", "extension": [".fq.gz"], "description": "Select second (right) fastq files"}'
    input_samples_names: '{"hidden":"true", "name": "Samples names", "type": "Array[String]", "description": "Enter samples names (or identifiers)"}'
    input_organism_name: '{"index": 3, "name": "Organism name",  "type": "String", "default": "Homo sapiens", "description": "Enter name of the organism in Latin, for example Homo sapiens, Mus musculus, Rattus norvegicus. List of available organism you can find on www.ensembl.org"}'
    input_release_version: '{"index": 4, "name": "Ensembl version",  "type": "String", "default": "100", "description": "Enter Ensembl database release version"}'

    output_fastqc_zip_files: '{"name": "Quality check fastQC files", "type": "Array[Array[File]]", "copy": "True", "description": "Zip files from FastQC"}'
    output_basic_statistics_excel: '{"name": "Quality check basic statistics xlsx", "type": "File", "copy": "true", "description": "Basic statistics in Excel"}'
    output_basic_statistics_csv: '{"name": "Quality check basic statistics csv", "type": "File", "copy": "true", "description": "Basic statistics in CSV"}'
    output_overrepresented: '{"name": "Overrepresented sequences xlsx", "type": "File", "copy": "true", "description": "Overrepresented in Excel"}'
    output_overrepresented_csv: '{"name": "Overrepresented sequences csv", "type": "File", "copy": "true", "description": "Overrepresented in CSV"}'
    output_bam_file: '{"name": "Bam", "type": "Array[File]", "copy": "True", "description": "Alignment results"}'
    output_bam_bai_file: '{"name": "Bai", "type": "Array[File]", "copy": "True", "description": "Alignment results index"}'
    output_summary_excel_file: '{"name": "Aligmnet statistics xlsx", "type": "File", "copy": "true", "description": "Statistics from Hisat2 in xlsx format"}'
    output_summary_csv_file: '{"name": "Aligmnet statistics csv", "type": "File", "copy": "true", "description": "Statistics from Hisat2 in csv format"}'
    output_abundances_file: '{"name": "Abundances files", "type": "Array[File]", "copy": "True", "description": "List of files prodused by Cuffquant in xcb format"}'
    output_cuffnorm_output: '{"name": "Abundances results", "type": "Array[File]", "copy": "True", "description": "FPKM and counts tables procuded by Cuffnorm"}'
  }
  Array[File]? fastqs
  Boolean is_fastqs_defined = defined(fastqs)
  Array[File]? fastqs_left
  Array[File]? fastqs_right
  Array[String]? samples_names
  File? ref_genome
  File? gtf
  Boolean is_ref_genome_defined = defined(ref_genome)
  String organism_name = "Homo sapiens"
  String release_version = "100"
  String analysis_id = "no_id_provided"
  String genome_basename = sub(organism_name, " ", "_") + "_genome"
  String gtf_basename = sub(organism_name, " ", "_") + "_gtf"
  String chromosome_name = "primary_assembly"
  String summary_file_name = "summary"
  String pipeline_name = "rna_seq_paired_end"
  String pipeline_version = "1.12.8"

  if(is_fastqs_defined) {
    call fq_organize_task.fq_organize {
      input:
        fastqs = fastqs,
        paired = true,
        split_files = false,
        possible_left_suffixes = ['1.fq.gz', '1.fastq.gz', 'R1_001.fastq.gz', 'R1_001.fq.gz', '1.clean.fq.gz'],
        possible_right_suffixes = ['2.fq.gz', '2.fastq.gz', 'R2_001.fastq.gz', 'R2_001.fq.gz', '2.clean.fq.gz']
    }
  }
  Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
  Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])
  Array[String] samples_ids = select_first([fq_organize.samples_ids, samples_names])

  scatter (index in range(length(fastqs_1))) {
    call rna_seq_fastqc_task.rna_seq_fastqc {
        input:
            fastq_1 = fastqs_1[index],
            fastq_2 = fastqs_2[index],
            sample_id = samples_ids[index],
            index = index
    }
  }

  call rna_seq_qc_stats_task.rna_seq_qc_stats {
    input:
        zip_files = rna_seq_fastqc.zip_files
  }

  call rna_seq_fastqc_overrep_task.rna_seq_fastqc_overrep {
    input:
        zip_files = rna_seq_fastqc.zip_files
  }

  if(is_ref_genome_defined) {
    call rna_seq_custom_hisat_task.rna_seq_custom_hisat {
      input:
        ref_genome = ref_genome,
        gtf = gtf,
        genome_basename = genome_basename,
        gtf_basename = gtf_basename
    }
  }

  if(!is_ref_genome_defined) {
    call rna_seq_ensembl_hisat_task.rna_seq_ensembl_hisat {
      input:
        release_version = release_version,
        chromosome_name = chromosome_name,
        organism_name = organism_name,
        genome_basename = genome_basename
    }
  }

  Array[File] ref_genome_index = select_first([rna_seq_custom_hisat.ref_genome_index, rna_seq_ensembl_hisat.ref_genome_index])
  File splicesites_file = select_first([rna_seq_custom_hisat.splicesites_file, rna_seq_ensembl_hisat.splicesites_file])
  File gtf_file = select_first([gtf, rna_seq_ensembl_hisat.gtf_file])

  scatter (index in range(length(fastqs_1))) {
    call rna_seq_hisat_task.rna_seq_hisat {
        input:
            fastq_1 = fastqs_1[index],
            fastq_2 = fastqs_2[index],
            sample_id = samples_ids[index],
            splicesites_file = splicesites_file,
            ref_genome_index = ref_genome_index,
            genome_basename = genome_basename,
            index = index
    }
  }

  call rna_seq_concat_summary_task.rna_seq_concat_summary {
    input:
        summary_files = rna_seq_hisat.summary,
        summary_file_name = summary_file_name
  }

  scatter (index in range(length(fastqs_1))) {
    call rna_seq_cuffquant_task.rna_seq_cuffquant {
        input:
            gtf_file = gtf_file,
            bam_file = rna_seq_hisat.bam_file[index],
            sample_id = samples_ids[index],
            index = index
    }
  }

  call rna_seq_cuffnorm_task.rna_seq_cuffnorm {
    input:
        gtf_file = gtf_file,
        abundances_files = rna_seq_cuffquant.abundances_file
  }

# Merge bco, stdout, stderr files
  File indexing_genome_bco = select_first([rna_seq_ensembl_hisat.bco, rna_seq_custom_hisat.bco])
  File indexing_genome_stdout = select_first([rna_seq_ensembl_hisat.stdout_log, rna_seq_custom_hisat.stdout_log])
  File indexing_genome_stderr = select_first([rna_seq_ensembl_hisat.stderr_log, rna_seq_custom_hisat.stderr_log])

  Array[File] bco_tasks = [rna_seq_qc_stats.bco, rna_seq_fastqc_overrep.bco, indexing_genome_bco, rna_seq_concat_summary.bco, rna_seq_cuffnorm.bco]
  Array[File] stdout_tasks = [rna_seq_qc_stats.stdout_log, rna_seq_fastqc_overrep.stdout_log, indexing_genome_stdout, rna_seq_concat_summary.stdout_log, rna_seq_cuffnorm.stdout_log]
  Array[File] stderr_tasks = [rna_seq_qc_stats.stderr_log, rna_seq_fastqc_overrep.stderr_log, indexing_genome_stderr, rna_seq_concat_summary.stderr_log, rna_seq_cuffnorm.stderr_log]

  Array[Array[File]] bco_scatters = [bco_tasks, rna_seq_fastqc.bco, rna_seq_hisat.bco, rna_seq_cuffquant.bco]
  Array[Array[File]] stdout_scatters = [stdout_tasks, rna_seq_fastqc.stdout_log, rna_seq_hisat.stdout_log, rna_seq_cuffquant.stdout_log]
  Array[Array[File]] stderr_scatters = [stderr_tasks, rna_seq_fastqc.stderr_log, rna_seq_hisat.stderr_log, rna_seq_cuffquant.stderr_log]

  Array[File] bco_array = flatten(bco_scatters)
  Array[File] stdout_array = flatten(stdout_scatters)
  Array[File] stderr_array = flatten(stderr_scatters)

  call bco_module.bco {
    input:
      bco_array = bco_array,
      stdout_array = stdout_array,
      stderr_array = stderr_array,
      module_name = pipeline_name,
      module_version = pipeline_version,
      sample_id = analysis_id
  }

  output {

    Array[Array[File]] fastqc_zip_files = rna_seq_fastqc.zip_files
    File basic_statistics_excel = rna_seq_qc_stats.basic_statistics_excel
    File basic_statistics_csv = rna_seq_qc_stats.basic_statistics_csv
    File overrepresetned = rna_seq_fastqc_overrep.overrepresented
    File overrepresetned_csv = rna_seq_fastqc_overrep.overrepresented_csv
    Array[File] bam_file = rna_seq_hisat.bam_file
    Array[File] bam_bai_file = rna_seq_hisat.bam_bai_file
    File summary_excel_file = rna_seq_concat_summary.summary_excel_file
    File summary_csv_file = rna_seq_concat_summary.summary_csv_file
    Array[File] abundances_file = rna_seq_cuffquant.abundances_file
    Array[File] cuffnorm_output = rna_seq_cuffnorm.cuffnorm_output

    #bco, stdout, stderr
    File bco_merged = bco.bco_merged
    File stdout_log = bco.stdout_log
    File stderr_log = bco.stderr_log

    #bco report (pdf, odt, docx, html)
    File bco_report_pdf = bco.bco_report_pdf
    File bco_report_odt = bco.bco_report_odt
    File bco_report_docx = bco.bco_report_docx
    File bco_report_html = bco.bco_report_html

    #bco table (csv)
    File bco_table_csv = bco.bco_table_csv
  }
}

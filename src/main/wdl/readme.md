WDL frequently errors solution

1. Separator in input Array[File] should be space (" ") or tab ("\t")

for example: `${sep = "," files}` causes an error

solution: `${sep=" " files}`
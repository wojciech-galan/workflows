#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)

### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $TASK_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

for vcf in "genotyped_vcf_gz" "clinvar_vcf_gz"
do 
    OUTPUT_NAME1=$vcf
    UNIT_TEST_NAME="Output files test"
    info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
    OUTPUT1=$WORKFLOW_NAME"."$OUTPUT_NAME1
    OUTPUT_FILE_PATH1=$(get_path_by_output_name $OUTPUT1)
    if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
    if vcf-validator $OUTPUT_FILE_PATH1; then ok "The result of vcf vcf-validator: "; else error; fi
    for string in "##fileformat=VCF" "#CHROM" "##contig=<ID=" "##INFO=<ID=ISEQ_CLINVAR_ALLELE_ID"
    do
        if zcat $OUTPUT_FILE_PATH1 | grep -q $string; then ok "File ${vcf} contains ${string}"; else error "File ${vcf} does not contain ${string}"; fi
    done 
done

OUTPUT_NAME1="detection_chance_report_html"
UNIT_TEST_NAME="Output files test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT1=$WORKFLOW_NAME"."$OUTPUT_NAME1
OUTPUT_FILE_PATH1=$(get_path_by_output_name $OUTPUT1)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
for string in "<b>Gene</b></p>"
do
    if cat $OUTPUT_FILE_PATH1 | grep -q $string; then ok "File ${OUTPUT_NAME1} contains ${string}"; else error "File ${OUTPUT_NAME1} does not contain ${string}"; fi
done


#!/usr/bin/env python3

import sys
import argparse
from bs4 import BeautifulSoup
from typing import TextIO

__version__ = '0.0.2'


def vitamin_d(tag:str) -> bool:
    return tag.name=='table' and tag.tbody and tag.tbody.tr and tag.tbody.tr.td.p.string and \
           'Vitamin D plays a important role in immunity' in tag.tbody.tr.td.p.string


def find_vitamin_d_text(handle:TextIO) -> str:
    html = BeautifulSoup(handle, 'lxml')
    body = html.body
    found = body.find_all(vitamin_d)
    assert len(found) == 1
    return ' '.join(found[0].get_text().strip().split())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-f', '--file', help='The file that is about to be parsed', required=True)

    arguments = parser.parse_args()

    with open(arguments.file) as f:
        print(find_vitamin_d_text(f))
    sys.stdout.flush()

#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains module name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $MODULE_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)

### test if bioobject contains module name
MODULE_NAME="vcf_var_filter"
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $MODULE_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if filtered vcf is not empty
OUTPUT_NAME="filtered_vcf_gz"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $MODULE_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
DATA_LINES=$( zcat $OUTPUT_FILE_PATH | grep -v '^#' | wc -l )
if [ $DATA_LINES -gt 10000 ]; then ok "$OUTPUT More than 10 000 data lines present"; else error "$OUTPUT output vcf has no data lines"; fi
printf "vcf-validator results (should be empty/without errors):\n"
vcf-validator $OUTPUT_FILE_PATH

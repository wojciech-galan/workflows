#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject contains task name
OUTPUT_NAME="bco" ### <- should be provided: name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
OUTPUT_FILE_CONTENT=$(cat $OUTPUT_FILE_PATH)

### test if bioobject contains task name
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -q $TASK_NAME) ### <- should be provided: test
if $TEST_RESULT; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if vcf output file exists
OUTPUT_NAME="sv_vcf" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi

### Test if vcf file has more than 10 data lines
TEST_RESULT1=$(zcat $OUTPUT_FILE_PATH | grep -v "^#" | wc -l)
info "Testing if genotyped sv vcf file has more than 10 variant lines"
if [ "$TEST_RESULT1" -gt 10 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

### Tests if vcf output file exists
OUTPUT_NAME="annotated_sv_vcf_gz" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi

### Test if vcf file has more than 10 data lines
TEST_RESULT2=$(zcat $OUTPUT_FILE_PATH | grep -v "^#" | wc -l)
info "Testing if annotated sv vcf file has more than 10 variant lines"
if [ "$TEST_RESULT2" -gt 10 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi

info "Running file truncation test..."
info "Testing if both output vcfs have equal number of data lines..."
if [ "$TEST_RESULT1" -eq "$TEST_RESULT2" ]; then ok "Test passed: all data lines present"; else error "Test failed, different number of data lines"; fi

printf "vcf-validator result ( annotated vcf checked): \n"
vcf-validator $OUTPUT_FILE_PATH

### Tests if vcf output file exists
OUTPUT_NAME="annotated_filtered_sv_vcf_gz" ### should be provided
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi

### Test if vcf file has more than 10 data lines
TEST_RESULT=$(zcat $OUTPUT_FILE_PATH | grep -v "^#" | wc -l )
info "Testing if genotyped sv vcf file has any variant lines"
if [ "$TEST_RESULT1" -gt 0 ]; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed"; fi
printf "vcf-validator result (annotated filtered vcf checked): \n"
vcf-validator $OUTPUT_FILE_PATH

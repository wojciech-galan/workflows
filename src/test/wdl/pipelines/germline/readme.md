# Walidacja oprogramowania użytkowego 

Dokumentacja opisuje metodologię walidacji automatycznej oprogramowania do wykrywania chorób rzadkich przy zastosowaniu IntelliseqFlow Germline na danych genetycznych zmodyfikowanych <em>in silico</em>  poprzez wprowadzenie określonych modyfikacji sekwencji. 

Wysymulowano <em>in silico</em> dwie próbki syntetyczne, symulujące surowe odczyty z sekwencjonowania nowej generacji w technologii Illumina, o długości 150 par zasad, zawierające kombinacje mutacji. Próbki zostały opisane poniżej: 

1. Symulacja odczytów DNA człowieka z dwoma homozygotycznymi mutacjami w genie ATAD3 (rs756429611) oraz pategoennym wariantem strukturalnym (delecja o długości 8000 par zasad). 

2. Symulacja odczytów DNA człowieka z homozygotyczną mutacją w genie CCDC134 (delecja o długości 8000 par zasad). 

### Rezultaty

IntelliseqFlow Germline ma zidentyfikować co najmniej 85% wariantów DNA, wprowadzonych do danych poprzez modyfikację <em>in silico</em>. 

W przypadku próbki 1. poprawnie zidentyfikowano zarówno wariant typu indel oraz wariant strukturalny. Zidentyfikowano 100% wariantów. 


## Table with all testing configurations in germline pipeline

| TEST_NAME | REFERENCE_GENOME | FASTQ_OR_BAM | SAMPLE | GENOME_OR_EXOME | ADDITIONAL_MODULES | SPECIFIC INPUTS | OUTPUTS_TO_CHECK |
|-|-|-|-|-|-|-|-|
| 1. FASTQ, ATAD3A child,<br>grch38-no-alt, WES (exome-v7) | grch38-no-alt | FASTQ | SIMULATED: <br>ATAD3A-TRIO child | EXOME-V7 | bam-qc, SV | GENES: <br>FBN1 | annotated_acmg_vcf_gz - EXISTS<br>annotated_acmg_vcf_gz - HEADER<br>final_bam - NOT EMPTY<br><br>bam-qc  - TURNED ON <br>sv_report_html - EXISTS <br>annotated_sv_vcf_gz - EXISTS |
| 2. FASTQ, 8 kB deletion,<br>grch38-no-alt, GENOME | grch38-no-alt | FASTQ | SIMULATED: <br>22-del-41811140-8000 Copy Number Loss<br>- 22-del-41811140-8000_1.fq.gz<br>- 22-del-41811140-8000_1.fq.gz | GENOME | SV | GENES: <br>CCDC134 | annotated_acmg_vcf_gz - EXISTS<br>annotated_acmg_vcf_gz - HEADER<br>final_bam - NOT EMPTY<br><br>sv_report_html - EXISTS |
| 3. FASTQ, FBN1,<br>hg38, TARGETED | hg38 | FASTQ | REAL: <br>FBN1 MT-CO1 MT-CO2 genes<br>- 282-chr15.47497301-49497301_1.fq.gz<br>- 282-chr15.47497301-49497301_2.fq.gz | TARGET | default (that is run): bam-qc,<br>default (that is run): detection-chance,<br>run_mt_varcall,<br>default (that is run): sex-check| HPO therms:<br>HP:0003549 | annotated_acmg_vcf_gz - EXISTS<br>annotated_acmg_vcf_gz - HEADER<br>final_bam - NOT EMPTY<br><br>annotated_acmg_vcf_gz - ALSO COMMON VARIANTS<br>annotated_acmg_vcf_gz - ALSO LINES WITHOUT PANEL MATCH<br><br>coverage_report_pdf - EXISTS,<br>detection_chance_report_html - EXISTS,<br>sex_check_report_html - EXISTS<br><br>mt_rejected_vcf_gz - EXISTS |
| 4. BAM, ZMYM2, chr13:19955406-20093845,<br>hg38, WES (exome-v6) | hg38 | BAM | REAL: <br>ZMYM2 gene<br>- S_136.ZMYM2.bam<br>- S_136.ZMYM2.bai | EXOME-V6 | carrier-screening | GENES:<br>FBN1 | annotated_acmg_vcf_gz - EXISTS<br>annotated_acmg_vcf_gz - HEADER<br>final_bam - NOT EMPTY |
| 5. BAM, MITOCHONDRIUM - small-chr1-mt.bam,<br>grch38-no-alt, GENOME | grch38-no-alt | BAM | REAL:<br>- small-chr1-mt.bam<br>- small-chr1-mt.bam.bai | GENOME | run_mt_varcall | GENES:<br>all mitochondrial<br>DISEASES:<br>(...) Mitochondrial Myopathy | annotated_acmg_vcf_gz - EXISTS<br>annotated_acmg_vcf_gz - HEADER<br>final_bam - NOT EMPTY<br><br>vcf_gz - has lines with mitochondrial variants  |

What to do next:

- which inputs include to testing that are not in table? 
- error_warning_json_coverage
- target-bed in targeted_sequencing
- what with other_bams and other_bais
- filtering vcf?



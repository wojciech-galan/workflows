import json
import argparse

__version__ = '0.0.1'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Tests json outputs and compares it to the reference')
    parser.add_argument('--reference', help='Reference output')
    parser.add_argument('--test_file', help='The tested file')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    args = parser.parse_args()

    assert(args.reference and args.test_file)

    with open(args.reference) as f:
        vit_D = json.load(f)
    with open(args.test_file) as f:
        results = json.load(f)
    assert vit_D in results
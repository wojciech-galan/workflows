#!/bin/bash

function testhelp() {
  printf "  \n"
  printf "  haplotype-caller-position task tests\n"
  printf "  ************************************\n"
  printf "  \n"
  printf "  Info:\n"
  printf "  -----\n"
  printf "    Status: Prototype\n"
  printf "    Last edited: 20-05-19 \n"
  printf "    Last edited by: Marcel Mroczek\n"
  printf "    Author(s):\n"
  printf "      + Marcin Piechota, <piechota@intelliseq.pl>, https://gitlab.com/marpiech\n"
  printf "      + Katarzyna Kolanek <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw\n"
  printf "      + Marcel Mroczek <mroczekmarcel@intelliseq.pl>, https://gitlab.com/mremre\n"
  printf "    Maintainer(s):\n"
  printf "      + Marcel Mroczek <mroczekmarcel@intelliseq.pl>, https://gitlab.com/mremre\n"
  printf "    Copyright: Copyright 2019 Intelliseq\n"
  printf "    License: All rights reserved\n"
  printf "  \n"
  printf "  Description:\n"
  printf "  ------------\n"
  printf "  \n"
  printf "  Run haplotype-caller tests.\n"
  printf "  \n"
  printf "      -h|--help                      print help\n"
  printf "      -v|--verbose                   print 'docker build' and 'docker push' logs\n"
  printf "  \n"
}

export -f testhelp

POSITIONAL=() #what does that mean
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -h|--help)
    dockerbuildhelp
    exit 0
    shift # past argument
    ;;
    -v|--verbose)
    export VERBOSE=TRUE
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done


TEST_EXECUTION_DIR="/tmp/workflows/test"
CROMWELL_JAR="/opt/tools/cromwell/cromwell.jar"
WOMTOOL_JAR="/opt/tools/womtool/womtool.jar"
INPUT_FILE="inputs-1.json"

################
# Generic test #
################

  printf "################\n"
  printf "# Generic test #\n"
  printf "################\n"

  # 1. Set paths
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  done
  TEST_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  MAIN_DIR=$(echo $TEST_DIR | sed 's/test/main/')
  TEST_RESOURCES_DIR=$(echo $TEST_DIR | sed 's/test\/wdl/test\/resources/g')

  # 2. Get WDL filename and task/module name
  WDL=$(ls -d $MAIN_DIR/*.wdl | rev | cut -d '/' -f 1 | rev)
  TASK_NAME=${WDL%.wdl}

  # 3. Set working directory, create symlinks to input/output directories
  DATE_AND_TIME=$(date +%Y-%m-%d_%H-%M)
  WORKING_DIR=$TEST_EXECUTION_DIR/$TASK_NAME/$DATE_AND_TIME
  mkdir -p $WORKING_DIR
  cd $WORKING_DIR
  ln -s $TEST_RESOURCES_DIR/inputs
  ln -s $TEST_RESOURCES_DIR/outputs
  cat $TEST_DIR/$INPUT_FILE | sed "s|\$TASK_TEST_RESOURCES|$WORKING_DIR|g" > $WORKING_DIR/$INPUT_FILE

  # 4. Validate workflow with Womtool

  printf "\nValidating workflow...\n\n"

  if [ ! "$VERBOSE" == "TRUE" ]; then
    java -jar "$WOMTOOL_JAR" validate $MAIN_DIR/$WDL &> womtool-validation.log
  else
    java -jar "$WOMTOOL_JAR" validate $MAIN_DIR/$WDL | tee womtool-validation.log
  fi

  VALIDATION_STATUS=$(cat womtool-validation.log | sed '1!d')

  printf "  ==> validate log path $WORKING_DIR/womtool-validation.log\n"
  printf "  ==> validation status: $VALIDATION_STATUS\n\n"

  # 5. Check validation status

  if [ ! "$VALIDATION_STATUS" == "Success!" ]; then
    printf "  Validation status: FAILED\n"; exit 1;
  else
    printf "  Validation status: PASS\n\n";
  fi


  # 6. Run Cromwell
  printf "\nRunning generic test...\n\n"

  if [ ! "$VERBOSE" == "TRUE" ]; then
    java -jar "$CROMWELL_JAR" run -i $WORKING_DIR/$INPUT_FILE $MAIN_DIR/$WDL &> cromwell-execution.log
  else
    java -jar "$CROMWELL_JAR" run -i $WORKING_DIR/$INPUT_FILE $MAIN_DIR/$WDL | tee cromwell-execution.log
  fi

  printf "  ==> log path: $WORKING_DIR/cromwell-execution.log\n"

  # 7. Get task ID and task name
  TASK_NAME_LOG=$(cat cromwell-execution.log | grep -Eo "Starting.*\..*" | cut -d " " -f 2)
  printf "  ==> task name: $TASK_NAME_LOG\n"
  TASK_ID=$(cat cromwell-execution.log | grep "\":" | sed 's/"//g' | sed 's/ //g' | grep "id:" | cut -d ':' -f 2)
  printf "  ==> task ID: $TASK_ID\n"
  TASK_STATUS=$(cat cromwell-execution.log | grep -Eo "finished with status .*'" | cut -d ' ' -f 4 | sed "s/'//g")
  printf "  ==> workflow status: $TASK_STATUS\n\n"


  # 8. Check task status
  if [ ! $TASK_STATUS == "Succeeded" ]; then
    printf "  Generic test status: FAILED\n"; exit 1;
  else
    printf "  Generic test status: PASS\n\n";
  fi

##############
# Task tests #
##############

printf "##############\n"
printf "# Task tests #\n"
printf "##############\n"


# 1. Test if the outputs are identical to test outputs using "diff" command
printf "\nRunning task-test-1...\n\n"

   # Get cromwell-executions subdirectory ID

  EXECUTION_ID=$(ls $WORKING_DIR/cromwell-executions/*_workflow)
  TASK_NAME_HARD_SPACES=$(echo $TASK_NAME | sed "s/-/_/g" )
  WORKFLOW_DIR=$TASK_NAME_HARD_SPACES"_workflow"
  CALL_DIR="call-"$TASK_NAME_HARD_SPACES

  if [ ! "$VERBOSE" == "TRUE" ]; then

      OUTPUT_FILE=test_sample.g.vcf.gz
      zcat $WORKING_DIR/cromwell-executions/$WORKFLOW_DIR/$EXECUTION_ID/$CALL_DIR/execution/$OUTPUT_FILE | sed '13d' > TESTED_OUTPUT_FILE
      zcat $TEST_RESOURCES_DIR/outputs/$OUTPUT_FILE | sed '13d' > ORIGINAL_OUTPUT_FILE
      FILE_STATUS=$(diff -s TESTED_OUTPUT_FILE ORIGINAL_OUTPUT_FILE)
      echo $FILE_STATUS >> $WORKING_DIR/task-test-1.log
  else


      OUTPUT_FILE=test_sample.g.vcf.gz
      zcat $WORKING_DIR/cromwell-executions/$WORKFLOW_DIR/$EXECUTION_ID/$CALL_DIR/execution/$OUTPUT_FILE > TESTED_OUTPUT_FILE
      zcat $TEST_RESOURCES_DIR/outputs/$OUTPUT_FILE > ORIGINAL_OUTPUT_FILE
      FILE_STATUS=$(diff -s TESTED_OUTPUT_FILE ORIGINAL_OUTPUT_FILE)
      echo $FILE_STATUS
    printf "\n"

  fi

  printf "  ==> Description: Test if the outputs are identical to test outputs using \"diff\" command\n"
  printf "  ==> log path: $WORKING_DIR/task-test-1.log\n\n"
  NO_IDENTICAL_OUTPUT=$(echo $FILE_STATUS)
  if [ ! "$NO_IDENTICAL_OUTPUT" == "Files TESTED_OUTPUT_FILE and ORIGINAL_OUTPUT_FILE are identical" ]; then
    printf "  task-test-1 status: FAILED\n"; exit 1;
  else
    printf "  task-test-1 status: PASS\n\n";
  fi

#!/usr/bin/env bash

res=`jq --argfile a $1 --argfile b $2 -n '($a | (.. | arrays) |= sort) as $a | ($b | (.. | arrays) |= sort) as $b | $a == $b'`

if [ $res == 'true' ]
then
  exit 0
else exit 1
fi
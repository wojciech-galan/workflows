#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"
source $PROJECT_DIR/src/test/scripts/test.sh

### TESTS ###

TEST_NUMBER=1

### Tests if bioobject exists and contains task name
OUTPUT_NAME="bco" ### name of the output in wdl
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$WORKFLOW_NAME"."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if bioobject exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi

### tests if bioobject contains task name
if grep -q $UNDERSCORED_TASK_NAME $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: can't find task name in bco"; fi

### tests if bioobject contains all domains: provenance_domain execution_domain parametric_domain description_domain
UNIT_TEST_NAME="bco domains test"
if grep -q provenance_domain $OUTPUT_FILE_PATH && grep -q execution_domain $OUTPUT_FILE_PATH && grep -q parametric_domain $OUTPUT_FILE_PATH && grep -q description_domain $OUTPUT_FILE_PATH; then ok "$UNIT_TEST_NAME passed"; else error "$UNIT_TEST_NAME failed: bco is missing at least one of those domains: provenance_domain, execution_domain, parametric_domain, description_domain"; fi

### tests if bioobject domains are not empty
TEST_RESULT=$(jq 'if (.provenance_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.execution_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.parametric_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)$(jq 'if (.description_domain | length) == 0 then "empty" else "ok" end' $OUTPUT_FILE_PATH)

if echo $TEST_RESULT | grep -q empty; then error "$UNIT_TEST_NAME failed: at least one domain is empty (or does not exist)"; else ok "$UNIT_TEST_NAME passed"; fi


### Tests if stdout file exists
OUTPUT_NAME="stdout_log"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if file exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; exit 1; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT file exists"; else error "$OUTPUT file doesn't exist"; exit 1; fi


### Tests if output file exists
OUTPUT_NAME="anno_vcf_gz"
UNIT_TEST_NAME="$OUTPUT_NAME test"
info "Running $TASK_NAME $UNIT_TEST_NAME (no. $((TEST_NUMBER++)))..."
OUTPUT=$UNDERSCORED_TASK_NAME"_workflow."$UNDERSCORED_TASK_NAME"."$OUTPUT_NAME
OUTPUT_FILE_PATH=$(get_path_by_output_name $OUTPUT)
### tests if file exists
if [ -z "$OUTPUT_FILE_PATH" ]; then error "Could not find output path for $OUTPUT"; fi
if [ -f "$OUTPUT_FILE_PATH" ]; then ok "$OUTPUT_NAME file exists"; else error "$OUTPUT_NAME file doesn't exist"; fi
TEST=$( zcat $OUTPUT_FILE_PATH | grep "##" | grep  -c "CSQ" )
if  [ $TEST -eq 1 ]; then ok "$UNIT_TEST_NAME passed; VEP info added to header"; else error "$UNIT_TEST_NAME failed due to lack of VEP info in header "; fi
TEST=$( zcat $OUTPUT_FILE_PATH | grep -v '^#' | grep -c 'CSQ' )
if [ $TEST -gt 0 ]; then ok "$UNIT_TEST_NAME passed: VEP CSQ annotation added"; else error "$UNIT_TEST_NAME VEP CSQ annotation not present"; fi
printf "vcf-validator result:\n"
vcf-validator $OUTPUT_FILE_PATH

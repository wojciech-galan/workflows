#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

python3 $SCRIPT \
  --input-meta-json "$PROJECT_DIR/$1" \
  --input-inputs-fields-json "$PROJECT_DIR/src/main/wdl/tasks/meta-test/inputs-fields.json" \
  --input-outputs-fields-json "$PROJECT_DIR/src/main/wdl/tasks/meta-test/outputs-fields.json" \
  --input-workflows-fields-json "$PROJECT_DIR/src/main/wdl/tasks/meta-test/workflows-fields.json"

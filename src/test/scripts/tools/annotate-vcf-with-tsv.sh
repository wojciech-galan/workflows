#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/coverage

$SCRIPT -v
set -e -o pipefail

python3 $SCRIPT \
    --input-vcf $RESOURCES_DIR/data/vcf/282-chr15_47497301-49497301.vcf.gz \
    --annotation-tsv $RESOURCES_DIR/data/txt/coverage.chr15.tsv.gz \
    --annotation-column 3 \
    --annotation-name ISEQ_GNOMAD_COV_MEAN \
    --annotation-description "Gnomad v3 mean coverage" \
    --annotation-type String \
    --annotation-number 1 \
    --annotation-column 4 \
    --annotation-name ISEQ_GNOMAD_COV_OVER \
    --annotation-description "Gnomad v3 fraction with minimum one coverage" \
    --annotation-type String \
    --annotation-number 1 | bgzip -c > /tmp/test/coverage/test_anno-cov.vcf.gz
    echo $?


python3 $SCRIPT \
    --input-vcf $RESOURCES_DIR/data/vcf/empty.vcf.gz \
    --annotation-tsv $RESOURCES_DIR/data/txt/coverage.chr15.tsv.gz \
    --annotation-column 3 \
    --annotation-name ISEQ_GNOMAD_COV_MEAN \
    --annotation-description "Gnomad v3 mean coverage" \
    --annotation-type String \
    --annotation-number 1 \
    --annotation-column 4 \
    --annotation-name ISEQ_GNOMAD_COV_OVER \
    --annotation-description "Gnomad v3 fraction with minimum one coverage" \
    --annotation-type String \
    --annotation-number 1 | bgzip -c > /tmp/test/coverage/empty_anno-cov.vcf.gz

echo $?
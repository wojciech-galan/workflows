#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/get-variants

python3 $SCRIPT -v


python3 $SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/acmg-test.vcf.gz" \
  --output-vcf /tmp/test/get-variants/out.vcf.gz \
  --variant-tsv $RESOURCES_DIR"data/txt/acmg-test-sites.tsv.gz"

python3 $SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/empty.vcf.gz" \
  --output-vcf /tmp/test/get-variants/empty-out.vcf.gz \
  --variant-tsv $RESOURCES_DIR"data/txt/acmg-test-sites.tsv.gz"

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_JSON=$RESOURCES_DIR/json/cheated-251-wes.json
#TEST_JSON="/home/kt/no_id_provided_converted-vcf-to-json.json"
TEST_SV_JSON=$RESOURCES_DIR/json/cheated-sv.json

OUTPUT_DIR="/tmp/test/select-variants"
mkdir -p  $OUTPUT_DIR

python3 $SCRIPT \
    --input-json $TEST_JSON \
    --output-json $OUTPUT_DIR/out.json \
    --variant-number 2

## jq '.[] | .CHROM , .POS , .ISEQ_GENES_NAMES[0], .ISEQ_ACMG_SUMMARY_CLASSIFICATION'  $OUTPUT_DIR/out.json | sed 's/\"//g' | paste -d ' ' - - - -

python3 $SCRIPT \
    --input-json $TEST_SV_JSON \
    --output-json $OUTPUT_DIR/out-sv.json \
    --variant-number 4 \
    --sv


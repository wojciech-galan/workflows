#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/gtc2vcf

python3 $SCRIPT -v


python3 $SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/freeseek-nonclustered-output.vcf.gz" \
  --output-vcf /tmp/test/gtc2vcf/genotyped.vcf.gz

python3 $SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/two-sample-freeseek-nonclustered-output.vcf.gz" \
  --output-vcf /tmp/test/gtc2vcf/genotyped-two-sample.vcf.gz


#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/src/main/scripts/tools
mkdir -p /tmp/test


python3 $SCRIPT \
  --input-version '101' \
  --output-dir '/tmp/test'


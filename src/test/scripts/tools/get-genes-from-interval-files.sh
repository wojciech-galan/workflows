#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/src/main/scripts/tools/
tmp_dir=$(mktemp -d -t test_workflows-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)
chmod +x $SCRIPT

echo "-------------------Executing script-------------------"

$SCRIPT \
  --interval-files $RESOURCES_DIR/"data/interval/no_id_provided_custom-regions-nuclear.interval_list" $RESOURCES_DIR/"data/interval/no_id_provided_custom-regions-mt.interval_list" \
  --outfile "$tmp_dir/output.json"

echo "-------------------Print first 5 genes from json file-------------------"
cat "$tmp_dir/output.json" | sed -n 2,16p
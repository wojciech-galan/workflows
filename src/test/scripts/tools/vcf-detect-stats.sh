#!/bin/bash 
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

$SCRIPT -f $RESOURCES_DIR/vcf/495.fbn1-capn_genotyped_by_clinvar.vcf.gz -o "out.json" -c "$RESOURCES_DIR/json/fbn1-capn3-bam-panel-coverage.json"
#$SCRIPT -f  /tmp/monika/bam-qc/426_genotyped-by-clinvar.vcf.gz -o "out.json" -c /tmp/monika/bam-qc/panel_coverage.json
[ -z /tmp/vcf-detects-statst ] || mkdir -p /tmp/vcf-detects-statst

mv out.json /tmp/vcf-detects-statst/out.json

echo "output" /tmp/vcf-detects-statst/out.json


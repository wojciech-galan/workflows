#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/src/main/scripts/tools/
tmp_dir=$(mktemp -d -t test_workflows-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)
chmod +x $SCRIPT

$SCRIPT \
  --fastqs $RESOURCES_DIR"data/fastq/282-chr15.47497301-49497301_1.fq.gz" $RESOURCES_DIR"data/fastq/282-chr15.47497301-49497301_2.fq.gz" \
  --outdir1 "$tmp_dir/output1" \
  --outdir2 "$tmp_dir/output2"
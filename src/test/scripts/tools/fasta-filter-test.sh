#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/src/main/scripts/tools/
mkdir -p /tmp/test/acmg


$SCRIPT \
  --input-fasta $RESOURCES_DIR"data/fasta/test.fasta" \
  --input-txt $RESOURCES_DIR"data/txt/contigs-list.txt" \
  --output-fasta '/tmp/test/output.fasta'



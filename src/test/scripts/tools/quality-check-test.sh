#!/bin/bash 

set -e
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"
if [ -d target_folder ]; then rm -Rf -r target_folder; fi 

mkdir -p /tmp/quality-check-test

# Main command
python3 $SCRIPT --input-fastqc-data "$RESOURCES_DIR/txt/fastqc_data.txt" \
                --output-quality-check-json /tmp/quality-check-test/quality_check_test_json.json

#############################
echo "This is the heaad of output json:"
cat /tmp/quality-check-test/quality_check_test_json.json | head
echo "This is the heaad of output should be:"
cat $RESOURCES_DIR/json/quality-check_test_output.json | head

# cmp --silent $RESOURCES_DIR/json/quality-check_test_output.json /tmp/quality-check-test/quality_check_test_json.json

echo "Output file is here:"
echo "/tmp/quality-check-test/quality_check_test_json.json"

echo "Success, you have done good coding. "
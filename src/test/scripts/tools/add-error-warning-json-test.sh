#!/bin/bash 

set -e

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|') 
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

if [ -d /tmp/add-error-warning-json-test ]; then mkdir -p /tmp/add-error-warning-json-test; fi 

mkdir -p /tmp/add-error-warning-json-test/

python3 $SCRIPT "$RESOURCES_DIR/json/quality-check-test-with-errors.json" "$RESOURCES_DIR/json/quality-check-json-with-nulls.json" > /tmp/add-error-warning-json-test/add-error-warning-json-test-output.json

echo "Output of the script is here:"
echo "/tmp/add-error-warning-json-test/add-error-warning-json-test-output.json"

echo " Fragment of the output file:"
cat /tmp/add-error-warning-json-test/add-error-warning-json-test-output.json | head
echo "Success"
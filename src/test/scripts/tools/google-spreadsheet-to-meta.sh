#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/meta.sh/meta.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/updated-meta

python3 $SCRIPT \
  --input-workflow-name "germline" \
  --input-credentials-json "" \
  --input-meta-json "$PROJECT_DIR/src/main/wdl/pipelines/germline/meta.json" \
  --input-google-spreadsheet-key "12iYYF_JV-xu_dAkgDndIZtMPiMV-iAwIV7kIoVAHn80" \
  --output-meta-json "/tmp/test/tools/updated-meta/meta.json"

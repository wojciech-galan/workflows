#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/src/main/scripts/tools/
tmp_dir=$(mktemp -d -t test_workflows-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)
chmod +x $SCRIPT

$SCRIPT \
  --input-vcf $RESOURCES_DIR"data/vcf/annotated.test.vcf" \
  --output-name "$tmp_dir/output" \
  --output-extensions json tsv






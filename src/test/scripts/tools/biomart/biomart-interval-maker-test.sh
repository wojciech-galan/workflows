#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/main/scripts/tools/biomart
mkdir -p /tmp/test


python $SCRIPT \
  --input-dataset "hsapiens_gene_ensembl" \
  --input-host "http://ensembl.org" \
  --input-json $RESOURCES_DIR"data/json/282-panel.json" \
  --output-csv '/tmp/test/interval_output.csv'

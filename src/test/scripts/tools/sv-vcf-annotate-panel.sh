#!/bin/bash

##paths ######################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_VCF=$RESOURCES_DIR/vcf/sv-annot.vcf.gz
TEST_PANEL=$RESOURCES_DIR/json/example-panel.json

### help #############################
printf "This script rquires pysam\n"
printf "\n"
printf "Help screen:\n"
printf "************\n"

python3 $SCRIPT -h

### usage ###########################

printf "\n"
printf "Running test1: annotation with non empty panel \n"
printf "************\n"

mkdir -p /tmp/test/sv-vcf-panel-filter
OUTDIR="/tmp/test/sv-vcf-panel-filter"

python3 $SCRIPT \
       --input-vcf $TEST_VCF \
       --output-vcf $OUTDIR/annotated.vcf.gz \
       --gene-panel $TEST_PANEL 

n=$( zcat  $OUTDIR/annotated.vcf.gz | grep -c 'ISEQ_GENE_PANEL_OVERLAP' )
if [ $n -gt 1 ];then
  printf "OK, annotation was added.\n"
else
  printf "ERROR, Annotation was not added.\n"
fi

printf "\n"
printf "Running test2: annotation with empty panel \n"
printf "************\n"


python3 $SCRIPT \
       --input-vcf $TEST_VCF \
       --output-vcf $OUTDIR/empty-panel.vcf.gz 
       

n=$( zcat  $OUTDIR/empty-panel.vcf.gz | grep -c 'ISEQ_GENE_PANEL_OVERLAP' )
if [ $n -eq 1 ];then
  printf "OK, annotation was added (to header).\n"
else
  printf "ERROR, Annotation was not added.\n"
fi

#n=$( zcat  $OUTDIR/panel-filtered.vcf.gz | grep -v 'ISEQ_GENE_PANEL_OVERLAP' | grep -v "^#" | wc -l )
#if [ $n -eq 0 ];then
#  printf "OK, \"Panel overlap\" filtering was done.\n"
#else
# printf "ERROR, Some lines that should be removed are still present.\n"
#fi


python3 $SCRIPT \
       --input-vcf $TEST_VCF \
       --output-vcf $OUTDIR/not-filtered.vcf.gz \
       --gene-panel $TEST_PANEL \
       --filter-type no_filter

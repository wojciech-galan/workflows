#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/vcf-anno-ge-panels

$SCRIPT \
  --input-vcf $RESOURCES_DIR"/large-data/vcf/267.vcf.gz" \
  --input-tsv "/home/mateusz/Desktop/a.tsv" \
  --output-vcf /tmp/test/tools/vcf-anno-ge-panels/annotate.vcf.gz




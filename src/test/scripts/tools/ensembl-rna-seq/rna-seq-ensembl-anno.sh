#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/test/resources/"

#mkdir -p /tmp/test/tools
cd $PROJECT_DIR/main/scripts/tools/ensembl-rna-seq
mkdir -p /tmp/test/acmg


python3 rna-seq-ensembl-anno.py \
    --input-organism-name 'hsapiens' \
    --input-version '101' \
    --input-ensembl-attributes "Chromosome/scaffold name" "Gene Synonym" "Gene stable ID"\
    --input-attributes-json "${RESOURCES_DIR}/data/json/ensembl/ensembl-attributes.json" \
    --input-versions-json "${RESOURCES_DIR}/data/json/ensembl/ensembl-versions.json" \
    --output-dir '/tmp/olaf/test/ensembl-rna-seq'


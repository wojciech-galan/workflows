#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/bco

$SCRIPT \
  --input-bco-json $RESOURCES_DIR"/data/json/bco/from-fastq-4.7M-plus-sv-calling.bco" \
  --output-csv '/tmp/test/bco/bco_csv'






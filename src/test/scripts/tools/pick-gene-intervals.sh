#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../" >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/test/resources/"


#mkdir -p /tmp/test/tools
mkdir -p /tmp/test/choose-intervals


python $SCRIPT \
  --ensembl-bed $RESOURCES_DIR/data/bed/ensembl-genes.bed \
  --gene-list $RESOURCES_DIR/data/txt/example-genes.txt \
  --output '/tmp/test/choose-intervals/small-gene.bed'




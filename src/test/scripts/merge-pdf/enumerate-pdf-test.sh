#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/\.sh/\.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data/other"
PDF_FILE_1="combined.pdf"
#test to merging pdfs

CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports/merge-pdf/$(date +%Y-%m-%d-%T | sed 's/:/-/g')/
mkdir -p $TEST_DIR"/PDFs"

cp $RESOURCES_DIR"/"$PDF_FILE_1 $TEST_DIR"/PDFs"
cp $RESOURCES_DIR"/"$PDF_FILE_1 $TEST_DIR"/PDFs"

python3 ${python_page_numbers}  --sample_id "test-sample" \
                                --pdf_files $PDF_FILE \
                                --output "test-sample_full-report.pdf"

#!/bin/bash


# DO POPRAWY!!!
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

#test to creating coverage statistics report qc-template-v4
mkdir -p /tmp/test/reports-test/
CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports-test/
cp $RESOURCES_DIR/other/test_*-qc.png $TEST_DIR

#$(date +%Y-%m-%d-%T | sed 's/:/-/g')/

#rm -r $TEST_DIR/qc-template-v2

TEST_DATA_DIR=$PROJECT_DIR/src/test/resources/data/json

$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json qc1=$TEST_DATA_DIR/qc-report-new-1.json \
--json qc2=$TEST_DATA_DIR/qc-report-new-2.json \
--json bam_qc=$TEST_DATA_DIR/report-bam-qc.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/qc-pl-v4/content.jinja \
--name report \
--output-dir /tmp/test/reports-test/ \
--pictures $TEST_DATA_DIR/quality-picture.json


$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json qc1=$TEST_DATA_DIR/qc-report-new-1.json \
--json qc2=$TEST_DATA_DIR/qc-report-new-2.json \
--json bam_qc=$TEST_DATA_DIR/report-bam-qc.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/qc-pl-v4/content-with-page-numbers.jinja \
--name report-with-page-numbers \
--output-dir /tmp/test/reports-test/ \
--pictures $TEST_DATA_DIR/quality-picture.json

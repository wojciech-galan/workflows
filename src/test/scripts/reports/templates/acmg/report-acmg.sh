#!/bin/bash
set -e

PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"
TEMPLATE_DIR=$PROJECT_DIR"src/main/scripts/reports/templates"

mkdir -p /tmp/test/report-acmg

### Test Germline
echo "######################################TEST_REPORT_GERMLINE######################################"
OUTPUT_FILE_PATH_GERMLINE="/tmp/test/report-acmg/germline.docx"

  docker run --rm -it -v /tmp/test/report-acmg:/outputs \
    -v $RESOURCES_DIR/data/json:/resources \
    -v $TEMPLATE_DIR/acmg/content.json:/resources/content.json \
    -v $PROJECT_DIR/src/main/scripts/reports:/intelliseqtools/reports \
    -v $PROJECT_DIR/src/main/scripts/reports/images:/resources/images \
  intelliseqngs/reports:4.0.7 \
  python3 /intelliseqtools/reports/templates/acmg/report-acmg.py \
  --template "/intelliseqtools/reports/templates/acmg/report-acmg-template.docx" \
  --vcf-jsons "/resources/report-acmg/pathogenic.json" \
              "/resources/report-acmg/likely-pathogenic.json" \
              "/resources/report-acmg/uncertain.json" \
              "/resources/report-acmg/likely-benign.json" \
              "/resources/report-acmg/benign.json" \
              "/resources/report-acmg/undefined.json" \
  --other-jsons "/resources/report-acmg/carrier-screening/sample-info.json" \
                "/resources/panel.json" \
                "/resources/content.json" \
                "/resources/panel_inputs.json" \
                "/resources/picture-generate-report-sh.json" \
                "/resources/images/images-width.json" \
  --analysis-type "exome" \
  --analysis-start "fastq" \
  --analysis-group "germline" \
  --output-filename "/outputs/germline.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH_GERMLINE" \
        --outdir "/tmp/test/report-acmg"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH_GERMLINE | grep -c 'ACMG')

if [ -f "$OUTPUT_FILE_PATH_GERMLINE" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "ACMG phrase exists"; fi

### Test Carrier Screening
echo "######################################TEST_REPORT_CARRIER_SCREENING######################################"
OUTPUT_FILE_PATH_CARRIER_SCREENING="/tmp/test/report-acmg/carrier-screening.docx"

  docker run --rm -it -v /tmp/test/report-acmg:/outputs \
    -v $RESOURCES_DIR/data/json:/resources \
    -v $TEMPLATE_DIR/acmg/content.json:/resources/content.json \
    -v $PROJECT_DIR/src/main/scripts/reports:/intelliseqtools/reports \
    -v $PROJECT_DIR/src/main/scripts/reports/images:/resources/images \
  intelliseqngs/reports:4.0.7 \
  python3 /intelliseqtools/reports/templates/acmg/report-acmg.py \
  --template "/intelliseqtools/reports/templates/acmg/report-carrier-screening-template.docx" \
  --vcf-jsons "/resources/report-acmg/carrier-screening/pathogenic.json" \
              "/resources/report-acmg/carrier-screening/likely-pathogenic.json" \
              "/resources/report-acmg/carrier-screening/others.json" \
  --other-jsons "/resources/report-acmg/carrier-screening/sample-info.json" \
                "/resources/panel.json" \
                "/resources/content.json" \
                "/resources/picture-generate-report-sh.json" \
                "/resources/images/images-width.json" \
  --analysis-type "exome" \
  --analysis-start "fastq" \
  --analysis-group "carrier_screening" \
  --output-filename "/outputs/carrier-screening.docx"

soffice --headless \
        --convert-to pdf "$OUTPUT_FILE_PATH_CARRIER_SCREENING" \
        --outdir "/tmp/test/report-acmg"

TIMES_MEASURE=$(pandoc $OUTPUT_FILE_PATH_CARRIER_SCREENING | grep -c 'ACMG')

if [ -f "$OUTPUT_FILE_PATH_CARRIER_SCREENING" ]; then echo "File exists"; fi
if [ $TIMES_MEASURE -gt 0 ]; then echo "ACMG phrase exists"; fi

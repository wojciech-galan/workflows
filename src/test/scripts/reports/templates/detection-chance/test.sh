#!/bin/bash

template=detection-chance
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"


CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports-test/

[ -z $TEST_DIR/$template ] | rm -r $TEST_DIR/$template
mkdir -p $TEST_DIR/$template

TEST_DATA_DIR=$PROJECT_DIR/src/test/resources/data/json
$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json genes=$TEST_DATA_DIR/report_detect_stats.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/$template/content.jinja \
--name report \
--output-dir $TEST_DIR/$template/


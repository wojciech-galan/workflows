#!/bin/bash

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

#test to creating coverage statistics report coverage-v2

CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports-test/

#$(date +%Y-%m-%d-%T | sed 's/:/-/g')/

rm -r $TEST_DIR/coverage-v2
mkdir -p $TEST_DIR/coverage-v2
#$(date +%Y-%m-%d-%T | sed 's/:/-/g')/

#mkdir -p $TEST_DIR/Pictures

#echo '[{"path":"my1.jpg"},{"path":"my2.jpg"},{"path":"my3.jpg"}]' > $TEST_DIR/pictures.json

TEST_DATA_DIR=$PROJECT_DIR/src/test/resources/data/json
$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json wes_or_wgs=$TEST_DATA_DIR/genome-or-exome.json,coverage=$TEST_DATA_DIR/merged_metrics.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/coverage-v2/content.jinja \
--name report \
--output-dir $TEST_DIR/coverage-v2/

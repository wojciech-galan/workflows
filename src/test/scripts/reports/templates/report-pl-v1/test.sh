#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"


TEMPLATE=report-pl-v1
#test to creating coverage statistics report coverage-v2

CURRENT_DIR=$(pwd)
TEST_DIR=/tmp/test/reports-test/

#$(date +%Y-%m-%d-%T | sed 's/:/-/g')/

rm -r $TEST_DIR/Pictures
mkdir -p $TEST_DIR/Pictures

rm -r $TEST_DIR/$TEMPLATE
mkdir -p $TEST_DIR/$TEMPLATE

#echo '[{"path":"my1.jpg"},{"path":"my2.jpg"},{"path":"my3.jpg"}]' > $TEST_DIR/pictures.json

tar xvzf $RESOURCES_DIR/other/282-chr15_47497301-49497301_igv-images.tar.gz -C $TEST_DIR/Pictures

touch $TEST_DIR/Pictures/picture.txt
echo "[" > $TEST_DIR/Pictures/picture-generate-report-sh.json
for png in $TEST_DIR/Pictures/*.png
do
  # Create png_name extracting chr and pos from png file name (png_name="chr15-48497301")
  png_name=$(echo $png | sed 's/.*\(chr.*-[0-9]*\)-igv-screenshot.png/\1/');
  # Get the path to png file
  png_path=$(realpath $png);
  png_basename=$(basename $png);
  # Save png_name and path to picture.txt ({"chr15-48497301":"/home/dzesikah/test/Pictures/test_chr15-48497301-igv-screenshot.png"})
  echo \"$png_name\":\"$png_basename\" >> $TEST_DIR/Pictures/picture.txt
  echo "{\"path\" : \""$png_path"\"},">> $TEST_DIR/Pictures/picture-generate-report-sh.json
done

# Replace new lines with commas, add {}
cat $TEST_DIR/Pictures/picture.txt | tr "\n" "," | sed 's/^"/{"/' | sed 's/",$/"}/' > $TEST_DIR/Pictures/picture.json

sed -i '$ s/.$//' $TEST_DIR/Pictures/picture-generate-report-sh.json
echo "]" >> $TEST_DIR/Pictures/picture-generate-report-sh.json


TEST_DATA_DIR=$PROJECT_DIR/src/test/resources/data/json
$PROJECT_DIR/src/main/scripts/reports/generate-report.sh \
--json patient=$TEST_DATA_DIR/germline-patient.json \
--json phenotypes=$TEST_DATA_DIR/germline-phenotypes.json \
--json panel_phenotypes=$TEST_DATA_DIR/282-chr15_47497301-49497301_panel_phenotypes.json \
--json panel_diseases=$TEST_DATA_DIR/282-chr15_47497301-49497301_panel_diseases.json \
--json sample=$TEST_DATA_DIR/germline-sample.json \
--json variants_uncertain=$TEST_DATA_DIR/282-chr15_47497301-49497301_variants-uncertain.json \
--json variants_pathogenic=$TEST_DATA_DIR/282-chr15_47497301-49497301_variants-pathogenic.json \
--json variants_likely_pathogenic=$TEST_DATA_DIR/282-chr15_47497301-49497301_variants-likely-pathogenic.json \
--json dict=$PROJECT_DIR/src/main/scripts/reports/templates/$TEMPLATE/dict.json \
--json seq_type=$PROJECT_DIR/src/main/scripts/reports/templates/$TEMPLATE/genome-pl.json \
--json picture=$TEST_DIR/Pictures/picture.json \
--template $PROJECT_DIR/src/main/scripts/reports/templates/$TEMPLATE/content.jinja \
--name report \
--output-dir $TEST_DIR/$TEMPLATE \
--pictures $TEST_DIR/Pictures/picture-generate-report-sh.json

#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

$SCRIPT \
  --input-path-to-fastqc-files $RESOURCES_DIR"/txt/fastqc/paired-end/fastqc-data" \
  --output-file-path /tmp/test/rna-seq/overrepresented/first-script/




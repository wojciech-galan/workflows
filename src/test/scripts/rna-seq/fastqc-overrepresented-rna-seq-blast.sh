#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/test/rna-seq/overrepresented/blast

$SCRIPT \
  --input-overrepresented-file $RESOURCES_DIR"/excel/overrepresented.xlsx" \
  --fasta-file-name "/tmp/test/rna-seq/overrepresented/blast/testfile.fasta" \
  --results-file-name "/tmp/test/rna-seq/overrepresented/blast/results.xml" \
  --database "/data/test/large-data/swissprot/swissprot" \
  --blast-excel-file-name "/tmp/test/rna-seq/overrepresented/blast/blast.xlsx" \
  --output-file-name "/tmp/test/rna-seq/overrepresented/blast/overrepresented"

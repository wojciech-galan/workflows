#!/bin/bash
#!/bin/bash

##paths ######################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_VCF=$RESOURCES_DIR/vcf/simple-repeat-test.vcf.gz
TEST_REPEAT_BED=$RESOURCES_DIR/bed/simpleRepeat-test.bed.gz

#echo $SCRIPT
#echo $PROJECT_DIR
#echo $RESOURCES_DIR
#echo $TEST_VCF

### help #############################

printf "\n"
printf "Help screen:\n"
printf "************\n"

python3 $SCRIPT -h

### usage ######################################
printf "\n"
printf "Test:\n"
printf "************\n"

OUTPUT_DIR="/tmp/test/simple-repeat"
mkdir -p $OUTPUT_DIR

python3 $SCRIPT -i $TEST_VCF -r $TEST_REPEAT_BED | bgzip > $OUTPUT_DIR/htt-simple-repeat.vcf.gz
tabix -p vcf $OUTPUT_DIR/htt-simple-repeat.vcf.gz

### check  if output exists ###

if [ -f $OUTPUT_DIR/htt-simple-repeat.vcf.gz ]
then
   printf "OK: Output htt-simple-repeat.vcf file is present.\n"
else
   printf "Error: Expected output file htt-simple-repeat.vcf) not present.\n" 
fi

## check if works ###
if [ -f $OUTPUT_DIR/htt-simple-repeat.vcf.gz ]
then
    n=$( zcat $OUTPUT_DIR/htt-simple-repeat.vcf.gz | grep '^#' | grep -c 'ISEQ_SIMPLE_REPEAT' )
    if [ $n  -eq 1 ]
    then
         printf "OK: ISEQ_SIMPLE_REPEAT annotation added to output vcf header.\n"
    else
        printf "Warning: ISEQ_SIMPLE_REPEAT annotation was not added to output vcf header.\n"
    fi
    n_ann_lines=$( zcat $OUTPUT_DIR/htt-simple-repeat.vcf.gz |  grep -v '^#' | grep -c 'ISEQ_SIMPLE_REPEAT' )
    if [ $n_ann_lines  -eq 2 ]
    then
        positions=$( zcat  $OUTPUT_DIR/htt-simple-repeat.vcf.gz  | grep -v '^#' | grep 'ISEQ_SIMPLE_REPEAT' | cut -f 1-5 )
        printf "OK: ISEQ_SIMPLE_REPEAT annotation added to two HTT variants.\n"
        printf "Annotated variants: \n"
        printf "\n"
        zcat $OUTPUT_DIR/htt-simple-repeat.vcf.gz | grep -v '^#'  | grep 'ISEQ_SIMPLE_REPEAT' | cut -f 1-5
    elif [ $n_ann_lines  -eq 0 ]
    then
        printf "Warning: ISEQ_SIMPLE_REPEAT annotation not added to any line\n"
        printf "(should be present in lines chr4:3074877 and chr4:3074945).\n"
    else
        printf "Warning: ISEQ_SIMPLE_REPEAT annotation added to too many lines or not all expected lines.\n"
        printf "(should be present in lines chr4:3074877 and chr4:3074945).\n"
    fi
fi

printf "\n"

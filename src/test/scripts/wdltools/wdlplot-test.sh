#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/data"

mkdir -p /tmp/wdlplot
echo "======> task <======"
python $SCRIPT --wdl $PROJECT_DIR"/src/main/wdl/tasks/resources-intervals/latest/resources-intervals.wdl" | tee >( grep '<svg' >/tmp/wdlplot/plot1.svg) /tmp/wdlplot/complete.txt | grep -v "<svg"
echo "======> module <======"
python $SCRIPT --wdl $PROJECT_DIR"/src/main/wdl/modules/alignment/latest/alignment.wdl" | tee >( grep '<svg' >/tmp/wdlplot/plot2.svg) /tmp/wdlplot/complete.txt | grep -v "<svg"
echo "======> pipeline <======"
python $SCRIPT --wdl $PROJECT_DIR"/src/main/wdl/pipelines/target-seq/latest/target-seq.wdl" | tee >( grep '<svg' >/tmp/wdlplot/plot3.svg) /tmp/wdlplot/complete.txt | grep -v "<svg"

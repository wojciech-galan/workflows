#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources"

mkdir -p /tmp/test/tools/variant-descriptor

python3 $SCRIPT -i $RESOURCES_DIR/data/vcf/282-chr15_47497301-49497301_anno-acmg.vcf.gz > /tmp/test/tools/variant-descriptor/annotated-with-acmg.vcf
#!/bin/bash

TEST_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
UTILS_DIR=$(echo $TEST_DIR | sed 's/test\/wdl.*/test\/scripts\/generic/')

source $PROJECT_DIR/src/test/scripts/test.conf

### get utils ###
source $UTILS_DIR"/help.sh"
source $UTILS_DIR"/arguments.sh"
source $UTILS_DIR"/colorcodes.sh"

### get task name and version ###
TASK_NAME=$(echo $TEST_DIR | grep -oh "test/wdl/.*" | cut -d '/' -f4)
TASK_VERSION=$(echo $TEST_DIR | grep -oh "test/wdl/.*" | cut -d '/' -f5)
info "### Testing: $TASK_NAME/$TASK_VERSION ###"

### get script name and extension###
SCRIPTNAME=$(basename -- "$SOURCE")
EXTENSION="${SCRIPTNAME##*.}"
SCRIPTNAME="${SCRIPTNAME%.*}"

### get required paths ###
MAIN_DIR=$(echo $TEST_DIR | sed 's/test/main/')
TEST_RESOURCES_DIR=$(echo $TEST_DIR | sed 's/test\/wdl.*/test\/resources\//g')
WDL=$(ls -d $MAIN_DIR/*.wdl | rev | cut -d '/' -f 1 | rev)
info "Wdl: $MAIN_DIR/$WDL"
#info "WDL: $WDL"
#TASK_NAME=${WDL%.wdl}
UNDERSCORED_TASK_NAME=$(echo $TASK_NAME | sed 's/-/_/g')
DATE_AND_TIME=$(date +%Y-%m-%d_%H-%M)

### Set working directory, create symlinks to input/output directories ###
DATE_AND_TIME=$(date +%Y-%m-%d_%H-%M)
WORKING_DIR=$TEST_EXECUTION_DIR/$TASK_NAME/$DATE_AND_TIME


mkdir -p $TEST_EXECUTION_DIR; chmod 777 $TEST_EXECUTION_DIR 2>/dev/null
mkdir -p $TEST_EXECUTION_DIR/$TASK_NAME; chmod 777 $TEST_EXECUTION_DIR/$TASK_NAME 2>/dev/null
mkdir -p $WORKING_DIR

### validating meta
META=$(python3 $PROJECT_DIR/src/main/scripts/bco/bco-meta.py --print_result $MAIN_DIR/$WDL 2>/dev/null)
if [ "$META" = "{}" ]; then
    warn "Wrong meta";
elif [ "$META" = "" ]; then
    warn "bco-meta.py exited unexpectedly (probably due to the problems with miniwdl)";
fi

# info "RUNNING: $TASK_NAME:$TASK_VERSION"
info "Working directory: $WORKING_DIR"
#info "TEST_EXECUTION_DIR: $TEST_EXECUTION_DIR"

if [ ! -f "$TEST_EXECUTION_DIR/cromwell.jar" ]; then
  info "Downloading cromwell.jar..."
  curl -o $TEST_EXECUTION_DIR/cromwell.jar $CROMWELL_JAR
  curl -o $TEST_EXECUTION_DIR/cromwell.cfg $CROMWELL_LOCAL_CFG
  #wget -O $/aws.cfg $CROMWELL_AWS_CFG
  #sed -i "s,BUCKET_TAG,$AWS_BUCKET," $TEST_EXECUTION_DIR/aws.cfg
  #sed -i "s,QUEUE_TAG,$AWS_QUEUE," $TEST_EXECUTION_DIR/aws.cfg
fi

cd $WORKING_DIR
ln -s $TEST_RESOURCES_DIR 2> /dev/null
if [ -z "$INPUT_FILE" ]; then
  INPUT_SOURCE=$TEST_DIR/$SCRIPTNAME"-input.json"
else
  INPUT_SOURCE=$TEST_DIR/$INPUT_FILE
fi
info "INPUT_SOURCE $INPUT_SOURCE"
INPUT_FILE=$WORKING_DIR/$SCRIPTNAME"-input.json"
#info "input path: "$TEST_DIR/$SCRIPTNAME"-input.json"

if [ "$AWS" == "TRUE" ]; then
  CFG=/opt/tools/cromwellaws/aws.cfg
  cat $INPUT_SOURCE | sed "s|\$TASK_TEST_RESOURCES|s3://iseq/testdata|g" > $INPUT_FILE
else
  CFG=$TEST_EXECUTION_DIR/cromwell.cfg
  cat $INPUT_SOURCE | sed "s|\$TASK_TEST_RESOURCES|$WORKING_DIR/resources|g" | sed "s|\$PROJECT_DIR|$PROJECT_DIR|g" > $INPUT_FILE
fi

### Start cromwell ###
info "Starting Cromwell..."
#info "Cromwell path: $TEST_EXECUTION_DIR/cromwell.jar"
#info "Input path: $INPUT_FILE"
#info "WDL path: $MAIN_DIR/$WDL"

if [ ! "$VERBOSE" == "TRUE" ]; then
  java -Dconfig.file=$CFG -jar $TEST_EXECUTION_DIR/cromwell.jar run -i $INPUT_FILE $MAIN_DIR/$WDL &> cromwell-execution.log
else
  java -Dconfig.file=$CFG -jar $TEST_EXECUTION_DIR/cromwell.jar run -i $INPUT_FILE $MAIN_DIR/$WDL | tee cromwell-execution.log
fi
info "Finished Task..."
info "Log: $WORKING_DIR/cromwell-execution.log"

if [ "$AWS" == "TRUE" ]; then
  ### Get task ID and task name
  TASK_NAME_LOG=$(cat cromwell-execution.log | grep -Eo "Starting.*\..*" | cut -d " " -f 2)
  #info "Task name: $TASK_NAME_LOG"
  TASK_ID=$(cat cromwell-execution.log | grep "\":" | sed 's/"//g' | sed 's/ //g' | grep "id:" | cut -d ':' -f 2)
  #info "Task ID: $TASK_ID"
  TASK_STATUS=$(cat cromwell-execution.log | grep -Eo "finished with status .*'" | cut -d ' ' -f 4 | sed "s/'//g")
  info "Workflow status: $TASK_STATUS"

  ### Check task status ###
  if [ $TASK_STATUS == "Succeeded" ]; then
    ok "Generic test status: PASS"; exit 0;
  else
    error "Generic test status: FAILED"; exit 1;
  fi
fi

# Get cromwell-executions subdirectory ID
EXECUTION_ID=$(ls $WORKING_DIR/cromwell-executions/ 2> /dev/null)
if [ ! $? -eq 0 ]; then
    error "No output in cromwell-executions"
    exit 1;
fi

### Get task ID and task name
TASK_NAME_LOG=$(cat cromwell-execution.log | grep -Eo "Starting.*\..*" | cut -d " " -f 2)
#info "Task name: $TASK_NAME_LOG"
TASK_ID=$(cat cromwell-execution.log | grep "\":" | sed 's/"//g' | sed 's/ //g' | grep "id:" | cut -d ':' -f 2)
#info "Task ID: $TASK_ID"
TASK_STATUS=$(cat cromwell-execution.log | grep -Eo "finished with status .*'" | cut -d ' ' -f 4 | sed "s/'//g")
info "Workflow status: $TASK_STATUS"

### Check task status ###
if [ $TASK_STATUS == "Succeeded" ]; then
  ok "Generic test status: PASS";
else
  error "Generic test status: FAILED"; exit 1;
fi

######################
### Run task tests ###
######################

TASK_NAME_HARD_SPACES=$(echo $TASK_NAME | sed "s/-/_/g" )
CALL_DIR="call-"$TASK_NAME_HARD_SPACES
WORKFLOW_DIR=$TASK_NAME_HARD_SPACES
WORKFLOW_NAME=$(cat $WORKING_DIR/cromwell-execution.log | grep -o ": Starting.*" | head -1 | cut -d ' ' -f 3 | cut -d '.' -f 1)

function get_path_by_output_name {
  cat $WORKING_DIR/cromwell-execution.log | \
    #echo $(grep "\"$1\"" | sed 's/"//g' | sed 's/ //g' | cut -d ":" -f2 | tail -n 1 | sed 's/,//g')
    echo $(grep "\"$1\"" | sed 's/"//g' | sed 's/ //g' | cut -d ":" -f2 | tail -n 1 | sed 's/\[//g' | cut -d ',' -f 1)
}
export -f get_path_by_output_name


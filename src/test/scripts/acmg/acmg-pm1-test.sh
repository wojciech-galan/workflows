#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

OUR_PATH=$(pwd)

cd $PROJECT_DIR/src/main/scripts/acmg/

mkdir -p /tmp/test/acmg

wget -O /tmp/test/acmg/clinvar-protein-changes-dictionary.json http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/09-12-2020/clinvar-protein-changes-dictionary.json
wget -O /tmp/test/acmg/clinvar-pathogenic-sites.tab  http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/09-12-2020/clinvar-pathogenic-sites.tab 
wget -O /tmp/test/acmg/uniprot-functional-data-from-table.json http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/uniprot/09-12-2020/uniprot-functional-data-from-table.json


$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated-pm1.vcf.gz \
  --uniprot-func /tmp/test/acmg/uniprot-functional-data-from-table.json \
  --clinvar-changes /tmp/test/acmg/clinvar-protein-changes-dictionary.json \
  --clinvar-patho /tmp/test/acmg/clinvar-pathogenic-sites.tab \
  --aa  $PROJECT_DIR/src/main/scripts/acmg/aa-symbols.json

#zcat /tmp/test/acmg/annotated.vcf.gz | grep -v "#" | head



#TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep "##INFO=ISEQ_PM1" | wc -l)
#if [ $TEST_RESULT -ge 1 ]; then echo ".vcf file is annotated with ISEQ_PM1 field"; else echo "error. Annotation with ISEQ_PM1 field failed"; fi
#TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -v "ISEQ_PM1_SCORE=0" | grep -v "##" | wc -l )
#if [ $TEST_RESULT -ge 1 ]; then echo "ISEQ_PM1_SCORE is annotated successfully. OK"; else echo "error. Annotation with ISEQ_PM1 field failed. Check your script or change test-vcf file"; fi



cd $OUR_PATH


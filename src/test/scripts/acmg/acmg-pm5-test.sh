#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

cd $PROJECT_DIR/src/main/scripts/acmg/

mkdir -p /tmp/test/acmg

wget -O /tmp/test/acmg/clinvar-protein-changes-dictionary.json http://anakin.intelliseq.pl/public/intelliseqngs/workflows/resources/acmg/clinvar/09-12-2020/clinvar-protein-changes-dictionary.json


#$SCRIPT \
#  --input-vcf $RESOURCES_DIR"large-data/vcf/clinvar_anno-ghr.vcf.gz" \
#  --output-vcf /tmp/test/acmg/annotated-pm5.vcf.gz \
#  --clinvar-changes /tmp/test/acmg/clinvar-protein-changes-dictionary.json

$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz" \
  --output-vcf /tmp/test/acmg/267-annotated-pm5.vcf.gz \
  --clinvar-changes /tmp/test/acmg/clinvar-protein-changes-dictionary.json

#zcat /tmp/test/acmg/annotated.vcf.gz | grep -v "#" | head

#$SCRIPT --version

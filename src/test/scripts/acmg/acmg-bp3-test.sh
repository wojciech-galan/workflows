#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg

$SCRIPT -v

$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated-bp3.vcf.gz

#zcat /tmp/test/acmg/annotated.vcf.gz | grep -v "#" | head


TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep "##INFO=ISEQ_BP3" | wc -l)
if [ $TEST_RESULT -ge 0 ]; then echo ".vcf file is annotated with ISEQ_BP3 field"; else echo "error. Annotation with ISEQ_BP3 field failed"; fi
TEST_RESULT=$(echo $OUTPUT_FILE_CONTENT | grep -v "ISEQ_BP3_SCORE=0" | grep -v "##" | wc -l )
if [ $TEST_RESULT -ge 0 ]; then echo "ISEQ_BP3_SCORE is annotated successfully. OK"; else echo "error. Annotation with ISEQ_BP3 field failed. Check your script or change test-vcf file"; fi


#$SCRIPT --version

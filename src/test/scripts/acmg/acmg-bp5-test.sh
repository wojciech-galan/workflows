#!/bin/bash
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." >/dev/null 2>&1 && pwd )"
RESOURCES_DIR=$PROJECT_DIR"/src/test/resources/"

mkdir -p /tmp/test/acmg

$SCRIPT -v

$SCRIPT \
  --input-vcf $RESOURCES_DIR"large-data/vcf/267.vcf.gz" \
  --output-vcf /tmp/test/acmg/annotated.vcf.gz




TEST_RESULT=$(zcat /tmp/test/acmg/annotated.vcf.gz | grep "^##" | grep "ISEQ_ACMG_BP5" | wc -l)
if [ $TEST_RESULT -gt 0 ]; then echo ".vcf file is annotated with ISEQ_ACMG_BP5 field"; else echo "error. Annotation with ISEQ_ACMG_BP5 field failed"; fi
TEST_RESULT=$(zcat /tmp/test/acmg/annotated.vcf.gz |grep -v "^##" | grep "ISEQ_ACMG_BP5_SCORE=1" | wc -l )
echo $TEST_RESULT
if [ $TEST_RESULT -gt 0 ]; then echo "ISEQ_ACMG_BP5_SCORE is annotated successfully. OK"; else echo "error. Annotation with ISEQ_ACMG_BP5 field failed. Check your script or change test-vcf file"; fi


#$SCRIPT --version

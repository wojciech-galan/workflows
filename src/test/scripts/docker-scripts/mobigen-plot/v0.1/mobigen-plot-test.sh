#!/bin/bash
echo
printf " Test for mobigen-plot.sh script\n"
printf " *******************************\n"
printf " \n"

### paths ######################################################################################

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' )
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../../.." >/dev/null 2>&1 && pwd )"
INPUT=$PROJECT_DIR"/src/test/resources/data/json/mobigen.json"
echo

### help ######################################################################################
printf " Help screen:\n"
printf "\n"
$SCRIPT -h
echo

### exctracting data and plotting #############################################################

printf " Plotting:\n"
printf "   Expected result:\n"
printf "   two pictures:\n"
printf "   1) breast_cancer_PGS000001_afr_model.svg\n"
printf "   2) something_else_than_breast_cancer_PGS000001_nfe_model.svg\n"
printf "   one table:\n"
printf "   3) lactose_model.svg\n"
printf "   And three annoying 'null device' messages from R ;)\n"
printf " \n"

$SCRIPT -i $INPUT
#echo $SCRIPT
#echo $PROJECT_DIR
printf "\n"

### checking if files were created ###########################################################

printf " Checking if output files were created:\n"
echo
if [[ -f breast_cancer_PGS000001_afr_model.svg ]] && [[ -f something_else_than_breast_cancer_PGS000001_nfe_model.svg ]] && [[ -f lactose_model.svg ]]
then
   printf " OK: All output files present\n"
else
   printf " Test failed: expected outpt files not present\n"
fi

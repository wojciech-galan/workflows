#!/bin/bash

### paths #####################################################################################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.R/' )
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../.." >/dev/null 2>&1 && pwd )"
INPUT_M=$PROJECT_DIR"/src/test/resources/data/txt/sv-dataframe-male.txt"
INPUT_F=$PROJECT_DIR"/src/test/resources/data/txt/sv-dataframe.txt"
### plotting ##################################################################################
mkdir -p /tmp/test/sv-plot
echo
printf " Plotting:\n"
Rscript $SCRIPT --input $INPUT_M --output /tmp/test/sv-plot/sv-m.svg --sex M
Rscript $SCRIPT --input $INPUT_F --output /tmp/test/sv-plot/sv-f.svg --sex F
Rscript $SCRIPT --input $INPUT_M --output /tmp/test/sv-plot/sv-u.svg --sex U
Rscript $SCRIPT --input ~/empty.df --output /tmp/test/sv-plot/sv-empty.svg --sex U

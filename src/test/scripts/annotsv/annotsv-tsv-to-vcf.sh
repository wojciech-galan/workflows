#!/bin/bash

SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_TSV=$RESOURCES_DIR/txt/test_annotsv-to-vcf.tsv
TEST_VCF=$RESOURCES_DIR//vcf/annotsv-to-vcf-test.vcf.gz
DESCRIPTIONS=$RESOURCES_DIR/txt/descriptions-annotsv.tsv


printf "Running script: \n"
mkdir -p /tmp/test/annotsv-tsv-to-vcf
OUTDIR="/tmp/test/annotsv-tsv-to-vcf"

python3 $SCRIPT \
        -i $TEST_VCF \
        -d  $DESCRIPTIONS \
        -t $TEST_TSV \
        -o $OUTDIR/annotsv.vcf
        
bgzip  $OUTDIR/annotsv.vcf

n=$( zcat $OUTDIR/annotsv.vcf.gz | grep -v '^#' | wc -l )
if [ $n -gt 1 ];then
  printf "OK, output vcf is not empty.\n"
else
  printf "ERROR, output vcf is empty.\n"
fi

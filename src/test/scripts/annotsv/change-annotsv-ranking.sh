#!/bin/bash

##paths ######################
SCRIPT=$(realpath "${BASH_SOURCE[0]}" | sed 's/test/main/' | sed 's/-test//' | sed 's/.sh/.py/')
PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd | sed 's|/workflows/.*|/workflows/|')
RESOURCES_DIR=$PROJECT_DIR"src/test/resources/data"
TEST_TSV=$RESOURCES_DIR/txt/test_annotated-with-annotsv.tsv.gz

#echo $SCRIPT
#echo $PROJECT_DIR
#echo $RESOURCES_DIR
#echo $TEST_VCF

### usage ###########################

printf "\n"
printf "Running script: \n"
printf "************\n"

mkdir -p /tmp/test/change-ranking
OUTDIR="/tmp/test/change-ranking"

python3 $SCRIPT \
       --input_annotsv $TEST_TSV \
       --output_short $OUTDIR/first-output-file.tsv \
       --output_full $OUTDIR/second-output-file.tsv.gz

n=$( grep -c '^' $OUTDIR/first-output-file.tsv )
if [ $n -gt 1 ];then
  printf "OK, main output file (first-output-file.tsv) is not empty.\n"
else
  printf "ERROR, main output file (first-output-file.tsv) is empty.\n"
fi

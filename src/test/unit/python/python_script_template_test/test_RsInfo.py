import pytest
from . import RsInfo


def test_init_proper_argument_type():
    RsInfo('rs1', 'A', 'C')


@pytest.mark.parametrize("rsid, ref, alt", [('rs1', 'A', []), ('rs1', (), 'C'), ({}, 'A', 'C')])
def test_init_improper_argument_type(rsid: str, ref: str, alt: str):
    with pytest.raises(AssertionError):
        RsInfo(rsid, ref, alt)


# so called fixture
@pytest.fixture(scope="module")
def sample_rsinfo() -> RsInfo:
    return RsInfo('rs1', 'A', 'C')


def test_proper_rsid(sample_rsinfo: RsInfo):
    assert sample_rsinfo.rsid == 'rs1'


def test_proper_ref(sample_rsinfo: RsInfo):
    assert sample_rsinfo.ref == 'A'


def test_proper_alt(sample_rsinfo: RsInfo):
    assert sample_rsinfo.alt == 'C'

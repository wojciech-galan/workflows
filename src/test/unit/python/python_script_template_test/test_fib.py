import pytest
from . import fib


def test_zero():
    assert fib(0) == 0


def test_one():
    assert fib(1) == 1


def test_bigger():
    assert fib(3) == 2


@pytest.mark.parametrize("n", [-1, '1', [], True])
def test_wrong_argument(n):
    with pytest.raises(AssertionError):
        fib(n)

# By default pytest only identifies the file names starting with test_ or ending with _test as the test files.
# how to install required packages:
# pip install pytest pytest-html pytest-xdist
# how to run:
# - pytest in directory containing tests - simple run
# - pytest -n 3 - to run tests in parallel
# - pytest –-html=report.html - to obtain report in html

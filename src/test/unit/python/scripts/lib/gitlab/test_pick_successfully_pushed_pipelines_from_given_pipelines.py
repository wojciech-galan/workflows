from . import pick_successfully_pushed_pipelines_from_given_pipelines

pipelines = \
    [{'id': 119360376,
      'sha': 'c9f556e0b3585ab193b5fa558b5fc101838f84f9',
      'ref': 'dev',
      'status': 'running'},
     {'id': 119360010,
      'sha': '58047eb1c6f331a08f99a652ac79757a56843316',
      'ref': 'dev',
      'status': 'success'},
     {'id': 119356914,
      'sha': '0e4bcc7b92180a0eab72218f83d8be75d637aeac',
      'ref': 'quality-check-module',
      'status': 'success'},
     {'id': 120016118,
      'sha': 'cff450b14aab7da240bb675793dc9b8e4deb3c49',
      'ref': 'report-refactoring',
      'status': 'success'}
     ]


def test_picked_proper_pipelines():
    successfully_pipelines = pick_successfully_pushed_pipelines_from_given_pipelines(pipelines)
    ids = set([pipeline['id'] for pipeline in successfully_pipelines])
    assert ids == {119360010, 119356914, 120016118}


def test_order_preserved():
    successfully_pipelines = pick_successfully_pushed_pipelines_from_given_pipelines(pipelines)
    ids = [pipeline['id'] for pipeline in successfully_pipelines]
    assert ids == [119360010, 119356914, 120016118]

import pytest
from . import merge_dockers
from . import Docker

input_simple = [('1', 'b'), ('2', 'a')]

input_one_double = [('1', 'b'), ('2', 'a'), ('1', 'c')]

input_two_doubles = [('1', 'b'), ('2', 'a'), ('1', 'c'), ('2', 'd')]


def test_merge_not_required():
    assert merge_dockers(input_simple) == {'1':frozenset('b'), '2':frozenset('a')}


def test_merge_required_for_one_docker():
    assert merge_dockers(input_one_double) == {'1':frozenset('bc'), '2':frozenset('a')}


def test_merge_required_for_two_dockers():
    assert merge_dockers(input_two_doubles) == {'1':frozenset('bc'), '2':frozenset('ad')}

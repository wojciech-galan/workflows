import os
import sys

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 6)[0])

from scripts.check_import_strings_in_modules_and_pipelines import check_import_string, ImproperImportStringError

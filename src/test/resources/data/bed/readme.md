## BED

This directory contains scripts needed to create test files (./src/test/resources).

---

## Table of Contents

**beds**
  * [Genes regions](#genes-regions)
    *[NF2, chr22](#nf2-chr22)

---

### NF2, chr22

```bash
printf "chr22\t29601556\t29700600\n" > chr22-nf2-gene.bed
```

```
[Return to the table of contents](#table-of-contents)
---

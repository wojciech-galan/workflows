# TEST FILES - FASTQ

## Table of contents
- [RNA-seq testing](#rna-seq-testing)
  * [Small rna-seq human samples (rna-seq project id: 0009-human-small-ra-rnaseq)](#small-rna-seq-human-samples--rna-seq-project-id--0009-human-small-ra-rnaseq-)
  * [Another files for RNA-seq testing](#another-files-for-rna-seq-testing)
- [Fq organize](#fq-organize)
- [WGS, WES, targeted-sequencing](#wgs--wes--targeted-sequencing)
  * [FBN1](#fbn1)
  * [Simulated reads](#simulated-reads)
    + [ATAD sample - child](#atad-sample---child)
    + [Deletion of 8000 nucleotides](#deletion-of-8000-nucleotides)
-----------------------------------
This directory contains .fastq.gz files for testing various wdl scripts. Files are smaller versions of real data or synthetic, simulated samples. 
Small files are defined as files of size less than 5Mb.

## RNA-seq testing
### Small rna-seq human samples (rna-seq project id: 0009-human-small-ra-rnaseq)

**List of files:**
```
H31.fq.gz
H34.fq.gz
H68.fq.gz
H71.fq.gz
```

**How it was generated?**

Files have been aligned to the human reference genome, GRCh38. The readings from the gene regions (hsa-mir-3158-1,
hsa-mir-3158-2, hsa-mir-7-2, hsa-mir-3529, hsa-mir-7-3, hsa-mir-607) were then excised from the bam file (+ - 1000 base pairs).
The resulting fastq files were created from these bam files.

1. made in Python, dictionary made by hand with data from Ensembl:
```
positions = [
      {"gene": "hsa-mir-3158-1",
        "start": 101601417,
        "stop": 101601497,
        "chr": 10},
      {
        "gene": "hsa-mir-3158-2" ,
        "start": 101601417,
        "stop": 101601497,
        "chr": 10},
      {
        "gene":"hsa-mir-7-2" ,
        "start": 88611825,
        "stop": 88611934,
        "chr": 15},
      {
        "gene":"hsa-mir-3529" ,
        "start":88611847 ,
        "stop": 88611924,
        "chr": 15},
      {
        "gene": "hsa-mir-7-3",
        "start": 4770670,
        "stop": 4770779,
        "chr": 19},
      {
        "gene": "hsa-mir-607 ",
        "start": 96828669 ,
        "stop": 96828764,
        "chr": 10 }]

for i in positions:
    i["_start"] = i["start"]-1000
    i["_stop"] = i["stop"]+1000
    i["position"] = "chr{0}:{1}-{2}".format(i["chr"], i["_start"], i["_stop"])

for i in positions:
    print(i["position"])

```
2. printed data in ``positions.txt``
```
chr10:101600417-101602497 chr10:101600417-101602497 chr15:88610825-88612934 chr15:88610847-88612924 chr19:4769670-4771779 chr10:96827669-96829764
```

```
#!/bin/bash

for i in 31 34 68 71; do
  samtools view -b -o small_H${i}.bam H${i}.bam $(cat positions.txt)
  gatk SamToFastq -I small_H${i}.bam -FASTQ H${i}.fq
  bgzip H${i}.fq
done
`````
### Another files for RNA-seq testing

``306_R1_T1_R1.fq.gz``
``306_R1_T1_R2.fq.gz``

## Fq organize

Files for testing: ``src/main/wdl/tasks/fq-organize/fq-organize.wdl``

Empty files for fq organization test:

- `22094_S5_L001_R1_001.fastq.gz`
- `22094_S5_L001_R2_001.fastq.gz`
- `22094_S5_L001_R1.fastq.gz`
- `22094_S5_L001_R2.fastq.gz`
- `22094_S5_L001_1.fq.gz`
- `22094_S5_L001_2.fq.gz`

Directory with empty files to check WHAT TO CHECK:
``test-fq-organize``

## WGS, WES, targeted-sequencing

Small paired fastq.gz files needed to test workflows for WGS, WES, targeted-sequencing analysis.

Example pipelines with fastq inputs:
- `src/main/wdl/pipelines/germline.wdl`
- `src/main/wdl/pgx/pgx.wdl`
- another modules/tasks with fastq as input

### FBN1

Small fragment of chromosome 15, paired reads. There is lack of information how were created:

``282-chr15.47497301-49497301_1.fq.gz``
``282-chr15.47497301-49497301_2.fq.gz``

### Simulated reads

Synthetic, simulated samples, using neat genereads software.

#### ATAD sample - child

Child from "family" with SNP and deletion.

Documentation: https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/miscellaneous/simulated-samples/samples/ATAD3A-TRIO/readme.md
Link on anakin:
``
/data/public/intelliseqngs/workflows/resources/miscellaneous/simulated-samples/ATAD3A-TRIO/ATAD3A-TRIO-28000/child/child_1.fq.gz
/data/public/intelliseqngs/workflows/resources/miscellaneous/simulated-samples/ATAD3A-TRIO/ATAD3A-TRIO-28000/child/child_2.fq.gz
``
Also on repository workflows herein:
``
child_1.fq.gz
child_2.fq.gz
``

#### Deletion of 8000 nucleotides

Documentation: https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/miscellaneous/simulated-samples/samples/22-del-41811140-8000

Links on anakin:
``
/data/public/intelliseqngs/workflows/resources/miscellaneous/simulated-samples/22-del-41811140-8000/22-del-41811140-8000_1.fq.gz
/data/public/intelliseqngs/workflows/resources/miscellaneous/simulated-samples/22-del-41811140-8000/22-del-41811140-8000_2.fq.gz
``
Also on repository workflows herein.

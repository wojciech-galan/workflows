# TEST FILES - BAMS

This directory contains small BAM files for testing purposes. Small files are defined as files of size less than 5Mb.


## Table of Contents

* **BAMs**
   * [220 - chr11:46332116-48332116](#220---chr11:46332116---48332116)
   * [S_136 - ZMYM2 gene](#S_136---ZMYM2-gene)
   * [282 - chr15:47497301-49497301](#282---chr15:47497301---49497301)


---

### 220 - chr11:46332116 - 48332116

|                           | |
|---------------------------|-|
| **Sample ID:**            | 220 |
| **Seqencing type**:       | WES |
| **Reference genome:**     | ? |
| **Regions:**              | chr11:46332116-48332116|
| **Total length:**         | 2000 Kbp |
| **How it was generated?** | ? |


**List of files:**
```
220-chr11.46332116-48332116.markdup.recalibrated.bai
220-chr11.46332116-48332116.markdup.recalibrated.bam
220-chr11.46332116-48332116.markdup.recalibrated.bam.md5
```

[Return to the  table of contents](#test-files---bams)

### S_136 - ZMYM2 gene

|                           | |
|---------------------------|-|
| **Sample ID:**            | S_136 |
| **Seqencing type**:       | WGS |
| **Reference genome:**     | grch38-no-alt-analysis-set |
| **Regions:**              | chr13:19955406-20093845, ZMYM2 gene|
| **Total length:**         | 138.439 Kbp |
| **How it was generated?** | Aligned with the use of Sentieon app availible on DNAnexus |
| **Known variants:**       | Heterozygotic deletion: chr13:20082360-20082606 |


**List of files:**
```
S_136.ZMYM2.bai
S_136.ZMYM2.bam
S_136.ZMYM2.bam.md5
```

[Return to the  table of contents](#test-files---bams)

### 282 - chr15:47497301 - 49497301

|                           | |
|---------------------------|-|
| **Sample ID:**            | 282 |
| **Seqencing type**:       | WGS |
| **Reference genome:**     | grch38-no-alt-analysis-set |
| **Regions:**              | chr15:47497301-49497301, FBN1 gene|
| **Total length:**         |  |
| **How it was generated?** |  |
| **Known variants:**       |  |



**List of files:**
```
282-chr15_47497301-49497301.markdup.recalibrated.bai - aligned with alignment module
282-chr15_47497301-49497301.markdup.recalibrated.bam - aligned with alignment module
282-chr15_47497301-49497301.realigned.bai - realigned with haplotype caller
282-chr15_47497301-49497301.realigned.bam - realigned with haplotype caller
```
### CYP2D6-14-28-no-alt_recalibrated-mardup - chr22:42129039-42130992
|                           | |
|---------------------------|-|
| **Sample ID:**            | no ID - synthetic |
| **Seqencing type**:       | WGS |
| **Reference genome:**     | grch38-no-alt-analysis-set |
| **Regions:**              | chr22:42129039-42130992, CYPD6 gene|
| **Known variants:**       | CYP2D6 star alleles *14/*28 |

| **How it was generated?** |  |

```
samtools view -h /data/public/intelliseqngs/workflows/resources/miscellaneous/simulated-samples/pgx-test/diplotypes/cov-60/cyps-14-28/22-mln/CYP2D6-14-28-no-alt_recalibrated-markdup.bam chr22:42129039-42130992 | samtools view -bS > ~/workflows/src/test/resources/data/bam/CYP2D6-14-28-no-alt_recalibrated-markdup.bam
```

[Return to the  table of contents](#test-files---bams)



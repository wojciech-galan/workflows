## Recommendations for descriptions in WDL files:

WDL file should be reachable through the following path [task name]/[version]-latest/[task name].wdl

### FILE HEADER:
1. First 4 lines of file will not be read, it's allowed to place there any text, header or title
```
      | 1|# # # # # # # # # # # |
      | 2|#
      | 3|#  Task name
      | 4|#  **************
      | 5|#                     <-- first read line
```
2. Comments and ornaments have to begin with "#  --"
```
      | 6|#  Dependencies:
      | 7|#  -------------      <-- ornament
      | 8|#  --Important!       <-- comment
      | 9|#    Dependency1
```
3. Spacing in form of "#" are allowed
```
      |10|#
      |11|#
```
4. Using "|" after the field name allows to maintain formatting in blocks of text
```
      |12|#  Description: |
      |13|#    Part of text,
      |14|#    Some other part!
```
5. Using ">" after the field name will result in considering the following block of text as one line - irrespectively of it's formatting in the given file
```
      |15|#  Next: >
      |16|#    Even more text
      |17|#    considered as one line.
```
6. Lists must be bulleted with "- "
```
      |18|#  List:
      |19|#    - object 1
      |20|#    - object 2
      |21|#    - object 3
```
7. File description ends with first line which doesn't begin with '#' - it's ought to be empty
   (It's allowed to use "# # "... as an ornament)
```
      |22|#
      |23|# # # # # # # # # # #
      |24|                      <-- Line without "#" at the beginning means the end of file description
```
### DESCRIPTION OF INPUTS:
1. Input description consists of two following lines from which the first one is an additional description and the second one is the input itself
2. Additional description starts with "#" followed by unrestricted number of spaces, tag in form of "@Input" and arguments given in round brackets and separated by commas
```
      |25|#   @Input(name = "Field Name", desc = "Field description", min = 0.0, max = 10.0, values = ["value1", "value2", "value3"])
```
3. Input has to be given in one of the following forms:
```
      |26|    Type name
```
      OR
```
      |26|    Type? name
```
      OR
```
      |26|    Type name = value
```

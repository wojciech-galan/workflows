# SCC Prodia pipeline releases
# Table of Contents
1. [GTC quality check](#gtc-quality-check)
2. [VCF Imputing](#vcf-imputing)
3. [PGS Models](#pgs-models)
4. [NIPT QC](#nipt-qc)
5. [NIPT Analysis](#nipt-analysis)

### Microarray workflow
![Proposed flow for microarray workflow](Blank_diagram_v2.png)

### GTC Quality check
##### Version
> 1.1.1
##### Description
> Quality check for .gtc files. There are two core outputs from this workflow. The first one is `output_perIndividual_vcf_qc_json` which contains map of sample with respective qc parameters and their acceptance statuses in a form of json file. The second is `output_perIndividual_vcf` Array of vcf files for each sample.

[Link to presentation of parameters](https://gitlab.com/intelliseq/workflows/-/blob/dev/docs/scc-prodia/gtc_to_.vcf_workflow_QC_parameters.pdf)

##### Packaging
```
export WORKFLOW="gtc-to-vcf-qc"
export VERSION="1.1.1"
export TYPE=module
wget https://gitlab.com/intelliseq/workflows/-/raw/$WORKFLOW@$VERSION/src/main/wdl/$TYPE"s"/$WORKFLOW/$WORKFLOW.wdl
source <(curl -s https://gitlab.com/intelliseq/workflows/raw/zipwdl/scripts/releasewdl.sh) $WORKFLOW.wdl $WORKFLOW
```
##### Resources
[Link to test including inputs](https://gitlab.com/intelliseq/workflows/-/raw/gtc-to-vcf-qc@1.1.1/src/test/wdl/modules/gtc-to-vcf-qc/test.json)

[Link to inputs documentation](https://gitlab.com/intelliseq/workflows/-/raw/gtc-to-vcf-qc@1.1.1/src/main/wdl/modules/gtc-to-vcf-qc/meta.json)

[List of missing variants due to mismatch with reference](https://gitlab.com/intelliseq/workflows/-/raw/optional_filtering/src/main/wdl/tasks/gtc-to-vcf/unmapped-ids.txt)

##### Additional documentation:  
- [gtc-to-vcf](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/wdl/tasks/gtc-to-vcf/readme.md)
- [vcf-qc plots](https://gitlab.com/intelliseq/workflows/-/blob/vcf-qc@1.0.2/src/main/wdl/modules/gtc-to-vcf-qc/readme.md)

### VCF Imputing
##### Version
> 1.2.3
##### Description
> SNP Imputing for VCF files
##### Packaging
```
# packaging script
export WORKFLOW="vcf-imputing"
export VERSION="1.2.3"
export TYPE=module
wget https://gitlab.com/intelliseq/workflows/-/raw/$WORKFLOW@$VERSION/src/main/wdl/$TYPE"s"/$WORKFLOW/$WORKFLOW.wdl
source <(curl -s https://gitlab.com/intelliseq/workflows/raw/zipwdl/scripts/releasewdl.sh) $WORKFLOW.wdl $WORKFLOW
```
##### Resources
[Link to test including inputs](https://gitlab.com/intelliseq/workflows/-/raw/vcf-imputing@1.2.3/src/test/wdl/modules/vcf-imputing/test.json)

[Link to inputs documentation](https://gitlab.com/intelliseq/workflows/-/raw/vcf-imputing@1.2.3/src/main/wdl/modules/vcf-imputing/meta.json)


### Model Computing
##### Version
> 1.1.2
##### Description
> Computing polygenic scores
##### Packaging
```
# packaging script
export WORKFLOW="mobigen-models"
export VERSION="mock"
export TYPE=module
wget https://gitlab.com/intelliseq/workflows/-/raw/mobien_mock/src/main/wdl/$TYPE"s"/$WORKFLOW/$WORKFLOW.wdl
source <(curl -s https://gitlab.com/intelliseq/workflows/raw/zipwdl/scripts/releasewdl.sh) $WORKFLOW.wdl $WORKFLOW
```
##### Resources
[Link to test including inputs](https://gitlab.com/intelliseq/workflows/-/blob/mobien_mock/src/test/wdl/tasks/mobigen-models/test-input.json)

[Link to inputs documentation](https://gitlab.com/intelliseq/workflows/-/blob/mobien_mock/src/main/wdl/tasks/mobigen-models/mobigen-models.wdl)

### PGS Models
##### Version
> unreleased
##### Resources
[model preparation](https://gitlab.com/intelliseq/workflows/-/blob/mobigen-models-docs/src/main/wdl/tasks/mobigen-models/readme.md)

### NIPT qc
##### Version
> 1.3.1
##### Description
> Conversion of bcl files to fastq, alignment with bowtie2 or bowtie1, marking duplicates, fastq and aligment quality check

##### Packaging
```
# packaging script
export WORKFLOW="bcl-to-fq-align-qc"
export VERSION="1.3.1"
export TYPE=module
wget https://gitlab.com/intelliseq/workflows/-/raw/$WORKFLOW@$VERSION/src/main/wdl/$TYPE"s"/$WORKFLOW/$WORKFLOW.wdl
source <(curl -s https://gitlab.com/intelliseq/workflows/raw/zipwdl/scripts/releasewdl.sh) $WORKFLOW.wdl $WORKFLOW
```

[Link to test including inputs](https://gitlab.com/intelliseq/workflows/-/raw/bcl-to-fq-align-qc@1.3.1/src/test/wdl/modules/bcl-to-fq-align-qc/test.json)

[Link to inputs documentation](https://gitlab.com/intelliseq/workflows/-/raw/bcl-to-fq-align-qc@1.3.1/src/main/wdl/modules/bcl-to-fq-align-qc/meta.json)

[MultiQC metrics and thresholds](https://gitlab.com/intelliseq/workflows/-/blob/bcl-to-fq-align-qc@1.3.1/src/main/wdl/tasks/qc-multiqc/readme.md)


### NIPT analysis
##### Version
>  1.0.0
##### Description
> NIPT analysis
##### Packaging
```
# packaging script
export WORKFLOW="bam-wisecondorx"
export VERSION="1.0.0"
export TYPE=task
wget https://gitlab.com/intelliseq/workflows/-/raw/$WORKFLOW@$VERSION/src/main/wdl/$TYPE"s"/$WORKFLOW/$WORKFLOW.wdl
source <(curl -s https://gitlab.com/intelliseq/workflows/raw/zipwdl/scripts/releasewdl.sh) $WORKFLOW.wdl $WORKFLOW
```
##### Resources
[Link to test including inputs](https://gitlab.com/intelliseq/workflows/-/raw/bam-wisecondorx@1.0.0/src/test/wdl/tasks/bam-wisecondorx/test.json)

[Link to inputs documentation](https://gitlab.com/intelliseq/workflows/-/raw/bam-wisecondorx@1.0.0/src/main/wdl/tasks/bam-wisecondorx/meta.json)

### Germline
##### Version
>  1.19.1
##### Description
> Gerlmline analysis for hereditary disorders
##### Packaging
```
# packaging script
export WORKFLOW="germline"
export VERSION="1.19.1"
export TYPE=pipeline
wget https://gitlab.com/intelliseq/workflows/-/raw/$WORKFLOW@$VERSION/src/main/wdl/$TYPE"s"/$WORKFLOW/$WORKFLOW.wdl
source <(curl -s https://gitlab.com/intelliseq/workflows/raw/zipwdl/scripts/releasewdl.sh) $WORKFLOW.wdl $WORKFLOW
```
##### Resources
[Link to test including inputs](https://gitlab.com/intelliseq/workflows/-/raw/germline@1.19.1/src/test/wdl/modules/germline/test.json)

[Link to inputs documentation](https://staging.api-wdl.flow.intelliseq.com/wdl/parse?url=https://gitlab.com/intelliseq/workflows/-/raw/germline@1.19.1/src/main/wdl/pipelines/germline/germline.wdl&variant=1)



Developing documentation
============================

Adding new section
^^^^^^^^^^^^^^^^^^^^

Create new .rst file in ``docs/dev-documentation`` catalog.
Include it's name (without extension) in index.rst file under ``..  toctree::`` section.
It will be seen as you set title for it in it's .rst file.
The title must be on the top of file and the next line should contain ``=`` signs according to title's length.
For example:

| title
| ======

The line which you can see above is made of ``=`` signs and is longer than ``title``.
The table of contents in main page consists of titles and their subtitles.
Subtitle can be done analogously to the title but instead of using ``=`` you need to use ``^``.
For example:

| subtitle
| ^^^^^^^^^^

After creating new section you should go into documentation's catalog (``docs/dev-documentation``)
and generate new html files writing ``make html`` command in the terminal.
While encountering minor problems with compatibility you can use ``make clean`` and then ``make html``.
``make clean`` removes everything under ``_build`` catalog.
``_build`` catalog contains files used to generate the view of page (html, doctrees, css, js etc).

Linking to another page or subtitle
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To link to another page or subtitle you need to use ``:ref:`` keyword.
The syntax looks like this: ``:ref:`title/subtitle``` for example:

 | ``:ref:`Quick start``` :ref:`Quick start`
 | ``:ref:`Adding new section``` :ref:`Adding new section`

Making changes to documentation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Firstly, you need to check if your branch is in Active Versions (``Versions`` tab):
https://readthedocs.org/projects/workflows-dev-documentation/versions/
If it's not active you must find it in ``Activate a version`` section and activate it.
Building a version is currently synchronized with pushing a commit to target branch.
If you want to build it again or check the error you need to visit ``Builds`` tab:
https://readthedocs.org/projects/workflows-dev-documentation/builds/
To check the built version's logs and view click on the build you want to choose.
You see now logs and the result of operation.
To look at newly built documentation click ``View docs`` button on the right of the page.

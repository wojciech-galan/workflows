Developer tools
===================

Creating task, module, pipeline
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To create wdl always use script:
scripts/create-task --name task-name
or
scripts/create-module --name module-name
or
scripts/create-pipeline --name pipeline-name <- not implemented yed
How does it work?
(to na dole trzeba uprzątnąć tak, żeby miało też sens dla modułów i pajplajnów)
Script creates wdl and test files based on git user.name and task/module/pipeline name.
create-task creates proper directories in scr/main/wdl/tasks/task-name/ and src/test/wdl/tasks/task-name/ and puts there:
WDL scripts: src/main/wdl/tasks/task-name/task-name.wdl
Script to run test for task-name task: src/test/wdl/tasks/task-name/test.sh
Test for task-name task: src/main/wdl/tasks/test-test/test.json
Additionally, src/test/wdl/tasks/task-name/ is symlinked to src/main/wdl/tasks/task-name/ directory.
info, że templatki do test.sh i test.json są trzymane w https://gitlab.com/intelliseq/workflows/-/blob/dev/docs/templates/task-template, a w module-template i pipeline-template są symlinki do test.sh i test.json z task-template (więc jak coś zmieniamy to wystarczy w jednym miejscu)

wdltest
^^^^^^^^^

link do repo z wdltest
info stąd: https://gitlab.com/intelliseq/workflows/tree/dev/src/test/wdl
przykłady użycia
jakieś info o danych testowych (:ref:`Data for testing`) i jak się do nich dostać

WDL tagging
^^^^^^^^^^^^^^

dockerbuilder
^^^^^^^^^^^^^^^

Parsing meta to json
^^^^^^^^^^^^^^^^^^^^^^^

info stąd: https://gitlab.com/intelliseq/workflows/-/tree/master#parsing-meta-to-json (ale tylko dotyczące parse_meta.py)

Creating meta
^^^^^^^^^^^^^^

skrypt do generowania metadata z wdl (sprawdź, co to meta :ref:`Metadata`)
Monia przygotuje opis jak tego używać, jak skończy zadanko :)

Validating meta
^^^^^^^^^^^^^^^^^^^

info co skrypt sprawdza

Parsing google spreadsheet to meta
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
info stąd: https://gitlab.com/intelliseq/workflows/-/tree/master#parsing-google-spreadsheet-to-meta



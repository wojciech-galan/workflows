Dev documentation
=====================================

..  toctree::
 Quick start
 WDL
 Intelliseq tools
 Data sources
 Data for testing
 BCO
 IntelliseqFlow
 Developer tools
 Metadata
 Docker
 Versioning
 Create new workflow - step by step
 Developer tips
 Developing documentation

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

#!/usr/bin/python3

import os
import re
import argparse
from pathlib import Path
from typing import Generator
from typing import Pattern

__version__ = '0.0.1'


class ImproperImportStringError(RuntimeError):
    pass


def get_files_with_given_extension_from_directory(path: str, extension_without_leading_dot: str) -> \
        Generator[str, None, None]:
    return Path(path).rglob('*.{}'.format(extension_without_leading_dot))


def contains_import_string(line: str) -> bool:
    return line.startswith('import ')


def find_import_strings(path: str) -> Generator[str, None, None]:
    with open(path) as f:
        for line in f:
            if "workflow " in line:
                break
            elif contains_import_string(line):
                yield line


def check_import_string(import_string: str, f_path: str, pattern: Pattern) -> None:
    if not pattern.search(import_string):
        raise ImproperImportStringError("'{}' in file {}".format(import_string, f_path))


def check_import_strings_in_directory(path: str, pattern: Pattern) -> None:
    for f_path in get_files_with_given_extension_from_directory(path, 'wdl'):
        for import_line in find_import_strings(f_path):
            check_import_string(import_line, f_path, pattern)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Checks whether modules and pipelines import from remote dev')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--branches_allowed', nargs='+', type=str, help="Branches allowed in wdl import strings")
    arguments = parser.parse_args()

    pattern = re.compile(
        'import "https://gitlab.com/intelliseq/workflows/(-/)?raw/({})/'.format('|'.join(arguments.branches_allowed)))
    script_path = os.path.abspath(os.path.dirname(__file__))
    workflows_rot_path = script_path.rsplit(os.path.sep, 1)[0]
    wdl_path = os.path.join(workflows_rot_path, 'src', 'main', 'wdl')
    modules_path = os.path.join(wdl_path, 'modules')
    pipelines_path = os.path.join(wdl_path, 'pipelines')
    check_import_strings_in_directory(modules_path, pattern)
    check_import_strings_in_directory(pipelines_path, pattern)

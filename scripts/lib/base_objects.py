#!/usr/bin/python3

import os
import abc
import sys
from typing import Set
from typing import Union
from typing import FrozenSet

PROJECT_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, PROJECT_DIR)

__version__ = '0.0.1'


class IdentifiableByString(object):
    def __init__(self, **kwargs):
        string_id = kwargs['string_id']
        assert string_id
        super().__init__()
        self.string_identifier = string_id

    def __eq__(self, other):
        return isinstance(other, IdentifiableByString) and self.string_identifier == other.string_identifier

    def __hash__(self):
        return hash(self.string_identifier)

    def __str__(self):
        return '{}, inside {}'.format(self.__class__.__name__, self.__dict__)


class Observer(IdentifiableByString):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.observed_objects = frozenset() # type: Union[Set, FrozenSet]
        self.untestable = False

    def add_observed_object(self, observed_object):
        self.observed_objects = set(self.observed_objects)
        self.observed_objects.add(observed_object)
        self.observed_objects = frozenset(self.observed_objects)

    @abc.abstractmethod
    def fire(self):
        pass

    def set_untestable(self):
        self.untestable = True


class Observable(object):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.observers = frozenset(kwargs['observers']) or frozenset() # type:FrozenSet[Observer]

    def attach(self, observer: Observer) -> None:
        """
        Attach an observer to the subject.
        """
        self.observers.add(observer)

    def detach(self, observer: Observer) -> None:
        """
        Detach an observer from the subject.
        """
        self.observers.remove(observer)

    def notify(self) -> None:
        """
        Notify all observers about an event.
        """
        for observer in self.observers:
            observer.fire(self)

    def set_observers_untestable(self):
        for observer in self.observers:
            observer.set_untestable()
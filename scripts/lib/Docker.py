#!/usr/bin/python3

import os
import re
import sys
import pathlib
from typing import Dict
from typing import List
from typing import Union
from typing import Tuple
from typing import Pattern
from typing import Callable
from typing import Optional
from typing import FrozenSet
from typing import NamedTuple
from collections import defaultdict

PROJECT_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, PROJECT_DIR)

from scripts.lib.my_sh_utils import get_output_and_error_from_command_unsafe
from scripts.lib.base_objects import Observer
from scripts.lib.base_objects import Observable
from scripts.lib.base_objects import IdentifiableByString

__version__ = '0.0.1'

IMAGES_THAT_WILL_NOT_CHANGE = ['broadinstitute', 'ubuntu']
DOCKER_RE = re.compile(r'String +docker_image\s*=\s*(.+)')
IMAGE_PART_RE = re.compile(r'if\s+defined\(\w*\) then "(.+?):')
IMAGE_START_RE = re.compile(r'^(\S+)"')


def docker_decorator(func, images_that_wont_change:List[str]=IMAGES_THAT_WILL_NOT_CHANGE) -> Callable:
    def inner(docker_obj) -> None:
        for image_name in images_that_wont_change:
            if docker_obj.string_identifier.startswith(image_name):
                return
        func(docker_obj)
        return None
    return inner


class DockerData(NamedTuple):
    name: str
    wdls: FrozenSet

class DockerImageNotFoundException(RuntimeError):
    pass


class CantRemoveDockerImageException(RuntimeError):
    pass


class Docker(Observable, IdentifiableByString):
    def __init__(self, name: str, observers: FrozenSet[Observer] = None):
        super().__init__(string_id=name, observers=observers)

    @docker_decorator
    def rm(self) -> None:
        print('Removing docker image {}'.format(self.string_identifier))
        p = get_output_and_error_from_command_unsafe('docker image rm {}'.format(self.string_identifier))
        if p.code:
            raise CantRemoveDockerImageException("Docker image: {}, error message: {}".format(self.string_identifier, p.err))

    @docker_decorator
    def pull(self) -> None:
        print('Pulling docker image {}'.format(self.string_identifier))
        p = get_output_and_error_from_command_unsafe('docker pull {}'.format(self.string_identifier))
        if p.code:
            raise DockerImageNotFoundException("Docker image: {}, error message: {}".format(self.string_identifier, p.err))

    def pull_current_image_and_notify_task(self) -> None:
        try:
            self.rm()
        except CantRemoveDockerImageException as e:
            print('Could not remove {}: {}'.format(self.string_identifier, e))
        try:
            self.pull()
            self.notify()
        except DockerImageNotFoundException as e:
            print('Could not pull {}: {}'.format(self.string_identifier, e))
            try:
                func, args = deal_with_complex_docker_name(self.string_identifier)
            except AssertionError:
                raise DockerImageNotFoundException(self.string_identifier)
            print('args:', args)
            self.notify()
            if isinstance(args, str):
                func(args)
            else:
                func(*args)
        try:
            self.rm()
        except CantRemoveDockerImageException as e:
            print('Could not remove {}: {}'.format(self.string_identifier, e))

    def __eq__(self, other):
        if not isinstance(other, Docker):
            return False
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(tuple(sorted(self.__dict__.items())))


def get_docker_from_wdl_file(path: pathlib.PurePath, docker_re: Pattern = DOCKER_RE) -> Optional[str]:
    try:
        with open(path) as f:
            content = f.read()
            dockers = docker_re.findall(content)
            assert len(dockers) <= 1
            return dockers[0].strip('"') if dockers else None
    except FileNotFoundError: # file was removed between commits
        pass


def merge_dockers(dockers:List[Tuple[Union[str, pathlib.PurePath]]]) ->Dict[str, FrozenSet[str]]:
    docker_dict = defaultdict(set)
    for docker_name, wdl_path in dockers:
        docker_dict[docker_name].add(wdl_path)
    return {k:frozenset(v) for k, v in docker_dict.items()}


def deal_with_complex_docker_name(docker_name:str) -> Tuple[Union[Callable, str, Tuple[str]]]:
    if docker_name.startswith('if defined'):
        return deal_with_if_defined(docker_name)
    else:
        return deal_with_partial_image_name(docker_name)


def deal_with_if_defined(docker_name:str, image_part_re:Pattern=IMAGE_PART_RE) -> Tuple[Union[Callable, str]]:
    image_parts = image_part_re.findall(docker_name)
    assert len(image_parts) == 0
    image_part = image_parts[0]
    remove_images_starting_with(image_part)
    return remove_images_starting_with, image_part


def deal_with_partial_image_name(docker_name:str, image_part_re:Pattern=IMAGE_START_RE) -> Tuple[Union[Callable, str, Tuple[str]]]:
    found = image_part_re.findall(docker_name)
    assert len(found) == 1
    if ':' in found[0]:
        image, tag = found[0].split(':')
    else:
        image, tag = found[0], None
    print("image, tag:", image, tag)
    if not tag:
        remove_images_starting_with(image)
        return remove_images_starting_with, image
    else:
        remove_images_with_partial_tag(image, tag)
        return remove_images_with_partial_tag, (image, tag)


def remove_images_with_partial_tag(image_name:str, partial_tag:str) -> None:
    images = get_images_with_partial_tag(image_name, partial_tag)
    remove_images(images)


def remove_images_starting_with(docker_name_start:str) -> None:
    images = get_images_starting_with(docker_name_start)
    remove_images(images)


def remove_images(images:List[str]) -> None:
    print("images:", images)
    for image in images:
        p = get_output_and_error_from_command_unsafe("docker image rm {}".format(image))
        print('remove_images', p.err)
        assert not p.err


def get_images_with_partial_tag(image_name:str, partial_tag:str) -> List[str]:
    p = get_output_and_error_from_command_unsafe("docker images | grep '^{}' | cut -f1,2 | grep {}".format(image_name, partial_tag))
    print('get_images_with_partial_tag', p.err)
    assert not p.err
    return process_docker_images_output(p.out)


def get_images_starting_with(docker_name_part:str) -> List[str]:
    p = get_output_and_error_from_command_unsafe("docker images | grep '^{}'".format(docker_name_part))
    print('get_images_starting_with', p.err)
    assert not p.err
    return process_docker_images_output(p.out)


def process_docker_images_output(output:str) -> List[str]:
    out = []
    for line in output.split('\n'):
        if line:
            out.append(':'.join(line.split()[:2]))
    return out
#!/usr/bin/python3

import os
import sys
import pathlib
from typing import Any
from typing import Set
from typing import Dict
from typing import List
from typing import Tuple
from typing import Union
from typing import Optional
from typing import Iterable
from typing import FrozenSet

PROJECT_DIR = os.path.abspath(__file__).rsplit(os.path.sep, 3)[0]
sys.path.insert(0, PROJECT_DIR)

from scripts.lib.base_objects import Observer
from scripts.lib.base_objects import Observable
from scripts.lib.Docker import Docker
from scripts.lib.Docker import DockerData
from scripts.lib.Docker import merge_dockers
from scripts.lib.Docker import get_docker_from_wdl_file
from scripts.lib.Docker import DockerImageNotFoundException
from scripts.lib.my_sh_utils import get_output_and_error_from_command_unsafe
from scripts.lib.my_sh_utils import ProcessData

__version__ = '0.0.1'

test_container = {}


class CantFindTestForWDL(RuntimeError):
    pass


# class TestDoesNotExist(RuntimeError):
#
#     def __init__(self, file_location:pathlib.PurePath, test_location:pathlib.PurePath):
#         super().__init__(self, "Couldn't find test for file {}. Tried {}".format(file_location, test_location))
#         self.file_location = file_location

class WDL(Observable, Observer):
    def __init__(self, name: str, test_paths: List[str], docker: Union[Docker, DockerData],
                 observers: List[Observer] = None):
        super().__init__(string_id=name, observers=observers)
        self.test_paths = frozenset(test_paths)
        self.docker = docker
        self.fired = False

    def __eq__(self, other):
        if not isinstance(other, WDL):
            return False
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(tuple(sorted(self.__dict__.items())))

    def fire(self, object_that_fired_action: Observable):
        if not self.fired:
            print('firing', self.string_identifier)
            self.observed_objects = remove_from_frozenset(self.observed_objects, object_that_fired_action)
            if not self.observed_objects:
                self.run_tests_and_notify()
                self.fired = True

    def run_tests_and_notify(self):
        if self.untestable:
            self.set_tests_to_untestable_state('untested due to lacking docker image')
        elif not self.test_paths:
            self.set_tests_to_untestable_state('untested due to lack of tests')
        else:
            for test in self.test_paths:
                test_res = get_output_and_error_from_command_unsafe('bash {}'.format(test))
                test_container[test] = test_res
                if test_res.err or test_res.code:
                    self.test_paths = frozenset()
                    return
        self.test_paths = frozenset()
        self.notify()

    def set_tests_to_untestable_state(self, reason: str):
        test_container['Test for {}'.format(self.string_identifier)] = ProcessData(out='',
                                                                                   err=reason,
                                                                                   code=1)

    def set_untestable(self):
        super().set_untestable()
        self.set_tests_to_untestable_state('untested due to lacking docker image')
        for observer in self.observers:
            observer.set_untestable()


def remove_from_frozenset(my_frozenset: FrozenSet, my_object: Any) -> FrozenSet:
    l = list(my_frozenset)
    l.remove(my_object)
    return frozenset(l)


def find_tests_for_wdl(wdl_path: str) -> List[str]:
    tests = [os.path.abspath(x) for x in
             pathlib.Path(wdl_path.rsplit(os.path.sep, 1)[0].replace('/main/', '/test/')).rglob('t*sh')]
    if tests:
        return tests
    raise CantFindTestForWDL(wdl_path)


def create_wdl_object(docker: Union[Docker, DockerData], wdl_path: str, cloned_project_directory: str,
                      already_produced_objects: Dict[str, WDL] = None):
    if already_produced_objects is None:
        already_produced_objects = {}
    if wdl_path not in already_produced_objects:
        try:
            tests = find_tests_for_wdl(wdl_path)
        except CantFindTestForWDL:
            tests = []  # todo remove - its easier to set its state 'fired' and set test result to lacking tests
        wdl_dir = os.path.join(cloned_project_directory, 'src', 'main', 'wdl')
        if '/wdl/tasks' in wdl_path:
            files_that_may_import_this_wdl = [f for f in
                                              pathlib.Path(os.path.join(wdl_dir, 'modules')).rglob('latest/*wdl') if
                                              is_imported_in_the_file(wdl_path, f)]
        elif '/wdl/modules' in wdl_path:
            files_that_may_import_this_wdl = [] # testing pipelines temporarily turned off [f for f in
                                              # pathlib.Path(os.path.join(wdl_dir, 'pipelines')).rglob('latest/*wdl') if
                                              # is_imported_in_the_file(wdl_path, f)]
        elif '/wdl/pipelines' in wdl_path:
            files_that_may_import_this_wdl = []
        else:
            raise RuntimeError()
        observers = {str(path): create_wdl_object(None, str(path), cloned_project_directory, already_produced_objects)
                     for path in
                     files_that_may_import_this_wdl}  # zakładam, że nie trzeba dockera dla pipeline'ów i modułów
        already_produced_objects[wdl_path] = WDL(wdl_path, tests, docker, observers.values())
        if observers:
            already_produced_objects.update(observers)
    return already_produced_objects[wdl_path]


def is_imported_in_the_file(potentially_imported_path: str, path_to_be_checked: str) -> bool:
    index = potentially_imported_path.find('/wdl/')
    string_to_be_searched_for = potentially_imported_path[index:]
    with open(path_to_be_checked) as f:
        for line in f:
            stripped = line.strip()
            if not stripped.startswith('#') and stripped.find(string_to_be_searched_for) >= 0:
                return True
    return False


def create_objects_for_wdl_files(paths: Iterable[pathlib.PurePath], cloned_project_directory: str) -> Tuple[
    Union[Set[WDL], Set[Docker]]]:
    wdl_objects = {}  # type:Dict[str, WDL]
    docker_names = []  # type:List[Tuple[Optional[str]]]
    wdl_path_docker_mapping = {}  # type:Dict[str, str]
    for path in paths:
        path_str = str(path)
        if '/tasks/' in path_str:
            docker_path = get_docker_from_wdl_file(path)
            wdl_path_docker_mapping[path_str] = docker_path
            docker_names.append((docker_path, path_str))
        elif ('/modules/' in path_str) or ('/pipelines/' in path_str):
            # wdl_path_docker_mapping.update({p:None for p in get_imported_wdls_from_wdl_file(path)}) #co to kurwa jest?
            wdl_path_docker_mapping[path_str] = None
        else:
            raise RuntimeError('Unexpected path: {}'.format(path))
    docker_name_wdl_paths_mapping = merge_dockers(docker_names)
    # osobna funkcja? todo
    for wdl_path, docker_name in wdl_path_docker_mapping.items():
        if docker_name:
            docker_data = DockerData(docker_name, docker_name_wdl_paths_mapping[docker_name])
        else:
            docker_data = None
        wdl_objects[wdl_path] = create_wdl_object(docker_data, wdl_path, cloned_project_directory, wdl_objects)
    docker_objects = create_dockers_inside_wdls(wdl_objects)
    set_observed_objects(docker_objects.values())
    set_observed_objects(wdl_objects.values())
    return wdl_objects, docker_objects


def create_dockers_inside_wdls(wdls: Dict[str, WDL]) -> Dict[str, Optional[Docker]]:
    dockers = {}
    for wdl_path, wdl in wdls.items():
        if wdl.docker is None:
            continue
        try:
            wdl.docker = dockers[wdl.docker.name]
        except KeyError:
            docker = Docker(wdl.docker.name, frozenset(wdls[path] for path in wdl.docker.wdls))
            wdl.docker = docker
            dockers[wdl.docker.string_identifier] = docker
    return dockers


def set_observed_objects(observable_objects: Iterable[Observable]) -> None:
    for observable_object in observable_objects:
        for observer_object in observable_object.observers:
            observer_object.add_observed_object(observable_object)


def wdl_sorting(s: str) -> int:
    if '/tasks/' in s:
        return 0
    elif '/modules/' in s:
        return 1
    elif '/pipelines/' in s:
        return 2
    else:
        raise RuntimeError(s)


def get_new_dockers_and_run_tests(wdl_objects: Iterable[WDL], docker_objects: Iterable[Docker]):
    sorted_docker_objects = sorted(docker_objects, key=lambda x: len(x.observers))
    for docker_object in sorted_docker_objects:
        try:
            docker_object.pull_current_image_and_notify_task()
        except DockerImageNotFoundException as e:
            docker_object.set_observers_untestable()

    untested_wdl_objects = sorted([x for x in wdl_objects if not x.fired and not x.untestable],
                                  key=lambda x: wdl_sorting(x.string_identifier))

    for wdl_object in untested_wdl_objects:
        print(wdl_object.string_identifier, id(wdl_object),
              id(list(wdl_object.observed_objects)[0]) if wdl_object.observed_objects else None)
        wdl_object.run_tests_and_notify()

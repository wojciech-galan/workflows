#!/usr/bin/python3

import re
import subprocess
from collections import namedtuple
from typing import List
from typing import Pattern

__version__ = '0.0.1'

COLOUR_RE = re.compile("\u001b.+?m")

ProcessData = namedtuple('ProcessData', 'out err code')


def get_output_from_command_safe(command: List[str]) -> str:
    p = subprocess.run(command, stdout=subprocess.PIPE, shell=False)
    return p.stdout.decode().strip()


def get_output_from_command_unsafe(command: str) -> str:
    """
    This function will be executed by gitlabCI, so doesn't have tobe safe.
    :param command:
    :return:
    """
    p = subprocess.run(command, stdout=subprocess.PIPE, shell='/bin/bash')
    return p.stdout.decode().strip()


def get_output_and_error_from_command_unsafe(command: str) -> ProcessData:
    """
    This function will be executed by gitlabCI, so doesn't have tobe safe.
    :param command:
    :return:
    """
    p = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell='/bin/bash')
    return ProcessData(
        out=strip_colour_codes(p.stdout.decode().strip()),
        err=p.stderr.decode().strip(),
        code=p.returncode
    )


def strip_colour_codes(a_string: str, colorur_re:Pattern=COLOUR_RE) -> str:
    return colorur_re.sub("", a_string)
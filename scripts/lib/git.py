#!/usr/bin/python3
import re
import os
import sys
from typing import List
from typing import Set

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 3)[0])

from scripts.lib.my_sh_utils import get_output_from_command_safe

__version__ = '0.0.1'


def clone_repo(repo_address: str = 'https://gitlab.com/intelliseq/workflows.git'):
    get_output_from_command_safe(['git', 'clone', repo_address])


def checkout_branch_or_commit(branch_or_commit: str):
    get_output_from_command_safe(['git', 'checkout', branch_or_commit])


def get_log() -> str:
    output = get_output_from_command_safe(['git', 'log'])
    return output


def get_email_for_given_commit_from_log(commit: str, log: str) -> str:
    pattern = re.compile('^commit {}[ \n].*?^Author: .*? <(.*?)>$'.format(commit), flags=re.DOTALL | re.MULTILINE)
    return pattern.search(log).group(1)


def get_files_changed_between_two_commits(old_commit:str, new_commit:str) -> List[str]:
    return get_output_from_command_safe(['git', 'diff', old_commit, new_commit, '--name-only', '--diff-filter=d']).split('\n')


def get_commits_from_log(log:str) -> Set[str]:
    commit_re = re.compile('^commit ([0-9a-f]+)', re.MULTILINE)
    return set(commit_re.findall(log))


if __name__ == '__main__':
    print(get_files_changed_between_two_commits('2ea59aa5ef99fcd1ca496f69959485b3d106f7ce', '402720b85e0b887f7b797f77465c3bda99f76567'))

#!/usr/bin/python3

import os
import sys
import json
import asyncio
import pathlib
import argparse
import tempfile
import multiprocessing
from typing import List
from typing import Dict
from typing import Iterable

sys.path.insert(0, os.path.abspath(__file__).rsplit(os.path.sep, 2)[0])

from scripts.lib.gmail import authenticate_and_send_email
from scripts.lib.git import clone_repo
from scripts.lib.git import checkout_branch_or_commit
from scripts.lib.git import get_email_for_given_commit_from_log
from scripts.lib.git import get_log
from scripts.lib.git import get_commits_from_log
from scripts.lib.git import get_files_changed_between_two_commits
from scripts.lib.WDL import test_container
from scripts.lib.gitlab import determine_branch
from scripts.lib.gitlab import read_token
from scripts.lib.gitlab import previous_successfully_pushed_commit
from scripts.lib.deduce_and_run_proper_tests import ProcessData
from scripts.lib.deduce_and_run_proper_tests import run_tests_based_on_extension

__version__ = '0.1.0'


# requires running pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
# and gmail credentials in credentials.json
# gmail API documentation: https://developers.google.com/gmail/api/guides


class ImproperUserInputException(RuntimeError):
    pass


class TemporaryWorkingDirectory(object):

    def __init__(self):
        super().__init__()
        self.__curr_dir_backup = os.getcwd()

    def __enter__(self):
        self.__tempdir = tempfile.TemporaryDirectory()
        os.chdir(self.__tempdir.name)
        return self

    def name(self) -> str:
        return self.__tempdir.name

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.chdir(self.__curr_dir_backup)
        self.__tempdir.cleanup()


def validate_input_keys(user_input: Dict[str, str], desired_keys: Iterable[str] = ('commit',)):
    if not set(user_input) == set(desired_keys):
        raise ImproperUserInputException(user_input)


def compose_message(files_changed: List[str], tests_run: Dict[pathlib.PurePath, ProcessData]):
    def format_test_run(path: pathlib.PurePath, process_data: ProcessData):
        return """{}:\nStdout:\n{p.out}\n----\nStderr:\n{p.err}\n----\nExit code:\n{p.code}\n-------------------------""".format(
            path, p=process_data)

    files_changed_part = '\n- '.join(files_changed)
    tests_run_part = '\n\n'.join(format_test_run(path, process_data) for path, process_data in tests_run.items())
    return '''Files changed since last successful push:\n- {}\n\n\nTest results:\n{}'''.format(files_changed_part,
                                                                                               tests_run_part)


def setup_environment(temp_dir: TemporaryWorkingDirectory, branch: str, commit: str) -> str:
    print("performing tests in {}".format(temp_dir.name()))
    clone_repo()
    os.chdir('workflows')
    checkout_branch_or_commit(branch)
    checkout_branch_or_commit(commit)
    git_log = get_log()
    return get_email_for_given_commit_from_log(commit, git_log)


def perform_tests(branch: str, commit: str, gitlab_token: str, cloned_project_directory: str) -> List[str]:
    git_log = get_log()
    previous_successful_commit = previous_successfully_pushed_commit(branch, commit, 10410163, gitlab_token,
                                                                     get_commits_from_log(git_log))
    print("Previous successful commit: {}".format(previous_successful_commit))
    files_changed = get_files_changed_between_two_commits(previous_successful_commit, commit)
    print("Files changed: {}".format(files_changed))
    try:
        run_tests_based_on_extension(files_changed, cloned_project_directory, branch)
    finally:
        return files_changed


def worker(commit: str) -> None:
    print("starting for commit '{}'".format(commit))
    with TemporaryWorkingDirectory() as temp_dir:
        gitlab_token = read_token()
        receivers = set()# {'dzesika.hoinkis@intelliseq.pl'}
        try:
            branch = determine_branch(commit, 10410163, gitlab_token)
        except Exception as e:
            formatted_receivers = ','.join(receivers)
            authenticate_and_send_email(formatted_receivers, message_str='While determining branch', exception=e,
                                        subject='Message for commit {}'.format(commit))
            return
        print("Branch: {}".format(branch))
        try:
            receiver = setup_environment(temp_dir, branch, commit)
        except Exception as e:
            formatted_receivers = ','.join(receivers)
            authenticate_and_send_email(formatted_receivers,
                                        message_str='While determining the one who pushed this commit', exception=e,
                                        subject='Message for commit {} on branch {}'.format(commit, branch))
            return
        print("Receiver: {}".format(receiver))
        receivers.add(receiver)
        try:
            files_changed = perform_tests(branch, commit, gitlab_token, os.path.join(temp_dir.name(), 'workflows'))
            formatted_receivers = ','.join(receivers)
        except Exception as e:
            formatted_receivers = ','.join(receivers)
            authenticate_and_send_email(formatted_receivers, message_str='While performing tests', exception=e,
                                        subject='Message for commit {} on branch {}'.format(commit, branch))
            return
    print("tests run", test_container)
    authenticate_and_send_email(formatted_receivers, compose_message(files_changed, test_container),
                                subject='Message for commit {} on branch {}'.format(commit, branch))

    print("message sent")


async def handle_connection(reader, writer):
    data = await reader.read(128)
    try:
        addr = writer.get_extra_info('peername')
        print("Received message from %r" % (addr,))
        message = json.loads(data.decode())
        print('Message content: {}'.format(message))
        validate_input_keys(message)
        try:
            p = multiprocessing.Process(target=worker, args=(message['commit'],))
            p.start()
            writer.write(b"Started testing on server")
        except:
            # Todo log że coś nie tak
            writer.write(b"something went wrong")
        finally:
            await writer.drain()
    except (json.decoder.JSONDecodeError, ImproperUserInputException):
        # probably a bad guy tries to send some payload
        print('message =', data.decode())
        writer.write(b"I know your IP")
    except UnicodeDecodeError:
        # probably a bad guy tries to send some payload
        print('message =', data)
        writer.write(b"I know your IP")
    finally:
        print("Close the client socket")
        writer.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=('Performs tests on test server (run with -i and -p options).',
    'For debugging use -c option (server turnef off, only one commit checked)'))
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-i', '--ip', type=str, help='This server will be bound to this IP')
    parser.add_argument('-p', '--port', type=int, help='Port number')
    parser.add_argument('-c', '--commit', help='Commit hash')
    arguments = parser.parse_args()

    if arguments.commit:
        worker(arguments.commit)
    else:
        assert arguments.ip and arguments.port
        loop = asyncio.get_event_loop()
        coroutine = asyncio.start_server(handle_connection, arguments.ip, arguments.port, loop=loop)
        server = loop.run_until_complete(coroutine)

        # Serve requests until Ctrl+C is pressed
        print('Serving on {}'.format(server.sockets[0].getsockname()))
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass

        # Close the server
        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()

#!/usr/bin/env python3

from create_meta import *
from os import path
from numpy.testing import assert_equal

base_dir = path.join(path.dirname(path.realpath(__file__)), "../../src/main/wdl")

def test_make_fields_1():
    wdl_json = read_wdl(path.join(base_dir, "modules/pgx-genotyping-report/pgx-genotyping-report.wdl"))
    jsons = make_fields(wdl_json)
    assert len(jsons) > 0
    assert 'input_bam' in jsons
    assert 'input_gvcf_gz' in jsons
    assert 'output_bco' in jsons


def test_make_fields_2():
    jsons = make_fields(read_wdl(path.join(base_dir, "pipelines/pgx/pgx.wdl")))
    assert len(jsons) > 0
    assert 'input_bam' in jsons
    assert 'input_gvcf_gz' in jsons
    assert 'output_json' in jsons


def test_make_fields_3():
    jsons = make_fields(read_wdl(path.join(base_dir, "tasks/pgx-aldy/pgx-aldy.wdl")))
    assert len(jsons) > 0
    assert 'input_bam' in jsons
    assert 'output_out_txt' in jsons


def test_make_fields_4():
    jsons = make_fields(read_wdl(path.join(base_dir, "tasks/resources-kit/resources-kit.wdl")))
    assert len(jsons) > 0
    assert 'output_bait' in jsons

def test_define_type():
    assert_equal(
        define_type(
            {'type': {'innerType': {'name': 'Array', 'subtype': ['File'], 'kind': 'Type'}, 'kind': 'OptionalType'},
             'name': 'other_bams', 'expression': {'values': [], 'kind': 'ArrayLiteral'}, 'kind': 'Declaration'}),
        "Array[File]"
    )
    assert_equal(
        define_type(
            {'type': {'innerType': {'name': 'Array', 'subtype': ['String'], 'kind': 'Type'}, 'kind': 'OptionalType'},
             'name': 'target_genes', 'expression': None, 'kind': 'Declaration'},
        ), 'Array[String]'
    )
    assert_equal(
        define_type({'type': 'String', 'name': 'sample_id', 'expression': 'no_id_provided', 'kind': 'Declaration'}),
        'String'
    )
    assert_equal(
        define_type({'type': 'Boolean', 'name': 'run_vcf_uniq', 'expression': False, 'kind': 'Declaration'}),
        'Boolean'
    )
    assert_equal(
        define_type({'type': 'File', 'name': 'bpm_manifest_file', 'expression': None, 'kind': 'Declaration'}),
        'File'
    )
    assert_equal(
        define_type({'type': 'Int', 'name': 'waiting_time', 'expression': 100000, 'kind': 'Declaration'}),
        'Int'
    )
    assert_equal(
        define_type({'type': 'Float', 'name': 'vaf_filter_threshold', 'expression': 0.05, 'kind': 'Declaration'}),
        'Float'
    )

    # outputs
    assert_equal(
        define_type(
            {'type': 'File', 'name': 'vcf', 'expression': {'lhs': 'gtc_to_vcf', 'rhs': 'vcf', 'kind': 'MemberAccess'},
             'kind': 'WorkflowOutputDeclaration'}),
        'File'
    )
    assert_equal(
        define_type(
            {'type': {'innerType': {'name': 'Array', 'subtype': ['File'], 'kind': 'Type'}, 'kind': 'OptionalType'},
             'name': 'fastqc_zips', 'expression': {'lhs': 'fq_qc', 'rhs': 'fastqc_zip', 'kind': 'MemberAccess'},
             'kind': 'WorkflowOutputDeclaration'}),
        "Array[File]"
    )
    assert_equal(
        define_type({'type': {'name': 'Array', 'subtype': ['File'], 'kind': 'Type'}, 'name': 'report_pdf',
                     'expression': {'lhs': 'report_mobigen', 'rhs': 'mobigen_report_pdf', 'kind': 'MemberAccess'},
                     'kind': 'WorkflowOutputDeclaration'}),
        'Array[File]'
    )



#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"

SCRIPT=${PROJECT_DIR}scripts/meta/create_meta.py

for pipeline in pgx germline; do
	echo "Testing pipeline ${pipeline}"
	python3 $SCRIPT -p ${PROJECT_DIR}src/main/wdl/pipelines/$pipeline/$pipeline.wdl
done

for module in pgx-genotyping-report detection-chance; do
	echo "Testing module ${module}"
	python3 $SCRIPT -p ${PROJECT_DIR}src/main/wdl/modules/$module/$module.wdl
done

for task in resources-kit pgx-aldy; do 
	echo "Testing task ${task}"
	python3 $SCRIPT -p ${PROJECT_DIR}src/main/wdl/tasks/$task/$task.wdl -d -t "${PROJECT_DIR}src/main/wdl/tasks/meta-test/" 
done


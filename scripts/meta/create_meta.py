#!/usr/bin/env python3

import argparse
import os
import os.path
import json
import re

__version__ = '1.2.0'

# wdl parser downloaded from here https://raw.githubusercontent.com/openwdl/wdl/main/versions/draft-2/parsers/python/wdl_parser.py
# wget https://raw.githubusercontent.com/openwdl/wdl/main/versions/draft-2/parsers/python/wdl_parser.py
import wdl_parser

CONFIG = {}

def load_json(path: str) -> dict:
    with open(path) as f:
        return json.load(f)

def load_config(templates_path=None):
    if templates_path is None:
        base_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
        templates_path = os.path.join(base_dir, "../src/main/wdl/tasks/meta-test")

    CONFIG["inputs"] = load_json(f"{templates_path}/inputs-fields.json")["general_fields"]
    CONFIG["outputs"] = load_json(f"{templates_path}/outputs-fields.json")["general_fields"]
    CONFIG["workflows"] = load_json(f"{templates_path}/workflows-fields.json")["general_fields"]

load_config()

def read_wdl(file: str) -> dict:
    with open(file, 'r') as wdl:
        return wdl_to_json(wdl_parser.parse(wdl.read()))

def wdl_to_json(val):
    if type(val) == wdl_parser.ParseTree:
        return wdl_to_json(val.ast())

    if type(val) == wdl_parser.Ast:
        result = {}

        for key in val.attributes:
            result[key] = wdl_to_json(val.attributes[key])
        if "map" in result:
            return result["map"]
        result["kind"] = val.name
        return result

    if type(val) == wdl_parser.AstList:
        result = []
        for e in val:
            result.append(wdl_to_json(e))

        if len(result) > 0 and "key" in result[0] and "value" in result[0]:
            final_result = {}
            for e in result:
                final_result[e["key"]] = wdl_to_json(e["value"])
            return final_result

        return result

    if type(val) == wdl_parser.Terminal:
        return wdl_to_json(val.__dict__['source_string'])

    if isinstance(val, str):
        if len(val) > 0 and val[0] == "{":
            return wdl_to_json(json.loads(val))
        if val == "true":
            return True
        if val == "false":
            return False
        if re.match(r'^-?\d+(?:\.\d+)$', val):
            return float(val)
        if val.isdigit():
            return int(val)
        return val

    if val is None:
        return None

    if isinstance(val, dict):
        result = {}
        for key, value in val.items():
            result[key] = wdl_to_json(value)
        return result

    return val


def obligatory_fields(item: dict) -> dict:
    if isinstance(item, dict):
        result = {"name": item['name'].replace("_", " ").capitalize(),
                  "description": "",
                  "type": define_type(item)}
    else:
        raise Exception(f"Item is not a dictionary, item is {type(item)}")
    return result


def define_extension(input: dict) -> list:
    if not "defined" in input["name"] and "File" in input["type"]:
        if "json" in input["name"]:
            return [".json"]
        if "bai" in input["name"]:
            return [".bai"]
        if "bam" in input["name"]:
            return [".bam"]
        if "bed" in input["name"]:
            return [".bed", ".bed.gz"]
        if "fastq" in input["name"]:
            return [".fq.gz", ".fastq.gz"]
        if "gvcf_tbi" in input["name"]:
            return [".gvcf.gz.tbi", ".g.vcf.gz.tbi"]
        if "gvcf" in input["name"]:
            return [".gvcf.gz", ".g.vcf.gz"]
        if "tbi" in input["name"]:
            return [".vcf.gz.tbi"]
        if "vcf" in input["name"]:
            return [".vcf.gz"]
        if "tsv" in input["name"]:
            return [".tsv"]
        if "txt" in input["name"]:
            return [".txt"]
        if "csv" in input["name"]:
            return [".csv"]
        if "gtc" in input["name"]:
            return [".gtc"]
        if "bpm" in input["name"]:
            return [".bpm"]
        if "egt" in input["name"]:
            return [".egt"]
        if "tar_gz" in input["name"]:
            return [".tar.gz"]

        raise Exception("Cannot generate extension for File: " + str(
            input) + ". Please add additional line in function define extension. ")

    return None


def remove_inputs(inputs: dict) -> list:
    result = []
    for i in inputs:
        if 'type' in i and \
                not 'defined' in i["name"] and \
                not "select_first" in str(i) and \
                not "version" in i["name"] and \
                not "_name" in i["name"] and \
                not "bco_" in i["name"] and \
                not "stdout_" in i["name"] and \
                not "stderr_" in i["name"] and \
                not "docker_image" in i["name"] and \
                not "index" in i["name"] and \
                not "scatters" in i["name"]:
            result.append(i)
    return result


def make_inputs(inputs: dict) -> dict:
    inputs = remove_inputs(inputs)
    result = {}
    index = 1

    for item in inputs:
        processed = obligatory_fields(item)

        # default nod needed yet
        # if item['expression'] != None:
        #    processed["default"] = item['expression']

        processed["index"] = index
        extension = define_extension(item)
        if extension != None:
            processed["extension"] = extension
        use_fields_json(processed, "inputs")
        result[f"input_{item['name']}"] = processed
        index = index + 1
    return result


def make_outputs(outputs: dict) -> dict:
    result = {}
    for output in outputs:
        processed_output = obligatory_fields(output)
        use_fields_json(processed_output, "outputs")

        result[f"output_{output['name']}"] = processed_output
    return result


def create_explanation(field: dict) -> str:
    example = str(field["field_example"])
    data_type = type(field["field_example"]).__name__
    if field["obligatory"]:
        obligatory = "OBLIGATORY field"
    else:
        obligatory = "OPTIONAL field. Remove it if it is not used"
    explanation = f"{obligatory}. {field['field_explanation']}. Example: '{example}'. Type: {data_type}."
    return explanation


def general_fields() -> dict:
    gen_fields = CONFIG["workflows"]
    result = {}
    for item in gen_fields:
        result[item["field_name"]] = create_explanation(item)
    return result


def merge_jsons(outputs: dict, inputs: dict, gen_fields: dict) -> dict:
    gen_fields.update(inputs)
    gen_fields.update(outputs)
    return gen_fields

def define_type(inputs):
    # input String, input File, input Int, input Float, input Boolean
    if isinstance(inputs["type"], str) or isinstance(inputs["type"], int) or isinstance(inputs["type"], float):
        return inputs["type"]
    if 'innerType' in inputs["type"]:
        # input Array[File]
        # input Arrat[String]
        # output Array[File]
        # outpuy Array[String]
        if 'subtype' in inputs["type"]['innerType'] and 'name' in inputs["type"]['innerType']:
            typ = inputs["type"]['innerType']['name']
            subtype = inputs["type"]['innerType']['subtype']
            return f"{typ}[{str(subtype[0])}]"
        # output File
        if not 'subtype' in inputs["type"]['innerType'] and not 'name' in inputs["type"]['innerType']:
            return inputs["type"]['innerType']
    # output Array[File]
    if not 'innerType' in inputs["type"] and 'name' in inputs["type"] and 'subtype' in inputs["type"]:
        typ = inputs["type"]['name']
        subtype = inputs["type"]['subtype']
        return f"{typ}[{str(subtype[0])}]"
    raise Exception("Cannot extract type: " + str(inputs))


def extract_inputs_and_outputs_from_workflow(wdl_json):
    inputs = []
    outputs = []

    for item in wdl_json['body'][0]['body']:
        if 'kind' in item.keys():
            if item['kind'] == 'WorkflowOutputs':
                outputs = item['outputs']
            else:
                inputs.append(item)

    return inputs, outputs


def extract_inputs_and_outputs_from_last_task(wdl_json):
    task = wdl_json['body'][-1]

    inputs = []
    for item in task['declarations']:
        inputs.append(item)

    assert task['sections'][-1]['kind'] == 'Outputs'

    outputs = []
    for item in task['sections'][-1]['attributes']:
        outputs.append(item)

    return inputs, outputs


def has_single_workflow(wdl_json):
    return 'body' in wdl_json and len(wdl_json['body']) == 1 and wdl_json['body'][0]['kind'] == 'Workflow'


def has_task_at_end(wdl_json):
    return 'body' in wdl_json and len(wdl_json['body']) > 0 and wdl_json['body'][-1]['kind'] == 'Task'


def make_fields(wdl_json):
    if has_single_workflow(wdl_json):
        inputs, outputs = extract_inputs_and_outputs_from_workflow(wdl_json)
    elif has_task_at_end(wdl_json):
        inputs, outputs = extract_inputs_and_outputs_from_last_task(wdl_json)
    else:
        assert False
    return merge_jsons(make_outputs(outputs), make_inputs(inputs), general_fields())


def load_general_fields(input_or_output):
    return CONFIG[input_or_output]


def use_fields_json(item: dict, input_or_output: str) -> dict:
    fields = load_general_fields(input_or_output)
    for field in fields:
        if item["type"] in field["list_of_types_with_this_field"]:
            if field["field_explanation"] != "auto-complete":
                item[field["field_name"]] = create_explanation(field)
    return item


def description_field_names(name):
    return [d["field_name"] for d in CONFIG[name] if d["used_in_description_mode"]]


def remove_non_description_mode_fields(result):
    workflow_fields = description_field_names("workflows")

    def keep_workflow_key(k):
        return k.startswith("input_") or k.startswith("output_") or k in workflow_fields

    input_fields = description_field_names("inputs")

    def process_input_field(field):
        return {k: v for k, v in field.items() if k in input_fields}

    output_fields = description_field_names("outputs")

    def process_output_field(field):
        return {k: v for k, v in field.items() if k in output_fields}

    result = {k: v for k, v in result.items() if keep_workflow_key(k)}
    result = {k: (process_input_field(v) if k.startswith("input_") else v) for k, v in result.items()}
    result = {k: (process_output_field(v) if k.startswith("output_") else v) for k, v in result.items()}

    return result

def main(args):
    if "templates_path" in args:
        TEMPLATES_PATH = args["templates_path"]
    wdl_json = read_wdl(args['path'])
    result = make_fields(wdl_json)
    if args["description_mode"]:
        result = remove_non_description_mode_fields(result)

    print(json.dumps(result, indent=2))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Script makes json with metadata in the same directory as wdl is - meta.json, based on wdl code. ')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    # INPUTS
    parser.add_argument('-p', '--path', help="Path to wdl.")
    parser.add_argument('-d', '--description_mode', action='store_true',
                        help="Keeps only meta fields that are marked to be kept in description mode ")
    parser.add_argument('-t', '--templates_path', help="Path to directory, containing configuration jsons (workflow, inputs, outputs). Default: src/main/wdl/tasks/meta-test")
    args = vars(parser.parse_args())
    main(args)

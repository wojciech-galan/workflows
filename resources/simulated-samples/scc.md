Wygenerowanie każdego chromosomu 10 razy z bazy danych gnomad

```
ls /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/04-11-2019/*vcf.gz | xargs -P 15 -n 1 -i bash -c 'CHR=$(echo {} | grep -oP 'chr.*?\.'); for i in $(seq 10); do ./gnomadrandom.py -i {} -f ISEQ_GNOMAD_GENOMES_V3_AF_nfe | bgzip > $CHR"."$i".vcf.gz"; done'
```

Dodanie mutacji właściwych

```
VCF_DIR=/tmp/marpiech/scc/vcf
vcf-concat $VCF_DIR/mutations/atad3a-mut1.vcf.gz $VCF_DIR/random/chr1.1.vcf.gz | vcf-sort | bgzip > $VCF_DIR/chrs-with-mutations/chr1.1.mut1.vcf.gz
vcf-concat $VCF_DIR/mutations/atad3a-mut2.vcf.gz $VCF_DIR/random/chr1.3.vcf.gz | vcf-sort | bgzip > $VCF_DIR/chrs-with-mutations/chr1.3.mut2.vcf.gz
vcf-concat $VCF_DIR/mutations/gne-mut1.vcf.gz $VCF_DIR/random/chr9.4.vcf.gz | vcf-sort | bgzip > $VCF_DIR/chrs-with-mutations/chr9.4.mut1.vcf.gz
vcf-concat $VCF_DIR/mutations/gne-mut2.vcf.gz $VCF_DIR/random/chr9.5.vcf.gz | vcf-sort | bgzip > $VCF_DIR/chrs-with-mutations/chr9.5.mut2.vcf.gz
vcf-concat $VCF_DIR/mutations/rab3gap1-mut1.vcf.gz $VCF_DIR/random/chr2.6.vcf.gz | vcf-sort | bgzip > $VCF_DIR/chrs-with-mutations/chr2.6.mut1.vcf.gz
vcf-concat $VCF_DIR/mutations/rab3gap1-mut1.vcf.gz $VCF_DIR/random/chr2.8.vcf.gz | vcf-sort | bgzip > $VCF_DIR/chrs-with-mutations/chr2.8.mut1.vcf.gz
```

Wygenerowanie inputów do simulate-neat

```
ls inputs/*.gz.json | grep mut | xargs -i bash -c 'sleep 10; curl -X POST "http://localhost:8000/api/workflows/v1" -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "workflowUrl=https://gitlab.com/intelliseq/workflows/-/raw/random-muatations/src/main/wdl/tasks/simulate-neat/simulate-neat.wdl" -F "workflowInputs=@{};type=application/json" > {}.id; echo {}'
```

Sprawdzenie statusu generowania na gcloud
```
ls inputs/*.id | xargs -i bash -c 'echo {}; ID=$(cat {} | jq -r ".id"); curl -s -X GET "http://localhost:8000/api/workflows/v1/$ID/status"; echo;'
```

Przekopiowanie wyników symulacji na anakina
```
ls inputs/*.id | head -1 | xargs -i bash -c 'echo {}; ID=$(cat {} | jq -r ".id"); NAME=$(echo {} | sed "s|inputs/||" | sed "s|.vcf.gz.json.id||"); gsutil cp gs://iseq/scc/exec/simulate_neat_workflow/$ID/call-simulate_neat/*.fq.gz gs://iseq/scc/fq/$NAME/'
```

Wylosowanie chromosomów do złożenia w sample
```
MUT='chr1.1/chr1.1.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=1; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample1.fqlist
MUT='chr1.1/chr1.1.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=2; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample1.fqlist
MUT='chr1.3/chr1.3.mut2'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=3; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample2.fqlist
MUT='chr1.3/chr1.3.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=10; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample2.fqlist
MUT='chr1.1/chr1.1.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=1; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample3.fqlist
MUT='chr1.3/chr1.3.mut2'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=3; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample3.fqlist
MUT='chr9.4/chr9.4.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=4; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample4.fqlist
MUT='chr9.5/chr9.5.mut2'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=5; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample4.fqlist
MUT='chr2.6/chr2.6.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=6; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample5.fqlist
MUT='chr2.6/chr2.6.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=7; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample5.fqlist
MUT='chr2.8/chr2.8.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=8; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample6.fqlist
MUT='chr2.8/chr2.8.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=9; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample6.fqlist
MUT='chr2.6/chr2.6.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=6; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample7.fqlist
MUT='chr2.8/chr2.8.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=8; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample7.fqlist
MUT='chr2.6/chr2.6.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=6; IDX[1]=7; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample8.fqlist
MUT='chr2.8/chr2.8.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=8; IDX[1]=9; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample8.fqlist
MUT='chr2.6/chr2.6.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=6; IDX[1]=7;  VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' > sample9.fqlist
MUT='chr2.8/chr2.8.mut1'; { seq 22 ; echo X; echo Y-and-the-rest; } | sed 's|^|gs://iseq/scc/fq/chr|' | sed 's|$|.REPLACE/simulated_1.fq.gz|' | awk 'BEGIN{srand(0);} { IDX[0]=8; IDX[1]=9; VAR=IDX[int(rand() * length(IDX))]; gsub(/REPLACE/, VAR, $0) } 1' | sed "s/$MUT/" | sed 's/^/\"/' | sed 's/$/\",/' | sed "p;s/lated_1/lated_2/" | tr '\n' ' ' | sed s'/..$//' >> sample9.fqlist
```

Lista sample
> sample1 - father mut1  
> sample2 - mother mut2  
> sample3 - proband male (Inborn errors of metabolism)  
> sample4 - proband female  
> sample5 - father mut1 y.6 x.7  
> sample6 - mother mut1 x.8 x.9  
> sample7 - proband male  
> sample8 - mut1 male  
> sample9 - ref female  

Utworzenie inputów dla germline'a
```
SAMPLEID=1; FAMILY=3; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=2; FAMILY=3; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=3; FAMILY=3; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=4; FAMILY=4; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=5; FAMILY=7; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=6; FAMILY=7; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=7; FAMILY=7; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=8; FAMILY=7; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json"
SAMPLEID=9; FAMILY=7; printf $SAMPLEID"\n"$FAMILY | xargs -n2 bash -c 'cat sampleinputs/template.json | sed "s/IDTAG/$0/" | sed "s/HPOTAG/$(cat lists/sample$1.hpolist)/" | sed "s/GENESTAG/$(cat lists/sample$1.genelist)/" | sed "s|FQTAG|\"/data/public/simulated/fq/sample$0_1/fq.gz\",\"/data/public/simulated/fq/sample$0_1/fq.gz\"|"' > "sampleinputs/sample"$SAMPLEID"-input.json" 
```

Zmergowanie pojedynczych chromosomów do jednego fastq
```
SAMPLE=sample1; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample2; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample3; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample4; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample5; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample6; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample7; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample8; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
SAMPLE=sample9; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _1 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_1.fq.gz"; zcat $(cat lists/$SAMPLE.fqlist | sed "s|gs://iseq/scc|/data/public/simulated|g" | sed "s/\", \"/ /g" | sed "s/ /\n/g" | grep _2 | tr '\n' ' ' | sed "s/\"//" | sed "s/ $//") | bgzip > fq/$SAMPLE"_2.fq.gz"
```

Poprawka identyfikatorów readów
```
ls /data/public/simulated/fq/ | tail -20 | xargs -i -P 4 -n 1 bash -c 'ID=$(echo {} | sed "s/\./-/g"); echo $ID; CHROM=$(echo {} | cut -d "." -f1); echo $CHROM; zcat /data/public/simulated/fq/{}/simulated_1.fq.gz | sed "s/$CHROM/$ID/" | gzip -4 > /data/public/simulated/fq/{}/simulated_cor_1.fq.gz; zcat /data/public/simulated/fq/{}/simulated_2.fq.gz | sed "s/$CHROM/$ID/" | gzip -4 > /data/public/simulated/fq/{}/simulated_cor_2.fq.gz'
```
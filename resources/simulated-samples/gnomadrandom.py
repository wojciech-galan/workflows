#!/usr/bin/python3
import sys
import pysam
import argparse
import gzip
import random

VERSION="0.0.1"
AUTHOR="gitlab.com/marpiech"

if sys.argv[1] == "--version":
    print("gnomadrandom.py version " + VERSION)
    exit(0)

parser = argparse.ArgumentParser(description='annotates vcf with tsv.gz')
parser.add_argument('--input-vcf', '-i', metavar='input_vcf', type=str, required=True, help='input vcf or vcf.gz file')
parser.add_argument('--af-field', '-f', metavar='af_field', type=str, required=True, help='info field name with af')
args = parser.parse_args()

vcf_in = pysam.VariantFile(args.input_vcf)  # auto-detect input format
vcf_in.header.add_meta('FORMAT', items=[('ID', "GT"), ('Number', "1"), ('Type', "String"), ('Description', "Genotpe")])
vcf_in.header.add_sample("synthetic_sample")
vcf_out = pysam.VariantFile('-', 'w', header=vcf_in.header)

for rec in vcf_in.fetch():
    if len(rec.alts) > 1:
        continue
    try:
        if rec.info[args.af_field][0] < random.random():
            continue
    except:
        continue
    newrec = vcf_out.new_record(contig=rec.contig, start=rec.start, stop=rec.stop, alleles=rec.alleles, filter=rec.filter)
    #rec.samples["synthetic_sample"] = vcf_in.header.samples[0]
    newrec.samples["synthetic_sample"]['GT'] = (1,1)
    vcf_out.write(newrec)

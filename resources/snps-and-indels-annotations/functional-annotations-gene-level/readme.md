# FUNCTIONAL ANNOTATIONS - GENE LEVEL

Resources to annotate VCF files with functional annotations (gene level). Reference genome: **GRCh38**

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level**

---

## Table of Contents

**Functional annotations - gene level**

`Used in task_vcf-anno-func-gene docker`
  * [**All functional annotations, gene level**](#all-functional-annotations-gene-level)
    * 0.3.0 # 0.3.1 the same resources with small fix
    * 0.2.0
    * 0.1.0
  * [ClinVar diseases](#clinvar-diseases)
  * [Human Phenotype Ontology](#human-phenotype-ontology)

[Return to: SNPs and INDELs annotations](./../readme.md)

---

## All functional annotations, gene level

**Last update date:** 15-12-2020

**Update requirements:** monthly

**Current version**: 0.3.1

**History of versions**:
+ 0.3.0 => 0.3.1 the same resources with small fix in clinvar-diseases.dictionary
+ 0.2.0
+ 0.1.0

---

**Description**:  


This folder contains symlinks to the [ClinVar diseases](#clinvar-diseases) and [Human Phenotype Ontology](#human-phenotype-ontology) dictionaries. The folder contains symlinks to files based on the most recently downloaded version of ClinVar and [Human Phenotype Ontology diseases as well as the older ones.

**[Instructions on how to create symlinks to functional annotations dictionaries (gene level)](#instructions-on-how-to-create-symlinks-to-functional-annotations-dictionaries-gene-level)**

[(Archival) Details of previous versions of functional annotations dictionaries (gene level)](#history-of-previous-versions-of-functional -nnotation-ictionaries-gene-level)

**Directory contents:**

```bash
<version>
    ├── clinvar-diseases.dictionary
    ├── gene-symbol-to-diseases.dictionary
    ├── gene-symbol-to-modes-of-inheritance.dictionary
    └── gene-symbol-to-phenotypes.dictionary
```

[Return to the table of contents](#table-of-contents)

---

### **Instructions on how to create symlinks to functional annotations dictionaries (gene level):**


**1.** Set bash variables pointing to the latest ClinVar diseases and HPO  directories, create directory with the newest version.

```bash
VERSION="0.3.0"
CLINVAR_DISEASES_VERSION="15-12-2020"
HPO_VERSION="15-12-2020"
FUNCTIONAL_ANNOTATIONS_GENE_LEVEL="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level"

ALL_GENE_LEVEL_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/all-gene-level"

cd $ALL_GENE_LEVEL_DIR
mkdir $VERSION
cd $VERSION
```
To  see variables for previous version go to: [(Archival) History of versions](#history-of-previous-versions-of-functional annotation-ictionaries-gene-level)

**2.** Create symlinks to dictionaries:


```bash
ln -s $FUNCTIONAL_ANNOTATIONS_GENE_LEVEL/clinvar-diseases/$CLINVAR_DISEASES_VERSION/clinvar-diseases.dictionary
ln -s $FUNCTIONAL_ANNOTATIONS_GENE_LEVEL/human-phenotype-ontology/$HPO_VERSION/gene-symbol-to-diseases.dictionary
ln -s $FUNCTIONAL_ANNOTATIONS_GENE_LEVEL/human-phenotype-ontology/$HPO_VERSION/gene-symbol-to-phenotypes.dictionary
ln -s $FUNCTIONAL_ANNOTATIONS_GENE_LEVEL/human-phenotype-ontology/$HPO_VERSION/gene-symbol-to-modes-of-inheritance.dictionary
```

[Return to the table of contents](#table-of-contents)


 ### History of previous versions of functional annotations dictionaries (gene level):

**version 0.2.0**

Last update: 12-02-2019

```bash
VERSION="0.2.0"
CLINVAR_DISEASES_VERSION="06-02-2019"
HPO_VERSION="12-02-2019"
```

**version 0.1.0**

Last update: 01-10-2019

```bash
VERSION="0.1.0"
CLINVAR_DISEASES_VERSION="01-10-2019"
HPO_VERSION="01-10-2019"
```
---

## ClinVar diseases

**Last update date:** 15-12-2020

**Update requirements:** monthly

**History of updates:**
 - 15-12-2020  
 - 06-02-2020
 - 01-10-2019

---
**Description:**

This folder contains a tab-delimited dictionary of genes and diseases associated with them, with VCF-style header. Dictionary is based on ClinVar database downloaded from [ClinVar FTP site](ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/). The new release of ClinVar database is released approximately monthly. The folder contains files based on the most recently downloaded version of ClinVar as well as the older ones.

**[Instructions on how to create ClinVar diseases annotation dictionary](#instructions-on-how-to-create-clinvar-diseases-annotation-dictionary)**


**Directory contents:**

```bash
<last-update-date>
    ├── clinvar-diseases.dictionary <- dictionary
    └── gene_condition_source_id <- database
```

[Return to the table of contents](#table-of-contents)

---

**List of ClinVar diseases INFO fields:**

**Source:** ClinVar

**Version:** 15-12-2020

| INFO                  | Number | Type   | Description                                                                                         |
|-----------------------|--------|--------|-----------------------------------------------------------------------------------------------------|
| ISEQ_CLINVAR_DISEASES | A      | String | Names of diseases associated with genes that overlap the variant - ClinVar                          |


[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create ClinVar diseases annotation dictionary


**1.**  Prepare the workspace.

Navigate to ClinVar diseases directory (anakin):
```bash
CLINVAR_DISEASES_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/clinvar-diseases"
cd $CLINVAR_DISEASES_DIR
```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="15-12-2020"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.**  Download newest  **gene_condition_source_id** from [NCBI FTP site](ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/):

```bash
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/gene_condition_source_id
```

**3.**  Replace troublesome ASCII characters in **gene_condition_source_id** and concatenate columns - "AssociatedGenes" and "RelatedGenes".

The downloaded file contains troublesome ASCII characters that cannot be handled properly by some software. Those characters are best to be simply replaced. To check which troublesome characters are present the following command can be used:

```bash
cat gene_condition_source_id | grep --color='auto' -P -o "[\x80-\xFF]" | sort | uniq
```
For the latest version the command returned the following output:

```bash
ã
ç
è
é
ê
ë
í
ó
ô
ö
ä
ü
```

These characters can be replaced with the use of sed (in this step columns "AssociatedGenes" and "RelatedGenes" are also concatenated):

```bash
cat gene_condition_source_id | sed 's/ã/a/g' | sed 's/ç/c/g' | sed 's/è/e/g' | sed 's/é/e/g' | sed 's/ê/e/g' | sed 's/ë/e/g' | sed 's/í/i/g' | sed 's/ó/o/g' | sed 's/ô/o/g' | sed 's/ö/o/g' | sed 's/ä/a/g' | sed 's/ü/u/g' | awk -F "\t" '{print $1"\t"$2$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9}'  | sed "s/AssociatedGenesRelatedGenes/AssociatedGenes\/RelatedGenes/g" > gene_condition_source_id-no-troublesome-characters
```

**4.**  Create **clinvar-diseases-dictionary.dictionary** file using **gene_condition_source_id**:

**Required:** [**create-a-dictionary-from-flat-file-with-two-columns.py 0.0.1**](../../../src/main/scripts/tools/readme.md#create-a-dictionary-from-flat-file-with-two-columns)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-a-dictionary-from-flat-file-with-two-columns.py@0.0.1/src/main/scripts/tools/create-a-dictionary-from-flat-file-with-two-columns.py
```

```bash
DATE=$UPDATE_DATE
printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"ClinVar\",Version=\"$DATE\">\n" > clinvar-diseases.dictionary
printf "##INFO=<ID=ISEQ_CLINVAR_DISEASES,Number=.,Type=String,Description=\"Names of diseases associated in ClinVar with given genes\",Source=\"ClinVar\",Version=\"$DATE\">\n" >> clinvar-diseases.dictionary
cat gene_condition_source_id-no-troublesome-characters | python3 create-a-dictionary-from-flat-file-with-two-columns.py 1 3 -c >> clinvar-diseases.dictionary

## fix in version resources 3.0.1 and in the clinvar-diseases.dictionary file in the 15-12-2020 directory (March 2021)
## fixed version from docker version: task_vcf-anno-func-gene:1.1.1
sed -i 's/;_/,/' clinvar-diseases.dictionary
```

**5.** Cleanup:

```bash
rm *py *gene_condition_source_id-no-troublesome-characters
```

[Return to the table of contents](#table-of-contents)


## Human Phenotype Ontology

**Last update date:** 15-12-2020

**Update requirements:** monthly

**History of updates:**
 - 15-12-2020
 - 12-02-2020
 - 01-10-2019

---
**Description:**

This folder contains a tab-delimited dictionaries of genes and diseases/phenotypes/modes of inheritance associated with them, with VCF-style header. Dictionaries are based on [HPO (Human Phenotype Ontology)](https://hpo.jax.org/app/. The new release of HPO database is released approximately monthly. The folder contains files based on the most recently downloaded version of HPO as well as the older ones.

**[Instructions on how to create HPO dictionaries](#instructions-on-how-to-create-hpo-dictionaries)**


**Directory contents:**

```bash
<last update date>
    ├── ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt <- database
    ├── disease-id-to-diseases.dictionary <- intermediate dictionary (used to create other dictionaries)
    ├── gene-symbol-to-diseases-ids.dictionary <- intermediate dictionary (used to create other dictionaries)
    ├── gene-symbol-to-diseases.dictionary <- dictionary
    ├── gene-symbol-to-modes-of-inheritance.dictionary <- dictionary
    ├── gene-symbol-to-phenotypes.dictionary <- dictionary
    ├── genes_to_diseases.txt <- database
    └── phenotype_annotation.tab <- database
```

[Return to the table of contents](#table-of-contents)

---

**List of HPO INFO fields**

**Source:** Human Phenotype Ontology

**Version:** 15-12-2020

| INFO                  | Number | Type   | Description                                                                                         |
|-----------------------|--------|--------|-----------------------------------------------------------------------------------------------------|
| ISEQ_HPO_DISEASES     | A     | String | Names of diseases associated with genes that overlap the variant - Human Phenotye Ontology (HPO)    |
| ISEQ_HPO_PHENOTYPES   | A      | String | Names of phenotypes associated with genes that overlap the variant - Human Phenotye Ontology (HPO)  |
| ISEQ_HPO_INHERITANCE  | A      | String | Modes of inheritance associated with genes that overlap the variant - Human Phenotye Ontology (HPO) |


[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create HPO dictionaries


**1.** Create folder for  the newest version of the database.

```bash
HPO_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-gene-level/human-phenotype-ontology"
```
```bash
UPDATE_DATE="15-12-2020"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.** Download files **phenotype_annotation.tab**, **genes_to_diseases.txt**, **ALL_SOURCES_ALL_FREQUENCIES_diseases_to_genes_to_phenotypes.txt**:

The files were downloaded from [HPO Hudson/Jenkins page - hpo.annotations/lastStableBuild/](http://compbio.charite.de/jenkins/job/hpo.annotations/lastStableBuild/) and [HPO Hudson/Jenkins page - hpo.annotations.monthly/](http://compbio.charite.de/jenkins/job/hpo.annotations.monthly/)

```bash
wget -c http://compbio.charite.de/jenkins/job/hpo.annotations/lastSuccessfulBuild/artifact/misc/phenotype_annotation.tab
wget -c http://compbio.charite.de/jenkins/job/hpo.annotations.monthly/lastSuccessfulBuild/artifact/annotation/genes_to_diseases.txt
wget -c http://compbio.charite.de/jenkins/job/hpo.annotations.monthly/lastSuccessfulBuild/artifact/annotation/ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt
```

One of downloaded files - **phenotype_annotation.tab** - contains troublesome ASCII characters that cannot be handled properly by some software. Those characters are best to be simply replaced. To check which troublesome characters are present the following command can be used:

```bash
cat phenotype_annotation.tab | grep --color='auto' -P -o "[\x80-\xFF]" | sort | uniq
```
For the latest version the command returned the following output:
```bash
Å
á
ç
è
é
í
ï
ó
ö
ü
```

Characters can be replaced with the use of:
```bash
cat phenotype_annotation.tab | sed 's/Å/A/g' | sed 's/á/a/g'  | sed 's/ç/c/g' | sed 's/è/e/g' | sed 's/é/e/g'  | sed 's/í/i/g' | sed 's/ï/i/g' | sed 's/ó/o/g' | sed 's/ö/o/g' | sed 's/ü/u/g' > tmp.txt
mv tmp.txt phenotype_annotation.tab
```

**3.**  Create **list-of-modes-of-inheritance.txt** file:

This is a manually created file containing tab delimited names and HPO IDs of subclasses of HPO class 'Mode of inheritance' (HP:0000005). It should be updated regualary.

**TO-DO**: This process should be automitized. Is is not automatized now, because browsing flat file **hp.obo** conaining HPO ontology is not straightforward.

current-version: [**list-of-modes-of-inheritance.txt, version 1.0.0**](https://gitlab.com/intelliseq/workflows/-/blob/list-of-modes-of-inheritance.txt@1.0.0/resources/snps-and-indels-annotations/functional-annotations-gene-level/list-of-modes-of-inheritance.txt)


**4.** Create dictionaries:

+ **disease-id-to-diseases.dictionary**
+ **gene-symbol-to-diseases-ids.dictionary**
+ **gene-symbol-to-diseases.dictionary**
+ **gene-symbol-to-phenotypes.dictionary**
+ **gene-symbol-to-modes-of-inheritance.dictionary**

**Required:** [**create-a-dictionary-from-flat-file-with-two-columns.py 0.0.1**](../../../src/main/scripts/tools/readme.md#create-a-dictionary-from-flat-file-with-two-columns)

**Required:** [**translate-values-of-a-dictionary.py 0.0.1**](../../../src/main/scripts/tools/readme.md#translate-values-of-a-dictionary)


**Required:** [**filter-hpo-dictionaries-by-modes-of-inheritance.py 0.0.1**](../../../src/main/scripts/resources-tools/readme.md#filter-hpo-dictionaries-by-modes-of-inheritance)

**Required:** [**list-of-modes-of-inheritance.txt 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/list-of-modes-of-inheritance.txt@1.0.0/resources/snps-and-indels-annotations/functional-annotations-gene-level/list-of-modes-of-inheritance.txt)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-a-dictionary-from-flat-file-with-two-columns.py@0.0.1/src/main/scripts/tools/create-a-dictionary-from-flat-file-with-two-columns.py
wget https://gitlab.com/intelliseq/workflows/-/raw/translate-values-of-a-dictionary.py@0.0.1/src/main/scripts/tools/translate-values-of-a-dictionary.py
wget https://gitlab.com/intelliseq/workflows/-/raw/filter-hpo-dictionaries-by-modes-of-inheritance.py@0.0.1/src/main/scripts/resources-tools/filter-hpo-dictionaries-by-modes-of-inheritance.py
wget https://gitlab.com/intelliseq/workflows/-/raw/list-of-modes-of-inheritance.txt@1.0.0/resources/snps-and-indels-annotations/functional-annotations-gene-level/list-of-modes-of-inheritance.txt
```


```bash
printf "##INFO=<ID=ISEQ_HPO_DISEASES_IDS,Number=.,Type=String,Description=\"ID's of diseases associated with given genes - OMIM, Orphanet, and DECIPHER\",Source=\"HPO\",Version=\"$UPDATE_DATE\">\n" > disease-id-to-diseases.dictionary
printf "##INFO=<ID=ISEQ_HPO_DISEASES,Number=.,Type=String,Description=\"Names of diseases from the HPO-team (mostly referring to OMIM), Orphanet, and DECIPHER\",Source=\"HPO\",Version=\"$UPDATE_DATEN\">\n" >> disease-id-to-diseases.dictionary
cat phenotype_annotation.tab | python3 create-a-dictionary-from-flat-file-with-two-columns.py -k ';' -l ';' -d ';' -e  5 2 | python3 create-a-dictionary-from-flat-file-with-two-columns.py -k ',' -l ';' 0 1 >> disease-id-to-diseases.dictionary

printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"Ensembl\",Version=\"$UPDATE_DATE\">\n" > gene-symbol-to-diseases-ids.dictionary
printf "##INFO=<ID=ISEQ_HPO_DISEASES_IDS,Number=.,Type=String,Description=\"ID's of diseases associated with given genes - OMIM, Orphanet, and DECIPHER\",Source=\"HPO\",Version=\"$UPDATE_DATE\">\n" >> gene-symbol-to-diseases-ids.dictionary
cat genes_to_diseases.txt | python3 create-a-dictionary-from-flat-file-with-two-columns.py -c 1 2 >> gene-symbol-to-diseases-ids.dictionary

cat gene-symbol-to-diseases-ids.dictionary | python3 translate-values-of-a-dictionary.py disease-id-to-diseases.dictionary > gene-symbol-to-diseases.dictionary

printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"Ensembl\",Version=\"$UPDATE_DATE\">\n" > gene-symbol-to-phenotypes.dictionary
printf "##INFO=<ID=ISEQ_HPO_PHENOTYPES,Number=.,Type=String,Description=\"Phenotypes associated with given genes\",Source=\"HPO\",Version=\"$UPDATE_DATE\">\n" >> gene-symbol-to-phenotypes.dictionary
cat ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt | python3 create-a-dictionary-from-flat-file-with-two-columns.py -c 1 2 | python3  filter-hpo-dictionaries-by-modes-of-inheritance.py list-of-modes-of-inheritance.txt >> gene-symbol-to-phenotypes.dictionary

printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"Ensembl\",Version=\"$UPDATE_DATE\">\n" > gene-symbol-to-modes-of-inheritance.dictionary
printf "##INFO=<ID=ISEQ_HPO_INHERITANCE,Number=.,Type=String,Description=\"Modes of inheritance associated with given genes\",Source=\"HPO\",Version=\"$UPDATE_DATE\">\n" >> gene-symbol-to-modes-of-inheritance.dictionary
cat ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt | python3 create-a-dictionary-from-flat-file-with-two-columns.py -c 1 2 | python3 filter-hpo-dictionaries-by-modes-of-inheritance.py -o list-of-modes-of-inheritance.txt >> gene-symbol-to-modes-of-inheritance.dictionary
```

**5** Cleanup:

```bash
rm *.py list-of-modes-of-inheritance.txt
```
[Return to the table of contents](#table-of-contents)

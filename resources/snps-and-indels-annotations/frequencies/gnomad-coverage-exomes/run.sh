#!/bin/bash 

set -e

target_dir=/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-coverage/2.0.0/gnomad-coverage-exomes/
mkdir -p $target_dir
cd $target_dir

for i in {1..22} X Y; do
#for i in Y; do
  cat test.bed | grep -w ^chr${i} > chr${i}.coverage.bed
  echo "chr${i}"
  bedtools intersect -a chr${i}.coverage.bed -b /data/public/intelliseqngs/workflows/resources/intervals/agilent/v6-r2-v7-combined-padded/v6-r2-v7-combined-padded.bed > chr${i}.coverage.temp.bed
  cat header.txt > chr${i}.coverage.tsv 
  echo "intersect for chr${i} finished"
  cat chr${i}.coverage.temp.bed | awk 'BEGIN{OFS="\t"}{print $1, $3, $4, $5}' >> chr${i}.coverage.tsv
  rm chr${i}.coverage.temp.bed
  bgzip chr${i}.coverage.tsv
done

#change name to be compatible with dockerbuilder for all chromosomes
# chrY-and-the-rest.coverage.tsv.gz should contain MT genome - merge

zcat chrY.coverage.tsv.gz > chrY-and-the-rest.coverage.tsv
zcat /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/technical/gnomad-coverage-exomes/chrY-and-the-rest.coverage.tsv.gz | grep ^chrM | bgzip >> chrY-and-the-rest.coverage.tsv

rm gnomad.genomes.r3.0.1.coverage.summary.tsv.gz test.bed
rm test.bed




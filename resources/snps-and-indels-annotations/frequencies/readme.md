# Frequencies
# Table of contents
- [Description](#description)
- [Resources to annotate VCF files with frequencies. Reference genome: **GRCh38**](#resources-to-annotate-vcf-files-with-frequencies-reference-genome----grch38--)
- [Location](#location)
- [Merged frequencies, exome](#merged-frequencies--exome)
  * [History of previous versions of VCF with frequencies annotations for exom](#history-of-previous-versions-of-vcf-with-frequencies-annotations-for-exom)
- [Merged frequencies, genome](#merged-frequencies--genome)
  * [**Instructions on how to create VCF with merged frequencies annotations for genome:**](#--instructions-on-how-to-create-vcf-with-merged-frequencies-annotations-for-genome---)
  * [History of previous versions of VCF with frequencies annotations for  genome](#history-of-previous-versions-of-vcf-with-frequencies-annotations-for--genome)
- [1000 Genomes](#1000-genomes)
  * [Instructions on how to create 1000 Genomes frequencies annotation VCFs:](#instructions-on-how-to-create-1000-genomes-frequencies-annotation-vcfs-)
- [ESP6500](#esp6500)
  * [Instructions on how to create ESP6500 frequencies annotation VCFs:](#instructions-on-how-to-create-esp6500-frequencies-annotation-vcfs-)
- [ExAC](#exac)
  * [Instructions on how to create ExAC frequencies annotation VCFs:](#instructions-on-how-to-create-exac-frequencies-annotation-vcfs-)
- [gnomAD exomes v2, liftover](#gnomad-exomes-v2--liftover)
  * [Instructions on how to create gnomAD exomes v2 liftover frequencies annotation VCFs:](#instructions-on-how-to-create-gnomad-exomes-v2-liftover-frequencies-annotation-vcfs-)
- [gnomAD genomes v3](#gnomad-genomes-v3)
  * [Instructions on how to create gnomAD genomes v3 frequencies annotation VCFs:](#instructions-on-how-to-create-gnomad-genomes-v3-frequencies-annotation-vcfs-)
- [gnomAD genomes v3, exome calling intervals](#gnomad-genomes-v3--exome-calling-intervals)
  * [Instructions on how to create gnomAD genomes v3 frequencies annotation VCFs, restricted to exome calling intervals](#instructions-on-how-to-create-gnomad-genomes-v3-frequencies-annotation-vcfs--restricted-to-exome-calling-intervals)
- [gnomAD genomes v2, liftover](#gnomad-genomes-v2--liftover)
  * [Instructions on how to create gnomAD genomes v2 liftover frequencies annotation VCFs:](#instructions-on-how-to-create-gnomad-genomes-v2-liftover-frequencies-annotation-vcfs-)
- [gnomAD genomes v2 liftover, exome calling intervals](#gnomad-genomes-v2-liftover--exome-calling-intervals)
  * [Instructions on how to create gnomAD genomes v2 frequencies annotation VCFs, restricted to exome calling intervals](#instructions-on-how-to-create-gnomad-genomes-v2-frequencies-annotation-vcfs--restricted-to-exome-calling-intervals)
- [Mitomap](#mitomap)
  * [Instructions on how to create MITOMAP frequencies annotation VCF:](#instructions-on-how-to-create-mitomap-frequencies-annotation-vcf-)
- [Gnomad-coverage](#gnomad-coverage)
<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


[Return to: SNPs and INDELs annotations](./../readme.md)

## Description
Resources to annotate VCF files with frequencies. Reference genome: **GRCh38**
---
## Location
The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies**

---
## Merged frequencies, exome

**Last update date:** 06-02-2020
**Update requirements:** monthly
**Current version**: 3.0.0
**History of versions**:
+ 3.0.0 - new method (remove non-coding regions from merged frequencies - genome)
+ 0.1.0 == 0.2.0 (symlinked, to have the same version as genome in task vcf-anno-freq)
+ 0.0.1

---
**Description**:  
This folder contains bgzipped VCFs files. Merged frequencies genome were restricted only to exome regions. Regions described in bed file (anakin: ``/data/public/intelliseqngs/workflows/resources/intervals/agilent/v6-r2-v7-combined-padded/v6-r2-v7-combined-padded.bed``)

**[Instructions on how to create VCF with merged frequencies annotations for exom](#instructions-on-how-to-create-vcf-with-merged-frequencies-annotations-for-exom)**

```
wget https://gitlab.com/intelliseq/workflows/-/raw/dev/resources/snps-and-indels-annotations/frequencies/merged-frequencies-exome/run.sh
bash run.sh
```
There is not mitochondrial region in interval_list used to extract fragments from genome. Because of this MT regions from genome are merge to exome (chrY-and-the-rest):

```
gunzip /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/merged-frequencies-exome/3.0.0/chrY-and-the-rest.frequencies.vcf.gz
zcat /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/merged-frequencies-genome/3.0.0/chrY-and-the-rest.frequencies.vcf.gz | grep ^chrM >> /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/merged-frequencies-exome/3.0.0/chrY-and-the-rest.frequencies.vcf
bgzip /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/merged-frequencies-exome/3.0.0/chrY-and-the-rest.frequencies.vc

```

[(Archival) Details of previous versions of VCF with frequencies annotations for exom](#history-of-previous-versions-of-vcf-with-frequencies-annotations-for-exom)

**Directory contents:**

```bash
<last update date>
  ├── chr1.frequencies.vcf.gz
  ├── chr1.frequencies.vcf.gz.tbi
  ├── chr2.frequencies.vcf.gz
  ├── chr2.frequencies.vcf.gz.tbi
(...)
  ├── chrX.frequencies.vcf.gz
  ├── chrX.frequencies.vcf.gz.tbi
  ├── chrY-and-the-rest.frequencies.vcf.gz
  └── chrY-and-the-rest.frequencies.vcf.gz.tbi
  
```

[Return to the table of contents](#table-of-contents)

---

### History of previous versions of VCF with frequencies annotations for exom

**version 0.1.0**

Last update: 01-10-2019

**Notes**: This version has **no gnomAD genomes v3 exom calling interval annotation**. The instuction is the same as in **[Instructions on how to create VCF with merged frequencies annotations for exom](#instructions-on-how-to-create-vcf-with-merged-frequencies-annotations-for-exom)** except steps **1.** and **2.**.

(Archival) https://gitlab.com/intelliseq/workflows/-/blob/readme@1.0.1/resources/snps-and-indels-annotations/frequencies/readme.md#merged-frequencies-exome

[Return to the table of contents](#table-of-contents)



---
## Merged frequencies, genome
**Last update date:** 31-10-2019
**Update requirements:** monthly
**Current version**: 3.0.0 (the same as merged exome to have the same version to build docker images)
**History of versions**:
+ 0.1.0
+ 0.0.1
+ 0.2.0 - Y.gnomadv3 included, previusly missed 
+ 3.0.0 (3.0.0 == 0.2.0 - symlinked)
---
**Description**:  

This folder contains bgzipped VCFs files created by merging the bgzipped frequencies annotation VCFs from [1000 Genomes](#1000-genomes), [ESP6500](#esp6500), [ExAC](#exac), [Mitomap](#mitomap) and gnomAD databases ([gnomAD exomes  v2, liftover](#gnomad-exomes-v2-liftover) and/or [gnomAD genomes v3](#gnomad-genomes-v3)
[gnomAD genomes v3 exome, calling intervals](#gnomad-genomes-v3-exome-calling-intervals) and/or [gnomAD genomes  v2, liftover](#gnomad-genomes-v2-liftover) and/or [gnomAD genomes v2, liftover, exome calling intervals](#gnomad-genomes-v2-liftover-exome-calling-intervals)).

**[Instructions on how to create VCF with merged frequencies annotations for genom](#instructions-on-how-to-create-vcf-with-merged-frequencies-annotations-for-genom)**

[(Archival) Details of previous versions of VCF with frequencies annotations for genom](#history-of-previous-versions-of-vcf-with-frequencies-annotations-for-genom)

**Directory contents:**

```bash
<last update date>
  ├── chr1.frequencies.vcf.gz
  ├── chr1.frequencies.vcf.gz.tbi
  ├── chr2.frequencies.vcf.gz
  ├── chr2.frequencies.vcf.gz.tbi
(...)
  ├── chrX.frequencies.vcf.gz
  ├── chrX.frequencies.vcf.gz.tbi
  ├── chrY-and-the-rest.frequencies.vcf.gz
  └── chrY-and-the-rest.frequencies.vcf.gz.tbi
  
```

[Return to the table of contents](#table-of-contents)

---

### **Instructions on how to create VCF with merged frequencies annotations for genome:**


**1.** Set bash variables:

```bash
VERSION="0.1.0"

KG_VERSION="30-09-2019"
ESP6500_VERSION="01-10-2019"
EXAC_VERSION="02-10-2019"
GNOMAD_EXOMES_V2_LIFTOVER_VERSION="02-10-2019"
GNOMAD_GENOMES_V2_LIFTOVER_VERSION="02-10-2019"
GNOMAD_GENOMES_V3="04-11-2019"
MITOMAP_VERSION="27-09-2019"

```

```bash
FREQUENCIES="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies"
KG_DIR="$FREQUENCIES_DIR/kg/$KG_VERSION"
ESP6500_DIR="$FREQUENCIES_DIR/esp6500/$ESP6500_VERSION"
EXAC_DIR="$FREQUENCIES_DIR/exac/$EXAC_VERSION"
GNOMAD_EXOMES_V2_LIFTOVER_DIR="$FREQUENCIES_DIR/gnomad-exomes-v2-liftover/$GNOMAD_EXOMES_V2_LIFTOVER_VERSION"
GNOMAD_GENOMES_V2_LIFTOVER_DIR="$FREQUENCIES_DIR/gnomad-genomes-v2-liftover/$GNOMAD_GENOMES_V2_LIFTOVER_DIR"
GNOMAD_GENOMES_V3="$FREQUENCIES_DIR/gnomad-genomes-v3/$GNOMAD_GENOMES_V3"
MITOMAP_DIR"$FREQUENCIES_DIR/mitomap/$MITOMAP_VERSION"

MERGED_FREQUENCIES_GENOM_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/merged-frequencies-genome"
cd $MERGED_FREQUENCIES_GENOM_DIR

```

To see variables for previous version go to: [(Archival) History of versions](#history-of-previous-versions-of-vcf-with-frequencies-annotations-for-genom)

There is a Makefile script (/snps-and-indels-annotations/frequencies/merged-frequencies-genome/Makefile) with all code described below, used to merge Y.gnomadv3 with the rest of frequency vcf files. 
To use this code to merge some frequency vcf files change fragment ```#### FRAGMENT TO CHANGE BELOW ###``` to your needs. 
Usage: ```for i in {1..22} X Y-and-the-rest; do make test chr=i &; done```

**2.** Merge chromosome-wise bgzipped VCFs:

**Required**:  [**keep-or-remove-vcf-fields.py 0.0.1**](../../../src/main/scripts/tools/readme.md#keep-or-remove-vcf-fields)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.py

```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
MITOMAP="$MITOMAP_DIR/chrY-and-the-rest.mitomap.vcf.gz"

for i in {1..22}; do
  GNOMAD_EXOMES_V2_LIFTOVER="$GNOMAD_EXOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-exomes.vcf.gz"
  GNOMAD_GENOMES_V2_LIFTOVER="$GNOMAD_GENOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-genomes.vcf.gz"
  GNOMAD_GENOMES_V3="$GNOMAD_GENOMES_V3_DIR/chr$i.gnomad-genomes-v3.vcf.gz"
  EXAC="$EXAC_DIR/chr"$i".exac.vcf.gz"
  KG="$KG_DIR/chr"$i".kg.vcf.gz"
  ESP6500="$ESP6500_DIR/chr"$i".esp6500.vcf.gz"

  echo "vcf-merge -c none $GNOMAD_EXOMES_V2_LIFTOVER $GNOMAD_GENOMES_V2_LIFTOVER $GNOMAD_GENOMES_V3 $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | grep -v \"^chrM\" | bgzip > chr$i.unsorted.unnormalized.vcf.gz &"
done

i="X"
GNOMAD_EXOMES_V2_LIFTOVER="$GNOMAD_EXOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-exomes.vcf.gz"
GNOMAD_GENOMES_V2_LIFTOVER="$GNOMAD_GENOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-genomes.vcf.gz"
GNOMAD_GENOMES_V3="$GNOMAD_GENOMES_V3_DIR/chr$i.gnomad-genomes-v3.vcf.gz"
EXAC="$EXAC_DIR/chr"$i".exac.vcf.gz"
KG="$KG_DIR/chr"$i".kg.vcf.gz"
ESP6500="$ESP6500_DIR/chr"$i".esp6500.vcf.gz"

echo "vcf-merge -c none $GNOMAD_EXOMES_V2_LIFTOVER $GNOMAD_GENOMES_V2_LIFTOVER $GNOMAD_GENOMES_V3 $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | grep -v \"^chrM\" | bgzip > chr$i.unsorted.unnormalized.vcf.gz &"

i="Y-and-the-rest"
GNOMAD_EXOMES_V2_LIFTOVER="$GNOMAD_EXOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-exomes.vcf.gz"
GNOMAD_GENOMES_V2_LIFTOVER="$GNOMAD_GENOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-genomes.vcf.gz"
EXAC="$EXAC_DIR/chr"$i".exac.vcf.gz"
KG="$KG_DIR/chr"$i".kg.vcf.gz"
ESP6500="$ESP6500_DIR/chr"$i".esp6500.vcf.gz"

echo "vcf-merge -c none $GNOMAD_EXOMES_V2_LIFTOVER $GNOMAD_GENOMES_V2_LIFTOVER $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.unsorted.unnormalized.vcf.gz &"

```

**3.**  Download or prepare files necessary for INDELs normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

```

**4.** Sort and normalize **chr*.unsorted.unnormalized.vcf.gz**. Fix "Source" and "Version" VCF INFO header fields (vcf-merge removes quotation marks):

**Required**: bcftools

**Required**: parallel

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "zcat chr$i.unsorted.unnormalized.vcf.gz | awk '{if(/^#/)print;else exit}' | awk '{if (/^##INFO/) { gsub(\"Source=\\\"\", \"Source=\"); gsub(\"\\\",Version=\", \",Version=\"); gsub(\",Version=\\\"\", \",Version=\"); gsub(\"\\\">\", \">\"); gsub(\"Source=\", \"Source=\\\"\"); gsub(\",Version=\", \"\\\",Version=\"); gsub(\",Version=\", \",Version=\\\"\"); gsub(\">\", \"\\\">\"); gsub(\"D \\\">\", \"D >\"); gsub(\"ff \\\">\", \"ff >\");  gsub(\"GQ \\\">\", \"GQ >\"); gsub(\"DP \\\">\", \"DP >\");print} else {print}}' | bgzip > chr$i.sorted.unnormalized.vcf.gz ; zcat chr$i.unsorted.unnormalized.vcf.gz | grep -v \"^#\" | grep -v \"^##contig\" | sort -k1,1V -k2,2n | bgzip >> chr$i.sorted.unnormalized.vcf.gz &"
done; echo "zcat chrX.unsorted.unnormalized.vcf.gz | awk '{if(/^#/)print;else exit}' | awk '{if (/^##INFO/) { gsub(\"Source=\\\"\", \"Source=\"); gsub(\"\\\",Version=\", \",Version=\"); gsub(\",Version=\\\"\", \",Version=\"); gsub(\"\\\">\", \">\"); gsub(\"Source=\", \"Source=\\\"\"); gsub(\",Version=\", \"\\\",Version=\"); gsub(\",Version=\", \",Version=\\\"\"); gsub(\">\", \"\\\">\"); gsub(\"D \\\">\", \"D >\"); gsub(\"ff \\\">\", \"ff >\");  gsub(\"GQ \\\">\", \"GQ >\"); gsub(\"DP \\\">\", \"DP >\");print} else {print}}' | bgzip > chrX.sorted.unnormalized.vcf.gz ; zcat chrX.unsorted.unnormalized.vcf.gz | grep -v \"^#\" | grep -v \"^##contig\" | sort -k1,1V -k2,2n | bgzip >> chrX.sorted.unnormalized.vcf.gz &"; echo "zcat chrY-and-the-rest.unsorted.unnormalized.vcf.gz | awk '{if(/^#/)print;else exit}' | awk '{if (/^##INFO/) { gsub(\"Source=\\\"\", \"Source=\"); gsub(\"\\\",Version=\", \",Version=\"); gsub(\",Version=\\\"\", \",Version=\"); gsub(\"\\\">\", \">\"); gsub(\"Source=\", \"Source=\\\"\"); gsub(\",Version=\", \"\\\",Version=\"); gsub(\",Version=\", \",Version=\\\"\"); gsub(\">\", \"\\\">\"); gsub(\"D \\\">\", \"D >\"); gsub(\"ff \\\">\", \"ff >\");  gsub(\"GQ \\\">\", \"GQ >\"); gsub(\"DP \\\">\", \"DP >\");print} else {print}}' | bgzip > chrY-and-the-rest.sorted.unnormalized.vcf.gz ; zcat chrY-and-the-rest.unsorted.unnormalized.vcf.gz | grep -v \"^#\" | grep -v \"^##contig\" | sort -k1,1V -k2,2n | bgzip >> chrY-and-the-rest.sorted.unnormalized.vcf.gz &"

```

```bash
parallel tabix -f -p vcf chr*.sorted.unnormalized.vcf.gz

```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any chr$i.sorted.unnormalized.vcf.gz  | bgzip > chr$i.frequencies.vcf.gz &"
done; echo "bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any chrX.sorted.unnormalized.vcf.gz  | bgzip > chrX.frequencies.vcf.gz &"; echo "bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any chrY-and-the-rest.sorted.unnormalized.vcf.gz  | bgzip > chrY-and-the-rest.frequencies.vcf.gz &"

```

```bash
parallel tabix -f -p vcf chr*.frequencies.vcf.gz

```

**5.** Validate VCFs with vcf-validator:

**Required:** vcf-validator

The commands generated by script below must be executed (they can be executed in parallel):
```bash
mkdir logs
for i in {1..22}; do
echo "vcf-validator chr$i.frequencies.vcf.gz &>> logs/chr$i.vcf-validator.log &";
done;

i="X"
echo "vcf-validator chr$i.frequencies.vcf.gz &>> logs/chr$i.vcf-validator.log &"

i="Y-and-the-rest"
echo "vcf-validator chr$i.frequencies.vcf.gz &>> logs/chr$i.vcf-validator.log &"

```

```bash
rm *sorted*
rm Ho*

```

[Return to the table of contents](#table-of-contents)

---

### History of previous versions of VCF with frequencies annotations for  genome

**version 0.1.0**

Last update: 06-10-2019

**Notes**: This version has **no gnomAD genomes v3**. The instuction is the same as in **[Instructions on how to create VCF with merged frequencies annotations for genom](#instructions-on-how-to-create-vcf-with-merged-frequencies-annotations-for-genom)** except steps **1.** and **2.**.


**1.** Set bash variables:

```bash
VERSION="0.1.0"

KG_VERSION="30-09-2019"
ESP6500_VERSION="01-10-2019"
EXAC_VERSION="02-10-2019"
GNOMAD_EXOMES_V2_LIFTOVER_VERSION="02-10-2019"
GNOMAD_GENOMES_V2_LIFTOVER_VERSION="02-10-2019"
MITOMAP_VERSION="27-09-2019"

```

```bash
FREQUENCIES="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies"
KG_DIR="$FREQUENCIES_DIR/kg/$KG_VERSION"
ESP6500_DIR="$FREQUENCIES_DIR/esp6500/$ESP6500_VERSION"
EXAC_DIR="$FREQUENCIES_DIR/exac/$EXAC_VERSION"
GNOMAD_EXOMES_V2_LIFTOVER_DIR="$FREQUENCIES_DIR/gnomad-exomes-v2-liftover/$GNOMAD_EXOMES_V2_LIFTOVER_VERSION"
GNOMAD_GENOMES_V2_LIFTOVER_DIR="$FREQUENCIES_DIR/gnomad-genomes-v2-liftover/$GNOMAD_GENOMES_V2_LIFTOVER_DIR"
MITOMAP_DIR"$FREQUENCIES_DIR/mitomap/$MITOMAP_VERSION"

MERGED_FREQUENCIES_GENOM_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/merged-frequencies-genome"
cd $MERGED_FREQUENCIES_GENOM_DIR

```

**2.** Merge chromosome-wise bgzipped VCFs:

**Required**:  [**keep-or-remove-vcf-fields.py 0.0.1**](../../../src/main/scripts/tools/readme.md#keep-or-remove-vcf-fields)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.py
```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
MITOMAP="$MITOMAP_DIR/chrY-and-the-rest.mitomap.vcf.gz"

for i in {1..22}; do
  GNOMAD_EXOMES_V2_LIFTOVER="$GNOMAD_EXOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-exomes.vcf.gz"
  GNOMAD_GENOMES_V2_LIFTOVER="$GNOMAD_GENOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-genomes.vcf.gz"
  EXAC="$EXAC_DIR/chr"$i".exac.vcf.gz"
  KG="$KG_DIR/chr"$i".kg.vcf.gz"
  ESP6500="$ESP6500_DIR/chr"$i".esp6500.vcf.gz"

  echo "vcf-merge -c none $GNOMAD_EXOMES_V2_LIFTOVER $GNOMAD_GENOMES_V2_LIFTOVER $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | grep -v \"^chrM\" | bgzip > chr$i.unsorted.unnormalized.vcf.gz &"
done

i="X"
GNOMAD_EXOMES_V2_LIFTOVER="$GNOMAD_EXOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-exomes.vcf.gz"
GNOMAD_GENOMES_V2_LIFTOVER="$GNOMAD_GENOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-genomes.vcf.gz"
EXAC="$EXAC_DIR/chr"$i".exac.vcf.gz"
KG="$KG_DIR/chr"$i".kg.vcf.gz"
ESP6500="$ESP6500_DIR/chr"$i".esp6500.vcf.gz"

echo "vcf-merge -c none $GNOMAD_EXOMES_V2_LIFTOVER $GNOMAD_GENOMES_V2_LIFTOVER $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | grep -v \"^chrM\" | bgzip > chr$i.unsorted.unnormalized.vcf.gz &"

i="Y-and-the-rest"
GNOMAD_EXOMES_V2_LIFTOVER="$GNOMAD_EXOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-exomes.vcf.gz"
GNOMAD_GENOMES_V2_LIFTOVER="$GNOMAD_GENOMES_V2_LIFTOVER_DIR/chr"$i".gnomad-genomes.vcf.gz"
EXAC="$EXAC_DIR/chr"$i".exac.vcf.gz"
KG="$KG_DIR/chr"$i".kg.vcf.gz"
ESP6500="$ESP6500_DIR/chr"$i".esp6500.vcf.gz"

echo "vcf-merge -c none $GNOMAD_EXOMES_V2_LIFTOVER $GNOMAD_GENOMES_V2_LIFTOVER $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.unsorted.unnormalized.vcf.gz &"

```

[Return to the table of contents](#table-of-contents)

---

## 1000 Genomes

**Last update date:** 30-09-2019

**Update requirements:** every few months

**History of updates:**

+ 30-09-2019

---

**Description:**

This folder contains chromosome-wise bgzipped VCFs files containing frequency annotations based on 1000 Genomes database with SNV and INDEL calls (v2a), aligned directly to GRCh38. The database was downloaded from [1000 Genomes FTP](http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20181203_biallelic_SNV/).  The database is briefly described in the following blogposts:
  - [Variant calls from 1000 Genomes Project data calling against GRCh38](https://www.internationalgenome.org/announcements/Variant-calls-from-1000-Genomes-Project-data-calling-against-GRCh38/)
  - [Variant calls from 1000 Genomes Project data on the GRCh38 reference assembly - updates](https://www.internationalgenome.org/announcements/Variant-calls-from-1000-Genomes-Project-data-on-the-GRCh38-reference-assemlby/)


[**Instructions on how to create 1000 Genomes frequencies annotation VCFs**](#instructions-on-how-to-create-1000-genomes-frequencies-annotation-vcfs)

**Directory contents:**

```bash
<last update date>
  ├── ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz <- original database
  ├── ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz.tbi
  ├── chr1.kg.vcf.gz
  ├── chr1.kg.vcf.gz.tbi
  ├── chr2.kg.vcf.gz
  ├── chr2.kg.vcf.gz.tbi
(...)
  ├── chrX.kg.vcf.gz
  ├── chrX.kg.vcf.gz.tbi
  ├── chrY-and-the-rest.kg.vcf.gz
  └── chrY-and-the-rest.kg.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

---

**List of 1000 Genomes INFO fields:**

**Source:** 1000 Genomes

**Version:** 1000 Genomes - 2019-02-27 biallelic SNVs and INDELs (GRCh38, v2a)

| INFO           | Number | Type  | Description                                                                          |
|----------------|--------|-------|--------------------------------------------------------------------------------------|
| ISEQ_KG_AF     | A      | Float | Alternate allele frequency in samples - 1000 Genomes                                 |
| ISEQ_KG_EAS_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, East Asian population (EAS)    |
| ISEQ_KG_EUR_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, European population (EUR)      |
| ISEQ_KG_ARF_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, African population (AFR)       |
| ISEQ_KG_AMR_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, Mixed American population (AMR |
| ISEQ_KG_SAS_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, South Asian population (SAS)   |

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create 1000 Genomes frequencies annotation VCFs:


**1.**  Prepare the workspace.

```bash
KG_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/kg"
cd $KG_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="30-09-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.** Download 1000 Genomes database with SNV and INDEL calls (v2a), aligned directly to GRCh38, from [1000 Genomes FTP](http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20181203_biallelic_SNV/). The database is briefly described in the following blogposts:
  - [Variant calls from 1000 Genomes Project data calling against GRCh38](https://www.internationalgenome.org/announcements/Variant-calls-from-1000-Genomes-Project-data-calling-against-GRCh38/)
  - [Variant calls from 1000 Genomes Project data on the GRCh38 reference assembly - updates](https://www.internationalgenome.org/announcements/Variant-calls-from-1000-Genomes-Project-data-on-the-GRCh38-reference-assemlby/)

```bash
wget -c http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz
wget -c http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz.tbi

```

**3**.   Download or prepare files necessary for INDELs normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

```

**3**. Left normalize indels. Create chromosome-wise VCF annotation files:

**Required**: [**create-kg-frequencies-annotation-vcf.py, version 0.0.1**](../../../src/main/scripts/resources-tools/readme.md#create-kg-frequencies-annotation-vcf)

**Required**: bcftools

**Required**: Biopython

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-kg-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-kg-frequencies-annotation-vcf.py

```

```bash
zcat ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz | awk '{if (/^#/){ gsub("contig=<ID=", "contig=<ID=chr", $0); print $0} else {print "chr"$0}}' | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | python3 create-kg-frequencies-annotation-vcf.py ./

for i in {1..22}; do tabix -p vcf chr$i.kg.vcf.gz; done
tabix -p vcf chrX.kg.vcf.gz
tabix -p vcf chrY-and-the-rest.kg.vcf.gz

```
**4.** Validate VCF files with vcf-validator:

**Required**: vcf-validator

```bash
for i in {1..22}; do
  vcf-validator chr$i.kg.vcf.gz ;
done
vcf-validator chrX.kg.vcf.gz
vcf-validator chrY-and-the-rest.kg.vcf.gz

```

**5.** Cleanup:

```bash
rm *py

```

[Return to the table of contents](#table-of-contents)

---

## ESP6500

**Last update date:** 01-10-2019

**Update requirements:** almost never

**History of updates:**

+ 01-10-2019

**Description:**

This folder contains chromosome-wise bgzipped VCFs files containing annotations based on ESP6500 database with annotation including positions lifted over to GRCh38 downloaded from [NHLBI Exome Sequencing Project (ESP) Download page](http://evs.gs.washington.edu/EVS/).

[**Instructions on how to create ESP6500 frequencies annotation VCFs**](#instructions-on-how-to-create-esp6500-frequencies-annotation-vcfs)

**Directory contents:**

```bash
<last update date>
  ├── chr1.esp6500.vcf.gz
  ├── chr1.esp6500.vcf.gz.tbi
  ├── chr2.esp6500.vcf.gz
  ├── chr2.esp6500.vcf.gz.tbi
(...)
  ├── chrX.esp6500.vcf.gz
  ├── chrX.esp6500.vcf.gz.tbi
  ├── chrY-and-the-rest.esp6500.vcf.gz
  └── chrY-and-the-rest.esp6500.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

**List of ESP6500 INFO fields:**

**Source:** ESP6500

**Version:** ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.7

| INFO          | Number | Type | Description                                                        |
|--------------------|-------------|-----------|-------------------------------------------------------------------------|
| ISEQ_ESP6500_EA_AF | A           | Integer   | Alternate allele frequency - ESP6500, European American population (EA) |
| ISEQ_ESP6500_AA_AF | A           | Integer   | Alternate allele frequency - ESP6500, African American population (AA)  |
| ISEQ_ESP6500_AN    | 1           | Integer   | Total allele count - ESP6500                                |
| ISEQ_ESP6500_AF    | A           | Integer   | Alternate allele frequency - ESP6500                                    |

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create ESP6500 frequencies annotation VCFs:
---

**1.**  Prepare the workspace.

```bash
ESP6500_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/esp6500"
cd $ESP6500

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="01-10-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**3.** Download ESP6500 database in VCF format, with annotation including positions lifted over to GRCh38,  from  [NHLBI Exome Sequencing Project (ESP) Download page](http://evs.gs.washington.edu/EVS/). Split mulitallelic sites.

```bash
wget -c http://evs.gs.washington.edu/evs_bulk_data/ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels.vcf.tar.gz
tar -zxvf ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels.vcf.tar.gz
rm ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels.vcf.tar.gz

```

**3.**  Download files or prepare necessary for indel normalization and for lifting over the database from GRCh37/hg19 to hg38:

* **Homo_sapiens_assembly19.fa** - reference genome from [GATK ftp](ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.fasta)

* **Homo_sapiens_assembly19.fa.fai** - reference genome index from [GATK ftp](ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.fasta.fai)

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19/Homo_sapiens_assembly19.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19/Homo_sapiens_assembly19.fa.fai

ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz

```


**4.** Create VCF annotation file for hg19 version. Normalize indels, split multiallelic sites. Validate file with vcf-validator:

**Required**:  [**keep-or-remove-vcf-fields.py 0.0.1**](../../../src/main/scripts/tools/readme.md#keep-or-remove-vcf-fields)

**Required**:  [**create-esp6500-frequencies-annotation-vcf.py 0.0.1**](../../../src/main/scripts/resources-tools/readme.md#create-esp6500-frequencies-annotation-vcf)

**Required**: bcftools

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.py
wget https://gitlab.com/intelliseq/workflows/-/raw/create-esp6500-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-esp6500-frequencies-annotation-vcf.py

```

```bash
cat ESP6500SI-V2-SSA137.GRCh38-liftover.chr1.snps_indels.vcf | python3 keep-or-remove-vcf-fields.py -l EA_AC,AA_AC,TAC | python3 create-esp6500-frequencies-annotation-vcf.py | bgzip > esp6500.hg19.vcf.gz

for i in {2..22}; do
  cat ESP6500SI-V2-SSA137.GRCh38-liftover.chr$i.snps_indels.vcf | python3 keep-or-remove-vcf-fields.py -l EA_AC,AA_AC,TAC | python3 create-esp6500-frequencies-annotation-vcf.py | grep -v "^#" | bgzip >> esp6500.hg19.vcf.gz;
done

cat ESP6500SI-V2-SSA137.GRCh38-liftover.chrX.snps_indels.vcf | python3 keep-or-remove-vcf-fields.py -l EA_AC,AA_AC,TAC | python3 create-esp6500-frequencies-annotation-vcf.py| grep -v "^#" | bgzip >> esp6500.hg19.vcf.gz

cat ESP6500SI-V2-SSA137.GRCh38-liftover.chrY.snps_indels.vcf | python3 keep-or-remove-vcf-fields.py -l EA_AC,AA_AC,TAC | python3 create-esp6500-frequencies-annotation-vcf.py | grep -v "^#" | bgzip >> esp6500.hg19.vcf.gz

tabix -p vcf esp6500.hg19.vcf.gz

vcf-validator esp6500.hg19.vcf.gz

bcftools norm --fasta-ref Homo_sapiens_assembly19.fa.gz --multiallelics -any esp6500.hg19.vcf.gz > esp6500.hg19.norm.vcf

rm ESP*

```

**5.**  Perform lifting over using CrossMap.py, version 0.3.7:

**Required**: CrossMap.py

```bash
CrossMap.py vcf hg19ToHg38.over.chain.gz esp6500.hg19.norm.vcf Homo_sapiens_assembly38.fa.gz esp6500.hg38.vcf
```
```bash
@ 2019-09-30 18:04:07: Read chain_file:  hg19ToHg38.over.chain.gz
@ 2019-09-30 18:04:07: Creating index for Homo_sapiens_assembly38.fa.gz
@ 2019-09-30 18:04:34: Updating contig field ...
@ 2019-09-30 18:04:34: Lifting over ...
@ 2019-09-30 18:15:52: Total entries: 1998204
@ 2019-09-30 18:15:52: Failed to map: 1348

```

```
rm esp6500.hg19.norm.vcf

```

**6.** Sort  and index **esp6500.hg38.vcf**. Validate with vcf-validator:

**Required**: vcf-validator (vcftools)

```bash
cat esp6500.hg38.vcf | awk '{if(/^#/) print;else exit}' | sed "s/##contig=<ID=/##contig=<ID=chr/g" | sed "s/chrHLA/HLA/g" | bgzip > tmp.vcf.gz
cat esp6500.hg38.vcf | grep -v "^#" | awk '{print "chr"$0}' |  sort -k1,1V -k2,2n | bgzip >> tmp.vcf.gz
tabix -p vcf tmp.vcf.gz
bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any tmp.vcf.gz | bgzip > esp6500.hg38.vcf.gz
tabix -p vcf esp6500.hg38.vcf.gz

vcf-validator esp6500.hg38.vcf.gz

rm tmp.vcf.gz

```

**6.** Create chromosome-wise VCF annotation files.  Bcftools sometimes changes floats to sciantific notation - reverse this change:

**Required**:  [**split-esp6500-frequencies-annotation-vcf-chromosome-wise.py 0.0.1**](../../../src/main/scripts/resources-tools/readme.md#split-esp6500-frequencies-annotation-vcf-chromosome-wise)

**Required**: Biopython


```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/split-esp6500-frequencies-annotation-vcf-chromosome-wise.py@0.0.1/src/main/scripts/resources-tools/split-esp6500-frequencies-annotation-vcf-chromosome-wise.py

```

```bash
zcat esp6500.hg38.vcf.gz | sed "s/ISEQ_ESP6500_AF=8e-05/ISEQ_ESP6500_AF=0.00008/g" | sed "s/ISEQ_ESP6500_AF=9e-05/ISEQ_ESP6500_AF=0.00009/g" | python3 split-esp6500-frequencies-annotation-vcf-chromosome-wise.py .

for i in {1..22}; do tabix -p vcf chr$i.esp6500.vcf.gz; done
tabix -p vcf chrX.esp6500.vcf.gz
tabix -p vcf chrY-and-the-rest.esp6500.vcf.gz

```

**7.** Validate VCF files with vcf-validator:

**Required**: vcf-validator (vcftools)

```bash
for i in {1..22}; do
  vcf-validator chr$i.esp6500.vcf.gz ;
done
vcf-validator chrX.esp6500.vcf.gz
vcf-validator chrY-and-the-rest.esp6500.vcf.gz

```

**8.** Cleanup

```
rm *py
```

[Return to the table of contents](#table-of-contents)



## ExAC

**Last update date:** 02-10-2019

**Update requirements:** every few months

**History of updates:**
 - 02-10-2019
---

**Description:**

This folder contains chromosome-wise bgzipped ExAC frequencies annotations VCFs, based on ExAC database in VCF format (version 1.0 lifted over to GRCh38) from [Broad Institute Google Cloud](https://console.cloud.google.com/storage/browser/gnomad-public/)

[**Instructions on how to create ExAC frequencies annotation VCFs**](#instructions-on-how-to-create-exac-frequencies-annotation-vcfs)

**Directory contents:**

```bash
<last update date>
  ├── chr1.exac.vcf.gz
  ├── chr1.exac.vcf.gz.tbi
  ├── chr2.exac.vcf.gz
  ├── chr2.exac.vcf.gz.tbi
(...)
  ├── chrX.exac.vcf.gz
  ├── chrX.exac.vcf.gz.tbi
  ├── chrY-and-the-rest.exac.vcf.gz
  └── chrY-and-the-rest.exac.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

---


**List of ExAC INFO fields:**

**Source:** ExAC
**Version:** ExAC v1.0, lifted over to GRCh38

| INFO                    | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|-------------------------|--------|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_EXAC_AC            | A      | Float   | Alternate allele count for samples (AC_Adj) - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_EXAC_AN            | 1      | Integer | Total number of alleles in samples (AN_Adj) - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_EXAC_AF            | A      | Float | Alternate allele frequency in samples (AC_Adj / AN_Adj) - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ISEQ_EXAC_POPMAX        | A      | String  | Population with maximum allele frequency - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_EXAC_POPMAX_AF     | A      | Float   | Maximum allele frequency across populations - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_EXAC_AFR_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, African / African American population (ARF)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_EXAC_AMR_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Latino population (AMR)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_EXAC_EAS_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, East Asian population (EAS)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_EXAC_FIN_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Finnish population (FIN)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_EXAC_NFE_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Non-Finnish European population (EUR)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_EXAC_OTH_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Other population (OTH)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_EXAC_SAS_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, South Asian population (SAS)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_EXAC_FILTER_STATUS | A      | String  | Filter status. Format: filter_status : (...) : filter_status. Possible values: Possible values: PASS (All filters passed), InbreedingCoeff_Filter (InbreedingCoeff <= -0.8), LowQual (Low quality), NewCut_Filter (VQSLOD > -2.632 && InbreedingCoeff >-0.8), VQSRTrancheINDEL95.00to96.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.9503 <= x < 1.2168), VQSRTrancheINDEL96.00to97.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.7622 <= x < 0.9503), VQSRTrancheINDEL97.00to99.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.0426 <= x < 0.7622), VQSRTrancheINDEL99.00to99.50 (Truth sensitivity tranche level for INDEL model at VQS Lod: -0.8363 <= x < 0.0426), VQSRTrancheINDEL99.50to99.90 (Truth sensitivity tranche level for INDEL model at VQS Lod: -8.5421 <= x < -0.8363), VQSRTrancheINDEL99.90to99.95 (Truth sensitivity tranche level for INDEL model at VQS Lod: -18.4482 <= x < -8.5421), VQSRTrancheINDEL99.95to100.00+ (Truth sensitivity tranche level for INDEL model at VQS Lod < -37254.4742), VQSRTrancheINDEL99.95to100.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: -37254.4742 <= x < -18.4482), VQSRTrancheSNP99.60to99.80 (Truth sensitivity tranche level for SNP model at VQS Lod: -4.9627 <= x < -1.8251), VQSRTrancheSNP99.80to99.90 (Truth sensitivity tranche level for SNP model at VQS Lod: -31.4709 <= x < -4.9627), VQSRTrancheSNP99.90to99.95 (Truth sensitivity tranche level for SNP model at VQS Lod: -170.3725 <= x < -31.4709), VQSRTrancheSNP99.95to100.00+ (Truth sensitivity tranche level for SNP model at VQS Lod < -39645.8352), VQSRTrancheSNP99.95to100.00 (Truth sensitivity tranche level for SNP model at VQS Lod: -39645.8352 <= x < -170.3725), AC_Adj0_Filter (No sample passed the adjusted threshold (GQ >= 20 and DP >= 10))|

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create ExAC frequencies annotation VCFs:


**1.**  Prepare the workspace.

```bash
EXAC_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/exac"
cd $EXAC_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="02-10-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.** Download ExAC database in VCF format (version 1.0  lifted over to GRCh38) from [Broad Institute Google Cloud](https://console.cloud.google.com/storage/browser/gnomad-public/):

```bash
wget -c https://storage.googleapis.com/gnomad-public/legacy/exacv1_downloads/liftover_grch38/release1/ExAC.r1.sites.liftover.b38.vcf.gz
wget -c https://storage.googleapis.com/gnomad-public/legacy/exacv1_downloads/liftover_grch38/release1/ExAC.r1.sites.liftover.b38.vcf.gz.tbi

```

**3.**  Download or prepare files necessary for INDELs normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

```

**3**. Left normalize INDELs. Create chromosome-wise VCF annotation files:

**Required**: [**create-exac-frequencies-annotation-vcf.py, version 0.0.1** ](../../../src/main/scripts/resources-tools/readme.md#create-exac-frequencies-annotation-vcf)

**Required**: bcftools

**Required**: Biopython

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-mitomap-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-exac-frequencies-annotation-vcf.py

```

```bash
zcat ExAC.r1.sites.liftover.b38.vcf.gz | awk '{if (/^#/){ gsub("contig=<ID=", "contig=<ID=chr", $0); print $0} else {print "chr"$0}}' | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | python3 create-exac-frequencies-annotation-vcf.py ./

for i in {1..22}; do tabix -p vcf chr$i.exac.vcf.gz; done
tabix -p vcf chrX.exac.vcf.gz
tabix -p vcf chrY-and-the-rest.exac.vcf.gz

```
**4.** Validate VCF files with vcf-validator:

**Required**: vcf-validator

```bash
for i in {1..22}; do
  vcf-validator chr$i.exac.vcf.gz ;
done
vcf-validator chrX.exac.vcf.gz
vcf-validator chrY-and-the-rest.exac.vcf.gz

```

**5.** Cleanup:

```bash
rm *.py

```

[Return to the table of contents](#table-of-contents)

---
## gnomAD exomes v2, liftover

**Last update date:** 02-10-2019

**Update requirements:** almost never

**History of updates:**
 - 02-10-2019
---

**Description:**

This folder contains chromosome-wise bgzipped gnomAD exomes frequencies annotation VCFs. VCFs are based on gnomAD exomes database in VCF format, version 2.1.1 lifted over to GRCh38, downloaded from [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/), description o 2.1.1 version changes can be read  [here](https://storage.googleapis.com/gnomad-public/release/2.1.1/README.txt)

[**Instructions on how to create gnomAD exomes v2 liftover frequencies annotation VCFs**](#instructions-on-how-to-create-gnomad-exomes-v2-liftover-frequencies-annotation-vcfs)

**Directory contents:**

```bash
<last update date>
  ├── chr1.gnomad-exomes.vcf.gz
  ├── chr1.gnomad-exomes.vcf.gz.tbi
  ├── chr2.gnomad-exomes.vcf.gz
  ├── chr2.gnomad-exomes.vcf.gz.tbi
(...)
  ├── chrX.gnomad-exomes.vcf.gz
  ├── chrX.gnomad-exomes.vcf.gz.tbi
  ├── chrY-and-the-rest.gnomad-exomes.vcf.gz
  └── chrY-and-the-rest.gnomad-exomes.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

---

**List of gnomAD exomes INFO fields:**

**Source:** gnomAD

**Verison:** gnomAD v2.1.1, lifted over to GRCh38

| INFO                                         | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|----------------------------------------------|--------|---------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_GNOMAD_EXOMES_AC                        | A      | Integer | Alternate allele count for samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_AN                        | 1      | Integer | Total number of alleles in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_AF                        | A      | Float   | Alternate allele frequency in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_nhomalt                   | A      | Integer | Count of homozygous individuals in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_controls_AF               | A      | Float   | Alternate allele frequency in samples in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_controls_nhomalt          | A      | Integer | Count of homozygous individuals in samples in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF              | A      | Float   | Alternate allele frequency in samples in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt         | A      | Integer | Count of homozygous individuals in samples in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF             | A      | Float   | Alternate allele frequency in samples in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt        | A      | Integer | Count of homozygous individuals in samples in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_popmax                    | A      | String  | Population with maximum AF - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_popmax                    | A      | Float   | Alternate allele frequency in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_nhomalt_popmax            | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_controls_popmax           | A      | String  | Population with maximum AF in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_controls_AF_popmax        | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_controls_nhomalt_popmax   | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_neuro_popmax          | A      | String  | Population with maximum AF in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_popmax       | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt_popmax  | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_EXOMES_non_cancer_popmax         | A      | String  | Population with maximum AF in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_popmax      | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_AF_afr                    | A      | Float   | Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_controls_AF_afr           | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the controls subset (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_afr          | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_neuro subset (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_afr         | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_cancer subset (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_AF_amr                    | A      | Float   | Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_EXOMES_controls_AF_amr           | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the controls subset (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_amr          | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_neuro subset (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_cancer subset (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_EXOMES_AF_asj                    | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_controls_AF_asj           | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the controls subset (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_asj          | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_neuro subset (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_cancer subset (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_AF_eas                    | A      | Float   | Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_controls_AF_eas           | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the controls subset (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_eas          | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_neuro subset (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_cancer subset (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations     | A      | String  | Alternate allele frequencies in samples of Korean ancestry (kor), Japanese ancestry (jap), non-Korean, non-Japanese East Asian ancestry (oea) in all samples, in the controls subset, in the non_neuro subset and in the non_cancer subset: 'Allele | subpopulation_name : AF_eas_kor : controls_AF_eas_kor : non_neuro_AF_eas_kor : non_cancer_AF_eas_kor : | subpopulation_name : AF_eas_jpn : controls_AF_eas_jpn : non_neuro_AF_eas_jpn : non_cancer_AF_eas_jpn : | subpopulation_name : AF_eas_oea : controls_AF_eas_oea : non_neuro_AF_eas_oea : non_cancer_AF_eas_oea' - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_EXOMES_AF_fin                    | A      | Float   | Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_EXOMES_controls_AF_fin           | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the controls subset (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_fin          | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_neuro subset (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_cancer subset (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_AF_nfe                    | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_controls_AF_nfe           | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the controls subset (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_nfe          | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_neuro subset (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_nfe         | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_cancer subset (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations     | A      | String  | Alternate allele frequencies in samples of  Bulgarian ancestry (bgr), Estonian ancestry (est), North-Western European ancestry (nwe), Southern European ancestry (seu), Swedish ancestry (swe), non-Finnish but otherwise indeterminate European ancestry ancestry (onf) in all samples, in the controls subset, in the non_neuro subset: 'Allele | subpopulation_name : AF_nfe_bgr : controls_AF_nfe_bgr : non_neuro_AF_nfe_bgr : non_cancer_AF_nfe_bgr : | subpopulation_name : AF_nfe_est : controls_AF_nfe_est : non_neuro_AF_nfe_est : non_cancer_AF_nfe_est : | subpopulation_name : AF_nfe_nwe : controls_AF_nfe_nwe : non_neuro_AF_nfe_nwe : non_cancer_AF_nfe_nwe : | subpopulation_name : AF_nfe_seu : controls_AF_nfe_seu : non_neuro_AF_nfe_seu : non_cancer_AF_nfe_seu : | subpopulation_name : AF_nfe_swe : controls_AF_nfe_swe : non_neuro_AF_nfe_swe : non_cancer_AF_nfe_swe : | subpopulation_name : AF_nfe_onf : controls_AF_nfe_onf : non_neuro_AF_nfe_onf : non_cancer_AF_nfe_onf' - gnomAD exomes |
| ISEQ_GNOMAD_EXOMES_AF_sas                    | A      | Float   | Alternate allele frequency in samples of South Asian ancestry (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_controls_AF_sas           | A      | Float   | Alternate allele frequency in samples of South Asian ancestry in the controls subset (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_sas          | A      | Float   | Alternate allele frequency in samples of South Asian ancestry in the non_neuro subset (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_sas         | A      | Float   | Alternate allele frequency in samples of South Asian ancestry in the non_cancer subset (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_AF_oth                    | A      | Float   | Alternate allele frequency in samples of uncertain ancestry (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_controls_AF_oth           | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the controls subset (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_oth          | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_neuro subset (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_cancer subset (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_segdup                    | 0      | Flag    | Variant falls within a segmental duplication region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_lcr                       | 0      | Flag    | Variant falls within a low complexity region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_decoy                     | 0      | Flag    | Variant falls within a reference decoy region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_EXOMES_nonpar                    | 0      | Flag    | Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_FILTER_STATUS             | A      | String  | Filter status. Format: filter_status : (…) : filter_status. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (Failed random forest filtering thresholds of 0.055272738028512555, 0.20641025579497013 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |


[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create gnomAD exomes v2 liftover frequencies annotation VCFs:

**1.**  Prepare the workspace.

```bash
GNOMAD_EXOMES_V2_LIFTOVER_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/gnomad-exomes-v2-liftover"
cd $GNOMAD_EXOMES_V2_LIFTOVER_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="02-10-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.** Download gnomAD exomes database in VCF format, version 2.1.1 lifted over to GRCh38, from [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/), description o 2.1.1 version changes can be read  [here](https://storage.googleapis.com/gnomad-public/release/2.1.1/README.txt)

```bash
curl https://storage.googleapis.com/gnomad-public/release/2.1.1/liftover_grch38/vcf/exomes/gnomad.exomes.r2.1.1.sites.liftover_grch38.vcf.bgz --output gnomad.exomes.r2.1.1.sites.liftover_grch38.vcf.gz
curl https://storage.googleapis.com/gnomad-public/release/2.1.1/liftover_grch38/vcf/exomes/gnomad.exomes.r2.1.1.sites.liftover_grch38.vcf.bgz.tbi --output gnomad.exomes.r2.1.1.sites.liftover_grch38.vcf.gz.tbi

```
**3.**  Download or prepare files necessary for INDELs normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai Homo_sapiens_assembly38.fa.fai

```

**4**. Left normalize INDELs. Create chromosome-wise VCF annotation files:

**Required**: [**create-gnomad-exomes-v2-liftover-annotation-vcf.py, version 0.0.1** ](../../../src/main/scripts/resources-tools/readme.md#create-gnomad-exomes-v2-liftover-annotation-vcf)

**Required**: bcftools

**Required**: Biopython

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-gnomad-exomes-v2-liftover-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-exomes-v2-liftover-annotation-vcf.py

```

```bash
zcat gnomad.exomes.r2.1.1.sites.liftover_grch38.vcf.gz | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | python3 create-gnomad-exomes-v2-liftover-annotation-vcf.py ./

for i in {1..22}; do echo "tabix -p vcf chr$i.gnomad-exomes.vcf.gz &"; done; echo "tabix -p vcf chrX.gnomad-exomes.vcf.gz &"; echo "tabix -p vcf chrY-and-the-rest.gnomad-exomes.vcf.gz &"

```

**5.** Validate VCFs annotation files with vcf-validator:

**Required**: vcf-validator (vcftools)

```bash
for i in {1..22}; do
  echo "vcf-validator chr$i.gnomad-exomes.vcf.gz &"
done; echo "vcf-validator chrX.gnomad-exomes.vcf.gz &"; echo "vcf-validator chrY-and-the-rest.gnomad-exomes.vcf.gz &"

```

**6.** Cleanup:

```bash
rm *.py

```

[Return to the table of contents](#table-of-contents)

---

## gnomAD genomes v3

**Last update date:** 04-11-2019

**Update requirements:** almost never

**History of updates:**
 - 04-11-2019
 - 08-05-2020 - added chrY-and-the-rest.gnomad-genomes-v3.vcf.gz, because previous was missed

---

**Description:**

This folder contains chromosome-wise bgzipped gnomAD genomes v3 frequencies annotation VCFs. VCFs are based on  gnomAD genomes database in VCF format, version 3, downloaded from [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Description of the version 3 can be found in this [blogpost](https://macarthurlab.org/2019/10/16/gnomad-v3-0/)

[**Instructions on how to create gnomAD genomes v3 frequencies annotation VCFs**](#instructions-on-how-to-create-gnomad-genomes-v3-frequencies-annotation-vcfs)

**Directory contents:**

```bash
<last update date>
  ├── chr1.gnomad-genomes-v3.vcf.gz
  ├── chr1.gnomad-genomes-v3.vcf.gz.tbi
  ├── chr2.gnomad-genomes-v3.vcf.gz
  ├── chr2.gnomad-genomes-v3.vcf.gz.tbi
(...)
  ├── chrX.gnomad-genomes-v3.vcf.gz
  ├── chrX.gnomad-genomes-v3.vcf.gz.tbi
  ├── chrY-and-the-rest.gnomad-genomes-v3.vcf.gz
  └── chrY-and-the-rest.gnomad-genomes-v3.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

---

**List of gnomAD exomes INFO fields:**

**Source:** gnomAD

**Verison:** gnomAD v3


| INFO                                       | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|--------------------------------------------|--------|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ\_GNOMAD\_GENOMES\_V3\_AC              | A      | Integer | Alternate allele count for samples \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AN              | 1      | Integer | Total number of alleles in samples \-  gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF              | A      | Float   | Alternate allele frequency in samples \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ\_GNOMAD\_GENOMES\_V3\_nhomalt         | A      | Integer | Count of homozygous individuals in samples \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ISEQ\_GNOMAD\_GENOMES\_V3\_popmax          | A      | String  | Population with maximum AF \(excluding Amish \(ami\) or uncertain \(oth\) ancestry\) \-  gnomAD v3                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ\_GNOMAD\_GENOMES\_V3\_popmax\_AF      | A      | Float   | Maximum allele frequency across populations \(excluding Amish \(ami\) or uncertain \(oth\) ancestry\) \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ\_GNOMAD\_GENOMES\_V3\_nhomalt\_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_ami         | A      | Float   | Alternate allele frequency in samples of Amish ancestry \(ami\) \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_afr         | A      | Float   | Alternate allele frequency in samples of African\-American ancestry \(afr\) \- gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry \(amr\) \- gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry \(asj\) \- gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry \(eas\) \- gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry \(fin\) \- gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_nfe         | A      | Float   | Alternate allele frequency in samples of non\-Finnish European ancestry \(nfe\) \- gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_sas         | A      | Float   | Alternate allele frequency in samples of South Asian ancestry \(sas\) \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry \- gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ\_GNOMAD\_GENOMES\_V3\_lcr            | 0      | Flag    | Variant falls within a low complexity region \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ\_GNOMAD\_GENOMES\_V3\_nonpar         | 0      | Flag    | Variant \(on sex chromosome\) falls outside a pseudoautosomal region \- gnomAD v3                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ\_GNOMAD\_GENOMES\_V3\_FILTER\_STATUS  | A      | String  | Filter status\. Format: filter\_status : \(\.\.\.\) : filter\_status\. Possible values: PASS \(All filters passed\), InbreedingCoeff \(InbreedingCoeff < \-0\.3\), RF \(failed random forest filtering thresholds of 0\.2634762834546574, 0\.22213813189901457 \(probabilities of being a true positive variant\) for SNPs, indels\), AC0 \(Allele count is zero after filtering out low\-confidence genotypes \(GQ < 20; DP < 10; and AB < 0\.2 for het calls\) \- gnomAD v3 |

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create gnomAD genomes v3 frequencies annotation VCFs:

**1.**  Prepare the workspace.

```bash
GNOMAD_GENOMES_V3_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/gnomad-genomes-v3"
cd $GNOMAD_GENOMES_V3_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="04-11-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.** Download gnomAD genomes database in VCF format, version 3, from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Description of the version 3 can be found in this [blogpost](https://macarthurlab.org/2019/10/16/gnomad-v3-0/):

```bash
for i in {1..22}; do
  curl https://storage.googleapis.com/gnomad-public/release/3.0/vcf/genomes/gnomad.genomes.r3.0.sites.chr$i.vcf.bgz --output gnomad.genomes.r3.0.sites.chr$i.vcf.gz
  curl https://storage.googleapis.com/gnomad-public/release/3.0/vcf/genomes/gnomad.genomes.r3.0.sites.chr$i.vcf.bgz.tbi --output gnomad.genomes.r3.0.sites.chr$i.vcf.gz.tbi
done

 curl https://storage.googleapis.com/gnomad-public/release/3.0/vcf/genomes/gnomad.genomes.r3.0.sites.chrX.vcf.bgz --output gnomad.genomes.r3.0.sites.chrX.vcf.gz
curl https://storage.googleapis.com/gnomad-public/release/3.0/vcf/genomes/gnomad.genomes.r3.0.sites.chrX.vcf.bgz.tbi --output gnomad.genomes.r3.0.sites.chrX.vcf.gz.tbi

```

**3.** Remove redundant fields (fields with information that will be not included in filal annotation files) to reduce disk space. List of those fields is in **list-of-fields-to-remove.txt**:

**Required**: [**gnomad-genomes-v3-list-of-fields-to-remove.txt 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/gnomad-genomes-v3-list-of-fields-to-remove.txt@0.0.1/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/gnomad-genomes-v3-list-of-fields-to-remove.txt)

**Required**:  [**keep-or-remove-vcf-fields.py 0.0.1**](../../../src/main/scripts/tools/readme.md#keep-or-remove-vcf-fields)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/gnomad-genomes-v3-list-of-fields-to-remove.txt.txt@0.0.1//resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/gnomad-genomes-v3-list-of-fields-to-remove.txt
wget https://gitlab.com/intelliseq/workflows/-/raw/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.py

```
```bash
for i in {1..22}; do
  zcat gnomad.genomes.r3.0.sites.chr$i.vcf.gz | python3 keep-or-remove-vcf-fields.py --remove-fields -f gnomad-genomes-v3-list-of-fields-to-remove.txt | bgzip > gnomad.genomes.r3.0.sites.chr$i.redundant-fields-removed.vcf.gz
done
zcat gnomad.genomes.r3.0.sites.chrX.vcf.gz | python3 keep-or-remove-vcf-fields.py --remove-fields -f gnomad-genomes-v3-list-of-fields-to-remove.txt | bgzip > gnomad.genomes.r3.0.sites.chrX.redundant-fields-removed.vcf.gz

```

```bash
for i in {1..22}; do
  rm gnomad.genomes.r3.0.sites.chr$i.vcf.gz
done
rm gnomad.genomes.r3.0.sites.chrX.vcf.gz

```


**4.**  Download or preprare files necessary for indels normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

```

**5**. Left normalize INDELs.

**Required**: bcftools

**Required**: Biopython

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "zcat gnomad.genomes.r3.0.sites.chr$i.redundant-fields-removed.vcf.gz | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | bgzip > gnomad.genomes.r3.0.sites.chr$i.norm.vcf.gz &"
done; echo "zcat gnomad.genomes.r3.0.sites.chrX.redundant-fields-removed.vcf.gz | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | bgzip > gnomad.genomes.r3.0.sites.chrX.norm.vcf.gz &"

```

**6** Create chromosome-wise VCF annotation files, sort and index them:

**Required**: [**create-gnomad-genomes-v3-annotation-vcf.py, version 0.0.1** ](../../../src/main/scripts/resources-tools/readme.md#create-gnomad-genomes-v3-annotation-vcf)


**Required**: Biopython

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-gnomad-genomesv3-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-genomesv3-annotation-vcf.py

```

```bash
zcat gnomad.genomes.r3.0.sites.chr*.norm.vcf.gz  | python3 create-gnomad-genomes-v3-annotation-vcf.py ./

```

```bash
for i in {1..22}; do
	zcat chr$i.gnomad-genomes-v3.vcf.gz | awk '{if(/^#/)print;else exit}' | bgzip > chr$i.tmp.vcf.gz
done
zcat chrX.gnomad-genomes-v3.vcf.gz  | awk '{if(/^#/)print;else exit}' | bgzip > chrX.tmp.vcf.gz

for i in {1..22}; do
  echo "zcat chr$i.gnomad-genomes-v3.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | bgzip >> chr$i.tmp.vcf.gz &"
done
echo "zcat chrX.gnomad-genomes-v3.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | bgzip >> chrX.tmp.vcf.gz &"

```
```bash
for i in {1..22}; do
echo "mv chr$i.tmp.vcf.gz chr$i.gnomad-genomes-v3.vcf.gz"
done; echo "mv chrX.tmp.vcf.gz chrX.gnomad-genomes-v3.vcf.gz"

```
```bash
for i in {1..22}; do echo "tabix -p vcf chr$i.gnomad-genomes-v3.vcf.gz &"; done; echo "tabix -p vcf chrX.gnomad-genomes-v3.vcf.gz &"

```

**7.** Validate VCFs with vcf-validator:

**Required**: vcf-validator (vcftools)

```bash
mkdir logs
for i in {1..22}; do
  echo " vcf-validator chr$i.gnomad-genomes-v3.vcf.gz &>> logs/chr$i.vcf-validator.log &"
done; echo " vcf-validator chrX.gnomad-genomes-v3.vcf.gz &>> logs/chrX.vcf-validator.log &"

```

**8**. Cleanup:

```bash
rm *.py

```

[Return to the table of contents](#table-of-contents)

---

## gnomAD genomes v3, exome calling intervals

**Last update date:** 04-11-2019

**Update requirements:** almost never

**History of updates**:
+ 04-11-2019

---

**Description:**

This folder contains chromosome-wise bgzipped gnomAD genomes v3 frequencies annotation VCFs, restricted to exome calling intervals. VCFs are based on  gnomAD genomes database in VCF format, version 3, downloaded from [gnomAD download site](http://gnomad.broadinstitute.org/downloads).

Description of the version 3 can be found in this [blogpost](https://macarthurlab.org/2019/10/16/gnomad-v3-0/).

[**Instructions on how to create gnomAD genomes v3 frequencies annotation VCFs, restricted to exome calling intervals**](#instructions-on-how-to-create-gnomad-genomes-v3-frequencies-annotation-vcfs-restricted-to-exome-calling-intervals)

**Directory contents:**

```bash
<last update date>
  ├── chr1.gnomad-genomes-v3-exome-calling-intervals.vcf.gz
  ├── chr1.gnomad-genomes-v3-exome-calling-intervals.vcf.gz.tbi
  ├── chr2.gnomad-genomes-v3-exome-calling-intervals.vcf.gz
  ├── chr2.gnomad-genomes-v3-exome-calling-intervals.vcf.gz.tbi
(...)
  ├── chrX.gnomad-genomes-v3-exome-calling-intervals.vcf.gz
  ├── chrX.gnomad-genomes-v3-exome-calling-intervals.vcf.gz.tbi
  ├── chrY-and-the-rest.gnomad-genomes-v3-exome-calling-intervals.vcf.gz
  └── chrY-and-the-rest.gnomad-genomes-v3-exome-calling-intervals.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

---

**List of gnomAD exomes INFO fields:**

**Source:** gnomAD

**Verison:** gnomAD v3



| INFO                                       | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|--------------------------------------------|--------|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ\_GNOMAD\_GENOMES\_V3\_AC              | A      | Integer | Alternate allele count for samples \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AN              | 1      | Integer | Total number of alleles in samples \-  gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF              | A      | Float   | Alternate allele frequency in samples \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ\_GNOMAD\_GENOMES\_V3\_nhomalt         | A      | Integer | Count of homozygous individuals in samples \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ\_GNOMAD\_GENOMES\_V3\_popmax          | A      | String  | Population with maximum AF \(excluding Amish \(ami\) or uncertain \(oth\) ancestry\) \-  gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ\_GNOMAD\_GENOMES\_V3\_popmax\_AF      | A      | Float   | Maximum allele frequency across populations \(excluding Amish \(ami\) or uncertain \(oth\) ancestry\) \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ\_GNOMAD\_GENOMES\_V3\_nhomalt\_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_ami         | A      | Float   | Alternate allele frequency in samples of Amish ancestry \(ami\) \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_afr         | A      | Float   | Alternate allele frequency in samples of African\-American ancestry \(afr\) \- gnomAD genomes - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry \(amr\) \- gnomAD genomes - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry \(asj\) \- gnomAD genomes - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry \(eas\) \- gnomAD genomes - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry \(fin\) \- gnomAD genomes - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_nfe         | A      | Float   | Alternate allele frequency in samples of non\-Finnish European ancestry \(nfe\) \- gnomAD genomes - exome calling regions                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_sas         | A      | Float   | Alternate allele frequency in samples of South Asian ancestry \(sas\) \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ\_GNOMAD\_GENOMES\_V3\_AF\_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry \- gnomAD genomes - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ\_GNOMAD\_GENOMES\_V3\_lcr            | 0      | Flag    | Variant falls within a low complexity region \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ\_GNOMAD\_GENOMES\_V3\_nonpar         | 0      | Flag    | Variant \(on sex chromosome\) falls outside a pseudoautosomal region \- gnomAD v3 - exome calling regions                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ\_GNOMAD\_GENOMES\_V3\_FILTER\_STATUS  | A      | String  | Filter status\. Format: filter\_status : \(\.\.\.\) : filter\_status\. Possible values: PASS \(All filters passed\), InbreedingCoeff \(InbreedingCoeff < \-0\.3\), RF \(failed random forest filtering thresholds of 0\.2634762834546574, 0\.22213813189901457 \(probabilities of being a true positive variant\) for SNPs, indels\), AC0 \(Allele count is zero after filtering out low\-confidence genotypes \(GQ < 20; DP < 10; and AB < 0\.2 for het calls\) \- gnomAD v3 - exome calling regions  |


[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create gnomAD genomes v3 frequencies annotation VCFs, restricted to exome calling intervals

**1.**  Prepare the workspace.

```bash
GNOMAD_GENOMES_EXOME_CALLING_INTERVALS_V3_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/gnomad-exomes-v3-exome-calling-intervals"
cd $GNOMAD_GENOMES_EXOME_CALLING_INTERVALS_V3_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="04-11-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.** Download exome calling regions from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Those regions were provided for gnomAD v2. Those regions will be used to since the gnomAD v3 database was provided only for whole genomes.

```bash
wget -c https://storage.googleapis.com/gnomad-public/intervals/exome_calling_regions.v1.interval_list

```

**3.**  Download files necessary for lifting over the exome calling interval list from GRCh37/hg19 to hg38 and to perform INDEL normalization:

 *  **GRCh37_to_GRCh38.chain.gz** - chain file, from [Ensembl FTP](ftp://ftp.ensembl.org/pub/assembly_mapping/homo_sapiens/)

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)


```bash
wget -c ftp://ftp.ensembl.org/pub/assembly_mapping/homo_sapiens/GRCh37_to_GRCh38.chain.gz

ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

```


**4.**  Create BED file from interval_file. Perform lifting over using CrossMap.py, version 0.3.7:

**Requires:** CrossMap.py

```bash
cat exome_calling_regions.v1.interval_list  | grep -v "^@" | cut -f 1,2,3 | awk '{print $1"\t"($2 - 1)"\t"$3}' > exome-interval-list.bed

```
```bash
CrossMap.py bed GRCh37_to_GRCh38.chain.gz exome-interval-list.bed | grep -v Fail | cut -f 5,6,7 | sort -k1,1 -k2,2n > exome-interval-list.grch38.bed

```

**5.** Intersect chromosome-wise VCF files with gnomAD v3 frequency annotation with lifted exome calling interval BED file.

**Requires:** bedtools

**Requires:** [gnomAD genomes v3](#gnomad-genomes-v3)

```bash
GNOMAD_GENOMES_V3_VERSION="04-11-2019"
GNOMAD_GENOMES_V3_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/gnomad-genomes-v3/$GNOMAD_GENOMES_V3_VERSION"

```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do echo "zcat ../gnomad-genomes-v3/chr$i.gnomad-genomes-v3.vcf.gz > chr$i.gnomad-genomes-v3.vcf &"; done ;  echo "zcat ../gnomad-genomes-v3/chrX.gnomad-genomes-v3.vcf.gz > chrX.gnomad-genomes-v3.vcf &"

```

```bash
cat chr1.gnomad-genomes-v3.vcf | grep "^#" | sed 's/ - gnomAD v3/ - gnomAD v3 - exome calling intervals/g' > header

```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do echo "bedtools intersect -sorted -a chr$i.gnomad-genomes-v3.vcf -b exome-interval-list.grch38.bed | cat header - | bgzip > chr$i.gnomad-genomes-v3-exome-calling-intervals.unnormalized.vcf.gz &"; done; echo "bedtools intersect -sorted -a chrX.gnomad-genomes-v3.vcf -b exome-interval-list.grch38.bed | cat header - | bgzip > chrX.gnomad-genomes-v3-exome-calling-intervals.unnormalized.vcf.gz &"
```

**6**. Left normalize INDELs.

**Required**: bcftools

**Required**: Biopython

**Required**: parallel


The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "zcat chr$i.gnomad-genomes-v3-exome-calling-intervals.unnormalized.vcf.gz | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | bgzip > chr$i.gnomad-genomes-v3-exome-calling-intervals.vcf.gz &"
done; echo "zcat chrX.gnomad-genomes-v3-exome-calling-intervals.unnormalized.vcf.gz | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | bgzip > chrX.gnomad-genomes-v3-exome-calling-intervals.vcf.gz &"

```
```bash
parallel tabix  -f -p vcf ::: chr*.gnomad-genomes-v3-exome-calling-intervals.vcf.gz

```

**7.** Validate VCFs with vcf-validator and remove temporarty files:

**Required**: vcf-validator (vcftools)

The commands generated by script below must be executed (they can be executed in parallel):
```bash
mkdir logs
for i in {1..22}; do
  echo "vcf-validator chr$i.gnomad-genomes-v3-exome-calling-intervals.vcf.gz &>> logs/chr$i.vcf-validator.log &"
done; echo "vcf-validator chr$i.gnomad-genomes-v3-exome-calling-intervals.vcf.gz &>> logs/chrX.vcf-validator.log &"

rm *unnormalized*
rm Ho*
rm *vcf
rm header
rm GRC*
rm exome*

```

[Return to the table of contents](#table-of-contents)

---


## gnomAD genomes v2, liftover

**Last update date:** 05-10-2019

**Update requirements:** almost never

**History of updates:**
 - 05-10-2019
---

**Description:**

This folder contains chromosome-wise bgzipped gnomAD genomes frequencies annotation VCFs. VCFs are based on gnomAD genomes database in VCF format, version 2.1.1 lifted over to GRCh38, downloaded from [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/), description o 2.1.1 version changes can be read  [here](https://storage.googleapis.com/gnomad-public/release/2.1.1/README.txt)

[**Instructions on how to create gnomAD genomes v2 liftover frequencies annotation VCFs**](#instructions-on-how-to-create-gnomad-genomes-v2-liftover-frequencies-annotation-vcfs)

**Directory contents:**

```bash
<last update date>
  ├── chr1.gnomad-genomes.vcf.gz
  ├── chr1.gnomad-genomes.vcf.gz.tbi
  ├── chr2.gnomad-genomes.vcf.gz
  ├── chr2.gnomad-genomes.vcf.gz.tbi
(...)
  ├── chrX.gnomad-genomes.vcf.gz
  ├── chrX.gnomad-genomes.vcf.gz.tbi
  ├── chrY-and-the-rest.gnomad-genomes.vcf.gz
  └── chrY-and-the-rest.gnomad-genomes.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

---

**List of  gnomAD genomes INFO fields:**

**Source:** gnomAD

**Version:** gnomAD v.2.1.1, lifted over to GRCh38

| INFO                                         | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|----------------------------------------------|--------|---------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_GNOMAD_GENOMES_AC                       | A      | Integer | Alternate allele count for samples - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_AN                       | 1      | Integer | Total number of alleles in samples -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF                       | A      | Float   | Alternate allele frequency in samples - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_nhomalt                  | A      | Integer | Count of homozygous individuals in samples - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_controls_AF              | A      | Float   | Alternate allele frequency in samples in the controls subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt         | A      | Integer | Count of homozygous individuals in samples in the controls subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF             | A      | Float   | Alternate allele frequency in samples in the non_neuro subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt        | A      | Integer | Count of homozygous individuals in samples in the non_neuro subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_popmax                   | A      | String  | Population with maximum AF -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_popmax_AF                | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_nhomalt_popmax           | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_controls_popmax          | A      | String  | Population with maximum AF in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_controls_AF_popmax       | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt_popmax  | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_non_neuro_popmax         | A      | String  | Population with maximum AF in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_popmax      | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_AF_afr                   | A      | Float   | Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_afr          | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the controls subset (afr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_afr         | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_neuro subset (afr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_amr                   | A      | Float   | Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_controls_AF_amr          | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the controls subset (amr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_neuro subset (amr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_AF_asj                   | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_asj          | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the controls subset (asj) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_neuro subset (asj) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_eas                   | A      | Float   | Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_controls_AF_eas          | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the controls subset (eas) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_neuro subset (eas) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_AF_fin                   | A      | Float   | Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_controls_AF_fin          | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the controls subset (fin) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_neuro subset  (fin) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_AF_nfe                   | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_controls_AF_nfe          | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the controls subset (nfe) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_nfe         | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_neuro subset (nfe) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_GENOMES_AF_oth                   | A      | Float   | Alternate allele frequency in samples of uncertain ancestry - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_controls_AF_oth          | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF_nfe_subpopulations    | A      | String  | Alternate allele frequencies in samples of Estonian ancestry (est), North-Western European ancestry (nwe), Southern European ancestry (seu), non-Finnish but otherwise indeterminate European ancestry ancestry (onf) in all samples, in the controls subset and in the non_neuro subset: 'Allele | subpopulation_name : AF_nfe_est : controls_AF_nfe_est : non_neuro_AF_nfe_est | subpopulation_name : AF_nfe_nwe : controls_AF_nfe_nwe: non_neuro_AF_nfe_nwe | subpopulation_name : AF_nfe_seu : controls_AF_nfe_seu : non_neuro_AF_nfe_seu | subpopulation_name : AF_nfe_onf : controls_AF_nfe_onf : non_neuro_AF_nfe_onf' - gnomAD genomes |
| ISEQ_GNOMAD_GENOMES_segdup                   | 0      | Flag    | Variant falls within a segmental duplication region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_lcr                      | 0      | Flag    | Variant falls within a low complexity region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_decoy                    | 0      | Flag    | Variant falls within a reference decoy region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_nonpar                   | 0      | Flag    | Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_FILTER_STATUS            | A      | String  | Filter status. Format: filter_status : (...) : filter_status. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (failed random forest filtering thresholds of 0.2634762834546574, 0.22213813189901457 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD genomes                                                                                                                                                                                                        |

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create gnomAD genomes v2 liftover frequencies annotation VCFs:


**1.**  Prepare the workspace.

```bash
GNOMAD_GENOMES_V2_LIFTOVER_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/gnomad-genomes-v2-liftover"
cd $GNOMAD_GENOMES_V2_LIFTOVER_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="05-10-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.** Download gnomAD genomes database in VCF format, version 2.1.1, from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/), description o 2.1.1 version changes can be read  [here](https://storage.googleapis.com/gnomad-public/release/2.1.1/README.txt):

```bash
for i in {1..22}; do
  curl https://storage.googleapis.com/gnomad-public/release/2.1.1/liftover_grch38/vcf/genomes/gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.vcf.bgz --output gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.vcf.gz
  curl  https://storage.googleapis.com/gnomad-public/release/2.1.1/liftover_grch38/vcf/genomes/gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.vcf.bgz.tbi --output gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.vcf.gz.tbi
done

curl https://storage.googleapis.com/gnomad-public/release/2.1.1/liftover_grch38/vcf/genomes/gnomad.genomes.r2.1.1.sites.X.liftover_grch38.vcf.bgz --output gnomad.genomes.r2.1.1.sites.X.liftover_grch38.vcf.gz
curl  https://storage.googleapis.com/gnomad-public/release/2.1.1/liftover_grch38/vcf/genomes/gnomad.genomes.r2.1.1.sites.X.liftover_grch38.vcf.bgz.tbi --output gnomad.genomes.r2.1.1.sites.X.liftover_grch38.vcf.gz.tbi

```

**3.** Remove redundant fields (fields with information that will be not included in filal annotation files) to reduce disk space. List of those fields is in **gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt**:

**Required**: [**gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt version 0.0.1** ](https://gitlab.com/intelliseq/workflows/-/blob/gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt@0.0.1/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v2-liftover/gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt)

**Required**:  [**keep-or-remove-vcf-fields.py 0.0.1**](../../../src/main/scripts/tools/readme.md#keep-or-remove-vcf-fields)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt@0.0.1/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v2-liftover/gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt
wget https://gitlab.com/intelliseq/workflows/-/raw/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.p

```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
echo "zcat gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.vcf.gz | python3 keep-or-remove-vcf-fields.py -r -f gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt | bgzip > gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.redundant-fields-removed.vcf.gz &"
done; echo "zcat gnomad.genomes.r2.1.1.sites.X.liftover_grch38.vcf.gz | python3 keep-or-remove-vcf-fields.py -r -f gnomad-genomes-v2-liftover-list-of-fields-to-remove.txt | bgzip > gnomad.genomes.r2.1.1.sites.X.liftover_grch38.redundant-fields-removed.vcf.gz &"

```
```bash
for i in {1..22}; do
rm gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.vcf.gz
done
rm gnomad.genomes.r2.1.1.sites.X.liftover_grch38.vcf.gz

```


**4.**  Download or preprare files necessary for indels normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

```

**5**. Left normalize INDELs.

**Required**: bcftools

**Required**: Biopython

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "zcat gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.redundant-fields-removed.vcf.gz | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | bgzip > gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.norm.vcf.gz"
done; echo "zcat gnomad.genomes.r2.1.1.sites.$i.liftover_grch38.redundant-fields-removed.vcf.gz  | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | > gnomad.genomes.r2.1.1.sites.X.liftover_grch38.norm.vcf.gz"

```

**6.** Create chromosome-wise VCF annotation files, sort and index them:

**Required**: [**create-gnomad-genomes-v2-liftover-annotation-vcf.py, version 0.0.1** ](../../../src/main/scripts/resources-tools/readme.md#create-gnomad-genomes-v2-liftover-annotation-vcf)

**Required**: Biopython

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-gnomad-genomesv3-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-genomesv3-annotation-vcf.py

```

```bash
zcat gnomad.genomes.r2.1.1.sites.*.liftover_grch38.norm.vcf.gz | python3 create-gnomad-genomes-v2-liftover-annotation-vcf.py ./

```

```bash
for i in {1..22}; do
	zcat chr$i.gnomad-genomes.vcf.gz | awk '{if(/^#/)print;else exit}' | bgzip > chr$i.tmp.vcf.gz
done
zcat chrX.gnomad-genomes.vcf.gz  | awk '{if(/^#/)print;else exit}' | bgzip > chrX.tmp.vcf.gz
zcat chrY-and-the-rest.gnomad-genomes.vcf.gz  | awk '{if(/^#/)print;else exit}' | bgzip > chrY-and-the-rest.tmp.vcf.gz

for i in {1..22}; do
  echo "zcat chr$i.gnomad-genomes.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | bgzip >> chr$i.tmp.vcf.gz &"
done
echo "zcat chrX.gnomad-genomes.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | bgzip >> chrX.tmp.vcf.gz &"
echo "zcat chrY-and-the-rest.gnomad-genomes.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | bgzip >> chrY-and-the-rest.tmp.vcf.gz &"

```
```bash
for i in {1..22}; do
echo "mv chr$i.tmp.vcf.gz chr$i.gnomad-genomes.vcf.gz"
done; echo "mv chrX.tmp.vcf.gz chrX.gnomad-genomes.vcf.gz"; echo "mv chrY-and-the-rest.tmp.vcf.gz chrY-and-the-rest.gnomad-genomes.vcf.gz"

```

```bash
parallel tabix -f -p vcf ::: chr*.gnomad-genomes.vcf.gz

```

**7.** Validate VCFs with vcf-validator:

**Required**: vcf-validator (vcftools)

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo " vcf-validator chr$i.gnomad-genomes.vcf.gz &>> logs/chr$i.vcf-validator.log &"
done; echo " vcf-validator chrX.gnomad-genomes.vcf.gz &>> logs/chrX.vcf-validator.log &"; echo " vcf-validator chrY-and-the-rest.gnomad-genomes.vcf.gz &>> logs/chrY-and-the-rest.vcf-validator.log &"

```

[Return to the table of contents](#table-of-contents)

---


## gnomAD genomes v2 liftover, exome calling intervals

**Last update date:** 04-10-2019

**Update requirements:** almost never

**Update requirements:** almost never

**History of updates**:
+ 04-10-2019

---

**Description:**

This folder contains chromosome-wise bgzipped gnomAD genomes v2 frequencies annotation VCFs, restricted to exome calling intervals. VCFs are based on gnomAD genomes (exome calling intervals) database in VCF format, version 2.1.1, from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads). Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/), description o 2.1.1 version changes can be read  [here](https://storage.googleapis.com/gnomad-public/release/2.1.1/README.txt):



[**Instructions on how to create gnomAD genomes v2 frequencies annotation VCFs, restricted to exome calling intervals**](#instructions-on-how-to-create-gnomad-genomes-v2-frequencies-annotation-vcfs-restricted-to-exome-calling-intervals)

**Directory contents:**

```bash
<last update date>
  ├── chr1.gnomad-genomes-exome-calling-intervals.vcf.gz
  ├── chr1.gnomad-genomes-exome-calling-intervals.vcf.gz.tbi
  ├── chr2.gnomad-genomes-exome-calling-intervals.vcf.gz
  ├── chr2.gnomad-genomes-exome-calling-intervals.vcf.gz.tbi
(...)
  ├── chrX.gnomad-genomes-exome-calling-intervals.vcf.gz
  ├── chrX.gnomad-genomes-exome-calling-intervals.vcf.gz.tbi
  ├── chrY-and-the-rest.gnomad-genomes-exome-calling-intervals.vcf.gz
  └── chrY-and-the-rest.gnomad-genomes-exome-calling-intervals.vcf.gz.tbi

```

[Return to the table of contents](#table-of-contents)

---

**List of  gnomAD genomes - exome calling intervals INFO fields:**

**Source:** gnomAD

**Version:** gnomAD v.2.1.1, lifted over to hg38 with CrossMap v0.3.7

| INFO                                         | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|----------------------------------------------|--------|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_GNOMAD_GENOMES_AC                       | A      | Integer | Alternate allele count for samples - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_AN                       | 1      | Integer | Total number of alleles in samples -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF                       | A      | Float   | Alternate allele frequency in samples - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_nhomalt                  | A      | Integer | Count of homozygous individuals in samples - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_controls_AF              | A      | Float   | Alternate allele frequency in samples in the controls subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt         | A      | Integer | Count of homozygous individuals in samples in the controls subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF             | A      | Float   | Alternate allele frequency in samples in the non_neuro subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt        | A      | Integer | Count of homozygous individuals in samples in the non_neuro subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_popmax                   | A      | String  | Population with maximum AF -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_popmax_AF                | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_nhomalt_popmax           | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_controls_popmax          | A      | String  | Population with maximum AF in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_controls_AF_popmax       | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt_popmax  | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_non_neuro_popmax         | A      | String  | Population with maximum AF in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_popmax      | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_AF_afr                   | A      | Float   | Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_afr          | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the controls subset (afr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_afr         | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_neuro subset (afr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_amr                   | A      | Float   | Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_controls_AF_amr          | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the controls subset (amr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_neuro subset (amr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_AF_asj                   | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_asj          | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the controls subset (asj) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_neuro subset (asj) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_eas                   | A      | Float   | Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_controls_AF_eas          | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the controls subset (eas) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_neuro subset (eas) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_AF_fin                   | A      | Float   | Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_controls_AF_fin          | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the controls subset (fin) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_neuro subset  (fin) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_AF_nfe                   | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_controls_AF_nfe          | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the controls subset (nfe) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_nfe         | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_neuro subset (nfe) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_GENOMES_AF_oth                   | A      | Float   | Alternate allele frequency in samples of uncertain ancestry - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_controls_AF_oth          | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF_nfe_subpopulations    | A      | String  | Alternate allele frequencies in samples of Estonian ancestry (est), North-Western European ancestry (nwe), Southern European ancestry (seu), non-Finnish but otherwise indeterminate European ancestry ancestry (onf) in all samples, in the controls subset and in the non_neuro subset: 'Allele | subpopulation_name : AF_nfe_est : controls_AF_nfe_est : non_neuro_AF_nfe_est | subpopulation_name : AF_nfe_nwe : controls_AF_nfe_nwe: non_neuro_AF_nfe_nwe | subpopulation_name : AF_nfe_seu : controls_AF_nfe_seu : non_neuro_AF_nfe_seu | subpopulation_name : AF_nfe_onf : controls_AF_nfe_onf : non_neuro_AF_nfe_onf' - gnomAD genomes - exome calling intervals |
| ISEQ_GNOMAD_GENOMES_segdup                   | 0      | Flag    | Variant falls within a segmental duplication region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_lcr                      | 0      | Flag    | Variant falls within a low complexity region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_decoy                    | 0      | Flag    | Variant falls within a reference decoy region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_nonpar                   | 0      | Flag    | Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_FILTER_STATUS            | A      | String  | Filter status. Format: filter_status : (...) : filter_status. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (failed random forest filtering thresholds of 0.2634762834546574, 0.22213813189901457 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD genomes – exome calling intervals                                                                                                                                                                                                                                       |


[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create gnomAD genomes v2 frequencies annotation VCFs, restricted to exome calling intervals

**1.**  Prepare the workspace.

```bash
GNOMAD_GENOMES_EXOME_CALLING_INTERVALS_V2_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/gnomad-exomes-v2-exome-calling-intervals"
cd $GNOMAD_GENOMES_EXOME_CALLING_INTERVALS_V2_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="04-10-2019"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.** Download gnomAD genomes (exome calling intervals) database in VCF format, version 2.1.1, from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads). Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/), description o 2.1.1 version changes can be read  [here](https://storage.googleapis.com/gnomad-public/release/2.1.1/README.txt):

```bash
curl https://storage.googleapis.com/gnomad-public/release/2.1/vcf/genomes/gnomad.genomes.r2.1.1.exome_calling_intervals.sites.vcf.bgz --output gnomad.genomes.r2.1.1.exome_calling_intervals.sites.vcf.gz
curl https://storage.googleapis.com/gnomad-public/release/2.1.1/vcf/genomes/gnomad.genomes.r2.1.1.exome_calling_intervals.sites.vcf.bgz.tbi --output gnomad.genomes.r2.1.1.exome_calling_intervals.sites.vcf.gz.tbi

```

**3.**  Download files or prepare necessary for indel normalization and for lifting over the database from GRCh37/hg19 to hg38:

* **Homo_sapiens_assembly19.fasta** - reference genome from [GATK ftp](ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.fasta)

* **Homo_sapiens_assembly19.fasta.fai** - reference genome index from [GATK ftp](ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.fasta.fai)

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19/Homo_sapiens_assembly19.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19/Homo_sapiens_assembly19.fa.fai

ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz

```

**3.** Split multiallelic sites and validate VCF using vcf-validator:

**Required**: vcf-validator (vcftools)

**Required**: bcftools

```bash
bcftools norm --fasta-ref Homo_sapiens_assembly19.fa.gz --multiallelics -any  gnomad.genomes.r2.1.1.exome_calling_intervals.sites.vcf.gz | bgzip >  gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.vcf.gz

vcf-validator gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.vcf.gz
tabix -p vcf gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.vcf.gz

```

**4.**  Perform lifting over using CrossMap.py, version 0.3.7:


**Required**: CrossMap.py

```bash
CrossMap.py vcf hg19ToHg38.over.chain.gz gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.vcf.gz Homo_sapiens_assembly38.fa.gz gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.hg38.vcf
```
```bash
@ 2019-07-01 13:24:05: Read chain_file:  hg19ToHg38.over.chain.gz
@ 2019-07-01 13:24:06: Updating contig field ...
@ 2019-07-01 13:24:06: Lifting over ...
@ 2019-10-01 13:16:33: Total entries: 4887049
@ 2019-10-01 13:16:33: Failed to map: 3975

```

**5.** Sort, bgzip and index  **gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.hg38.vcf**:

**Required**: vcf-validator (vcftools)

**Required**: bcftools

```bash
cat gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.hg38.vcf | awk '{if(/^#/)print;else exit}' | sed "s/##contig=<ID=/##contig=<ID=chr/g" | sed "s/##contig=<ID=chrHLA/##contig=<ID=HLA/g"> tmp.vcf
cat gnomad.genomes.r2.1.1.exome_calling_intervals.sites.split.hg38.vcf| grep -v "^#" | sort -k1,1V -k2,2n | awk '{print "chr"$0}' >> tmp.vcf

bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any tmp.vcf | bgzip > gnomad.genomes.r2.1.1.exome_calling_intervals.sites.hg38.vcf.gz

vcf-validator gnomad.genomes.r2.1.1.exome_calling_intervals.sites.hg38.vcf.gz
tabix -p vcf gnomad.genomes.r2.1.1.exome_calling_intervals.sites.hg38.vcf.gz

rm tmp.vcf

```

**6.** Create chromosome-wise VCF annotation files:

**Required**: [**create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vcf.py, version 0.0.1** ](../../../src/main/scripts/resources-tools/readme.md#create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vcf)

**Required**: bcftools

**Required**: Biopython

**Required**: parallel

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vcf.pyy

```

```bash
zcat gnomad.genomes.r2.1.1.exome_calling_intervals.sites.hg38.vcf.gz | python3 create-gnomad-genomes-v2-liftover-exome-calling-intervals-annotation-vcf.py ./

parallel tabix -f -p vcfchr*.gnomad-genomes-exome-calling-intervals.vcf.gz

```

**7.** Validate VCF files with vcf-validator:

**Required**: vcf-validator (vcftools)

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  vcf-validator chr$i.gnomad-genomes-exome-calling-intervals.vcf.gz
done;
vcf-validator chrX.gnomad-genomes-exome-calling-intervals.vcf.gz
vcf-validator chrY-and-the-rest.gnomad-genomes-exome-calling-intervals.vcf.gz

```


[Return to the table of contents](#table-of-contents)

---

## Mitomap

**Last update date:** 14-02-2020

**Update requirements:** every few months

**History of updates:**
 - 14-02-2020
 - 27-09-2019
---

**Description:**

This folder contains bgzipped VCFs files containing annotations based on polymorphisms.vcf file downloaded from file downloaded from MITOMAP  Resources site (https://mitomap.org/foswiki/bin/view/MITOMAP/Resources).
MITOMAP frequencies VCF files are  downloaded approximately every few months.  The folder contains files based on the most recently downloaded version of MITOMAP polymorphisms VCF as well as the older ones.

**Directory contents:**

```bash
<last update date>
    ├── chrY-and-the-rest.mitomap.vcf.gz
    └── chrY-and-the-rest.mitomap.vcf.gz.tbi

```

[**Instructions on how to create MITOMAP frequencies annotation VCF**](#instructions-on-how-to-create-mitomap-frequencies-annotation-vcf)

[Return to the table of contents](#table-of-contents)

---


**List of MITOMAP INFO fields**

**Source:** MITOMAP

(the most recent) **Version:** 14-02-2020

| INFO       | Number | Type  | Description                                              |
|------------|--------|-------|----------------------------------------------------------|
| MITOMAP_AC | A      | Float | Allele count in GenBank out of 50175 sequences – MITOMAP |
| MITOMAP_AF | A      | Float | Allele Frequency in GenBank - MITOMAP                    |

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create MITOMAP frequencies annotation VCF:


**1.**  Prepare the workspace.

```bash
MITOMAP_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/freqencies/mitomap"
cd $MITOMAP_DIR

```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="14-02-2020"
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2** Download MITOMAP database in VCF format from  [MITOMAP  Resources](https://mitomap.org/foswiki/bin/view/MITOMAP/Resources):

```bash
wget http://mitomap.org/cgi-bin/polymorphisms.cgi?format=vcf
mv polymorphisms.cgi\?format\=vcf mitomap.vcf

```

**2**.   Download or preprare files necessary for indels normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai

```

**3.** Create annotation VCF. Split multiallelic sites:

**Required**: [**create-mitomap-frequencies-annotation-vcf.py, version 0.0.1**](../../../src/main/scripts/resources-tools/readme.md#create-mitomap-frequencies-annotation-vcf)

**Required**: bcftools

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-mitomap-frequencies-annotation-vcf.py@0.0.1/src/main/scripts/resources-tools/create-mitomap-frequencies-annotation-vcf.py

```

```bash
cat mitomap.vcf | python3 create-mitomap-frequencies-annotation-vcf.py  | bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any | bgzip > chrY-and-the-rest.mitomap.vcf.gz
rm mitomap.vcf
rm Homo*
tabix -p vcf chrY-and-the-rest.mitomap.vcf.gz

```

**4.** Validate VCF file with vcf-validator:

**Required**: vcf-validator (vcftools)

```bash
vcf-validator chrY-and-the-rest.mitomap.vcf.gz

```

[Return to the table of contents](#table-of-contents)

## GnomAD coverage

This data is used in task: https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/wdl/tasks/vcf-anno-gnomadcov/latest/vcf-anno-gnomadcov.wdl
Data is in docker images, created from this dockerfile: https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/docker/task/task_vcf-anno-gnomadcov-exome/Dockerfile


**Archival dockerfiles: 
- https://gitlab.com/intelliseq/workflows/-/tree/vcf-anno-gnomadcov@1.0.0/src/main/docker/vcf-annotation/coverage-exome/Dockerfile
- https://gitlab.com/intelliseq/workflows/-/blob/vcf-anno-gnomadcov@1.0.0/src/main/docker/vcf-annotation/coverage-genome/Dockerfile


### gnomAD coverage exomes

**Last update date:** 16.12.2020 (2.0.0)

**Previous update:**  22-10-2019, documentation: https://gitlab.com/intelliseq/workflows/-/blob/readme@1.0.2/resources/snps-and-indels-annotations/frequencies/readme.md#gnomad-coverage 

**Note** Previous version was moved from snp-and-indels-annotations/technical dir to snp-and-indels-annotations/frequencies on anakin.

### About version 16.12.2020
Version 22-10-2019 was updated to extract regions described in intervals (/data/public/intelliseqngs/workflows/resources/intervals/agilent/v6-r2-v7-combined-padded/v6-r2-v7-combined-padded.bed). In previous version some variants were missing. In previous update code was different, bacause for exomes was used gnomAD v.02 liftedover. This time is used gnomAD v.03. Structure of those tsv.gz files is different, more columns in different order and different index.
This time liftover is not needed. File is large and downloading takes around 1 hour. Operations below also take few hours. Please be patient... 
Intervals do not contains mitochondrial regions, is not intersected using betools (please check run.sh). Procedure is different. In gnomAD v0.3 there is not equivalent tsv coverage table available. MT data is taken from previous version of gnomad-exomes. 

At the and mitochondrial coverage tsv file is merged with Y-and-the-rest.

```
wget -c  wget https://storage.googleapis.com/gcp-public-data--gnomad/release/3.0.1/coverage/genomes/gnomad.genomes.r3.0.1.coverage.summary.tsv.bgz
mv gnomad.genomes.r3.0.1.coverage.summary.tsv.bgz gnomad.genomes.r3.0.1.coverage.summary.tsv.gz
zcat gnomad.genomes.r3.0.1.coverage.summary.tsv.gz | awk 'BEGIN{OFS="\t"} {split($1,a , ":"); print a[1], a[2]-1, a[2], $2, $5}' > test.bed
zcat /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/technical/gnomad-coverage-exomes/chr1.coverage.tsv.gz | head -n 3 > header.txt
```
**Note**: before executing run.sh please open it and comment come code sections to make it step by step. 
```
wget https://gitlab.com/intelliseq/workflows/-/raw/dev/resources/snps-and-indels-annotations/frequencies/gnomad-coverage-exomes/run.sh
bash run.sh
```

### gnomAD coverage genomes

**Last update date:** 17-12-2020 (2.0.0) (v0.3)

**Previous update date:** 28-11-2019  (v0.3)

**Update requirements:** unnecessary

#### About last update version - 17.12.2020 (named 2.0.0)

Tsv files with coverage from chromosomes 1-22 + X are the same as previous version (28-11-2019). Y-and-the-rest is different. Previous version does not contain mitochondrial genome (propably by mystake). The newest version was created by mergind mitochodnrial regions from exome with old file Y-and-the-rest. The rest pf chromosomes were symlinked to new location:

```
cd /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-coverage/2.0.0/gnomad-coverage-genomes
for i in {1..22} X; do ln -s /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-coverage/22-10-2019/gnomad-coverage-genomes/chr${i}.coverage.tsv.gz; done
```
Copy old file (only with Y chromosome) and merge

```
cp /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-coverage/22-10-2019/gnomad-coverage-genomes/chrY-and-the-rest.coverage.tsv.gz .
gunzip chrY-and-the-rest.coverage.tsv.gz
zcat  ../../22-10-2019/gnomad-coverage-exomes/chrY-and-the-rest.coverage.tsv.gz | grep ^chrM >> chrY-and-the-rest.coverage.tsv
bgzip chrY-and-the-rest.coverage.tsv

```

#### Version 28-11-2019

**1.** Download genomes coverage summary TSV file from [gnomAD site](https://storage.googleapis.com/gnomad-public/release/3.0/coverage/genomes/gnomad.genomes.r3.0.coverage.summary.tsv.bgz):
```
wget -c https://storage.googleapis.com/gnomad-public/release/3.0/coverage/genomes/gnomad.genomes.r3.0.coverage.summary.tsv.bgz
mv gnomad.genomes.r3.0.coverage.summary.tsv.bgz gnomad.genomes.r3.0.coverage.summary.tsv.gz
gunzip gnomad.genomes.r3.0.coverage.summary.tsv.gz 
awk -i inplace 'BEGIN{OFS="\t"}{print $1, $2, $4}' gnomad.genomes.r3.0.coverage.summary.tsv
awk -i inplace '{sub(/:/,"\t",$1)};1' gnomad.genomes.r3.0.coverage.summary.tsv
awk -i inplace 'BEGIN{OFS="\t"}NR>1{print $1, $2, $3, $4}' gnomad.genomes.r3.0.coverage.summary.tsv
cat header.txt > output.tsv
cat gnomad.genomes.r3.0.coverage.summary.tsv >> output.tsv
rm gnomad.genomes.r3.0.coverage.summary.tsv
mv output.tsv gnomad.genomes.r3.0.coverage.summary.tsv
```

**2.** Create chromosome-wise TSV annotation files:
```
for i in {1..22}
do 
	CHR="chr${i}"
    awk '/chrom/' gnomad.genomes.r3.0.coverage.summary.tsv > chr$i.coverage.tsv
	grep -w $CHR gnomad.genomes.r3.0.coverage.summary.tsv >> chr$i.coverage.tsv
    cat info.txt | cat - chr$i.coverage.tsv > temp && mv temp chr$i.coverage.tsv
    bgzip < chr$i.coverage.tsv > chr$i.coverage.tsv.gz
done

i="X"
CHR="chr${i}"
awk '/chrom/' gnomad.genomes.r3.0.coverage.summary.tsv > chr$i.coverage.tsv
grep -w $CHR gnomad.genomes.r3.0.coverage.summary.tsv >> chr$i.coverage.tsv
cat info.txt | cat - chr$i.coverage.tsv > temp && mv temp chr$i.coverage.tsv
bgzip < chr$i.coverage.tsv > chr$i.coverage.tsv.gz

i="Y-and-the-rest"
grep -w 'Y\|MT' gnomad.genomes.r3.0.coverage.summary.tsv > chr$i.coverage.tsv
awk '/chrom/' gnomad.genomes.r3.0.coverage.summary.tsv > new.chr$i.coverage.tsv
cat info.txt | cat - chr$i.coverage.tsv > temp && mv temp chr$i.coverage.tsv
bgzip < chr$i.coverage.tsv > chr$i.coverage.tsv.gz
```

**List of COVERAGE INFO fields**

| INFO                        | Number | Type   | Description     |
|-----------------------------|--------|--------|-----------------|
| ISEQ_GNOMAD_COV_MEAN        | 1      | Float  | Coverage mean   |
| ISEQ_GNOMAD_COV_OVER        | 1      | Float  | Coverage over 1 |


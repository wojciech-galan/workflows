#!/bin/bash
set -e -o pipefail

VERSION="0.0.1"

MONTH=$1
YEAR=$2
DATE="01-$MONTH-$YEAR"
NEW_DATE=`echo $DATE | sed 's/-01-/-Jan-/' | sed 's/-02-/-Feb-/' | sed 's/-03-/-Mar-/' |
                       sed 's/-04-/-Apr-/' | sed 's/-05-/-May-/' | sed 's/-05-/-Jun-/' |
                       sed 's/-07-/-Jul-/' | sed 's/-08-/-Aug-/' | sed 's/-09-/-Sep-/' |
                       sed 's/-10-/-Oct-/' | sed 's/-11-/-Nov-/' | sed 's/-12-/-Dec-/'`
                    
mkdir /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/civic/$DATE
cd /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/civic/$DATE

wget https://civicdb.org/downloads/"$NEW_DATE"/"$NEW_DATE"-civic_accepted.vcf
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/md5sum.txt


## the vcf is not sorted and has no contigs in the header, chromosomes 1,2,3...
grep '^#' "$NEW_DATE"-civic_accepted.vcf | sed 's/ \"/\"/g' > tmp1.vcf
grep -v '^#' "$NEW_DATE"-civic_accepted.vcf | sort  -k1,1 -k2,2n >> tmp1.vcf
vcf-validator tmp1.vcf
bgzip tmp1.vcf
tabix -p vcf tmp1.vcf.gz

## normalize indels, remove sites not matching the reference (4), change chromosome names
for i in {1..22} X Y;do printf "$i\tchr$i\n" >> map.file;done
printf "MT\tchrM\n" >> map.file
bcftools norm -m -any -c x -f /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19/Homo_sapiens_assembly19.fa tmp1.vcf.gz \
| bcftools annotate --rename-chrs map.file  -o tmp2.vcf.gz -O z

## lift-over
CrossMap.py vcf hg19ToHg38.over.chain.gz tmp2.vcf.gz \
 /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa tmp3.vcf


## remove contigs from header, change chromosomes order

grep -v '^##contig' tmp3.vcf | bgzip > tmp4.vcf.gz
tabix -p vcf tmp4.vcf.gz 
tabix -H tmp4.vcf.gz | bgzip > tmp5.vcf.gz
for i in {1..22} X Y M; do tabix   tmp4.vcf.gz chr"$i" | bgzip >> tmp5.vcf.gz;done
tabix -p vcf tmp5.vcf.gz

## normalize indels, check reference change CSQ field name
bcftools norm -m -any -c x -f /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa tmp5.vcf.gz | \
 sed 's/\\x2E/./g' |\
 sed 's/\\x3A/:/g' |\
 sed 's/\\x3E/>/g' |\
 sed 's/\\x5F/_/g' |\
 sed 's/\\x2A/*/g' |\
  bgzip  > civic-hg38.vcf.gz
tabix -p vcf civic-hg38.vcf.gz

vcf-validator civic-hg38.vcf.gz

## clean
rm tmp* md5sum* hg19ToHg38.over.chain.gz map.file


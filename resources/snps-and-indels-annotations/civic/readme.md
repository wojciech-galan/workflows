The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/civic**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/civic**


## table of contents

- [CIViC](#civic)
  * [Description](#description)
  * [How to create](#how-to-create)
  * [Content of directory](#content-of-directory)


## CIViC

**Last update date:** 01-04-2021

**Update requirements:** Monthly 

**History of versions and updates:**
 - **version** 01-04-2021 


---

### Description

The CIViC vcf file contains curated information on cancer related variants. 
The file used for annotation was downloaded from the [CIViC download site](https://civicdb.org/releases) 
and lifted-over to hg38 coordinates.

### How to create (on anakin)
Requirements:   
 * bcftools 
 * tabix  
 * CrossMap.py  
 * vcf-validator  
 (all programs are installed on anakin)   

```bash
## get the civic.sh script
wget https://gitlab.com/intelliseq/workflows/-/raw/civic.sh@0.0.1/resources/snps-and-indels-annotations/civic/civic.sh

## run the script giving two positional arguments: month year
## for example, to get and prepare CIViC vcf from April 2021 run:
chmod +x civic.sh  
./civic.sh 04 2021
```

### Content of directory

```bash
civic
|
version (date)
|
├── 01-Apr-2021-civic_accepted.vcf ## downloaded file
├── civic-hg38.vcf.gz              ## file needed for civic annotation (docker: task_vcf-anno-civic) 
└── civic-hg38.vcf.gz.tbi          ## file needed for civic annotation (docker: task_vcf-anno-civic)

```

[Return to the table of contents](#table-of-contents)
#!/bin/bash
# version: 2.0.1
# last update: 13.10.2020
# author: Katarzyna Kolanek, Monika Krzyżanowska
# Dockerfile: -
# Dockerimage: intelliseqngs/clinvar-func-anno:2.0.1
# Code created according this documentation
# https://gitlab.com/intelliseq/workflows/-/blob/bam-concat@1.0.2/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md

# graph: https://bit.ly/3

# Runs each script as whole instead of line-by-line
.ONESHELL:

# Makes each script to fail as soon as there is any error
.SHELLFLAGS = -ec

REF_GENOME="/resources/reference-genomes/Homo_sapiens_assembly38.fa.gz"
#REF_GENOME="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz"

default: downloads clinvar.variant-level.vcf.gz.tbi

downloads: clinvar.vcf.gz submission_summary.txt.gz variation_allele.txt.gz

clean: 
	echo "Removing unnecessary, temporary files..."
	rm *.py tmp* *md5

clinvar.vcf.gz: 
	echo "Downloading clinvar.vcf.gz..."
	wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz
	wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz.md5
	#md5sum clinvar.vcf.gz
	#cat clinvar.vcf.gz.md5
	#rm *md5

submission_summary.txt.gz:
	echo "Downloading submission_summary.txt..."
	wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz
	wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz.md5
	#md5sum submission_summary.txt.gz
	#submission_summary.uiitxt.gz.md5
	#rm *md5
	#gunzip

variation_allele.txt.gz:
	echo "Donwloading variation_allele.txt.gz..."
	wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variation_allele.txt.gz
	wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variation_allele.txt.gz.md5
	#md5sum variation_allele.txt.gz
	#cat variation_allele.txt.gz.md5
	#rm *md5
	#gunzip variation_allele.txt.gz

tmp.vcf.gz: clinvar.vcf.gz
	echo "Removimg troublesome characters: ß ã ä è é ê ë ô ö ú ü..."
	zcat clinvar.vcf.gz | sed 's/Ã©/A/g' | sed 's/ß/B/g' | sed 's/ã/a/g' | sed 's/ä/a/g' | sed 's/é/e/g' | sed 's/è/e/g' | sed 's/ê/e/g' | sed 's/ë/e/g' | sed 's/ô/o/g' | sed 's/ö/o/g' | sed 's/ú/u/g' | sed 's/ü/u/g' | bgzip > tmp.vcf.gz

tmp2.vcf.gz: tmp.vcf.gz
	echo "Changing chromosome naming convention, removing a (single) variant that is on NW_009646201.1 contig..."
	zcat tmp.vcf.gz | awk '{if (/^#/) {print;} else if (/^MT/) {FS="\t"; OFS="\t"; $$1="chrM"; print} else {print "chr"$$0}}' | grep -v "NW_009646201.1" | bgzip > tmp2.vcf.gz

tmp2.vcf.gz.tbi: tmp2.vcf.gz
	echo "Creating index for tmp2.vcf.gz"
	tabix -p vcf tmp2.vcf.gz

tmp_clinvar.split.normalized.vcf.gz: tmp2.vcf.gz tmp2.vcf.gz.tbi
	echo "Splitting multiallelic sites, left-indel normalizing variants and validating VCF using vcf-validator..."
	bcftools norm --fasta-ref ${REF_GENOME} --multiallelics -any tmp2.vcf.gz |  grep -v 23868631 | bgzip > tmp_clinvar.split.normalized.vcf.gz
	vcf-validator tmp_clinvar.split.normalized.vcf.gz
	# REMOVE MALFORMED LINE 
	#zcat tmp_clinvar.split.normalized.vcf.gz | grep -v 23868631 | bgzip > tmp_clinvar.split.normalized.vcf.gz

tmp_clinvar.split.normalized.vcf.gz.tbi: tmp_clinvar.split.normalized.vcf.gz
	echo "Creating index for tmp_clinvar.split.normalized.vcf.gz..."
	tabix -p vcf tmp_clinvar.split.normalized.vcf.gz

tmp.txt.gz: submission_summary.txt.gz
	echo "Removing malformed line from submission_summary.txt..."
	zcat submission_summary.txt.gz  | grep -v "ENIGMA rules, 2015" | bgzip > tmp.txt.gz
	mv tmp.txt.gz submission_summary.txt.gz

create-clinvar-annotation-vcf.py: 
	echo "Downloading create-clinvar-annotation-vcf.py, version: 0.0.1..."
	wget https://gitlab.com/intelliseq/workflows/-/raw/create-clinvar-annotation-vcf.py@2.0.1/src/main/scripts/resources-tools/create-clinvar-annotation-vcf.py

clinvar.variant-level.vcf.gz: tmp_clinvar.split.normalized.vcf.gz tmp_clinvar.split.normalized.vcf.gz.tbi create-clinvar-annotation-vcf.py tmp.txt.gz
	echo "Creating clinvar.variant-level.vcf.gz annotated file using variation_allele.txt.gz and submission_summary.txt.gz..."
	python3 create-clinvar-annotation-vcf.py submission_summary.txt.gz variation_allele.txt.gz tmp_clinvar.split.normalized.vcf.gz | bgzip > clinvar.variant-level.vcf.gz

clinvar.variant-level.vcf.gz.tbi: clinvar.variant-level.vcf.gz
	echo "Creating index for clinvar.variant-level.vcf.gz..."
	tabix -p vcf clinvar.variant-level.vcf.gz
	echo "Validating clinvar.variant-level.vcf.gz..."
	vcf-validator clinvar.variant-level.vcf.gz

### SOME TESTS: CHECK IF FILES ARE NOT EMPTY, USAGE: make test ###

test_tmp.vcf.gz: tmp.vcf.gz
	[ -s tmp.vcf.gz ]

test_tmp2.vcf.gz: tmp2.vcf.gz
	[ -s tmp2.vcf.gz ] 

test_tmp2.vcf.gz.tbi: tmp2.vcf.gz.tbi
	[ -s tmp2.vcf.gz.tbi ]
	
test_tmp_clinvar.split.normalized.vcf.gz: tmp_clinvar.split.normalized.vcf.gz
	[ -s tmp_clinvar.split.normalized.vcf.gz ]

test_tmp_clinvar.split.normalized.vcf.gz.tbi: tmp_clinvar.split.normalized.vcf.gz.tbi
	[ -s tmp_clinvar.split.normalized.vcf.gz.tbi ]

test_clinvar.variant-level.vcf.gz: clinvar.variant-level.vcf.gz
	[ -s clinvar.variant-level.vcf.gz ] 

test_clinvar.variant-level.vcf.gz.tbi: clinvar.variant-level.vcf.gz.tbi
	[ -s clinvar.variant-level.vcf.gz.tbi ]

test: test_tmp.vcf.gz test_tmp2.vcf.gz test_tmp2.vcf.gz.tbi test_tmp_clinvar.split.normalized.vcf.gz test_tmp_clinvar.split.normalized.vcf.gz.tbi tmp2.vcf.gz.tbi test_clinvar.variant-level.vcf.gz test_clinvar.variant-level.vcf.gz.tbi


# Code created according this documentation
#https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md#clinvar

# graph: https://bit.ly/3

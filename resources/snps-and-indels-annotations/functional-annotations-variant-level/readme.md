# FUNCTIONAL ANNOTATIONS - VARIANT LEVEL

Resources to annotate VCF files containing SNPs and INDELs variants with functional annotations (variant level).
Reference genome: **GRCh38**

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level**

---

## Table of Contents

**Functional annotations - variant level**

`Used in task_vcf-anno-func-var docker`  
  * [**Merged VCF functional annotations, variant level (CLINVAR + MITOMAP)**](#merged-vcf-functional-annotations-variant-level)
  * [ClinVar](#clinvar)
  * [Mitomap diseases](#mitomap-diseases)

[Return to: SNPs and INDELs annotations](./../readme.md)

---

## Merged VCF functional annotations, variant level

**Last update date:** 14-12-2020

**Update requirements:** monthly

**Current version**: 2.1.0

**History of versions**:
+ 2.1.0  
+ 2.0.0
+ 1.0.0
+ 0.2.0
+ 0.1.0

---
**Description**:

This folder contains bgzipped VCFs files created by merging the bgzipped annotation VCFs from the [ClinVar](#clinvar) and [Mitomap diseases](#mitomap-diseases) databases. The folder contains files based on the most recently downloaded version of ClinVar and MITOMAP diseases  as well as the older ones.


**[Instructions on how to create VCF with functional annotations (variant level, CLINVAR + MITOMAP)](#instructions-on-how-to-create-vcf-with-functional-annotations-variant-level)**

[(Archival) Details of previous versions of VCF with functional annotations (variant level)](#history-of-previous-versions-of-vcf-with-functional-annotations-variant-level)

**Directory contents:**

```bash
<version>
    ├── all-variant-level.vcf.gz
    └── all-variant-level.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)
---

### **Instructions on how to create VCF with functional annotations (variant level):**

**1.** Set bash variables pointing to the latest ClinVar and MITOMAP diseases annotation VCF, create directory with the newest version, set path to necessary scripts.

```bash
VERSION="2.1.0"
CLINVAR_VERSION="09-12-2020"
MITOMAP_DISEASES_VERSION="14-12-2020"
FUNCTIONAL_ANNOTATIONS_VARIANT_LEVEL="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level"

ALL_VARIANT_LEVEL_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/all-variant-level"
cd $ALL_VARIANT_LEVEL_DIR
```
To  see variables for previous version go to: [(Archival) History of versions](#history-of-previous-versions-of-vcf-with-functional-annotations-variant-level)

```bash
mkdir $VERSION
cd $VERSION
CLINVAR="$FUNCTIONAL_ANNOTATIONS_VARIANT_LEVEL/clinvar/$CLINVAR_VERSION/clinvar.variant-level.vcf.gz"
MITOMAP_DISEASES="$FUNCTIONAL_ANNOTATIONS_VARIANT_LEVEL/mitomap-diseases/$MITOMAP_DISEASES_VERSION/mitomap-diseases.variant-level.vcf.gz"
```

**2.** Create annotation file containing all variant-level annotations:

**Requires:** bcftools

**Requires:** [**keep-or-remove-vcf-fields.py, version 0.0.1**](../../../src/main/scripts/tools/readme.md#keep-or-remove-vcf-fields)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/keep-or-remove-vcf-fields.py@0.0.1/src/main/scripts/tools/keep-or-remove-vcf-fields.py

bcftools merge $CLINVAR $MITOMAP_DISEASES | python3 keep-or-remove-vcf-fields.py -r -l AC,AN,SF  | grep -v "##reference" | grep -v "##FILTER" | grep -v "##source" | grep -v "##contig" | grep -v -i "##filedate" | awk '{if(/^#/) print; else print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t.\t"$8}' | bgzip > all-variant-level.unsorted.unnormalized.vcf.gz
```

**3.**  Download or prepare files necessary for indels normalization:

 * **Homo_sapiens_assembly38.fa.gz** and **Homo_sapiens_assembly38.fa.fai** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```bash
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.gz
ln -s /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa.fai
```

**3.** Sort and normalize **all-variant-level.unsorted.unnormalized.vcf.gz**. Fix 'Source' and 'Version' fields in VCF INFO header fields (vcf-merge removes quotation marks). Validate with vcf-validator:

**Required**: bcftools

**Required:** vcf-validator

```bash
zcat all-variant-level.unsorted.unnormalized.vcf.gz | awk '{if(/^#/)print;else exit}' | awk '{if (/^##INFO/) { gsub("Source=\"", "Source="); gsub("\",Version=", ",Version="); gsub(",Version=\"", ",Version="); gsub("\">", ">"); gsub("Source=", "Source=\""); gsub(",Version=", "\",Version="); gsub(",Version=", ",Version=\""); gsub(">", "\">"); print} else {print}}' | bgzip > all-variant-level.sorted.unnormalized.vcf.gz
zcat all-variant-level.unsorted.unnormalized.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | bgzip >> all-variant-level.sorted.unnormalized.vcf.gz

tabix -p vcf all-variant-level.sorted.unnormalized.vcf.gz

bcftools norm --fasta-ref Homo_sapiens_assembly38.fa.gz --multiallelics -any all-variant-level.sorted.unnormalized.vcf.gz  | bgzip > all-variant-level.vcf.gz

vcf-validator all-variant-level.vcf.gz
tabix -p vcf all-variant-level.vcf.gz
```

**4.** Cleanup

```bash
rm *norm* *Homo* *py
```

[Return to the table of contents](#table-of-contents)

 ### History of previous versions of VCF with functional annotations (variant level):

**version 1.0.0**

Last update: 06-02-2020

```bash
VERSION="2.0.0"
CLINVAR_VERSION="20-02-2020"
MITOMAP_DISEASES_VERSION="06-02-2020"
```

Last update: 06-02-2020

```bash
VERSION="1.0.0"
CLINVAR_VERSION="06-02-2020"
MITOMAP_DISEASES_VERSION="06-02-2020"
```

**version 0.2.0**

Last update: 28-12-2019

```bash
VERSION="0.2.0"
CLINVAR_VERSION="27-12-2019"
MITOMAP_DISEASES_VERSION="28-12-2019"
```

**version 0.1.0**

Last update: 01-10-2019

```bash
VERSION="0.1.0"
CLINVAR_VERSION="01-10-2019"
MITOMAP_DISEASES_VERSION="01-10-2019"
```

[Return to the table of contents](#table-of-contents)

## ClinVar

**Last update date:** 09-12-2020

**Update requirements:** monthly

**History of updates:**
 - 09-12-2020 
 - 13-10-2020 (intelliseqngs/clinvar-func-anno:1.0.1)
 - 17-06-2020 (intelliseqngs/clinvar-func-anno:1.0.0)
 - 07-05-2020
 - 20-02-2020
 - 06-02-2020
 - 27-12-2019
 - 01-10-2019

---

**Description:**

This folder contains bgzipped VCFs files containing annotations based on ClinVar database downloaded from [ClinVar FTP site](ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/).  The new release of ClinVar database is released approximately monthly. The folder contains files based on the most recently downloaded version of ClinVar as well as the older ones.

**[Instructions on how to create ClinVar annotation VCF](#instructions-on-how-to-create-clinvar-annotation-vcf)**

**Directory contents:**

```bash
{last update date}
   ├── clinvar.variant-level.vcf.gz <- main ClinVar annotation VCF, needed to create acmg resources and merged func-variant-level resources
   ├── clinvar.variant-level.vcf.gz.tbi
   ├── clinvar.vcf.gz
   ├── clinvar.vcf.gz.tbi
   ├── submission_summary.txt.gz
   └── variation_allele.txt.gz
```

 [Return to the table of contents](#table-of-contents)

---

**List of ClinVar INFO fields:**

**Source:** ClinVar

**Version:** 09-12-2020

| ID | Number | Type | Description |
|----|--------|------|-------------|
| ISEQ_CLINVAR_ALLELE_ID | A | String | ClinVar Allele ID |
| ISEQ_CLINVAR_VARIATION_ID | A | String | ClinVar Variation ID of a variation consisted of the variant alone (Type of variation: Variant). |
| ISEQ_CLINVAR_VARIANT_TYPE | A | String | ClinVar variant type. Possible values: single_nucleotide_variant, indel, deletion, insertion, duplication, inversion, copy_number_gain, copy_number_loss, microsatellite, variation. |
| ISEQ_VARIANT_CLINVAR_DISEASES | A | String | Diseases associated with the variant in ClinVar. Format: disease_name : (...) : disease_name.|
| ISEQ_AGGREGATED_CLINVAR_SIGNIFICANCE | A | String | The overall interpretation of clinical significance of a variation based on aggregating data from submitters. Format: clinical_significance : (...) : clinical_significance. |
| ISEQ_CLINVAR_SIGNIFICANCE | A | String | List of clinical significances of a variation gathered from ClinVar submissions. Taken from tab-file variant_summary.txt.gz. Format: clinical_significance : (...) : clinical_significance. |
| ISEQ_CLINVAR_DATE_LAST_EVALUATED | A | String | List of the last dates the variation-condition relationship last evaluated by the submitter. Taken from tab-file variant_summary.txt.gz. The order corresponds to the order in ISEQ_CLINVAR_SIGNIFICANCE. Format: date : (...) : date. |
| ISEQ_CLINVAR_COLLECTION_METHOD | A | String | List of the methods by which the submitter obtained the information provided. Taken from tab-file variant_summary.txt.gz. The order corresponds to the order in ISEQ_CLINVAR_SIGNIFICANCE. Format: collection_method : (...) : collection_method. |
| ISEQ_CLINVAR_REVIEW_STATUS | A | String | Review status for the ClinVar submissions. Possible values: criteria_provided_-_conflicting_interpretations, criteria_provided_-_multiple_submitters_-_no_conflicts, criteria_provided_-_single_submitter, no_assertion_criteria_provided, no_assertion_provided, practice_guideline, reviewed_by_expert_panel. |
| ISEQ_CLINVAR_GOLD_STARS | A | Integer | Assignment of stars for the ClinVar submissions. Based on review status (ISEQ_CLINVAR_REVIEW_STATUS). Possible values: 0, 1, 2, 3, 4. For more information see: https://www.ncbi.nlm.nih.gov/clinvar/docs/review_status/#revstat. |
| ISEQ_CLINVAR_VARIATION_INCLUDED_INFO | A | String | Information regarding ClinVar Variations including this variant (Types of variation: CompoundHeterozygote, Diplotype, Distinct chromosomes, Haplotype, Phase unknown). Format: variation_id \| variation_type \| < clinical_significance ^ collection_method ^ date_last_evaluated > (...) < clinical_significance ^ collection_method ^ date_last_evaluated > : (...) : variation_id \| variation_type \| < clinical_significance ^ collection_method ^ date_last_evaluated > (...) < clinical_significance ^ collection_method ^ date_last_evaluated >. |
| ISEQ_CLINVAR_GENE_INFO | A | String | Name(s) and ID(s) of genes for the Clinvar variant. Format: gene_name : gene_id | (...) | gene_name : gene_id. |


[Return to the table of contents](#table-of-contents)

---

####  Instructions on how to create ClinVar annotation VCF:

To create this automatically:
```
TARGET="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar"
docker run --rm -it -v ${TARGET}:/outputs -v /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38:/resources/reference-genomes intelliseqngs/clinvar-func-anno:2.0.1

```
All the above code (how to create clinvar functional annotation vcf) is included in intelliseqngs/clinvar-func-anno:2.0.1
- src/main/docker/resources/clinvar-func-anno/Dockerfile
- /resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar/Makefile

Archival:  https://gitlab.com/intelliseq/workflows/-/blob/e3c527cf22414895ebbde6c7b09f713d12613ae9/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md

[Return to the table of contents](#table-of-contents)

## Mitomap diseases

**Last update date:** 14-12-2020

**Update requirements:** every few months.

**History of updates:**
 - 14-12-2020
 - 06-02-2020
 - 28-12-2019
 - 01-10-2019
---

**Description:**

This folder contains bgzipped VCFs files containing annotations based on diseases.vcf file downloaded from file downloaded from MITOMAP  Resources site (https://mitomap.org/foswiki/bin/view/MITOMAP/Resources).
MITOMAP diseases VCF files are  downloaded approximately every few months.  The folder contains files based on the most recently downloaded version of MITOMAP diseases VCF as well as the older ones.

**Directory contents:**

```bash
<last update date>
    ├── mitomap-diseases.variant-level.vcf.gz
    └── mitomap-diseases.variant-level.vcf.gz.tbi
```

[**Instructions on how to create MITOMAP diseases annotation VCF**](#instructions-on-how-to-create-mitomap-diseases-annotation-vcf)

[Instructions on how to create MITOMAP diseases annotation VCF for versions 28-12-2019 and older](#instructions-on-how-to-create-mitomap-diseases-annotation-vcf-for-versions-28-12-2019-and-older)

[MITOMAP diseases abbreviations dictionary (.txt)](#mitomap-diseases-abbreviations-dictionary) - file used for MITOMAP diseases annotation VCF creation for versions 28-12-2019 and older

[Return to the table of contents](#table-of-contents)


---

**List of MITOMAP INFO fields**

**Source:** MITOMAP

(the most recent)  **Version:** 20201214

| INFO                   | Number | Type   | Description                            |
|------------------------|--------|--------|----------------------------------------|
| MITOMAP_DISEASE        | A      | String | Putative Disease Association - MITOMAP. Format: disease : (...) : disease |
| MITOMAP_DISEASE_STATUS | A      | String | Disease Association Status - MITOMAP   |

[Return to the table of contents](#table-of-contents)

---

#### Instructions on how to create MITOMAP diseases annotation VCF:


**1.**  Prepare the workspace.

Navigate to MITOMAP diseases directory (anakin):
```bash
MITOMAP_DISEASES_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/mitomap-diseases"
cd $MITOMAP_DISEASES_DIR
```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="14-12-2020"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.** Download MITOMAP database in VCF format from [MITOMAP  Resources](https://mitomap.org/foswiki/bin/view/MITOMAP/Resources)

```bash
wget http://mitomap.org/cgi-bin/disease.cgi?format=vcf
mv disease.cgi\?format\=vcf  disease.vcf
```

**3**.  Fix extra whitespaces inside INFO column, change contig names, split multiallelic lines and normalize indels (version: 14-12-2020)

```bash
cat disease.vcf | sed 's\  ATP6\ATP6\g' | sed 's/^MT/chrM/'  | bgzip > disease.vcf.gz
  
tabix -p vcf disease.vcf.gz
bcftools norm disease.vcf.gz --multiallelics -any --fasta-ref /data/public/intelliseqngs/workflows/resources/broad-institute-mt/0.0.1/Homo_sapiens_assembly38.chrM.fasta -o tmp-disease.vcf.gz -O z -cw  

```

**4.** Create annotation file with **create-mitomap-diseases-annotation-vcf.py** script. Validate with vcf-validator.

**Required**: [**create-mitomap-diseases-annotation-vcf.py, version 1.0.1** ](../../../src/main/scripts/resources-tools/readme.md#create-mitomap-diseases-annotation-vcf)

**Required**: vcf-validator (vcftools)

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/create-mitomap-diseases-annotation-vcf.py@1.0.1/src/main/scripts/resources-tools/create-mitomap-diseases-annotation-vcf.py
python3 create-mitomap-diseases-annotation-vcf.py tmp-disease.vcf.gz | bgzip > mitomap-diseases.variant-level.vcf.gz
rm tmp-diseqse.vcf.gz
vcf-validator mitomap-diseases.variant-level.vcf.gz
```


Archival: https://gitlab.com/intelliseq/workflows/-/blob/e3c527cf22414895ebbde6c7b09f713d12613ae9/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md

[Return to the table of contents](#table-of-contents)

***

#### MITOMAP diseases abbreviations dictionary

Dictionary of MITOMAP abbreviations to diseases -  [**mitomap-diseases-abbreviations-dictionary.txt, version 0.0.1** ](https://gitlab.com/intelliseq/workflows/-/raw/create-mitomap-diseases-annotation-vcf.py@0.0.1/resources/snps-and-indels-annotations/functional-annotations-variant-level/mitomap-diseases/mitomap-diseases-abbreviations-dictionary.txt) - was created by copying the content from: http://mitomap.org/foswiki/bin/view/MITOMAP/DiseaseList, with the following amendments applied:

- item "LD: Leigh Disease (or LS: Leigh Syndrome)" was split into two records - "LD: Leigh Disease" and "LS: Leigh Syndrome"
- item "MERME: MERRF/MELAS, Lactic Acidosis, and Stroke-like episodes overlap disease" was changed to "MERME: Myoclonic Epilepsy and Ragged Red Muscle Fibers/Mitochondrial Encephalomyopathy, Lactic Acidosis, and Stroke-like episodes overlap disease"
- item "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa; alternate phenotype at this locus is reported as Leigh Disease" was changed to "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa"
- item "Multisystem Mitochondrial Disorder (myopathy, encephalopathy, blindness, hearing loss, peripheral neuropathy)" was removed

Additionally, the following items were added:

- HCM : Hypertrophic CardioMyopathy
- PEO: Progressive External Ophthalmoplegia
- PD: Parkinson Disease
- FSHD: Facioscapulohumeral Muscular Dystrophy
- MIDM: Maternally Inherited Diabetes Mellitus
- MDD: Major Depressive Disorder
- BD: Bipolar Disorder
- SZ: Schizophrenia
- BSN: Bilateral Striatal Necrosis
- MR: Mental Retardation
- DM: Diabetes Mellitus
- NSHL: Non-Syndromic Hearing Loss
- RP: Retinitis Pigmentosa
- CM: Cardiomiopathy

[Return to the table of contents](#table-of-contents)

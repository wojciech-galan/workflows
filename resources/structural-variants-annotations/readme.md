#  STRUCTURAL VARIANTS ANNOTATIONS

Resources for structural variants annotation.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/structural-variants-annotations**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/structural-variants-annotations**

---

## Table of Contents

**Structural variants annotation**
   * [AnnotSV, annotations description](#annotsv-annotations-description)
        * 1.0.0
        * 0.0.1

[Return to: Resources](./../readme.md)


---

## dbvar

**Last update date:** 29-06-2020 (moni.krzyz)

**Update requirements:**  

**History of versions and updates:** (examples)
 - **version** (29-06-2020)

---

### Description:

"dbVar contains more than 6 million submitted structural variants from over 185 submitted human studies including large diversity projects such as the 1000 Genomes Project, gnomAD, and the CNV Global Population Survey (Sudmant et al. 2015), and from clinical sources such as ClinVar and ClinGen"

### How to create

Download from: ...

### Content of directory

```
├── GRCh38.variant_call.all.vcf.gz
└── GRCh38.variant_call.all.vcf.gz.tbi
```

### File contents

Structural variants described in vcf format in VEP: 
http://www.ensembl.info/2020/03/27/cool-stuff-the-ensembl-vep-can-do-annotating-structural-variants/

Remarks: 
- no contigs
- name of chromosomes: 22, 1 instead of chr22, chr1
- Variants as <DEL>, <INS>, <DUP> ect. 

## AnnotSV, annotations description

**Last update date:** 26-03-2020  

**Update requirements:** every few weeks

 **Current version**: 1.0.0

**History of versions:**
   + 1.0.0
   + 0.0.1

---

**Description:**  

This directory contains a file **annotsv-annotations-description.csv** required to convert TSV output from AnnotSV to VCF file by [**annotsv-tsv-to-vcf.py, version 1.0.0** ](https://gitlab.com/intelliseq/workflows/-/blob/annotsv-tsv-to-vcf.py@0.0.1/src/main/scripts/tools/annotsv-tsv-to-vcf.py) script (for current version - 1.0.0, for older versions there may be older version of the script).

[**(current - 1.0.0) Instructions on how to create AnnotSV 2.3 annotations description CSV**](#instructions-on-how-to-create-annotsv-2.3-annotations-description-csv)

[(0.0.1) Instructions on how to create AnnotSV 2.2 annotations description CSV](#instructions-on-how-to-create-annotsv-2.2-annotations-description-csv)


**Directory contents:**

```
<last update date>
  └── annotsv-annotations-description.csv
```


---

### Instructions on how to create AnnotSV 2.3 annotations description CSV


**1.**  Prepare the workspace.

```bash
ANNOTSV_DIR="/data/public/intelliseqngs/workflows/resources/structural-variants-annotations/annotsv-annotations-description"
cd $ANNOTSV_DIR
```
Create folder for  the newest version of the database.
```bash
ANNOTSV_VERSION="1.0.0"
mkdir $ANNOTSV_VERSION
cd $ANNOTSV_VERSION
```
**2.** File **annotsv-annotations-description.csv** was manually created using [AnnotSV v2.3 documentation](https://lbgi.fr/AnnotSV/Documentation/README.AnnotSV_2.3.pdf).  The file starts with a header and contains the following columns:

  - Name: **"TSV field name"**
    Index: 1
    Contents: the name of column in the TSV file produced by AnnotSV.
  - Names: **"ID"**, **"Number"**, **"Type"**, **"Description"**, **"Source"**, **"Version"**
    Indices: 2-6
    Contents: the description of the contents of a TSV field converted to VCF format. Compatible with VCF v4.3 specification.
  - Name: **"Python function"**
    Index: 7
    Contents: the name of the method that should be used to convert the contents of TSV field into contents of VCF INFO field. Methods are defined in class AnnotationDescription in script  [**annotsv-tsv-to-vcf.py, version 1.0.0**](https://gitlab.com/intelliseq/workflows/-/blob/annotsv-tsv-to-vcf.py@1.0.0/src/main/scripts/tools/annotsv-tsv-to-vcf.py). The location of the script: src/main/python/tools/annotsv-tsv-to-vcf.py.

**List of TSV field included in the file:**

AnnotSV ID, SV end, SV length, SV type, AnnotSV type, Gene name, NM, CDS length, tx length, location, location2, intersectStart, intersectEnd, DGV_GAIN_IDs, DGV_GAIN_n_samples_with_SV, DGV_GAIN_n_samples_tested, DGV_GAIN_Frequency, DGV_LOSS_IDs, DGV_LOSS_n_samples_with_SV, DGV_LOSS_n_samples_tested, DGV_LOSS_Frequency, 1000g_event, 1000g_AF, 1000g_max_AF, IMH_ID, IMH_AF, IMH_ID_others, promoters, dbVar_event, dbVar_variant, dbVar_status, TADcoordinates, ENCODEexperiments, GCcontent_left, GCcontent_right, Repeats_coord_left, Repeats_type_left, Repeats_coord_right, Repeats_type_right, ACMG, HI_CGscore, TriS_CGscore, DDD_status, DDD_mode, DDD_consequence, DDD_disease, DDD_pmids, HI_DDDpercent, synZ_ExAC, misZ_ExAC, pLI_ExAC, delZ_ExAC, dupZ_ExAC, cnvZ_ExAC, morbidGenes, morbidGenesCandidates, Mim Number, Phenotypes, Inheritance, GD_ID, GD_AN, GD_N_HET, GD_N_HOMALT, GD_AF, GD_POPMAX_AF, GD_ID_others, EXOMISER_GENE_PHENO_SCORE, MOUSE_PHENO_EVIDENCE, FISH_PHENO_EVIDENCE, HUMAN_PHENO_EVIDENCE, AnnotSV ranking, ASV_ranking_decision

---
 [Return to the table of contents](#table-of-contents)

### Instructions on how to create AnnotSV 2.2 annotations description CSV

**1.**  Prepare the workspace.

```bash
ANNOTSV_DIR="/data/public/intelliseqngs/workflows/resources/structural-variants-annotations/annotsv-annotations-description"
cd $ANNOTSV_DIR
```
Create folder for  the newest version of the database.
```bash
ANNOTSV_VERSION="0.0.1"
mkdir $ANNOTSV_VERSION
cd $ANNOTSV_VERSION
```
**2.** File **annotsv-annotations-description.csv** was manually created using [AnnotSV v2.2 documentation](https://lbgi.fr/AnnotSV/Documentation/README.AnnotSV_2.2.pdf).  The file starts with a header and contains the following columns:

  - Name: **"TSV field name"**
    Index: 1
    Contents: the name of column in the TSV file produced by AnnotSV.
  - Names: **"ID"**, **"Number"**, **"Type"**, **"Description"**, **"Source"**, **"Version"**
    Indices: 2-6
    Contents: the description of the contents of a TSV field converted to VCF format. Compatible with VCF v4.3 specification.
  - Name: **"Python function"**
    Index: 7
    Contents: the name of the method that should be used to convert the contents of TSV field into contents of VCF INFO field. Methods are defined in class AnnotationDescription in script  [**annotsv-tsv-to-vcf.py, version 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/annotsv-tsv-to-vcf.py@0.0.1/src/main/scripts/tools/annotsv-tsv-to-vcf.py). The location of the script: src/main/python/tools/annotsv-tsv-to-vcf.py.

**List of TSV field included in the file:**

AnnotSV ID, SV end, SV length, SV type, AnnotSV type, Gene name, NM, CDS length, tx length, location, location2, intersectStart, intersectEnd, DGV_GAIN_IDs, DGV_GAIN_n_samples_with_SV, DGV_GAIN_n_samples_tested, DGV_GAIN_Frequency, DGV_LOSS_IDs, DGV_LOSS_n_samples_with_SV, DGV_LOSS_n_samples_tested, DGV_LOSS_Frequency, 1000g_event, 1000g_AF, 1000g_max_AF, IMH_ID, IMH_AF, IMH_ID_others, promoters, dbVar_event, dbVar_variant, dbVar_status, TADcoordinates, ENCODEexperiments, GCcontent_left, GCcontent_right, Repeats_coord_left, Repeats_type_left, Repeats_coord_right, Repeats_type_right, ACMG, HI_CGscore, TriS_CGscore, DDD_status, DDD_mode, DDD_consequence, DDD_disease, DDD_pmids, HI_DDDpercent, synZ_ExAC, misZ_ExAC, pLI_ExAC, delZ_ExAC, dupZ_ExAC, cnvZ_ExAC, morbidGenes, morbidGenesCandidates, Mim Number, Phenotypes, Inheritance, AnnotSV ranking

 [Return to the table of contents](#table-of-contents)

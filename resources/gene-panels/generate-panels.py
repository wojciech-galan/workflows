import requests

#########################################################################

# Level 2 disease group and COVID-19 research
panels_disease_group_and_COVID = ["Skeletal disorders", "Neurology and neurodevelopmental disorders", "Ophthalmological disorders", 
          "Cardiovascular disorders", "Renal and urinary tract disorders", "Hearing and ear disorders", 
          "Dermatological disorders", "Growth disorders", "Metabolic disorders", 
          "Dysmorphic and congenital abnormality syndromes", "Endocrine disorders", "Haematological disorders", 
          "Rheumatological disorders", "Gastroenterological disorders", "Respiratory disorders", 
          "Haematological and immunological disorders", "Ciliopathies", "Tumour syndromes", "COVID-19 research"]

for panel in panels_disease_group_and_COVID:
    panels_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/?format=json")
    panels_json = panels_response.json()
    genes = []
    next_site = True
    while next_site:
        for i in range(len(panels_json['results'])):
            if panels_json['results'][i]['disease_group'] == panel:
                panel_id_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/{}/?format=json".format(panels_json['results'][i]['id']))
                panel_id_json = panel_id_response.json()
                for j in range(len(panel_id_json['genes'])):
                    genes.append(panel_id_json['genes'][j]['gene_data']['gene_symbol'])
            elif panels_json['results'][i]['name'] == panel:
                panel_id_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/{}/?format=json".format(panels_json['results'][i]['id']))
                panel_id_json = panel_id_response.json()
                for j in range(len(panel_id_json['genes'])):
                    genes.append(panel_id_json['genes'][j]['gene_data']['gene_symbol'])
        try:
            panels_response = requests.get("{}".format(panels_json['next']))
            panels_json = panels_response.json()
        except:
            next_site = False
    # unique genes
    genes = list(set(genes))
    with open("{}.txt".format(panel.replace(" ", "_")), "w") as outfile:
        outfile.write(", ".join(genes))
        
#########################################################################
       
# panel "Cancer Germline"
panels_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/?format=json")
panels_json = panels_response.json()
genes = []
next_site = True
while next_site:
    for i in range(len(panels_json['results'])):
        try:
            for k in range(len(panels_json['results'][i]['types'])):
                if "Cancer Germline" in panels_json['results'][i]['types'][k]['name']:
                    panel_id_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/{}/?format=json".format(panels_json['results'][i]['id']))
                    panel_id_json = panel_id_response.json()
                    for j in range(len(panel_id_json['genes'])):
                        genes.append(panel_id_json['genes'][j]['gene_data']['gene_symbol'])
        except:
            continue
    try:
        panels_response = requests.get("{}".format(panels_json['next']))
        panels_json = panels_response.json()
    except:
        next_site = False
# unique genes
genes = list(set(genes))
with open("Cancer_Germline.txt", "w") as outfile:
    outfile.write(", ".join(genes))

#########################################################################
    
panels_others = ["Adult onset movement disorder", "Albinism or congenital nystagmus", "Amyloidosis", "Aniridia", 
          "Ataxia and cerebellar anomalies - narrow panel", "Autosomal recessive primary hypertrophic osteoarthropathy", 
          "Bardet Biedl syndrome", "Bleeding and platelet disorders", "Cardiac arrhythmias", 
          "Cardiomyopathies - including childhood onset", "Cerebral malformations", 
          "Childhood onset dystonia or chorea or related movement disorder", "Cholestasis", 
          "Combined factor V and VIII deficiency", "Common craniosynostosis syndromes", 
          "Confirmed Fanconi anaemia or Bloom syndrome", "Congenital fibrosis of the extraocular muscles", 
          "Corneal dystrophies", "Currarino triad", "Cutaneous photosensitivity with a likely genetic cause", 
          "Cystic renal disease", "Cytopenia - NOT Fanconi anaemia", "DDG2P", "Ductal plate malformation", 
          "Ectodermal dysplasia", "Epidermodysplasia verruciformis", "Epidermolysis bullosa and congenital skin fragility", 
          "Familial dysautonomia", "Familial hypercholesterolaemia - targeted panel", "Familial hyperparathyroidism", 
          "Familial melanoma", "Familial Neural Tube Defects", "Familial prostate cancer", "Familial tumoral calcinosis", 
          "Fetal anomalies", "Glycogen storage disease", "Growth failure in early childhood", 
          "Hereditary ataxia - adult onset", "Hereditary ataxia and cerebellar anomalies - childhood onset", 
          "Hereditary Erythrocytosis", "Hereditary neuropathy NOT PMP22 copy number", 
          "Hereditary spastic paraplegia - adult onset", "Hereditary spastic paraplegia - childhood onset", 
          "Hydrocephalus", "Hypocalciuric hypercalcaemia", "Hypogonadotropic hypogonadism idiopathic", 
          "Hypophosphataemia or rickets", "Hypotonic infant", "Ichthyosis and erythrokeratoderma", 
          "Inborn errors of metabolism", "Inherited MMR deficiency (Lynch syndrome)", "Inherited pancreatic cancer", 
          "Inherited polyposis", "Inherited predisposition to acute myeloid leukaemia (AML)", 
          "Inherited predisposition to GIST", "Inherited renal cancer", "Intestinal failure", 
          "Iron metabolism disorders", "Laterality disorders and isomerism", "Leber hereditary optic neuropathy", 
          "Limb disorders", "Lipodystrophy - childhood onset", "Lipoprotein lipase deficiency", 
          "Lysosomal storage disorder", "Mitochondrial disorder with complex I deficiency", 
          "Mitochondrial disorder with complex II deficiency", "Mitochondrial disorder with complex III deficiency", 
          "Mitochondrial disorder with complex IV deficiency", "Mitochondrial disorder with complex V deficiency", 
          "Mitochondrial DNA maintenance disorder", "Mitochondrial liver disease", "Monogenic diabetes", 
          "Monogenic nephrogenic diabetes insipidus", "Mosaic skin disorders - deep sequencing", "Multiple lipomas", 
          "Multiple monogenic benign skin tumours", "Neurodegenerative disorders - adult onset", 
          "Neurological segmental overgrowth", "Neuromuscular disorders", "Neuronal ceroid lipofuscinosis", 
          "Non-acute porphyrias", "Ocular and oculo-cutaneous albinism", "Paediatric disorders", 
          "Paediatric disorders - additional genes", "Palmoplantar keratodermas", "Pancreatitis", 
          "Paroxysmal central nervous system disorders", "Pigmentary skin disorders", 
          "Pituitary hormone deficiency", "Pityriasis rubra pilaris", "Polycystic liver disease interim", 
          "Possible mitochondrial disorder - nuclear genes", "Primary immunodeficiency", 
          "Primary pigmented nodular adrenocortical disease", "Progressive cardiac conduction disease", 
          "Pyruvate dehydrogenase (PDH) deficiency", "Rare anaemia", "Rare genetic inflammatory skin disorders", 
          "Rare multisystem ciliopathy Super panel", "Respiratory ciliopathies including non-CF bronchiectasis", 
          "Rhabdoid tumour predisposition", "Segmental overgrowth disorders", "Short QT syndrome", 
          "Skeletal muscle channelopathy", "Structural eye disease", "Sudden cardiac death", "Surfactant deficiency", 
          "Thoracic aortic aneurysm and dissection", "Thrombophilia", "Tubulointerstitial kidney disease", 
          "Unexplained paediatric onset end-stage renal disease", "Vascular skin disorders", 
          "White matter disorders - adult onset", "White matter disorders and cerebral calcification - narrow panel", 
          "White matter disorders - childhood onset"]

genes = []

# panel "Rare Disease"
panels_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/?format=json")
panels_json = panels_response.json()
next_site = True
while next_site:
    for i in range(len(panels_json['results'])):
        try:
            for k in range(len(panels_json['results'][i]['types'])):
                if "Rare Disease" in panels_json['results'][i]['types'][k]['name']:
                    panel_id_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/{}/?format=json".format(panels_json['results'][i]['id']))
                    panel_id_json = panel_id_response.json()
                    for j in range(len(panel_id_json['genes'])):
                        genes.append(panel_id_json['genes'][j]['gene_data']['gene_symbol'])
        except:
            continue
    try:
        panels_response = requests.get("{}".format(panels_json['next']))
        panels_json = panels_response.json()
    except:
        next_site = False

for panel in panels_others:
    panels_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/?format=json")
    panels_json = panels_response.json()
    next_site = True
    while next_site:
        for i in range(len(panels_json['results'])):
            if panels_json['results'][i]['name'] == panel:
                panel_id_response = requests.get("https://panelapp.genomicsengland.co.uk/api/v1/panels/{}/?format=json".format(panels_json['results'][i]['id']))
                panel_id_json = panel_id_response.json()
                for j in range(len(panel_id_json['genes'])):
                    genes.append(panel_id_json['genes'][j]['gene_data']['gene_symbol'])
        try:
            panels_response = requests.get("{}".format(panels_json['next']))
            panels_json = panels_response.json()
        except:
            next_site = False
# unique genes
genes = list(set(genes))
with open("Rare_Diseases.txt", "w") as outfile:
    outfile.write(", ".join(genes))

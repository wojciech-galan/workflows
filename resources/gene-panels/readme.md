#  GENE-PANELS
  
Gene panels developed by clinical molecular laboratories assess multiple potential genetic causes of a suspected disorder(s) 
simultaneously and reduce the cost and time of diagnostic testing.

Websites: 
- https://panelapp.genomicsengland.co.uk/panels/
- https://www.ncbi.nlm.nih.gov/clinvar/docs/acmg/
  
---  
The location of resources:  
  - **GitLab** (main location):  
    - on-line: https://gitlab.com/intelliseq/workflows/-/tree/dev/resources/gene-panels   
---

### Updates
 
**Last update date:** 16-11-2020

**History of versions and updates:**
 - **version** (16-11-2020) 
 - **version** (23-08-2020)
 - **version** (30-07-2020)
     
---

### How to create panels from Genomics England PanelApp

**1. Run *generate-panels.sh* bash script** 

```
./generate-panels.sh
```
 
In a bash script, the *generate-panels.py* python script script was run using *Python 3.8.2* and 
the following libraries are required:

|   library	    |   version	|
|---	        |---	    |
|   requests	|   2.22.0 	|



This files are created:

```bash
├── Cancer_Germline.json
├── Cardiovascular_disorders.json
├── Ciliopathies.json
├── COVID-19_research.json
├── Dermatological_disorders.json
├── Dysmorphic_and_congenital_abnormality_syndromes.json
├── Endocrine_disorders.json
├── Gastroenterological_disorders.json
├── Growth_disorders.json
├── Haematological_and_immunological_disorders.json
├── Haematological_disorders.json
├── Hearing_and_ear_disorders.json
├── Metabolic_disorders.json
├── Neurology_and_neurodevelopmental_disorders.json
├── Ophthalmological_disorders.json
├── Rare_Diseases.json
├── Renal_and_urinary_tract_disorders.json
├── Respiratory_disorders.json
├── Rheumatological_disorders.json
├── Skeletal_disorders.json
├── Tumour_syndromes.json
```

---

### How to create panels from NCBI
#  REFERENCE GENOMES

Directory with reference genomes.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/reference-genomes**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/reference-genomes**

---

## Table of Contents

**Reference genomes**
  * [Broad institute hg19](#broad-institute-hg19)
  * [Broad institute hg38](#broad-institute-hg38)
  * [GRCh38 no alt analysis set](#grch38-no-alt-analysis-set)
  * [GRCh38 no alt plus hs38d1 analysis set](#grch38-no-alt-plus-hs38d1-analysis-set)
  * [beagle-grch38](#beagle-grch38)

[Return to: Resources](./../readme.md)

---


## Broad institute hg19


**Last update date:** 21-11-2019

**Update requirements:** almost never

---

**Description:**

Hg19 reference genome files downloaded from [GATK ftp](ftp://ftp.broadinstitute.org/pub/seq/references/).

**TO DO**: More detailed description is needed:
  + more details about this version of genome itself
  + how to create sequence_grouping.txt
  + how to create indices

[Instruction on how to create Broad Institute hg19 reference genome database](#instruction-on-how-to-create-broad-institute-hg19-reference-genome-database)


**Directory contents:**

```bash
  ├── Homo_sapiens_assembly19.dict
  ├── Homo_sapiens_assembly19.fa
  ├── Homo_sapiens_assembly19.fa.fai
  ├── Homo_sapiens_assembly19.fa.gz
  ├── Homo_sapiens_assembly19.fa.gz.fai
  ├── Homo_sapiens_assembly19.fa.gz.gzi
  ├── Homo_sapiens_assembly19.fasta
  ├── Homo_sapiens_assembly19.fasta.amb
  ├── Homo_sapiens_assembly19.fasta.ann
  ├── Homo_sapiens_assembly19.fasta.bwt
  ├── Homo_sapiens_assembly19.fasta.pac
  ├── Homo_sapiens_assembly19.fasta.sa
  ├── Homo_sapiens_assembly19.sequence_grouping.txt
  └── Homo_sapiens_assembly19.sequence_grouping_with_unmapped.txt
```

 [Return to the table of contents](#table-of-contents)

---

### Instruction on how to create Broad Institute hg19 reference genome database:

**1.**  Prepare the workspace.

```bash
BROAD_INSTITUTE_HG19="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg19"
cd $BROAD_INSTITUTE_HG19
```

**2.**  Download hg19 reference genome files from [GATK ftp](ftp://ftp.broadinstitute.org/pub/seq/references/)

```bash
wget -c ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.fasta
wget -c ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.fasta.fai
wget -c ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.dict

mv Homo_sapiens_assembly19.fasta Homo_sapiens_assembly19.fa
mv Homo_sapiens_assembly19.fasta.fai Homo_sapiens_assembly19.fa.fai
cat Homo_sapiens_assembly19.fa | bgzip > Homo_sapiens_assembly19.fa.gz

bgzip -r Homo_sapiens_assembly19.fa.gz
cp Homo_sapiens_assembly19.fa.fai Homo_sapiens_assembly19.fa.gz.fai
```


 [Return to the table of contents](#table-of-contents)

---


## Broad institute hg38

**Last update date:** 16-03-2020

**Update requirements:** almost never

---

**Description:**

This directory contains Hg38 reference genome files downloaded from [GATK ftp](ftp://ftp.broadinstitute.org/pub/seq/references/).  Additionally, chromosome-wise chunks of the reference genomes were created for chr1, chr2, ... chr22, chrX and chrY-and-the-rest.


**TO DO**: More detailed description is needed:
  + more details about this version of genome itself

[Instruction on how to create Broad Institute hg38 reference genome database](#instruction-on-how-to-create-broad-institute-hg38-reference-genome-database)


**Directory contents:**

```bash
  ├── Homo_sapiens_assembly38.dict
  ├── Homo_sapiens_assembly38.fa
  ├── Homo_sapiens_assembly38.fa.alt
  ├── Homo_sapiens_assembly38.fa.amb
  ├── Homo_sapiens_assembly38.fa.ann
  ├── Homo_sapiens_assembly38.fa.bwt
  ├── Homo_sapiens_assembly38.fa.fai
  ├── Homo_sapiens_assembly38.fa.gz
  ├── Homo_sapiens_assembly38.fa.gz.fai
  ├── Homo_sapiens_assembly38.fa.gz.gzi
  ├── Homo_sapiens_assembly38.fa.pac
  ├── Homo_sapiens_assembly38.fa.sa
  ├── Homo_sapiens_assembly38.sequence_grouping.txt
  ├── Homo_sapiens_assembly38.sequence_grouping_with_unmapped.txt
  └── chromosome-wise
      ├── chr1
      │   ├── chr1.Homo_sapiens_assembly38.amb
      │   ├── chr1.Homo_sapiens_assembly38.ann
      │   ├── chr1.Homo_sapiens_assembly38.pac
      │   ├── chr1.Homo_sapiens_assembly38.bwt
      │   ├── chr1.Homo_sapiens_assembly38.sa
      │   ├── chr1.Homo_sapiens_assembly38.dict
      │   ├── chr1.Homo_sapiens_assembly38.fa
      │   ├── chr1.Homo_sapiens_assembly38.fa.fai
      │   ├── chr1.Homo_sapiens_assembly38.fa.gz
      │   ├── chr1.Homo_sapiens_assembly38.fa.gz.fai
      │   └── chr1.Homo_sapiens_assembly38.fa.gz.gzi
      ├── chr2
      │   ├── chr2.Homo_sapiens_assembly38.amb
      │  (...)
      │   └── chr2.Homo_sapiens_assembly38.fa.gz.gzi
    (...)
      ├── chrX
      │   ├── chrX.Homo_sapiens_assembly38.amb
      │ (...)
      │   └── chrX.Homo_sapiens_assembly38.fa.gz.gzi
      └── chrY-and-the-rest
          ├── chrY-and-the-rest.Homo_sapiens_assembly38.amb
        (...)
          └── chrY-and-the-rest.Homo_sapiens_assembly38.fa.gz.gzi
```

 [Return to the table of contents](#table-of-contents)

---

### Instruction on how to create Broad Institute hg38 reference genome database:

**1.**  Prepare the workspace.

```bash
BROAD_INSTITUTE_HG38="/data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38"
cd $BROAD_INSTITUTE_HG19
```

**2.**  Download hg38 reference genome files from [Broad references Google bucket](https://console.cloud.google.com/storage/browser/broad-references/hg38/v0)

```bash
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta.fai
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.dict

wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta.64.alt
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta.64.amb
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta.64.ann
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta.64.bwt
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta.64.pac
wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.fasta.64.sa

```

**3.** Rename files, bgzip FASTA file, add tbi index and prepare fasta image file:

**Required:** tabix

```bash
mv Homo_sapiens_assembly38.fasta Homo_sapiens_assembly38.fa
mv Homo_sapiens_assembly38.fasta.fai Homo_sapiens_assembly38.fa.fai
mv Homo_sapiens_assembly38.fasta.64.alt Homo_sapiens_assembly38.fa.alt
mv Homo_sapiens_assembly38.fasta.64.amb Homo_sapiens_assembly38.fa.amb
mv Homo_sapiens_assembly38.fasta.64.ann Homo_sapiens_assembly38.fa.ann
mv Homo_sapiens_assembly38.fasta.64.bwt Homo_sapiens_assembly38.fa.bwt
mv Homo_sapiens_assembly38.fasta.64.pac Homo_sapiens_assembly38.fa.pac
mv Homo_sapiens_assembly38.fasta.64.sa Homo_sapiens_assembly38.fa.sa

bgzip Homo_sapiens_assembly38.fa
bgzip -r Homo_sapiens_assembly38.fa.gz
cp Homo_sapiens_assembly38.fa.fai Homo_sapiens_assembly38.fa.gz.fai

gatk BwaMemIndexImageCreator -I Homo_sapiens_assembly38.fa-O Homo_sapiens_assembly38.fa.img ## used in the vcf-filer-align-errors task 
```

**4.** Create sequence_grouping files:
The Commands creating `sequence_grouping.txt` and `sequence_grouping_with_unmapped.txt` were added to Dockerfile (`task_bam-seq-grouping-hg38:1.0.1`),   
`N_BATCHES` was changed from 3 to 17

```bash
cat Homo_sapiens_assembly38.dict | cut -f2,3 | tail -n +2 |  sed 's/SN://' | sed 's/LN://' | awk '{I++;a[I]=$1;b[I]=$2}END{N_BATCHES=3; SUM=0;for(i = 1; i <= I; i++){SUM=SUM+b[i]};DIVIDE=SUM/N_BATCHES;LINE="";SUM=0;ITER=1;for(i = 1; i <= I; i++){SUM=SUM+b[i];LINE=LINE""a[i]":1+\t";if((SUM - 1) >= (ITER * DIVIDE)){print LINE; ITER=ITER+1; LINE=""}}print LINE}' | sed 's/\(.*\)\t/\1/'  > Homo_sapiens_assembly38.sequence_grouping.txt

cp Homo_sapiens_assembly38.sequence_grouping.txt Homo_sapiens_assembly38.sequence_grouping_with_unmapped.txt
printf "\nunmapped" >> Homo_sapiens_assembly38.sequence_grouping_with_unmapped.txt
```

**5.** Create chromosome-wise fasta files:

**Required**: cssplit


 - create directories and enter the chromosome-wise directory (no HLA)
	```bash
	mkdir chromosome-wise
	cd chromosome-wise
	for i in {1..22}; do mkdir chr$i; done ; mkdir chrX; mkdir chrY-and-the-rest
	```

  - split **Homo_sapiens_assembly38.fa** chromosome-wise

	```bash
	csplit -s -z ../Homo_sapiens_assembly38.fa '/>/' '{*}'
	for i in xx* ; do \
	  n=$(sed 's/>// ; s/ .*// ; 1q' "$i") ; \
	  mv "$i" "$n.Homo_sapiens_assembly38.fa" ; \
	 done
	```
  - mv chr1, (...), chr22, chrX files to corresponding directories

	``````bash
	for i in {1..22}; do
	mv chr$i.Homo_sapiens_assembly38.fa chr$i/chr$i.Homo_sapiens_assembly38.fa
	done;
	mv chrX.Homo_sapiens_assembly38.fa chrX/chrX.Homo_sapiens_assembly38.fa
	```
  - create chrY-and-the-rest fasta

	``````bash
	touch chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa
	cat ../Homo_sapiens_assembly38.dict | grep -o -P "SN:.*?\t" | sed "s/SN://g" | grep -v -P "chr[1-2]?[0-9X]\t" | sed "s/\t//g" | grep -v HLA | awk '{print "cat "$1".Homo_sapiens_assembly38.fa >> chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa"}' | bash
	```
	``````bash
	cat ../Homo_sapiens_assembly38.dict | grep -o -P "SN:.*?\t" | sed "s/SN://g" | grep -v -P "chr[1-2]?[0-9X]\t" | sed "s/\t//g" | grep -v HLA | awk '{print "rm "$1".Homo_sapiens_assembly38.fa"}' | bash
	rm HLA*
	```

**6.** Create indices and dict for chromosome-wise fasta and bzgipped fasta files:


**Requires:** GATK4

**Requires:** bwa

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "cat chr$i/chr$i.Homo_sapiens_assembly38.fa | bgzip > chr$i/chr$i.Homo_sapiens_assembly38.fa.gz &"
done; echo "cat chrX/chrX.Homo_sapiens_assembly38.fa | bgzip > chrX/chrX.Homo_sapiens_assembly38.fa.gz &"; echo "cat chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa | bgzip > chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa.gz &"
```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "gatk CreateSequenceDictionary -R chr$i/chr$i.Homo_sapiens_assembly38.fa.gz &"
done; echo "gatk CreateSequenceDictionary -R chrX/chrX.Homo_sapiens_assembly38.fa.gz &"; echo "gatk CreateSequenceDictionary -R chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa.gz &"
```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "samtools faidx chr$i/chr$i.Homo_sapiens_assembly38.fa.gz &";
  echo "samtools faidx chr$i/chr$i.Homo_sapiens_assembly38.fa &"
done; echo "samtools faidx chrX/chrX.Homo_sapiens_assembly38.fa.gz &"; echo "samtools faidx chrX/chrX.Homo_sapiens_assembly38.fa &"; echo "samtools faidx  chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa.gz &"; echo "samtools faidx  chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa &"
```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "bwa index chr$i/chr$i.Homo_sapiens_assembly38.fa &";
done; echo "bwa index chrX/chrX.Homo_sapiens_assembly38.fa &"; echo "bwa index chrY-and-the-rest/chrY-and-the-rest.Homo_sapiens_assembly38.fa &"
```

 [Return to the table of contents](#table-of-contents)

---

## GRCh38 no alt analysis set

**Last update date:** 16-03-2020

**Update requirements:** almost never

---

**Description:**

This directory contains GRCh38 (no alt analysis set) reference genome files downloaded from [NCBI FTP server](ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/).  Additionally, chromosome-wise chunks of the reference genomes were created for chr1, chr2, ... chr22, chrX and chrY-and-the-rest. 

Directories `bowtie-index` and `bowtie2-index` contain indexes for this genome created by and needed for bowtie/bowtie2 aligner (used in task https://gitlab.com/intelliseq/workflows/raw/fq-bowtie2@1.1.0/src/main/wdl/tasks/fq-bowtie2/fq-bowtie2.wdl).


**TO DO**: More detailed description is needed:
  + more details about this version of genome itself

[Instruction on how to create GRCh38 (no alt analysis set) reference genome database](#instruction-on-how-to-create-grch38-no-alt-analysis-set-reference-genome-database)


**Directory contents:**

```bash
  ├── GRCh38.no_alt_analysis_set.dict
  ├── GRCh38.no_alt_analysis_set.fa
  ├── GRCh38.no_alt_analysis_set.fa.alt # this file is absent and is not needed as alt contigs are not present in this reference 
  ├── GRCh38.no_alt_analysis_set.fa.amb
  ├── GRCh38.no_alt_analysis_set.fa.ann
  ├── GRCh38.no_alt_analysis_set.fa.bwt
  ├── GRCh38.no_alt_analysis_set.fa.fai
  ├── GRCh38.no_alt_analysis_set.fa.gz
  ├── GRCh38.no_alt_analysis_set.fa.gz.fai
  ├── GRCh38.no_alt_analysis_set.fa.gz.gzi
  ├── GRCh38.no_alt_analysis_set.fa.pac
  ├── GRCh38.no_alt_analysis_set.fa.sa
  ├── GRCh38.no_alt_analysis_set.sequence_grouping.txt
  ├── GRCh38.no_alt_analysis_set.sequence_grouping_with_unmapped.txt
  ├── chromosome-wise
      ├── chr1
      │   ├── chr1.GRCh38.no_alt_analysis_set.amb
      │   ├── chr1.GRCh38.no_alt_analysis_set.ann
      │   ├── chr1.GRCh38.no_alt_analysis_set.pac
      │   ├── chr1.GRCh38.no_alt_analysis_set.bwt
      │   ├── chr1.GRCh38.no_alt_analysis_set.sa
      │   ├── chr1.GRCh38.no_alt_analysis_set.dict
      │   ├── chr1.GRCh38.no_alt_analysis_set.fa
      │   ├── chr1.GRCh38.no_alt_analysis_set.fa.fai
      │   ├── chr1.GRCh38.no_alt_analysis_set.fa.gz
      │   ├── chr1.GRCh38.no_alt_analysis_set.fa.gz.fai
      │   └── chr1.GRCh38.no_alt_analysis_set.fa.gz.gzi
      ├── chr2
      │   ├── chr2.GRCh38.no_alt_analysis_set.amb
      │  (...)
      │   └── chr2.GRCh38.no_alt_analysis_set.fa.gz.gzi
    (...)
      ├── chrX
      │   ├── chrX.GRCh38.no_alt_analysis_set.amb
      │ (...)
      │   └── chrX.GRCh38.no_alt_analysis_set.fa.gz.gzi
      └── chrY-and-the-rest
          ├── chrY-and-the-rest.GRCh38.no_alt_analysis_set.amb
        (...)
          └── chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa.gz.gzi
  └── bowtie2-index
      ├── GRCh38.no_alt_analysis_set.1.bt2
      ├── GRCh38.no_alt_analysis_set.2.bt2
      ├── GRCh38.no_alt_analysis_set.3.bt2
      ├── GRCh38.no_alt_analysis_set.4.bt2
      ├── GRCh38.no_alt_analysis_set.rev.1.bt2
      └── GRCh38.no_alt_analysis_set.rev.2.bt2

```

 [Return to the table of contents](#table-of-contents)

---

### Instruction on how to create GRCh38 (no alt analysis set) reference genome database:

**1.**  Prepare the workspace.

```bash
GRCH38_NO_ALT_ANALYSIS_HG38="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set"
cd $GRCH38_NO_ALT_ANALYSIS_HG38
```

**2.**  Download GRCh38 reference genome files from [NCBI FTP server](ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/):

```bash
wget -c ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.gz
wget -c ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.fai
wget -c ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bwa_index.tar.gz

tar -xvf GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bwa_index.tar.gz
rm GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bwa_index.tar.gz

mv GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.gz GRCh38.no_alt_analysis_set.fa.gz
gunzip GRCh38.no_alt_analysis_set.fa.gz
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.fai GRCh38.no_alt_analysis_set.fa.fai
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.amb GRCh38.no_alt_analysis_set.fa.amb
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.ann GRCh38.no_alt_analysis_set.fa.ann
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bwt GRCh38.no_alt_analysis_set.fa.bwt
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.pac GRCh38.no_alt_analysis_set.fa.pac
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.sa GRCh38.no_alt_analysis_set.fa.sa
```

**3.** Create FASTA dictionary, add gzi index:

**Requires**: GATK4

```bash
gatk CreateSequenceDictionary -R GRCh38.no_alt_analysis_set.fa.gz  
bgzip -r GRCh38.no_alt_analysis_set.fa.gz
cp GRCh38.no_alt_analysis_set.fa.fai GRCh38.no_alt_analysis_set.fa.gz.fai
```

**4.** Create sequence_grouping files:
The Commands creating `sequence_grouping.txt` and `sequence_grouping_with_unmapped.txt` were were added to Dockerfile (`task_bam-seq-grouping-grch38-no-alt:1.0.1`),   
`N_BATCHES` was changed from 3 to 17
```bash
cat GRCh38_no_alt_analysis_set.dict | cut -f2,3 | tail -n +2 | sed 's/\t/:/'| cut -d ':' -f 2,4 | sed 's/:/\t/' | awk '{I++;a[I]=$1;b[I]=$2}END{N_BATCHES=17; SUM=0;for(i = 1; i <= I; i++){SUM=SUM+b[i]};DIVIDE=SUM/N_BATCHES;LINE="";SUM=0;ITER=1;for(i = 1; i <= I; i++){SUM=SUM+b[i];LINE=LINE""a[i]":1+\t";if((SUM - 1) >= (ITER * DIVIDE)){print LINE; ITER=ITER+1; LINE=""}}print LINE}' | sed 's/\(.*\)\t/\1/'  > GRCh38_no._alt_analysis_set.sequence_grouping.txt

cp GRCh38.no_alt_analysis_set.sequence_grouping.txt GRCh38.no_alt_analysis_set.sequence_grouping_with_unmapped.txt
printf "\nunmapped" >> GRCh38.no_alt_analysis_set.sequence_grouping_with_unmapped.txt
```

**5.** Create chromosome-wise fasta files:


 - create directories and enter the chromosome-wise directory
	```bash
	mkdir chromosome-wise
	cd chromosome-wise
	for i in {1..22}; do mkdir chr$i; done ; mkdir chrX; mkdir chrY-and-the-rest
	```

  - split **GRCh38.no_alt_analysis_set.fa** chromosome-wise

	```bash
	csplit -s -z ../GRCh38.no_alt_analysis_set.fa '/>/' '{*}'
	for i in xx* ; do \
	  n=$(sed 's/>// ; s/ .*// ; 1q' "$i") ; \
	  mv "$i" "$n.GRCh38.no_alt_analysis_set.fa" ; \
	 done
	```
  - mv chr1, (...), chr22, chrX files to corresponding directories

	```bash
	for i in {1..22}; do
	mv chr$i.GRCh38.no_alt_analysis_set.fa chr$i/chr$i.GRCh38.no_alt_analysis_set.fa
	done;
	mv chrX.GRCh38.no_alt_analysis_set.fa chrX/chrX.GRCh38.no_alt_analysis_set.fa
	```
  - create chrY-and-the-rest fasta

	```bash
	touch chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa
	cat ../GRCh38.no_alt_analysis_set.dict | grep -o -P "SN:.*?\t" | sed "s/SN://g" | grep -v -P "chr[1-2]?[0-9X]\t" | sed "s/\t//g" | awk '{print "cat "$1".GRCh38.no_alt_analysis_set.fa >> chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa"}' | bash

	cat ../GRCh38.no_alt_analysis_set.dict | grep -o -P "SN:.*?\t" | sed "s/SN://g" | grep -v -P "chr[1-2]?[0-9X]\t" | sed "s/\t//g" | awk '{print "rm "$1".GRCh38.no_alt_analysis_set.fa"}' | bash
	```
**5.** Create indices and dict for chromosome-wise fasta and bzgipped fasta files:


**Requires:** GATK4

**Requires:** bwa

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "cat chr$i/chr$i.GRCh38.no_alt_analysis_set.fa | bgzip > chr$i/chr$i.GRCh38.no_alt_analysis_set.fa.gz &"
done; echo "cat chrX/chrX.GRCh38.no_alt_analysis_set.fa | bgzip > chrX/chrX.GRCh38.no_alt_analysis_set.fa.gz &"; echo "cat chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa | bgzip > chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa.gz &"
```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "gatk CreateSequenceDictionary -R chr$i/chr$i.GRCh38.no_alt_analysis_set.fa.gz &"
done; echo "gatk CreateSequenceDictionary -R chrX/chrX.GRCh38.no_alt_analysis_set.fa.gz &"; echo "gatk CreateSequenceDictionary -R chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa.gz &"
```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "samtools faidx chr$i/chr$i.GRCh38.no_alt_analysis_set.fa.gz &";
  echo "samtools faidx chr$i/chr$i.GRCh38.no_alt_analysis_set.fa &"
done;
echo "samtools faidx chrX/chrX.GRCh38.no_alt_analysis_set.fa.gz &"; echo "samtools faidx chrX/chrX.GRCh38.no_alt_analysis_set.fa &"; echo "samtools faidx  chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa.gz &"; echo "samtools faidx  chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa &"
```

The commands generated by script below must be executed (they can be executed in parallel):
```bash
for i in {1..22}; do
  echo "bwa index chr$i/chr$i.GRCh38.no_alt_analysis_set.fa &";
done; echo "bwa index chrX/chrX.GRCh38.no_alt_analysis_set.fa &"; echo "bwa index chrY-and-the-rest/chrY-and-the-rest.GRCh38.no_alt_analysis_set.fa &"
```

### Instruction on how to create GRCh38 (no alt analysis set) reference genome Bowtie2 index from fasta file:

**Requires**: bowtie2

``` bowtie2-build -f /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa GRCh38.no_alt_analysis_set ```

 [Return to the table of contents](#table-of-contents)

---

## GRCh38 no alt plus hs38d1 analysis set

**Last update date:** 21-11-2019

**Update requirements:** almost never

---

**Description:**

This directory contains GRCh38 (no alt plus hs38d1 analysis set) reference genome files downloaded from [NCBI FTP server](ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/).


**TO DO**: More detailed description is needed:
  + more details about this version of genome itself

[Instruction on how to create GRCh38 (no alt analysis set plus hs38d1) reference genome database](#instruction-on-how-to-create-grch38-no-alt-plus-hs38d1-analysis-set-reference-genome-database)


**Directory contents:**

```bash
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.dict
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.amb
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.ann
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.bwt
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.fai
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.gz
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.gz.fai
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.pac
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.fa.sa
  ├── GRCh38.no_alt_plus_hs38d1_analysis_set.sequence_grouping.txt
  └── GRCh38.no_alt_plus_hs38d1_analysis_set.sequence_grouping_with_unmapped.txt
```

 [Return to the table of contents](#table-of-contents)

---

### Instruction on how to create GRCh38 (no alt analysis set plus hs38d1) reference genome database:

**1.**  Prepare the workspace.

```bash
GRCH38_NO_ALT_plus_HS38D1_ANALYSIS_HG38="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-plus-hs38d1-analysis-set"
cd $GRCH38_NO_ALT_plus_HS38D1_ANALYSIS_HG38
```


**2.**  Download GRCh38 reference genome files from [NCBI FTP server](ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/):

```bash
wget -c ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.gz
wget -c ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.fai
wget -c ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.bwa_index.tar.gz

tar -xvf GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.bwa_index.tar.gz
rm GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.bwa_index.tar.gz

mv GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.gz GRCh38.no_alt_plus_hs38d1_analysis_set.fa.gz
mv GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.fai GRCh38.no_alt_plus_hs38d1_analysis_set.fa.fai
mv GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.amb GRCh38.no_alt_plus_hs38d1_analysis_set.fa.amb
mv GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.ann GRCh38.no_alt_plus_hs38d1_analysis_set.fa.ann
mv GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.bwt GRCh38.no_alt_plus_hs38d1_analysis_set.fa.bwt
mv GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.pac GRCh38.no_alt_plus_hs38d1_analysis_set.fa.pac
mv GCA_000001405.15_GRCh38_no_alt_plus_hs38d1_analysis_set.fna.sa GRCh38.no_alt_plus_hs38d1_analysis_set.fa.sa
```

**3.** Create FASTA dictionary:

**Requires**: GATK4

```bash
gatk CreateSequenceDictionary -R GRCh38.no_alt_plus_hs38d1_analysis_set.fa.gz  
bgzip -r GRCh38.no_alt_plus_hs38d1_analysis_set.fa.gz
cp GRCh38.no_alt_plus_hs38d1_analysis_set.fa.fai GRCh38.no_alt_plus_hs38d1_analysis_set.fa.gz.fai
```

**4.** Create sequence_grouping files:

```bash
cat GRCh38.no_alt_plus_hs38d1_analysis_set.dict | cut -f2,3 | tail -n +2 | sed 's/\t/:/'| cut -d ':' -f 2,4 | sed 's/:/\t/' | awk '{I++;a[I]=$1;b[I]=$2}END{N_BATCHES=3; SUM=0;for(i = 1; i <= I; i++){SUM=SUM+b[i]};DIVIDE=SUM/N_BATCHES;LINE="";SUM=0;ITER=1;for(i = 1; i <= I; i++){SUM=SUM+b[i];LINE=LINE""a[i]":1+\t";if((SUM - 1) >= (ITER * DIVIDE)){print LINE; ITER=ITER+1; LINE=""}}print LINE}' | sed 's/\(.*\)\t/\1/' | tr -d '\n' > GRCh38.no_alt_plus_hs38d1_analysis_set.sequence_grouping.txt

cp GRCh38.no_alt_plus_hs38d1_analysis_set.sequence_grouping.txt GRCh38.no_alt_plus_hs38d1_analysis_set.sequence_grouping_with_unmapped.txt
printf "\nunmapped" >> GRCh38.no_alt_plus_hs38d1_analysis_set.sequence_grouping_with_unmapped.txt
```

 [Return to the table of contents](#table-of-contents)  

## Beagle GRCh38


**Last update date:** 25-05-2021

**Update requirements:** almost never

---

**Description:**

This reference genome contains only chromosomes 1..22 X, Y and M, without the `chr` prefix.
Is needed for Beagle resources preparation and for Beagle output vcfs merging.

[Instruction on how to create Beagle GRCh38 reference files](#instruction-on-how-to-create-beagle-grch38-reference-files) 


**Directory contents:**

```bash
  
├── grch38-ref.dict
├── grch38-ref.fa.gz
├── grch38-ref.fa.gz.fai
└── grch38-ref.fa.gz.gzi

```

 [Return to the table of contents](#table-of-contents)

---


### Instruction on how to create Beagle GRCh38 reference files:

**1.**  Prepare the workspace.

```bash
BEAGLE_GRCH38="/data/public/intelliseqngs/workflows/resources/reference-genomes/beagle-grch38"
cd $BEAGLE_GRCH38
```

**2.**  Extract only needed contigs and change their names, prepare indexes 

```bash
ref_file="/data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa.gz"  
for i in {1..22} X Y M; do samtools faidx $ref_file chr"$i" | sed 's/>chr/>/' | bgzip >> grch38-ref.fa.gz; done
samtools faidx grch38-ref.fa.gz
gatk CreateSequenceDictionary -R grch38-ref.fa.gz
```
[Return to the table of contents](#table-of-contents)

---

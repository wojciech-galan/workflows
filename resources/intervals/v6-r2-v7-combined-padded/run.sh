#!/bin/bash

INTERVALS_AGILENT=/data/public/intelliseqngs/workflows/resources/intervals/agilent

gatk IntervalListTools \
       -ACTION CONCAT \
       -SORT true \
       -UNIQUE true \
       -I $INTERVALS_AGILENT/sureselect-human-all-exon-v6-r2/sureselect-human-all-exon-v6-r2.target.broad-institute-hg38.interval_list \
       -I $INTERVALS_AGILENT/sureselect-human-all-exon-v7/sureselect-human-all-exon-v7.target.broad-institute-hg38.interval_list \
       -O new.interval_list \
       --PADDING 400

sed 's/:\|-/\t/gi' new.interval_list | grep -v "^@" | cut -f 1-3 > $INTERVALS_AGILENT/v6-r2-v7-combined-padded/v6-r2-v7-combined-padded.bed




## Pharmcat

**Last update date:** 15-06-20

**Update requirements:** depending on release (last update using development branch of the Pharmcat tool)

To create pgx.vcf.gz and pgx.bed.gz use docker `intelliseqngs/pharmcat-resources:1.0.2` (in repo: `src/main/docker/resources/pharmcat-resources/Dockerfile`):
```
TESTING_FOLDER=/tmp/pharmcat
mkdir -p $TESTING_FOLDER
docker run --rm -it -v ${TESTING_FOLDER}:/outputs intelliseqngs/pharmcat-resources:1.0.2
```
It was created according to documentation: https://github.com/PharmGKB/PharmCAT/wiki/Preparing-VCF-Files

Those files are used in docker `intelliseqngs/pgx-pharmcat`, which is used in task `resources_pharmcat` and `pgx-pharmcat` in module `pharmcat`.

## PharmGkb

Description in progress

```bash
.
└── variant-annotations
    └── 24-02-2020
        ├── CLINICAL_ANNOTATIONS_README.pdf
        ├── CREATED_2020-02-05.txt
        ├── LICENSE.txt
        ├── VARIANT_ANNOTATIONS_README.pdf
        ├── VERSIONS.txt
        ├── annotations.zip
        ├── clinical_ann.tsv
        ├── clinical_ann_metadata.tsv
        ├── study_parameters.tsv
        ├── var_drug_ann.tsv
        ├── var_fa_ann.tsv
        └── var_pheno_ann.tsv
```

# BROAD INSTITUTE MT

Resources for mitochondrial variants calling. Reference genome: **GRCh38**.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/broad-institute-mt**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/broad-institute-mt**

---
**Last update date:** 02-11-2020

**Update requirements:** almost never (with new version of gatk mitochondrial pipeline)

**History of versions:**  
  + 0.0.1 (download date: 02-11-2020)    
  This version corresponds to the Broad Institute mitochondrial resources version v0   
 
---
**Description:**

**[Instructions on how to obtain Broad Institute MT resources](#instructions-on-how-to-obtain-broad-institute-mt-resources)**

**Directory contents:**

```bash
   <broad-institute-mt>
      |
  <version>
      |
      ├── Homo_sapiens_assembly38.chrM.dict                               ## dockers: task_mt-realign, task_mt-varcall-mutect2
      ├── Homo_sapiens_assembly38.chrM.fasta                              ## dockers: task_mt-realign, mitolib, task_mt-varcall-mutect2
      ├── Homo_sapiens_assembly38.chrM.fasta.amb                          ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.fasta.ann                          ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.fasta.bwt                          ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.fasta.fai                          ## dockers: task_mt-realign, mitolib, task_mt-varcall-mutect2
      ├── Homo_sapiens_assembly38.chrM.fasta.pac                          ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.fasta.sa                           ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.dict         ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta        ## dockers: task_mt-realign      
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta.amb    ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta.ann    ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta.bwt    ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta.fai    ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta.pac    ## dockers: task_mt-realign
      ├── Homo_sapiens_assembly38.chrM.shifted_by_8000_bases.fasta.sa     ## dockers: task_mt-realign
      ├── ShiftBack.chain                                                 ## dockers: task_mt-varcall-mutect2
      ├── blacklist_sites.hg38.chrM.bed                                   ## dockers: task_mt-varcall-mutect2
      ├── blacklist_sites.hg38.chrM.bed.idx                               ## dockers: task_mt-varcall-mutect2
      ├── blacklist_sites.hg38.chrM.shifted_by_8000_bases.fixed.bed       
      ├── blacklist_sites.hg38.chrM.shifted_by_8000_bases.fixed.bed.idx
      ├── control_region_shifted.chrM.interval_list
      └── non_control_region.interval_list
```

### Instructions on how to obtain Broad Institute MT resources   

**1.**  Prepare the workspace.


```bash
VERSION="0.0.1"
mkdir $VERSION
cd $VERSION
```

**2.**  Download files

All files are used in the GATK4 mitochondrial pipeline, are 
listed in the [input.json](https://github.com/gatk-workflows/gatk4-mitochondria-pipeline/blob/dev/mitochondria-pipeline.inputs.json) 
and can be downloaded (on anakin) from google cloud with the [**broad-mito-resources.sh**](https://gitlab.com/intelliseq/workflows/blob/broad-mito-resources.sh@0.0.1/resources/broad-institute-mt/broad-mito-resources.sh) script     
```bash
wget https://gitlab.com/intelliseq/workflows/raw/broad-mito-resources.sh@0.0.1/resources/broad-institute-mt/broad-mito-resources.sh  
chmod +x broad-mito-resources.sh  
./broad-mito-resources.sh   
```  
 
[Return to: Resources](./../readme.md) 

---
   

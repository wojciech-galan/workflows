#!/usr/bin/env bash

VERSION='1.0.1'

function version() {
  script_name=`basename "$0"`
  printf "$script_name $VERSION\n"
}

while [[ $# -gt 0 ]]
do
  key="$1"
  case $key in
    -v|--version)
    version
    exit 0;;
    -o|--output)
    export OUTPUT_DIR=$2
    shift # past argument
    shift # past value
    ;;
    *)
    echo "Invalid argument"
    exit 1;
  esac
done

concatenated="concatenated.vcf.gz"
liftovered="liftovered.vcf"
header="header"
bref3_path="/tools/bref3.18May20.d20.jar"
unbref3_path="/tools/unbref3.18May20.d20.jar"

mkdir /work_dir
cd /work_dir

set -e

for chr in {1..22} X; do
  curl -o ${chr}.b37.bref3 http://bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.bref3/chr${chr}.1kg.phase3.v5a.b37.bref3
  echo ${chr}.b37.bref3 " downloaded"
  java -jar $unbref3_path ${chr}.b37.bref3 > ${chr}.b37.vcf
  echo "Created " ${chr}.b37.vcf.gz
done

grep '^#' 22.b37.vcf > $header

cat $(ls -d *.b37.vcf) | grep -v "#" | gzip > /tmp/$concatenated
rm *.b37.vcf
cat <(gzip -c $header) /tmp/$concatenated > $concatenated
rm /tmp/$concatenated
echo "Concatenated reference files"

CrossMap.py vcf /resources/chain_files/hg19ToHg38.over.chain.gz $concatenated /resources/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa $liftovered --discard_unmapped
rm ${liftovered}.unmap
rm $concatenated
echo "Leftover reference data"

grep -v "#" ${liftovered} | awk '{print >$1".38.vcf"}'
rm ${liftovered}

# sort vcfs, find duplicated & so on
for chr in {1..22} X; do
  echo "Processing chrom $chr"
  fname=${chr}.38.vcf
  fname_sorted=${fname}.sorted
  fname_duplicated=${fname}.duplicated
  fname_final=${fname}.final
  fname_final_sorted=${fname}.final.sorted
  fname_invalid=${fname}.invalid
  fname_bref=${chr}.38.bref3

  sort -k2,2 -n $fname > ${fname_sorted}
  rm $fname

  cut -f2 ${fname_sorted} |uniq -d > ${fname_duplicated}

  python3 /intelliseqtools/remove_duplicated.py ${fname_sorted} ${fname_final} ${fname_duplicated} --invalid_out $fname_invalid
  rm $fname_duplicated
  rm $fname_sorted

  sort -k2,2 -n $fname_final > ${fname_final_sorted}
  rm $fname_final

  if [[ ${chr} == "X" ]]; then
    cat $header $fname_final_sorted > X.38.vcf.final.sorted_with_header
    bgzip X.38.vcf.final.sorted_with_header
  else
    cat $header $fname_final_sorted | java -jar $bref3_path > $fname_bref
  fi
done

cp *.38.bref3 $OUTPUT_DIR
cp X.38.vcf.final.sorted_with_header.gz $OUTPUT_DIR/X.38.vcf.gz
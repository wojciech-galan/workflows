#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"

PGX_VCF_GZ="/data/public/intelliseqngs/workflows/resources/pgx/pharmcat/14-06-20/pgx.vcf.gz"
STAR="82"
GENE="CYP2D6"
OUTPUT_DIR="/tmp/create-inputs/"

[ -d $OUTPUT_DIR ] || mkdir $OUTPUT_DIR

bash ${PROJECT_DIR}resources/miscellaneous/simulated-samples/samples/pgx-test/create-inputs/create-inputs.sh $PGX_VCF_GZ $STAR $GENE

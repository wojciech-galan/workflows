#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"

set -e

PGX_VCF_GZ=$1
STAR=$2
GENE=$3
TMP_RESOURCES="/tmp/temporary-resources"
OUTPUT_PATH="$TMP_RESOURCES/$GENE-$STAR"
[ -d $OUTPUT_PATH ] || mkdir -p $OUTPUT_PATH

echo "$OUTPUT_PATH/$GENE-$STAR.vcf.gz"

#Steps 1 and 2 already done in readme descibed.

echo "3. Taking some random mutations and save to another vcf file..."

python3 ${PROJECT_DIR}resources/simulated-samples/gnomadrandom.py -i "$TMP_RESOURCES/cyps-gnomad.vcf.gz" -f ISEQ_GNOMAD_GENOMES_V3_AF_nfe | bgzip > "$OUTPUT_PATH/cyps-gnomad-random.vcf.gz"

zcat $OUTPUT_PATH/cyps-gnomad-random.vcf.gz | tail


echo "4. Taking  your variants with your gene $GENE and star allele $STAR  from pharmcat.vcf.gz $PGX_VCF_GZ..."

python3  ${PROJECT_DIR}resources/miscellaneous/simulated-samples/samples/pgx-test/extract-pgx-variants/extract-pgx-variants.py -p $PGX_VCF_GZ -o "$OUTPUT_PATH/$GENE-$STAR.vcf.gz" -g "$GENE" -s "$STAR"

zcat "$OUTPUT_PATH/$GENE-$STAR.vcf.gz" | tail

echo "5. Concating vcfs (with random mutations from gnomad + pharmcat)..."

vcf-concat $OUTPUT_PATH/cyps-gnomad-random.vcf.gz  $OUTPUT_PATH/$GENE-$STAR.vcf.gz | vcf-sort | bgzip > "$OUTPUT_PATH/$GENE-star-$STAR-and-cyps.vcf.gz"

echo "6. Creating input for simulate-neat.wdl..."

SIMULATION_BED="${PROJECT_DIR}resources/miscellaneous/simulated-samples/bed_files/cyps_2mln.bed"
INPUT_TEMPLATE="${PROJECT_DIR}resources/miscellaneous/simulated-samples/samples/pgx-test/create-inputs/template-inputs.json"
CHROMOSOME="chr22"
SAMPLE_NAME="$GENE-star-$STAR"
SIMULATION_VCF_GZ="$OUTPUT_PATH/$GENE-star-$STAR-and-cyps.vcf.gz"
COVERAGE="30"


sed -e "s#\$SIMULATION_BED#$SIMULATION_BED#" \
-e "s#\$CHROMOSOME#$CHROMOSOME#" \
-e "s#\$SAMPLE_NAME#$SAMPLE_NAME#" \
-e "s#\$SIMULATION_VCF_GZ#$SIMULATION_VCF_GZ#" \
-e "s#\$COVERAGE#$COVERAGE#" \
$INPUT_TEMPLATE > $OUTPUT_PATH/$GENE-star-$STAR-simulate-neat-input.json

echo "$OUTPUT_PATH/$GENE-star-$STAR-simulate-neat-input.json"
cat "$OUTPUT_PATH/$GENE-star-$STAR-simulate-neat-input.json"





#6. Run simulate-neat.wd


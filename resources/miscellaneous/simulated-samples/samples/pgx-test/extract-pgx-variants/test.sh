#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/workflows/' )"

OUT_DIR="/tmp/test"
mkdir -p $OUT_DIR
python3 ${PROJECT_DIR}resources/miscellaneous/simulated-samples/samples/pgx-test/extract-pgx-variants/extract-pgx-variants.py -p /data/public/intelliseqngs/workflows/resources/pgx/pharmcat/14-06-20/pgx.vcf.gz -o "$OUT_DIR/super.vcf.gz" -g "CYP2D6" -s "2"

zcat "$OUT_DIR/super.vcf.gz" | head -n 40


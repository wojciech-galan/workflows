#!/usr/bin/env python3

import re
import argparse
import datetime

from pysam import VariantFile

__version__ = '1.0.1'

info_test = "CYP2D6:*1[122]isG,CYP2D6:*4[15]isR,CYP2D6:*10[2]isA,CYP2D6:*36[10]isA,CYP2D6:*37[3]isA,CYP2D6:*47[3]isA,CYP2D6:*49[3]isA,CYP2D6:*52[3]isA,CYP2D6:*54[3]isA,CYP2D6:*56[4]isR,CYP2D6:*57[11]isA,CYP2D6:*64[3]isA,CYP2D6:*65[3]isA,CYP2D6:*69[4]isA,CYP2D6:*72[3]isA,CYP2D6:*87[3]isA,CYP2D6:*94[3]isA,CYP2D6:*95[3]isA,CYP2D6:*99[3]isA,CYP2D6:*100[3]isA,CYP2D6:*101[3]isA,CYP2D6:*114[4]isA,CYP2D6:*132[4]isA"


def add_header(output_vcf):
    output_vcf.header.add_line('##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype"')
    output_vcf.header.add_line("##fileDate=" + datetime.date.today().strftime("%d-%m-%Y"))
    contigs = list(range(1, 23))  # There is no X and Y in pharmacogenes
    for i in contigs:
        output_vcf.header.add_line('##contig=<ID=chr{},assembly=hg38,species="Homo sapiens">'.format(str(i)))
    output_vcf.header.add_sample('synthetic_sample')


def star_in_line(record, star):
    regexp = re.compile(f'\*{star}\[')
    for i in record.info["PX"]:
        if regexp.search(i):
            return True
    return False


def extract_alt(record, star):
    regexp = re.compile(f'\*{star}' + '\[.*\]is(.{0,3})(.+)?')
    for i in record.info["PX"]:
        found = regexp.search(i)
        if found:
            if found.group(1) == "del":
                return record.alts[0]
            elif found.group(1) == "ins":
                return found.group(2)
            return found.group(1)


def generate_vcf(pgx_vcf_path: str, output_vcf_path: str, star: str, gene: str):
    with VariantFile(pgx_vcf_path, 'r') as pgx_vcf, VariantFile(output_vcf_path, 'w') as output_vcf:
        add_header(output_vcf)

        for rec in pgx_vcf.fetch():
            if "PX" in rec.info.keys() and gene in rec.info["PX"][0] and star_in_line(rec, star):
                alt = extract_alt(rec, star)
                if rec.ref != alt:  # Remove all records where ref=alt - not needed for simulation.
                    new_record = output_vcf.new_record(contig=rec.chrom, start=rec.start, stop=rec.stop,
                                                       alleles=(rec.ref, alt), id=None, filter=rec.filter,
                                                       info=None)

                    new_record.samples['synthetic_sample']['GT'] = (1, 1)
                    output_vcf.write(new_record)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert vcf with Structural variants in format VEP to normal vcf format.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('-p', '--pgx_vcf', help='Path to pgx.vcf')
    parser.add_argument('-g', '--gene', help='Pharmacogene, example: CYP2D6, CYP2C19')
    parser.add_argument('-s', '--star', help='Star allele, example *1, *22, *33')
    parser.add_argument('-o', '--output_vcf_gz_path',
                        help='Path to output vcf file in "indel" style, converted from pgx_vcf_path')
    args = parser.parse_args()
    generate_vcf(args.pgx_vcf, args.output_vcf_gz_path, args.star, args.gene)

# Pgx test samples

Synthetic samples. Needed to validate and test pharmacogenomics software. For example pharmcat.wdl module. Created using multistep process, described in **Steps to create pgx sample**
## Contents
- [Pgx test samples](#pgx-test-samples)
  * [Samples](#samples)
    + [Haploids](#haploids)
    + [Diploids](#diploids)
  * [Steps to create pgx sample](#steps-to-create-pgx-sample)
  *
<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>



## Samples

There are haploids and diploid combinations.
There are small fragments and whole chromosomes.
Used gene is CYP2D6 and two paralogs: CYP2D7 and CYP2D6P.
Variants choosen arbitrally from PharmVar database: https://www.pharmvar.org/gene/CYP2D6

Each sample can be in 3 versions. Versions have the same variants (star allele combination) but differ in simulated region (region flanked with 22 mln bp, whole chromosome or all chromosomes). Simulated region is defined in bed file.
- 22 mln - CYP2D6, CYP2D7 and CYP2D6P region flanked with 22 mln basepairs. Bed: https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/miscellaneous/simulated-samples/bed_files/cyps_2mln.bed
- chrom - whole chromosome with variants included the same as in 22 mln. Bed: https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/miscellaneous/simulated-samples/bed_files/chr22.bed
Most of the samles have only 22 mln flanked regions. It is sufficient to testing pourposes.

Diploids are created by combining fastq files from haploids. Example:
```
zcat h1_1.fq.gz > 1.fq
zcat h1_2.fq.gz > 2.fq
zcat h2_1.fq.gz >> 1.fq
zcat h2_2.fq.gz >> 2.fq
bgzip 1.fq
bgzip 2.fq
```


### Haploids:

| Variant | sample-name | Created versions | Coverage | Date, author | Comment | Germline performed | pharmcat performed |
|-|-|-|-|-|-|-|-|
| CYP2D6*1 | cyps-1-a | 22 mln + chrom22 | 30 and 15| moni.krzyz, 3.11.2020 | Version "a" |  |  |
| CYP2D6*1 | cyps-1-b | 22 mln + chrom22 | 30 and 15  | moni.krzyz, 4.11.2020 | Version "b" |  |  |
| CYP2D6*8 | cyps-8 | 22 mln | 30 and 15 | moni.krzyz, 4.11.2020 |  |  |  |
| CYP2D6*14 | cyps-14 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 ||yes  | y |
| CYP2D6*28 | cyps-28 | 22 mln | 30 and 15| moni.krzyz, 6.11.2020 || yes   | y |
| CYP2D6*35 | cyps-35 | 22 mln | 30 and 15 | moni.krzyz, 5.11.2020 | | yes  |yes  |
| CYP2D6*36 | cyps-36 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 | |  yes |  |
| CYP2D6*40 | cyps-40 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 ||  | yes  y |
| CYP2D6*59 | cyps-59 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 || yes   | y |
| CYP2D6*82 | cyps-82 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 | |  |  |
| CYP2D6*83 | cyps-83 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 | | yes  | y |
| CYP2D6*101 | cyps-101 | 22 mln | 30 and 15 | moni.krzyz, 5.11.2020 || yes   |yes  |

### Diploids

(*coverage 60 made from haploids 30 + 30 and coverage 30 made from haploids 15 + 15 )

| Variant | sample-name | Created versions | Date, author | Comment | Germline performed | pharmcat performed | coverage|
|-|-|-|-|-|-| -| -|
| CYP2D6**1/CYP2D6*1| cyps-1a-1b| 22 mln + chrom22| moni.krzyz, 5.11.2020 | |  |  | 30 and 60 |
| CYP2D6*1/CYP2D6*8 | cyps-1a-8 | 22 mln | moni.krzyz, 5.11.2020 | |  |  | 30 and 60  |
| CYP2D6*14/CYP2D6*28 | cyps-14-28 | 22 mln | moni.krzyz, 6.11.2020 |y| y |  |30 and 60   |
| CYP2D6*35/CYP2D6*36 | cyps-35-36 | 22 mln | moni.krzyz, 6.11.2020 |y | y |  | 30 and 60  |
| CYP2D6*40/CYP2D6*59 | cyps-40-59 | 22 mln | moni.krzyz, 6.11.2020 |y | y |   |30 and 60  |
| CYP2D6*82/CYP2D6*101 | cyps-82-101 | 22 mln | moni.krzyz, 6.11.2020 |y | y |  | 30 and 60  |

### Other
| Variant | sample-name | Created versions | Date, author | Comment | Germline performed | pharmcat performed | coverage|
|-|-|-|-|-|-| -| -|
| None| cyrius-regions |500 and 22000  bases flanked | moni.krzyz, 31.12.2020 | |  |  | 30 |

Cyrius regions - to test if cyrius software returns proper results if all needed regions are included

```
wget https://raw.githubusercontent.com/Illumina/Cyrius/master/data/CYP2D6_region_38.bed
cat CYP2D6_region_38.bed | awk 'BEGIN{OFS="\t"}{print $1, $2 - 500, $3 + 500}' > flanked.bed
sortBed -chrThenSizeD -i flanked.bed > sorted.bed
```
Then use sorted.bed to simulate-neat.wdl (all chromosomes, ploidy=2, coverage=30)

## Steps to create pgx sample with CYP2D6
**Note**: Script was tested only for CYP2D6 gene. Every time you use check: CYP2D6-star-*.vcf.gz and view golden bam in IGV browser.

STEPS 1 and 2 are already done
______________________
1. Create bed file with region to take random mutations from gnomad for regions outside of the pharmacogene
2. Save only wariants from this region to vcf file:

```
bcftools view -R ${PROJECT_DIR}resources/miscellaneous/simulated-samples/bed_files/cyp2d7_2d8p_2mln.bed /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/04-11-2019/chr22.gnomad-genomes-v3.vcf.gz | bgzip > /tmp/temporary-resources/cyps-gnomad.vcf.gz

```
-----------------------------
3. Run Script:
```
STAR="1"
GENE="CYP2D6"
PGX_VCF_GZ="/data/public/intelliseqngs/workflows/resources/pgx/pharmcat/14-06-20/pgx.vcf.gz"

bash ${PROJECT_DIR}resources/miscellaneous/simulated-samples/samples/pgx-test/create-inputs/create-inputs.sh $PGX_VCF_GZ $STAR $GENE

```
4. Run simulate-neat.wdl with input created by script runned above. All data needed to find outputs is printed in console.


#!/usr/bin/python3
from pybiomart import Dataset
import numpy as np
import pandas as pd
import argparse
import sys
import time
from requests.exceptions import HTTPError, ConnectionError, SSLError


VERSION="0.0.1"
AUTHOR = "gitlab.com/olaf.tomaszewski"

if sys.argv[1] == "--version":
    print("oldbiomart-table-maker.py version " + VERSION)
    exit(0)

parser = argparse.ArgumentParser(description='Generates two csv files: unique genes with appris and unique genes without appris')
parser.add_argument('--input-dataset', '-i', metavar='input_dataset', type=str, required=True,
                    help='Name of required dataset containing following genes')
parser.add_argument('--input-host', '-j', metavar='input_host', type=str, required=True, help='Adress of host page')
parser.add_argument('--output-biomart-csv', '-k', metavar='output_biomart_csv', type=str, required=True, help='Unique genes in a table')
parser.add_argument('--max_attempts', type=int, default=8,
                    help='Max number of attempts when retrieving data from biomart.')
args = parser.parse_args()


def attempt_to_get_database_query():
    biomart_available = False
    num_of_attempts = 0
    while not biomart_available:
        try:
            return get_database_query()
            biomart_available = True
        except (HTTPError, ConnectionError, SSLError):
            if num_of_attempts >= args.max_attempts:
                raise
            time.sleep(2 ** num_of_attempts)
        num_of_attempts += 1

def get_database_query():
    dataset = Dataset(name=args.input_dataset,
                      host=args.input_host)

    gencode_query = dataset.query(attributes=['ensembl_gene_id', 'chromosome_name',
                                                'external_gene_name', 'transcript_mane_select',
                                                'ensembl_transcript_id', 'transcript_length',
                                                'transcript_tsl', 'transcript_appris',
                                                'transcript_biotype', 'transcript_gencode_basic'])

    final_query = dataset.query(attributes=['external_gene_name', 'ensembl_gene_id',
                                            'ensembl_transcript_id', 'chromosome_name', 'exon_chrom_start',
                                            'exon_chrom_end'])


    return gencode_query, final_query


def get_proper_transcript(gencode_query):
    protein_coding_not_null_gencodes_mask = np.logical_and(gencode_query['Transcript type'] == "protein_coding", gencode_query['GENCODE basic annotation'].notnull())
    protein_coding_not_null_gencodes = gencode_query[protein_coding_not_null_gencodes_mask]
    return protein_coding_not_null_gencodes


def get_appris(protein_coding_not_null_gencodes, null_refseq_not_null_appris_mask, refseq_genes):
    appris = protein_coding_not_null_gencodes[null_refseq_not_null_appris_mask]
    only_appris_mask = ~appris["Gene stable ID"].isin(refseq_genes)
    only_appris = appris[only_appris_mask]
    unique_appris = only_appris.drop_duplicates(["Gene stable ID"], keep=False)

    dupli_appris_mask = only_appris.duplicated(subset=["Gene stable ID"], keep=False)
    dupli_appris = only_appris[dupli_appris_mask]
    dupli_appris = dupli_appris.set_index('Gene stable ID').sort_index()
    dupli_appris.index = dupli_appris.index.set_names(["Gene stable ID"])

    appris_indexes = list(dupli_appris.index.values)
    unique_dupli_appris_indexes = np.unique(appris_indexes)

    for index in unique_dupli_appris_indexes:
        sorted = dupli_appris.loc[index].sort_values(by=['APPRIS annotation'], ascending=False)
        appris_annotation = sorted['APPRIS annotation'].iloc[0]
        correct_appris = sorted[sorted['APPRIS annotation'] == appris_annotation]
        longest_appris_transcript = correct_appris[
            correct_appris['Transcript length (including UTRs and CDS)'] ==
            correct_appris['Transcript length (including UTRs and CDS)'].max()
        ].reset_index().iloc[0]

        unique_appris = unique_appris.append(longest_appris_transcript, ignore_index=True)

    appris_genes = unique_appris["Gene stable ID"].to_list()

    return appris_genes, unique_appris


def get_refseq(protein_coding_not_null_gencodes, not_null_refseq_mask):
    # refseq is unique anyway
    refseq = protein_coding_not_null_gencodes[not_null_refseq_mask]
    unique_refseq = refseq.drop_duplicates(["Gene stable ID"], keep='first').reset_index()
    refseq_genes = unique_refseq["Gene stable ID"].to_list()

    return refseq_genes, unique_refseq


def get_tsl(protein_coding_not_null_gencodes, null_refseq_null_appris_not_null_tsl, refseq_genes, appris_genes):
    tsl = protein_coding_not_null_gencodes[null_refseq_null_appris_not_null_tsl]
    only_tsl_mask = np.logical_and(~tsl["Gene stable ID"].isin(refseq_genes), ~tsl["Gene stable ID"].isin(appris_genes))
    only_tsl = tsl[only_tsl_mask]
    unique_tsl = only_tsl.drop_duplicates(subset=["Gene stable ID"], keep=False)

    dupli_tsl_mask = only_tsl.duplicated(subset=["Gene stable ID"], keep=False)
    dupli_tsl = only_tsl[dupli_tsl_mask]
    dupli_tsl = dupli_tsl.set_index('Gene stable ID').sort_index()
    dupli_tsl.index = dupli_tsl.index.set_names(["Gene stable ID"])

    tsl_indexes = list(dupli_tsl.index.values)
    unique_dupli_tsl_indexes = np.unique(tsl_indexes)

    for index in unique_dupli_tsl_indexes:
        sorted = dupli_tsl.loc[index].sort_values(by=['Transcript support level (TSL)'], ascending=True)
        tsl_value = sorted['Transcript support level (TSL)'].iloc[0]
        correct_tsl = sorted[sorted['Transcript support level (TSL)'] == tsl_value]
        longest_tsl_transcript = correct_tsl[
            correct_tsl['Transcript length (including UTRs and CDS)'] ==
            correct_tsl['Transcript length (including UTRs and CDS)'].max()
            ].reset_index().iloc[0]

        unique_tsl = unique_tsl.append(longest_tsl_transcript, ignore_index=True)

    return unique_tsl


def get_rest(protein_coding_not_null_gencodes, genes_list):
    rest = protein_coding_not_null_gencodes[
        ~protein_coding_not_null_gencodes['Gene stable ID'].isin(genes_list)
    ]

    unique_rest = rest.drop_duplicates(subset=["Gene stable ID"], keep=False)

    dupli_rest_mask = rest.duplicated(subset=["Gene stable ID"], keep=False)
    dupli_rest = rest[dupli_rest_mask]

    dupli_rest = dupli_rest.set_index('Gene stable ID').sort_index()
    dupli_rest.index = dupli_rest.index.set_names(["Gene stable ID"])
    dupli_rest_indexes = list(dupli_rest.index.values)
    unique_dupli_rest_indexes = np.unique(dupli_rest_indexes)

    for index in unique_dupli_rest_indexes:
        subrest = dupli_rest.loc[index]

        longest_rest_transcript = subrest[
            subrest['Transcript length (including UTRs and CDS)'] ==
            subrest['Transcript length (including UTRs and CDS)'].max()
            ].reset_index().iloc[0]

        unique_rest = unique_rest.append([longest_rest_transcript], ignore_index=True)
    return unique_rest


def get_output_genes_from_final_query(genes, final_query):
    output_transcripts = genes["Transcript stable ID"].to_list()
    output_mask = final_query["Transcript stable ID"].isin(output_transcripts)
    output_genes_df = final_query[output_mask]

    return output_genes_df


if __name__ == "__main__":
    gencode_query, final_query = attempt_to_get_database_query()

    protein_coding_not_null_gencodes = get_proper_transcript(gencode_query)

    not_null_refseq_mask = protein_coding_not_null_gencodes['RefSeq match transcript'].notnull()
    not_null_aprris_mask = protein_coding_not_null_gencodes['APPRIS annotation'].notnull()
    not_null_tsl_mask = protein_coding_not_null_gencodes['Transcript support level (TSL)'].notnull()
    null_refseq_mask = protein_coding_not_null_gencodes['RefSeq match transcript'].isnull
    null_appris_mask = protein_coding_not_null_gencodes['APPRIS annotation'].isnull
    null_refseq_not_null_appris_mask = np.logical_and(null_refseq_mask, not_null_aprris_mask)
    null_refseq_null_appris_not_null_tsl = np.logical_and(np.logical_and(null_refseq_mask, null_appris_mask), not_null_tsl_mask)

    refseq_genes, unique_refseq = get_refseq(protein_coding_not_null_gencodes,
                                             not_null_refseq_mask)

    appris_genes, unique_appris = get_appris(protein_coding_not_null_gencodes,
                                             null_refseq_not_null_appris_mask,
                                             refseq_genes)

    unique_tsl = get_tsl(protein_coding_not_null_gencodes,
                                    null_refseq_null_appris_not_null_tsl,
                                    refseq_genes, appris_genes)

    genes = unique_refseq.append([unique_appris, unique_tsl], ignore_index=True)
    genes = genes.drop(['index'], axis=1)
    genes_list = genes['Gene stable ID'].to_list()

    unique_rest = get_rest(protein_coding_not_null_gencodes, genes_list)
    genes = genes.append([unique_rest], ignore_index=True)

    output_genes_df = get_output_genes_from_final_query(genes, final_query)

    output_genes_df.to_csv(args.output_biomart_csv, sep='\t')

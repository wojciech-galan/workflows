#!/bin/bash
# version: 2.4.0
# last update: 23.03.20
# author: Katarzyna Kolanek, Monika Krzyżanowska
# Dockerfile: https://gitlab.com/intelliseq/workflows/-/tree/dev/src/main/docker/clinvar-annotation/0.1.0
# Dockerimage: intelliseqngs/clinvar-annotation/2.4.0

#TEST

set -e

DATE=$(date +%d-%m-%Y | sed 's/:/-/g')

echo "Creating directory with today's date..."
[ -f outputs/$DATE/ ] || mkdir -p outputs/$DATE/
cd outputs/$DATE/

echo "Downloading data from clinvar..."
[ -f clinvar.vcf.gz ] || wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz
[ -f clinvar.vcf.gz.tbi ] || wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz.tbi
[ -f submission_summary.txt.gz ] || wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz

echo "Removing invalid characters from clinvar.vcf.gz..."
    zcat clinvar.vcf.gz | sed 's/ß/B/g' | sed 's/ã/a/g' | sed 's/ä/a/g' | \
    sed 's/é/e/g' | sed 's/è/e/g' | sed 's/ê/e/g' | sed 's/ë/e/g' | sed 's/ô/o/g' | \
    sed 's/ö/o/g' | sed 's/ú/u/g' | sed 's/ü/u/g' | grep -v '^NW_009646201.1' | \
    bgzip > tmp.vcf.gz
    zcat tmp.vcf.gz | awk '{if (/^#/) {print;} else if (/^MT/) {FS="\t"; OFS="\t"; $1="chrM"; print} else {print "chr"$0}}' | grep -v "NW_009646201.1" | bgzip > tmp1.vcf.gz
    tabix -p vcf tmp1.vcf.gz

echo "Normalize vcf..."
    [ -f clinvar.split.normalized.vcf.gz ] || \
     bcftools norm --fasta-ref /reference-genomes/Homo_sapiens_assembly38.fa.gz --multiallelics -any tmp1.vcf.gz | bgzip > clinvar.split.normalized.vcf.gz
echo "Validate vcf..."
    [ -f clinvar.split.normalized.vcf.gz ] || \
    vcf-validator clinvar.split.normalized.vcf.gz
echo "Tabix: creating index for  vcf..."    
    [ -f clinvar.split.normalized.vcf.gz.tbi ] || \
    tabix -p vcf clinvar.split.normalized.vcf.gz

echo "Running create-clinvar-annotation-file-no-included-info.py..."
    python3 /opt/tools/create-clinvar-annotation-file-no-included-info.py  \
    submission_summary.txt.gz \
    clinvar.split.normalized.vcf.gz | sed 's/enigma_rules,_2015/enigma_rules_2015/g' | bgzip > clinvar-annotation-file.vcf.gz 

echo "Tabix: creating index for vcf..."
    tabix -p vcf clinvar-annotation-file.vcf.gz

echo "Creating bed file..."
    zcat clinvar-annotation-file.vcf.gz | awk '{print $1"\t"($2 - 1)"\t"$2}' | sort -k1,1d -k2,2n | grep -v "^#" | bgzip > clinvar-annotation-file.bed.gz

echo "Tabix: creating index for bed..."
    tabix -p bed clinvar-annotation-file.bed.gz

echo "Removing unnecessary files..."
    rm tmp*
    rm clinvar.split.normalized.vcf.gz
    rm clinvar.split.normalized.vcf.gz*

# for testing:
# scripts/docker-build -t clinvar-annotation:X.X.X --context --nochecksum --nopush
# docker run --rm -it -v /tmp/monika:/outputs -v /data/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38:/reference-genomes --user $(id -u):$(id -g) intelliseqngs/clinvar-annotation:X.X.X

# Clinvar annotation

This data was created using script clinvar-annotation-script.sh. 

Environment is defined in docker image intelliseqngs/intelliseqngs/clinvar-annotation:v0.

Dockerfile: /workflows/src/main/docker/resources/clinvar-annotation/v0.1/Dockerfile

This vcf is created usign vcf from clinvar and submission-summary.txt. 

Release: every month. 
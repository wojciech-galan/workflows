#  BROAD INSTITUTE RESOURCES FOR BQSR AND VQSR

Directory with Broad Institute resources for performing Base Quality Score Recalibration (BQSR) and Base Variant Score Recalibration (VQSR).

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/broad-institute-resources-for-bqsr-and-vqsrs**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/broad-institute-resources-for-bqsr-and-vqsrs**

---

## Table of Contents


**Broad institute resources for BQSR and VQSR**
   * [INDELs, known sites](#indels-known-sites)
   * [SNPs, known sites](#snps-known-sites)
   * [dbSNP for BQSR and VQSR](#dbsnp-for-bqsr-and-vqsr)

[Return to: Resources](./../readme.md)

---

## INDELs, known sites

**Last update date:** 09-08-2020

**Update requirements:** once per few months

**History of updates:**
 - 09-08-2020   
 - 23-04-2019

---

**Description:**

This directory contained VCFs with known INDEL sites downloaded from [Broad google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0/).

[ Instructions on how to create INDELs, known sites database](#instructions-on-how-to-create-indels-known-sites-database)

**Directory contents:**

```bash
├── Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf.gz
├── Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf.gz.tbi
├── Homo_sapiens_assembly38.known_indels.vcf.gz        # not downloaded in 09-08-2020 version
├── Homo_sapiens_assembly38.known_indels.vcf.gz.tbi    # not downloaded in 09-08-2020 version
├── Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
└── Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create INDELs, known sites database:

**1.**  Prepare the workspace.

```bash
INDELS_KNOWN_SITES_DIR="/data/public/intelliseqngs/workflows/resources/broad-institute-resources-for-bqsr-and-vqsr/indels-known-sites"
cd $INDELS_KNOWN_SITES_DIR
```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="09-08-2020"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.** Download files from [Broad google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0/):

```bash
wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi

wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf.gz
wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)

---

## SNPs, known sites

**Last update date:** 08-08-2020  

**Update requirements:** once per few months

**History of updates:**
 - 08-08-2020  
 - 23-04-2019

---

**Description:**

This directory contained VCFs with known SNP sites downloaded from [Broad google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0/).

[ Instructions on how to create SNPs, known sites database](#instructions-on-how-to-create-snps-known-sites-database)

**Directory contents:**

```bash
  ├── 1000G_omni2.5.hg38.vcf.gz
  ├── 1000G_omni2.5.hg38.vcf.gz.tbi
  ├── 1000G_phase1.snps.high_confidence.hg38.vcf.gz
  ├── 1000G_phase1.snps.high_confidence.hg38.vcf.gz.tbi
  ├── hapmap_3.3.hg38.vcf.gz
  └── hapmap_3.3.hg38.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create SNPs, known sites database:

**1.**  Prepare the workspace.

```bash
SNPS_KNOWN_SITES_DIR="/data/public/intelliseqngs/workflows/resources/broad-institute-resources-for-bqsr-and-vqsr/snps-known-sites"
cd $SNPS_KNOWN_SITES_DIR
```
Create folder for  the newest version of the database.
```bash
UPDATE_DATE="08-08-2020"
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.** Download files from [Broad google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0/):

```bash
 wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/hapmap_3.3.hg38.vcf.gz
 wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/hapmap_3.3.hg38.vcf.gz.tbi

 wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/1000G_omni2.5.hg38.vcf.gz
 wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/1000G_omni2.5.hg38.vcf.gz.tbi

 wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/1000G_phase1.snps.high_confidence.hg38.vcf.gz
 wget -c https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/1000G_phase1.snps.high_confidence.hg38.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)

---

## dbSNP for BQSR and VQSR

**Last update date:** 08-08-2020

**Update requirements:** once per few months

**History of version and updates:**
 - build 138 (08-08-2020)

---

**Description:**

This directory contains dbSNP for BQSR and VQSR VCF downloaded from [Broad google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0/)

[ Instructions on how to create dbSNP for BQSR and VQSR sites database](#instructions-on-how-to-create-dbsnp-for-bqsr-and-vqsr-database)

**Directory contents:**

```bash
  ├── Homo_sapiens_assembly38.dbsnp138.vcf.gz
  └── Homo_sapiens_assembly38.dbsnp138.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create dbSNP for BQSR and VQSR database:

**1.**  Prepare the workspace.

```bash
DBSNP_FOR_BQSR_AND_VQSR_DIR="/data/public/intelliseqngs/workflows/resources/broad-institute-resources-for-bqsr-and-vqsr/dbsnp-for-bqsr-and-vqsr"
cd $DBSNP_FOR_BQSR_AND_VQSR_DIR
```
Create folder for  the newest version of the database.
```bash
DBSNP_FOR_BQSR_AND_VQSR_VERSION="build-138"
mkdir $DBSNP_FOR_BQSR_AND_VQSR_VERSION
cd $DBSNP_FOR_BQSR_AND_VQSR_VERSION
```

**2.** Download dbSNP database from [Broad google bucket](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0/), bgzipit and index it with tabix:

```bash
 wget -c https://storage.googleapis.com/broad-references/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf

 bgzip Homo_sapiens_assembly38.dbsnp138.vcf
 tabix -p vcf Homo_sapiens_assembly38.dbsnp138.vcf.gz
```

[Return to the table of contents](#table-of-contents)

#!/usr/bin/env bash

WORKFLOWS=$HOME/workflows/
DATE=$1

OUTPUT="/data/public/intelliseqngs/workflows/resources/inputs/selectable/hpo-terms/$DATE"
DOCKER="intelliseqngs/ubuntu-minimal-20.04:3.0.3"

mkdir -p $OUTPUT

docker run --rm -it -v $WORKFLOWS:/workflows -v $OUTPUT:/tmp -w /workflows $DOCKER python3 resources/inputs/selectable/hpo-terms/create-hpo-terms-json.py -o /tmp/hpo-terms.json





#!/usr/bin/env python3

__version__ = '1.0.0'

import requests
import re
import json
import argparse 


parser = argparse.ArgumentParser(description='Creates json with hpo therms and diseases, example: {"HP:3000072": "Abnormal levator palpebrae superioris morphology"}')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument("--output_name", "-o", help="path to the output file")
args = parser.parse_args()


url="https://raw.githubusercontent.com/obophenotype/human-phenotype-ontology/master/hp.obo"

#url = "https://gitlab.com/intelliseq/workflows/-/raw/hpo-therms-jsno/src/test/resources/data/txt/hpo-terms.txt"

response = requests.get(url)
hpo = response.text
result = re.split('\[Term\]', hpo)

d = {}
for i in result[1:]:
    records = i.split("\n")
    name = [element for element in records if "name: " in element][0]
    hpo_id = [element for element in records if "id: " in element][0]
    d[hpo_id[4:]] = name[6:]

def dict_to_json(dictionary: dict, json_name: str):
    with open(json_name, 'w') as json_simple:
        json.dump(dictionary, json_simple, indent=2)

if __name__ == '__main__':
    dict_to_json(d, args.output_name)
    

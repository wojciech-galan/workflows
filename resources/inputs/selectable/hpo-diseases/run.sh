#!/usr/bin/env bash

WORKFLOWS=$HOME/workflows/
DATE=$1

OUTPUT="/data/public/intelliseqngs/workflows/resources/inputs/selectable/hpo-diseases/$DATE"
DOCKER="intelliseqngs/ubuntu-minimal-20.04:3.0.3"

mkdir -p $OUTPUT
echo "Creating directory..."
mkdir -p "$OUTPUT"

echo "Running..."
docker run --rm -it -v $WORKFLOWS:/workflows -v $OUTPUT:/tmp -w /workflows $DOCKER python3 resources/inputs/selectable/hpo-diseases/create-hpo-diseases-json.py -o /tmp/hpo-diseases.json

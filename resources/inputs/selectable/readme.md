# Few words about those resources

## hpo json
in progress

## pene-panel
in progress

## hpo-terms

**Last update date:** 11-08-20

**Update requirements:** every release 

**History of versions and updates:** (examples)
 - **version** 11-08-20
---

### Description:

This is json file with HPO terms and diseases from HPO database (Human Phenotype Onthology). https://hpo.jax.org/app/ 
This json is needed in intelliseqflow interface to choose diseases and hpo therms by user. 

This resource is created based on database to out purposes. 

### How to create
```
RELEASE_DATE="11-08-2020"
bash resources/inputs/selectable/hpo-terms/run.sh $RELEASE_DATE
```
File appears in: ``/data/public/intelliseqngs/workflows/resources/inputs/selectable/hpo-terms/``

### Hpo json content

```
{
"HP:3000072": "Abnormal levator palpebrae superioris morphology",
"HP:3000073": "Abnormality of levator veli palatini muscle",
"HP:3000074": "Abnormal lingual artery morphology"
}
```

## hpo-diseases

**Last update date:** 11-08-2020

**Update requirements:** every release, the same as above hpo-terms

**History of versions and updates:** (examples)
 - **version** 11-08-20
---

### Description:

This is json file with HPO diseases and DB id from HPO database (Human Phenotype Onthology). https://hpo.jax.org/app/ 
This json is needed in intelliseqflow interface to choose diseases and hpo therms by user. This is the same source and release date as "hpo-terms", but there is seperate instruction how to create and separate scripts, because data derives from different files, with different data structure. Release data is in the middle bottom of the HPO website. 

This resource is created based on database to out purposes. 

### How to create
```
RELEASE_DATE="11-08-20"
bash resources/inputs/selectable/hpo-diseases/run.sh $RELEASE_DATE
```
File appears in: ``/data/public/intelliseqngs/workflows/resources/inputs/selectable/hpo-diseases/``

### Hpo json content

```
{
"Familial cerebral saccular aneurysm": "ORPHA:231160",
"Temtamy syndrome": "ORPHA:1777"
}
```





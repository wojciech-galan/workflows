The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/inputs/selectable/ensembl-genes**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/inputs/selectable/ensembl-genes**


## table of contents

- [ensembl-genes](#ensembl-genes)
  * [Description:](#description-)
  * [How to create](#how-to-create)
  * [Content of directory](#content-of-directory)
  * [File contents](#file-contents)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


## ensembl-genes

**Last update date:** day-month-year (06-10-20)

**Update requirements:** whenever needed 

**History of versions and updates:**
 - **version** 1.0.0 (docker with python script and test)

---

### Description:

Task created to get genes from ensembl database (http://ensembl.org dataset hsapiens_gene_ensembl) and their synonyms in .json dictionary-like format.

### How to create

How to run script locally:
```
python3 gene-name-synonym-to-json.py \
    --input-version 'VERSION' \
    --output-dir 'path/to/output/directory'

```

How to run script in docker (for example on anakin):
```
docker run --rm -it -v /data/public/intelliseqngs/workflows/resources/inputs/selectable/ensembl-genes:/resources \
intelliseqngs/gene-name-synonym-to-json:1.0.0 \
python3 /intelliseqtools/gene-name-synonym-to-json.py \
--input-version '101' \
--output-dir '/resources'
```
version will always be the latest, user must specify it to name the folder

### Content of directory

This is how the directory containing version folders looks like:
```bash
├── 101
├── 102 
└── 103
```
Each folder contains the .json file:

```bash
├── ensembl-genes.json
```


### File contents
Gene name to find synonyms
Gene Synonyms separated by commas



[Return to the table of contents](#table-of-contents)

#!/usr/bin/python3
from pybiomart import Dataset
import json
import argparse
import sys
from requests.exceptions import HTTPError, ConnectionError
import time
from pathlib import Path


VERSION="0.0.1"
AUTHOR = "gitlab.com/olaf.tomaszewski"

if sys.argv[1] == "--version":
    print("gene-name-synonym-to-json.py version " + VERSION)
    exit(0)

parser = argparse.ArgumentParser(description='Generates .json file with ')
parser.add_argument('--input-version', '-j', metavar='input_version', type=str, required=True, help='Version of dataset')
parser.add_argument('--output-dir', '-k', metavar='output_dir', type=str, required=True, help='Directory where the version catalog should be done')
parser.add_argument('--max_attempts', type=int, default=8,
                    help='Max number of attempts when retrieving data from biomart.')
args = parser.parse_args()


def attempt_to_get_database_query():
    biomart_available = False
    num_of_attempts = 0
    max_n_of_attempts = 2
    while not biomart_available:
        try:
            return get_database_query()
            biomart_available = True
        except (HTTPError, ConnectionError):
            if num_of_attempts >= max_n_of_attempts:
                raise
            time.sleep(2 ** num_of_attempts)
        num_of_attempts += 1


def get_database_query():
    dataset = Dataset(name='hsapiens_gene_ensembl',
                      host='http://ensembl.org')

    queried = dataset.query(attributes=['external_gene_name', 'external_synonym'])
    return queried


def rename_na_and_aggregate(gene_synonym):
    gene_synonym = gene_synonym.fillna('')

    aggregated = gene_synonym.groupby('Gene name', as_index=True).agg({
        'Gene name': lambda x: x.iloc[0],
        'Gene Synonym': lambda x: ', '.join(x)
    })

    return aggregated


def get_gene_synonym_dict(indexes):
    gene_synonym_dict = dict()

    for index in indexes:
        gene_mask = aggregated['Gene name'] == index
        gene_synonym_dict[index] = aggregated[gene_mask]['Gene Synonym'].to_string(header=False,
                                                                                   index=False).lstrip()
        if gene_synonym_dict[index]:
            gene_synonym_dict[index] = "Synonyms: " + gene_synonym_dict[index]


    return gene_synonym_dict


def create_version_catalog_and_save_json(gene_synonym_dict):
    version_folder = f'{args.output_dir}/{args.input_version}'
    Path(version_folder).mkdir(parents=True, exist_ok=True)

    with open(f'{version_folder}/ensembl-genes.json', 'w') as file:
        json.dump(gene_synonym_dict, file, indent=2)


def get_index_values_as_list(aggregated):
    return aggregated.index.values.tolist()


if __name__ == "__main__":
    gene_synonym = attempt_to_get_database_query()
    aggregated = rename_na_and_aggregate(gene_synonym)

    indexes = get_index_values_as_list(aggregated)
    gene_synonym_dict = get_gene_synonym_dict(indexes)

    create_version_catalog_and_save_json(gene_synonym_dict)

# ACMG

Resources for American College of Medical Genetics and Genomics (ACMG). Reference genome: **GRCh38**.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg**


The location of archival resources (Archival directory):
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg_files**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/acmg_files**

---

## Table of Contents

**ACMG**
  * [ClinVar](#clinvar)
  * [UniProt](#uniprot)
  * [GnomAD](#gnomad)
  * [Archival directory](#archival-directory)  
  
[Return to: Resources](./../readme.md)

---

## ClinVar

**Last update date:** 09-12-2020

**Update requirements:** monthly

**History of versions:**
  + 09-12-2020
  + 30-07-2020  
  + 20-02-2020
  + 15-07-2019 (Archival directory)

---
**Description:**


**[Instructions on how to create ClinVar dictionaries for ACMG annotation](#instructions-on-how-to-create-clinvar-dictionaries-for-acmg-annotation)**

**Directory contents:**

```bash
   <clinvar>
      |
<last-update-date>
      |
      ├── clinvar-lof-dictionary.json
      ├── clinvar-ms-dictionary.json
      ├── clinvar-pathogenic-sites.tab
      ├── clinvar-protein-changes-dictionary.json
      ├── clinvar.variant-level-annotated-with-snpeff.vcf.gz
      └── clinvar.variant-level-annotated-with-snpeff.vcf.gz.tbi
```

[Return to the table of contents](#table-of-contents)

---


### Instructions on how to create ClinVar dictionaries for ACMG annotation


**1.**  Prepare the workspace.

**Required:** [Latest version of the ClinVar annotation vcf](https://gitlab.com/intelliseq/workflows/-/blob/dev/resources/snps-and-indels-annotations/functional-annotations-variant-level/readme.md#clinvar)  

Set bash variables pointing to the latest *ClinVar annotation vcf*, create directory with the newest version:

```bash
VERSION="09-12-2020"
CLINVAR_ANNOTATION_DIR="/data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/functional-annotations-variant-level/clinvar"
CLINVAR_ANNOTATION_VCF="$CLINVAR_ANNOTATION_DIR/$VERSION/clinvar.variant-level.vcf.gz"

CLINVAR_ACMG_DIR="/data/public/intelliseqngs/workflows/resources/acmg/clinvar"
cd $CLINVAR_ACMG_DIR

UPDATE_DATE=$VERSION
mkdir $UPDATE_DATE
cd $UPDATE_DATE
```

**2.**  Annotate **ClinVar annotation vcf** with SnpEff:  

```bash
zcat $CLINVAR_ANNOTATION_VCF | sed "s/chrM\t/chrMT\t/g" \
   | java -Xmx4g -jar /opt/tools/snpeff-5.0c/snpEff/snpEff.jar \
      -c /opt/tools/snpeff-5.0c/snpEff/snpEff.config \ 
      GRCh38.102 \
      -onlyProtein -strict \
   | sed "s/chrMT\t/chrM\t/g" \
   | sed '/^chrM\t/ { s,WARNING_TRANSCRIPT_INCOMPLETE,,g }' \
   | bgzip > clinvar.variant-level-annotated-with-snpeff.vcf.gz 

tabix -p vcf clinvar.variant-level-annotated-with-snpeff.vcf.gz  

```  
Important notes:  
It is very important to use the same version of SnpEff and SnpEff database, as in the target seq pipeline.  
Thus, it may be more convinient/safe to use [**vcf-anno-snpeff.wdl**](https://gitlab.com/intelliseq/workflows/-/blob/vcf-anno-snpeff@1.2.0/src/main/wdl/tasks/vcf-anno-snpeff/vcf-anno-snpeff.wdl)    
for **ClinVar annotation vcf** annotation. In this case set the **annotation_mode** argument as **clinvar** (add to input json:   
`"annotate_vcf_with_snpeff_workflow.annotate_vcf_with_snpeff.annotation_mode": "clinvar"`)     
   

**3.** Create ClinVar based files needed for ACMG annotation:    
* clinvar-lof-dictionary.json (used by: acmg-pvs1.py)  
* clinvar-ms-dictionary.json (used by: acmg-bp1.py, acmg-pp2.py)  
* clinvar-pathogenic-sites.tab (used by: acmg-pm1.py)   
* clinvar-protein-changes-dictionary.json (used by: acmg-pm1.py, acmg-pm5.py, acmg-ps1.py)  

**Required:** [**prepare-clinvar-lof-dictionary.py, version 0.0.2**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-lof-dictionary.py)  
**Required:** [**prepare-clinvar-ms-dictionary.py, version 0.0.2**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-ms-dictionary.py)  
**Required:** [**prepare-clinvar-pathogenic-sites-list.py, version 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-pathogenic-sites-list.py)    
**Required:** [**prepare-clinvar-protein-changes-dictionary.py, version 0.0.3**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-clinvar-protein-changes-dictionary.py)  

  
```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-lof-dictionary.py@0.0.2/src/main/scripts/resources-tools/prepare-clinvar-lof-dictionary.py  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-ms-dictionary.py@0.0.2/src/main/scripts/resources-tools/prepare-clinvar-ms-dictionary.py  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-pathogenic-sites-list.py@0.0.1/src/main/scripts/resources-tools/prepare-clinvar-pathogenic-sites-list.py  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-clinvar-protein-changes-dictionary.py@0.0.3/src/main/scripts/resources-tools/prepare-clinvar-protein-changes-dictionary.py  
  
python3 prepare-clinvar-lof-dictionary.py -i clinvar.variant-level-annotated-with-snpeff.vcf.gz -o clinvar-lof-dictionary.json   
python3 prepare-clinvar-ms-dictionary.py -i clinvar.variant-level-annotated-with-snpeff.vcf.gz -o clinvar-ms-dictionary.json   
python3 prepare-clinvar-pathogenic-sites-list.py -i clinvar.variant-level-annotated-with-snpeff.vcf.gz -o clinvar-pathogenic-sites.tab   
python3 prepare-clinvar-protein-changes-dictionary.py -i clinvar.variant-level-annotated-with-snpeff.vcf.gz -o clinvar-protein-changes-dictionary.json  
```

*** Version notes:   
* 09-12-2020  
  + new version of SnpEff (5.0c) and SnpEff database (102). This database version is nor compatible with snpEff 4.3t

* 30-07-2020    
  + new versions of scripts (prepare-clinvar-lof-dictionary.py@0.0.2, prepare-clinvar-ms-dictionary.py@0.0.2, prepare-clinvar-protein-changes-dictionary.py@0.0.3)  
  fixed bug with omitting variants from Clinvar vcf lines with many genes;  
  + new snpEff database version (Ensembl 100)     

[Return to the table of contents](#table-of-contents)   

---

## UniProt

**Last update date:** 09.12.2020

**Update requirements:** ?

**History of updates:**  
  + 09.12.2020  
  + 30.07.2020  
  + 24.02.2020
  + 15.09.2020 (Archival directory)


---
**Description:**

**[Instructions on how to create UniProt dictionaries](#instructions-on-how-to-create-uniprot-dictionaries)**


**Directory contents:**

```bash
    <uniprot>
        |
<last update date>
        |
        ├── human-genes-uniprot-ids.txt
        ├── uniprot-functional-data-from-table.json  <- this file is used by the acmg scripts
        ├── uniprot-functional-data.json  # not prepared in this resources version
        ├── uniprot-mutagenesis-data-from-table.json  <- this file is used by the acmg scripts
        ├── uniprot-mutagenesis-data.json # # not prepared in this resources version
        └── uniprot-table.tab

```

---

### Instructions on how to create UniProt dictionaries  

**1.** Create folder for  the newest version of the database.  

```bash
VERSION="09-12-2020"
UNIPROT_ACMG_DIR="/data/public/intelliseqngs/workflows/resources/acmg/uniprot"
cd $UNIPROT_ACMG_DIR

UPDATE_DATE=$VERSION
mkdir $UPDATE_DATE
cd $UPDATE_DATE

```

**2.**  Download files from Ensembl and Uniprot:
```bash
wget -O uniprot-table.tab "https://www.uniprot.org/uniprot/?query=organism:%22Homo%20sapiens%20(Human)%20[9606]%22&format=tab&columns=id,entry%20name,reviewed,protein%20names,genes,organism,length,feature(MUTAGENESIS),feature(ACTIVE%20SITE),feature(BINDING%20SITE),feature(CALCIUM%20BIND),feature(DNA%20BINDING),feature(NP%20BIND),sequence,feature(SIGNAL),comment(POST-TRANSLATIONAL%20MODIFICATION),feature(ZINC%20FINGER)&sort=score"  
wget -O human_genes_temp.txt 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "biotype" value = "protein_coding"/><Attribute name = "external_gene_name" /><Attribute name = "uniprot_gn_id" /></Dataset></Query>'  
awk -F '\t' '$2 != ""' human_genes_temp.txt > human-genes-uniprot-ids.txt  

```  

**3.** Create UniProt based files needed for ACMG annotation:   
* uniprot-functional-data-from-table.json (used by: acmg-pm1.py)    
* uniprot-mutagenesis-data-from-table.json (used by: acmg-ps3.py)    

**Required:** [**prepare-uniprot-functional-data.py, version 0.0.2**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-uniprot-functional-data.py)  
**Required:** [**prepare-uniprot-mutagenesis-data.py, version 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-uniprot-mutagenesis-data.py)  

```bash  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-uniprot-functional-data.py@0.0.2/src/main/scripts/resources-tools/prepare-uniprot-functional-data.py  
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-uniprot-mutagenesis-data.py@0.0.1/src/main/scripts/resources-tools/prepare-uniprot-mutagenesis-data.py  

python3 prepare-uniprot-functional-data.py -u uniprot-table.tab -e human-genes-uniprot-ids.txt -o uniprot-functional-data-from-table.json  
python3 prepare-uniprot-mutagenesis-data.py -u uniprot-table.tab -e human-genes-uniprot-ids.txt -o uniprot-mutagenesis-data-from-table.json  
   
```   
   
[Return to the table of contents](#table-of-contents)  
       
---

## gnomAD

**Last update date:** 24.02.2020

**Update requirements:** almost never

**History of updates:**
  + 24.02.2020
  + 15.07.2020 (Archival directory)  

---
**Description:**

**[Instructions on how to create gnomAD LOF dictionary](#instructions-on-how-to-create-gnomad-LOF-dictionary)**  


**Directory contents:**  
```bash
  <gnomad>
     |
    <v2>
     |
     ├── gnomad-lof-dictionary.json  <-file used for acmg pvs1 annotation
     ├── gnomad-lof-metrics.txt
     └── gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz
```   

[Return to the table of contents](#table-of-contents)

---

### Instructions on how to create gnomAD LOF dictionary  

**1.** Create folder for  the newest version of the database.

```bash
VERSION="v2"
GNOMAD_ACMG_DIR="/data/public/intelliseqngs/workflows/resources/acmg/gnomad"  
cd $GNOMAD_ACMG_DIR

mkdir $VERSION
cd $VERSION

```
**2.**  Download and modify gnomAD constraint metrics file.  
   
The downloaded file contains not unique gene names. Find and remove repeated genes and extract useful data   
(gene name, oe_lof, oe_lof_lower, oe_lof_upper from columns 1,24,29,30):   

```bash
wget 'https://storage.googleapis.com/gnomad-public/release/2.1.1/constraint/gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz'   
zcat gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz > gnomad-temp.txt  
diff <( tail -n +2 gnomad-temp.txt | cut -f 1 | sort ) <( tail -n +2 gnomad-temp.txt | cut -f 1 | sort | uniq ) | grep '<' | cut -f 2 -d ' ' > gnomad-repeated-genes.txt   
grep -w -v -f gnomad-repeated-genes.txt gnomad-temp.txt  | cut -f 1,24,29,30 | grep -v -w 'NA' | tail -n +2  > gnomad-lof-metrics.txt   
rm gnomad-repeated-genes.txt gnomad-temp.txt  

```

**3.** Create final gnomAD LOF dictionary needed for ACMG PVS1 annotation:  
* gnomad-lof-dictionary.json (used by: acmg-pvs1.py)  


**Required:** [**prepare-gnomad-lof-dictionary.py, version 0.0.1**](https://gitlab.com/intelliseq/workflows/-/blob/dev/src/main/scripts/resources-tools/readme.md#prepare-gnomad-lof-dictionary.py)   

```bash
wget https://gitlab.com/intelliseq/workflows/-/raw/prepare-gnomad-lof-dictionary.py@0.0.1/src/main/scripts/resources-tools/prepare-gnomad-lof-dictionary.py   

python3 prepare-gnomad-lof-dictionary.py -i gnomad-lof-metrics.txt  -o gnomad-lof-dictionary.json  
```   
[Return to the table of contents](#table-of-contents)   
   
---
     
## Archival directory       
   
**Directory contents:**  
```bash  
├── clinvar.vcf
├── clinvar_ann.vcf
├── clinvar_gene_changes_dict.txt  <- old version of clinvar-protein-chenges-dictionary.json  
├── clinvar_gene_lof_dict.json  <- old version of clinvar-lof-dictionary.json  
├── clinvar_gene_ms_dict.json  <- old version of clinvar-ms-dictionary.json   
├── clinvar_pathogenic_sites.tab <- old version of clinvar-pathogenic-sites.tab  
├── gnomad_final.txt
├── gnomad_lof_dict.json  <- old version of gnomad-lof-dictionary.json  
├── gnomad_lof_metrics.txt
├── gnomad_populations.json
├── human_genes.txt <- old version of human-genes-uniprot-ids.txt  
├── readme.md
├── uniprot_functional_sites.json <- old version of uniprot-functional-data-from-table.json  
└── uniprot_mutagenesis.json <- old version of uniprot-mutagenesis-data-from-table.json  
```
   
[Return to the table of contents](#table-of-contents)   

---


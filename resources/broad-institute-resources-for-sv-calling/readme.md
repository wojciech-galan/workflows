#  BROAD INSTITUTE RESOURCES FOR SV CALLING

Directory with Broad Institute resources for performing structural variant calling.

---

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/workflows/resources/broad-institute-resources-for-sv-calling**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/workflows/resources/broad-institute-resources-for-sv-calling**

---


## Table of Contents

**Broad institute resources for SV calling**
  * [Ploidy priors tables](#ploidy-priors-tables)

[Return to: Resources](./../readme.md)

---


## Ploidy priors tables

**Last update date:** 27-08-2019  

**Update requirements:** when a need arises

---

**Description:**

**IN PROGRESS**

[Instruction on how to create ploidy priors tables](#instruction-on-how-to-create-ploidy-priors-tables)


**Directory contents:**

```bash
```

[Return to the table of contents](#table-of-contents)

---

### Instruction on how to create ploidy priors tables

**2.**  Create a TSV table based on recommendation in this [thread on GATK forum](https://gatkforums.broadinstitute.org/gatk/discussion/23502/germline-cnv-ploidy-and-best-practices). Use this name: **generic-ploidy-priors-table.without-chr-prefix.tsv**.

**3.**  Add chr prefixes to chromosome names and save to **generic-ploidy-priors-table.with-chr-prefix.tsv**.



 [Return to the table of contents](#table-of-contents)
